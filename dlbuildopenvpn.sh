#!/bin/bash

if [ "$2" = "debug" ]
then
  DBGTST=true
else
  DBGTST=false
fi

download=false
archis=()
if [ "$1" = "arm" ]
then
  archis=(arm)
elif [ "$1" = "aarch64" ]
then
  archis=(aarch64)
elif [ "$1" = "i686" ]
then
  archis=(i686)
elif [ "$1" = "x86_64" ]
then
  archis=(x86_64)
elif [ "$1" = "download" ]
then
  download=true
elif [ "$1" = "all" ]
then
  archis=(arm aarch64 i686 x86_64)
  if [ $DBGTST = false ]
  then
    download=true
  fi
elif [ "$1" = "allnodl" ]
then
  archis=(arm aarch64 i686 x86_64)
  download=false
else
  echo -ne "take your pick :\n\tdownload\tdownload sources\n\tarm\t\tbuild arm 32bits\n\taarch64\t\tbuild arm 64bits\n\ti686\t\tbuild x86 32bits\n\tx86_64\t\tbuild x86 64bits\n\tall\t\tdownload sources and then build all 4 architectures\n\t\tdebug to build only selected repo\n\tallnodl\n\n"
  exit 0
fi

function checkerror(){
  if [ $? -ne 0 ]
  then
    echo " --- ERROR dlbuildopenvpn${1}/${2}"
    exit 1
  else
    echo " +++ SUCCESS dlbuildopenvpn${1}/${2}"
  fi
}

if [ -d "$HOME/Android/Sdk/ndk/28.0.13004108" ]
then
  export ANDROID_NDK="$HOME/Android/Sdk/ndk/28.0.13004108"
fi
if [ ! -d "$ANDROID_NDK/toolchains/llvm/prebuilt" ]
then
  echo -ne "Please set the ANDROID_NDK path : export ANDROID_NDK=sdk/ndk/28.0.13004108\n"
  exit 1
fi
systeme=$(ls -1 "$ANDROID_NDK/toolchains/llvm/prebuilt" | head -n 1)
ndktoolchain="$ANDROID_NDK/toolchains/llvm/prebuilt/${systeme}/bin"
echo $PATH | grep ${ndktoolchain}
if [ $? -ne 0 ]
then
  export PATH="${ANDROID_NDK}:${ndktoolchain}:${PATH}"
fi
export basedir="$PWD"
export NPROC=$(nproc)
export NDK_PLATFORM_LEVEL=24

if [ $download = true ]
then
  rm -fr ${basedir}/dlbuildopenvpn/*
  sync
  mkdir -p ${basedir}/dlbuildopenvpn
  
  [ ! -f ${basedir}/torlockedhashes.txt ] && echo "no locked hash found : building latest versions of cloned repos"
  
  echo -ne "" > ${basedir}/torlatesthashes.txt
  
  function wdhashtor(){
    cd "${1}"
    git log -n1 --format=format:"%ad" >> ${basedir}/torlatesthashes.txt
    echo -ne "     ${1}    " >> ${basedir}/torlatesthashes.txt
    git log -n1 --format=format:"%H" >> ${basedir}/torlatesthashes.txt
    echo -ne "\n" >> ${basedir}/torlatesthashes.txt
    if [ -f "${basedir}/torlockedhashes.txt" ]
    then
      lockedhash=`awk '/ '"${1}"' / {print $NF; exit 0;}' ${basedir}/torlockedhashes.txt`
      if [ "${#lockedhash}" -gt 0 ]
      then
        git checkout "${lockedhash}" .
        echo "--------> git checked out ${lockedhash} <--------- return code = $?"
      fi
    fi
  }
  
  #cd ${basedir}/dlbuildopenvpn  && git clone https://github.com/ARMmbed/mbedtls.git -b v3.5.2
  #echo "--------> git mbedtls $?"
  
  
  # OpenSSL_1_1_1-stable     release/2.4
  # openssl-3.0
  
  #cd ${basedir}/dlbuildopenvpn  && git clone https://github.com/ecki/net-tools.git
  #echo "--------> git nettools $?"
  
  cd ${basedir}/dlbuildopenvpn  && git clone https://github.com/openssl/openssl.git -b OpenSSL_1_1_1-stable
  echo "--------> git openssl $?"
  
  cd ${basedir}/dlbuildopenvpn  && git clone https://github.com/openvpn/openvpn.git -b release/2.5
  echo "--------> git openvpn $?"
  
  

  sync
  
  if [ "$1" = "download" ]
  then
	echo "dowload quit"
    exit 0
  else
	echo "nothing else to do"
  fi
  echo "finished downloading"
fi

for archi in ${archis[@]}
do

  echo " -> building for ${archi}"

  if [ $DBGTST = false ]
  then
    rm -fr ${basedir}/dlbuildopenvpn${archi}/out
    mkdir -p ${basedir}/dlbuildopenvpn${archi}/out
  fi

  cd ${basedir}/dlbuildopenvpn${archi}
  export outdir=${basedir}/dlbuildopenvpn${archi}/out

  if [ $DBGTST = false ]
  then
    # enforce flat structure (/usr/local -> /)
    ln -s . "$outdir/usr"
    ln -s . "$outdir/local"
  fi

  unset CPATH LIBRARY_PATH C_INCLUDE_PATH CPLUS_INCLUDE_PATH

  # abi 21 mini for 64bits & opengl3
  # abi 24 mini for vulkan

  export mesoncpu=${archi}
  export mesoncpufamily=${archi}
  export ndkx=${archi}-linux-android
  export ndkxpath="${archi}-linux-android"
  export CC="${archi}-linux-android${NDK_PLATFORM_LEVEL}-clang"
  export CXX="${archi}-linux-android${NDK_PLATFORM_LEVEL}-clang++"

  if [ "${archi}" == "arm" ]; then

    export ndkx=${ndkx}eabi
    export ndkxpath="${ndkxpath}eabi"

    export mesoncpu=armv7a
    export CC="armv7a-linux-androideabi${NDK_PLATFORM_LEVEL}-clang"
    export CXX="armv7a-linux-androideabi${NDK_PLATFORM_LEVEL}-clang++"
  
  elif [ "${archi}" == "i686" ]; then
    
    export mesoncpufamily="x86"

  fi

  export LDFLAGS="-Wl,-z,max-page-size=16384"
  export AR="llvm-ar"
  export AS="llvm-as"
  export NM="llvm-nm"
  export LD="llvm-ld"
  export RANLIB="llvm-ranlib"
  export STRIP="llvm-strip"
  # dav1d  ../configure: line 5869: aarch64-linux-android-nm: command not found
  #        ../configure: line 5869: arm-linux-androideabi-nm: command not found
  ln -s "${ndktoolchain}/llvm-ar" "${ndktoolchain}/${ndkxpath}-ar"
  ln -s "${ndktoolchain}/llvm-as" "${ndktoolchain}/${ndkxpath}-as"
  ln -s "${ndktoolchain}/llvm-nm" "${ndktoolchain}/${ndkxpath}-nm"
  ln -s "${ndktoolchain}/llvm-ld" "${ndktoolchain}/${ndkxpath}-ld"
  ln -s "${ndktoolchain}/llvm-ranlib" "${ndktoolchain}/${ndkxpath}-ranlib"
  ln -s "${ndktoolchain}/llvm-strip" "${ndktoolchain}/${ndkxpath}-strip"

  export PKG_CONFIG_SYSROOT_DIR="$outdir"
  export PKG_CONFIG_LIBDIR="$outdir/lib"
  unset PKG_CONFIG_PATH
  
  # meson wants to be spoonfed this file, so create it ahead of time
	# also define: release build, static libs and no source downloads at runtime(!!!)
  cat >${outdir}/crossfile.txt <<AAA
[built-in options]
buildtype = 'release'
default_library = 'static'
wrap_mode = 'nodownload'
prefix = '/usr/local'
[binaries]
c = '$CC'
cpp = '$CXX'
ar = '$AR'
strip = '$STRIP'
pkgconfig = 'pkg-config'
[host_machine]
system = 'android'
cpu_family = '${mesoncpufamily}'
cpu = '${mesoncpu}'
endian = 'little'
AAA

  # pour openssl
  export NDK_ABI="${archi}"
  if [ "${archi}" == "aarch64" ]; then
    export NDK_ABI="arm64"
  elif [ "${archi}" == "i686" ]; then
    export NDK_ABI="x86"
  fi
  
  export APP_ABI="${archi}"
  if [ "${archi}" = "arm" ]; then
    export APP_ABI="armeabi-v7a"
  elif [ "${archi}" = "aarch64" ]; then
    export APP_ABI="arm64-v8a"
  elif [ "${archi}" = "i686" ]; then
    export APP_ABI="x86"
  fi
  export APP_PLATFORM="android-${NDK_PLATFORM_LEVEL}"
  export APP_STL="c++_shared"

  ls -al ${ndktoolchain}/$CC
  ls -al ${ndktoolchain}/$AR
  ls -al ${ndktoolchain}/${ndkxpath}*
  ls -al $PKG_CONFIG_SYSROOT_DIR
  
  mkdir -p "${outdir}"/include
  mkdir -p "${outdir}"/lib/pkgconfig
  
  
function mbedtls(){
#------------------------------------------------------------------------------
# mbedtls
#------------------------------------------------------------------------------
rm -fr ${basedir}/dlbuildopenvpn${archi}/mbedtls
cp -a ${basedir}/dlbuildopenvpn/mbedtls ${basedir}/dlbuildopenvpn${archi}/
sync
cd ${basedir}/dlbuildopenvpn${archi}/mbedtls
#------------------------------------------------------------------------------
# patch mbedtls :
#     - do not link shared libraries and keep .so extension
#cat ${basedir}/0001-no-shared-lib-link-aurel32.patch
#cp ${basedir}/0001-no-shared-lib-link-aurel32.patch .
#git apply 0001-no-shared-lib-link-aurel32.patch
#cat ${basedir}/0001-no-shared-lib-link.patch
#cp ${basedir}/0001-no-shared-lib-link.patch .
#git apply 0001-no-shared-lib-link.patch
#checkerror ${archi} "mbedtls patch"
#echo -ne "\n\n\n--------------------\n\n\n"
#------------------------------------------------------------------------------
if [ "$archi" == "i686" ]
then
	./scripts/config.py unset MBEDTLS_AESNI_C
else
	./scripts/config.py set MBEDTLS_AESNI_C
fi
# missing python3-jsonschema python3-markupsafe python3-jinja2
#pipx ensurepath
#pipx install jsonschema
#pipx install jinja2
# scripts/basic.requirements.txt
#make SHARED=1 -j${NPROC} no_test
make -j${NPROC} no_test > /dev/null
checkerror ${archi} "mbedtls make"
make DESTDIR="$outdir" install
}

function openssl(){
rm -fr ${basedir}/dlbuildopenvpn${archi}/openssl
cp -a ${basedir}/dlbuildopenvpn/openssl ${basedir}/dlbuildopenvpn${archi}/
sync
cd ${basedir}/dlbuildopenvpn${archi}/openssl
#git checkout openssl-3.0
export CFLAGS="-Wno-macro-redefined"
mkdir out
cd out
# shared ou no-shared
# deprecated no-ssl2 no-hw
../Configure \
no-comp \
no-dtls \
no-ec2m \
no-psk \
no-srp \
no-ssl3 \
no-camellia \
no-idea \
no-md2 \
no-md4 \
no-mdc2 \
no-rc2 \
no-rc4 \
no-rc5 \
no-rmd160 \
no-whirlpool \
no-dso \
no-ui-console \
no-shared \
no-unit-test \
android-${NDK_ABI} \
-D__ANDROID_API__=${NDK_PLATFORM_LEVEL} \
--prefix=${outdir} \
--openssldir=/
checkerror ${archi} "openssl configure"
export CFLAGS="-fPIC"
make -j${NPROC} install_dev > /dev/null
checkerror ${archi} "openssl make"
}

function nettools(){
rm -fr ${basedir}/dlbuildopenvpn${archi}/net-tools
cp -a ${basedir}/dlbuildopenvpn/net-tools ${basedir}/dlbuildopenvpn${archi}/
sync
cd ${basedir}/dlbuildopenvpn${archi}/net-tools
while read f
do
sed -i -e 's|index(|strchr(|g' "$f"
done < <(find . -type f)
export CFLAGS="-I$outdir/include"
export LDFLAGS="-L$outdir/lib"
cp ../../nettoolsconfig.h config.h
./configure.sh config.h
export CFLAGS="-fPIC"
#make -j${NPROC} config
make -j${NPROC}
checkerror ${archi} "net-tools make"
make -j${NPROC} install DESTDIR="$outdir"
checkerror ${archi} "net-tools make install"
}

function openvpn(){
rm -fr ${basedir}/dlbuildopenvpn${archi}/openvpn
cp -a ${basedir}/dlbuildopenvpn/openvpn ${basedir}/dlbuildopenvpn${archi}/
sync
cd ${basedir}/dlbuildopenvpn${archi}/openvpn
autoreconf -i -v -f
checkerror ${archi} "openvpn autoreconf"
./configure --help
mkdir out
cd out
#--enable-iproute2       enable support for iproute2 [default=no]
#--enable-selinux
export CFLAGS="-I$outdir/include"
export LDFLAGS="-L$outdir/lib"
export ROUTE="/system/bin/ip route"
export IFCONFIG="/system/bin/ifconfig"
export NETSTAT="/system/bin/netstat"
export IPROUTE="/system/bin/ip"
../configure \
--host=${ndkx} \
--disable-lzo \
--enable-iproute2 \
--with-crypto-library=openssl \
--disable-libtool-lock \
--disable-plugin-auth-pam \
--disable-plugin-down-root \
--enable-shared \
--disable-static \
--disable-unit-tests \
--with-pic \
--prefix=/
checkerror ${archi} "openvpn configure"
export CFLAGS="-fPIC"
make -j${NPROC} > /dev/null
checkerror ${archi} "openvpn make"
make -j${NPROC} install DESTDIR="$outdir"
#checkerror ${archi} "openvpn make install"
#find . -type f -ls
}


#nettools
#mbedtls
openssl
openvpn


#find ${outdir} -type f -ls

  #------------------------------------------------------------------------------
  # copy libraries (arm aarch64 i686 x86_64)
  #------------------------------------------------------------------------------
  a=`ls -al ${outdir}/sbin/openvpn | wc -l`
  if [ $a -gt 0 ]
  then

    destidir="${basedir}/app/src/main/cpp/libs/${APP_ABI}"
    #destindir="${basedir}/app/src/main/cpp/include/${APP_ABI}"

    mkdir -p ${destidir}
    #mkdir -p ${destindir}
    cp -vf ${outdir}/sbin/openvpn ${destidir}/libopenvpn.so
    cp -vf ${outdir}/sbin/openvpn ${basedir}/openvpn.${archi}.bin
    #cp -vfr ${outdir}/lib/*.* ${destidir}/
    #cp -fr ${outdir}/include/* ${destindir}/

    #ndktoolchainlib="${ndktoolchainbase}/sysroot/usr/lib/${ndkx}"
    #cp -vfr ${ndktoolchainlib}/21/liblog.so  ${destidir}/
    #cp -vfr ${ndktoolchainlib}/21/libEGL.so ${destidir}/
    #cp -vfr ${ndktoolchainlib}/21/libGLESv2.so ${destidir}/
    #cp -vfr ${ndktoolchainlib}/21/libGLESv3.so ${destidir}/
    #cp -vfr ${ndktoolchainlib}/libc++_shared.so ${destidir}/

    sync
    
    if [ $DBGTST = false ]
    then
      rm -fr ${basedir}/dlbuildopenvpn${archi}/
      
      sync
    fi
  
  else

    echo "error no lib openvpn"
    ls -al ${outdir}/sbin/*.*

  fi
  
done




exit 0





adb push openvpn /storage/emulated/0/Download/
adb push ifconfig /storage/emulated/0/Download/
adb push route /storage/emulated/0/Download/
adb push netstat /storage/emulated/0/Download/

alias ll='ls -al'

cd /data/local/tmp
cp /storage/emulated/0/Download/openvpn .
cp /storage/emulated/0/Download/route .
cp /storage/emulated/0/Download/ifconfig .
cp /storage/emulated/0/Download/netstat .

chmod +x *

./openvpn --config udp.ovpn --auth-user-pass .passwd

--pull-filter ignore "route-gateway"

--pull-filter ignore "redirect-gateway"

--redirect-gateway autolocal


./openvpn --show-gateway

sitnl_send: rtnl: generic error (-101): Network is unreachable
ROUTE_GATEWAY 0.0.0.0



PUSH_REPLY,
redirect-gateway def1 bypass-dhcp,
dhcp-option DNS 10.20.40.1,
route-gateway 10.20.41.1,
topology subnet,ping 20,
ping-restart 120,
ifconfig 10.20.41.2 255.255.255.0,peer-id 0,cipher AES-256-GCM


2024-07-14 21:21:53 net_route_v4_best_gw query: dst 0.0.0.0
2024-07-14 21:21:53 net_route_v4_best_gw result: via 10.20.40.1 dev enp2s0
ROUTE_GATEWAY 10.20.40.1/255.255.255.0 IFACE=enp2s0 HWADDR=2a:07:4c:38:0c:de

2024-07-14 21:01:00 ROUTE_GATEWAY 0.0.0.0
2024-07-14 21:01:00 TUN/TAP device tun0 opened
2024-07-14 21:01:00 /system/bin/ip link set dev tun0 up mtu 1500
2024-07-14 21:01:00 /system/bin/ip link set dev tun0 up
2024-07-14 21:01:00 /system/bin/ip addr add dev tun0 10.20.41.2/24

2024-07-14 21:21:53 net_route_v4_add: 82.64.14.137/32 via 10.20.40.1 dev [NULL] table 0 metric -1
2024-07-14 21:21:53 net_route_v4_add: 0.0.0.0/1 via 10.20.41.1 dev [NULL] table 0 metric -1
2024-07-14 21:21:53 net_route_v4_add: 128.0.0.0/1 via 10.20.41.1 dev [NULL] table 0 metric -1



ip route add 82.64.14.137/32 via 10.20.40.1 dev wlan0 table wlan0
ip route add 0.0.0.0/1 dev tun0 table wlan0
ip route add 128.0.0.0/1 dev tun0 table wlan0



curl 82.64.14.137

curl ipconfig.io




eos:/ # ip rule
0:      from all lookup local

ip route show table main

ip route show table wlan0

ip route show table local

local 10.20.40.44 dev wlan0 proto kernel scope host src 10.20.40.44 
broadcast 10.20.40.255 dev wlan0 proto kernel scope link src 10.20.40.44 
local 10.20.41.2 dev tun1 proto kernel scope host src 10.20.41.2 
broadcast 10.20.41.255 dev tun1 proto kernel scope link src 10.20.41.2 
local 127.0.0.0/8 dev lo proto kernel scope host src 127.0.0.1 
local 127.0.0.1 dev lo proto kernel scope host src 127.0.0.1 
broadcast 127.255.255.255 dev lo proto kernel scope link src 127.0.0.1





curl ipconfig.io --interface wlan0
curl ipconfig.io --interface tun0



iptables -I INPUT 1 -s 10.20.41.0/24 -j ACCEPT
iptables -I INPUT 2 -s 10.20.40.0/24 -j ACCEPT
iptables -I OUTPUT 1 -d 10.20.41.0/24 -j ACCEPT
iptables -I OUTPUT 2 -d 10.20.40.0/24 -j ACCEPT


iptables -D INPUT -s 10.20.41.0/24 -j ACCEPT
iptables -D INPUT -s 10.20.40.0/24 -j ACCEPT
iptables -D OUTPUT -d 10.20.41.0/24 -j ACCEPT
iptables -D OUTPUT -d 10.20.40.0/24 -j ACCEPT


iptables -L -nv


iptables -I INPUT 1 -s 10.0.0.0/8 -d 10.0.0.0/8 -j ACCEPT
iptables -I INPUT 2 -s 82.64.14.137/32 -j ACCEPT
iptables -I INPUT 3 ! -i tun0 -j REJECT
iptables -I OUTPUT 1 -s 10.0.0.0/8 -d 10.0.0.0/8 -j ACCEPT
iptables -I OUTPUT 2 -d 82.64.14.137/32 -j ACCEPT
iptables -I OUTPUT 3 ! -o tun0 -j REJECT







































































