#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <locale.h>
#include <sstream>
#include <stddef.h>
#include <chrono>
#include <stdint.h>

#include <string.h>
#include <unistd.h>
#include <ctype.h>
#include <fcntl.h>
#include <utime.h>
#include <errno.h>
#include <stdarg.h>
#include <limits.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <iconv.h>
#include <langinfo.h>

#include <assert.h>
#include <dirent.h>
#include <math.h>
#include <sys/time.h>
#include <getopt.h>

extern "C" {

#include "libavcodec/jni.h"

#include "libavutil/avutil.h"
#include "libavutil/imgutils.h"
#include "libavformat/avformat.h"
#include "libavcodec/avcodec.h"
#include "libswscale/swscale.h"
#include "libavutil/frame.h"
#include "libavformat/avio.h"
#include <libavutil/file.h>

#include "libavfilter/buffersink.h"
#include "libavfilter/buffersrc.h"
#include "libavutil/opt.h"

#include "libavutil/dict.h"
#include "libavutil/display.h"
#include "libavutil/eval.h"

}


#define AV_FRAME_FLAG_KEY (1 << 1)


#ifdef __clang__
#include <jni.h>
#include <android/native_window_jni.h>
#include <android/native_window.h>

#include <android/log.h>
#define LOG_TAG "YYYxtct"
#define ALOGV(...) __android_log_print(ANDROID_LOG_VERBOSE, LOG_TAG, __VA_ARGS__)

#elif __GNUC__
#define ALOGV(...) (printf(__VA_ARGS__),printf("\n"))
#define printf(...) (printf(__VA_ARGS__),printf("\n"))

//  gcc -lavcodec -lpostproc -lswresample -lavutil -lavformat -lswscale -lavfilter -lavdevice thumb.cpp -o thumb.o && ./thumb.o video.mp4 ~/video.mp4.thmb 300 300 25 100

#endif


#ifndef PHOTOCHIOTTE_THUMB_H
#define PHOTOCHIOTTE_THUMB_H


int bitmapwidth = 0;
int bitmapheight = 0;
int bitmapstride = 0;
int* bitmapint = NULL;


#endif //PHOTOCHIOTTE_THUMB_H


























