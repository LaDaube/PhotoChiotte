#include "player.h"

int nsleep(long sec, long millisec, long microsec){
    struct timespec ts;
    int ret;
    ts.tv_sec = sec;
    ts.tv_nsec = (millisec * 1000L + microsec) * 1000L;
    do {
        ret = nanosleep(&ts, &ts);
    } while (ret && errno == EINTR);
    return ret;
}

static struct singleinstance nst[INTANCEARRAYSIZE];

/**************************************************************
 * Declare JNI_VERSION for use in JNI_Onload/JNI_OnUnLoad
 * Change value if a Java upgrade requires it (prior: JNI_VERSION_1_6)
 **************************************************************/
static jint JNI_VERSION = JNI_VERSION_1_6;
static JavaVM *jvm;

/**************************************************************
 * Initialize the static Class and Method Id variables
 **************************************************************/
jint JNI_OnLoad(JavaVM* vm, void* reserved) {
    jvm = vm;
    // Obtain the JNIEnv from the VM and confirm JNI_VERSION
    JNIEnv* env;
    if (jvm->GetEnv(reinterpret_cast<void**>(&env), JNI_VERSION) != JNI_OK) {
        return JNI_ERR;
    }
    return JNI_VERSION;
}

/**************************************************************
 * Destroy the global static Class Id variables
 **************************************************************/
void JNI_OnUnload(JavaVM *vm, void *reserved) {
    JNIEnv* env;
    vm->GetEnv(reinterpret_cast<void**>(&env), JNI_VERSION);
}

extern "C" JNIEXPORT jint JNICALL Java_la_daube_photochiotte_MPVLib_create(JNIEnv* env, jclass thisClass, jint id) {
    setlocale(LC_NUMERIC, "C");
    if (!env->GetJavaVM(&jvm) && jvm) {
        av_jni_set_java_vm(jvm, NULL);
    }
    //ALOGV("++++++++++++++++++++MPV_CREATE+++++++++++++++++++++++nst[id].wasactive %d nst[id].mpv %d", nst[id].wasactive, nst[id].mpv != NULL);
    nst[id].mpv = mpv_create();
    nst[id].wasactive = true;
    if (!nst[id].mpv)
        return -1;
    mpv_request_log_messages(nst[id].mpv, "v"); // TODO : release with warn instead of verbose
    return 0;
}

extern "C" JNIEXPORT jint JNICALL Java_la_daube_photochiotte_MPVLib_init(JNIEnv* env, jclass thisClass, jint id) {
    if (!nst[id].mpv)
        return -1;
    //ALOGV("++++++++++++++++++++MPV_INIT+++++++++++++++++++++++");
    if (mpv_initialize(nst[id].mpv) < 0)
        return -2;
    return 0;
}

extern "C" JNIEXPORT jint JNICALL Java_la_daube_photochiotte_MPVLib_attachSurface(JNIEnv* env, jclass thisClass, jobject surface_, jint id) {
    nst[id].surface = env->NewGlobalRef(surface_);
    int64_t wid = (int64_t)(intptr_t) nst[id].surface;
    mpv_set_option(nst[id].mpv, "wid", MPV_FORMAT_INT64, (void*) &wid);
    return 0;
}

extern "C" JNIEXPORT jint JNICALL Java_la_daube_photochiotte_MPVLib_detachSurface(JNIEnv* env, jclass thisClass, jint id) {
    int64_t wid = 0;
    mpv_set_option(nst[id].mpv, "wid", MPV_FORMAT_INT64, (void*) &wid);
    env->DeleteGlobalRef(nst[id].surface);
    return 0;
}

extern "C" JNIEXPORT jint JNICALL Java_la_daube_photochiotte_MPVLib_command(JNIEnv* env, jclass thisClass, jobjectArray jarray, jint id) {
    if (!nst[id].mpv) {
        return -1;
    }
    const char *arguments[128] = { 0 };
    int len = env->GetArrayLength(jarray);
    if (len >= sizeof(arguments)/sizeof(arguments[0])) {
        return -2;
    }
    for (int i = 0; i < len; ++i)
        arguments[i] = env->GetStringUTFChars((jstring)env->GetObjectArrayElement(jarray, i), NULL);
    int ret = mpv_command(nst[id].mpv, arguments);
    if (ret < 0)
        ALOGV("--------------- error mpv command %s %d", arguments[0], ret);
    for (int i = 0; i < len; ++i)
        env->ReleaseStringUTFChars((jstring)env->GetObjectArrayElement(jarray, i), arguments[i]);
    return ret;
}

static int64_t size_fn(void *cookie)
{
    jint id = *((jint*) cookie);
    return nst[id].vidbuffersize; // MPV_ERROR_UNSUPPORTED necessary to keep stream opened even if no data comes in for longer than 2 seconds
}

static int64_t seek_fn(void *cookie, int64_t offset) {
    jint id = *((jint*) cookie);
    return offset;
}

static void close_fn(void *cookie) {
    jint id = *((jint*) cookie);
    nst[id].vidbufferreaduntil = nst[id].vidbufferend; // free java from endless loop while (cppbufferpos < bufferposition) {
}

extern "C" JNIEXPORT jint JNICALL Java_la_daube_photochiotte_MPVLib_sendBufferRegion(JNIEnv* env, jclass thisClass, jint bufferend, jint id) {
    nst[id].vidbufferend = bufferend;
    if (bufferend == -1) {         // finished reading and there is nothing more in the playlist
        nst[id].vidbuffersize = 0;
    } else if (bufferend == 0) {   // start over read from beginning
        nst[id].vidbufferreaduntil = 0;
    }
    return nst[id].vidbufferreaduntil;
}

static int64_t read_fn_buffer(void *cookie, char *buf, uint64_t nbytes) {
    jint id = *((jint*) cookie);
    while (nst[id].vidbufferreaduntil == nst[id].vidbufferend) {
        //auto start = std::chrono::high_resolution_clock::now();
        nsleep(0, 500, 0);
        //auto end = std::chrono::high_resolution_clock::now();
        //auto duration = std::chrono::duration_cast<std::chrono::nanoseconds>(end-start).count();
    }
    if (nst[id].vidbufferend == -1) {
        return 0;
    }
    if (nst[id].vidbufferreaduntil > nst[id].vidbufferend) {
        nst[id].vidbufferreaduntil = 0;
    }
    int i = nst[id].vidbufferreaduntil;
    int f = nst[id].vidbufferend;
    int d = f-i;
    if (d > 0) {
        int size = d;
        if (nbytes <= d) {
            size = nbytes;
        }
        for (int x = 0 ; x < size ; x++) {
            buf[x] = nst[id].vidbuffer[i + x];
        }
        nst[id].vidbufferreaduntil += size;
        return size;
    }
    return 0;
}

static int open_fn(void *user_data, char *uri, mpv_stream_cb_info *info) {
    jint id = *((jint*) user_data);
    nst[id].vidbufferend = 0;
    nst[id].vidbufferreaduntil = 0;
    nst[id].vidbuffersize = MPV_ERROR_UNSUPPORTED;
    info->cookie = user_data;
    info->size_fn = size_fn;
    info->read_fn = read_fn_buffer;
    info->seek_fn = seek_fn;
    info->close_fn = close_fn;
    return 1;
}

extern "C" JNIEXPORT jint JNICALL Java_la_daube_photochiotte_MPVLib_streamcbAddRo(JNIEnv* env, jclass thisClass, jstring jprotocol, jobject data, jint id) {
    if (!nst[id].mpv)
        return -1;
    const char *protocol = env->GetStringUTFChars(jprotocol, NULL);
    jint *arg = (int *)malloc(sizeof(*arg));
    *arg = id;
    mpv_stream_cb_add_ro(nst[id].mpv, protocol, arg, open_fn);
    env->ReleaseStringUTFChars(jprotocol, protocol);
    nst[id].vidbuffer = (char *) env->GetDirectBufferAddress(data);
    return 0;
}

extern "C" JNIEXPORT jint JNICALL Java_la_daube_photochiotte_MPVLib_setOptionString(JNIEnv* env, jclass thisClass, jstring joption, jstring jvalue, jint id) {
    if (!nst[id].mpv) {
        return -1;
    }
    const char *option = env->GetStringUTFChars(joption, NULL);
    const char *value = env->GetStringUTFChars(jvalue, NULL);
    int result = mpv_set_option_string(nst[id].mpv, option, value);
    if (result < 0)
        ALOGV("---------- error mpv setoptionstring %s %s %d", option, value, result);
    env->ReleaseStringUTFChars(joption, option);
    env->ReleaseStringUTFChars(jvalue, value);
    return result;
}

extern "C" JNIEXPORT jint JNICALL Java_la_daube_photochiotte_MPVLib_setProperty(JNIEnv* env, jclass thisClass, jstring jproperty, jstring jvalue, jint id) {
    if (!nst[id].mpv) {
        return -1;
    }
    const char *prop = env->GetStringUTFChars(jproperty, NULL);
    const char *value = env->GetStringUTFChars(jvalue, NULL);
    int result = mpv_set_property_string(nst[id].mpv, prop, value);
    if (result < 0)
        ALOGV("---------- error mpv setoptionstring %s %s %d", prop, value, result);
    env->ReleaseStringUTFChars(jproperty, prop);
    env->ReleaseStringUTFChars(jvalue, value);
    return result;
}

extern "C" JNIEXPORT jint JNICALL Java_la_daube_photochiotte_MPVLib_observeProperty(JNIEnv* env, jclass thisClass, jstring property, jint propertyi, jint format, jint id) {
    if (!nst[id].mpv) {
        return -1;
    }
    const char *prop = env->GetStringUTFChars(property, NULL);
    mpv_observe_property(nst[id].mpv, propertyi, prop, (mpv_format)format);
    env->ReleaseStringUTFChars(property, prop);
    return 0;
}

extern "C" JNIEXPORT jint JNICALL Java_la_daube_photochiotte_MPVLib_unobserveProperty(JNIEnv* env, jclass thisClass, jint propertyi, jint id) {
    if (!nst[id].mpv) {
        return -1;
    }
    mpv_unobserve_property(nst[id].mpv, propertyi);
    return 0;
}

extern "C" JNIEXPORT jstring JNICALL Java_la_daube_photochiotte_MPVLib_getProperty(JNIEnv* env, jclass thisClass, jstring jproperty, jint id) {
    if (!nst[id].mpv) {
        return NULL;
    }
    const char *prop = env->GetStringUTFChars(jproperty, NULL);
    char *output = mpv_get_property_string(nst[id].mpv, prop);
    env->ReleaseStringUTFChars(jproperty, prop);
    if (output == NULL) {
        mpv_free(output);
        return NULL;
    }
    jstring jvalue = env->NewStringUTF(output);
    mpv_free(output);
    return jvalue;
}





extern "C" JNIEXPORT jint JNICALL Java_la_daube_photochiotte_MPVLib_destroy(JNIEnv* env, jclass thisClass, jint id) {
    if (!nst[id].mpv)
        return -1;
    // if we call "quit" we don't need to terminate_destroy
    /*int playercount = 0;
    for (int i = 0; i < INTANCEARRAYSIZE; i++) {
        if (nst[i].mpv != NULL || nst[i].wasactive) {
            playercount += 1;
        }
    }
    if (playercount > maxplayercount)
        maxplayercount = playercount;
    //if (maxplayercount == 1 && nst[id].mpv != NULL)*/
    ALOGV("-------------destroy-----------");
    mpv_terminate_destroy(nst[id].mpv);
    //ALOGV("----------- player count %d max %d ----------- terminate_destroy %d", playercount, maxplayercount, maxplayercount == 1 && nst[id].mpv != NULL);
    //nst[id].wasactive = false;
    nst[id].mpv = NULL;
    return 0;
}

extern "C" JNIEXPORT jint JNICALL Java_la_daube_photochiotte_MPVLib_getEvents(JNIEnv* env, jclass thisClass, jint id, jobject data) {
    if (!nst[id].mpv) {
        return -1;
    }
    const char* ptr;
    static const int maxlen = 1000000;
    static const int buffersize = 55 * 1024 - 5 * 1024;
    jint p = 0;
    char *msgbuffer = (char *) env->GetDirectBufferAddress(data);
    msgbuffer[p] = (char) id;
    p++;
    jint eventcount = 0;
    jboolean nomoreevent = false;
    while (!nomoreevent) {
        if (p > buffersize) {
            break;
        }
        mpv_event *mp_event = mpv_wait_event(nst[id].mpv, 0);
        msgbuffer[p] = (char) mp_event->event_id;
        p++;
        eventcount++;
        switch (mp_event->event_id) {
            case MPV_EVENT_NONE:
                // Nothing happened. Happens on timeouts or sporadic wakeups.
                nomoreevent = true;
                break;
            case MPV_EVENT_LOG_MESSAGE: {
                auto *msg = (mpv_event_log_message *) mp_event->data;
                int restoreposition = p;
                msgbuffer[p] = (char) msg->log_level;
                p++;
                for (int i = 0; i < maxlen; i++, p++) {
                    msgbuffer[p] = (char) msg->prefix[i];
                    if (msgbuffer[p] == 0x0)
                        break;
                }
                p++;
                bool hlsmatch = true;
                for (int i = 0; i < maxlen; i++, p++) {
                    msgbuffer[p] = (char) msg->text[i];
                    if (hlsmatch && i < 4) {
                        if (i == 0 && msgbuffer[p] != 'h')
                            hlsmatch = false;
                        else if (i == 1 && msgbuffer[p] != 'l')
                            hlsmatch = false;
                        else if (i == 2 && msgbuffer[p] != 's')
                            hlsmatch = false;
                        else if (i == 3 && msgbuffer[p] != ':')
                            hlsmatch = false;
                    }
                    if (msgbuffer[p] == 0x0)
                        break;
                }
                p++;
                if (hlsmatch)
                    p = restoreposition;
                break;
            }
            case MPV_EVENT_PROPERTY_CHANGE: {
                auto *mp_property = (mpv_event_property *) mp_event->data;
                msgbuffer[p] = (char) mp_property->format;
                p++;
                for (int i = 0; i < maxlen; i++, p++) {
                    msgbuffer[p] = (char) mp_property->name[i];
                    if (msgbuffer[p] == 0x0)
                        break;
                }
                p++;
                switch (mp_property->format) {
                    case MPV_FORMAT_NONE: // mp_property->data is set to NULL if retrieving the property failed
                        break;
                    case MPV_FORMAT_FLAG: {
                        ptr = *(const char **) &mp_property->data; // we find the address of the first byte
                        for (int i = 0; i < 4; i++, p++)
                            msgbuffer[p] = (char) ptr[3 - i];
                        break;
                    }
                    case MPV_FORMAT_INT64:
                    case MPV_FORMAT_DOUBLE: {
                        ptr = *(const char **) &mp_property->data; // we find the address of the first byte
                        for (int i = 0; i < 8; i++, p++)
                            msgbuffer[p] = (char) ptr[7 - i];
                        break;
                    }
                    case MPV_FORMAT_STRING:
                    case MPV_FORMAT_OSD_STRING: {
                        ptr = *(const char **) mp_property->data;
                        //int l = 0;
                        //char aff[8192];
                        int i = 0;
                        for (i = 0; i < maxlen; i++, p++) {
                            msgbuffer[p] = (char) ptr[i];
                            //if (l < 8192)
                            //    l += snprintf(aff + l, 512, "%c", (char) ptr[i]);
                            if ((char) ptr[i] == 0x0)
                                break;
                        }
                        p++;
                        //ALOGV("<%s>", aff);
                        //ALOGV("string length <%d>", i);
                        break;
                    }
                    case MPV_FORMAT_NODE_MAP:
                    case MPV_FORMAT_NODE:
                    case MPV_FORMAT_NODE_ARRAY:
                    case MPV_FORMAT_BYTE_ARRAY:
                    default:
                        break;
                }
                break;
            }
            case MPV_EVENT_SHUTDOWN: {
                // we called quit we can destroy the mpv handle now
                //nomoreevent = true;
                //ALOGV("------------------------MPV_EVENT_SHUTDOWN--------------------- à éviter");
                //mpv_terminate_destroy(nst[id].mpv); // crash
                //nst[id].wasactive = false;
                //nst[id].mpv = NULL;
                break;
            }
            default:
                break;
        }
    }
    return p;
}

extern "C" JNIEXPORT jint JNICALL Java_la_daube_photochiotte_MPVLib_grabThumbnail(JNIEnv* env, jclass thisClass,
                                       jint id, jobject bybyf) {
    if (!nst[id].mpv) {
        return -1;
    }
    char *bubuf = (char *) env->GetDirectBufferAddress(bybyf);

    mpv_node result;
    {
        mpv_node c, c_arg0, c_arg1;
        mpv_node c_args[2];
        mpv_node_list c_array;
        c_arg0.format = MPV_FORMAT_STRING;
        c_arg0.u.string = (char*) "screenshot-raw";
        c_args[0] = c_arg0;
        c_arg1.format = MPV_FORMAT_STRING;
        c_arg1.u.string = (char*) "video";
        c_args[1] = c_arg1;
        c_array.num = 2;
        c_array.values = c_args;
        c.format = MPV_FORMAT_NODE_ARRAY;
        c.u.list = &c_array;
        if (mpv_command_node(nst[id].mpv, &c, &result) < 0)
            return -1;
    }

    int w, h, s;
    jint p = 0;
    const char* ptr;
    {
        if (result.format != MPV_FORMAT_NODE_MAP)
            return -1;
        for (int i = 0; i < result.u.list->num; i++) {
            std::string key(result.u.list->keys[i]);
            const mpv_node *val = &result.u.list->values[i];
            if (key == "w" || key == "h" || key == "stride") {
                if (val->format != MPV_FORMAT_INT64)
                    return -1;
                int valeur = (int) val->u.int64;
                if (key == "w") {
                    bubuf[p] = 'w';
                    p++;
                    w = valeur;
                } else if (key == "h") {
                    bubuf[p] = 'h';
                    p++;
                    h = valeur;
                } else {
                    bubuf[p] = 's';
                    p++;
                    s = valeur;
                }
                bubuf[p] = (char) ((valeur & 0xff000000) >> 24);
                p++;
                bubuf[p] = (char) ((valeur & 0xff0000) >> 16);
                p++;
                bubuf[p] = (char) ((valeur & 0xff00) >> 8);
                p++;
                bubuf[p] = (char) ((valeur & 0xff));
                p++;
            } else if (key == "format") {
                if (val->format != MPV_FORMAT_STRING)
                    return -1;
                // check that format equals BGR0
                if (strcmp(val->u.string, "bgr0") != 0)
                    return -1;
            } else if (key == "data") {
                if (val->format != MPV_FORMAT_BYTE_ARRAY)
                    return -1;
                ptr = *(const char **) val->u.ba;
                int maxp = 15 + 4 * w * h;
                int maxw = 4 * w;
                int maxsize = s * h;
                int pos = 0;
                for (int l = 0 ; l < h ; l++) {
                    for (int c = 0 ; c < s ; c+=4) {
                        if (p < maxp && c < maxw && pos < maxsize){
                            /*R*/bubuf[p+0] = (char) (ptr[pos+2]); // B
                            /*G*/bubuf[p+1] = (char) (ptr[pos+1]); // G
                            /*B*/bubuf[p+2] = (char) (ptr[pos+0]); // R
                            /*A*/bubuf[p+3] = (char) (ptr[pos+3]); // A
                            p+=4;
                        }
                        pos+=4;
                    }
                }
            }
        }
    }

    mpv_free_node_contents(&result); // frees data->data
    return 0;
}

















