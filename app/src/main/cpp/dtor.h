
int checksignal(int, unsigned char *, int);

void* runTor(void* arg) {
  int i;
  int rc;
  FILE *fp;

  char cachedir[256];
  snprintf(cachedir, 255, "%s%s", PATHPREFIX, PATHPREFIXCACHE);
  char torrc[256];
  snprintf(torrc, 255, "%s%storrc", PATHPREFIX, PATHPREFIXTOR);
  char defaultstorrc[256];
  snprintf(defaultstorrc, 255, "%s%storrc-defaults", PATHPREFIX, PATHPREFIXTOR);
  char datadir[256];
  snprintf(datadir, 255, "%s%s", PATHPREFIX, PATHPREFIXTORDATA);
  char hiddendir[256];
  snprintf(hiddendir, 255, "%s%s", PATHPREFIX, PATHPREFIXTORHIDDEN);
  char socksport[256];
  snprintf(socksport, 255, "%d", TORSOCKSPORT);

  fp = fopen(torrc, "w");
  if (fp == NULL) {
    printf("error no write access %s", torrc);
    FINISHED = true;
    pthread_exit(NULL);
  }
  fputs("\n", fp);
  //fputs("Log warn syslog\n", fp);
  fputs("Log warn stdout\n", fp);
  fputs("SafeLogging 0\n", fp);
  fputs("\n", fp);
  fprintf(fp, "HiddenServiceDir %s\n", hiddendir);
  fputs("HiddenServiceVersion 3\n", fp);
  printf("TODO : set all ranges of client/server ports");
  fprintf(fp, "HiddenServicePort %d 127.0.0.1:%d\n", TORCLIENTPORT, TORSERVERPORT);
  fputs("\n", fp);
  fclose(fp);

  const int argc = 24;
  const char *argv[argc];
  i = 0;
  argv[i++] = (char*)("tor");
  argv[i++] = (char*)("--RunAsDaemon");  argv[i++] = (char*)("0");
  argv[i++] = (char*)("-f");  argv[i++] = strdup(torrc);
  argv[i++] = (char*)("--defaults-torrc");  argv[i++] = strdup(defaultstorrc);
  argv[i++] = (char*)("--ignore-missing-torrc");
  argv[i++] = (char*)("--SyslogIdentityTag");  argv[i++] = (char*)("YYY");
  argv[i++] = (char*)("--CacheDirectory");  argv[i++] = strdup(cachedir);
  argv[i++] = (char*)("--DataDirectory");  argv[i++] = strdup(datadir);
  argv[i++] = (char*)("--ControlSocket");  argv[i++] = PATHCONTROLSOCKET;
  argv[i++] = (char*)("--CookieAuthentication");  argv[i++] = (char*)("0");
  argv[i++] = (char*)("--SOCKSPort");  argv[i++] = strdup(socksport);
  argv[i++] = (char*)("--LogMessageDomains");  argv[i++] = (char*)("1");
  argv[i++] = (char*)("--TruncateLogFile");  argv[i++] = (char*)("1");

  int l = 0;
  char o[4096];
  for (i = 0 ; i < argc ; i++)
    l += snprintf(o + l, 256, "%s ", argv[i]);
  printf("launching tor with command line\n%s", o);

#ifdef __clang__
  tor_main_configuration_t *tor_config = NULL;
  tor_config = tor_main_configuration_new();
  if (tor_config == NULL) {
    printf("Allocating and creating a new configuration structure failed.");
    FINISHED = true;
    pthread_exit(NULL);
  }
  rc = tor_main_configuration_set_command_line(tor_config, argc, (char **)(argv));
  if (rc != 0) {
    printf("Setting tor command line failed.");
    FINISHED = true;
    unset_owning_controller_socket(tor_config);
    tor_main_configuration_free(tor_config);
    pthread_exit(NULL);
  }
  rc = tor_run_main(tor_config);
  if (rc != 0) { // blocking call
    printf("Tor could not start.");
    FINISHED = true;
  }
  FINISHED = true;
  unset_owning_controller_socket(tor_config);
  tor_main_configuration_free(tor_config);
#else
  int status = system(o);
  if (status != 0) {
    printf("Tor could not start %d", status);
    FINISHED = true;
  }
  FINISHED = true;
  printf("tor exited, return code %d", status);
#endif

  pthread_exit(NULL);
}

#ifdef __clang__
const char *getProviderVersion() {
  return tor_api_get_provider_version();
}
void torFreeAll() {
  tor_free_all(0);
}
#endif

int communicateSignal(int sig) {
  int rc = 0;
  /*if (PATHCONTROLSOCKET == NULL) {
    printf("error controlsocket path not set");
    return -1;
  }*/

  int fd = socket(AF_UNIX, SOCK_STREAM, 0);
  struct sockaddr_un addr;
  memset(&addr, 0, sizeof(addr));
  addr.sun_family = AF_UNIX;
  strncpy(addr.sun_path, PATHCONTROLSOCKET, sizeof(addr.sun_path) - 1);

  if (fd < 0 || connect(fd, (struct sockaddr *) &addr, sizeof(addr)) == -1) {
    char buf[1024];
    snprintf(buf, 1023, "%s open: %s", PATHCONTROLSOCKET, strerror(errno));
    return -1;
  }

  char socksrequest[1024];
  snprintf(socksrequest, 1023, "AUTHENTICATE \r\n");
  int socksrequestl = strlen(socksrequest);
  rc = send(fd, (char *) socksrequest, socksrequestl, MSG_NOSIGNAL);
  if (rc != socksrequestl) {
    printf("  [t]  ERROR TOR authenticate send %d", rc);
    close(fd);
    return -2;
  }
  char socksresponse[8192];
  rc = recv(fd, socksresponse, 8192, 0);
  if (rc < 0) {
    printf("  [c]  ERROR ERROR TOR authenticate recv rc %d errno %d", rc, errno);
    close(fd);
    return -3;
  }
  //dumpbuf("authenticate", socksresponse, rc);

  const char *socksrequest2;
  if (sig == signal_getinfo_version)
    socksrequest2 = "GETINFO version\r\n";
  else if (sig == signal_getinfo_circuitestablished)
    socksrequest2 = "GETINFO status/circuit-established\r\n";
  else if (sig == signal_getinfo_circuitstatus)
    socksrequest2 = "GETINFO circuit-status\r\n";
  else if (sig == signal_shutdown)
    socksrequest2 = "SIGNAL SHUTDOWN\r\n";
  int socksrequest2l = strlen(socksrequest2);
  rc = send(fd, (char *) socksrequest2, socksrequest2l, MSG_NOSIGNAL);
  if (rc != socksrequest2l) {
    printf("  [t]  ERROR TOR signal send %d", rc);
    close(fd);
    return -4;
  }
  unsigned char socksresponse2[8192];
  rc = recv(fd, socksresponse2, 8192, 0);
  if (rc < 0) {
    printf("  [c]  ERROR TOR signal recv rc %d errno %d", rc, errno);
    close(fd);
    return -5;
  }
  socksresponse2[8191] = 0;
  //dumpbuf("signal", socksresponse2, rc);

  close(fd);

  rc = checksignal(sig, socksresponse2, rc);

  return rc;
}

int checksignal(int sig, unsigned char *r, int rl) {
  int rc = -1;
  int i;

/*

https://torproject.gitlab.io/torspec/control-spec/#getinfo

authenticate(8)<250 OK\x0d\x0a>
               <512 Bad arguments to SIGNAL: Cannot accept more than 1 argument(s)

Communicate(controlsocket, "SIGNAL SHUTDOWN");

 GETINFO
  net/listeners/socks
  net/listeners/httptunnel

Communicate(controlsocket, "GETINFO version");
signal(62)<250-version=0.4.9.0-alpha-dev (git-34b53ed8df1c315a)\x0d\x0a250 OK\x0d\x0a>

Communicate(controlsocket, "GETINFO status/reachability-succeeded/dir");
signal(49)<250-status/reachability-succeeded/dir=1\x0d\x0a250 OK\x0d\x0a>

Communicate(controlsocket, "GETINFO status/circuit-established");
signal(42)<250-status/circuit-established=0\x0d\x0a250 OK\x0d\x0a>
signal(42)<250-status/circuit-established=1\x0d\x0a250 OK\x0d\x0a>

Communicate(controlsocket, "GETINFO circuit-status");
signal  (29)<250-circuit-status=\x0d\x0a250 OK\x0d\x0a>
signal (816)<250+circuit-status=\x0d\x0a1 EXTENDED BUILD_FLAGS=NEED_CAPACITY,NEED_UPTIME PURPOSE
signal(1178)<250+circuit-status=\x0d\x0a1 EXTENDED $DB6...
signal(4030)<250+circuit-status=\x0d\x0a1 BUILT $DB6...

*/

  if (sig == signal_getinfo_version) {
    if (foundstring((unsigned char *) r, 0 , "250-version="))
      rc = 0;
  } else if (sig == signal_getinfo_circuitestablished) {
    if (foundstring((unsigned char *) r, 0 , "250-status/circuit-established=0")) {
      printf("established = 0");
      rc = 1;
    } else if (foundstring((unsigned char *) r, 0 , "250-status/circuit-established=1"))
      rc = 0;
    else
      printf("established = ?");
  } else if (sig == signal_getinfo_circuitstatus) {
    if (foundstring((unsigned char *) r, 0, "250+circuit-status=\r\n1 EXTENDED ")) {
      printf("250-circuit-status = EXTENDED");
      rc = 1;
    } else if (foundstring((unsigned char *) r, 0 , "250+circuit-status=\r\n1 BUILT "))
      rc = 0;
    else if (foundstring((unsigned char *) r, 0 , "250-circuit-status="))
      rc = 1;
    else
      printf("250-circuit-status = ?");
  } else if (sig == signal_shutdown) {
    rc = 0;
  }

  return rc;
}

int torisconnected() {
  int rc;

  rc = communicateSignal(signal_getinfo_version);
  if (rc < 0) {
    printf("error couldn't get version");
    return -1;
  }

  rc = communicateSignal(signal_getinfo_circuitestablished);
  if (rc < 0) {
    printf("error circuit not established");
    return -2;
  } else if (rc == 1) {
    printf("waiting for established circuit");
    return 2;
  }

  rc = communicateSignal(signal_getinfo_circuitstatus);
  if (rc < 0) {
    printf("error circuit wrong status");
    return -3;
  } else if (rc == 1) {
    printf("waiting for built circuit");
    return 3;
  }

  return rc;
}



































