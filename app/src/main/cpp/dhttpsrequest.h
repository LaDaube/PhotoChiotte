

struct memory {
    char *response;
    size_t size;
};

static size_t cb(void *data, size_t size, size_t nmemb, void *clientp) {
    size_t realsize = size * nmemb;
    struct memory *mem = (struct memory *)clientp;

    char *ptr = (char *)(realloc(mem->response, mem->size + realsize + 1));
    if(ptr == NULL)
        return 0;  /* out of memory! */

    mem->response = ptr;
    memcpy(&(mem->response[mem->size]), data, realsize);
    mem->size += realsize;
    mem->response[mem->size] = 0;

    return realsize;
}

int extractpubkeys(unsigned char *pub, int *publ, int len, char *data) {
  int res = -1;

  int publmax = *publ;
  *publ = 0;
  unsigned char m[] = "0123456789abcdef";
  int p = 0;
  int j = 0;
  int pos, k;
  int i = 0;
  while (i < len) {
    if (*publ > publmax)
      break;
    if (
        (data[i] == 'b' && p == 0)
        || (data[i] == 's' && p == 1)
        || (data[i] == 't' && p == 2)
        || (data[i] == 'r' && p == 3)
        || (data[i] == 'a' && p == 4)
        || (data[i] == 'p' && p == 5)
        ) {
      p++;
    } else if (data[i] == '\n') {
      p = 0;
    } else if (p >= 6) {
      p++;
      pos = -1;
      for (k = 0; k < 16; k++) {
        if (m[k] == data[i]) {
          pos = k;
          break;
        }
      }
      if (pos == -1) {
        p = 0;
      } else {
        j = (p - 6 - 1) / 2;
        if (p % 2 == 1) {
          pub[*publ + j] = k << 4;
        } else {
          pub[*publ + j] += k;
        }
      }
      if (p == 6 + (HASHL + 2 + 2) * 2) {
        *publ += HASHL + 2 + 2;
        p = 0;
      }
    }
    i++;
  }
  printf("publ got %d bytes, %d address(es)", *publ, (int) (*publ/(HASHL + 2 + 2)));

  return res;
}

int getBootstrapNodes(unsigned char *pub, int *publ) {
    struct memory chunk = {0};
    CURLcode res = CURLE_CHUNK_FAILED;
    struct curl_slist *list = NULL;

    CURL *curl = curl_easy_init();
    if (curl && BOOTSTRAPS != NULL) {

        curl_easy_setopt(curl, CURLOPT_NOPROGRESS, 1L);

        //curl_easy_setopt(curl, CURLOPT_URL, "https://codeberg.org/LaDaube/PhotoChiotte/raw/branch/master/torbootstrap.txt");
        //curl_easy_setopt(curl, CURLOPT_URL, "http://tor/torbootstrap.txt");
        //curl_easy_setopt(curl, CURLOPT_URL, "https://gitlab.com/LaDaubePhotoChiotte/photochiotte/-/raw/master/torbootstrap.txt");
        curl_easy_setopt(curl, CURLOPT_URL, BOOTSTRAPS);
        curl_easy_setopt(curl, CURLOPT_PROXY, "socks5h://127.0.0.1:9053");
        curl_easy_setopt(curl, CURLOPT_PROXYTYPE, CURLPROXY_SOCKS5_HOSTNAME);
        curl_easy_setopt(curl, CURLOPT_PROXYPORT, TORSOCKSPORT);

        //list = curl_slist_append(list, "Host: tor");
        list = curl_slist_append(list, "Host: codeberg.org");
        //list = curl_slist_append(list, "Host: gitlab.com");
        list = curl_slist_append(list, "User-Agent: Mozilla/5.0 (Android 10; Mobile; rv:109.0) Gecko/109.0 Firefox/109.0");
        list = curl_slist_append(list, "Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,*/*;q=0.8");
        list = curl_slist_append(list, "Accept-Language: en-US,en;q=0.5");
        list = curl_slist_append(list, "Accept-Encoding: identity");
        //list = curl_slist_append(list, "Accept-Encoding: gzip, deflate, br");
        list = curl_slist_append(list, "Connection: keep-alive");
        list = curl_slist_append(list, "Upgrade-Insecure-Requests: 1");
        list = curl_slist_append(list, "Sec-Fetch-Dest: document");
        list = curl_slist_append(list, "Sec-Fetch-Mode: navigate");
        list = curl_slist_append(list, "Sec-Fetch-Site: none");
        list = curl_slist_append(list, "Sec-Fetch-User: ?1");
        curl_easy_setopt(curl, CURLOPT_HTTPHEADER, list);

        curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 0L);

        curl_easy_setopt(curl, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_2_0);

        curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, cb);
        curl_easy_setopt(curl, CURLOPT_WRITEDATA, (void *)&chunk);

        curl_easy_setopt(curl, CURLOPT_USE_SSL, CURLUSESSL_TRY);
        curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, 0L);
        curl_easy_setopt(curl, CURLOPT_SSL_VERIFYHOST, 0L);

        res = curl_easy_perform(curl);
        if (res != CURLE_OK) {
            printf("curl_easy_perform() failed: %d %s port %d", res, curl_easy_strerror(res), TORSOCKSPORT);
            *publ = 0;
        } else {

            int len = chunk.size;
            printf("(len=%d)\n%s", len, chunk.response);

            extractpubkeys(pub, publ, len, chunk.response);

        }

        free(chunk.response);

        curl_easy_cleanup(curl);
    }

    return res;
}
