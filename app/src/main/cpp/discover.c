#include "discover.h"
#include "dbencoder.h"
#include "ded25519.h"
#include "dthreads.h"
#include "dhttpsrequest.h"
#include "ddht.h"
#include "dcollection.h"
#include "dserver.h"
#include "dclient.h"
#include "dtor.h"


int launchserver(int serveurporti, int serveurportf, int clientporti, int clientportf) {
  FILE *fp;
  char fname[256];
  int i, j;
  int r;
  int rc;
  srand(arc4random());
  FINISHED = false;

  rc = verifyhiddendir();
  printf("hidden dir check 1 status %d", rc);
  if (rc != 0) {
    rc = verifyhiddendir();
    if (rc != 0) {
      printf("hidden dir had to be generated so the tor program needs to be restarted");
      return -1;
    } else {
      printf("hidden dir check 2 status %d", rc);
    }
  }

  printf("asked to exit");
  return -1;

  snprintf(PATHCONTROLSOCKET, 255, "%s%sControlSocket", PATHPREFIX, PATHPREFIXTORDATA);

  pthread_t ptid = 0;
  pthread_attr_t attrt;
  void *statust;
  pthread_attr_init(&attrt);
  pthread_attr_setdetachstate(&attrt, PTHREAD_CREATE_JOINABLE);
  rc = pthread_create(&ptid, &attrt, &runTor, NULL);
  if (rc) {
      printf("unable to create thread tor");
      FINISHED = true;
  }

  sleep(2);

  if (FINISHED) {
    printf("tor already exited");
    return -1;
  }
  while (torisconnected() != 0 && !FINISHED) {
    printf("waiting for tor to connect first");
    sleep(2);
  }
  if (FINISHED) {
    printf("tor already exited");
    return -1;
  }

  if (rangesl <= 2) {
      printf("reinitialize ranges 0x000000..... - 0xfffffff...");
      for (i = rangesl * HASHL + 0; i < rangesl * HASHL + HASHL; i++)
          ranges[i] = 0x00;
      rangesl++;
      for (i = rangesl * HASHL + 0; i < rangesl * HASHL + HASHL; i++)
          ranges[i] = 0xff;
      rangesl++;
  }

  // check if we are in the table
  if (OURNODE == -1) {
      printf("add ourself to the clientinfo");
      struct NodeInfo serveur;
      snprintf(fname, sizeof(fname), "%s%s%s", PATHPREFIX, PATHPREFIXTORHIDDEN, "public.raw");
      fp = fopen(fname, "rb");
      if (fp != NULL) {
          r = fread(serveur.pub, 1, serveur.publ, fp);
          fclose(fp);
          printf("public.raw read %d", r);
          serveur.publ = r;
      }
      dumpbufx("pub", serveur.pub, serveur.publ);
      serveur.thisisus = true;
      serveur.porti = clientporti;
      serveur.portf = clientportf;
      OURNODE = find_client(serveur.pub, serveur.publ);
      if (OURNODE == -1) {
          OURNODE = add_client(serveur);
      }
      if (OURNODE == -1) {
          printf("error we coundn't add ourself to the table");
      } else {
          for (i = 0 ; i < HASHL ; i++) {
              OURPUBKEY[i] = serveur.pub[i];
          }
      }
  }

  /**
   *   server threads
   */
  int serverl = serveurportf - serveurporti + 1;
  server_fd = (int *) malloc(sizeof(int) * serverl);
  struct ThreadServerPortData td[serverl];
  pthread_t thread[serverl];
  pthread_attr_t attr;
  void *status;
  // Initialize and set thread joinable
  pthread_attr_init(&attr);
  pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE);
  for (i = 0; i < serverl; i++) {
      server_fd[i] = -1;
      td[i].id = i;
      td[i].port = serveurporti + i;
      printf("create server on port %d", td[i].port);
      rc = pthread_create(&thread[i], &attr, thread_server_port, (void *) &td[i]);
      if (rc) {
          printf("unable to create thread %d", (serveurporti + i));
      }
  }
  /**
   *   client threads
   */
  struct ThreadClientData tdc[THREADCLIENTL];
  pthread_t threadc[THREADCLIENTL];
  pthread_attr_t attrc;
  void *statusc;
  pthread_attr_init(&attrc);
  pthread_attr_setdetachstate(&attrc, PTHREAD_CREATE_JOINABLE);
  for (j = 0; j < THREADCLIENTL; j++) {
      rc = pthread_create(&threadc[j], &attrc, thread_client, (void *) &tdc[j]);
      if (rc) {
          printf("unable to create thread %d", j);
      }
  }

  sleep(2);

  /**
   *   main loop
   */
  while (!FINISHED) {

      sleep(3);
      if (clientsl <= 1)
          check_repopulate_with_bootstrap();

      bool onethreadisfree = false;
      for (i = 0 ; i < THREADCLIENTL ; i++) {
          if (COMMAND[i].status == status_isfree) {
              onethreadisfree = true;
              break;
          }
      }

      if (onethreadisfree) {
          if (TASK_SEARCH_KEYWORDS == 1) {
              // top priority we are looking for a specific collection
              printf(" : SEARCH_KEYWORDS : search keywords %s %s", DOSSIERDESSIN, SEARCH_KEYWORDS);
              rc = get_peers(SEARCH_KEYWORDS_HASH);
              if (rc > 0) {

              } else if (rc == 0) {
                  printf(" : SEARCH_KEYWORDS : we already asked all clients not so long ago");
              } else {
                  printf(" : SEARCH_KEYWORDS : there is no client in our table");
              }

              if (rc == 0) {
                  rc = download(SEARCH_KEYWORDS_HASH);
                  if (rc > 0) {

                  } else if (rc == 0) {
                      printf(" : download : we have no peer for this hash");
                  } else {
                      printf(" : download : there is no corresponding hash");
                  }
              }

          } else {

              // high priority always be on our spot on the dht
              rc = find_node();
              if (rc > 0) {

              } else if (rc == 0) {
                  printf(" : find_node : we already asked all clients not so long ago");
              } else {
                  printf(" : find_node : there is no client in our table");
              }

              if (rc == 0) {

                  // check whether we have source collections to publish
                  time_t currtime = time(NULL);
                  if (currtime - LASTTIME_RE_COLLECTION_CREATE_FROM_SOURCE > DT_RE_COLLECTION_CREATE_FROM_SOURCE) {
                      LASTTIME_RE_COLLECTION_CREATE_FROM_SOURCE = currtime;
                      printf(" : create_psel_from_psels : we found our position and we're idle : search for sourcepublicselection and convert to publicselection");
                      create_pslp_from_psl("/mnt/sda1");
                  }

                  // announce our public collections on the dht
                  pthread_mutex_lock(&infohashmutex);
                  for (i = 0; i < hashesl; i++) {
                      if (hashinfo[i].filenamel > 0) { // we subscribed to this collection
                          time_t currenttime = time(NULL);
                          if (currenttime - hashinfo[i].lastannouncedkeywords > DT_RE_COLLECTION_ANNOUNCE) {
                              printf(" : get_peers : find closest nodes to our keywordshash <%s>", hashinfo[i].keywords);
                              rc = get_peers(hashinfo[i].keywordshash);
                              if (rc > 0) {

                              } else if (rc == 0) {
                                  printf(" : get_peers : we already asked all clients not so long ago");
                              } else {
                                  printf(" : get_peers : there is no client in our table");
                              }

                              if (rc == 0) {
                                  printf(" : announce_peer : announce we have a publicselection that corresponds to keywordshash <%s> search : <ournodeid %s>", hashinfo[i].keywords, hashinfo[i].keywords);

                                  rc = announce_peer(hashinfo[i].keywordshash);
                                  if (rc > 0) {

                                  } else if (rc == 0) {
                                      printf(" : announce_peer : we already announced it to all clients not so long ago");
                                  } else {
                                      printf(" : announce_peer : there is no client in our table");
                                  }
                                  if (rc == 0) {
                                      hashinfo[i].lastannouncedkeywords = currenttime;
                                      TIME_LAST_RESET_SEARCH = time(NULL);
                                      printf(" : announce_peer : announcing finished for %s %s", hashinfo[i].filename, hashinfo[i].keywords);
                                  }

                              }
                              break;

                          //} else if (currenttime - hashinfo[i].lastannouncedpubkeywords > DTANNOUNCECOLLECTION) {
                          //    printf("get_peers : find closest nodes to our pubkeykeywordshash <ournodeid crap crap>");
                          //    printf("announce_peer : announce we have a publicselection that corresponds to pubkeykeywordshash <ournodeid crap crap> search : publicselection");
                          } else {
                              printf("we already announced this collection %lds ago %s", currenttime - hashinfo[i].lastannouncedkeywords, hashinfo[i].filename);
                              //when a client asks our server for the keywordshash <> we send them all the pubkeykeywordshash that correspond
                              //when a client asks our server for the pubkeykeywordshash <ournodeid > we send them our publicselection file read on disk if download is asked in the metadata
                          }
                      }
                  }
                  pthread_mutex_unlock(&infohashmutex);

              }


          }
      }

  }

  /**
   *   exiting, close threads
   */
  printf("exiting... closing threads");

  communicateSignal(signal_shutdown);

  for (i = 0; i < serverl; i++) {
      if (server_fd[i] == -1) {
          printf("error server wasn't initialized");
      } else {
          shutdown(server_fd[i], SHUT_RD);
          printf("shutdown server %d", server_fd[i]);
      }
  }

  printf("free attribute and wait for the other client threads");
  pthread_attr_destroy(&attrc);
  for (j = 0; j < THREADCLIENTL; j++) {
      rc = pthread_join(threadc[j], &statusc);
      if (rc) {
          printf("unable to join client thread %d", j);
      }
      printf("exiting client thread %d with status %ld", j, (long) ((int *) statusc));
  }
  printf("free attribute and wait for the other server threads");
  pthread_attr_destroy(&attr);
  for (i = 0; i < serverl; i++) {
      rc = pthread_join(thread[i], &status);
      if (rc) {
          printf("unable to join server thread %d", (serveurporti + i));
      }
      printf("exiting server thread %d with status %ld", (serveurporti + i), (long) ((int *) status));
  }

  rc = pthread_join(ptid, &statust);
  if (rc) {
      printf("unable to join tor thread");
  }
  printf("exiting tor thread with status %ld", (long) ((int *) statust));

  sleep(1);
  free((int *) server_fd);
  printf("main program exiting");
  sleep(1);
  return 0;
}

void* survey_folder_thread(void* arg) {
    int i;
    // detach the current thread from the calling thread
    pthread_detach(pthread_self());
    FILE *op;
    char oname[256];
    snprintf(oname, sizeof(oname), "s");
    while (!FINISHED) {
        sleep(5);

        if (TASK_SEARCH_KEYWORDS == 0) {
            op = fopen(oname, "rb");
            if (op != NULL) {
                snprintf(SEARCH_KEYWORDS, 256, "crap crap");
                int searchkeywordsl = strlen(SEARCH_KEYWORDS);
                unsigned int hashl = 0;
                stringtohash((const unsigned char *) SEARCH_KEYWORDS,
                             searchkeywordsl,
                             SEARCH_KEYWORDS_HASH, &hashl);
                printf("hashl %d %s", (int) strlen(SEARCH_KEYWORDS), SEARCH_KEYWORDS);
                dumpbufx("WILL SEARCH FOR ", SEARCH_KEYWORDS_HASH, hashl);
                snprintf(DOSSIERDESSIN, 256, "/mnt/sda1");
                printf("hashl %d %s", (int) strlen(DOSSIERDESSIN), DOSSIERDESSIN);
                fclose(op);

                TIME_LAST_RESET_SEARCH = time(NULL);
                TASK_SEARCH_KEYWORDS = 1;
            }
        }

    }
    pthread_exit(NULL);
}

#ifdef __clang__

JNIEXPORT jint JNICALL Java_la_daube_photochiotte_ThreadDiscover_GetDiscoverStats(JNIEnv* env, jclass thisClass, jobject data) {
  unsigned char *status = (unsigned char *) (*env)->GetDirectBufferAddress(env, data);
  //printf("ping E:%d %d find E:%d %d", STATUS_PING_OURSELF_FAILED, STATUS_PING_OURSELF_SUCCESS, STATUS_FIND_OUR_POSITION_FAILED, STATUS_FIND_OUR_POSITION_SUCCESS);
  jint l = 0;
  status[l++] = (unsigned char) ((OURPUBKEY[0]) & 0xff);
  status[l++] = (unsigned char) ((OURPUBKEY[1]) & 0xff);
  status[l++] = (unsigned char) ((OURPUBKEY[2]) & 0xff);
  status[l++] = (unsigned char) ((OURPUBKEY[3]) & 0xff);
  status[l++] = (unsigned char) ((OURPUBKEY[4]) & 0xff);
  status[l++] = (unsigned char) ((OURPUBKEY[5]) & 0xff);
  status[l++] = (unsigned char) ((STATUS_GET_PEERS_FAILED >> 24) & 0xff);
  status[l++] = (unsigned char) ((STATUS_GET_PEERS_FAILED >> 16) & 0xff);
  status[l++] = (unsigned char) ((STATUS_GET_PEERS_FAILED >> 8) & 0xff);
  status[l++] = (unsigned char) ((STATUS_GET_PEERS_FAILED) & 0xff);
  status[l++] = (unsigned char) ((STATUS_GET_PEERS_SUCCESS >> 24) & 0xff);
  status[l++] = (unsigned char) ((STATUS_GET_PEERS_SUCCESS >> 16) & 0xff);
  status[l++] = (unsigned char) ((STATUS_GET_PEERS_SUCCESS >> 8) & 0xff);
  status[l++] = (unsigned char) ((STATUS_GET_PEERS_SUCCESS) & 0xff);
  status[l++] = (unsigned char) ((STATUS_FIND_OUR_POSITION_FAILED >> 24) & 0xff);
  status[l++] = (unsigned char) ((STATUS_FIND_OUR_POSITION_FAILED >> 16) & 0xff);
  status[l++] = (unsigned char) ((STATUS_FIND_OUR_POSITION_FAILED >> 8) & 0xff);
  status[l++] = (unsigned char) ((STATUS_FIND_OUR_POSITION_FAILED) & 0xff);
  status[l++] = (unsigned char) ((STATUS_FIND_OUR_POSITION_SUCCESS >> 24) & 0xff);
  status[l++] = (unsigned char) ((STATUS_FIND_OUR_POSITION_SUCCESS >> 16) & 0xff);
  status[l++] = (unsigned char) ((STATUS_FIND_OUR_POSITION_SUCCESS >> 8) & 0xff);
  status[l++] = (unsigned char) ((STATUS_FIND_OUR_POSITION_SUCCESS) & 0xff);
  status[l++] = (unsigned char) ((STATUS_ANNOUNCE_PEER_FAILED >> 24) & 0xff);
  status[l++] = (unsigned char) ((STATUS_ANNOUNCE_PEER_FAILED >> 16) & 0xff);
  status[l++] = (unsigned char) ((STATUS_ANNOUNCE_PEER_FAILED >> 8) & 0xff);
  status[l++] = (unsigned char) ((STATUS_ANNOUNCE_PEER_FAILED) & 0xff);
  status[l++] = (unsigned char) ((STATUS_ANNOUNCE_PEER_SUCCESS >> 24) & 0xff);
  status[l++] = (unsigned char) ((STATUS_ANNOUNCE_PEER_SUCCESS >> 16) & 0xff);
  status[l++] = (unsigned char) ((STATUS_ANNOUNCE_PEER_SUCCESS >> 8) & 0xff);
  status[l++] = (unsigned char) ((STATUS_ANNOUNCE_PEER_SUCCESS) & 0xff);
  status[l++] = (unsigned char) ((STATUS_DOWNLOAD_FAILED >> 24) & 0xff);
  status[l++] = (unsigned char) ((STATUS_DOWNLOAD_FAILED >> 16) & 0xff);
  status[l++] = (unsigned char) ((STATUS_DOWNLOAD_FAILED >> 8) & 0xff);
  status[l++] = (unsigned char) ((STATUS_DOWNLOAD_FAILED) & 0xff);
  status[l++] = (unsigned char) ((STATUS_DOWNLOAD_SUCCESS >> 24) & 0xff);
  status[l++] = (unsigned char) ((STATUS_DOWNLOAD_SUCCESS >> 16) & 0xff);
  status[l++] = (unsigned char) ((STATUS_DOWNLOAD_SUCCESS >> 8) & 0xff);
  status[l++] = (unsigned char) ((STATUS_DOWNLOAD_SUCCESS) & 0xff);
  status[l++] = (unsigned char) ((TASK_SEARCH_KEYWORDS >> 24) & 0xff);
  status[l++] = (unsigned char) ((TASK_SEARCH_KEYWORDS >> 16) & 0xff);
  status[l++] = (unsigned char) ((TASK_SEARCH_KEYWORDS >> 8) & 0xff);
  status[l++] = (unsigned char) ((TASK_SEARCH_KEYWORDS) & 0xff);
  status[l++] = (unsigned char) ((clientsl >> 24) & 0xff);
  status[l++] = (unsigned char) ((clientsl >> 16) & 0xff);
  status[l++] = (unsigned char) ((clientsl >> 8) & 0xff);
  status[l++] = (unsigned char) ((clientsl) & 0xff);
  status[l++] = (unsigned char) ((rangesl >> 24) & 0xff);
  status[l++] = (unsigned char) ((rangesl >> 16) & 0xff);
  status[l++] = (unsigned char) ((rangesl >> 8) & 0xff);
  status[l++] = (unsigned char) ((rangesl) & 0xff);
  status[l++] = (unsigned char) ((hashesl >> 24) & 0xff);
  status[l++] = (unsigned char) ((hashesl >> 16) & 0xff);
  status[l++] = (unsigned char) ((hashesl >> 8) & 0xff);
  status[l++] = (unsigned char) ((hashesl) & 0xff);
  return l;
}
JNIEXPORT jint JNICALL Java_la_daube_photochiotte_ThreadDiscover_SearchDiscover(JNIEnv* env, jclass thisClass, jstring jkeywords, jstring destinationfolder) {
    int i;
    jint rc = -1;

    const char *searchkeywords = (*env)->GetStringUTFChars(env, jkeywords, NULL);
    snprintf(SEARCH_KEYWORDS, 256, "%s", searchkeywords);
    int searchkeywordsl = strlen(SEARCH_KEYWORDS);
    unsigned int hashl = 0;
    stringtohash((const unsigned char *) searchkeywords, searchkeywordsl, SEARCH_KEYWORDS_HASH, &hashl);
    (*env)->ReleaseStringUTFChars(env, jkeywords, searchkeywords);

    const char *dossierdessin = (*env)->GetStringUTFChars(env, destinationfolder, NULL);
    snprintf(DOSSIERDESSIN, 256, "%s", dossierdessin);
    (*env)->ReleaseStringUTFChars(env, destinationfolder, dossierdessin);

    printf("search for %s %s", SEARCH_KEYWORDS, DOSSIERDESSIN);

    TIME_LAST_RESET_SEARCH = time(NULL);
    TASK_SEARCH_KEYWORDS = 1; // en dernier seulement

    return rc;
}
JNIEXPORT jint JNICALL Java_la_daube_photochiotte_ThreadDiscover_RetrievedSearchResults(JNIEnv* env, jclass thisClass) {
    TASK_SEARCH_KEYWORDS = 0;
    return 0;
}
JNIEXPORT jint JNICALL Java_la_daube_photochiotte_ThreadDiscover_CheckHiddenFolder(JNIEnv* env, jclass thisClass, jstring pathprefix) {

    PATHPREFIX = (*env)->GetStringUTFChars(env, pathprefix, NULL);
    int rc = verifyhiddendir();

    (*env)->ReleaseStringUTFChars(env, pathprefix, PATHPREFIX);
    return rc;
}
JNIEXPORT jint JNICALL Java_la_daube_photochiotte_ThreadDiscover_LaunchServer(JNIEnv* env, jclass thisClass, jstring pathprefix, jstring bootstraps, jint msocksport, jint serveurporti, jint serveurportf,  jint clientporti, jint clientportf) {
    jint rc = -1;

    TORSOCKSPORT = msocksport;
    BOOTSTRAPS = (*env)->GetStringUTFChars(env, bootstraps, NULL);
    PATHPREFIX = (*env)->GetStringUTFChars(env, pathprefix, NULL);

    rc = launchserver(serveurporti, serveurportf, clientporti, clientportf);

    (*env)->ReleaseStringUTFChars(env, pathprefix, PATHPREFIX);
    (*env)->ReleaseStringUTFChars(env, bootstraps, BOOTSTRAPS);

    return rc;
}
JNIEXPORT jint JNICALL Java_la_daube_photochiotte_ThreadDiscover_StopServer(JNIEnv* env, jclass thisClass) {

    FINISHED = true;
    return 0;
}

#else

int main(int argc, char **argv) {
    int rc = 0;

    //compresstest();
    //check_repopulate_with_bootstrap();
    //create_pslp_from_psl("/mnt/sda1");
    /*
    PATHPREFIX = argv[1];
    TORSOCKSPORT = atoi(argv[2]);
    printf("start");
    int bsl = 8192;
    unsigned char bs[bsl];
    getBootstrapNodes(bs, bsl);
    printf("end");
    */

    pthread_t ptid;
    pthread_create(&ptid, NULL, &survey_folder_thread, NULL);

    // path to the tor hidden_service directory
    PATHPREFIX = argv[1];
    // torrc socksport
    TORSOCKSPORT = atoi(argv[2]);
    // torrc listening parti-portf
    int serveurporti = atoi(argv[3]);
    int serveurportf = atoi(argv[4]);
    int clientporti = atoi(argv[5]);
    int clientportf = atoi(argv[6]);

    rc = launchserver(serveurporti, serveurportf, clientporti, clientportf);

    pthread_join(ptid, NULL);
    pthread_exit(NULL);


    return rc;
}

#endif





















