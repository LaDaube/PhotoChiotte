#include "thumb.h"

extern "C" {











void additionalMetadata(char *&addmetadata, int &l, const char *outputfilename) {
  FILE *f = fopen(outputfilename, "rb");
  if (f) {
    long fl = -1;
    int found = 0;
    fseek(f, 0L, SEEK_END);
    fl = ftell(f);
    if (fl > 0) {
      fseek(f, 0L, SEEK_SET);
      unsigned char buf[fl];
      int r = fread(buf, 1, fl, f);
      if (r > 0 && r == fl) {

        for (int i = fl - 1 ; i > 0 ; i--) { // ff d9
          if ((buf[i-1] & 0xff) == 0xff && (buf[i] & 0xff) == 0xd9) {
            found = i + 1;
            break;
          }
        }
        if (found > 0 && found < fl - 3) {
          addmetadata = (char*) malloc(65536);
          l = 0;

          char k[256];
          char v[256];
          int kp = 0;
          int vp = 0;
          int i = found;
          char c;
          int vi = 0;
          int vf = 0;
          bool iskey = true;
          while (i < fl) {
            c = (char) (buf[i] & 0xff); // nécessaire sinon bar est signé
            if (c == (char) '\n' || i == fl - 1) {
              if (c != '\n') {
                v[vp++] = c;
                vf = i + 1;
              } else {
                vf = i;
              }
              if (kp > 0) {
                k[kp++] = 0;
                v[vp++] = 0;
                if (strcmp(k, "printname") == 0) {
                  l += snprintf(addmetadata + l, sizeof("printName=") + vp + 1, "printName=%s\n", v);
                  //ALOGV("new key %s = %s : <%s>", k, v, addmetadata);
                } else if (strcmp(k, "printdetails") == 0) {
                  l += snprintf(addmetadata + l, sizeof("printDetails=") + vp + 1, "printDetails=%s\n", v);
                } else if (strcmp(k, "printfooter") == 0) {
                  l += snprintf(addmetadata + l, sizeof("printFooter=") + vp + 1, "printFooter=%s\n", v);
                } else if (strcmp(k, "playinsequence") == 0) {
                  l += snprintf(addmetadata + l, sizeof("playInSequence=") + vp + 1, "playInSequence=%s\n", v);
                } else if (strcmp(k, "playstartatposition") == 0) {
                  l += snprintf(addmetadata + l, sizeof("playStartAtPosition=") + vp + 1, "playStartAtPosition=%s\n", v);
                } else if (strcmp(k, "subtitleaddress") == 0) {
                  l += snprintf(addmetadata + l, sizeof("subtitleAddress=") + vp + 1, "subtitleAddress=%s\n", v);
                }
              }
              iskey = true;
              kp = 0;
              vp = 0;
            } else if (c == (char) '=') {
              if (iskey) {
                iskey = false;
                vi = i + 1;
                vf = i + 1;
              } else {
                v[vp++] = c;
              }
            } else {
              if (iskey)
                k[kp++] = tolower(c);
              else
                v[vp++] = c;
            }
            i++;
          }


        }

      }
    }
    fclose(f);
    //if (l > 0) ALOGV("%s additionalMetadata found at %d / %ld : %s", outputfilename, found, fl, addmetadata);
  }

}

static void ppm_save(unsigned char *buf, int wrap, int xsize, int ysize, const char *filename) {
    FILE *f;
    int i, j;
    f = fopen(filename,"wb");
    if (!f) {
        ALOGV("Error could not write %s", filename);
        return;
    }
    fprintf(f, "P6\n%d %d\n%d\n", xsize, ysize, 255);
    for (i = 0; i < ysize; i++)
        fwrite(buf + i * wrap, 1, xsize*3, f);
        //fwrite(buf + i * wrap, 1, xsize, f);
    fclose(f);
    ALOGV("wrote %s", filename);
}

static char * get_error_text(const int error) {
  static char error_buffer[255];
  av_strerror(error, error_buffer, sizeof(error_buffer));
  return error_buffer;
}

static int my_encode(AVCodecContext *output_codec_context, AVPacket *output_packet, AVFrame *frame,
                     int *data_present) {
    int ret;
    ret = avcodec_send_frame(output_codec_context, frame);
    if (ret < 0) {
        ALOGV("Error sending a frame for encoding %d\n", ret);
        return ret;
    }
    *data_present = 0;
    ret = 1;
    while (ret > 0) {
        ret = avcodec_receive_packet(output_codec_context, output_packet);
        if (ret == AVERROR(EAGAIN))
            ALOGV("Error during encoding no picture in packet %d\n", ret);
        else if (ret == AVERROR_EOF)
            ALOGV("Error during encoding EOF %d\n", ret);
        else if (ret < 0)
            ALOGV("Error during encode: '%s'\n", get_error_text(ret));
        else {
            *data_present = 1;
        }
    }
    return 0;
}

static int resize_image(AVPixelFormat original_pixelformat,
                        AVCodecContext *resized_image_codec_context,
                        AVPacket *resized_image_cover_packet,
                        AVFrame *input_frame,
                        int resized_width, int resized_height, bool isapicture) {
    int ret = -1;
    int data_present;
    struct SwsContext *sws_c;

    AVFrame *output_frame = av_frame_alloc();
    if (output_frame == NULL) {
        ALOGV("Error av_frame_alloc %d\n", ret);
        return ret;
    }
    if (!resized_width)
        resized_width = input_frame->width;
    if (!resized_height)
        resized_height = input_frame->height;

    //ALOGV("input   : %4d %4d", input_frame->width, input_frame->height);
    //ALOGV("resized : %4d %4d", resized_width, resized_height);

    sws_c = sws_getContext(input_frame->width, input_frame->height,
                           original_pixelformat,
                           resized_width, resized_height,
                           AV_PIX_FMT_YUV420P, SWS_BICUBIC,
                           NULL, NULL, NULL);
    if (sws_c == NULL) {
        ALOGV("Error sws_getContext %d\n", ret);
        av_frame_unref(output_frame);
        av_frame_free(&output_frame);
        return ret;
    }

    ret = av_image_alloc(output_frame->data, output_frame->linesize, resized_width,
                   resized_height, AV_PIX_FMT_YUV420P, 32);
    if (ret < 0) {
        ALOGV("Error av_image_alloc %d\n", ret);
        sws_freeContext(sws_c);
        av_frame_unref(output_frame);
        av_frame_free(&output_frame);
        return ret;
    }

    output_frame->width = resized_width;
    output_frame->height = resized_height;
    output_frame->format = AV_PIX_FMT_YUV420P;
    ret = sws_scale(sws_c,
                    (const uint8_t * const *)&input_frame->data,
                    input_frame->linesize,
                    0,
                    input_frame->height,
                    output_frame->data,
                    output_frame->linesize);
    if (ret < 0) {
        ALOGV("Error sws_scale %d\n", ret);
        sws_freeContext(sws_c);
        av_frame_unref(output_frame);
        av_frame_free(&output_frame);
        return ret;
    }

    if (!isapicture) {
        int imin;
        int imax;
        char inter;
        char exter;
        if (resized_width == input_frame->width) {
            if (resized_width < resized_height) {
                imin = (int) ((float) resized_width * 0.10f);
                imax = (int) ((float) resized_width * 0.25f);
            } else {
                imin = (int) ((float) resized_height * 0.10f);
                imax = (int) ((float) resized_height * 0.25f);
            }
            //inter = 0xa0;
            //exter = 0x4f;
        } else {
            if (resized_width < (int) ((float) resized_height)) {
                imin = (int) ((float) resized_width * 0.10f);
                imax = (int) ((float) resized_width * 0.50f);
            } else {
                imin = (int) ((float) resized_height * 0.10f);
                imax = (int) ((float) resized_height * 0.50f);
            }
            //inter = 0xbf;
            //exter = 0x30;
        }
        int jmin = imin;
        int jmax = jmin + (imax - imin);
        int jmaxhalf = (int) ((float) (jmin + jmax) * 0.50f);
        int icurr = imin + 1;
        int iincrease = (int) (((float) (imax - imin) * 2.0f) / (float) ((jmax - jmin)));
        int iborder = iincrease * 4;
        int iminborder = (int) ((float) (imin + iborder * 0.5));
        int i, j, pos;
        uint8_t diff = 0x20;
        uint8_t moins = diff;
        uint8_t plus = 0xff - diff;
        for (j = jmin; j < jmax && j < resized_height; j++) {
            for (i = imin; i < icurr && i < imax && i < output_frame->linesize[0]; i++) {
                pos = output_frame->linesize[0] * j + i;
                if (iminborder < i && i < icurr - iborder) { // contour
                    if (output_frame->data[0][pos] < plus)
                        output_frame->data[0][pos] += diff;
                } else {                                     // intérieur
                    if (moins < output_frame->data[0][pos])
                        output_frame->data[0][pos] -= diff;
                }

            }
            if (j < jmaxhalf)
                icurr += iincrease;
            else
                icurr -= iincrease;
        }
    }

    ret = my_encode(resized_image_codec_context, resized_image_cover_packet, output_frame, &data_present);

    if (data_present == 0)
        ALOGV("Error sws_scale no data present %d\n", ret);

    sws_freeContext(sws_c);
    av_frame_unref(output_frame);
    av_frame_free(&output_frame);
    return ret;
}

static int create_rgb(AVCodecContext  *full_size_cover_codec_context, AVFrame  *input_frame,
                       int thumb_width, int thumb_height, bool isapicture) {
    int ret = -1;
    struct SwsContext *sws_c;

    AVFrame *output_frame = av_frame_alloc();
    if (output_frame == NULL) {
        ALOGV("Error av_frame_alloc %d\n", ret);
        return ret;
    }
    if (!thumb_width)
        thumb_width = input_frame->width;
    if (!thumb_height)
        thumb_height = input_frame->height;

    sws_c = sws_getContext(input_frame->width, input_frame->height,
                           full_size_cover_codec_context->pix_fmt,
                           thumb_width, thumb_height,
                           AV_PIX_FMT_BGRA, SWS_BICUBIC,
                           NULL, NULL, NULL);
    if (sws_c == NULL) {
        ALOGV("Error sws_getContext %d\n", ret);
        av_frame_unref(output_frame);
        av_frame_free(&output_frame);
        return ret;
    }

    ret = av_image_alloc(output_frame->data, output_frame->linesize, thumb_width,
                         thumb_height, AV_PIX_FMT_BGRA, 32);
    if (ret < 0) {
        ALOGV("Error av_image_alloc %d\n", ret);
        sws_freeContext(sws_c);
        av_frame_unref(output_frame);
        av_frame_free(&output_frame);
        return ret;
    }

    output_frame->width = thumb_width;
    output_frame->height = thumb_height;
    output_frame->format = AV_PIX_FMT_BGRA;
    ret = sws_scale(sws_c,
                    (const uint8_t * const *)&input_frame->data,
                    input_frame->linesize,
                    0,
                    input_frame->height,
                    output_frame->data,
                    output_frame->linesize);
    if (ret < 0) {
        ALOGV("Error sws_scale %d\n", ret);
        sws_freeContext(sws_c);
        av_frame_unref(output_frame);
        av_frame_free(&output_frame);
        return ret;
    }

    if (output_frame->linesize[0] > 0 && output_frame->width > 0 && output_frame->height > 0) {
        bitmapwidth = output_frame->width;
        bitmapheight = output_frame->height;
        bitmapstride = output_frame->linesize[0];
        int isize = (bitmapstride * bitmapheight) / 4;
        bitmapint = (int *) malloc(isize * sizeof(int));
        int *pt = (int *) output_frame->data[0];
        for (int k = 0 ; k < isize ; k++) {
            bitmapint[k] = pt[k];
        }
    } else {
        ALOGV("Error linesize %d\n", ret);
        ret = -1;
    }

    sws_freeContext(sws_c);
    av_frame_unref(output_frame);
    av_frame_free(&output_frame);
    return ret;
}

static int blur_rgb(AVCodecContext  *full_size_cover_codec_context, AVFrame  *input_frame,
                      int thumb_width, int thumb_height, int maxwidth, int maxheight, bool isapicture) {
    int ret = -1;
    struct SwsContext *sws_cs;

    AVFrame *output_frames = av_frame_alloc();
    if (output_frames == NULL) {
        ALOGV("Error av_frame_alloc %d\n", ret);
        return ret;
    }
    thumb_width = maxwidth * 0.01f;
    if (thumb_width < 5)
        thumb_width = 5;
    thumb_height = maxheight * 0.01f;
    if (thumb_height < 5)
        thumb_height = 5;
    sws_cs = sws_getContext(input_frame->width, input_frame->height,
                           full_size_cover_codec_context->pix_fmt,
                           thumb_width, thumb_height,
                           AV_PIX_FMT_BGRA, SWS_BICUBIC,
                           NULL, NULL, NULL);

    if (sws_cs == NULL) {
        ALOGV("Error sws_getContext %d\n", ret);
        av_frame_free(&output_frames);
        return ret;
    }
    ret = av_image_alloc(output_frames->data, output_frames->linesize, thumb_width,
                         thumb_height, AV_PIX_FMT_BGRA, 32);
    if (ret < 0) {
        ALOGV("Error av_image_alloc %d\n", ret);
        sws_freeContext(sws_cs);
        av_frame_unref(output_frames);
        av_frame_free(&output_frames);
        return ret;
    }
    output_frames->width = thumb_width;
    output_frames->height = thumb_height;
    output_frames->format = AV_PIX_FMT_BGRA;
    ret = sws_scale(sws_cs,
                    (const uint8_t * const *)&input_frame->data,
                    input_frame->linesize,
                    0,
                    input_frame->height,
                    output_frames->data,
                    output_frames->linesize);
    if (ret < 0) {
        ALOGV("%d sws_scale", ret);
        sws_freeContext(sws_cs);
        av_frame_unref(output_frames);
        av_frame_free(&output_frames);
        return ret;
    }





    struct SwsContext *sws_c;
    AVFrame *output_frame = av_frame_alloc();
    if (output_frame == NULL) {
        ALOGV("Error av_frame_alloc %d\n", ret);
        sws_freeContext(sws_cs);
        av_frame_unref(output_frames);
        av_frame_free(&output_frames);
        return ret;
    }
    thumb_width = maxwidth;
    thumb_height = maxheight;
    sws_c = sws_getContext(output_frames->width, output_frames->height,
                           AV_PIX_FMT_BGRA,
                           thumb_width, thumb_height,
                           AV_PIX_FMT_BGRA, SWS_BICUBIC,
                           NULL, NULL, NULL);

    if (sws_c == NULL) {
        ALOGV("Error sws_getContext %d\n", ret);
        sws_freeContext(sws_cs);
        av_frame_unref(output_frames);
        av_frame_free(&output_frames);
        av_frame_unref(output_frame);
        av_frame_free(&output_frame);
        return ret;
    }
    ret = av_image_alloc(output_frame->data, output_frame->linesize, thumb_width,
                         thumb_height, AV_PIX_FMT_BGRA, 32);
    if (ret < 0) {
        ALOGV("Error av_image_alloc %d\n", ret);
        sws_freeContext(sws_cs);
        sws_freeContext(sws_c);
        av_frame_unref(output_frames);
        av_frame_free(&output_frames);
        av_frame_unref(output_frame);
        av_frame_free(&output_frame);
        return ret;
    }
    output_frame->width = thumb_width;
    output_frame->height = thumb_height;
    output_frame->format = AV_PIX_FMT_BGRA;
    ret = sws_scale(sws_c,
                    (const uint8_t * const *)&output_frames->data,
                    output_frames->linesize,
                    0,
                    output_frames->height,
                    output_frame->data,
                    output_frame->linesize);
    if (ret < 0) {
        ALOGV("Error sws_scale %d\n", ret);
        sws_freeContext(sws_cs);
        sws_freeContext(sws_c);
        av_frame_unref(output_frames);
        av_frame_free(&output_frames);
        av_frame_unref(output_frame);
        av_frame_free(&output_frame);
        return ret;
    }





    if (output_frame->linesize[0] > 0 && output_frame->width > 0 && output_frame->height > 0) {
        bitmapwidth = output_frame->width;
        bitmapheight = output_frame->height;
        bitmapstride = output_frame->linesize[0];
        int isize = (bitmapstride * bitmapheight) / 4;
        bitmapint = (int *) malloc(isize * sizeof(int));
        int *pt = (int *) output_frame->data[0];
        for (int k = 0 ; k < isize ; k++) {
            bitmapint[k] = pt[k];
        }
    } else {
        ALOGV("Error linesize %d\n", ret);
        ret = -1;
    }

    sws_freeContext(sws_cs);
    av_frame_unref(output_frames);
    av_frame_free(&output_frames);
    sws_freeContext(sws_c);
    av_frame_unref(output_frame);
    av_frame_free(&output_frame);
    return ret;
}

static int filter(AVCodecContext  *full_size_cover_codec_context, AVFrame *input_frame,
                      bool isapicture, const char *filters_descr, AVFrame *&filt_frame,
                      bool createjpgfile) {
    int ret = -1;

    AVFilterGraph *filter_graph;
    AVFilterContext *buffersink_ctx;
    AVFilterContext *buffersrc_ctx;
    static int video_stream_index = -1;
    static int64_t last_pts = AV_NOPTS_VALUE;

    char args[512];
    const AVFilter *buffersrc  = avfilter_get_by_name("buffer");
    const AVFilter *buffersink = avfilter_get_by_name("buffersink");
    AVFilterInOut *outputs = avfilter_inout_alloc();
    AVFilterInOut *inputs  = avfilter_inout_alloc();
    enum AVPixelFormat pix_fmts[] = { AV_PIX_FMT_BGRA, AV_PIX_FMT_NONE };
    filter_graph = avfilter_graph_alloc();
    if (!outputs || !inputs || !filter_graph) {
        ALOGV("avfilter_graph_alloc %d\n", ret);
        return ret;
    }
    full_size_cover_codec_context->time_base.num = 1; // 0 wrong
    full_size_cover_codec_context->time_base.den = 1;
    snprintf(args, sizeof(args),
             "video_size=%dx%d:pix_fmt=%d:time_base=%d/%d:pixel_aspect=%d/%d",
             full_size_cover_codec_context->width, full_size_cover_codec_context->height,
             full_size_cover_codec_context->pix_fmt,
             full_size_cover_codec_context->time_base.num, full_size_cover_codec_context->time_base.den,
             full_size_cover_codec_context->sample_aspect_ratio.num, full_size_cover_codec_context->sample_aspect_ratio.den);
    ret = avfilter_graph_create_filter(&buffersrc_ctx, buffersrc, "in", args, NULL, filter_graph);
    if (ret < 0) {
        ALOGV("avfilter_graph_create_filter %d\n", ret);
        avfilter_graph_free(&filter_graph);
        return ret;
    }
    ret = avfilter_graph_create_filter(&buffersink_ctx, buffersink, "out",
                                       NULL, NULL, filter_graph);
    if (ret < 0) {
        ALOGV("avfilter_graph_create_filter %d\n", ret);
        avfilter_graph_free(&filter_graph);
        return ret;
    }
    ret = av_opt_set_int_list(buffersink_ctx, "pix_fmts", pix_fmts,
                              AV_PIX_FMT_NONE, AV_OPT_SEARCH_CHILDREN);
    if (ret < 0) {
        ALOGV("av_opt_set_int_list %d\n", ret);
        avfilter_graph_free(&filter_graph);
        return ret;
    }
    outputs->name       = av_strdup("in");
    outputs->filter_ctx = buffersrc_ctx;
    outputs->pad_idx    = 0;
    outputs->next       = NULL;
    inputs->name       = av_strdup("out");
    inputs->filter_ctx = buffersink_ctx;
    inputs->pad_idx    = 0;
    inputs->next       = NULL;
    ret = avfilter_graph_parse_ptr(filter_graph, filters_descr,
                                   &inputs, &outputs, NULL);
    if (ret < 0) {
        ALOGV("avfilter_graph_parse_ptr %d\n", ret);
        avfilter_graph_free(&filter_graph);
        return ret;
    }
    ret = avfilter_graph_config(filter_graph, NULL);
    if (ret < 0) {
        ALOGV("avfilter_graph_config %d\n", ret);
        avfilter_graph_free(&filter_graph);
        return ret;
    }
    avfilter_inout_free(&inputs);
    avfilter_inout_free(&outputs);

    ret = av_buffersrc_add_frame_flags(buffersrc_ctx, input_frame, AV_BUFFERSRC_FLAG_KEEP_REF);
    if (ret < 0) {
        ALOGV("av_buffersrc_add_frame_flags %d\n", ret);
        avfilter_graph_free(&filter_graph);
        return ret;
    }
    //AVFrame *filt_frame;
    filt_frame = av_frame_alloc();
    if (filt_frame == NULL) {
        ALOGV("Error av_frame_alloc %d\n", ret);
        avfilter_graph_free(&filter_graph);
        return ret;
    }
    ret = av_buffersink_get_frame(buffersink_ctx, filt_frame);
    if (ret < 0) {
        ALOGV("av_buffersink_get_frame %d\n", ret);
        avfilter_graph_free(&filter_graph);
        av_frame_unref(filt_frame);
        av_frame_free(&filt_frame);
        return ret;
    }

    if (filt_frame->linesize[0] > 0 && filt_frame->width > 0 && filt_frame->height > 0) {

      if (!createjpgfile) {
        bitmapwidth = filt_frame->width;
        bitmapheight = filt_frame->height;
        bitmapstride = filt_frame->linesize[0];
        int isize8 = bitmapstride * bitmapheight;
        int isize32max;
        bool iscrop = false;
        int feds = strlen(filters_descr) - 7;
        for (int k = 0; k < feds; k++) {
          if (filters_descr[k] == 'c' && filters_descr[k + 1] == 'r' &&
              filters_descr[k + 2] == 'o' && filters_descr[k + 3] == 'p' &&
              filters_descr[k + 4] == '=') {
            iscrop = true;
            break;
          }
        }
        if (iscrop) {
          isize32max = isize8 / 4 - bitmapstride / 4;
          bitmapheight -= 1;
          ALOGV("reduced %d to %d", isize8, isize32max * 4);
        } else
          isize32max = isize8 / 4;

        bitmapint = (int *) malloc(isize32max * 4);

        int *pt = ((int *) filt_frame->data[0]);
        for (int k = 0; k < isize32max; k++) {
          bitmapint[k] = pt[k];
        }
      }

      ret = 0;
    } else {
      ret = -1;
      ALOGV("Error linesize %d\n", ret);
      avfilter_graph_free(&filter_graph);
      av_frame_unref(filt_frame);
      av_frame_free(&filt_frame);
      return ret;
    }

    avfilter_graph_free(&filter_graph);
    //av_frame_unref(filt_frame);
    //av_frame_free(&filt_frame);
    return ret;
}

static int create_jpg(AVPixelFormat original_pixel_format, AVFrame  *input_frame,
                                   const char *destname, int thumb_width, int thumb_height, bool isapicture) {
    AVPacket         thumbnail_cover_packet;
    AVCodecContext  * thumbnail_cover_codec_context = NULL;
    const AVCodec * output_codec = NULL;
    int ret = -1;
    FILE * file;
    av_new_packet(&thumbnail_cover_packet, 0);

    output_codec = avcodec_find_encoder(AV_CODEC_ID_MJPEG);
    if (output_codec == NULL) {
        ALOGV("init_output_jpeg_image - error avcodec_find_encoder\n");
        return ret;
    }
    thumbnail_cover_codec_context = avcodec_alloc_context3(output_codec);
    if (thumbnail_cover_codec_context == NULL) {
        ALOGV("init_output_jpeg_image - error avcodec_alloc_context3\n");
        return ret;
    }
    thumbnail_cover_codec_context->codec_id = AV_CODEC_ID_MJPEG;
    thumbnail_cover_codec_context->codec_type = AVMEDIA_TYPE_VIDEO;
    thumbnail_cover_codec_context->pix_fmt = AV_PIX_FMT_YUV420P;
    thumbnail_cover_codec_context->width = thumb_width;
    thumbnail_cover_codec_context->height = thumb_height;
    thumbnail_cover_codec_context->time_base.num = 1;
    thumbnail_cover_codec_context->time_base.den = 1;
    thumbnail_cover_codec_context->strict_std_compliance = FF_COMPLIANCE_EXPERIMENTAL;
    thumbnail_cover_codec_context->qmin = 1;
    thumbnail_cover_codec_context->qmax = 1;
    thumbnail_cover_codec_context->flags |= AV_CODEC_FLAG_QSCALE;
    thumbnail_cover_codec_context->global_quality = thumbnail_cover_codec_context->qmin * FF_QP2LAMBDA;

    /*
    thumbnail_cover_codec_context->bit_rate = pCodecCtx->bit_rate;
    thumbnail_cover_codec_context->codec_id = AV_CODEC_ID_MJPEG;
    thumbnail_cover_codec_context->codec_type = AVMEDIA_TYPE_VIDEO;
    thumbnail_cover_codec_context->time_base.num = pCodecCtx->time_base.num;
    thumbnail_cover_codec_context->time_base.den = pCodecCtx->time_base.den;
     //  autre  thumbnail_cover_codec_context->time_base = AVRational{ 1,25 };
    thumbnail_cover_codec_context->pix_fmt = PIX_OUTPUT_FORMAT;
    thumbnail_cover_codec_context->mb_lmin = encoder_codec_ctx->qmin * FF_QP2LAMBDA;
    thumbnail_cover_codec_context->mb_lmax = encoder_codec_ctx->qmax * FF_QP2LAMBDA;
    thumbnail_cover_codec_context->flags = AV_CODEC_FLAG_QSCALE;
    thumbnail_cover_codec_context->global_quality = encoder_codec_ctx->qmin * FF_QP2LAMBDA;
    dst_frame->pts = 1;
    dst_frame->quality = encoder_codec_ctx->global_quality;
    */

    ret = avcodec_open2(thumbnail_cover_codec_context, output_codec, NULL);
    if (ret < 0) {
        ALOGV("init_output_jpeg_image - error avcodec_open2 (%s)\n", get_error_text(ret));
        av_packet_unref(&thumbnail_cover_packet);
        avcodec_free_context(&thumbnail_cover_codec_context);
        return ret;
    }

    ret = resize_image(original_pixel_format,
                     thumbnail_cover_codec_context,
                     &thumbnail_cover_packet,
                     input_frame,
                     thumb_width, thumb_height,
                     isapicture);
    if (ret >= 0) {
        //ALOGV("   - result size: %zu %s\n", thumbnail_cover_packet.size, destname);
        file = fopen(destname, "wb");
        if (file) {
            fwrite(thumbnail_cover_packet.data, 1, thumbnail_cover_packet.size, file);
            fclose(file);
            ret = 0;
        } else {
            ALOGV("fopen error destination path %s\n", destname);
            ret = -1;
        }
    } else {
        ALOGV("media_get_metadata - Error resize_image\n");
    }

    av_packet_unref(&thumbnail_cover_packet);
    avcodec_free_context(&thumbnail_cover_codec_context);

    return ret;
}

/**
 * calculate the thumbnail dimensions, taking pixel aspect into account
 *
 * @param src_width source image width
 * @param src_height source image height
 * @param src_sar_num
 * @param src_sar_den
 * @param dst_width desired thumbnail width (set)
 * @param dst_height desired thumbnail height (set)
  */
static void
calculate_thumbnail_dimensions(int src_width,
                               int src_height,
                               int src_sar_num,
                               int src_sar_den,
                               int *dst_width,
                               int *dst_height,
                               int maxthumbw,
                               int maxthumbh) {
    if ((src_sar_num <= 0) || (src_sar_den <= 0)) {
        src_sar_num = 1;
        src_sar_den = 1;
    }
    if ((src_width * src_sar_num) / src_sar_den > src_height) {
        if (maxthumbw <= 0)
            *dst_width = src_width;
        else
            *dst_width = maxthumbw;
        *dst_height = (*dst_width * src_height)
                      / ((src_width * src_sar_num) / src_sar_den);
    } else {
        if (maxthumbh <= 0)
            *dst_height = src_height;
        else
            *dst_height = maxthumbh;
        *dst_width = (*dst_height
                      * ((src_width * src_sar_num) / src_sar_den))
                     / src_height;
    }
    if (*dst_width < 8)
        *dst_width = 8;
    if (*dst_height < 1)
        *dst_height = 1;
    //ALOGV("Thumbnail dimensions: %d %d\n", *dst_width, *dst_height);
}

struct buffer_data {
    uint8_t *ptr;
    size_t size; ///< size left in the buffer
};
static int read_packet(void *opaque, uint8_t *buf, int buf_size) {
    struct buffer_data *bd = (struct buffer_data *)opaque;
    buf_size = FFMIN(buf_size, bd->size);
    if (!buf_size)
        return AVERROR_EOF;
    printf("ptr:%p size:%zu\n", bd->ptr, bd->size);
    /* copy internal buffer data to buf */
    memcpy(buf, bd->ptr, buf_size);
    bd->ptr  += buf_size;
    bd->size -= buf_size;
    return buf_size;
}

static int
extract_video(const char *fname, uint8_t *buf, int buf_size, const char *destname, int maxwidth, int maxheight, int fisapicture, int64_t maxincrement,
              int maxpercentvideo, bool getrgb, const char *filterrgb, bool blurrgb, const char *additionalmetadata) {
    AVPacket packet;
    struct AVFormatContext *format_ctx;
    AVIOContext *avio_ctx = NULL;
    uint8_t *avio_ctx_buffer = NULL;
    int avio_ctx_buffer_size = 4096;
    struct buffer_data bd = { 0 };
    AVCodecContext *codec_ctx;
    AVCodecParameters *codecpar;
    const AVCodec *codec;
    AVDictionary *options;
    AVFrame *frame;
    AVDictionaryEntry *tag = NULL;
    int video_stream_index;
    int original_width;
    int original_height;
    int thumb_width;
    int thumb_height;
    int i;
    int err;
    int ret = -1;
    int totalpicturecount = 0;
    long long duration = -1;
    int acount = 0;
    int scount = 0;
    int rotangle = 0;
    bool dofilterrgb = false;
    if (filterrgb != NULL)
        dofilterrgb = true;
    bool isapicture = false;
    if (fisapicture == 0)
        isapicture = false;
    else if (fisapicture == 1)
        isapicture = true;
    int theta = 0;
    char creationtime[256];
    int creationtimel = 0;
    bool checkedmetadataonce = false;
    char *addmetadata = NULL;
    int addmetadatal = 0;

    format_ctx = avformat_alloc_context();
    if (NULL == format_ctx)
        return ret;

    if (buf_size > 0) {
        fname = NULL;
        bd.ptr  = buf;
        bd.size = buf_size;
        avio_ctx_buffer = (uint8_t *) av_malloc(avio_ctx_buffer_size);
        if (!avio_ctx_buffer) {
            return ret;
        }
        avio_ctx = avio_alloc_context(avio_ctx_buffer, avio_ctx_buffer_size, 0, &bd, &read_packet, NULL, NULL);
        if (!avio_ctx) {
            return ret;
        }
        format_ctx->pb = avio_ctx;
    }

    // Open the video
    options = NULL;
    if (0 != avformat_open_input(&format_ctx, fname, NULL, &options))
        return ret;
    av_dict_free(&options);

    // Get the stream information
    if (0 > avformat_find_stream_info(format_ctx, NULL)) {
        avformat_close_input(&format_ctx);
        return ret;
    }

    // Find the video stream
    codec = NULL;
    codec_ctx = NULL;
    video_stream_index = -1;
    /*for (i = 0; i < format_ctx->nb_streams; i++) {
        codecpar = format_ctx->streams[i]->codecpar;
        if (AVMEDIA_TYPE_VIDEO == codecpar->codec_type)
            ALOGV("stream %d AVMEDIA_TYPE_VIDEO", i);
        else if (AVMEDIA_TYPE_AUDIO == codecpar->codec_type)
            ALOGV("stream %d AVMEDIA_TYPE_AUDIO", i);
        else if (AVMEDIA_TYPE_DATA == codecpar->codec_type)
            ALOGV("stream %d AVMEDIA_TYPE_DATA", i);
        else if (AVMEDIA_TYPE_SUBTITLE == codecpar->codec_type)
            ALOGV("stream %d AVMEDIA_TYPE_SUBTITLE", i);
        else if (AVMEDIA_TYPE_UNKNOWN == codecpar->codec_type)
            ALOGV("stream %d AVMEDIA_TYPE_UNKNOWN", i);
        else if (AVMEDIA_TYPE_ATTACHMENT == codecpar->codec_type)
            ALOGV("stream %d AVMEDIA_TYPE_ATTACHMENT", i);
        else if (AVMEDIA_TYPE_UNKNOWN == codecpar->codec_type)
            ALOGV("stream %d AVMEDIA_TYPE_UNKNOWN", i);
    }*/
    for (i = 0; i < format_ctx->nb_streams; i++) {
        switch(format_ctx->streams[i]->codecpar->codec_type) {
            case AVMEDIA_TYPE_AUDIO:
                acount++;
                break;
            case AVMEDIA_TYPE_SUBTITLE:
                scount++;
                break;
            default:
                break;
        }
    }
    // "creation_time=2022-12-10T14:06:15.000000Z" aussi dans stream
    // make= model=
    tag = av_dict_get(format_ctx->metadata, "creation_time", NULL, AV_DICT_IGNORE_SUFFIX);
    if (tag) {
        //ALOGV("%s=%s", tag->key, tag->value);
        unsigned int vall = strlen(tag->value);
        int p = 0;
        int n = 0;
        for (i = 0 ; i < vall ; i++) {
            if ((char) '0' <= tag->value[i] && tag->value[i] <= (char) '9') {
                creationtime[p++] = tag->value[i];
                n++;
            } else if ( tag->value[i] == ':')
                creationtime[p++] = tag->value[i];
            else
                creationtime[p++] = (char) ' ';
            if (n == 14)
                break;
        }
        if (p > 0) {
            creationtime[p++] = 0x0;
            //sprintf(creationtime, "%s", tag->value);
            creationtimel = 1;
        }
    }
    /*tag = av_dict_get(format_ctx->metadata, "", NULL, AV_DICT_IGNORE_SUFFIX);
    while (tag != NULL) {
        ALOGV("format_ctx <%s=%s>", tag->key, tag->value);
        tag = av_dict_get(format_ctx->metadata, "", tag, AV_DICT_IGNORE_SUFFIX);
    }*/
    for (i = 0; i < format_ctx->nb_streams; i++) {
        AVStream *st = format_ctx->streams[i];
        codecpar = st->codecpar;
        if (AVMEDIA_TYPE_VIDEO != codecpar->codec_type) {
            //ALOGV("ERROR codec_type %d %s", codecpar->codec_type, fname);
            continue;
        }
        if (fisapicture == -1) {
            if (codecpar->codec_id == AV_CODEC_ID_BMP
                || codecpar->codec_id == AV_CODEC_ID_GIF
                || codecpar->codec_id == AV_CODEC_ID_MJPEG
                || codecpar->codec_id == AV_CODEC_ID_PNG
                || codecpar->codec_id == AV_CODEC_ID_PPM
                || codecpar->codec_id == AV_CODEC_ID_PGM
                || codecpar->codec_id == AV_CODEC_ID_WEBP
                || codecpar->codec_id == AV_CODEC_ID_NONE ) {
                //ALOGV("this is a picture : codec id %d", codecpar->codec_id);
                isapicture = true;
            }
        }
        // Find the actual codec
        codec = avcodec_find_decoder(codecpar->codec_id);
        if (NULL == codec) {
            ALOGV("ERROR avcodec_find_decoder %d %s", i, fname);
            continue;
        }
        codec_ctx = avcodec_alloc_context3(codec);
        if (NULL == codec_ctx) {
            ALOGV("ERROR avcodec_alloc_context3 %s", fname);
            continue;
        }
        if(avcodec_parameters_to_context(codec_ctx, codecpar) <0 ){
            ALOGV("ERROR avcodec_parameters_to_context %s", fname);
            avcodec_free_context(&codec_ctx);
            continue;
        }
        options = NULL;
        // Open the new decoder codec context
        err = avcodec_open2(codec_ctx, codec, &options);
        if (0 != err) {
            ALOGV("ERROR avcodec_open2 %s", fname);
            avcodec_free_context(&codec_ctx);
            codec = NULL;
            continue;
        }

        /*tag = av_dict_get(st->metadata, "", NULL, AV_DICT_IGNORE_SUFFIX);
        while (tag != NULL) {
            ALOGV("stream <%s=%s>", tag->key, tag->value);
            tag = av_dict_get(st->metadata, "", tag, AV_DICT_IGNORE_SUFFIX);
        }*/

        /*if (st->nb_side_data > 0) {
            //ALOGV("stream side_data %d", st->nb_side_data);
            uint8_t *displaymatrix = av_stream_get_side_data(st, AV_PKT_DATA_DISPLAYMATRIX, NULL);
            if (displaymatrix != NULL) {
                theta = (int) av_display_rotation_get((int32_t *) displaymatrix);
                //ALOGV("displaymatrix %d", theta);
            }
        }*/

        av_dict_free(&options);
        video_stream_index = i;
        break;
    }
    if ((-1 == video_stream_index) || (0 == codec_ctx->width) || (0 == codec_ctx->height)) {
        ALOGV("No video streams or no suitable codec found %s", fname);
        avcodec_free_context(&codec_ctx);
        avformat_close_input(&format_ctx);
        return ret;
    }

    calculate_thumbnail_dimensions(codec_ctx->width, codec_ctx->height,
                                       codec_ctx->sample_aspect_ratio.num,
                                       codec_ctx->sample_aspect_ratio.den,
                                       &original_width, &original_height, codec_ctx->width, codec_ctx->height);
    //ALOGV("original frame %dx%d -> %dx%d", codec_ctx->width, codec_ctx->height, original_width, original_height);
    calculate_thumbnail_dimensions(codec_ctx->width, codec_ctx->height,
                                       codec_ctx->sample_aspect_ratio.num,
                                       codec_ctx->sample_aspect_ratio.den,
                                       &thumb_width, &thumb_height, maxwidth, maxheight);
   // ALOGV("thumbnail frame %dx%d -> %dx%d", codec_ctx->width, codec_ctx->height, thumb_width, thumb_height);

    // - `video_frame` the video frame (any format)
    frame = av_frame_alloc();
    if (NULL == frame) {
        ALOGV("Failed to allocate frame %s", fname);
        avcodec_free_context(&codec_ctx);
        avformat_close_input(&format_ctx);
        return ret;
    }

    if (!isapicture) {
        if (format_ctx->duration == AV_NOPTS_VALUE) {
            duration = -1;
            ALOGV("Duration unknown %s", fname);
        } else {
            duration = format_ctx->duration;
            //ALOGV("Duration %lld\n" , duration);
        }
    }
    int64_t file_size = avio_size(format_ctx->pb);

    bool gotframe = false;
    int lcc = 0;
    long long j = 0;
    if (isapicture)
        maxincrement = 1;
    int64_t byteposition = 0;
    int passnumber = 0;
    bool respecttimestamp = true;
    bool onlykeyframes = true;
    int64_t jlastposition = -1;
    // A packet is a wrapper of some useful information, related to the video playback, like the video frames, the audio, the subtitles, etc...
    // With `av_read_frame` it is possible to retrieve a series of packets, that decoded allow to get the video information. Doc: https://ffmpeg.org/doxygen/2.8/structAVPacket.html#details
    int64_t timestamp = ((j+1) * file_size) / (maxincrement+2);
    while (j < maxincrement && lcc < 1000000) {
        lcc++;
        err = av_read_frame(format_ctx, &packet);
        if (err == AVERROR_EOF) {
            passnumber++;
            av_packet_unref(&packet);
            // passnumber 0 : all pics for long shitty vids
            if (!isapicture && j <= 1 && passnumber == 1) {
                //ALOGV("%d %6d av_read_frame No more frames AVERROR_EOF onlykeyframes", passnumber, lcc);
                // short hq vids
                respecttimestamp = false;
                onlykeyframes = true;
                j = 0;
                jlastposition = -1;
                err = av_seek_frame(format_ctx, video_stream_index, 0, AVSEEK_FLAG_BACKWARD);
                if (err < 0)
                    ALOGV("%6d ERROR av_seek_frame AVSEEK_FLAG_ANY %d", lcc, err);
                else
                    avcodec_flush_buffers(codec_ctx); // nécessaire
                continue;
            } else if (!isapicture && j <= 1 && passnumber == 2) {
                //ALOGV("%d %6d av_read_frame No more frames AVERROR_EOF respecttimestamp", passnumber, lcc);
                // 3 pics for animated gif
                respecttimestamp = true;
                onlykeyframes = false;
                j = 0;
                jlastposition = -1;
                err = av_seek_frame(format_ctx, video_stream_index, 0, AVSEEK_FLAG_BACKWARD);
                if (err < 0)
                    ALOGV("%6d ERROR av_seek_frame AVSEEK_FLAG_ANY %d", lcc, err);
                else
                    avcodec_flush_buffers(codec_ctx); // nécessaire
                continue;
            /*} else if (!isapicture && j <= 1 && passnumber == 2) {
                ALOGV("%d %6d av_read_frame No more frames AVERROR_EOF", passnumber, lcc);
                respecttimestamp = false;
                onlykeyframes = false;
                j = 0;
                jlastposition = 0;
                err = av_seek_frame(format_ctx, video_stream_index, 0, AVSEEK_FLAG_ANY);
                if (err < 0)
                    ALOGV("%6d ERROR av_seek_frame AVSEEK_FLAG_ANY %d", lcc, err);
                else
                    avcodec_flush_buffers(codec_ctx); // nécessaire
                continue;*/
            } else {
                //ALOGV("%d %6d av_read_frame No more frames AVERROR_EOF --stop break", passnumber, lcc);
                break;
            }
        } else if (err < 0) {
            ALOGV("%6d av_read_frame error %d", lcc, err);
            av_packet_unref(&packet);
            continue;
        }
        if (onlykeyframes && !(packet.flags & AV_PKT_FLAG_KEY)) {
            //ALOGV("%6d packet.flags & AV_PKT_FLAG_KEY = false %d", lcc, err);
            av_packet_unref(&packet);
            continue;
        }
        byteposition = format_ctx->pb->pos;
        if (passnumber < 2) { // otherwise send everything to the decoder first
            if (byteposition <= jlastposition) {
                // change packet
                av_packet_unref(&packet);
                continue;
            }
            if (!isapicture && byteposition < timestamp && respecttimestamp) {
                // respect preset intervals
                av_packet_unref(&packet);
                continue;
            }
        }
        //ALOGV("%d %6d av_read_frame %.1f%% %12lld / %12lld", passnumber, lcc, percent, byteposition, file_size);
        if (packet.stream_index == video_stream_index) {
            err = avcodec_send_packet(codec_ctx, &packet);       // Set the packet to decode
            if (err < 0) {
                //ALOGV("%6d avcodec_send_packet Can't send this packet to the decoder %d", lcc, err);
                av_packet_unref(&packet);
                continue;
            }
            float percent = (100.0f * (float) byteposition) / (1.0f * (float) file_size);
            gotframe = false;
            while (!gotframe) {
                err = avcodec_receive_frame(codec_ctx, frame);   // Decode all the frames inside that packet
                if (err == AVERROR_EOF) {
                    ALOGV("%6d avcodec_receive_frame No more frames AVERROR_EOF %d", lcc, err);
                    break;
                } else if (err == AVERROR(EINVAL)) {
                    ALOGV("%6d avcodec_receive_frame Codec not opened AVERROR(EINVAL) %d", lcc, err);
                    break;
                } else if (err == AVERROR(EAGAIN)) {
                    //ALOGV("avcodec_receive_frame no picture in this frame peek next frame inside this packet AVERROR(EAGAIN) %s", fname);
                    break;
                } else if (err < 0) {
                    ALOGV("%6d avcodec_receive_frame Error during decoding packet %d", lcc, err);
                    break;
                } else {

                    /*
                    tag = av_dict_get(frame->metadata, "rotate", NULL, AV_DICT_IGNORE_SUFFIX);
                    if (tag)
                        ALOGV("%s=%s", tag->key, tag->value);
                    tag = av_dict_get(frame->metadata, "Orientation", NULL, AV_DICT_IGNORE_SUFFIX);
                    if (tag)
                        ALOGV("%s=%s", tag->key, tag->value);
                    tag = av_dict_get(frame->metadata, "DateTime", NULL, AV_DICT_IGNORE_SUFFIX);
                    if (tag)
                        ALOGV("%s=%s", tag->key, tag->value);
                    if (frame->nb_side_data > 0)
                        ALOGV("frame nb_side_data %d", frame->nb_side_data);
                    */
                    // Make= Model=
                    // DateTime=2007:02:09 21:56:32 DateTimeOriginal= DateTimeDigitized=
                    /*tag = av_dict_get(frame->metadata, "", NULL, AV_DICT_IGNORE_SUFFIX);
                    while (tag) {
                        ALOGV("frame <%s=%s>", tag->key, tag->value);
                        tag = av_dict_get(frame->metadata, "", tag, AV_DICT_IGNORE_SUFFIX);
                    }*/

                    if (!isapicture && respecttimestamp && byteposition < timestamp && passnumber >= 2) {
                        // respect preset intervals
                        //av_packet_unref(&packet);
                        //continue;
                        break;
                    } else {
                    //if (((frame->flags & AV_FRAME_FLAG_KEY) && onlykeyframes) || !onlykeyframes) {
                        if (blurrgb) {
                            ret = blur_rgb(
                                    codec_ctx,
                                    frame,
                                    original_width,
                                    original_height,
                                    maxwidth,
                                    maxheight,
                                    isapicture
                            );
                        } else if (getrgb) {
                            ret = create_rgb(
                                    codec_ctx,
                                    frame,
                                    original_width,
                                    original_height,
                                    isapicture
                            );
                        } else if (dofilterrgb) {
                            AVFrame *filt_frame = NULL;
                            ret = filter(
                                    codec_ctx,
                                    frame,
                                    isapicture,
                                    filterrgb,
                                    filt_frame,
                                    false
                            );
                            if (ret >= 0) {
                              av_frame_unref(filt_frame);
                              av_frame_free(&filt_frame);
                            }
                        } else {

                            if (!checkedmetadataonce && filterrgb == NULL && !dofilterrgb) {
                                checkedmetadataonce = true;
                                bool rotate = false;
                                tag = av_dict_get(frame->metadata, "Orientation", NULL, AV_DICT_IGNORE_SUFFIX);
                                char ori = ' ';
                                if (tag) {
                                    unsigned int tagl = strlen(tag->value);
                                    if (tagl > 1) {
                                        //ALOGV("Orientation <%s> %c", tag->value, tag->value[tagl - 1]);
                                        //
                                        //    0x0112 	Orientation 	int16u 	IFD0
                                        //
                                        ori = (char) tag->value[tagl - 1];
                                        if (ori == (char) '0') {
                                        } else if (ori == (char) '1') {
                                            //        1 = Horizontal (normal)
                                        } else if (ori == (char) '2') {
                                            //        2 = Mirror horizontal
                                            filterrgb = "hflip";
                                        } else if (ori == (char) '3') {
                                            //        3 = Rotate 180
                                            filterrgb = "vflip,hflip";
                                        } else if (ori == (char) '4') {
                                            //        4 = Mirror vertical
                                            filterrgb = "vflip";
                                        } else if (ori == (char) '5') {
                                            //        5 = Mirror horizontal and rotate 270 CW
                                            filterrgb = "transpose=2";
                                            rotate = true;
                                        } else if (ori == (char) '6') {
                                            //        6 = Rotate 90 CW
                                            filterrgb = "transpose=1";
                                            rotate = true;
                                        } else if (ori == (char) '7') {
                                            //        7 = Mirror horizontal and rotate 90 CW
                                            filterrgb = "transpose=1";
                                            rotate = true;
                                        } else if (ori == (char) '8') {
                                            //        8 = Rotate 270 CW
                                            filterrgb = "transpose=2";
                                            rotate = true;
                                        } else {
                                            ALOGV("unknown Orientation <%s> %c", tag->value, tag->value[tagl - 1]);
                                        }
                                    }
                                }
                                if (frame->nb_side_data > 0) {
                                    //ALOGV("frame side_data %d", frame->nb_side_data);
                                    AVFrameSideData *sidedata = av_frame_get_side_data(frame, AV_FRAME_DATA_DISPLAYMATRIX);
                                    if (sidedata != NULL) {
                                        theta = (int) av_display_rotation_get((int32_t *) sidedata->data);
                                        //ALOGV("%d displaymatrix %d", sidedata->size, theta);
                                        if (theta != 0) {
                                            if (theta == -180 || theta == 180) {
                                                filterrgb = "vflip,hflip";
                                            } else if (theta == 90 || theta == -270) {
                                                filterrgb = "transpose=2";
                                                rotate = true;
                                            } else if (theta == -90 || theta == 270) {
                                                filterrgb = "transpose=1";
                                                rotate = true;
                                            } else {
                                                ALOGV("unknown Orientation %d", theta);
                                            }
                                        }
                                    }
                                }
                                //if (filterrgb != NULL)
                                //    ALOGV("ori %c theta %d rotate %d filter <%s>", ori, theta, rotate, filterrgb);
                                if (rotate) {
                                    int tmp = thumb_width;
                                    thumb_width = thumb_height;
                                    thumb_height = tmp;
                                    tmp = original_width;
                                    original_width = original_height;
                                    original_height = tmp;
                                }
                            }

                            ret = 0;
                            char outputfilename[256];
                            /* if (isapicture)
                                sprintf(outputfilename, "%s.ppm", destname);
                            else
                                sprintf(outputfilename, "%s.%lld.ppm", destname, j);
                            ppm_save(frame->data[0], frame->linesize[0],
                                     frame->width, frame->height,
                                     outputfilename); */
                            int wevegotpictures = 0;
                            //if (isapicture)
                            //    sprintf(outputfilename, "%s", destname);
                            //else
                            if (filterrgb != NULL && !dofilterrgb) {
                                AVFrame *filt_frame = NULL;
                                ret = filter(
                                    codec_ctx,
                                    frame,
                                    isapicture,
                                    filterrgb,
                                    filt_frame,
                                    true
                                );
                                if (ret >= 0) {
                                    // thumbnails
                                    sprintf(outputfilename, "%s_%lld", destname, j);
                                    if (j == 0)
                                      additionalMetadata(addmetadata, addmetadatal, outputfilename);
                                    wevegotpictures = create_jpg(
                                        AV_PIX_FMT_BGRA,
                                        filt_frame,
                                        outputfilename,
                                        thumb_width,
                                        thumb_height,
                                        isapicture
                                    );
                                    if (wevegotpictures == 0)
                                        totalpicturecount = 1 + (int) j;
                                    // big pictures
                                    if (!isapicture) {
                                        sprintf(outputfilename, "%s.%lld", destname, j);
                                        int count = create_jpg(
                                            AV_PIX_FMT_BGRA,
                                            filt_frame,
                                            outputfilename,
                                            original_width,
                                            original_height,
                                            isapicture
                                        );
                                    }
                                    av_frame_unref(filt_frame);
                                    av_frame_free(&filt_frame);
                                }
                            } else {
                                // thumbnails
                                sprintf(outputfilename, "%s_%lld", destname, j);
                                if (j == 0)
                                  additionalMetadata(addmetadata, addmetadatal, outputfilename);
                                wevegotpictures = create_jpg(
                                    codec_ctx->pix_fmt,
                                    frame,
                                    outputfilename,
                                    thumb_width,
                                    thumb_height,
                                    isapicture
                                );
                                if (wevegotpictures == 0)
                                    totalpicturecount = 1 + (int) j;
                                // big pictures
                                if (!isapicture) {
                                    sprintf(outputfilename, "%s.%lld", destname, j);
                                    create_jpg(
                                        codec_ctx->pix_fmt,
                                        frame,
                                        outputfilename,
                                        original_width,
                                        original_height,
                                        isapicture
                                    );
                                }
                            }
                        }
                        //ALOGV("%d %6d got picture number %lld/%lld %.1f%% %12lld / %12lld",
                        //        passnumber, lcc, j, maxincrement, percent, byteposition, file_size);
                        jlastposition = byteposition;
                        gotframe = true;
                        j++;
                        timestamp = ((j + 1) * file_size) / (maxincrement + 2);
                        if (maxpercentvideo < 100 && ((int) percent) > maxpercentvideo &&
                            totalpicturecount > 5)
                            lcc = 1000000; // we stop here
                    }
                }
            }
        }
        av_packet_unref(&packet); // Always free the read packet
    }

    if (//!isapicture &&
        totalpicturecount > 0 && !getrgb && !blurrgb && !dofilterrgb) {
        char outputfilename[256];
        FILE *file;
        sprintf(outputfilename, "%s", destname);
        if (remove(outputfilename) == 0)
          ALOGV("removed %s", outputfilename);
        for (j = totalpicturecount ; j < 1000 ; j++) {
          sprintf(outputfilename, "%s_%lld", destname, j);
          if (remove(outputfilename) != 0)
            break;
          ALOGV("removed %s", outputfilename);
          sprintf(outputfilename, "%s.%lld", destname, j);
          if (remove(outputfilename) != 0)
            break;
          ALOGV("removed %s", outputfilename);
        }
        sprintf(outputfilename, "%s_0", destname);
        file = fopen(outputfilename, "ab");
        if (file) {
            fputc('\n', file);
            fprintf(file, "filmstripcount=%d\n", totalpicturecount);
            fprintf(file, "width=%d\nheight=%d\n", original_width, original_height);
            if (maxwidth > 1 && maxheight > 1)
              fprintf(file, "maxwidth=%d\nmaxheight=%d\n", maxwidth, maxheight);
            if (acount > 0)
                fprintf(file, "audio=%d\n", acount);
            if (scount > 0)
                fprintf(file, "subtitle=%d\n", scount);
            if (theta != 0)
                fprintf(file, "rotation=%d\n", theta);
            if (creationtimel > 0)
                fprintf(file, "creationtime=%s\n", creationtime);
            if (duration != -1)
                fprintf(file, "duration=%lld\n", duration);
            if (addmetadatal > 0) {
              fputs(addmetadata, file);
              free((char *) addmetadata);
            } else if (additionalmetadata != NULL)
              fputs(additionalmetadata, file);
            fclose(file);
        } else {
            ALOGV("fopen error destination path %s\n", destname);
        }
    }



    av_frame_unref(frame);
    av_frame_free(&frame);
    avcodec_free_context(&codec_ctx);

    if (buf_size > 0) {
        if (avio_ctx)
            av_freep(&avio_ctx->buffer);
        avio_context_free(&avio_ctx);
    }

    avformat_close_input(&format_ctx);

    return ret;
}

}

#ifdef __clang__
extern "C" JNIEXPORT jint JNICALL Java_la_daube_photochiotte_Libextractor_getWidth(JNIEnv *env, jclass thisClass) {
    return bitmapwidth;
}
extern "C" JNIEXPORT jint JNICALL Java_la_daube_photochiotte_Libextractor_getHeight(JNIEnv *env, jclass thisClass) {
    return bitmapheight;
}
extern "C" JNIEXPORT jint JNICALL Java_la_daube_photochiotte_Libextractor_getStride(JNIEnv *env, jclass thisClass) {
    return bitmapstride;
}
extern "C" JNIEXPORT jintArray JNICALL Java_la_daube_photochiotte_Libextractor_getIntarray(JNIEnv *env, jclass thisClass) {
    int size = (bitmapstride * bitmapheight) / 4;
    jintArray intJavaArray = NULL;
    if (size > 0) {
        intJavaArray = env->NewIntArray(size);
        if (intJavaArray != NULL)
            env->SetIntArrayRegion(intJavaArray, 0, size, bitmapint);
        bitmapwidth = 0;
        bitmapheight = 0;
        bitmapstride = 0;
        free((int *) bitmapint);
    }
    bitmapint = NULL;
    return intJavaArray;
}
extern "C" JNIEXPORT jint JNICALL Java_la_daube_photochiotte_Libextractor_getRGB(JNIEnv *env, jclass thisClass, jstring jpictureaddress, jbyteArray jbuffer, jint bufferl) {
    char *pictureaddress = NULL;
    jboolean bo;
    bo = JNI_FALSE;
    unsigned char* buf = NULL;
    if (bufferl > 0) {
        buf = new unsigned char[bufferl];
        env->GetByteArrayRegion(jbuffer, 0, bufferl, reinterpret_cast<jbyte *>(buf));
    } else if (jpictureaddress != NULL) {
        pictureaddress = (char *) env->GetStringUTFChars(jpictureaddress, &bo);
    }

    int returncode = extract_video(pictureaddress, buf, bufferl, NULL, -1, -1, 1, 1, 100, true, NULL, false, NULL);

    if (bufferl > 0) {
        env->ReleaseByteArrayElements(jbuffer, reinterpret_cast<jbyte *>(buf), JNI_ABORT);
    } else if (jpictureaddress != NULL) {
        env->ReleaseStringUTFChars(jpictureaddress, pictureaddress);
    }

    return returncode;
}

extern "C" JNIEXPORT jint JNICALL Java_la_daube_photochiotte_Libextractor_renderRGB(JNIEnv *env, jclass thisClass, jobject javaSurface, jint top, jint left, jint bottom, jint right) {
    ARect rect;
    rect.top = top;
    rect.left = left;
    rect.bottom = bottom;
    rect.right = right;
    ANativeWindow* window = ANativeWindow_fromSurface(env, javaSurface);
    if (window != 0 ) {
        ANativeWindow_Buffer buffer;
        ANativeWindow_setBuffersGeometry(window, 0, 0, WINDOW_FORMAT_RGBA_8888);
        if (ANativeWindow_lock(window, &buffer, &rect) == 0) {
            ALOGV("rect %d %d %d %d bitmap %d %d %d buffer %d %d %d ", rect.top, rect.left, rect.bottom, rect.right, bitmapstride, bitmapwidth, bitmapheight, buffer.stride, buffer.width, buffer.height);
            char *ptr = (char *) buffer.bits;
            uint32_t color = 0xffff0000;
            int pos = 0;
            int bpos = 0;
            for (int h = top; h < bottom ; h++) {
                pos = (h * buffer.stride) * 4;
                for (int w = left; w < right ; w++) {
                    memcpy(ptr + pos + w * 4, &color, 4);
                }
            }
            ANativeWindow_unlockAndPost(window);
        }
        ANativeWindow_release(window);
    }
    return 0;
}
extern "C" JNIEXPORT jint JNICALL Java_la_daube_photochiotte_Libextractor_filterRGB(JNIEnv *env, jclass thisClass, jstring jpictureaddress, jbyteArray jbuffer, jint bufferl, jstring jfilterstring) {
    char *pictureaddress = NULL;
    char *filterstring = NULL;
    jboolean bo;
    bo = JNI_FALSE;
    unsigned char* buf = NULL;
    if (bufferl > 0) {
        buf = new unsigned char[bufferl];
        env->GetByteArrayRegion(jbuffer, 0, bufferl, reinterpret_cast<jbyte *>(buf));
    } else if (jpictureaddress != NULL) {
        pictureaddress = (char *) env->GetStringUTFChars(jpictureaddress, &bo);
    }
    if (jfilterstring != NULL)
        filterstring = (char *) env->GetStringUTFChars(jfilterstring, &bo);

    int returncode = extract_video(pictureaddress, buf, bufferl, NULL, -1, -1, 1, 1, 100, false, filterstring, false, NULL);

    if (bufferl > 0) {
        env->ReleaseByteArrayElements(jbuffer, reinterpret_cast<jbyte *>(buf), JNI_ABORT);
    } else if (jpictureaddress != NULL) {
        env->ReleaseStringUTFChars(jpictureaddress, pictureaddress);
    }
    if (filterstring != NULL)
        env->ReleaseStringUTFChars(jfilterstring, filterstring);

    return returncode;
}
extern "C" JNIEXPORT jint JNICALL Java_la_daube_photochiotte_Libextractor_blurRGB(JNIEnv *env, jclass thisClass, jstring jpictureaddress, jint w, jint h) {
    char *pictureaddress = NULL;
    jboolean bo;
    bo = JNI_FALSE;

    if (jpictureaddress != NULL)
        pictureaddress = (char *) env->GetStringUTFChars(jpictureaddress, &bo);

    int returncode = extract_video(pictureaddress, NULL, 0, NULL, w, h, 1, 1, 100, false, NULL, true, NULL);

    if (pictureaddress != NULL)
        env->ReleaseStringUTFChars(jpictureaddress, pictureaddress);

    return returncode;
}
extern "C" JNIEXPORT jint JNICALL Java_la_daube_photochiotte_Libextractor_run(JNIEnv *env, jclass thisClass, jstring jmediaaddress, jstring jdestinationaddress, jint w, jint h, jboolean jisapicture, jlong maxpicturecount, jstring jadditionalmetadata) {
    const char *mediaddress;
    const char *destinationaddress;
    const char *additionalmetadata = NULL;
    jboolean bo;
    bo = JNI_FALSE;

    mediaddress = (const char *) env->GetStringUTFChars(jmediaaddress, &bo);
    destinationaddress = (const char *) env->GetStringUTFChars(jdestinationaddress, &bo);
    if (jadditionalmetadata != NULL)
      additionalmetadata = (char *) env->GetStringUTFChars(jadditionalmetadata, &bo);

    int isapicture = -1;
    if (jisapicture)
        isapicture = 1;
    else
        isapicture = 0;

    int returncode = extract_video(mediaddress, NULL, 0, destinationaddress, w, h, isapicture, maxpicturecount, 100, false, NULL, false, additionalmetadata);

    if (additionalmetadata != NULL)
        env->ReleaseStringUTFChars(jadditionalmetadata, additionalmetadata);
    if (mediaddress != NULL)
        env->ReleaseStringUTFChars(jmediaaddress, mediaddress);
    if (destinationaddress != NULL)
        env->ReleaseStringUTFChars(jdestinationaddress, destinationaddress);

    return returncode;
}
#elif __GNUC__
int main(int argc, char **argv) {
    int rc = -1;

    const char *mediaddress = argv[1];
    const char *destinationaddress = argv[2];
    const int w = atoi(argv[3]);
    const int h = atoi(argv[4]);
    const int maxpicturecount = atoi(argv[5]);
    const int maxpercentvideo = atoi(argv[6]);

    const int fisapicture = -1;

    rc = extract_video(mediaddress, NULL, 0, destinationaddress, w, h, fisapicture, maxpicturecount, maxpercentvideo, false, NULL, false, NULL);

    return rc;
}
#endif






















