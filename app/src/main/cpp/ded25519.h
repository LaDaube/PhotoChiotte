
static const char b32str[] = "abcdefghijklmnopqrstuvwxyz234567";
void dbase32_encode(const unsigned char *in, int inlen, char *out, int *outlen) {
    const int outlensave = *outlen;
    while (inlen && *outlen) {
        *out++ = b32str[(in[0] >> 3) & 0x1f];
        if (!--*outlen)
            break;
        *out++ = b32str[((in[0] << 2) + (--inlen ? in[1] >> 6 : 0)) & 0x1f];
        if (!--*outlen)
            break;
        *out++ = (inlen ? b32str[(in[1] >> 1) & 0x1f] : '=');
        if (!--*outlen)
            break;
        *out++ = (inlen ? b32str[((in[1] << 4) + (--inlen ? in[2] >> 4 : 0)) & 0x1f] : '=');
        if (!--*outlen)
            break;
        *out++ = (inlen ? b32str[((in[2] << 1) + (--inlen ? in[3] >> 7 : 0)) & 0x1f] : '=');
        if (!--*outlen)
            break;
        *out++ = (inlen ? b32str[(in[3] >> 2) & 0x1f] : '=');
        if (!--*outlen)
            break;
        *out++ = (inlen ? b32str[((in[3] << 3) + (--inlen ? in[4] >> 5 : 0)) & 0x1f] : '=');
        if (!--*outlen)
            break;
        *out++ = inlen ? b32str[in[4] & 0x1f] : '=';
        if (!--*outlen)
            break;
        if (inlen)
            inlen--;
        if (inlen)
            in += 5;
    }
    if (*outlen)
        *out = '\0';
    *outlen = outlensave - *outlen;
}

int dht_new_private(unsigned char *priv, int *privl) {
    int r;
    EVP_PKEY *keypair = NULL;
    EVP_PKEY_CTX *pctx1 = EVP_PKEY_CTX_new_id(EVP_PKEY_ED25519, NULL);
    EVP_PKEY_keygen_init(pctx1);
    EVP_PKEY_keygen(pctx1, &keypair);
    EVP_PKEY_CTX_free(pctx1);
    r = EVP_PKEY_get_raw_private_key(keypair, priv, (size_t *) privl);
    //PEM_write_PrivateKey(stdout, keypair, NULL, NULL, 0, NULL, NULL);
    //PEM_write_PUBKEY(stdout, keypair);
    EVP_PKEY_free(keypair);
    return r;
}

int dht_hostname_from_public(const unsigned char *pub, int publ, char *hostname, int *hostnamel) {
    int i, r;
    EVP_PKEY *keypair = EVP_PKEY_new_raw_public_key(EVP_PKEY_ED25519, NULL, pub, publ);
    if (keypair == NULL) {
        printf("EVP_PKEY_new_raw_public_key NULL");
        return -1;
    }

    const char *checksum_str = ".onion checksum";
    const unsigned char version_byte[] = "\x03";

    EVP_MD_CTX *mdctx;
    const EVP_MD *md = EVP_sha3_256();
    unsigned char md_value[EVP_MAX_MD_SIZE];
    unsigned int md_len;
    mdctx = EVP_MD_CTX_new();
    EVP_DigestInit_ex(mdctx, md, NULL);
    EVP_DigestUpdate(mdctx, checksum_str, strlen(checksum_str));
    EVP_DigestUpdate(mdctx, pub, publ);
    EVP_DigestUpdate(mdctx, version_byte, 1);
    EVP_DigestFinal_ex(mdctx, md_value, &md_len);
    EVP_MD_CTX_free(mdctx);

    int onionl = publ + 2 + 1;
    unsigned char onion[onionl];
    for (i = 0; i < publ; i++)
        onion[i] = pub[i];
    for (i = 0; i < 2; i++)
        onion[publ + i] = md_value[i];
    onion[publ + 2] = version_byte[0];

    dbase32_encode(onion, onionl, hostname, hostnamel);
    strcat(hostname, ".onion");
    *hostnamel = strlen(hostname);
    //printf("(%d) %s", *hostnamel, hostname);

    EVP_PKEY_free(keypair);
    return 0;
}

int dht_gen_hidden_service_files(unsigned char *priv, int *privl) {
    int r, i;
    FILE *fp;

    EVP_PKEY *keypair = EVP_PKEY_new_raw_private_key(EVP_PKEY_ED25519, NULL, priv,*privl);
    if (keypair == NULL) {
        printf("EVP_PKEY_new_raw_private_key NULL");
        return -1;
    }
    PEM_write_PrivateKey(stdout, keypair, NULL, NULL, 0, NULL, NULL);
    PEM_write_PUBKEY(stdout, keypair);

    const char *_private_key_filename = "private.raw";
    char fpriv[256];
    snprintf(fpriv, 255, "%s%s%s", PATHPREFIX, PATHPREFIXTORHIDDEN, _private_key_filename);
    fp = fopen(fpriv, "wb");
    if (fp == NULL) {
        printf("error no write access");
        return -1;
    }
    fwrite(priv, 1, *privl, fp);
    fclose(fp);
    dumpbufx("our raw private key is", priv, *privl);

    // save public key to file
    int publ = 128;
    unsigned char pub[128];
    r = EVP_PKEY_get_raw_public_key(keypair, pub, (size_t *) &publ);
    dumpbufx("our raw public key is", pub, publ);

    const char *_public_key_filename = "public.raw";
    char fpub[256];
    snprintf(fpub, 255, "%s%s%s", PATHPREFIX, PATHPREFIXTORHIDDEN, _public_key_filename);
    printf("raw public key written to %s%s%s", PATHPREFIX, PATHPREFIXTORHIDDEN, _public_key_filename);
    fp = fopen(fpub, "wb");
    if (fp == NULL) {
        printf("error no write access");
        return -1;
    }
    fwrite(pub, 1, publ, fp);
    fclose(fp);
    char fpubhs[256];
    const char *_pub_key_filename = "hs_ed25519_public_key";
    const char *_header_pub = "== ed25519v1-public: type0 ==\x00\x00\x00";
    snprintf(fpubhs, 255, "%s%s%s", PATHPREFIX, PATHPREFIXTORHIDDEN, _pub_key_filename);
    fp = fopen(fpubhs, "wb");
    if (fp == NULL) {
        printf("error no write access");
        return -1;
    }
    fwrite(_header_pub, 1, HASHL, fp);
    fwrite(pub, 1, publ,  fp);
    fclose(fp);

    // save private key to file
    EVP_MD_CTX *mdctx5;
    const EVP_MD *md5 = EVP_sha512();
    unsigned char md_value5[EVP_MAX_MD_SIZE];
    unsigned int md_len5;
    mdctx5 = EVP_MD_CTX_new();
    EVP_DigestInit_ex(mdctx5, md5, NULL);
    EVP_DigestUpdate(mdctx5, priv, *privl);
    EVP_DigestFinal_ex(mdctx5, md_value5, &md_len5);
    EVP_MD_CTX_free(mdctx5);

    md_value5[0] &= 248;
    md_value5[31] &= 63;
    md_value5[31] |= 64;

    char fprivhs[256];
    const char *_priv_key_filename = "hs_ed25519_secret_key";
    const char *_header_priv = "== ed25519v1-secret: type0 ==\x00\x00\x00";
    snprintf(fprivhs, 255, "%s%s%s", PATHPREFIX, PATHPREFIXTORHIDDEN, _priv_key_filename);
    fp = fopen(fprivhs, "wb");
    if (fp == NULL) {
        printf("error no write access");
        return -1;
    }
    fwrite(_header_priv, 1, HASHL,  fp);
    fwrite(md_value5, 1, md_len5,  fp);
    fclose(fp);

    int hostnamel = 256;
    char hostname[256];
    dht_hostname_from_public(pub, publ, hostname, &hostnamel);
    printf("our hostname is (%d) %s", hostnamel, hostname);

    char fhost[256];
    const char *_host_filename = "hostname";
    snprintf(fhost, 255, "%s%s%s", PATHPREFIX, PATHPREFIXTORHIDDEN, _host_filename);
    fp = fopen(fhost, "w");
    if (fp == NULL) {
        printf("error no write access");
        return -1;
    }
    fwrite(hostname, 1, hostnamel,  fp);
    fputs("\n", fp);
    fclose(fp);

    EVP_PKEY_free(keypair);
    return 0;
}

int stringtohash(const unsigned char *to_hash, int to_hashl,
                 unsigned char *md_value, unsigned int *md_len) {
    EVP_MD_CTX *mdctx;
    const EVP_MD *md = EVP_sha3_256();
    mdctx = EVP_MD_CTX_new();
    EVP_DigestInit_ex(mdctx, md, NULL);
    EVP_DigestUpdate(mdctx, to_hash, to_hashl);
    EVP_DigestFinal_ex(mdctx, md_value, md_len);
    EVP_MD_CTX_free(mdctx);
    return 0;
}

int dht_sign_message(const unsigned char *priv, int privl,
                     const unsigned char *message, int messagel,
                     unsigned char *signe, int *signel) {
    int r;
    size_t ssignel;
    EVP_PKEY *keypair = EVP_PKEY_new_raw_private_key(EVP_PKEY_ED25519, NULL, priv, privl);
    if (keypair == NULL)
        printf("dht_sign_message private key NULL");
    EVP_PKEY_CTX *pctx = NULL;
    EVP_MD_CTX *ctx = EVP_MD_CTX_new();
    EVP_DigestSignInit(ctx, &pctx, NULL, NULL, keypair);
    r = EVP_DigestSign(ctx, signe, &ssignel, message, messagel);
    *signel = (int) ssignel;
    EVP_PKEY_free(keypair);
    EVP_MD_CTX_free(ctx);
    return r;
}

int dht_verify_message(const unsigned char *pub, int publ,
                       const unsigned char *message, int messagel,
                       const unsigned char *signe, int signel) {
    int r;
    EVP_PKEY *keypair = EVP_PKEY_new_raw_public_key(EVP_PKEY_ED25519, NULL, pub, publ);
    if (keypair == NULL)
        printf("dht_verify_message public key NULL");
    EVP_PKEY_CTX *pctx = NULL;
    EVP_MD_CTX *ctx = EVP_MD_CTX_new();
    EVP_DigestVerifyInit(ctx, &pctx, NULL, NULL, keypair);
    r = EVP_DigestVerify(ctx, signe, signel, message, messagel);
    EVP_PKEY_free(keypair);
    EVP_MD_CTX_free(ctx);
    return r;
}

int verifyhiddendir() {
    FILE *fp;
    int i, j;
    int r;
    int rc;

    rc = mkdir(PATHPREFIX, 0700);
    if (rc == -1 && errno != 17)
      printf("directory %s %s", PATHPREFIX, strerror(errno));
    else if (rc != -1)
      printf("created directory %s", PATHPREFIX);

    char cachedir[256];
    snprintf(cachedir, 255, "%s%s", PATHPREFIX, PATHPREFIXCACHE);
    rc = mkdir(cachedir, 0700);
    if (rc == -1 && errno != 17)
      printf("directory %s %s", cachedir, strerror(errno));
    else if (rc != -1)
      printf("created directory %s", cachedir);

    char tordir[256];
    snprintf(tordir, 255, "%s%s", PATHPREFIX, PATHPREFIXTOR);
    rc = mkdir(tordir, 0700);
    if (rc == -1 && errno != 17)
      printf("directory %s %s", tordir, strerror(errno));
    else if (rc != -1)
      printf("created directory %s", tordir);

    char tordatadir[256];
    snprintf(tordatadir, 255, "%s%s", PATHPREFIX, PATHPREFIXTORDATA);
    rc = mkdir(tordatadir, 0700);
    if (rc == -1 && errno != 17)
      printf("directory %s %s", tordatadir, strerror(errno));
    else if (rc != -1)
      printf("created directory %s", tordatadir);

    char torhiddendir[256];
    snprintf(torhiddendir, 255, "%s%s", PATHPREFIX, PATHPREFIXTORHIDDEN);
    rc = mkdir(torhiddendir, 0700);
    if (rc == -1 && errno != 17)
      printf("directory %s %s", torhiddendir, strerror(errno));
    else if (rc != -1)
      printf("created directory %s", torhiddendir);

    char fpriv[256];
    snprintf(fpriv, 255, "%s%s%s", PATHPREFIX, PATHPREFIXTORHIDDEN, "private.raw");
    fp = fopen(fpriv, "rb");
    if (fp == NULL) {
        printf("generate new private key %s", fpriv);
        dht_new_private(PRIV, &PRIVL);
        //dumpbufx("PRIV", PRIV, PRIVL);
        rc = dht_gen_hidden_service_files(PRIV, &PRIVL);
        if (rc < 0)
          return rc;
        return 1;
    } else
        fclose(fp);

    fp = fopen(fpriv, "rb");
    if (fp != NULL) {
        r = fread(PRIV, 1, PRIVL, fp);
        fclose(fp);
        printf("private key read %s %d", fpriv, r);
        PRIVL = r;
    } else {
      printf("error still no private key %s", fpriv);
    }
    //dumpbufx("priv", PRIV, PRIVL);

    char fpub[256];
    snprintf(fpub, 255, "%s%s%s", PATHPREFIX, PATHPREFIXTORHIDDEN, "public.raw");
    fp = fopen(fpub, "rb");
    if (fp == NULL) {
        printf("generate tor service files %s", fpub);
        dumpbufx("priv2", PRIV, PRIVL);
        dht_gen_hidden_service_files(PRIV, &PRIVL);
        if (rc < 0)
          return rc;
        return 2;
    } else
        fclose(fp);

    printf("private key loaded %s", fpriv);
    return 0;
}

/*
char out[1024];
int outl = 1024;
const unsigned char in[] = "\x00";
int inl = sizeof(in) - 1; // inclus le string \x00 à la fin
dumpbufx("in", in, inl);
dbase32_encode(in, inl, out, &outl);
dump_buf("out", out, outl);
return 0;
*/

/*
// sign message
const unsigned char message[] = "Hello, this is a very long message.\x00\x00\x00\xff";
int messagel = sizeof(message);
printf("message len %d", messagel);
unsigned char signe[256];
int signel = 256;

snprintf(fname, sizeof(fname), "%s%s%s", PATHPREFIX, PATHPREFIXTORHIDDEN, "private.raw");
fp = fopen(fname, "rb");
if (fp != NULL) {
    unsigned char priv[256];
    int privl = 256;

    r = fread(priv, 1, privl, fp);
    fclose(fp);
    printf("private.raw read %d", r);
    privl = r;

    r = dht_sign_message(priv, privl, message, messagel, signe, signel);
    if (r == 1)
        printf("signed %d", r);
    else
        printf("error sign failed %d", r);
}

// verify message
snprintf(fname, sizeof(fname), "%s%s%s", PATHPREFIX, PATHPREFIXTORHIDDEN, "public.raw");
fp = fopen(fname, "rb");
if (fp != NULL) {

    unsigned char pub[256];
    int publ = 256;

    r = fread(pub, 1, publ, fp);
    fclose(fp);
    printf("public.raw read %d", r);
    publ = r;

    r = dht_verify_message(pub, publ, message, messagel, signe, signel);
    if (r == 1)
        printf("verified %d", r);
    else
        printf("error verify failed %d", r);
}
*/

/*
unsigned char id[UUIDL];
int fd = open("/dev/urandom", O_RDONLY);
if (fd < 0) {
    printf("error open urandom");
    return -1;
}
rc = read(fd, id, UUIDL);
if (rc < 0) {
    printf("error read urandom");
    return -1;
}
close(fd);
printf("generated new uuid : ");
for (i = 0; i < UUIDL; i++) {
    printf("%02x", id[i]);
}
printf("");
*/
