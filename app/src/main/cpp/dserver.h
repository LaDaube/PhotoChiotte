
int server(int server_fd_id, struct sockaddr_in address, int addrlen) {
    int i, j, rc;
    /*
    struct timeval timeout;
    timeout.tv_sec = 6;
    timeout.tv_usec = 0;
    struct timeval timeoutreset;
    timeoutreset.tv_sec = 0;
    timeoutreset.tv_usec = 0;
    if (setsockopt (server_fd[server_fd_id], SOL_SOCKET, SO_RCVTIMEO, (char *)&timeout, sizeof(timeout)) < 0) return -8;
    if (setsockopt (server_fd[server_fd_id], SOL_SOCKET, SO_SNDTIMEO, (char *)&timeoutreset, sizeof(timeoutreset)) < 0) return -8;
    */
    printf("  [ ]  server ready and listening");
    while (!FINISHED) {

        printf("  [ ]  server idle waiting for command");
        pthread_mutex_lock(&servermutex);
        int new_socket = -1;
        if (!FINISHED)
            new_socket = accept(server_fd[server_fd_id], (struct sockaddr *) &address, (socklen_t *) &addrlen);
        pthread_mutex_unlock(&servermutex);
        if (new_socket < 0) {
            printf("  [ ]  accept failed %d", errno);
            //close(new_socket);
            continue;
        }
        printf("  [ ]  %d.%d.%d.%d  %d accepted", (address.sin_addr.s_addr >> 0) & 0xff, (address.sin_addr.s_addr >> 8) & 0xff,
               (address.sin_addr.s_addr >> 16) & 0xff, (address.sin_addr.s_addr >> 24) & 0xff,
               address.sin_port);



        struct timeval rcvtimeout;
        rcvtimeout.tv_sec = SRVRCVTIMEOUT;
        rcvtimeout.tv_usec = 0;
        rc = setsockopt(new_socket, SOL_SOCKET, SO_RCVTIMEO, (char *) &rcvtimeout, sizeof(rcvtimeout));
        if (rc < 0) {
            printf("  [c]  ERROR TCPclient SO_RCVTIMEO");
            close(new_socket);
            return -1;
        }
        struct timeval sndtimeout;
        sndtimeout.tv_sec = SRVSNDTIMEOUT;
        sndtimeout.tv_usec = 0;
        rc = setsockopt(new_socket, SOL_SOCKET, SO_SNDTIMEO, (char *) &sndtimeout, sizeof(sndtimeout));
        if (rc < 0) {
            printf("  [c]  ERROR TCPclient SO_SNDTIMEO");
            close(new_socket);
            return -1;
        }



        printf("  [ ]  read what the client asks");
        int bufal = MAXMSGSIZE;
        unsigned char *bufa = (unsigned char *) malloc(sizeof(unsigned char) * bufal);

        //bufal = recv(new_socket, bufa, bufal, 0);
        int chunksize = CHUNKSIZE;
        int bytesRead = 0;
        int readThisTime = 1;
        int messagesize = -1;
        int offset = 0;
        unsigned char type = 0;
        while (readThisTime > 0) {
            readThisTime = recv(new_socket, bufa + bytesRead, chunksize, 0);
            if (readThisTime != -1)
                bytesRead += readThisTime;
            if (readThisTime == -1 && errno == EAGAIN) {
                printf("  [ ]  recv socket timeout EAGAIN %d total %d", readThisTime, bytesRead);
                bufal = bytesRead;
            } else if (readThisTime == -1) {
                printf("  [ ]  recv failed readThisTime %d", errno);
                bufal = 0;
                break;
            } else {
                //printf("  [ ]  recv %d total %d", readThisTime, bytesRead);
                bufal = bytesRead;
                if (messagesize == -1) {
                    if (bytesRead < 8) {
                        printf("  [ ]  recv too few bytes");
                    } else {
                        messagesize = read_header(bufa, bytesRead, &offset, &type);
                        if (messagesize == 0) {
                            //0500000100000000000 00000008700000087d3a1
                            char o[bytesRead * 2];
                            int l = 0;
                            for (i = 0 ; i < bytesRead ; i++)
                                l += snprintf(o + l, 256, "%02x", bufa[i]);
                            printf("  [ ]  rcv error bailing out %s", o);
                            bufal = 0;
                            break;
                        } else {
                            printf("  [ ]  recv message will have %d bytes", messagesize);
                        }
                    }
                }
                if (bytesRead >= messagesize && messagesize >= 0) {
                    printf("  [ ]  recv we have the full message");
                    break;
                }
            }
        }

        struct AskResponse ask;
        if (bufal > 0 && type == TYPE_BENCODED_XZPE) {
            //dumpbufx("  [ ]  ", bufa, bufal);
            unsigned char *bufatmp;
            int bufatmpl = devigenerize(OURPUBKEY, HASHL, offset, bufa, bytesRead, &bufatmp);
            //dumpbufx("  [ ]  ", bufatmp, bufatmpl);
            rc = parsebencodede(bufatmp, bufatmpl, &ask);
            free((unsigned char *) bufatmp);
        } else if (bufal > 0 && type == TYPE_COLLECTION_XZPE) {
            //rc = parsecollectione(bufa, bufal, destinationpath);
        } else {
            bufal = 0;
        }
        free((unsigned char *) bufa);
        if (bufal <= 0 || rc < 0) {
            printf("  [ ]  recv parse failed %d", errno);
            close(new_socket);
            continue;
        }




        // register the client as a new node
        struct NodeInfo client;
        for (i = 0 ; i < ask.idl ; i++)
            client.pub[i] = ask.id[i];
        client.publ = ask.idl;
        client.porti = ask.porti;
        client.portf = ask.portf;

        // prepare answer to the client
        struct AskResponse resp;
        resp.t = (unsigned char *) malloc(sizeof(unsigned char) * ask.tl);
        for (i = 0 ; i < ask.tl; i++)
            resp.t[i] = ask.t[i];
        resp.tl = ask.tl;
        resp.v = APPVERSION;
        resp.vl = APPVERSIONL;
        pthread_mutex_lock(&mutexclientinfo);
        resp.id = clientinfo[OURNODE].pub;
        resp.idl = clientinfo[OURNODE].publ;
        resp.porti = clientinfo[OURNODE].porti;
        resp.portf = clientinfo[OURNODE].portf;
        if (ask.query) {
            if (ask.q == dic_find_node) {
                printf("  [ ]  query find_node");
            } else if (ask.q == dic_get_peers) {
                pthread_mutex_lock(&infohashmutex);
                int found = find_hash(ask.target, ask.targetl);
                if (found >= 0) {
                    if (hashinfo[found].peerlistl > 0) {
                        printf("  [ ]  query get_peers hash found send our %d peers", hashinfo[found].peerlistl);
                        resp.target = (unsigned char *) malloc(sizeof(unsigned char) * ask.targetl);
                        for (i = 0 ; i < ask.targetl; i++)
                            resp.target[i] = ask.target[i];
                        resp.targetl = ask.targetl;
                        int bl = HASHL + 2 + 2;
                        resp.peers = (unsigned char *) malloc(sizeof(unsigned char) * bl * hashinfo[found].peerlistl);
                        for (i = 0 ; i < hashinfo[found].peerlistl ; i++) {
                            for (j = 0; j < bl; j++)
                                resp.peers[i * bl + j] = hashinfo[found].peerlist[i].pub[j];
                            resp.peersl += bl;
                        }
                    } else {
                        printf("  [ ]  query get_peers hash found but no peer");
                    }
                } else {
                    printf("  [ ]  query get_peers no matching hash found");
                }
                pthread_mutex_unlock(&infohashmutex);
            } else if (ask.q == dic_announce_peer) {
                printf("  [ ]  query announce_peer");
                resp.query = false;
                pthread_mutex_lock(&infohashmutex);
                int found = find_hash(ask.target, ask.targetl);
                if (found < 0) {
                    struct HashInfo hi;
                    hi.keywordshashl = ask.targetl;
                    for (i = 0 ; i < HASHL ; i++) {
                        hi.keywordshash[i] = ask.target[i];
                    }
                    found = add_hashinfo(hi);
                    printf("  [ ]  hash not found we create a new one %d", found);
                }
                if (found < 0) {
                    printf("  [ ]  we were unable to add the target hash");
                } else {
                    if (hashinfo[found].filenamel > 0) {
                        printf("  [ ]  we are also subscribed to this collection");
                    }
                    if (hashinfo[found].peerlistl > 0) {
                        printf("  [ ]  we already have %d peers for this hash", hashinfo[found].peerlistl);
                        free((struct PeerInfo *) hashinfo[found].peerlist);
                        hashinfo[found].peerlistl = 0;
                    }
                    hashinfo[found].peerlistl = 1;
                    hashinfo[found].peerlist = (struct PeerInfo *) malloc(sizeof(struct PeerInfo) * hashinfo[found].peerlistl);
                    i = 0;
                    int bl = HASHL + 2 + 2;
                    for (j = 0 ; j < HASHL ; j++)
                        hashinfo[found].peerlist[i].pub[j] = ask.id[j];
                    hashinfo[found].peerlist[i].pub[i * bl + HASHL + 0] = (ask.porti >> 8) & 0xff;
                    hashinfo[found].peerlist[i].pub[i * bl + HASHL + 1] = (ask.porti >> 0) & 0xff;
                    hashinfo[found].peerlist[i].pub[i * bl + HASHL + 2] = (ask.porti >> 8) & 0xff;
                    hashinfo[found].peerlist[i].pub[i * bl + HASHL + 3] = (ask.porti >> 0) & 0xff;
                    printf("  [ ]  we added %d peers for this hash", hashinfo[found].peerlistl);
                }
                pthread_mutex_unlock(&infohashmutex);
            } else if (ask.q == dic_download) {
                printf("  [ ]  query download");
            } else if (ask.q == dic_ping) {
                printf("  [ ]  query ping");
                resp.query = false;
            } else {
                printf("  [ ]  query unknown");
            }
            if (ask.q == dic_find_node || ask.q == dic_get_peers) {
                resp.query = false;
                unsigned int *found;
                int foundl = 0;
                find_closest_nodes(ask.target, ask.targetl, MAXCLIENTPERRANGE, &found, &foundl);
                if (foundl > 0) {
                    printf("  [ ]  found nodes len %d closest %d porti %d", foundl, found[0], clientinfo[found[0]].porti);
                    int bl = HASHL + 2 + 2;
                    resp.nodes = (unsigned char *) malloc(sizeof(unsigned char) * bl * foundl);
                    resp.nodesl = 0;
                    for (i = 0; i < foundl; i++) {
                        for (j = 0; j < HASHL; j++)
                            resp.nodes[i * bl + j] = clientinfo[found[i]].pub[j];
                        // network byte order big indian
                        resp.nodes[i * bl + HASHL + 0] = (clientinfo[found[i]].porti >> 8) & 0xff;
                        resp.nodes[i * bl + HASHL + 1] = (clientinfo[found[i]].porti >> 0) & 0xff;
                        resp.nodes[i * bl + HASHL + 2] = (clientinfo[found[i]].portf >> 8) & 0xff;
                        resp.nodes[i * bl + HASHL + 3] = (clientinfo[found[i]].portf >> 0) & 0xff;
                        resp.nodesl += bl;
                    }
                    free((unsigned int *) found);
                } else {
                    printf("  [ ]  no closest node to target found");
                }
            }
        } else {
            if (resp.q == dic_find_node)
                printf("  [ ]  response find_node");
            else
                printf("  [ ]  response unknown");
        }
        pthread_mutex_unlock(&mutexclientinfo);





        //int bufrl = MAXMSGSIZE;
        //unsigned char *bufr = (unsigned char *) malloc(sizeof(unsigned char) * bufrl);
        int bufrtmpl = MAXMSGSIZE;
        unsigned char *bufrtmp = (unsigned char *) malloc(sizeof(unsigned char) * bufrtmpl);
        if (ask.q == dic_download) {
            char *sourcepath = NULL;
            pthread_mutex_lock(&infohashmutex);
            int found = find_hash(ask.target, ask.targetl);
            if (found < 0) {
                printf("  [ ]  query download target not found");
            } else {
                if (hashinfo[found].filenamel <= 0) {
                    printf("  [ ] query download target file not found");
                } else {
                    sourcepath = hashinfo[found].filename;
                }
            }
            //querycollectione(bufr, &bufrl, sourcepath);
            querycollectione(bufrtmp, &bufrtmpl, sourcepath);
            pthread_mutex_unlock(&infohashmutex);
            offset = header_size(TYPE_COLLECTION_XZPE);
        } else {
            //querybencodede(bufr, &bufrl, resp);
            querybencodede(bufrtmp, &bufrtmpl, resp);
            offset = header_size(TYPE_BENCODED_XZPE);
        }
        unsigned char *bufr;
        if (ask.q == dic_download)
            offset = header_size(TYPE_COLLECTION_XZPE);
        else
            offset = header_size(TYPE_BENCODED_XZPE);
        int bufrl = envigenerize(ask.id, HASHL, offset, bufrtmp, bufrtmpl, &bufr);
        if (ask.q == dic_download)
            offset = write_header(bufr, bufrl, TYPE_COLLECTION_XZPE);
        else
            offset = write_header(bufr, bufrl, TYPE_BENCODED_XZPE);
        free((unsigned char *) bufrtmp);


        if (ask.tl > 0)
            free((unsigned char *) ask.t);
        if (ask.vl > 0)
            free((unsigned char *) ask.v);
        if (ask.idl > 0)
            free((unsigned char *) ask.id);
        if (ask.targetl > 0)
            free((unsigned char *) ask.target);

        if (resp.tl > 0)
            free((unsigned char *) resp.t);
        if (resp.targetl > 0)
            free((unsigned char *) resp.target);
        if (resp.nodesl > 0)
            free((unsigned char *) resp.nodes);
        if (resp.peersl > 0)
            free((unsigned char *) resp.peers);

        //bufrl = send(new_socket, bufr, bufrl, MSG_NOSIGNAL);
        chunksize = CHUNKSIZE;
        int bytesWritten = 0;
        int bytesToWrite = bufrl;
        while (bytesWritten != bytesToWrite) {
            int writtenThisTime;
            if (bytesToWrite - bytesWritten < chunksize)
                chunksize = bytesToWrite - bytesWritten;
            writtenThisTime = send(new_socket, bufr + bytesWritten, chunksize, MSG_NOSIGNAL);
            bytesWritten += writtenThisTime;
            if (writtenThisTime == -1) {
                printf("  [ ]  send failed writtenThisTime %d", errno);
                bufrl = -1;
                break;
            } else {
                printf("  [ ]  sent %d total %d", writtenThisTime, bytesWritten);
            }
        }

        free((unsigned char *) bufr);
        if (bufrl < 0) {
            printf("  [ ]  send failed %d", errno);
            close(new_socket);
            continue;
        }



        pthread_mutex_lock(&mutexclientinfo);
        int foundat = find_client(client.pub, client.publ);
        if (foundat == -1) {
            add_client(client);
        } else {
            time_t currenttime = time(NULL);
            clientinfo[foundat].commsuccess++;
            clientinfo[foundat].timelastsuccessfulcomm = currenttime;
            clientinfo[foundat].commfailed = 0;
        }
        pthread_mutex_unlock(&mutexclientinfo);

        close(new_socket);
    }
    printf("  [ ]  server exited");

    return 0;
}
