#!/system/bin/sh

packagename="${1}"
rule="${2}"

if [ "${rule}" = "keep" ]
then
  pm uninstall -k "${packagename}"
  echo "uninstall -k package ${packagename} : rc=$?"
  pm uninstall -k --user 0 "${packagename}"
  echo "uninstall -k --user 0 package ${packagename} : rc=$?"
elif [ "${rule}" = "update" ]
then
  pm uninstall "${packagename}"
  echo "uninstall package ${packagename} : rc=$?"
elif [ "${rule}" = "full" ]
then
  pm uninstall "${packagename}"
  echo "uninstall package ${packagename} : rc=$?"
  pm uninstall --user 0 "${packagename}"
  echo "uninstall --user 0 package ${packagename} : rc=$?"
fi


