#!/system/bin/sh

packagename="${1}"
service="${2}"
permission="${3}"

if [ "${#permission}" -gt 0 ]
then
    pm revoke "${packagename}" "${permission}"
    echo "revoke permission ${packagename} ${permission} : rc=$?"
elif [ "${#service}" -gt 0 ]
then
    pm disable "${packagename}/${service}"
    echo "disable service ${packagename}/${service} : rc=$?"
else
    pm disable "${packagename}"
    echo "disable package : rc=$?"
fi



