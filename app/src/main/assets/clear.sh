#!/system/bin/sh

packagename="${1}"

pm clear "${packagename}"
echo "clear package ${packagename} : rc=$?"
pm clear --user 0 "${packagename}"
echo "clear --user 0 package ${packagename} : rc=$?"

