#!/system/bin/sh

apkname="${1}"

mkdir -p /data/local/tmp/

echo "${apkname}" | grep "^http"
if [ $? -eq 0 ]
then
  curl -s "${apkname}" -o /data/local/tmp/tmp.apk
  echo "curl ${apkname} : rc=$?"
else
  cp -f "${apkname}" /data/local/tmp/tmp.apk
  echo "cp ${apkname} /data/local/tmp/ : rc=$?"
fi

if [ -f /data/local/tmp/tmp.apk ]
then
  pm install /data/local/tmp/tmp.apk
  echo "install package ${apkname} : rc=$?"
  rm -f /data/local/tmp/tmp.apk
fi


