#!/system/bin/sh

packagename="${1}"
sourcedir="${2}"
magiskmodule="/data/adb/modules/photochiotte"


if [ -d "/data/adb/modules/" ]
then

  apkfolder="${sourcedir%/*}"

  basefolder="${sourcedir#*/}"
  basefolder="${basefolder%%/*}"

  if [ "${basefolder}" = "system" ]
  then
    mkdir -p "${magiskmodule}${apkfolder}"
    touch "${magiskmodule}${sourcedir}"
    chmod -R 644 "${magiskmodule}${apkfolder}"
    ls -l "${magiskmodule}${apkfolder}"
  else
    echo "basefolder ${basefolder}"
  fi

fi

