#!/system/bin/sh

magiskmodule="/data/adb/modules/photochiotte/"

if [ -d "/data/adb/modules/" ]
then

  for d in $(find "${magiskmodule}" -type d)
  do
    rmdir "${d}" &> /dev/null
    if [ $? -eq 0 ]
    then
      echo "removed ${d}"
    fi
  done

fi




