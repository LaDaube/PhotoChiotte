package la.daube.photochiotte;

import android.content.Context;
import android.content.Intent;
import android.text.InputType;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class IntentRescan {
  private final static String TAG = "YYYisc";

  public IntentRescan(Context context, Intent intent, Gallery model, RelativeLayout mainlayout, EditText input, Button inputvalidate){
    model.deactivateactivitykeydown = true;
    final int id = intent.getIntExtra("id", -1);

    input.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_WEB_EDIT_TEXT);
    input.setX(0);
    input.setY(100);
    input.setBackgroundColor(Gallery.CouleurTresSombre);
    input.setHint("/storage/emulated/0/Download ... DCIM ... or click a folder name");
    input.setHintTextColor(Gallery.CouleurClaire);
    input.setTextColor(Gallery.CouleurTresClaire);
    input.setFocusableInTouchMode(true);
    input.setImeOptions(EditorInfo.IME_ACTION_DONE);
    if (0 <= id && id < model.surf.size()) {
      if (model.surf.get(id).basisfolderclicked != null) {
        input.setText(model.surf.get(id).basisfolderclicked);
      }
    }
    
    mainlayout.addView(input);
    ViewGroup.LayoutParams parms = input.getLayoutParams();
    parms.width = model.bigScreenWidth;
    input.setLayoutParams(parms);
    mainlayout.requestLayout();
  
    input.setOnFocusChangeListener(new View.OnFocusChangeListener() {
      @Override
      public void onFocusChange(View view, boolean focused) {
        InputMethodManager keyboard = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        if (focused)
          keyboard.showSoftInput(input, 0);
        else
          keyboard.hideSoftInputFromWindow(input.getWindowToken(), 0);
      }
    });
  
    input.setOnEditorActionListener(new EditText.OnEditorActionListener() {
      @Override
      public boolean onEditorAction(TextView textView, int actionId, KeyEvent keyEvent) {
        if (actionId == EditorInfo.IME_ACTION_DONE || actionId == EditorInfo.IME_ACTION_UNSPECIFIED) {
          llog.d(TAG, "actionId " + actionId);
          String searchthis = input.getText().toString();
          if (searchthis.length() > 0) {
            try {
              model.commandethreaddatabase.put(new String[]{"addautorescan", searchthis, String.valueOf(id)});
            } catch (InterruptedException e) {
              e.printStackTrace();
            }
          }
          input.clearFocus();
          mainlayout.removeView(input);
          mainlayout.requestLayout();
          model.deactivateactivitykeydown = false;
          return true;
        } else {
          llog.d(TAG, "unused actionId " + actionId);
        }
        return false;
      }
    });
    
    input.requestFocus();
  }
}
