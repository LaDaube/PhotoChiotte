package la.daube.photochiotte;

import android.graphics.Bitmap;
import android.view.Surface;

import java.util.concurrent.locks.ReentrantLock;

public class Libextractor {
    private final static String TAG = "YYYlex";
    static {
        System.loadLibrary("thumb");
    }

    public Libextractor(){
    }

    private final ReentrantLock lock = new ReentrantLock();
    public Bitmap decodergb(String address, byte[] buffer, int bufferl){
      Bitmap bitmap = null;
      lock.lock();
      int rc = getRgb(address, buffer, bufferl);
      if (rc < 0) {
          llog.d(TAG, "error decodergb " + address);
      } else {
        int width = Libextractor.getwidth();
        int height = Libextractor.getheight();
        int stride = Libextractor.getstride();
        int size = height * stride;
        if (size > 0) {
          int intstride = stride / 4;
          int[] arr = Libextractor.getintarray();
          /*llog.d(TAG, "Libextractor.getrgb " + address + " " + width + " " + height + " "
                  + stride + " buffer alloc bytes " + size + " " + intstride + " "
                  + String.format("%08x %08x", arr[0], arr[1])
          );*/
          bitmap = Bitmap.createBitmap(arr, 0, intstride, width, height, Bitmap.Config.ARGB_8888);
        }
      }
      lock.unlock();
      return bitmap;
    }
    public void renderrgb(String address, String filter, Surface surface, int top, int left, int bottom ,int right){
        lock.lock();
        int rc = filterRgb(address, null, 0, filter);
        long tf = System.currentTimeMillis();
        long ti = System.currentTimeMillis();
        rc = renderRgb(surface, top, left, bottom, right);
        tf = System.currentTimeMillis();
        llog.d(TAG, "renderNDK " + (tf - ti));
        if (rc < 0) {
            llog.d(TAG, "error renderRgb");
        } else {
          int width = Libextractor.getwidth();
          int height = Libextractor.getheight();
          int stride = Libextractor.getstride();
          int size = height * stride;
          if (size > 0) {
            int intstride = stride / 4;
            int[] arr = Libextractor.getintarray();
          }
        }
        lock.unlock();
    }
    public Bitmap filterrgb(String address, byte[] buffer, int bufferl, String filter){
      Bitmap bitmap = null;
      lock.lock();
      int rc = filterRgb(address, buffer, bufferl, filter);
      if (rc < 0) {
          llog.d(TAG, "error filterrgb " + address + " " + filter);
      } else {
        int width = Libextractor.getwidth();
        int height = Libextractor.getheight();
        int stride = Libextractor.getstride();
        int size = height * stride;
        if (size > 0) {
          int intstride = stride / 4;
          int[] arr = Libextractor.getintarray();
          bitmap = Bitmap.createBitmap(arr, 0, intstride, width, height, Bitmap.Config.ARGB_8888);
        }
      }
      lock.unlock();
      return bitmap;
    }
    public Bitmap blurrgb(String address, int newwidth, int newheight) {
      Bitmap bitmap = null;
      lock.lock();
      int rc = blurRgb(address, newwidth, newheight);
      if (rc < 0) {
          llog.d(TAG, "error blurrgb " + address + " " + newwidth + "x" + newheight);
      } else {
        int width = Libextractor.getwidth();
        int height = Libextractor.getheight();
        int stride = Libextractor.getstride();
        int size = height * stride;
        if (size > 0) {
          int intstride = stride / 4;
          int[] arr = Libextractor.getintarray();
          bitmap = Bitmap.createBitmap(arr, 0, intstride, width, height, Bitmap.Config.ARGB_8888);
        }
      }
      lock.unlock();
      return bitmap;
    }

    public static int extract(String video, String destname, int mwidth, int mheight, boolean isapicture, String additionalmetadata) {
      //llog.d(TAG, " x " + video);
      return run(video, destname, mwidth, mheight, isapicture, Gallery.maxpicturecount, additionalmetadata);
    }
  private static int getwidth() {
        return getWidth();
    }
  private static int getheight() {
        return getHeight();
    }
  private static int getstride() {
        return getStride();
    }
  private static int[] getintarray() {
        return getIntarray();
    }
  private static int getRgb(String fname, byte[] buffer, int bufferl) {
        return getRGB(fname, buffer, bufferl);
    }
  private static int renderRgb(Surface surface, int top, int left, int bottom, int right) {
        return renderRGB(surface, top, left, bottom, right);
    }
  private static int filterRgb(String fname, byte[] buffer, int bufferl, String filter) {
        return filterRGB(fname, buffer, bufferl, filter);
    }
  private static int blurRgb(String fname, int newwidth, int newheight) {
        return blurRGB(fname, newwidth, newheight);
    }

    private static native int run(String fname, String destname, int width, int height, boolean isapicture, long maxpicturecount, String additionalmetadata);

    private static native int getWidth();
    private static native int getHeight();
    private static native int getStride();
    private static native int[] getIntarray();
    private static native int getRGB(String fname, byte[] buffer, int bufferl);
    private static native int renderRGB(Surface surface, int top, int left, int bottom, int right);
    private static native int filterRGB(String fname, byte[] buffer, int bufferl, String filter);
    private static native int blurRGB(String fname, int newwidth, int newheight);

}
