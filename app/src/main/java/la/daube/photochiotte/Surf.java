package la.daube.photochiotte;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;

import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.locks.ReentrantLock;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

class Surf {
  private static final String TAG = "YYYsrf";
  public static final int FRAGMENT_TYPE_NONE         = 0;
  public static final int FRAGMENT_TYPE_BROWSER      = 1;
  public static final int FRAGMENT_TYPE_DRAWING      = 2;
  public static final int FRAGMENT_TYPE_VIDEO        = 3;
  public static final int FRAGMENT_TYPE_PUZZLE       = 4;
  public static final float PERCENTOFSCREENIGNORENAVIGATIONBARTOP = 0.015f;
  public static final float PERCENTOFSCREENIGNORENAVIGATIONBARBOTTOM = 1.0f - PERCENTOFSCREENIGNORENAVIGATIONBARTOP;
  public static final float PERCENTOFSCREENIGNORENAVIGATIONBARRIGHTBACK = 1.0f - PERCENTOFSCREENIGNORENAVIGATIONBARTOP;
  public int fragmenttype = FRAGMENT_TYPE_BROWSER;
  public boolean idle = true;

  public volatile long filmstripreftime = 1000L;

  public int surftagi = 0;
  public int myid = -1;
  public int myx = 0;
  public int myy = 0;
  public int mywidth = 0;
  public int myheight = 0;
  public int myscreenwidth = 0;
  public int myscreenheight = 0;
  public String filter = null;
  public String[] filterlist = new String[]{};
  public boolean[] filteractive = new boolean[]{};
  public String[] filterprint = new String[]{};
  public String[] filterinfo = new String[]{};
  public int filterlastselected = 0;
  public boolean splitleft = false;
  public boolean splittop = false;
  public boolean splitright = false;
  public boolean splitbottom = false;
  public View fragmentView = null;
  public volatile SurfaceView drawingSurfaceView = null;
  public volatile SurfaceView mpvSurfaceView = null;
  public volatile SurfaceView browserSurfaceView = null;
  public volatile SurfaceHolder mpvSurfaceHolder = null;
  public volatile SurfaceHolder drawingSurfaceHolder = null;
  public volatile SurfaceHolder browserSurfaceHolder = null;
  public volatile boolean surfaceIsCurrentlyDrawing = false;
  public volatile boolean mysurfacestopdrawing = true;
  public volatile boolean mysurfaceisdestroyed = false;
  public volatile int ScreenWidth = 0;
  public volatile int ScreenHeight = 0;
  public volatile int keypressdown = 0;
  
  public volatile int GraphWidth = 0;
  public volatile int SettingsWidth = 0;
  public final Paint paintdither = new Paint();

  public Bitmap touslesdossierssplitviewbitmap = null;
  public Bitmap drawtoptempbitmap = null;
  public Canvas drawtoptempcanvas = null;
  public Bitmap drawpilebitmap = null;
  public Canvas drawpilecanvas = null;


  public volatile long lastsplitviewredraw = 0L;
  public volatile boolean redrawsplitviewbitmap = true;
  public volatile boolean drawblackscreennext = false;
  public volatile boolean cachefromlefttoright = true;

  public volatile int justnexted = 0;
  public volatile float lastposx = -1.0f;
  public volatile float lastposy = -1.0f;

  public volatile int zoomsetbyuserto = Gallery.ZoomKeep;
  public volatile String zoomsetbyusertoaddress = "dummy";
  public volatile float zoomsetbyusertobscale = 1.0f;
  public volatile float zoomsetbyusertobpx = 0.0f;
  public volatile float zoomsetbyusertobpy = 0.0f;
  public volatile int zoommode = Gallery.ZoomKeep;
  public volatile int zoomFitMode = Gallery.ZoomFitStrict;
  public volatile int zoomComicMode = Gallery.ZoomComicTopLeft;
  public volatile boolean putbigpictureinmemory = false;
  public volatile boolean centeronscreen = false;
  public volatile boolean initial = true;

  public int mediaIndex = -1;
  public int ordnerIndex = -1;
  public String ordnerIndexAddress = "";
  public String mediaIndexAddress = "";
  public String fichierprecedent = "f";

  public volatile float bpx = 0.0f;
  public volatile float bpy = 0.0f;
  public volatile float bscale = 1.0f;
  public volatile int imagewidth = 300;
  public volatile int imageheight = 300;
  public volatile float bpxmax = 1.0f;
  public volatile float bpymax = 1.0f;

  public volatile int bigimagecurrentlydisplayed = 0;
  public volatile int thumbcurrentlydisplayed = 0;
  public volatile long diaporamalastchange = 0;
  public volatile long filmstriplastchange = 0;
  public volatile float diaporamadeltatime = 0.0f;
  
  public volatile float cursorx = -1.0f;
  public volatile float cursory = -1.0f;
  public volatile String basisfolderclicked = null;

  public volatile boolean showthumbnails = true;
  public volatile boolean OptionMenuShown = false;
  public volatile boolean showfoldernames = false;
  public volatile boolean showfoldernamesforce = false;
  public volatile boolean drawgraph = false;

  public volatile boolean optiondiaporamaactive = false;
  public final static int DIAPORAMA_LINEAR = 0;
  public final static int DIAPORAMA_RANDOM_FOLDER = 1;
  public final static int DIAPORAMA_RANDOM = 2;
  public volatile int optiondiaporamalinear = DIAPORAMA_LINEAR;

  public static final float[] deltabright = new float[]{0.001f, 0.01f, 0.03f, 0.1f, 1.0f, 10.0f};
  public volatile int deltabrighti = 2;
  public volatile float brightness = 0.0f;
  public volatile float contrast = 1.0f;
  public volatile float saturation = 1.0f;
  public volatile float gamma = 1.0f;
  public volatile float gamma_weight = 1.0f;

  public volatile boolean paintwantsdither = false;

  public volatile String drawingcurrfichier = "";
  public volatile ArrayList<Couche> listecouche = new ArrayList<>();
  public volatile ArrayList<Dessin> listedessin = new ArrayList<>();
  public volatile boolean styluspalmrejection = false;
  public volatile boolean pressuresize = false;
  public volatile int pickacolor = 0;
  public volatile int pickacoloralllayers = 1;
  public volatile int pickacolorclosest = 2;
  public volatile int pickacolorthickness = 3;
  public volatile int selectedtrace = 0;
  public volatile boolean onlymouseup = false;
  public volatile int onlymouseupstatus = -1;
  public volatile boolean drawdevelopmenutype = false;
  public volatile boolean drawdevelopmenuselection = false;
  public volatile boolean drawmovezoom = false;
  public volatile float drawmovemenusizeyi = 0.0f;
  public volatile boolean drawmovemenusize = false;
  public volatile int drawcurrlayer = -1;
  public volatile float selectedtraceresizeratio = 1.0f;
  public ArrayList<float[]> drawtracecurr = new ArrayList<>();
  public volatile int drawtracecurrdefcb = 0;
  public volatile float drawtracecurrbezier = 0.0f;
  public volatile float drawtracecurrcolorh = 68.0f;
  public volatile float drawtracecurrcolors = 0.8f;
  public volatile float drawtracecurrcolorv = 0.8f;
  public volatile float drawtracecurrcolora = 255.0f;
  public volatile float drawtracecurrsizemin = 1.0f;
  public volatile float drawtracecurrsizemax = 1.0f;
  public volatile float drawtracemaximumadd = 0.3f;
  public volatile float drawmenutotalw = 1.0f;
  public volatile float drawmenutotalh = 1.0f;
  public volatile boolean drawmenucolor = false;
  public volatile boolean drawmenumod = false;
  public volatile boolean drawmenusize = false;
  public volatile boolean drawmenupath = false;

  public volatile boolean optionvideoframemod = false;
  public volatile boolean optionvideolog = false;
  public volatile boolean optionvideodelay = false;
  public volatile boolean optionvideotweak = false;
  public volatile boolean optionshowtweak = false;
  public volatile boolean optionshowhowto = false;
  public volatile boolean optionshowroot = false;
  //public volatile boolean bailouthidemenu = false;
  public volatile boolean optionshowsearch = false;
  public volatile boolean optionshowmusic = false;
  public volatile boolean optionshowzoom = false;
  public volatile boolean optionshowsplit = false;
  public volatile boolean optionshowbookmarked = false;
  public volatile boolean optionshowapps = false;
  public volatile boolean optionshowhidden = false;
  public volatile boolean optionshowonline = false;
  public volatile boolean optionshowselection = false;
  public volatile boolean optionshowcollection = false;
  public volatile boolean optionshowselectionactive = false;
  public volatile boolean optionshowmodifypic = false;
  public volatile boolean optionshowsubcomment = false;
  public volatile boolean optionshowsubeffect = false;
  public volatile boolean optionshowsubeffectadvanced = false;
  public volatile boolean optionshowtopten = false;
  public volatile boolean optionshowshare = false;
  public volatile boolean optionshowdiscover = false;
  public volatile boolean optionshowmove = false;
  public volatile boolean optionshowdelete = false;
  public volatile boolean optionshowrescan = false;
  public volatile boolean optionshowwallpaper = false;
  public volatile boolean optionshowwidgets = false;
  public volatile boolean optionshowlockscreen = false;
  public volatile boolean optionshowminigame = false;
  public volatile boolean optionshowrecord = false;
  public volatile boolean videoabloop = false;
  public volatile boolean videoaudiodelay = false;
  public volatile boolean videosubtitledelay = false;
  public volatile boolean videoShowOverlay = false;
  public volatile boolean videoShowProgressBar = false;
  public volatile boolean videoShowOptionMenu = false;
  public volatile boolean videoShowLog = false;
  public volatile boolean videoShowSub = false;
  //public volatile boolean cropw = false;
  //public volatile boolean croph = false;
  public volatile boolean videoshowaspect = false;
  public volatile boolean videoshowplaylist = false;
  public volatile boolean videoshowvid = false;
  public volatile boolean videoshowaid = false;
  public volatile boolean videoshowsid = false;
  public volatile boolean videoshowextsubtitle = false;
  public volatile boolean videoshowvolume = false;
  public volatile boolean videoshowspeed = false;
  public volatile boolean videoshowabloop = false;
  public volatile boolean videoshowzoom = false;
  public volatile boolean videoshowpanx = false;
  public volatile boolean videoshowpany = false;
  public volatile boolean videoshowrotate = false;
  public volatile boolean videoshowsubfontsize = false;
  public volatile boolean videoshowseek = false;
  public volatile boolean videoshowthreads = false;
  public volatile boolean videoshowcachesize = false;
  public volatile boolean gamehitshowfriction = false;
  public volatile boolean gamehitshowtempscalcul = false;
  public volatile boolean gamehitshowbums = false;
  public volatile boolean gamehitshowattir = false;
  public volatile int videoshowlogfrom = 0;
  public void foldoptions(){
    llog.d(TAG, "-------- fold options");
    //bailouthidemenu = false;
    optionvideoframemod = false;
    optionvideodelay = false;
    optionvideolog = false;
    //cropw = false;
    //croph = false;
    drawgraph = false;
    optionvideotweak = false;
    optionshowtweak = false;
    optionshowhowto = false;
    optionshowroot = false;
    optionshowsearch = false;
    optionshowzoom = false;
    optionshowmusic = false;
    optionshowsplit = false;
    optionshowbookmarked = false;
    optionshowapps = false;
    optionshowhidden = false;
    optionshowonline = false;
    optionshowselection = false;
    optionshowcollection = false;
    optionshowselectionactive = false;
    optionshowmodifypic = false;
    //optionshowsubcomment = false;
    //optionshowsubeffect = false;
    //optionshowsubeffectmore = false;
    optionshowtopten = false;
    optionshowshare = false;
    optionshowdiscover = false;
    optionshowmove = false;
    optionshowdelete = false;
    optionshowrescan = false;
    optionshowwallpaper = false;
    optionshowwidgets = false;
    optionshowlockscreen = false;
    optionshowminigame = false;
    optionshowrecord = false;
    videoabloop = false;
    videoaudiodelay = false;
    videosubtitledelay = false;
    videoShowOverlay = false;
    videoShowProgressBar = false;
    videoShowOptionMenu = false;
    videoShowLog = false;
    videoShowSub = false;
    videoshowaspect = false;
    videoshowplaylist = false;
    videoshowvid = false;
    videoshowaid = false;
    videoshowsid = false;
    videoshowvolume = false;
    videoshowspeed = false;
    videoshowabloop = false;
    videoshowzoom = false;
    videoshowpanx = false;
    videoshowpany = false;
    videoshowrotate = false;
    videoshowsubfontsize = false;
    videoshowseek = false;
    videoshowthreads = false;
    videoshowcachesize = false;
    gamehitshowfriction = false;
    gamehitshowtempscalcul = false;
    gamehitshowbums = false;
    gamehitshowattir = false;
    videoshowextsubtitle = false;
    videoshowlogfrom = 0;
  }

  public MPVLib mpvlib = null;

  public volatile Rect srcrect = new Rect();
  public volatile RectF dstrect = new RectF();
  
  public volatile float SettingsYmin = 0.0f;
  public volatile float SettingsXmin = 0.0f;
  public volatile float SettingsYmax = 0.0f;
  
  public volatile int CaseInvisibleSuivXmin;
  public volatile int CaseInvisiblePrecXmax;
  public volatile int CaseInvisiblePrecSuivYmin;
  public volatile int CaseInvisibleOptionXmax;
  public volatile int CaseInvisibleOptionYmax;
  public volatile int CaseInvisibleW;

  private final Map<String, CachedBitmap> cachedbitmap = new HashMap<>();
  private final ArrayList<String> asupprimerducache = new ArrayList<>();
  private final ReentrantLock cachedbitmaplock = new ReentrantLock();
  public float[] getcachedbitmapnfo(String key) {
    float[] nfo = new float[]{1.0f, 1.0f, 1.0f, 1.0f};
    cachedbitmaplock.lock();
    if (cachedbitmap.containsKey(key)) {
      CachedBitmap cb = cachedbitmap.get(key);
      nfo[0] = cb.width;
      nfo[1] = cb.height;
      nfo[2] = cb.bitmapcurrent;
      nfo[3] = cb.bitmapcount;
    }
    cachedbitmaplock.unlock();
    return nfo;
  }
  public Bitmap getcachedbitmap(String key) {
    Bitmap bitmap = null;
    cachedbitmaplock.lock();
    if (cachedbitmap.containsKey(key)) {
      CachedBitmap cb = cachedbitmap.get(key);
      if (cb.bitmap != null) {
        if (!cb.bitmap.isRecycled()) {
          bitmap = cb.bitmap;
        } else {
          cb.bitmap = null;
          cachedbitmap.remove(key);
        }
      } else {
        cachedbitmap.remove(key);
      }
    }
    cachedbitmaplock.unlock();
    if (bitmap == null) {
      llog.d(TAG, "error getcachedbitmap bitmap null " + key);
      bitmap = Bitmap.createBitmap(100, 100, Bitmap.Config.ARGB_8888);
      Canvas canv = new Canvas(bitmap);
      Paint paint = new Paint();
      paint.setStrokeWidth(5.0f);
      paint.setColor(Color.rgb(255, 0, 0));
      canv.drawLine(0, 0, 100, 100, paint);
      canv.drawLine(100, 0, 0, 100, paint);
    }
    return bitmap;
  }
  public boolean isincache(String key){
    boolean isincache = false;
    cachedbitmaplock.lock();
    if (cachedbitmap.containsKey(key))
      isincache = true;
    cachedbitmaplock.unlock();
    return isincache;
  }
  public boolean addtocache(String key, int w, int h, int current, int count, Bitmap cachebitmap, boolean onlinepicturefaileddonotoverwriteifkeyexists){
    boolean added = false;
    if (cachebitmap != null && w >= 1 && h >= 1) {
      if (!cachebitmap.isRecycled()) {
        cachedbitmaplock.lock();
        if (cachedbitmap.containsKey(key)) {
          CachedBitmap cb = cachedbitmap.get(key);
          if (onlinepicturefaileddonotoverwriteifkeyexists) {
            //llog.d(TAG, "addtocache containskey onlinepicturefaileddonotoverwriteifkeyexists " + key + " " + current + " " + count + " old " + cb.bitmapcurrent + " " + cb.bitmapcount);
            cb.bitmapcurrent = current;
          } else {
            //llog.d(TAG, "addtocache containskey !onlinepicturefaileddonotoverwriteifkeyexists " + key + " " + current + " " + count + " old " + current + " " + count);
            if (cb.bitmap != null) {
              if (!cb.bitmap.isRecycled()) {
                cb.bitmap.recycle();
              }
            }
            cb.bitmap = cachebitmap;
            cb.width = w;
            cb.height = h;
            cb.bitmapcurrent = current;
            cb.bitmapcount = count;
          }
          cb.bitmapaskednext = false;
          cachebitmap.prepareToDraw();
          added = true;
        } else {
          //llog.d(TAG, "addtocache !containskey " + key + " " + current + " " + count);
          CachedBitmap cb = new CachedBitmap(cachebitmap, w, h, current, count);
          cachedbitmap.put(key, cb);
          cachebitmap.prepareToDraw();
          added = true;
        }
        cachedbitmaplock.unlock();
      }
    } else {
      llog.d(TAG, "addtocache cachebitmap == null w h " + key + " " + w + " " + h);
    }
    return added;
  }
  public long bitmaplastchange = 0L;
  public volatile boolean bitmapaskedforceredraw = false;
  public boolean drawCachedBitmap(String key, Canvas canvas, Rect src, RectF dst, long filmstripanimatetime, Gallery model) {
    cachedbitmaplock.lock();
    if (cachedbitmap.containsKey(key)) {
      CachedBitmap cb = cachedbitmap.get(key);
      if (cb.bitmap != null) {
        if (!cb.bitmap.isRecycled()) {
          long currtime = System.currentTimeMillis();
          if ((cb.bitmapcount > 1
                  && !cb.bitmapaskednext
                  && currtime - filmstriplastchange > filmstripanimatetime)
             || (cb.bitmapcount > 0 && bitmapaskedforceredraw)
          ) {
            int bitmapcurrent = (int) ((filmstripreftime / filmstripanimatetime) % (cb.bitmapcount));
            if ((bitmapcurrent != cb.bitmapcurrent && currtime - bitmaplastchange > filmstripanimatetime)
                || bitmapaskedforceredraw
            ) {
              bitmaplastchange = currtime;
              cb.bitmapaskednext = true;
              bitmapaskedforceredraw = false;
              try {
                model.commandebigimagethreadqueue.put(new String[]{
                        "updatebigpicture",
                        String.valueOf(myid),
                        String.valueOf(ordnerIndex),
                        String.valueOf(mediaIndex),
                        String.valueOf(bigimagecurrentlydisplayed),
                        String.valueOf(false),
                        "-1",
                        String.valueOf(cb.bitmapcount)});
              } catch (InterruptedException e) {
                e.printStackTrace();
              }
            }
          }
          try {
            canvas.drawBitmap(cb.bitmap, src, dst, paintdither);
          } catch (RuntimeException e) {
            llog.d(TAG, "Canvas: trying to draw too large(bytes) bitmap." + key);
          }
        } else {
          cb.bitmap = null;
          cachedbitmap.remove(key);
        }
      } else {
        cachedbitmap.remove(key);
      }
    }
    cachedbitmaplock.unlock();
    return true;
  }
  public boolean saveCachedBitmap(String key, String destinationfile) {
    boolean success = false;
    cachedbitmaplock.lock();
    if (cachedbitmap.containsKey(key)) {
      CachedBitmap cb = cachedbitmap.get(key);
      if (cb.bitmap != null) {
        if (!cb.bitmap.isRecycled()) {
          FileOutputStream fos;
          try {
            fos = new FileOutputStream(destinationfile);
            cb.bitmap.compress(Bitmap.CompressFormat.PNG, 100, fos); // 100 ignored for png
            fos.close();
            success = true;
          } catch (IOException e) {
            e.printStackTrace();
          }
        }
      }
    }
    cachedbitmaplock.unlock();
    return success;
  }
  public boolean removekeyfromcache(String key){
    boolean removed = true;
    cachedbitmaplock.lock();
    asupprimerducache.add(key);
    cachedbitmaplock.unlock();
    return removed;
  }
  public boolean removekeysfromcache(){
    boolean removed = true;
    cachedbitmaplock.lock();
    for (String key : cachedbitmap.keySet())
      asupprimerducache.add(key);
    cachedbitmaplock.unlock();
    return removed;
  }
  public int removefromcache(){
    int removed = 0;
    cachedbitmaplock.lock();
    int asupprimerducachesize = asupprimerducache.size();
    //llog.d(TAG, cachedbitmap.size() + " en cache " + asupprimerducachesize + " à virer");
    for (int i = 0; i < asupprimerducachesize; i++) {
      String suppr = asupprimerducache.get(i);
      if (cachedbitmap.containsKey(suppr)) {
        CachedBitmap cb = cachedbitmap.get(suppr);
        if (cb != null) {
          if (cb.bitmap != null) {
            if (!cb.bitmap.isRecycled()) {
              //llog.d(TAG, "removefromcache " + suppr);
              cb.bitmap.recycle();
            }
            cb.bitmap = null;
          }
        }
        cachedbitmap.remove(suppr);
        removed += 1;
      }
    }
    asupprimerducache.clear();
    cachedbitmaplock.unlock();
    return removed;
  }
  public String[] getcachekeyset(){
    cachedbitmaplock.lock();
    int keysetl = cachedbitmap.size();
    String[] keyset = new String[keysetl];
    int i = 0;
    for (String key : cachedbitmap.keySet()) {
      if (i >= keysetl)
        break;
      keyset[i] = key;
      i++;
    }
    cachedbitmaplock.unlock();
    return keyset;
  }

}