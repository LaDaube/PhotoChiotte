package la.daube.photochiotte;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.graphics.RectF;
import android.os.AsyncTask;
import android.os.Build;
import android.view.Surface;

import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLDecoder;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class ThreadVideo implements Runnable {
  private static final String TAG = "YYYtvid";

  private long[] tempsinit = new long[30];
  private long[] tempsdiff = new long[30];
  private int[] tempscompte = new int[30];

  private Surf mysurf;
  private int currid = -1;

  private boolean clickquicknav = false;
  private boolean clickquicknavmenu = false;
  private boolean clicktohandle = false;
  private int clickrightleft = 0;
  private float clicktohandlex = 0.0f;
  private float clicktohandley = 0.0f;

  private volatile long POLL_TIMEOUT = Gallery.polltimeoutdefault;

  private Gallery model;

  private String[] nworder = null;
  private MPVLib currmpvlib = null;

  private final static int streambuffersize = 8192;
  public final static float speedlimitdefault = 2000.0f; // ko/s

  class DownloadFileFromURL extends AsyncTask<String, String, String> {
    public volatile int cusprotoindex = 0;
    public volatile long datawritten = 0L;
    public volatile boolean readingnotstarted = true;
    public float speedlimit = speedlimitdefault;
    public final int bytebuffersize = streambuffersize * 2000;
    @Override
    protected void onPreExecute() {
      super.onPreExecute();
      System.out.println("DownloadFileFromURL Starting download");
    }
    @Override
    protected String doInBackground(String... f_url) {
      model.threadvideoplaylistrunning = true;
      int mycurrid = currid;
      if (mycurrid >= model.surf.size())
        return null;
      MPVLib mympvlib = model.surf.get(mycurrid).mpvlib;
      int count;
      try {
        String baseurl = f_url[0];
        llog.d(TAG, "DownloadFileFromURL loading " + baseurl);

        byte data[] = new byte[streambuffersize];
        long starttime = System.currentTimeMillis();
        boolean stilldownloading = true;
        int playlistnext = mympvlib.playlistnext;
        int bufferposition = 0;
        ByteBuffer buf = ByteBuffer.allocateDirect(bytebuffersize);
        mympvlib.streamcbAddRo("cusproto", buf);
        mympvlib.loadfile("cusproto://dummy" + cusprotoindex + ".ts", false);

        while (stilldownloading && !mympvlib.videoaskclose && model.threadvideoplaylistrunning) {

          URL url = new URL(baseurl);
          URLConnection conection = url.openConnection();
          conection.connect();
          int lenghtOfFile = conection.getContentLength();
          BufferedReader in = new BufferedReader(new InputStreamReader(conection.getInputStream()));
          String inputLine;
          Pattern pattern1 = Pattern.compile("#EXTINF:");
          Pattern patternnamevalue = Pattern.compile(" ([^ ]+)=[\'\"]([^\"\']+)", Pattern.CASE_INSENSITIVE);
          Pattern pattern2 = Pattern.compile("([^\"\']+)");
          String name;
          String value;
          int currpos = 0;
          while ((inputLine = in.readLine()) != null) {
            Matcher matcher = pattern1.matcher(inputLine);
            if (matcher.find()) {
              String grouptitle = "group-title";
              String tvgname = "tvg-name";
              matcher = patternnamevalue.matcher(inputLine);
              while (matcher.find()) {
                name = matcher.group(1);
                value = matcher.group(2);
                //llog.d(TAG, name + " ===================== " + value);
                if (name.equals("group-title")){
                  grouptitle = value;
                }
                if (name.equals("tvg-name")){
                  tvgname = value;
                }
              }
              inputLine = in.readLine();
              if (inputLine == null) {
                break;
              }
              matcher = pattern2.matcher(inputLine);
              if (matcher.find()) {
                if (currpos >= mympvlib.playlistdetail.size()) {
                  String currurl = matcher.group(1);
                  String[] detail = new String[]{ currurl, grouptitle, tvgname };
                  mympvlib.playlistdetail.add(detail);
                  llog.d(TAG, currpos + " " + currurl + " " + grouptitle + " " + tvgname);
                }
                currpos += 1;
              }
            }
          }
          in.close();

          if (mympvlib.videoaskclose || playlistnext >= mympvlib.playlistdetail.size()) {
            llog.d(TAG, "no more file to read " + playlistnext + " >= " + mympvlib.playlistdetail.size());
            // only quit if videoaskclose otherwise sleep and retry
            stilldownloading = false;
            mympvlib.sendBufferRegion(-1);
          } else {
            // on maj le plus tôt possible après la lecture du m3u mais on doit rester à un indexe valide
            mympvlib.playlistnext = playlistnext;
            String thisurl = mympvlib.playlistdetail.get(playlistnext)[0];
            url = new URL(thisurl);
            conection = url.openConnection();
            conection.connect();
            lenghtOfFile = conection.getContentLength();
            llog.d(TAG, "+ Downloading " + lenghtOfFile + "B from " + thisurl);
            InputStream input = new BufferedInputStream(url.openStream(), streambuffersize); // buffer size
            while ((count = input.read(data)) != -1) {
              datawritten += count;
              buf.put(data);
              bufferposition += count;
              buf.position(bufferposition);
              int cppbufferpos = mympvlib.sendBufferRegion(bufferposition);
              if (buf.remaining() < streambuffersize) {
                /**
                 *   rewind buffer but wait for mpv first
                 */
                while (cppbufferpos < bufferposition) {
                  //llog.d(TAG, String.format("======== sleep wait for c++ till false %d < %d", cppbufferpos, bufferposition));
                  Thread.sleep(500);
                  cppbufferpos = mympvlib.sendBufferRegion(bufferposition);
                }
                bufferposition = 0;
                buf.rewind();
                //llog.d(TAG, "========= buffer rewinded");
              }
              //llog.d(TAG, String.format("%08x %08x   %02x %02x %02x     %02x %02x %02x" , count, datawritten, data[0], data[1], data[2], data[count-3], data[count-2], data[count-1]));
              if (mympvlib.playlistnext != playlistnext) {
                llog.d(TAG, "playlist position changed stop and reload");
                break;
              }
              long currtime = System.currentTimeMillis();
              float speedkos = (float) datawritten / (float) (currtime - starttime);
              if (speedkos > speedlimit) {
                //llog.d(TAG, "speedlimit reached slow down " + speedkos);
                Thread.sleep(1000);
              }
              if (mympvlib.timepos < mympvlib.duration - 60 && mympvlib.duration > 10 && mympvlib.timepos > 0) {
                //llog.d(TAG, "timepos reached slow down " + mympvlib.timepos + " " + mympvlib.duration);
                Thread.sleep(3000);
              }
              if (mympvlib.videoaskclose || !model.threadvideoplaylistrunning) {
                stilldownloading = false;
                break;
              }
            }
            input.close();
          }
          if (mympvlib.videoaskclose || playlistnext >= mympvlib.playlistdetail.size() || !stilldownloading) {
            llog.d(TAG, playlistnext + " >= " + mympvlib.playlistdetail.size());
            stilldownloading = false;
          } else {
            if (mympvlib.playlistnext == playlistnext) {
              llog.d(TAG, "append next file");
              playlistnext += 1;
            } else {
              llog.d(TAG, "playlist position changed stop and reload");
              playlistnext = mympvlib.playlistnext;
              //mympvlib.pause(true);
              //mympvlib.command(new String[]{"stop"});
              //Thread.sleep(500);
              bufferposition = 0;
              mympvlib.sendBufferRegion(bufferposition);
              buf.rewind();
              cusprotoindex += 1;
              mympvlib.loadfile("cusproto://dummy" + cusprotoindex + ".ts", false);
              datawritten = 0;
              starttime = System.currentTimeMillis();
              mympvlib.timepos = 0;
              mympvlib.duration = 0;
            }
          }


        }


      } catch (InterruptedException e) {
        llog.d("Error: ", e.getMessage());
      } catch (IOException e) {
        llog.d("Error: ", e.getMessage());
      }
      return null;
    }
    @Override
    protected void onPostExecute(String file_url) {
      System.out.println("DownloadFileFromURL Downloaded");
    }
  }



  class DownloadFileFromTorrent extends AsyncTask<String, String, String> {
    public volatile int cusprotoindex = 0;
    public volatile long datawritten = 0L;
    public volatile boolean readingnotstarted = true;
    public float speedlimit = speedlimitdefault;
    public final int bytebuffersize = streambuffersize * 2000;
    @Override
    protected void onPreExecute() {
      super.onPreExecute();
      System.out.println("DownloadFileFromURL Starting download");
    }
    @Override
    protected String doInBackground(String... f_url) {
      model.threadvideoplaylistrunning = true;
      int mycurrid = currid;
      if (mycurrid >= model.surf.size())
        return null;
      MPVLib mympvlib = model.surf.get(mycurrid).mpvlib;
      int count;
      try {
        String baseurl = f_url[0];
        llog.d(TAG, "DownloadFileFromURL loading " + baseurl);

        byte data[] = new byte[streambuffersize];
        long starttime = System.currentTimeMillis();
        boolean stilldownloading = true;
        int playlistnext = mympvlib.playlistnext;
        int bufferposition = 0;
        ByteBuffer buf = ByteBuffer.allocateDirect(bytebuffersize);
        mympvlib.streamcbAddRo("cusproto", buf);
        mympvlib.loadfile("cusproto://dummy" + cusprotoindex + ".ts", false);

        while (stilldownloading && !mympvlib.videoaskclose && model.threadvideoplaylistrunning) {

          URL url = new URL(baseurl);
          URLConnection conection = url.openConnection();
          conection.connect();
          int lenghtOfFile = conection.getContentLength();
          BufferedReader in = new BufferedReader(new InputStreamReader(conection.getInputStream()));
          String inputLine;
          Pattern pattern1 = Pattern.compile("#EXTINF:");
          Pattern patternnamevalue = Pattern.compile(" ([^ ]+)=[\'\"]([^\"\']+)", Pattern.CASE_INSENSITIVE);
          Pattern pattern2 = Pattern.compile("([^\"\']+)");
          String name;
          String value;
          int currpos = 0;
          while ((inputLine = in.readLine()) != null) {
            Matcher matcher = pattern1.matcher(inputLine);
            if (matcher.find()) {
              String grouptitle = "group-title";
              String tvgname = "tvg-name";
              matcher = patternnamevalue.matcher(inputLine);
              while (matcher.find()) {
                name = matcher.group(1);
                value = matcher.group(2);
                //llog.d(TAG, name + " ===================== " + value);
                if (name.equals("group-title")){
                  grouptitle = value;
                }
                if (name.equals("tvg-name")){
                  tvgname = value;
                }
              }
              inputLine = in.readLine();
              if (inputLine == null) {
                break;
              }
              matcher = pattern2.matcher(inputLine);
               if (matcher.find()) {
                if (currpos >= mympvlib.playlistdetail.size()) {
                  String currurl = matcher.group(1);
                  String[] detail = new String[]{ currurl, grouptitle, tvgname };
                  mympvlib.playlistdetail.add(detail);
                  llog.d(TAG, currpos + " " + currurl + " " + grouptitle + " " + tvgname);
                }
                currpos += 1;
              }
            }
          }
          in.close();

          if (mympvlib.videoaskclose || playlistnext >= mympvlib.playlistdetail.size()) {
            llog.d(TAG, "no more file to read " + playlistnext + " >= " + mympvlib.playlistdetail.size());
            // only quit if videoaskclose otherwise sleep and retry
            stilldownloading = false;
            mympvlib.sendBufferRegion(-1);
          } else {
            // on maj le plus tôt possible après la lecture du m3u mais on doit rester à un indexe valide
            mympvlib.playlistnext = playlistnext;
            String thisurl = mympvlib.playlistdetail.get(playlistnext)[0];
            url = new URL(thisurl);
            conection = url.openConnection();
            conection.connect();
            lenghtOfFile = conection.getContentLength();
            llog.d(TAG, "+ Downloading " + lenghtOfFile + "B from " + thisurl);
            InputStream input = new BufferedInputStream(url.openStream(), streambuffersize); // buffer size
            while ((count = input.read(data)) != -1) {
              datawritten += count;
              buf.put(data);
              bufferposition += count;
              buf.position(bufferposition);
              int cppbufferpos = mympvlib.sendBufferRegion(bufferposition);
              if (buf.remaining() < streambuffersize) {
                /**
                 *   rewind buffer but wait for mpv first
                 */
                while (cppbufferpos < bufferposition) {
                  //llog.d(TAG, String.format("======== sleep wait for c++ till false %d < %d", cppbufferpos, bufferposition));
                  Thread.sleep(500);
                  cppbufferpos = mympvlib.sendBufferRegion(bufferposition);
                }
                bufferposition = 0;
                buf.rewind();
                //llog.d(TAG, "========= buffer rewinded");
              }
              //llog.d(TAG, String.format("%08x %08x   %02x %02x %02x     %02x %02x %02x" , count, datawritten, data[0], data[1], data[2], data[count-3], data[count-2], data[count-1]));
              if (mympvlib.playlistnext != playlistnext) {
                llog.d(TAG, "playlist position changed stop and reload");
                break;
              }
              long currtime = System.currentTimeMillis();
              float speedkos = (float) datawritten / (float) (currtime - starttime);
              if (speedkos > speedlimit) {
                //llog.d(TAG, "speedlimit reached slow down " + speedkos);
                Thread.sleep(1000);
              }
              if (mympvlib.timepos < mympvlib.duration - 60 && mympvlib.duration > 10 && mympvlib.timepos > 0) {
                //llog.d(TAG, "timepos reached slow down " + mympvlib.timepos + " " + mympvlib.duration);
                Thread.sleep(3000);
              }
              if (mympvlib.videoaskclose || !model.threadvideoplaylistrunning) {
                stilldownloading = false;
                break;
              }
            }
            input.close();
          }
          if (mympvlib.videoaskclose || playlistnext >= mympvlib.playlistdetail.size() || !stilldownloading) {
            llog.d(TAG, playlistnext + " >= " + mympvlib.playlistdetail.size());
            stilldownloading = false;
          } else {
            if (mympvlib.playlistnext == playlistnext) {
              llog.d(TAG, "append next file");
              playlistnext += 1;
            } else {
              llog.d(TAG, "playlist position changed stop and reload");
              playlistnext = mympvlib.playlistnext;
              //mympvlib.pause(true);
              //mympvlib.command(new String[]{"stop"});
              //Thread.sleep(500);
              bufferposition = 0;
              mympvlib.sendBufferRegion(bufferposition);
              buf.rewind();
              cusprotoindex += 1;
              mympvlib.loadfile("cusproto://dummy" + cusprotoindex + ".ts", false);
              datawritten = 0;
              starttime = System.currentTimeMillis();
              mympvlib.timepos = 0;
              mympvlib.duration = 0;
            }
          }


        }


      } catch (InterruptedException e) {
        llog.d("Error: ", e.getMessage());
      } catch (IOException e) {
        llog.d("Error: ", e.getMessage());
      }
      return null;
    }
    @Override
    protected void onPostExecute(String file_url) {
      System.out.println("DownloadFileFromURL Downloaded");
    }
  }

  protected ThreadVideo(Gallery mmodel) {
    model = mmodel;
  }

  boolean checkready() {
    for (int i = 0 ; i < 1000 ; i++) {
      if (model.dossierminiature != null
        && model.surf.size() > 0)
        return true;
      try {
        Thread.sleep(10);
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
      if (i % 50 == 0) {
        llog.d(TAG, "error checkready() " + i);
      }
    }
    return false;
  }

  private boolean failedtoaappendnextsequence = false;
  private long failedtoaappendnextsequencet = -1;

  @Override
  public void run() {
    model.threadvideorunning = true;
    llog.d(TAG, "++++++++++++++++++++ ThreadVideo");
    if (!checkready()) {
      llog.d(TAG, "error checkready bail out");
      model.threadvideorunning = false;
      return;
    }

    //boolean nextpictureposition0 = false;
    ByteBuffer buffer = ByteBuffer.allocateDirect(55 * 1024);
    boolean updateonefinaltime = false;

    currid = 0;
    mysurf = model.surf.get(currid);
    currmpvlib = null;

    long lastplaylaunch = System.currentTimeMillis();

    while (true) {
      boolean encore = true;
      while (encore == true) {

        // on vide la liste
        if (model.commandethreadvideo.isEmpty() && updateonefinaltime) {
          /**
           *   on rafraîchit l'écran une dernière fois
           *   tout à la fin si jamais on est disponible
           *   car les options sont modifiées en même temps que l'on dessine
           *   et on risque donc de ne pas avoir la version finale de l'écran
           */
          updateonefinaltime = false;
          nworder = new String[]{"-1", "update"};
          // on ne modifie pas myid on reste sur le précédent
        } else {
          try {
            nworder = model.commandethreadvideo.poll(POLL_TIMEOUT, TimeUnit.MILLISECONDS);
          } catch (InterruptedException e) {
            e.printStackTrace();
          }
          model.threadvideorunninglastpoll = System.currentTimeMillis();
          updateonefinaltime = false;
        }

        if (nworder == null) {
          nworder = new String[]{"-1", "poll"};
          int surfl = model.surf.size();
          boolean gotavid = false;
          for (int i = 0 ; i < surfl ; i++) {
            currid = (currid + 1) % surfl;
            mysurf = model.surf.get(currid);
            if (mysurf.fragmenttype == Surf.FRAGMENT_TYPE_VIDEO) {
              gotavid = true;
              break;
            }
          }
          long currtime = System.currentTimeMillis();
          if (POLL_TIMEOUT != Gallery.polltimeoutdefault) {
            if (!gotavid || (currtime - lastplaylaunch > 6000L && !mysurf.videoShowSub)) {
              POLL_TIMEOUT = Gallery.polltimeoutdefault;
              llog.d(TAG, "POLL_TIMEOUT DEFAULT");
            }
          }
        }
        int targetid = Integer.parseInt(nworder[0]);
        if (targetid != -1 && targetid < model.surf.size()) {
          if (targetid != currid)
            encore = false;
          currid = targetid;
          mysurf = model.surf.get(currid);
        }

        if (nworder[1].equals("quit")) {
          if (model.threadvideoon) {
            llog.d(TAG, "////////////////////// thread video keep running");
          } else {
            model.threadvideoplaylistrunning = false;
            buffer.clear();
            model.threadvideorunning = false;
            llog.d(TAG, "---------------------- thread video");
            return;
          }

        } else if (!model.threadvideorunning) {
          // si on pause on efface toutes les commandes moins importantes suivantes dans la queue

        } else if (nworder[1].equals("videoaaspectresize")) {
          MPVLib mympvlib = mysurf.mpvlib;
          if (mympvlib != null) {
            Intent intent = new Intent();
            intent.setAction(Gallery.broadcastname);
            intent.putExtra("goal", "resizevideoframe");
            intent.putExtra("id", currid);
            if (nworder.length >= 3)
              intent.putExtra("maxout", true);
            else
              intent.putExtra("maxout", false);
            LocalBroadcastManager.getInstance(model.activitycontext).sendBroadcast(intent);
          }
          updateonefinaltime = true;
          encore = false;

        } else if (nworder[1].equals("startbrowser")) {
          llog.d(TAG, "startbrowser asked");
          if (mysurf.mpvlib != null) {
            mysurf.mpvlib.videoaskclose = true;
            model.endvideotemps(mysurf.mpvlib.lastfileadded);
            if (mysurf.mpvlib.timepos > 0 && mysurf.mpvlib.streampath != null && mysurf.mpvlib.duration > 0) {
              float timepos = mysurf.mpvlib.timepos;
              float duration = mysurf.mpvlib.duration;
              float perc = timepos / duration;
              if (0.01f < perc && perc < 0.99f) {
                int posstoredcount = 0;
                int storetopos = -1;
                posstoredcount = model.preferences.getInt("videoposstorecount", 0);
                for (int i = 0; i < posstoredcount; i++) {
                  if (model.preferences.getString("video" + i + "name", "crap").equals(mysurf.mpvlib.streampath)) {
                    storetopos = i;
                    break;
                  }
                }
                if (storetopos == -1)
                  storetopos = posstoredcount;
                long currtime = System.currentTimeMillis();
                if (storetopos >= Gallery.maxstorelastvideoposition) { // on écrase la plus vieille sauvegarde
                  int plusancieni = 0;
                  long plusancien = currtime;
                  for (int i = 0; i < posstoredcount; i++) {
                    long anciennete = model.preferences.getLong("video" + i + "date", currtime);
                    if (anciennete < plusancien) {
                      plusancieni = i;
                      plusancien = anciennete;
                    }
                  }
                  storetopos = plusancieni;
                }
                llog.d(TAG, "saving video position at socket " + storetopos + " : " + perc * 100.0 + "%");
                SharedPreferences.Editor prefEdit = model.preferences.edit();
                prefEdit.putString("video" + storetopos + "name", mysurf.mpvlib.streampath);
                prefEdit.putInt("video" + storetopos + "timepos", (int) timepos);
                prefEdit.putLong("video" + storetopos + "date", System.currentTimeMillis());
                if (storetopos >= posstoredcount)
                  prefEdit.putInt("videoposstorecount", posstoredcount + 1);
                prefEdit.commit();
              } else {
                llog.d(TAG, "do2 not save position " + mysurf.mpvlib.timepos + " " + mysurf.mpvlib.streampath + " " + mysurf.mpvlib.duration);
              }
            } else {
              llog.d(TAG, "do not save position " + mysurf.mpvlib.timepos + " " + mysurf.mpvlib.streampath + " " + mysurf.mpvlib.duration);
            }
            if (mysurf.mpvlib != null)
              mysurf.mpvlib.stop();
            mysurf.mpvlib = null;
            llog.d(TAG, "   ---------------------- stopplaying in " + currid);
          } else {
            llog.d(TAG, "do not save position mpvlib null");
          }

          Intent intent = new Intent();
          intent.setAction(Gallery.broadcastname);
          intent.putExtra("goal", "startbrowser");
          intent.putExtra("id", currid);
          LocalBroadcastManager.getInstance(model.activitycontext).sendBroadcast(intent);

        } else if (nworder[1].equals("surfacechanged")) {
          llog.d(TAG, "-----------surfacechanged-------------");
          updateonefinaltime = true;
          encore = false;

        } else if (nworder[1].equals("startplaying") || nworder[1].equals("restartplayingnohw")) {
          /**
           *    on attrape tous les startplaying on n'en rate jamais un seul
           */
          POLL_TIMEOUT = Gallery.polltimeoutfast;
          lastplaylaunch = System.currentTimeMillis();

          llog.d(TAG, "   +++++++++++++++++++++ " + nworder[1] + " : " + nworder[2] + " in " + currid);

          mysurf.fragmenttype = Surf.FRAGMENT_TYPE_VIDEO;
          model.startVideoClearBrowserSurfaceTransparent = true;

          if (mysurf.mpvlib != null) {
            //mysurf.mpvlib.stop();
            if (!nworder[1].equals("restartplayingnohw")) {
              llog.d(TAG, "   +++++++++++++++++++++ " + nworder[1] + " : mpvlib is not null resetvars()");
              mysurf.mpvlib.resetvars();
            } else {
              llog.d(TAG, "   +++++++++++++++++++++ " + nworder[1] + " : mpvlib is not null restartplayingnohw do not resetvars()");
            }
          } else {
            mysurf.mpvlib = new MPVLib(currid);
          }

          mysurf.mpvlib.setOptionString("config", "yes");
          mysurf.mpvlib.setOptionString("config-dir", model.dossierconfig);
          mysurf.mpvlib.setOptionString("screenshot-directory", model.dossierscreenshot);
          mysurf.mpvlib.setOptionString("screenshot-format", "webp");
          mysurf.mpvlib.setOptionString("screenshot-webp-lossless", "yes");
          mysurf.mpvlib.setOptionString("screenshot-webp-compression", "0");
          //mysurf.mpvlib.setOptionString("scripts", model.dossierconfig + "/stats.lua" + ":" + model.dossierconfig + "/visualizer.lua");

          mysurf.mpvlib.setOptionString("cache", "yes");
          model.videocachesize = model.preferences.getInt("videocachesize", model.videocachesize);
          mysurf.mpvlib.setOptionString("demuxer-max-bytes", String.valueOf(model.videocachesize * 1000000));
          mysurf.mpvlib.setOptionString("demuxer-max-back-bytes", String.valueOf(model.videocachesize * 1000000));

          String cache = model.activitycontext.getFilesDir().getAbsolutePath();
          mysurf.mpvlib.setOptionString("gpu-shader-cache-dir", cache);
          mysurf.mpvlib.setOptionString("icc-cache-dir", cache);

          model.videoloop = model.preferences.getInt("videoloop", model.videoloop);
          if (model.videoloop == Gallery.videoloopLoop
              || model.videoloop == Gallery.videoloopSequence
              || mysurf.optiondiaporamaactive) {
            llog.d(TAG, " -------- eof yes ---------- " + mysurf.optiondiaporamaactive);
            mysurf.mpvlib.setOptionString("eof", "yes");
          } else {
            mysurf.mpvlib.setOptionString("eof", "no");
          }
          if (model.videoloop == Gallery.videoloopLoop) {
            mysurf.mpvlib.setOptionString("loop-file", "inf");
          }
          if (model.videoloop == Gallery.videoloopSequence) {
            mysurf.mpvlib.setOptionString("prefetch-playlist", "yes");
          }

          if (model.videohardwaremode == Gallery.videohwaamodesw || nworder[1].equals("restartplayingnohw")) {        // sw decoding     Software decoding with opengl rendering
            mysurf.mpvlib.setOptionString("vo", "gpu");
            mysurf.mpvlib.setOptionString("gpu-context", "android");
            mysurf.mpvlib.setOptionString("opengl-es", "yes");
            mysurf.mpvlib.setOptionString("hwdec", "no");
          } else if (model.videohardwaremode == Gallery.videohwaamodehw) { // hw copy to ram  copies video back to system RAM Decodes in hardware, but then downloads the hardware image into a software buffer which is rendered using the regular opengl pipeline.
            mysurf.mpvlib.setOptionString("vo", "gpu");
            mysurf.mpvlib.setOptionString("gpu-context", "android");
            mysurf.mpvlib.setOptionString("opengl-es", "yes");
            mysurf.mpvlib.setOptionString("hwdec", "mediacodec-copy");
          } else if (model.videohardwaremode == Gallery.videohwaamodehwp) {
            mysurf.mpvlib.setOptionString("vo", "gpu");
            mysurf.mpvlib.setOptionString("gpu-context", "android");
            mysurf.mpvlib.setOptionString("opengl-es", "yes");
            mysurf.mpvlib.setOptionString("hwdec", "mediacodec");

          } else if (model.videohardwaremode == Gallery.videohwaamodehwpp) { // hw direct       Decodes to IMGFMT_MEDIACODEC and then emits those frames directly onto the display. Renders IMGFMT_MEDIACODEC frames directly to an android.view.Surface this video output driver uses native decoding and rendering routines
            mysurf.mpvlib.setOptionString("vo", "mediacodec_embed");
            mysurf.mpvlib.setOptionString("gpu-context", "android");
            mysurf.mpvlib.setOptionString("hwdec", "mediacodec");

          } else if (model.videohardwaremode == Gallery.videohwaamodensw) { // gpu-next sw decoding
            mysurf.mpvlib.setOptionString("vo", "gpu-next");
            mysurf.mpvlib.setOptionString("gpu-context", "android");
            mysurf.mpvlib.setOptionString("opengl-es", "yes");
            mysurf.mpvlib.setOptionString("hwdec", "no");
          } else if (model.videohardwaremode == Gallery.videohwaamodenhw) { // gpu-next hw copy to ram
            mysurf.mpvlib.setOptionString("vo", "gpu-next");
            mysurf.mpvlib.setOptionString("gpu-context", "android");
            mysurf.mpvlib.setOptionString("opengl-es", "yes");
            mysurf.mpvlib.setOptionString("hwdec", "mediacodec-copy");
          } else if (model.videohardwaremode == Gallery.videohwaamodenhwp) {
            mysurf.mpvlib.setOptionString("vo", "gpu-next");
            mysurf.mpvlib.setOptionString("gpu-context", "android");
            mysurf.mpvlib.setOptionString("opengl-es", "yes");
            mysurf.mpvlib.setOptionString("hwdec", "mediacodec");

          } else if (model.videohardwaremode == Gallery.videohwaamodevksw) { // vulkan sw decoding
            mysurf.mpvlib.setOptionString("vo", "gpu");
            mysurf.mpvlib.setOptionString("gpu-context", "androidvk");
            mysurf.mpvlib.setOptionString("hwdec", "no");
          } else if (model.videohardwaremode == Gallery.videohwaamodevkhw) { // vulkan hw copy to ram
            mysurf.mpvlib.setOptionString("vo", "gpu");
            mysurf.mpvlib.setOptionString("gpu-context", "androidvk");
            mysurf.mpvlib.setOptionString("hwdec", "mediacodec-copy");
          } else if (model.videohardwaremode == Gallery.videohwaamodevkhwp) { // vulkan hw+
            mysurf.mpvlib.setOptionString("vo", "gpu");
            mysurf.mpvlib.setOptionString("gpu-context", "androidvk");
            mysurf.mpvlib.setOptionString("hwdec", "mediacodec");

          } else if (model.videohardwaremode == Gallery.videohwaamodevknsw) { // gpu-next vulkan sw decoding
            mysurf.mpvlib.setOptionString("vo", "gpu-next");
            mysurf.mpvlib.setOptionString("gpu-context", "androidvk");
            mysurf.mpvlib.setOptionString("hwdec", "no");
          } else if (model.videohardwaremode == Gallery.videohwaamodevknhw) { // gpu-next vulkan hw copy to ram
            mysurf.mpvlib.setOptionString("vo", "gpu-next");
            mysurf.mpvlib.setOptionString("gpu-context", "androidvk");
            mysurf.mpvlib.setOptionString("hwdec", "mediacodec-copy");
          } else if (model.videohardwaremode == Gallery.videohwaamodevknhwp) { // gpu-next vulkan hw+
            mysurf.mpvlib.setOptionString("vo", "gpu-next");
            mysurf.mpvlib.setOptionString("gpu-context", "androidvk");
            mysurf.mpvlib.setOptionString("hwdec", "mediacodec");
          }

          //mysurf.mpvlib.setOptionString("vf", "format:rotate=-1");
          if (mysurf.mpvlib.currfilter != null) {
            mysurf.mpvlib.setOptionString("vf", mysurf.mpvlib.currfilter);
          } else if (mysurf.filter != null) {
            mysurf.mpvlib.setOptionString("vf", mysurf.filter);
          }

          mysurf.mpvlib.setOptionString("hwdec-codecs", "h264,hevc,mpeg4,mpeg2video,vp8,vp9,av1");

          model.videomute = model.preferences.getBoolean("videomute", false);
          if (model.videomute) {
            //llog.d(TAG, "mute -------");
            //mysurf.mpvlib.setmutereal(true);
            mysurf.mpvlib.setOptionString("aid", "no");
          }/* else {
            mysurf.mpvlib.setOptionString("aid", "auto");
          }*/

          // 0 pas d'accrochage si ça rame
          model.videosyncaudio = model.preferences.getInt("videosyncaudio", 0);
          if (model.videosyncaudio == 0) {

          } else if (model.videosyncaudio == 1) {
            mysurf.mpvlib.setOptionString("video-sync", "audio");
          } else if (model.videosyncaudio == 2) {
            mysurf.mpvlib.setOptionString("video-sync", "display-resample");
          } else if (model.videosyncaudio == 3) {
            mysurf.mpvlib.setOptionString("video-sync", "display-resample-vdrop");
          } else if (model.videosyncaudio == 4) {
            mysurf.mpvlib.setOptionString("video-sync", "display-vdrop");
          } else if (model.videosyncaudio == 5) {
            mysurf.mpvlib.setOptionString("video-sync", "display-adrop");
          }

          // mpv/audio/out/ao_audiotrack.c
          // audiotrack crash with multiple players
          // Abort message: 'FORTIFY: pthread_mutex_lock called on a destroyed mutex'
          model.videoaudiotrack = model.preferences.getBoolean("videoaudiotrack", false);
          if (model.videoaudiotrack)
            mysurf.mpvlib.setOptionString("ao", "audiotrack,opensles");
          else
            mysurf.mpvlib.setOptionString("ao", "opensles");

          // Setting --vd-queue-enable=yes can help a lot to make playback smooth (once it works).
          model.videoqueue = model.preferences.getBoolean("videoqueue", false);
          if (model.videoqueue) {
            mysurf.mpvlib.setOptionString("vd-queue-enable", "yes");
            mysurf.mpvlib.setOptionString("ad-queue-enable", "yes");
            mysurf.mpvlib.setOptionString("vd-queue-max-bytes", "2000MiB");
            mysurf.mpvlib.setOptionString("ad-queue-max-bytes", "2000MiB");
          }

          model.videoscale = model.preferences.getBoolean("videoscale", false);
          if (model.videoscale) {
            //mysurf.mpvlib.setOptionString("vf-add", "scale=-1:360:flags=fast_bilinear"); // fast_bilinear ou neighbour
            mysurf.mpvlib.setOptionString("scale", "bilinear");
            mysurf.mpvlib.setOptionString("cscale", "bilinear");
            mysurf.mpvlib.setOptionString("dscale", "bilinear");
            mysurf.mpvlib.setOptionString("scale-antiring", "0");
            mysurf.mpvlib.setOptionString("cscale-antiring", "0");
            mysurf.mpvlib.setOptionString("dither-depth", "no");
            mysurf.mpvlib.setOptionString("correct-downscaling", "no");
            mysurf.mpvlib.setOptionString("sigmoid-upscaling", "no");
            mysurf.mpvlib.setOptionString("deband", "no");
          }

          model.videofpsasked = model.preferences.getInt("videofpsasked", 0);
          if (model.videofpsasked > 0)
            mysurf.mpvlib.setOptionString("fps", String.valueOf(model.videofpsasked));

          model.videofpsor = model.preferences.getBoolean("videofpsor", false);
          if (model.refreshrate > 0.0f && model.videofpsor) {
            mysurf.mpvlib.setOptionString("display-fps-override", String.valueOf(model.refreshrate));
          }

          model.videoprofile = model.preferences.getInt("videoprofile", model.videoprofile);
          if (model.videoprofile == 0)
            mysurf.mpvlib.setOptionString("profile", "fast");
          else if (model.videoprofile == 2)
            mysurf.mpvlib.setOptionString("profile", "high-quality");

          model.videofastdecode = model.preferences.getBoolean("videofastdecode", false);
          if (model.videofastdecode) {
            mysurf.mpvlib.setOptionString("vd-lavc-fast", "yes");
            mysurf.mpvlib.setOptionString("vd-lavc-skiploopfilter", "nonkey");
          }

          model.videoaaspectresize = model.preferences.getBoolean("videoaaspectresize", model.videoaaspectresize);
          //if (nworder[1].equals("restartplayingnohw")) ben non
          //  model.videoaaspectresize = false;

          model.videovthreadcount = model.preferences.getInt("videovthreadcount", model.videovthreadcount);
          if (model.videovthreadcount > 0)
            mysurf.mpvlib.setOptionString("vd-lavc-threads", String.valueOf(model.videovthreadcount));
          model.videoathreadcount = model.preferences.getInt("videoathreadcount", model.videoathreadcount);
          if (model.videoathreadcount > 0)
            mysurf.mpvlib.setOptionString("ad-lavc-threads", String.valueOf(model.videoathreadcount));

          mysurf.mpvlib.setOptionString("tls-verify", "yes");
          mysurf.mpvlib.setOptionString("tls-ca-file", model.dossierconfig + "/cacert.pem");
          mysurf.mpvlib.setOptionString("input-default-bindings", "yes");

          model.videoseekmode = model.preferences.getInt("videoseekmode", model.videoseekmode);
          model.videovolume = model.preferences.getInt("videovolume", model.videovolume);
          mysurf.mpvlib.setOptionString("volume", String.valueOf(model.videovolume));

          model.videolastaudiodelay = model.preferences.getFloat("videolastaudiodelay", model.videolastaudiodelay);
          model.videolastsubtitledelay = model.preferences.getFloat("videolastsubtitledelay", model.videolastsubtitledelay);
          model.videoaudiodelay = model.preferences.getFloat("videoaudiodelay", model.videoaudiodelay);
          mysurf.mpvlib.audiodelay = model.videoaudiodelay;
          if (model.videoaudiodelay != 0)
            mysurf.mpvlib.setOptionString("audio-delay", String.format("%.3f", model.videoaudiodelay));

          model.videosubfontsize = model.preferences.getInt("videosubfontsize", model.videosubfontsize);
          mysurf.mpvlib.setOptionString("sub-font-size", String.valueOf(model.videosubfontsize));

          mysurf.mpvlib.setOptionString("save-position-on-quit", "no");
          mysurf.mpvlib.setOptionString("force-window", "no");
          mysurf.mpvlib.setOptionString("idle", "yes"); // ne jamais recevoir les shutdowns sinon ça merde

          mysurf.mpvlib.attachSurface(mysurf.mpvSurfaceHolder.getSurface());
          mysurf.mpvlib.framewidth = mysurf.mywidth;
          mysurf.mpvlib.frameheight = mysurf.myheight;

          mysurf.mpvlib.setProperty("android-surface-size", mysurf.mywidth + "x" + mysurf.myheight);

          mysurf.mpvlib.init();

          // il faut commencer par ça
          mysurf.videoShowOverlay = false;
          mysurf.videoShowProgressBar = false;
          mysurf.videoShowOptionMenu = false;
          mysurf.videoShowLog = false;
          mysurf.videoShowSub = false;

          //model.addvideotemps(nworder[2]);
          if (nworder[2].equals("asmusic")) {

              /*llog.d(TAG, "play audio " + nworder[3]);
              int idzic = 0;
              Surf surfzic = model.surf.get(idzic);

              if (surfzic.mpvlib == null) {
                surfzic.mpvlib = new MPVLib(idzic);

                surfzic.mpvlib.setOptionString("ao", "opensles");
                surfzic.mpvlib.setOptionString("eof", "yes");
                surfzic.mpvlib.setOptionString("vid", "no");

                surfzic.mpvlib.init();

                surfzic.mpvlib.observeProperty("time-pos", MPVLib.mpvFormat.MPV_FORMAT_INT64);
                surfzic.mpvlib.observeProperty("duration", MPVLib.mpvFormat.MPV_FORMAT_INT64);
                surfzic.mpvlib.observeProperty("pause", MPVLib.mpvFormat.MPV_FORMAT_FLAG);
                surfzic.mpvlib.observeProperty("track-list", MPVLib.mpvFormat.MPV_FORMAT_STRING);
                surfzic.mpvlib.observeProperty("playlist-pos", MPVLib.mpvFormat.MPV_FORMAT_STRING);
                surfzic.mpvlib.observeProperty("playlist-count", MPVLib.mpvFormat.MPV_FORMAT_STRING);
                surfzic.mpvlib.observeProperty("volume", MPVLib.mpvFormat.MPV_FORMAT_STRING);
                surfzic.mpvlib.observeProperty("metadata", MPVLib.mpvFormat.MPV_FORMAT_STRING);
                surfzic.mpvlib.observeProperty("stream-path", MPVLib.mpvFormat.MPV_FORMAT_STRING);
              }

              boolean doappend = false;
              if (nworder[4].equals("append"))
                doappend = true;
              llog.d(TAG, "append " + doappend);

              surfzic.mpvlib.loadfile(nworder[3], doappend);*/

          } else {

            mysurf.mpvlib.lastfileadded = nworder[2];
            if (nworder.length > 3) {
              mysurf.mpvlib.lastfolderadded = nworder[3];
              int d = model.findFolder(mysurf.mpvlib.lastfolderadded);
              int f = model.findMediaAddress(d, mysurf.mpvlib.lastfileadded);
              if (d >= 0 && f >= 0) {
                mysurf.mpvlib.trackposition =  model.getMediaPlayStartAtPosition(d, f);
                mysurf.mpvlib.subtitles = new String[0];
                mysurf.mpvlib.subtitlespprint = new String[0];
                String[] subtitles = model.getMediaSubtitleAddress(d, f, mysurf.mpvlib.lastfileadded);
                if (subtitles != null) {
                  int sl = subtitles.length;
                  if (sl > 0) {
                    mysurf.mpvlib.subtitles = subtitles;
                    mysurf.mpvlib.subtitlespprint = new String[sl];
                    for (int i = 0; i < sl; i++) {
                      mysurf.mpvlib.subtitlespprint[i] = Gallery.ifUrlDecode(subtitles[i]);
                    }
                  }
                }
                mysurf.mpvlib.playInSequence = model.getMediaPlayInSequence(d, f);
                if (mysurf.mpvlib.playInSequence) {
                  mysurf.mpvlib.setOptionString("eof", "yes");
                  mysurf.mpvlib.setOptionString("prefetch-playlist", "yes");
                }
              }
            }
            if (nworder[2].startsWith("playlist://")) {
              new DownloadFileFromURL().execute(nworder[2].substring("playlist://".length()));
            } else if (nworder[2].startsWith("torrent://")) {
              new DownloadFileFromTorrent().execute(nworder[2].substring("torrent://".length()));
            } else if (nworder[2].startsWith("preload://")) {
              String reallink = nworder[2].substring("preload://".length());
              String[] headers = InternetSession.gethtmlheaders(model, reallink);
              if (headers.length == 0) {
                llog.d(TAG, "Error no file nor subtitle found... bail out.");
              } else if (headers.length == 1) {
                llog.d(TAG, "No subtitle available.");
                mysurf.mpvlib.loadfile(headers[0], false);
              } else {
                llog.d(TAG, "got " + (headers.length - 1) + " subtitles -> load file now " + headers[0]);
                mysurf.mpvlib.loadfile(headers[0], false);
                mysurf.mpvlib.subtitles = headers;
                mysurf.mpvlib.subtitlespprint = new String[headers.length];
                for (int i = 0; i < headers.length; i++) {
                  String ecritoption = mysurf.mpvlib.subtitles[i];
                  int lastslash = ecritoption.lastIndexOf('/');
                  ecritoption = ecritoption.substring(lastslash + 1);
                  try {
                    ecritoption = URLDecoder.decode(ecritoption, "UTF-8");
                  } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                  }
                  mysurf.mpvlib.subtitlespprint[i] = ecritoption;
                }
              }
            } else {
              mysurf.mpvlib.loadfile(nworder[2], false);
            }
          }

          encore = false;

        } else if (nworder[1].equals("poll")) {

          // ne pas dépendre de currid ici ça peut venir sans currid
          if (mysurf.fragmenttype != Surf.FRAGMENT_TYPE_VIDEO)
            continue;
          MPVLib mympvlib = mysurf.mpvlib;
          if (mympvlib == null)
            continue;
          if (mympvlib.isshutdown)
            continue;
          mympvlib.getdemuxercachestate();

          /*if (mympvlib != null) { // videos with wrong duration
            if (mympvlib.delayingaudiostart >= 60) {
              llog.d(TAG, "delayingaudiostart " + mympvlib.delayingaudiostart);
              mympvlib.setaid(0);
              mympvlib.setaid(1);
              mympvlib.delayingaudiostart = -1;
            }
          }*/

          if (failedtoaappendnextsequence) {
            long newt = System.currentTimeMillis();
            if (newt - failedtoaappendnextsequencet > 30000) {
              failedtoaappendnextsequencet = newt;
              if (mympvlib.lastfileadded == null || mympvlib.lastfolderadded == null) {
                llog.d(TAG, "lastfileaddedd lastfileaddedf " + mympvlib.lastfileadded + " " + mympvlib.lastfolderadded);
              } else {
                int d = model.findFolder(mympvlib.lastfolderadded);
                int f = model.findMediaAddress(d, mympvlib.lastfileadded);
                if (d == -1 || f == -1)
                  llog.d(TAG, "folder file not found " + d + " " + f + " " + mympvlib.lastfileadded + " " + mympvlib.lastfolderadded);
                else {
                  int mediatype = model.getMediaType(d, f + 1);
                  if (mediatype == Media.media_video || mediatype == Media.media_music) {
                    String thisurl = model.getMediaAddress(d, f + 1);
                    if (thisurl == null) {
                      failedtoaappendnextsequence = true;
                      failedtoaappendnextsequencet = System.currentTimeMillis();
                      llog.d(TAG, "failedtoaappendnextsequence no more video to enqueue we reached the end of the playlist.");
                    } else {
                      mympvlib.clearplaylist();
                      mympvlib.loadfile(thisurl, true);
                      failedtoaappendnextsequence = false;
                      String prevurl = model.getMediaAddress(d, f - 1);
                      if (prevurl != null) {
                        mympvlib.loadfilesetat0(prevurl);
                      }
                      String getplaylist = mympvlib.getplaylist();
                      llog.d(TAG, "really loading next video : " + d + " " + f + " f=" + prevurl + " < (" + getplaylist + " " + mympvlib.lastfileadded + ") < " + thisurl);
                    }
                  } else {
                    failedtoaappendnextsequence = true;
                    failedtoaappendnextsequencet = System.currentTimeMillis();
                    llog.d(TAG, "failedtoaappendnextsequence no more video to enqueue.");
                  }
                }
              }
            }
          }

          ArrayList<Integer> rc = mympvlib.getEvents(buffer);
          int rcl = rc.size();
          if (rcl == 0) {
            //if (!mysurf.videoShowProgressBar && !mysurf.videoShowLog && !mysurf.videoShowSub && !mysurf.videoShowOverlay)
            if (!mysurf.videoShowProgressBar && !mysurf.videoShowLog)
              continue; // do not draw something aftewards
          } else {
            for (int i = 0; i < rcl; i++) {
              int rcg = rc.get(i);
              if (rcg == MPVLib.EVENT_DISPLAY_SUB) {
                // draw sub

              } else if (rcg == MPVLib.EVENT_LOOK_LAST_PLAYED_POSITION) {
                if (mympvlib != null) {
                  if (mympvlib.streampath == null) {
                    llog.d(TAG, "EVENT_LOOK_LAST_PLAYED_POSITION mympvlib.streampath == null");
                    mympvlib.getstreampath();
                  }
                  if (mympvlib.lastfileadded != null) {
                    if (!mympvlib.lastfileadded.equals(mympvlib.streampath)) {
                      llog.d(TAG, "EVENT_LOOK_LAST_PLAYED_POSITION !mympvlib.lastfileadded.equals(mympvlib.streampath) " + mympvlib.lastfileadded + " " + mympvlib.streampath);
                      mympvlib.getstreampath();
                    }
                  }
                  mympvlib.lastplayedposition = 0;
                  if (mympvlib.streampath != null) {
                    int storetopos = -1;
                    int posstoredcount = model.preferences.getInt("videoposstorecount", 0);
                    for (int j = 0; j < posstoredcount; j++) {
                      if (model.preferences.getString("video" + j + "name", "crap").equals(mympvlib.streampath)) {
                        storetopos = j;
                        break;
                      }
                    }
                    if (storetopos >= 0) {
                      int wantedpos = model.preferences.getInt("video" + storetopos + "timepos", 0);
                      mympvlib.lastplayedposition = wantedpos;
                    }
                  }
                }
                llog.d(TAG, "EVENT_LOOK_LAST_PLAYED_POSITION lastfileadded " + mympvlib.lastfileadded + " = "
                        + mympvlib.lastplayedposition + " " + mympvlib.streampath);

              } else if (rcg == MPVLib.EVENT_GOT_VIDEO_FORMAT_CAN_CORRECT) {
                llog.d(TAG, "EVENT_GOT_VIDEO_FORMAT_CAN_CORRECT"
                    + " surf " + mysurf.mywidth + "x" + mysurf.myheight
                    + " screen " + mysurf.myscreenwidth + "x" + mysurf.myscreenheight
                    + " frame " + mympvlib.framewidth + "x" + mympvlib.frameheight
                    + " video " + mympvlib.videowidth + "x" + mympvlib.videoheight
                    + " demux " + mympvlib.demuxwidth + "x" + mympvlib.demuxheight
                );
                mympvlib.setaiddefault();
                llog.d(TAG, "    ----   set aid default, set sid " + mympvlib.sid + " " + mysurf.videoShowSub + " " + POLL_TIMEOUT);
                if (mympvlib.sid > 0) {
                  llog.d(TAG, "    ----   2 set sid " + mympvlib.sid);
                  //mympvlib.setsid(mympvlib.sid);
                  if (model.videohardwaremode == Gallery.videohwaamodehwpp) {
                    mysurf.videoShowOverlay = true;
                    mysurf.videoShowProgressBar = false;
                    mysurf.videoShowSub = true;
                    POLL_TIMEOUT = Gallery.polltimeoutfast;
                  }
                }
                if (model.videoaaspectresize) {
                  if (0 < mympvlib.eventFatalNoVideo) {
                    llog.d(TAG, "EVENT_GOT_VIDEO_FORMAT_CAN_CORRECT NOTvideoaaspectresize eventFatalNoVideo " + mympvlib.eventFatalNoVideo + " ignore");
                    try {
                      model.commandethreadvideo.put(new String[]{String.valueOf(currid), "videoaaspectresize", "true"});
                    } catch (InterruptedException e) {
                      e.printStackTrace();
                    }
                  } else {
                    llog.d(TAG, "EVENT_GOT_VIDEO_FORMAT_CAN_CORRECT videoaaspectresize");
                    try {
                      model.commandethreadvideo.put(new String[]{String.valueOf(currid), "videoaaspectresize"});
                    } catch (InterruptedException e) {
                      e.printStackTrace();
                    }
                  }
                } else {
                  llog.d(TAG, "EVENT_GOT_VIDEO_FORMAT_CAN_CORRECT NO videoaaspectresize");
                }
                if (model.surfzappernumber > 0) {
                  if (model.surfzapperoriginal >= 0 && model.surfzapperplayer >= 0) {
                    if (currid == model.surfzapperplayer) {
                      llog.d(TAG, "EVENT_GOT_VIDEO_FORMAT_CAN_CORRECT surfzapperoriginal setmutefakevolume");
                      Surf surfremet = model.surf.get(model.surfzapperoriginal);
                      if (surfremet.mpvlib != null)
                        if (!surfremet.mpvlib.ismutedfakevolume) {
                          surfremet.mpvlib.setmutefakevolume(true);
                        }
                    }
                  }
                }

              } else if (rcg == MPVLib.EVENT_START_FILE_APPEND_NEXT_SEQUENCE) {
                failedtoaappendnextsequence = false;
                if (mympvlib != null) {
                  String getplaylist = mympvlib.getplaylist();
                  if (mympvlib.eventFinishedFailed != 0) {
                    llog.d(TAG, "EVENT_START_FILE_APPEND_NEXT_SEQUENCE do not load next video " + mympvlib.eventFinishedFailed + " : " + mympvlib.eventFatalNoVideo);
                  } else if (model.videoloop == Gallery.videoloopSequence || mympvlib.playInSequence) {
                    llog.d(TAG, "EVENT_START_FILE_APPEND_NEXT_SEQUENCE load next video " + mympvlib.eventFinishedFailed + " : " + mympvlib.eventFatalNoVideo);
                    if (mympvlib.lastfileadded == null || mympvlib.lastfolderadded == null) {
                      llog.d(TAG, "EVENT_START_FILE_APPEND_NEXT_SEQUENCE lastfileaddedd lastfileaddedf " + mympvlib.lastfileadded + " " + mympvlib.lastfolderadded);
                    } else {
                      int d = model.findFolder(mympvlib.lastfolderadded);
                      int f = model.findMediaAddress(d, mympvlib.lastfileadded);
                      if (d == -1 || f == -1)
                        llog.d(TAG, "folder file not found " + d + " " + f + " " + mympvlib.lastfileadded + " " + mympvlib.lastfolderadded);
                      else {

                        llog.d(TAG, "EVENT_START_FILE_APPEND_NEXT_SEQUENCE playlist pos " + mympvlib.playlistpos + " count " + mympvlib.playlistcount
                                + " : " + d + "," + f + " current " + mympvlib.lastfileadded + " trackpos " + mympvlib.trackposition);
                        if (mympvlib.playlistcount > 1 && mympvlib.playlistpos == 0)
                          f -= 1;
                        if (mympvlib.playlistcount > 2 && mympvlib.playlistpos == 2)
                          f += 1;
                        mympvlib.lastfileadded = model.getMediaAddress(d, f);
                        model.changeBigPicture(currid, d, 0, f, 0, true, false);
                        mysurf.centeronscreen = true;

                        mympvlib.trackposition = model.getMediaPlayStartAtPosition(d, f);
                        mympvlib.subtitles = new String[0];
                        mympvlib.subtitlespprint = new String[0];
                        String[] subtitles = model.getMediaSubtitleAddress(d, f, mysurf.mpvlib.lastfileadded);
                        if (subtitles != null) {
                          int sl = subtitles.length;
                          if (sl > 0) {
                            mympvlib.subtitles = subtitles;
                            mympvlib.subtitlespprint = new String[sl]; // même thread ok
                            for (int ii = 0; ii < sl; ii++)
                              mympvlib.subtitlespprint[ii] = Gallery.ifUrlDecode(subtitles[ii]);
                          }
                        }
                        llog.d(TAG, "EVENT_START_FILE_APPEND_NEXT_SEQUENCE playlist pos " + mympvlib.playlistpos + " count " + mympvlib.playlistcount
                                + " : " + d + "," + f + " changeBigPicture to : " + mympvlib.lastfileadded + " trackpos " + mympvlib.trackposition);

                        int mediatype = model.getMediaType(d, f + 1);
                        if (mediatype == Media.media_video || mediatype == Media.media_music) {
                          String thisurl = model.getMediaAddress(d, f + 1);
                          if (thisurl == null) {
                            failedtoaappendnextsequence = true;
                            failedtoaappendnextsequencet = System.currentTimeMillis();
                            llog.d(TAG, "EVENT_START_FILE_APPEND_NEXT_SEQUENCE failedtoaappendnextsequence no more video to enqueue we reached the end of the playlist.");
                          } else {
                            mympvlib.clearplaylist();
                            mympvlib.loadfile(thisurl, true);
                            failedtoaappendnextsequence = false;
                            String prevurl = model.getMediaAddress(d, f - 1);
                            if (prevurl != null) {
                              mympvlib.loadfilesetat0(prevurl);
                            }
                            getplaylist = mympvlib.getplaylist();
                            llog.d(TAG, "EVENT_START_FILE_APPEND_NEXT_SEQUENCE really loading next video : " + d + " " + f + " f="
                                    + prevurl + " < (" + getplaylist + " " + mympvlib.lastfileadded + ") < " + thisurl + " " + mympvlib.lastfileadded);
                          }
                        } else {
                          failedtoaappendnextsequence = true;
                          failedtoaappendnextsequencet = System.currentTimeMillis();
                          llog.d(TAG, "EVENT_START_FILE_APPEND_NEXT_SEQUENCE failedtoaappendnextsequence no more video to enqueue.");
                        }
                      }
                    }
                  } else {
                    llog.d(TAG, "EVENT_START_FILE_APPEND_NEXT_SEQUENCE do not load next video");
                  }
                }



              } else if (rcg == MPVLib.EVENT_END_FILE) {
                if (mympvlib.eventFinishedFailed > 0) {

                  mympvlib.eventFinishedFailed = 0;
                  if (0 < mympvlib.eventFatalNoVideo) {
                    llog.d(TAG, "EVENT_END_FILE eventFinishedFailed " + mympvlib.eventFinishedFailed + " eventFatalNoVideo " + mympvlib.eventFatalNoVideo);
                    try {
                      model.commandethreadvideo.put(new String[]{String.valueOf(currid), "restartplayingnohw", mympvlib.lastfileadded, mympvlib.lastfolderadded});
                    } catch (InterruptedException e) {
                      e.printStackTrace();
                    }

                  } else if (model.surfzappernumber == 0) {
                    llog.d(TAG, "EVENT_END_FILE eventFinishedFailed " + mympvlib.eventFinishedFailed + " surfzappernumber == 0");
                    if (mysurf.fragmenttype == Surf.FRAGMENT_TYPE_VIDEO) {
                      try {
                        model.commandethreadvideo.put(new String[]{String.valueOf(currid), "startbrowser"});
                      } catch (InterruptedException e) {
                      }
                    }

                  } else {
                    llog.d(TAG, "EVENT_END_FILE eventFinishedFailed failed to load video");
                    if (model.surfzapperoriginal >= 0) {
                      Surf surfremet = model.surf.get(model.surfzapperoriginal);
                      if (surfremet.mpvlib != null)
                        if (surfremet.mpvlib.ismutedfakevolume)
                          surfremet.mpvlib.setmutefakevolume(false);
                    }
                  }

                } else if (model.videoloop == Gallery.videoloopNoLoop
                        && mysurf.optiondiaporamaactive
                        && model.surfzappernumber == 0) {
                  llog.d(TAG, "EVENT_END_FILE quit video start browser");
                  if (mysurf.fragmenttype == Surf.FRAGMENT_TYPE_VIDEO) {
                    try {
                      model.commandethreadvideo.put(new String[]{String.valueOf(currid), "startbrowser"});
                    } catch (InterruptedException e) {
                    }
                  }
                } else if (model.videoloop == Gallery.videoloopSequence) {
                  llog.d(TAG, "EVENT_END_FILE videoloopSequence");
                } else {
                  llog.d(TAG, "EVENT_END_FILE ignored");
                }
              }



            }
          }
          encore = false;

        } else if (nworder[1].equals("reloadthisfolder")) {
          encore = false;

        } else if (nworder[1].equals("menudessiner")) {
          /**
           *    on attrape tous les menudessiner init on n'en rate jamais un seul
           *    move on en rate pour ne pas traîner
           */
          updateonefinaltime = true;
          encore = false;
        } else if (nworder[1].equals("option")) {
          /**
           *    on attrape tous les option on n'en rate jamais un seul
           */
          updateonefinaltime = true;
          encore = false;
        } else if (nworder[1].equals("next tv")) {
          /**
           *    on attrape tous les next tv on n'en rate jamais un seul
           */
          encore = false;
        } else if (nworder[1].equals("next")) {
          /**
           *    on attrape tous les next on n'en rate jamais un seul
           */
          encore = false;
        } else if (nworder[1].equals("dontmissupdate")) {
          /**
           *    on attrape tous les initupdate on n'en rate jamais un seul
           */
          llog.d(TAG, currid+" initupdate");
          encore = false;
        } else if (nworder[1].equals("event")) {
          /**
           *    on attrape tous les event on n'en rate jamais un seul
           */
          if (!mysurf.videoShowOverlay) { // si pas d'affichage pas besoin de rafraîchir le time-pos
            encore = true;
          }

          // ici on ne s'arre que si on veut redessiner dans cette instance
          //encore = false;
        } else if (model.commandethreadvideo.isEmpty() == true) {
          /**
           toutes les autres commandes :
           on attend que la queue soit vide
           et on ne prend que la dernière
           */
          encore = false;
        }
      }

      if (mysurf.ScreenWidth < 10 || mysurf.ScreenHeight < 10) {
        llog.d(TAG, currid + " Screen too small " + mysurf.ScreenWidth + " " + mysurf.ScreenHeight);
        continue;
      }

      if (mysurf.fragmenttype != Surf.FRAGMENT_TYPE_VIDEO) {
        llog.d(TAG, currid + " fragmenttype " + mysurf.fragmenttype);
        continue;
      }

      if (mysurf.mpvlib == null) {
        llog.d(TAG, currid + " mpvlib null=" + (mysurf.mpvlib == null) + " start browser instead");
        try {
          model.commandethreadvideo.put(new String[]{String.valueOf(currid), "startbrowser"});
        } catch (InterruptedException e) {
        }
        continue;
      }

      currmpvlib = mysurf.mpvlib;

      clicktohandle = false;
      clickquicknav = false;
      clickquicknavmenu = false;
      clickrightleft = 0;
      // pour ceux-là on attend que la liste soit vide et on ne prend que le dernier
      // on se permet donc d'en rater plein

      if (nworder[1].equals("next tv")) {
        if (nworder[2].equals("droite")) {
          clickrightleft = 1;
          if (mysurf.videoShowOptionMenu) {
            mysurf.cursorx = mysurf.SettingsXmin + mysurf.SettingsWidth * 0.80f;
            mysurf.cursory = model.settingsYmin + model.GenericCaseH * 0.5f;
          } else {
            currmpvlib.seek(model.videoseekmode, true);
          }
        } else if (nworder[2].equals("gauche")) {
          clickrightleft = -1;
          if (mysurf.videoShowOptionMenu) {
            mysurf.cursorx = mysurf.SettingsXmin + mysurf.SettingsWidth * 0.20f;
            mysurf.cursory = model.settingsYmin + model.GenericCaseH * 0.5f;
          } else {
            if (currmpvlib.seek(model.videoseekmode, false))
              model.message("preceding video loaded");
          }
        } else if (nworder[2].equals("haut")) {
          if (mysurf.videoShowOptionMenu) {
            if (mysurf.videoShowLog) {
              if (mysurf.videoshowlogfrom - 1 >= 0) {
                mysurf.videoshowlogfrom -= 1;
              }
            } else {
              mysurf.SettingsYmin += model.GenericCaseH + model.GenericInterSpace;
            }
          } else if (mysurf.videoShowProgressBar) {
            mysurf.videoShowProgressBar = false;
            if (mysurf.videoShowOverlay != mysurf.videoShowProgressBar
                    && !mysurf.videoShowOptionMenu
                    && !mysurf.videoShowSub) {
              mysurf.videoShowOverlay = mysurf.videoShowProgressBar;
              if (!mysurf.videoShowOverlay)
                model.startVideoClearBrowserSurfaceTransparent = true;
            }
          } else {
            Intent intent = new Intent();
            intent.setAction(Gallery.broadcastname);
            intent.putExtra("goal", "splitscreenzapping");
            intent.putExtra("splitnumber", 2);
            intent.putExtra("splitsurf", currid);
            intent.putExtra("splitratio", 0.50f);
            intent.putExtra("splitvertical", false);
            LocalBroadcastManager.getInstance(model.activitycontext).sendBroadcast(intent);
          }
        } else if (nworder[2].equals("bas")) {
          if (mysurf.videoShowOptionMenu) {
            if (mysurf.videoShowLog) {
              if (mysurf.videoshowlogfrom + 1 < currmpvlib.log.size()) {
                mysurf.videoshowlogfrom += 1;
              }
            } else {
              mysurf.SettingsYmin -= model.GenericCaseH + model.GenericInterSpace;
            }
          } else {
            mysurf.videoShowProgressBar = !mysurf.videoShowProgressBar;
            if (mysurf.videoShowOverlay != mysurf.videoShowProgressBar
                    && !mysurf.videoShowOptionMenu
                    && !mysurf.videoShowSub) {
              mysurf.videoShowOverlay = mysurf.videoShowProgressBar;
              if (!mysurf.videoShowOverlay)
                model.startVideoClearBrowserSurfaceTransparent = true;
            }
          }
        } else if (nworder[2].equals("ok")) {
          if (mysurf.videoShowOptionMenu) {
            if (mysurf.cursorx == -1.0f) {
              mysurf.cursorx = mysurf.SettingsXmin + mysurf.SettingsWidth * 0.20f;
            }
            mysurf.cursory = model.settingsYmin + model.GenericCaseH * 0.5f;
            clickquicknavmenu = true;
            clicktohandlex = mysurf.cursorx;
            clicktohandley = mysurf.cursory;
          } else {
            currmpvlib.cyclepause();
          }
        } else if (nworder[2].equals("menu")) {
          // déjà fait dans main avant de créer la surface
          //mysurf.videoshowoptions = !mysurf.videoshowoptions;
          //mysurf.SettingsYmin = model.settingsYmin;
          mysurf.videoShowOptionMenu = !mysurf.videoShowOptionMenu;
          if (mysurf.videoShowOverlay != mysurf.videoShowOptionMenu
                  && !mysurf.videoShowProgressBar
                  && !mysurf.videoShowSub) {
            mysurf.videoShowOverlay = mysurf.videoShowOptionMenu;
            if (!mysurf.videoShowOverlay)
              model.startVideoClearBrowserSurfaceTransparent = true;
          }
          if (mysurf.videoShowOptionMenu) {
            //mysurf.SettingsYmin = model.settingsYmin;
            mysurf.cursorx = mysurf.SettingsXmin + mysurf.SettingsWidth * 0.20f;
            mysurf.cursory = model.settingsYmin + model.GenericCaseH * 0.5f;
          } else {
            mysurf.cursorx = mysurf.mywidth * 0.5f;
            mysurf.cursory = mysurf.myheight * 0.5f;
          }
          if (mysurf.videoShowOptionMenu) {
            currmpvlib.aspectCheckIfVideoOrScreen();
          }
        }
      }

      if (nworder[1].equals("update")) {
      } else if (nworder[1].equals("move")) {
      } else if (nworder[1].equals("rescale")) {
        //llog.d(TAG, "rescale " + currmpvlib.videozoom + " : " + mysurf.mywidth + "x" + mysurf.myheight);
        currmpvlib.setzoom(currmpvlib.videozoom);
        currmpvlib.setvideopanx(currmpvlib.videopanx);
        currmpvlib.setvideopany(currmpvlib.videopany);
        if (!currmpvlib.maxout) {
          currmpvlib.maxout = true;
          //resizevideoframe(currmpvlib, true);
        }
      } else if (nworder[1].equals("next")) {
        float posfx = Float.parseFloat(nworder[2]);
        float posfy = Float.parseFloat(nworder[3]);
        if (mysurf.videoShowOptionMenu && posfx > mysurf.SettingsXmin && mysurf.SettingsYmin < posfy && posfy < mysurf.SettingsYmax) {
          /** click inside menu */
          clickquicknavmenu = true;
          clicktohandlex = posfx;
          clicktohandley = posfy;

        } else if (posfy < mysurf.myheight * 0.33f) {
          /** haut menu 3 fingers */
          if (mysurf.videoShowOptionMenu) {

          } else if (model.surfzappernumber > 0) {
            if (currid == model.surfzapperplayer) {
              Intent intent = new Intent();
              intent.setAction(Gallery.broadcastname);
              intent.putExtra("goal", "playvid");
              intent.putExtra("zapperplayer", true);
              LocalBroadcastManager.getInstance(model.activitycontext).sendBroadcast(intent);
            } else if (currid == model.surfzapperoriginal) {
              Intent intent = new Intent();
              intent.setAction(Gallery.broadcastname);
              intent.putExtra("goal", "playvid");
              intent.putExtra("zapperoriginal", true);
              LocalBroadcastManager.getInstance(model.activitycontext).sendBroadcast(intent);
            } else {
              llog.d(TAG, "ERR currid " + currid + " " + model.surfzappercommander + " " + model.surfzapperoriginal + " " + model.surfzapperplayer);
            }
          } else {
            Intent intent = new Intent();
            intent.setAction(Gallery.broadcastname);
            intent.putExtra("goal", "splitscreenzapping");
            intent.putExtra("splitnumber", 2);
            intent.putExtra("splitsurf", currid);
            intent.putExtra("splitratio", 0.50f);
            intent.putExtra("splitvertical", false);
            LocalBroadcastManager.getInstance(model.activitycontext).sendBroadcast(intent);
          }

        } else if (posfx < mysurf.mywidth * 0.33f                                         && mysurf.myheight * 0.33f < posfy && posfy < mysurf.myheight * 0.66f) {
          /** gauche seek - */
          if (currmpvlib.seek(model.videoseekmode, false))
            model.message("preceding video loaded");

        } else if (mysurf.mywidth * 0.33f < posfx && posfx < mysurf.mywidth * 0.66f       && mysurf.myheight * 0.33f < posfy && posfy < mysurf.myheight * 0.66f) {
          /** centre pause */
          currmpvlib.cyclepause();

        } else if (mysurf.mywidth * 0.66f < posfx                                          && mysurf.myheight * 0.33f < posfy && posfy < mysurf.myheight * 0.66f) {
          /** droite seek + */
          currmpvlib.seek(model.videoseekmode, true);


        } else if (mysurf.videoShowProgressBar && posfy > mysurf.myheight * Gallery.percentofvideoheightforprogressbarclick) {
          clickquicknav = true;
          clicktohandlex = posfx;
          clicktohandley = posfy;

        } else if (posfy > mysurf.myheight * 0.66f) {
          mysurf.videoShowProgressBar = !mysurf.videoShowProgressBar;
          if (mysurf.videoShowOverlay != mysurf.videoShowProgressBar
                  && !mysurf.videoShowOptionMenu
                  && !mysurf.videoShowSub) {
              mysurf.videoShowOverlay = mysurf.videoShowProgressBar;
            if (!mysurf.videoShowOverlay)
              model.startVideoClearBrowserSurfaceTransparent = true;
          }

        } else {
          clicktohandle = true;
          clicktohandlex = posfx;
          clicktohandley = posfy;
        }
      }

      if ((!mysurf.videoShowOverlay || !mysurf.videoShowOptionMenu) && mysurf.videoShowLog) { // impossible de fermer le log sinon à cause du reset de position androidTV cursor début,m mysurf.SettingsYmin = model.settingsYmin;
        mysurf.videoShowLog = false;
        mysurf.videoshowlogfrom = 0;
      }

      if (!mysurf.videoShowOverlay && !model.startVideoClearBrowserSurfaceTransparent) {
        continue;
      }

      //llog.d(TAG, "draw");

      //long tempsinitial = System.currentTimeMillis();
      drawall();
      /*long tempsdaffichage = System.currentTimeMillis() - tempsinitial;
      if (tempsdaffichage < 30) { // on a 16ms en moyenne on peut ralentir le bordel
        try {
          Thread.sleep(30-tempsdaffichage);
        } catch (InterruptedException e) {
          e.printStackTrace();
        }
      }*/

    }
  }

  private boolean drawall() {
    // model.mysurfacecanvas.drawColor(Color.TRANSPARENT, PorterDuff.Mode.CLEAR);
    // RectF rectprec = new RectF(model.suivantposx1, model.suivantposy1, model.suivantprecedent, model.suivantposy2); surfacecanvas.drawRoundRect(rectprec,(model.suivantposx2-model.suivantposx1)*0.2f, (model.suivantposy2-model.suivantposy1)*0.2f, model.paintgrey);

    //tempsinit[0] = tempsinitial;

    /**
    *   fond + miniatures + image principale
    *   + options
    */
    mysurf.surfaceIsCurrentlyDrawing = true;
    if (mysurf.mysurfacestopdrawing) {
      mysurf.surfaceIsCurrentlyDrawing = false;
      return false;
    }
    if (mysurf.browserSurfaceHolder == null) {
      mysurf.surfaceIsCurrentlyDrawing = false;
      return false;
    }
    Surface surface = mysurf.browserSurfaceHolder.getSurface();
    if (surface == null) {
      mysurf.surfaceIsCurrentlyDrawing = false;
      return false;
    }
    if (!surface.isValid()) {
      mysurf.surfaceIsCurrentlyDrawing = false;
      return false;
    }
    Canvas canv = null;
    try {
      if (model.optionhardwareaccelerationb && Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
          canv = mysurf.browserSurfaceHolder.lockHardwareCanvas();
      } else {
          canv = mysurf.browserSurfaceHolder.lockCanvas();
      }
    } catch (Exception e) {
      llog.d(TAG, "IllegalStateException " + e.toString());
      e.printStackTrace();
      canv = null;
      return false;
    }
    if (canv == null) {
      mysurf.surfaceIsCurrentlyDrawing = false;
      return false;
    }

    canv.drawColor(Color.TRANSPARENT, PorterDuff.Mode.CLEAR);
    if (model.startVideoClearBrowserSurfaceTransparent)
      model.startVideoClearBrowserSurfaceTransparent = false;
    drawthumbsbarname(canv);

    try {
      mysurf.browserSurfaceHolder.unlockCanvasAndPost(canv);
    } catch (Exception e) {
      llog.d(TAG, " drawall unlockCanvasAndPost " + e.toString());
    }
    mysurf.surfaceIsCurrentlyDrawing = false;

    /*
    if(tempscompte[0] == 100) {
      llog.d(TAG, "tempsdraw   : "+(((float)tempsdiff[0])/((float)tempscompte[0]+1)));
      tempscompte[0] = 0;
      tempsdiff[0] = 0;
    }
    */
    return true;
  }

  private boolean scalebitmaponcanvas(float ix0, float iy0, float iw, float ih, float scale, int screenwidth, int screenheight) {
    float ix1 = ix0 + iw * scale;
    float iy1 = iy0 + ih * scale;
    float fwidth = screenwidth;
    float fheight = screenheight;
    if (ix0 < fwidth && iy0 < fheight && ix1 >= 0 && iy1 >= 0) {
      float srcx0, srcy0, srcx1, srcy1;
      float dstx0, dsty0, dstx1, dsty1;
      if (ix0 >= 0.0f) {
        srcx0 = 0.0f;                           // dès le début
        if (ix1 <= fwidth) {
          srcx1 = iw;                                // jusqu'à la fin
          dstx1 = ix1;
        } else {
          srcx1 = (fwidth - ix0) / scale;            // pas jusqu'à la fin
          dstx1 = fwidth;
        }
        dstx0 = ix0;                                      // on placera en ix0
      } else {
        srcx0 = - ix0 / scale;                  // pas depuis le début
        if (ix1 <= fwidth) {
          srcx1 = iw;                                // jusqu'à la fin
          dstx1 = ix1;
        } else {
          srcx1 = srcx0 + fwidth / scale;            // pas juqu'à la fin
          dstx1 = fwidth;
        }
        dstx0 = 0.0f;                                      // on placera en 0
      }
      if (iy0 >= 0.0f) {
        srcy0 = 0.0f;                           // dès le début
        if (iy1 <= fheight) {
          srcy1 = ih;                                // jusqu'à la fin
          dsty1 = iy1;
        } else {
          srcy1 = (fheight - iy0) / scale;            // pas jusqu'à la fin
          dsty1 = fheight;
        }
        dsty0 = iy0;                                      // on placera en iy0
      } else {
        srcy0 = - iy0 / scale;                  // pas depuis le début
        if (iy1 <= fheight) {
          srcy1 = ih;                                // jusqu'à la fin
          dsty1 = iy1;
        } else {
          srcy1 = srcy0 + fheight / scale;            // pas juqu'à la fin
          dsty1 = fheight;
        }
        dsty0 = 0.0f;                                      // on placera en 0
      }

      mysurf.srcrect = new Rect((int) Math.floor(srcx0),
          (int) Math.floor(srcy0),
          (int) Math.floor(srcx1),
          (int) Math.floor(srcy1));
      mysurf.dstrect = new RectF(dstx0, dsty0, dstx1, dsty1);
      return true;
    }
    return false;
  }

  private volatile float []audiodelaypower = new float[]{0.1f, 1.0f, 10.0f, 0.001f, 0.01f};
  private volatile int audiodelaypoweri = 0;
  private volatile float []subtitledelaypower = new float[]{0.1f, 1.0f, 10.0f, 0.001f, 0.01f};
  private volatile int subtitledelaypoweri = 0;
  private volatile float []speedpower = new float[]{0.1f, 1.0f, 10.0f, 0.01f};
  private volatile int speedpoweri = 0;
  private volatile float []zoompower = new float[]{0.1f, 1.0f, 10.0f, 0.01f};
  private volatile int zoompoweri = 0;

  private float mabase;
  private float roundedRectRatio;
  private float quatredixiememillimetre;
  private float y1, y2, taillex, x1, x2, ecritx, ecrity;
  private String ecritoption = "";
  private RectF recti;
  private boolean needanotherupdate;
  private int premierdossieraffiche = -1;
  private int dernierdossieraffiche = -1;
  private boolean askloadthumbnails = false;

  private boolean drawthumbsbarname(Canvas surfacecanvas) {

    premierdossieraffiche = -1;
    dernierdossieraffiche = -1;
    needanotherupdate = false;
    askloadthumbnails = false;

    if (mysurf.mpvlib == null) {
      return false;
    }

    /***********************
     *
     *    NAVIGATION BAR
     *
     * *********************
     */
    if (mysurf.videoShowOptionMenu) {

      mabase = mysurf.SettingsYmin;
      roundedRectRatio = model.GenericCaseH * 0.2f;
      quatredixiememillimetre = model.GenericCaseH * 0.12f;

      if (mabase < mysurf.myheight)
        menuseek(surfacecanvas);
      if (mabase < mysurf.myheight)
        menuquit(surfacecanvas);
      if (mabase < mysurf.myheight)
        menulastposition(surfacecanvas);
      if (mabase < mysurf.myheight)
        menutrackposition(surfacecanvas);
      if (mabase < mysurf.myheight)
        menuplaylist(surfacecanvas);
      if (mabase < mysurf.myheight)
        menuvideo(surfacecanvas);
      if (mabase < mysurf.myheight)
        menuaudio(surfacecanvas);
      if (mabase < mysurf.myheight)
        menusubtitles(surfacecanvas);
      if (mabase < mysurf.myheight)
        menuextsubtitle(surfacecanvas);
      if (mabase < mysurf.myheight)
        menuabloop(surfacecanvas);
      if (mabase < mysurf.myheight)
        menuvratio(surfacecanvas);
      //if (mabase < mysurf.myheight)
      //  menudelay(surfacecanvas);
      if (mabase < mysurf.myheight)
        menuaudiodelay(surfacecanvas);
      if (mabase < mysurf.myheight)
        menusubtitledelay(surfacecanvas);
      if (mabase < mysurf.myheight)
        menuspeed(surfacecanvas);
      if (mabase < mysurf.myheight)
        menuloop(surfacecanvas);
      if (mabase < mysurf.myheight)
        menuhwdec(surfacecanvas);
      if (mabase < mysurf.myheight)
        menuframemod(surfacecanvas);
      if (mysurf.optionvideoframemod) {
        //if (mabase < mysurf.myheight)
        //  menurotate(surfacecanvas);
        if (mabase < mysurf.myheight)
          menuzoom(surfacecanvas);
        if (mabase < mysurf.myheight)
          menupanx(surfacecanvas);
        if (mabase < mysurf.myheight)
          menupany(surfacecanvas);
        if (mabase < mysurf.myheight)
          menumodify(surfacecanvas);
        //if (mabase < mysurf.myheight)
        //  menubrightness(surfacecanvas);
        //if (mabase < mysurf.myheight)
        //  menucontrast(surfacecanvas);
        //if (mabase < mysurf.myheight)
        //  menusaturation(surfacecanvas);
        //if (mabase < mysurf.myheight)
        //  menugamma(surfacecanvas);
        //if (mabase < mysurf.myheight)
        //  menuhue(surfacecanvas);
        //if (mabase < mysurf.myheight)
        //  menumodifydelta(surfacecanvas);
      }
      //if (mabase < mysurf.myheight)
      //  menushowlog(surfacecanvas);
      //if (mysurf.optionvideolog) {
        if (mabase < mysurf.myheight)
          menulog(surfacecanvas);
        //if (mabase < mysurf.myheight)
        //  menuluatoggle(surfacecanvas);
      //}
      if (mabase < mysurf.myheight)
        menutweak(surfacecanvas);
      if (mysurf.optionvideotweak) {
        if (mabase < mysurf.myheight)
          menuswcopy(surfacecanvas);
        if (mabase < mysurf.myheight)
          menuvolume(surfacecanvas);
        if (mabase < mysurf.myheight)
          menuvideoprofile(surfacecanvas);
        if (mabase < mysurf.myheight)
          menuvsync(surfacecanvas);
        if (mabase < mysurf.myheight)
          menufps(surfacecanvas);
        if (mabase < mysurf.myheight)
          menufpsasked(surfacecanvas);
        if (mabase < mysurf.myheight)
          menuaudiotrack(surfacecanvas);
        if (mabase < mysurf.myheight)
          menuqueue(surfacecanvas);
        if (mabase < mysurf.myheight)
          menuvideoscale(surfacecanvas);
        //if (mabase < mysurf.myheight)
        //  menuvideoperformance(surfacecanvas);
        if (mabase < mysurf.myheight)
          menuvideofastdecode(surfacecanvas);
        if (mabase < mysurf.myheight)
          menuvideoresize(surfacecanvas);
        if (mabase < mysurf.myheight)
          menucache(surfacecanvas);
        if (mabase < mysurf.myheight)
          menusubtitlesize(surfacecanvas);
        if (mabase < mysurf.myheight)
          menuthreads(surfacecanvas);
      }
      if (mabase < mysurf.myheight)
        menusplit(surfacecanvas);
      if (mabase < mysurf.myheight)
        menuscreenshot(surfacecanvas);
      //if (mabase < mysurf.myheight)
      //  menuscreenshotraw(surfacecanvas);

      if (mysurf.SettingsYmin > mysurf.myheight - model.GenericCaseH)
        mysurf.SettingsYmin = model.settingsYmin;
      mysurf.SettingsYmax = mabase;
      if (mabase < model.GenericCaseH)
        mysurf.SettingsYmin = model.settingsYmin;

    }


    if (mysurf.videoShowSub && currmpvlib.sid > 0) {
      //llog.d(TAG, "++++++++++++++++++++++++++++ print subtitles+++++++++++++++++++++++++++++++");
      int subtextl = currmpvlib.subtext.length();
      if (subtextl > 0) {

        Rect bounds = new Rect();
        model.VideoSubPaint.getTextBounds(currmpvlib.subtext, 0, subtextl, bounds);

        float tw = bounds.width();
        float th = bounds.height();
        float nl = tw / ((float) mysurf.mywidth * 0.50f);
        int ti = 0;
        int tl = (int) ((float) subtextl / nl);
        ArrayList<String> decomp = new ArrayList<>();
        for (int i = 0 ; ti < subtextl ; i++) {
          String txt;
          if (nl == 1) {
            txt = currmpvlib.subtext;
            ti += subtextl;
          } else {
            if (ti + tl > subtextl)
              tl = subtextl - ti;
            txt = currmpvlib.subtext.substring(ti, ti + tl);
            int tlp = 0;
            for (int j = 0 ; j < 20 && ti + tl + j < subtextl ; j++) {
              char car = currmpvlib.subtext.charAt(ti + tl + j);
              if (car == ' ' || car == '\t' || car == '\n') {
                break;
              } else {
                txt += car;
                tlp += 1;
              }
            }
            ti += tl + tlp;
          }
          decomp.add(txt);
        }
        int decompl = decomp.size();
        for (int i = 0 ; i < decompl ; i++) {
          surfacecanvas.drawText(decomp.get(i),
                  0.500f * mysurf.mywidth,
                  mysurf.myheight * 0.97f - th * 1.10f * (decompl - i - 1),
                  model.VideoSubPaint);
        }
      }
    }

    if (mysurf.videoShowLog || mysurf.videoShowProgressBar) {
      Rect bounds = new Rect();
      //llog.d(TAG, "mysurf.videoshowlog || mysurf.videoshowprogress");
      ArrayList<String> displaythat = new ArrayList<>();
      if (mysurf.videoShowLog) {
        //llog.d(TAG, "mysurf.videoshowlog");
        for (String key  : currmpvlib.mapmetadata.keySet()) {
          displaythat.add(key + " : " + currmpvlib.mapmetadata.get(key));
        }
        displaythat.addAll(currmpvlib.log);
      } else {
        if (currmpvlib.mapmetadata.containsKey("source")) {
          displaythat.add(currmpvlib.mapmetadata.get("source"));
        } else {
          String escaped = currmpvlib.streampath;
          if (Gallery.couldBeOnline(escaped)) {
            try {
              escaped = URLDecoder.decode(currmpvlib.streampath, "utf-8");
            } catch (UnsupportedEncodingException e) {
              e.printStackTrace();
            }
          }
          int lastslash = escaped.lastIndexOf('/');
          if (lastslash == -1 || lastslash == escaped.length()) {
            displaythat.add(escaped);
          } else {
            displaythat.add(escaped.substring(0, lastslash + 1));
            displaythat.add(escaped.substring(lastslash + 1));
          }
          if (currmpvlib.subtitles.length > 1) {
            displaythat.add((currmpvlib.subtitles.length-1) + " subs");
          }
        }
      }
      int displaythatl = displaythat.size();
      float inposy = model.GenericInterSpace;
      int j = 0;
      for (int i = mysurf.videoshowlogfrom; i < displaythatl; i++) {
        String displaytahts = displaythat.get(i);
        int displaytahtsl = displaytahts.length();
        if (displaytahtsl > 1 && inposy + model.GenericTextH < model.bigScreenHeight) {
          model.VideoTextPaintRight.getTextBounds(displaytahts, 0, displaytahtsl, bounds);
          if (bounds.width() < mysurf.mywidth) {
            RectF rect = new RectF(model.GenericInterSpace, model.GenericInterSpace + j * model.GenericTextH,
                    model.GenericInterSpace + bounds.width() + model.GenericInterSpace * 2.0f, model.GenericInterSpace + (j + 1) * model.GenericTextH);
            surfacecanvas.drawRect(rect, model.VideoTextBgPaint);
            surfacecanvas.drawText(displaytahts, model.GenericInterSpace * 2.0f, inposy - bounds.top, model.VideoTextPaintRight);
            inposy += model.GenericTextH;
            j += 1;
          } else {
            displaytahtsl = displaytahtsl / 2;
            String displaytahts1 = displaytahts.substring(0, displaytahtsl);
            model.VideoTextPaintRight.getTextBounds(displaytahts1, 0, displaytahtsl, bounds);
            RectF rect = new RectF(model.GenericInterSpace, model.GenericInterSpace + j * model.GenericTextH,
                    model.GenericInterSpace + bounds.width() + model.GenericInterSpace * 2.0f, model.GenericInterSpace + (j + 1) * model.GenericTextH);
            surfacecanvas.drawRect(rect, model.VideoTextBgPaint);
            surfacecanvas.drawText(displaytahts1, model.GenericInterSpace * 2.0f, inposy - bounds.top, model.VideoTextPaintRight);
            inposy += model.GenericTextH;
            j += 1;
            float decalx = model.GenericCaseH * 2.0f;
            displaytahts1 = displaytahts.substring(displaytahtsl);
            model.VideoTextPaintRight.getTextBounds(displaytahts1, 0, displaytahts1.length(), bounds);
            rect = new RectF(model.GenericInterSpace + decalx, model.GenericInterSpace + j * model.GenericTextH,
                    model.GenericInterSpace + bounds.width() + model.GenericInterSpace * 2.0f + decalx, model.GenericInterSpace + (j + 1) * model.GenericTextH);
            surfacecanvas.drawRect(rect, model.VideoTextBgPaint);
            surfacecanvas.drawText(displaytahts1, model.GenericInterSpace * 2.0f + decalx, inposy - bounds.top, model.VideoTextPaintRight);
            inposy += model.GenericTextH;
            j += 1;
          }
        }
      }
    }

    if (mysurf.videoShowProgressBar) {
        Rect bounds = new Rect();
        if (currmpvlib.timepos > 0 && currmpvlib.duration > 0) {
          float currpos = ((mysurf.mywidth - model.GenericInterSpace * 2.0f) * currmpvlib.timepos) / currmpvlib.duration;

          RectF rect2 = new RectF(model.GenericInterSpace + currpos, mysurf.myheight - model.GenericTextH - model.GenericInterSpace * 2.0f,
                  mysurf.mywidth - model.GenericInterSpace, mysurf.myheight - model.GenericInterSpace);
          surfacecanvas.drawRect(rect2, model.VideoTextBgPaint);
          if (clickquicknav) {
            if (model.GenericInterSpace < clicktohandlex && clicktohandlex < mysurf.mywidth - model.GenericInterSpace
                && clicktohandley > mysurf.myheight * Gallery.percentofvideoheightforprogressbarclick) {
              float wantedpos = currmpvlib.duration * (clicktohandlex - model.GenericInterSpace) / (mysurf.mywidth - model.GenericInterSpace * 2.0f);
              currmpvlib.settimepos((int) wantedpos);
              needanotherupdate = true;
              clicktohandle = false;
            }
          }

          RectF rect1 = new RectF(model.GenericInterSpace, mysurf.myheight - model.GenericTextH - model.GenericInterSpace * 2.0f,
                  model.GenericInterSpace + currpos, mysurf.myheight - model.GenericInterSpace);
          surfacecanvas.drawRect(rect1, model.VideoBottomBarPaint);

          String texte;
          int textl = 0;

          if (currmpvlib.rawinputrate > 0 && !currmpvlib.idle) {
            texte = String.format("   %.1f kB/s", currmpvlib.rawinputrate);
            model.VideoTextPaint.getTextBounds(texte, 0, texte.length(), bounds);
            surfacecanvas.drawText(texte, 0.250f * mysurf.mywidth, mysurf.myheight - model.GenericTextH - bounds.top - model.GenericInterSpace, model.VideoTextPaint);
          }

          if (currmpvlib.timepos < 60) {
            texte = ((int) currmpvlib.timepos) + "\"";
          } else if (currmpvlib.timepos < 3600) {
            int secs = ((int) currmpvlib.timepos) % 60;
            int mins = (int) (((int) currmpvlib.timepos - secs) / 60);
            texte = mins + "' " + secs + "\"";
          } else {
            int secs = ((int) currmpvlib.timepos) % 60;
            int mins = (int) (((int) currmpvlib.timepos - secs) / 60);
            texte = mins + "' " + secs + "\"";
          }
          model.VideoTextPaint.getTextBounds(texte, 0, texte.length(), bounds);
          surfacecanvas.drawText(texte, 0.500f * mysurf.mywidth, mysurf.myheight - model.GenericTextH - bounds.top - model.GenericInterSpace, model.VideoTextPaint);

          texte = "";
          if (currmpvlib.underrun)
            texte += "underrun    ";
          if (currmpvlib.ispaused)
            texte += "paused";
          textl = texte.length();
          if (textl > 0) {
            model.VideoTextPaint.getTextBounds(texte, 0, texte.length(), bounds);
            surfacecanvas.drawText(texte, 0.750f * mysurf.mywidth, mysurf.myheight - model.GenericTextH - bounds.top - model.GenericInterSpace, model.VideoTextPaint);
          }

        }
    }

    if (needanotherupdate) {
      try {
        model.commandethreadvideo.put(new String[]{String.valueOf(currid), "update"});
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
    }
    return needanotherupdate;
  }


  private void menudelay(Canvas surfacecanvas) {
    if (!mysurf.optionvideodelay)
      ecritoption = "delays";
    else
      ecritoption = "_";
    y1 = mabase;
    y2 = mabase + model.GenericCaseH;
    taillex = model.Menu1TextePaint.measureText(ecritoption);
    x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
    x2 = mysurf.ScreenWidth - model.GenericInterSpace;
    if (x1 > mysurf.SettingsXmin) {
      x1 = mysurf.SettingsXmin;
    }
    ecritx = x2 - model.GenericInterSpace;
    ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
    if (clickquicknavmenu) {
      if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
        mysurf.optionvideodelay = !mysurf.optionvideodelay;
        needanotherupdate = true;
        clickquicknavmenu = false;
      }
    }
    recti = new RectF(x1, y1, x2, y2);
    surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu1BgPainta);
    recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
    surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu1BgPainto);
    if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
      RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
      surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
    }
    surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);
    mabase += model.GenericCaseH + model.GenericInterSpace;
  }

  private void menutweak(Canvas surfacecanvas) {
    if (!mysurf.optionvideotweak)
      ecritoption = "tweaks";
    else
      ecritoption = "_";
    y1 = mabase;
    y2 = mabase + model.GenericCaseH;
    taillex = model.Menu1TextePaint.measureText(ecritoption);
    x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
    x2 = mysurf.ScreenWidth - model.GenericInterSpace;
    if (x1 > mysurf.SettingsXmin) {
      x1 = mysurf.SettingsXmin;
    }
    ecritx = x2 - model.GenericInterSpace;
    ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
    if (clickquicknavmenu) {
      if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
        mysurf.optionvideotweak = !mysurf.optionvideotweak;
        needanotherupdate = true;
        clickquicknavmenu = false;
      }
    }
    recti = new RectF(x1, y1, x2, y2);
    surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu1BgPainta);
    recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
    surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu1BgPainto);
    if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
      RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
      surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
    }
    surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);
    mabase += model.GenericCaseH + model.GenericInterSpace;
  }

  private void menushowlog(Canvas surfacecanvas) {
    if (!mysurf.optionvideolog)
      ecritoption = "log";
    else
      ecritoption = "_";
    y1 = mabase;
    y2 = mabase + model.GenericCaseH;
    taillex = model.Menu1TextePaint.measureText(ecritoption);
    x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
    x2 = mysurf.ScreenWidth - model.GenericInterSpace;
    if (x1 > mysurf.SettingsXmin) {
      x1 = mysurf.SettingsXmin;
    }
    ecritx = x2 - model.GenericInterSpace;
    ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
    if (clickquicknavmenu) {
      if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
        mysurf.optionvideolog = !mysurf.optionvideolog;
        needanotherupdate = true;
        clickquicknavmenu = false;
      }
    }
    recti = new RectF(x1, y1, x2, y2);
    surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu1BgPainta);
    recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
    surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu1BgPainto);
    if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
      RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
      surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
    }
    surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);
    mabase += model.GenericCaseH + model.GenericInterSpace;
  }

  private void menuframemod(Canvas surfacecanvas) {
    ecritoption = "modify";
    y1 = mabase;
    y2 = mabase + model.GenericCaseH;
    taillex = model.Menu1TextePaint.measureText(ecritoption);
    x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
    x2 = mysurf.ScreenWidth - model.GenericInterSpace;
    if (x1 > mysurf.SettingsXmin) {
      x1 = mysurf.SettingsXmin;
    }
    ecritx = x2 - model.GenericInterSpace;
    ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
    if (clickquicknavmenu) {
      if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
        mysurf.optionvideoframemod = !mysurf.optionvideoframemod;
        needanotherupdate = true;
        clickquicknavmenu = false;
      }
    }
    recti = new RectF(x1, y1, x2, y2);
    surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu1BgPainta);
    recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
    surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu1BgPainto);
    if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
      RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
      surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
    }
    surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);
    mabase += model.GenericCaseH + model.GenericInterSpace;
  }

  private void menuseek(Canvas surfacecanvas) {
      if (model.videoseekmode == 0) {
        ecritoption = "frame step";
      } else {
        ecritoption = "seek " + MPVLib.modeseek[model.videoseekmode] + "s"; // fit window each time for slideshow
      }
      y1 = mabase;
      y2 = mabase + model.GenericCaseH;
      taillex = model.Menu1TextePaint.measureText(ecritoption);
      x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
      x2 = mysurf.ScreenWidth - model.GenericInterSpace;
      if (x1 > mysurf.SettingsXmin) {
        x1 = mysurf.SettingsXmin;
      }
      ecritx = x2 - model.GenericInterSpace;
      ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
      if (clickquicknavmenu) {
        if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
          mysurf.videoshowseek = !mysurf.videoshowseek;
          //currmpvlib.addsubtitle("/storage/emulated/0/bb.srt");
          needanotherupdate = true;
          clickquicknavmenu = false;
        }
      }
      recti = new RectF(x1, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu1BgPainta);
      recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu1BgPainto);
      if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
        RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
        surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
      }
      surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

      mabase += model.GenericCaseH + model.GenericInterSpace;
      if (mabase > mysurf.myheight)
        return;

      if (mysurf.videoshowseek) {

        for (int i = 0 ; i < MPVLib.modeseek.length ; i++) {
          if (i == 0) {
            ecritoption = "frame step";
          } else {
            ecritoption = "seek " + MPVLib.modeseek[i] + "s"; // fit window each time for slideshow
          }
          y1 = mabase;
          y2 = mabase + model.GenericCaseH;
          taillex = model.Menu1TextePaint.measureText(ecritoption);
          x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
          x2 = mysurf.ScreenWidth - model.GenericInterSpace;
          if (x1 > mysurf.SettingsXmin) {
            x1 = mysurf.SettingsXmin;
          }
          ecritx = x2 - model.GenericInterSpace;
          ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
          if (clickquicknavmenu) {
            if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
              model.videoseekmode = i;
              model.preferences.edit().putInt("videoseekmode", model.videoseekmode).apply();
              needanotherupdate = true;
              clickquicknavmenu = false;
            }
          } else if (clickrightleft != 0) {
            if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
              if (clickrightleft == 1)
                currmpvlib.seek(model.videoseekmode, true);
              else {
                if (currmpvlib.seek(model.videoseekmode, false))
                  model.message("preceding video loaded");
              }
            }
          }
          recti = new RectF(x1, y1, x2, y2);
          surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
          recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
          surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
          if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
            RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
            surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
          }
          surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

          mabase += model.GenericCaseH + model.GenericInterSpace;
          if (mabase > mysurf.myheight)
            break;
        }

      }
  }

  private void menuquit(Canvas surfacecanvas) {
      ecritoption = "quit";
      y1 = mabase;
      y2 = mabase + model.GenericCaseH;
      taillex = model.Menu1TextePaint.measureText(ecritoption);
      x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
      x2 = mysurf.ScreenWidth - model.GenericInterSpace;
      if (x1 > mysurf.SettingsXmin) {
        x1 = mysurf.SettingsXmin;
      }
      ecritx = x2 - model.GenericInterSpace;
      ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
      if (clickquicknavmenu) {
        if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
          if (mysurf.optiondiaporamaactive) {
            mysurf.optiondiaporamaactive = false;
          }
          if (mysurf.fragmenttype == Surf.FRAGMENT_TYPE_VIDEO) {
            try {
              model.commandethreadvideo.put(new String[]{String.valueOf(currid), "startbrowser"});
            } catch (InterruptedException e) {
            }
          }
          needanotherupdate = true;
          clickquicknavmenu = false;
        }
      }
      recti = new RectF(x1, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu1BgPainta);
      recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu1BgPainto);
      if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
        RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
        surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
      }
      surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

      mabase += model.GenericCaseH + model.GenericInterSpace;
  }

  private void menulastposition(Canvas surfacecanvas) {
      if (currmpvlib.lastplayedposition > 0) {
        ecritoption = "last position " + currmpvlib.lastplayedposition + "s";
        y1 = mabase;
        y2 = mabase + model.GenericCaseH;
        taillex = model.Menu1TextePaint.measureText(ecritoption);
        x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
        x2 = mysurf.ScreenWidth - model.GenericInterSpace;
        if (x1 > mysurf.SettingsXmin) {
          x1 = mysurf.SettingsXmin;
        }
        ecritx = x2 - model.GenericInterSpace;
        ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
        if (clickquicknavmenu) {
          if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
            if (currmpvlib.streampath != null && currmpvlib.duration > 0) {
              llog.d(TAG, "jump to last pos " + currmpvlib.lastplayedposition);
              currmpvlib.settimepos(currmpvlib.lastplayedposition);
            }
            needanotherupdate = true;
            clickquicknavmenu = false;
          }
        }
        recti = new RectF(x1, y1, x2, y2);
        surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu1BgPainta);
        recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
        surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu1BgPainto);
        if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
          RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
          surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
        }
        surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

        mabase += model.GenericCaseH + model.GenericInterSpace;
      }
  }

  private void menutrackposition(Canvas surfacecanvas) {
      if (currmpvlib.trackposition > 0) {
        ecritoption = "track position " + currmpvlib.trackposition + "s";
        y1 = mabase;
        y2 = mabase + model.GenericCaseH;
        taillex = model.Menu1TextePaint.measureText(ecritoption);
        x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
        x2 = mysurf.ScreenWidth - model.GenericInterSpace;
        if (x1 > mysurf.SettingsXmin) {
          x1 = mysurf.SettingsXmin;
        }
        ecritx = x2 - model.GenericInterSpace;
        ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
        if (clickquicknavmenu) {
          if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
            if (currmpvlib.streampath != null && currmpvlib.duration > 0) {
              llog.d(TAG, "jump to track pos " + currmpvlib.trackposition);
              currmpvlib.settimepos(currmpvlib.trackposition);
            }
            needanotherupdate = true;
            clickquicknavmenu = false;
          }
        }
        recti = new RectF(x1, y1, x2, y2);
        surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu1BgPainta);
        recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
        surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu1BgPainto);
        if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
          RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
          surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
        }
        surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

        mabase += model.GenericCaseH + model.GenericInterSpace;
      }
  }

  private void menuplaylist(Canvas surfacecanvas) {
      int playlistnext = currmpvlib.playlistnext;
      int playlistdetaill = currmpvlib.playlistdetail.size();
      if (playlistdetaill > 0 && playlistnext >= 0 && playlistnext < playlistdetaill) {
        ecritoption = currmpvlib.playlistdetail.get(playlistnext)[1] + " " + currmpvlib.playlistdetail.get(playlistnext)[2];
        y1 = mabase;
        y2 = mabase + model.GenericCaseH;
        taillex = model.Menu1TextePaint.measureText(ecritoption);
        x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
        x2 = mysurf.ScreenWidth - model.GenericInterSpace;
        if (x1 > mysurf.SettingsXmin) {
          x1 = mysurf.SettingsXmin;
        }
        ecritx = x2 - model.GenericInterSpace;
        ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
        if (clickquicknavmenu) {
          if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
            mysurf.videoshowplaylist = !mysurf.videoshowplaylist;
            needanotherupdate = true;
            clickquicknavmenu = false;
          }
        }
        recti = new RectF(x1, y1, x2, y2);
        surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu1BgPainta);
        recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
        surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu1BgPainto);
        if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
          RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
          surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
        }
        surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

        mabase += model.GenericCaseH + model.GenericInterSpace;
        if (mabase > mysurf.myheight)
          return;

        if (mysurf.videoshowplaylist) {

          for (int i = 0 ; i < playlistdetaill ; i++) {
            ecritoption = currmpvlib.playlistdetail.get(i)[2];
            y1 = mabase;
            y2 = mabase + model.GenericCaseH;
            taillex = model.Menu1TextePaint.measureText(ecritoption);
            x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
            x2 = mysurf.ScreenWidth - model.GenericInterSpace;
            if (x1 > mysurf.SettingsXmin) {
              x1 = mysurf.SettingsXmin;
            }
            ecritx = x2 - model.GenericInterSpace;
            ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
            if (clickquicknavmenu) {
              if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
                currmpvlib.playlistnext = i;
                needanotherupdate = true;
                clickquicknavmenu = false;
              }
            }
            recti = new RectF(x1, y1, x2, y2);
            surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
            recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
            surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
            if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
              RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
              surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
            }
            surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

            mabase += model.GenericCaseH + model.GenericInterSpace;
            if (mabase > mysurf.myheight)
              break;
          }

        }
      }
  }

  private void menuvideo(Canvas surfacecanvas) {
      ecritoption = "video";
      if (currmpvlib.vida.containsKey(currmpvlib.vid)) {
        if (currmpvlib.vida.get(currmpvlib.vid).containsKey("pprint")) {
          ecritoption = currmpvlib.vida.get(currmpvlib.vid).get("pprint");
        }
      }
      if (currmpvlib.vida.size() >= 2) {
        ecritoption += String.format(" %d/%d", currmpvlib.vid, currmpvlib.vida.size()-1);
      } else {
        ecritoption = "no video";
      }
      y1 = mabase;
      y2 = mabase + model.GenericCaseH;
      taillex = model.Menu1TextePaint.measureText(ecritoption);
      x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
      x2 = mysurf.ScreenWidth - model.GenericInterSpace;
      if (x1 > mysurf.SettingsXmin) {
        x1 = mysurf.SettingsXmin;
      }
      ecritx = x2 - model.GenericInterSpace;
      ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
      if (clickquicknavmenu) {
        if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
          mysurf.videoshowvid = !mysurf.videoshowvid;
          needanotherupdate = true;
          clickquicknavmenu = false;
        }
      }
      recti = new RectF(x1, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu1BgPainta);
      recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu1BgPainto);
      if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
        RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
        surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
      }
      surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

      mabase += model.GenericCaseH + model.GenericInterSpace;
      if (mabase > mysurf.myheight)
        return;

      if (mysurf.videoshowvid) {

        for (int i = 0 ; i < currmpvlib.vida.size() ; i++) {
          ecritoption = "no video";
          if (currmpvlib.vida.containsKey(i)) {
            if (currmpvlib.vida.get(i).containsKey("pprint")) {
              ecritoption = currmpvlib.vida.get(i).get("pprint");
            }
          }
          y1 = mabase;
          y2 = mabase + model.GenericCaseH;
          taillex = model.Menu1TextePaint.measureText(ecritoption);
          x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
          x2 = mysurf.ScreenWidth - model.GenericInterSpace;
          if (x1 > mysurf.SettingsXmin) {
            x1 = mysurf.SettingsXmin;
          }
          ecritx = x2 - model.GenericInterSpace;
          ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
          if (clickquicknavmenu) {
            if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
              currmpvlib.setvid(i);
              needanotherupdate = true;
              clickquicknavmenu = false;
            }
          }
          recti = new RectF(x1, y1, x2, y2);
          surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
          recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
          surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
          if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
            RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
            surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
          }
          surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

          mabase += model.GenericCaseH + model.GenericInterSpace;
          if (mabase > mysurf.myheight)
            break;
        }

      }
  }

  private void menuaudio(Canvas surfacecanvas) {
      ecritoption = "audio";
      if (currmpvlib.aida.containsKey(currmpvlib.aid)) {
        if (currmpvlib.aida.get(currmpvlib.aid).containsKey("pprint")) {
          ecritoption = currmpvlib.aida.get(currmpvlib.aid).get("pprint");
        }
      }
      if (currmpvlib.aida.size() >= 2) {
        ecritoption += String.format(" %d/%d", currmpvlib.aid, currmpvlib.aida.size()-1);
      } else {
        ecritoption = "no audio";
      }
      y1 = mabase;
      y2 = mabase + model.GenericCaseH;
      taillex = model.Menu1TextePaint.measureText(ecritoption);
      x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
      x2 = mysurf.ScreenWidth - model.GenericInterSpace;
      if (x1 > mysurf.SettingsXmin) {
        x1 = mysurf.SettingsXmin;
      }
      ecritx = x2 - model.GenericInterSpace;
      ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
      if (clickquicknavmenu) {
        if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
          mysurf.videoshowaid = !mysurf.videoshowaid;
          needanotherupdate = true;
          clickquicknavmenu = false;
        }
      }
      recti = new RectF(x1, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu1BgPainta);
      recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu1BgPainto);
      if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
        RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
        surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
      }
      surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

      mabase += model.GenericCaseH + model.GenericInterSpace;
      if (mabase > mysurf.myheight)
        return;

      if (mysurf.videoshowaid) {

        for (int i = 0 ; i < currmpvlib.aida.size() ; i++) {
          ecritoption = "no audio";
          if (currmpvlib.aida.containsKey(i)) {
            if (currmpvlib.aida.get(i).containsKey("pprint")) {
              ecritoption = currmpvlib.aida.get(i).get("pprint");
            }
          }
          y1 = mabase;
          y2 = mabase + model.GenericCaseH;
          taillex = model.Menu1TextePaint.measureText(ecritoption);
          x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
          x2 = mysurf.ScreenWidth - model.GenericInterSpace;
          if (x1 > mysurf.SettingsXmin) {
            x1 = mysurf.SettingsXmin;
          }
          ecritx = x2 - model.GenericInterSpace;
          ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
          if (clickquicknavmenu) {
            if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
              currmpvlib.setaid(i);
              needanotherupdate = true;
              clickquicknavmenu = false;
            }
          }
          recti = new RectF(x1, y1, x2, y2);
          surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
          recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
          surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
          if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
            RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
            surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
          }
          surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

          mabase += model.GenericCaseH + model.GenericInterSpace;
          if (mabase > mysurf.myheight)
            break;
        }

      }
  }

  private void menusubtitles(Canvas surfacecanvas) {
      if (currmpvlib.sida.size() < 2) {
        return;
      }
      ecritoption = "subtitles";
      if (currmpvlib.sida.containsKey(currmpvlib.sid)) {
        if (currmpvlib.sida.get(currmpvlib.sid).containsKey("pprint")) {
          ecritoption = currmpvlib.sida.get(currmpvlib.sid).get("pprint");
        }
      }
      if (currmpvlib.sida.size() >= 2) {
        ecritoption += String.format(" %d/%d", currmpvlib.sid, currmpvlib.sida.size()-1);
      } else {
        ecritoption = "no subtitle";
      }
      y1 = mabase;
      y2 = mabase + model.GenericCaseH;
      taillex = model.Menu1TextePaint.measureText(ecritoption);
      x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
      x2 = mysurf.ScreenWidth - model.GenericInterSpace;
      if (x1 > mysurf.SettingsXmin) {
        x1 = mysurf.SettingsXmin;
      }
      ecritx = x2 - model.GenericInterSpace;
      ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
      if (clickquicknavmenu) {
        if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
          mysurf.videoshowsid = !mysurf.videoshowsid;
          /*if (mysurf.videoshowsid && model.videohwaamode == 2)
            model.message("to be able to render subtitles\n" +
                    "you need to switch decoding to\n" +
                    "tweaks -> hardware copy to ram");*/
          needanotherupdate = true;
          clickquicknavmenu = false;
        }
      }
      recti = new RectF(x1, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu1BgPainta);
      recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu1BgPainto);
      if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
        RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
        surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
      }
      surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

      mabase += model.GenericCaseH + model.GenericInterSpace;
      if (mabase > mysurf.myheight)
        return;

      if (mysurf.videoshowsid) {

        for (int i = 0 ; i < currmpvlib.sida.size() ; i++) {
          ecritoption = "no subtitle";
          if (currmpvlib.sida.containsKey(i)) {
            if (currmpvlib.sida.get(i).containsKey("pprint")) {
              ecritoption = currmpvlib.sida.get(i).get("pprint");
            }
          }
          y1 = mabase;
          y2 = mabase + model.GenericCaseH;
          taillex = model.Menu1TextePaint.measureText(ecritoption);
          x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
          x2 = mysurf.ScreenWidth - model.GenericInterSpace;
          if (x1 > mysurf.SettingsXmin) {
            x1 = mysurf.SettingsXmin;
          }
          ecritx = x2 - model.GenericInterSpace;
          ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
          if (clickquicknavmenu) {
            if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
              currmpvlib.setsid(i);
              if (model.videohardwaremode == Gallery.videohwaamodehwpp) {
                if (i > 0) {
                  mysurf.videoShowSub = true;
                  POLL_TIMEOUT = Gallery.polltimeoutfast;
                } else {
                  mysurf.videoShowSub = false;
                  POLL_TIMEOUT = Gallery.polltimeoutdefault;
                  if (mysurf.videoShowOverlay != mysurf.videoShowSub
                          && !mysurf.videoShowOptionMenu
                          && !mysurf.videoShowProgressBar) {
                    mysurf.videoShowOverlay = mysurf.videoShowSub;
                    if (!mysurf.videoShowOverlay)
                      model.startVideoClearBrowserSurfaceTransparent = true;
                  }
                }
              }
              needanotherupdate = true;
              clickquicknavmenu = false;
            }
          }
          recti = new RectF(x1, y1, x2, y2);
          surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
          recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
          surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
          if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
            RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
            surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
          }
          surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

          mabase += model.GenericCaseH + model.GenericInterSpace;
          if (mabase > mysurf.myheight)
            break;
        }

      }
  }

  private void menuextsubtitle(Canvas surfacecanvas) {
    int extsubtitlelength = currmpvlib.subtitles.length - 1;
    if (extsubtitlelength > 0) {
      if (currmpvlib.extsubtitle == 0)
        ecritoption = extsubtitlelength + " ext. subtitles";
      else
        ecritoption = currmpvlib.subtitlespprint[currmpvlib.extsubtitle];
      y1 = mabase;
      y2 = mabase + model.GenericCaseH;
      taillex = model.Menu1TextePaint.measureText(ecritoption);
      x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
      x2 = mysurf.ScreenWidth - model.GenericInterSpace;
      if (x1 > mysurf.SettingsXmin) {
        x1 = mysurf.SettingsXmin;
      }
      ecritx = x2 - model.GenericInterSpace;
      ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
      if (clickquicknavmenu) {
        if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
          mysurf.videoshowextsubtitle = !mysurf.videoshowextsubtitle;
          needanotherupdate = true;
          clickquicknavmenu = false;
        }
      }
      recti = new RectF(x1, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu1BgPainta);
      recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu1BgPainto);
      if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
        RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
        surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
      }
      surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

      mabase += model.GenericCaseH + model.GenericInterSpace;
      if (mabase > mysurf.myheight)
        return;

      if (mysurf.videoshowextsubtitle) {

        boolean subtitlechanged = false;
        for (int i = 0; i < currmpvlib.subtitles.length; i++) {
          ecritoption = currmpvlib.subtitlespprint[i];
          //llog.d(TAG, ecritoption);
          y1 = mabase;
          y2 = mabase + model.GenericCaseH;
          taillex = model.Menu1TextePaint.measureText(ecritoption);
          x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
          x2 = mysurf.ScreenWidth - model.GenericInterSpace;
          if (x1 > mysurf.SettingsXmin) {
            x1 = mysurf.SettingsXmin;
          }
          ecritx = x2 - model.GenericInterSpace;
          ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
          if (clickquicknavmenu) {
            if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
              currmpvlib.extsubtitle = i;
              if (i > 0) {
                currmpvlib.addsubtitle(i);
                subtitlechanged = true;
                //model.message(currmpvlib.subtitlespprint[0]+"\n"+currmpvlib.subtitlespprint[i]);

                if (model.videohardwaremode == Gallery.videohwaamodehwpp) {
                  mysurf.videoShowSub = true;
                  POLL_TIMEOUT = Gallery.polltimeoutfast;
                }

              }
              needanotherupdate = true;
              clickquicknavmenu = false;
            }
          }
          recti = new RectF(x1, y1, x2, y2);
          surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
          recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
          surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
          if (i == 0 || i == currmpvlib.extsubtitle) {
            RectF rectisel = new RectF(x1 + quatredixiememillimetre, y1 + quatredixiememillimetre, x2 - quatredixiememillimetre, y2 - quatredixiememillimetre);
            surfacecanvas.drawRoundRect(rectisel, roundedRectRatio, roundedRectRatio, model.SettingsSelPaint);
          }
          if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
            RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
            surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
          }
          surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

          mabase += model.GenericCaseH + model.GenericInterSpace;
          if (mabase > mysurf.myheight)
            break;
        }

      }
    }
  }

  private void menuaudiodelay(Canvas surfacecanvas) {
    if (currmpvlib.aida.size() < 2) {
      return;
    }
      if (currmpvlib.audiodelay > 0.0005f)
        ecritoption = String.format("audio delay +%.3fs", currmpvlib.audiodelay);
      else if (currmpvlib.audiodelay < -0.0005f)
        ecritoption = String.format("audio delay %.3fs", currmpvlib.audiodelay);
      else
        ecritoption = "audio delay";
      y1 = mabase;
      y2 = mabase + model.GenericCaseH;
      taillex = model.Menu1TextePaint.measureText(ecritoption);
      x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
      x2 = mysurf.ScreenWidth - model.GenericInterSpace;
      if (x1 > mysurf.SettingsXmin) {
        x1 = mysurf.SettingsXmin;
      }
      ecritx = x2 - model.GenericInterSpace;
      ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
      if (clickquicknavmenu) {
        if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
          mysurf.videoaudiodelay = !mysurf.videoaudiodelay;
          needanotherupdate = true;
          clickquicknavmenu = false;
        }
      }
      recti = new RectF(x1, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu1BgPainta);
      recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu1BgPainto);
      if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
        RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
        surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
      }
      surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

      mabase += model.GenericCaseH + model.GenericInterSpace;
      if (mabase > mysurf.myheight)
        return;

      if (mysurf.videoaudiodelay) {
        y1 = mabase;
        y2 = mabase + model.GenericCaseH;
        ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;

        ecritoption = "+";
        taillex = model.Menu1TextePaint.measureText(ecritoption);
        x1 = (mysurf.ScreenWidth - model.GenericInterSpace + mysurf.SettingsXmin) * 0.5f + model.GenericInterSpace * 0.5f;
        x2 = mysurf.ScreenWidth - model.GenericInterSpace;
        ecritx = x2;
        if (clickquicknavmenu) {
          if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
            currmpvlib.audiodelay += audiodelaypower[audiodelaypoweri];
            currmpvlib.setaudiodelay(currmpvlib.audiodelay);
            if (currmpvlib.audiodelay != 0) {
              model.videolastaudiodelay = currmpvlib.audiodelay;
              model.preferences.edit().putFloat("videolastaudiodelay", model.videolastaudiodelay).apply();
            }
            needanotherupdate = true;
            clickquicknavmenu = false;
          }
        }
        recti = new RectF(x1, y1, x2, y2);
        //surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
        //recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
        surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
        if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
          RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
          surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
        }
        surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

        if (currmpvlib.audiodelay >= 0.0f)
          ecritoption = String.format("+%.3fs -", currmpvlib.audiodelay);
        else
          ecritoption = String.format("%.3fs -", currmpvlib.audiodelay);
        taillex = model.Menu1TextePaint.measureText(ecritoption);
        x2 = x1 - model.GenericInterSpace;
        x1 = x2 - model.GenericInterSpace * 2.0f - taillex;
      if (x1 > mysurf.SettingsXmin)
        x1 = mysurf.SettingsXmin;
        ecritx = x2 - model.GenericInterSpace;
        if (clickquicknavmenu) {
          if (mysurf.SettingsXmin < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
            currmpvlib.audiodelay -= audiodelaypower[audiodelaypoweri];
            currmpvlib.setaudiodelay(currmpvlib.audiodelay);
            if (currmpvlib.audiodelay != 0) {
              model.videolastaudiodelay = currmpvlib.audiodelay;
              model.preferences.edit().putFloat("videolastaudiodelay", model.videolastaudiodelay).apply();
            }
            needanotherupdate = true;
            clickquicknavmenu = false;
          }
        }
        recti = new RectF(x1, y1, x2, y2);
        surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
        recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
        surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
        if ((model.isandroidtv || model.keypadnavactive) && mysurf.SettingsXmin < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
          RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
          surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
        }
        surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

        mabase += model.GenericCaseH + model.GenericInterSpace;
        if (mabase > mysurf.myheight)
          return;

        ecritoption = String.format("+/- %.3f", audiodelaypower[audiodelaypoweri]);
        y1 = mabase;
        y2 = mabase + model.GenericCaseH;
        taillex = model.Menu1TextePaint.measureText(ecritoption);
        x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
        x2 = mysurf.ScreenWidth - model.GenericInterSpace;
        if (x1 > mysurf.SettingsXmin) {
          x1 = mysurf.SettingsXmin;
        }
        ecritx = x2 - model.GenericInterSpace;
        ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
        if (clickquicknavmenu) {
          if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
            audiodelaypoweri = (audiodelaypower.length + audiodelaypoweri + 1) % audiodelaypower.length;
            needanotherupdate = true;
            clickquicknavmenu = false;
          }
        }
        recti = new RectF(x1, y1, x2, y2);
        surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
        recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
        surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
        if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
          RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
          surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
        }
        surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

        mabase += model.GenericCaseH + model.GenericInterSpace;
        if (mabase > mysurf.myheight)
          return;

        if (model.videolastaudiodelay != 0) {
          if (model.videolastaudiodelay > -0.0005f)
            ecritoption = String.format("last +%.3fs", model.videolastaudiodelay);
          else
            ecritoption = String.format("last %.3fs", model.videolastaudiodelay);
          y1 = mabase;
          y2 = mabase + model.GenericCaseH;
          taillex = model.Menu1TextePaint.measureText(ecritoption);
          x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
          x2 = mysurf.ScreenWidth - model.GenericInterSpace;
          if (x1 > mysurf.SettingsXmin) {
            x1 = mysurf.SettingsXmin;
          }
          ecritx = x2 - model.GenericInterSpace;
          ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
          if (clickquicknavmenu) {
            if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
              currmpvlib.audiodelay = model.videolastaudiodelay;
              currmpvlib.setaudiodelay(currmpvlib.audiodelay);
              needanotherupdate = true;
              clickquicknavmenu = false;
            }
          }
          recti = new RectF(x1, y1, x2, y2);
          surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
          recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
          surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
          if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
            RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
            surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
          }
          surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

          mabase += model.GenericCaseH + model.GenericInterSpace;
          if (mabase > mysurf.myheight)
            return;
        }

        if (currmpvlib.audiodelay != 0) {
          ecritoption = "reset";
          y1 = mabase;
          y2 = mabase + model.GenericCaseH;
          taillex = model.Menu1TextePaint.measureText(ecritoption);
          x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
          x2 = mysurf.ScreenWidth - model.GenericInterSpace;
          if (x1 > mysurf.SettingsXmin) {
            x1 = mysurf.SettingsXmin;
          }
          ecritx = x2 - model.GenericInterSpace;
          ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
          if (clickquicknavmenu) {
            if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
              currmpvlib.audiodelay = 0.0f;
              currmpvlib.setaudiodelay(currmpvlib.audiodelay);
              needanotherupdate = true;
              clickquicknavmenu = false;
            }
          }
          recti = new RectF(x1, y1, x2, y2);
          surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
          recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
          surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
          if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
            RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
            surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
          }
          surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

          mabase += model.GenericCaseH + model.GenericInterSpace;
          if (mabase > mysurf.myheight)
            return;
        }

        if (currmpvlib.audiodelay > -0.0005f)
          ecritoption = String.format("default +%.3fs", model.videoaudiodelay);
        else
          ecritoption = String.format("default %.3fs", model.videoaudiodelay);
        y1 = mabase;
        y2 = mabase + model.GenericCaseH;
        taillex = model.Menu1TextePaint.measureText(ecritoption);
        x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
        x2 = mysurf.ScreenWidth - model.GenericInterSpace;
        if (x1 > mysurf.SettingsXmin) {
          x1 = mysurf.SettingsXmin;
        }
        ecritx = x2 - model.GenericInterSpace;
        ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
        if (clickquicknavmenu) {
          if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
            model.videoaudiodelay = currmpvlib.audiodelay;
            model.preferences.edit().putFloat("videoaudiodelay", model.videoaudiodelay).apply();
            model.message("start all videos with this delay");
            needanotherupdate = true;
            clickquicknavmenu = false;
          }
        }
        recti = new RectF(x1, y1, x2, y2);
        surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
        recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
        surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
        if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
          RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
          surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
        }
        surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

        mabase += model.GenericCaseH + model.GenericInterSpace;
        if (mabase > mysurf.myheight)
          return;



      }
  }

  private void menusubtitledelay(Canvas surfacecanvas) {
    if (currmpvlib.sida.size() < 2) {
      return;
    }
      if (currmpvlib.subtitledelay > 0.0005f)
        ecritoption = String.format("subtitle delay +%.3fs", currmpvlib.subtitledelay);
      else if (currmpvlib.subtitledelay < -0.0005f)
        ecritoption = String.format("subtitle delay %.3fs", currmpvlib.subtitledelay);
      else
        ecritoption = "subtitle delay";
      y1 = mabase;
      y2 = mabase + model.GenericCaseH;
      taillex = model.Menu1TextePaint.measureText(ecritoption);
      x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
      x2 = mysurf.ScreenWidth - model.GenericInterSpace;
      if (x1 > mysurf.SettingsXmin) {
        x1 = mysurf.SettingsXmin;
      }
      ecritx = x2 - model.GenericInterSpace;
      ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
      if (clickquicknavmenu) {
        if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
          mysurf.videosubtitledelay = !mysurf.videosubtitledelay;
          needanotherupdate = true;
          clickquicknavmenu = false;
        }
      }
      recti = new RectF(x1, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu1BgPainta);
      recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu1BgPainto);
      if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
        RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
        surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
      }
      surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

      mabase += model.GenericCaseH + model.GenericInterSpace;
      if (mabase > mysurf.myheight)
        return;

      if (mysurf.videosubtitledelay) {
        y1 = mabase;
        y2 = mabase + model.GenericCaseH;
        ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;

        ecritoption = "+";
        taillex = model.Menu1TextePaint.measureText(ecritoption);
        x1 = (mysurf.ScreenWidth - model.GenericInterSpace + mysurf.SettingsXmin) * 0.5f + model.GenericInterSpace * 0.5f;
        x2 = mysurf.ScreenWidth - model.GenericInterSpace;
        ecritx = x2;
        if (clickquicknavmenu) {
          if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
            currmpvlib.subtitledelay += subtitledelaypower[subtitledelaypoweri];
            currmpvlib.setsubtitledelay(currmpvlib.subtitledelay);
            if (currmpvlib.subtitledelay != 0) {
              model.videolastsubtitledelay = currmpvlib.subtitledelay;
              model.preferences.edit().putFloat("videolastsubtitledelay", model.videolastsubtitledelay).apply();
            }
            needanotherupdate = true;
            clickquicknavmenu = false;
          }
        }
        recti = new RectF(x1, y1, x2, y2);
        //surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
        //recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
        surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
        if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
          RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
          surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
        }
        surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

        if (currmpvlib.subtitledelay >= 0.0f)
          ecritoption = String.format("+%.3fs -", currmpvlib.subtitledelay);
        else
          ecritoption = String.format("%.3fs -", currmpvlib.subtitledelay);
        taillex = model.Menu1TextePaint.measureText(ecritoption);
        x2 = x1 - model.GenericInterSpace;
        x1 = x2 - model.GenericInterSpace * 2.0f - taillex;
      if (x1 > mysurf.SettingsXmin)
        x1 = mysurf.SettingsXmin;
        ecritx = x2 - model.GenericInterSpace;
        if (clickquicknavmenu) {
          if (mysurf.SettingsXmin < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
            currmpvlib.subtitledelay -= subtitledelaypower[subtitledelaypoweri];
            currmpvlib.setsubtitledelay(currmpvlib.subtitledelay);
            if (currmpvlib.subtitledelay != 0) {
              model.videolastsubtitledelay = currmpvlib.subtitledelay;
              model.preferences.edit().putFloat("videolastsubtitledelay", model.videolastsubtitledelay).apply();
            }
            needanotherupdate = true;
            clickquicknavmenu = false;
          }
        }
        recti = new RectF(x1, y1, x2, y2);
        surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
        recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
        surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
        if ((model.isandroidtv || model.keypadnavactive) && mysurf.SettingsXmin < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
          RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
          surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
        }
        surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

        mabase += model.GenericCaseH + model.GenericInterSpace;
        if (mabase > mysurf.myheight)
          return;

        ecritoption = String.format("+/- %.3f", subtitledelaypower[subtitledelaypoweri]);
        y1 = mabase;
        y2 = mabase + model.GenericCaseH;
        taillex = model.Menu1TextePaint.measureText(ecritoption);
        x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
        x2 = mysurf.ScreenWidth - model.GenericInterSpace;
        if (x1 > mysurf.SettingsXmin) {
          x1 = mysurf.SettingsXmin;
        }
        ecritx = x2 - model.GenericInterSpace;
        ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
        if (clickquicknavmenu) {
          if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
            subtitledelaypoweri = (subtitledelaypower.length + subtitledelaypoweri + 1) % subtitledelaypower.length;
            needanotherupdate = true;
            clickquicknavmenu = false;
          }
        }
        recti = new RectF(x1, y1, x2, y2);
        surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
        recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
        surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
        if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
          RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
          surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
        }
        surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

        mabase += model.GenericCaseH + model.GenericInterSpace;
        if (mabase > mysurf.myheight)
          return;

        if (model.videolastsubtitledelay != 0) {
          if (model.videolastsubtitledelay > -0.0005f)
            ecritoption = String.format("last +%.3fs", model.videolastsubtitledelay);
          else
            ecritoption = String.format("last %.3fs", model.videolastsubtitledelay);
          y1 = mabase;
          y2 = mabase + model.GenericCaseH;
          taillex = model.Menu1TextePaint.measureText(ecritoption);
          x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
          x2 = mysurf.ScreenWidth - model.GenericInterSpace;
          if (x1 > mysurf.SettingsXmin) {
            x1 = mysurf.SettingsXmin;
          }
          ecritx = x2 - model.GenericInterSpace;
          ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
          if (clickquicknavmenu) {
            if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
              currmpvlib.subtitledelay = model.videolastsubtitledelay;
              currmpvlib.setsubtitledelay(currmpvlib.subtitledelay);
              needanotherupdate = true;
              clickquicknavmenu = false;
            }
          }
          recti = new RectF(x1, y1, x2, y2);
          surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
          recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
          surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
          if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
            RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
            surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
          }
          surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

          mabase += model.GenericCaseH + model.GenericInterSpace;
          if (mabase > mysurf.myheight)
            return;
        }

        if (currmpvlib.subtitledelay != 0) {
          ecritoption = "reset";
          y1 = mabase;
          y2 = mabase + model.GenericCaseH;
          taillex = model.Menu1TextePaint.measureText(ecritoption);
          x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
          x2 = mysurf.ScreenWidth - model.GenericInterSpace;
          if (x1 > mysurf.SettingsXmin) {
            x1 = mysurf.SettingsXmin;
          }
          ecritx = x2 - model.GenericInterSpace;
          ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
          if (clickquicknavmenu) {
            if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
              currmpvlib.subtitledelay = 0.0f;
              currmpvlib.setsubtitledelay(currmpvlib.subtitledelay);
              needanotherupdate = true;
              clickquicknavmenu = false;
            }
          }
          recti = new RectF(x1, y1, x2, y2);
          surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
          recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
          surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
          if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
            RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
            surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
          }
          surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

          mabase += model.GenericCaseH + model.GenericInterSpace;
          if (mabase > mysurf.myheight)
            return;
        }

      }
  }

  private void menuabloop(Canvas surfacecanvas) {
    ecritoption = "a-b loop";
    y1 = mabase;
    y2 = mabase + model.GenericCaseH;
    taillex = model.Menu1TextePaint.measureText(ecritoption);
    x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
    x2 = mysurf.ScreenWidth - model.GenericInterSpace;
    if (x1 > mysurf.SettingsXmin) {
      x1 = mysurf.SettingsXmin;
    }
    ecritx = x2 - model.GenericInterSpace;
    ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
    if (clickquicknavmenu) {
      if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
        mysurf.videoshowabloop = !mysurf.videoshowabloop;
        needanotherupdate = true;
        clickquicknavmenu = false;
      }
    }
    recti = new RectF(x1, y1, x2, y2);
    surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu1BgPainta);
    recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
    surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu1BgPainto);
    if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
      RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
      surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
    }
    surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

    mabase += model.GenericCaseH + model.GenericInterSpace;
    if (mabase > mysurf.myheight)
      return;

    if (mysurf.videoshowabloop) {
      ecritoption = "set a";
      y1 = mabase;
      y2 = mabase + model.GenericCaseH;
      taillex = model.Menu1TextePaint.measureText(ecritoption);
      x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
      x2 = mysurf.ScreenWidth - model.GenericInterSpace;
      if (x1 > mysurf.SettingsXmin) {
        x1 = mysurf.SettingsXmin;
      }
      ecritx = x2 - model.GenericInterSpace;
      ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
      if (clickquicknavmenu) {
        if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
          currmpvlib.setProperty("ab-loop-a", "no");
          currmpvlib.setProperty("ab-loop-b", "no");
          currmpvlib.command(new String[]{"ab-loop"});
          needanotherupdate = true;
          clickquicknavmenu = false;
        }
      }
      recti = new RectF(x1, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
      recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
      if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
        RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
        surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
      }
      surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

      mabase += model.GenericCaseH + model.GenericInterSpace;
      if (mabase > mysurf.myheight)
        return;

      ecritoption = "set b";
      y1 = mabase;
      y2 = mabase + model.GenericCaseH;
      taillex = model.Menu1TextePaint.measureText(ecritoption);
      x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
      x2 = mysurf.ScreenWidth - model.GenericInterSpace;
      if (x1 > mysurf.SettingsXmin) {
        x1 = mysurf.SettingsXmin;
      }
      ecritx = x2 - model.GenericInterSpace;
      ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
      if (clickquicknavmenu) {
        if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
          currmpvlib.setProperty("ab-loop-b", "no");
          currmpvlib.command(new String[]{"ab-loop"});
          needanotherupdate = true;
          clickquicknavmenu = false;
        }
      }
      recti = new RectF(x1, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
      recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
      if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
        RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
        surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
      }
      surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

      mabase += model.GenericCaseH + model.GenericInterSpace;
      if (mabase > mysurf.myheight)
        return;

      ecritoption = "stop";
      y1 = mabase;
      y2 = mabase + model.GenericCaseH;
      taillex = model.Menu1TextePaint.measureText(ecritoption);
      x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
      x2 = mysurf.ScreenWidth - model.GenericInterSpace;
      if (x1 > mysurf.SettingsXmin) {
        x1 = mysurf.SettingsXmin;
      }
      ecritx = x2 - model.GenericInterSpace;
      ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
      if (clickquicknavmenu) {
        if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
          currmpvlib.setProperty("ab-loop-a", "no");
          currmpvlib.setProperty("ab-loop-b", "no");
          needanotherupdate = true;
          clickquicknavmenu = false;
        }
      }
      recti = new RectF(x1, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
      recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
      if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
        RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
        surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
      }
      surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

      mabase += model.GenericCaseH + model.GenericInterSpace;
      if (mabase > mysurf.myheight)
        return;
    }
  }

  private void menuspeed(Canvas surfacecanvas) {
      ecritoption = String.format("speed %.2fx", currmpvlib.speed);
      y1 = mabase;
      y2 = mabase + model.GenericCaseH;
      taillex = model.Menu1TextePaint.measureText(ecritoption);
      x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
      x2 = mysurf.ScreenWidth - model.GenericInterSpace;
      if (x1 > mysurf.SettingsXmin) {
        x1 = mysurf.SettingsXmin;
      }
      ecritx = x2 - model.GenericInterSpace;
      ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
      if (clickquicknavmenu) {
        if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
          mysurf.videoshowspeed = !mysurf.videoshowspeed;
          needanotherupdate = true;
          clickquicknavmenu = false;
        }
      }
      recti = new RectF(x1, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu1BgPainta);
      recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu1BgPainto);
      if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
        RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
        surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
      }
      surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

      mabase += model.GenericCaseH + model.GenericInterSpace;
      if (mabase > mysurf.myheight)
        return;

      if (mysurf.videoshowspeed) {

        ecritoption = "1/20 x";
        y1 = mabase;
        y2 = mabase + model.GenericCaseH;
        taillex = model.Menu1TextePaint.measureText(ecritoption);
        x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
        x2 = mysurf.ScreenWidth - model.GenericInterSpace;
        if (x1 > mysurf.SettingsXmin) {
          x1 = mysurf.SettingsXmin;
        }
        ecritx = x2 - model.GenericInterSpace;
        ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
        if (clickquicknavmenu) {
          if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
            currmpvlib.speed = 0.05;
            currmpvlib.setspeed(currmpvlib.speed);
            needanotherupdate = true;
            clickquicknavmenu = false;
          }
        }
        recti = new RectF(x1, y1, x2, y2);
        surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
        recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
        surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
        if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
          RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
          surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
        }
        surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

        mabase += model.GenericCaseH + model.GenericInterSpace;
        if (mabase > mysurf.myheight)
          return;

        ecritoption = "1/5 x";
        y1 = mabase;
        y2 = mabase + model.GenericCaseH;
        taillex = model.Menu1TextePaint.measureText(ecritoption);
        x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
        x2 = mysurf.ScreenWidth - model.GenericInterSpace;
        if (x1 > mysurf.SettingsXmin) {
          x1 = mysurf.SettingsXmin;
        }
        ecritx = x2 - model.GenericInterSpace;
        ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
        if (clickquicknavmenu) {
          if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
            currmpvlib.speed = 0.20;
            currmpvlib.setspeed(currmpvlib.speed);
            needanotherupdate = true;
            clickquicknavmenu = false;
          }
        }
        recti = new RectF(x1, y1, x2, y2);
        surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
        recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
        surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
        if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
          RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
          surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
        }
        surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

        mabase += model.GenericCaseH + model.GenericInterSpace;
        if (mabase > mysurf.myheight)
          return;

        ecritoption = "normal";
        y1 = mabase;
        y2 = mabase + model.GenericCaseH;
        taillex = model.Menu1TextePaint.measureText(ecritoption);
        x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
        x2 = mysurf.ScreenWidth - model.GenericInterSpace;
        if (x1 > mysurf.SettingsXmin) {
          x1 = mysurf.SettingsXmin;
        }
        ecritx = x2 - model.GenericInterSpace;
        ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
        if (clickquicknavmenu) {
          if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
            currmpvlib.speed = 1.0;
            currmpvlib.setspeed(currmpvlib.speed);
            needanotherupdate = true;
            clickquicknavmenu = false;
          }
        }
        recti = new RectF(x1, y1, x2, y2);
        surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
        recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
        surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
        if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
          RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
          surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
        }
        surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

        mabase += model.GenericCaseH + model.GenericInterSpace;
        if (mabase > mysurf.myheight)
          return;

        ecritoption = "2x";
        y1 = mabase;
        y2 = mabase + model.GenericCaseH;
        taillex = model.Menu1TextePaint.measureText(ecritoption);
        x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
        x2 = mysurf.ScreenWidth - model.GenericInterSpace;
        if (x1 > mysurf.SettingsXmin) {
          x1 = mysurf.SettingsXmin;
        }
        ecritx = x2 - model.GenericInterSpace;
        ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
        if (clickquicknavmenu) {
          if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
            currmpvlib.speed = 2.0;
            currmpvlib.setspeed(currmpvlib.speed);
            needanotherupdate = true;
            clickquicknavmenu = false;
          }
        }
        recti = new RectF(x1, y1, x2, y2);
        surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
        recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
        surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
        if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
          RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
          surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
        }
        surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

        mabase += model.GenericCaseH + model.GenericInterSpace;
        if (mabase > mysurf.myheight)
          return;

        ecritoption = "5x";
        y1 = mabase;
        y2 = mabase + model.GenericCaseH;
        taillex = model.Menu1TextePaint.measureText(ecritoption);
        x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
        x2 = mysurf.ScreenWidth - model.GenericInterSpace;
        if (x1 > mysurf.SettingsXmin) {
          x1 = mysurf.SettingsXmin;
        }
        ecritx = x2 - model.GenericInterSpace;
        ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
        if (clickquicknavmenu) {
          if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
            currmpvlib.speed = 5.0;
            currmpvlib.setspeed(currmpvlib.speed);
            needanotherupdate = true;
            clickquicknavmenu = false;
          }
        }
        recti = new RectF(x1, y1, x2, y2);
        surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
        recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
        surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
        if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
          RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
          surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
        }
        surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

        mabase += model.GenericCaseH + model.GenericInterSpace;
        if (mabase > mysurf.myheight)
          return;

        y1 = mabase;
        y2 = mabase + model.GenericCaseH;
        ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;

        ecritoption = "+";
        taillex = model.Menu1TextePaint.measureText(ecritoption);
        x1 = (mysurf.ScreenWidth - model.GenericInterSpace + mysurf.SettingsXmin) * 0.5f + model.GenericInterSpace * 0.5f;
        x2 = mysurf.ScreenWidth - model.GenericInterSpace;
        ecritx = x2;
        if (clickquicknavmenu) {
          if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
            currmpvlib.speed += speedpower[speedpoweri];
            currmpvlib.setspeed(currmpvlib.speed);
            needanotherupdate = true;
            clickquicknavmenu = false;
          }
        }
        recti = new RectF(x1, y1, x2, y2);
        //surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
        //recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
        surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
        if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
          RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
          surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
        }
        surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

        ecritoption = String.format("%.2fx -", currmpvlib.speed);
        taillex = model.Menu1TextePaint.measureText(ecritoption);
        x2 = x1 - model.GenericInterSpace;
        x1 = x2 - model.GenericInterSpace * 2.0f - taillex;
        if (x1 > mysurf.SettingsXmin)
          x1 = mysurf.SettingsXmin;
        ecritx = x2 - model.GenericInterSpace;
        if (clickquicknavmenu) {
          if (mysurf.SettingsXmin < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
            currmpvlib.speed -= speedpower[speedpoweri];
            currmpvlib.setspeed(currmpvlib.speed);
            needanotherupdate = true;
            clickquicknavmenu = false;
          }
        }
        recti = new RectF(x1, y1, x2, y2);
        surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
        recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
        surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
        if ((model.isandroidtv || model.keypadnavactive) && mysurf.SettingsXmin < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
          RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
          surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
        }
        surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

        mabase += model.GenericCaseH + model.GenericInterSpace;
        if (mabase > mysurf.myheight)
          return;

        ecritoption = String.format("+/- %.2f", speedpower[speedpoweri]);
        y1 = mabase;
        y2 = mabase + model.GenericCaseH;
        taillex = model.Menu1TextePaint.measureText(ecritoption);
        x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
        x2 = mysurf.ScreenWidth - model.GenericInterSpace;
        if (x1 > mysurf.SettingsXmin) {
          x1 = mysurf.SettingsXmin;
        }
        ecritx = x2 - model.GenericInterSpace;
        ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
        if (clickquicknavmenu) {
          if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
            speedpoweri = (speedpoweri + 1) % speedpower.length;
            needanotherupdate = true;
            clickquicknavmenu = false;
          }
        }
        recti = new RectF(x1, y1, x2, y2);
        surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
        recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
        surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
        if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
          RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
          surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
        }
        surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

        mabase += model.GenericCaseH + model.GenericInterSpace;

      }
  }

  private void menurotate(Canvas surfacecanvas) {
      ecritoption = String.format("rotate %.0f°", currmpvlib.videorotate);
      y1 = mabase;
      y2 = mabase + model.GenericCaseH;
      taillex = model.Menu1TextePaint.measureText(ecritoption);
      x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
      x2 = mysurf.ScreenWidth - model.GenericInterSpace;
      if (x1 > mysurf.SettingsXmin) {
        x1 = mysurf.SettingsXmin;
      }
      ecritx = x2 - model.GenericInterSpace;
      ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
      if (clickquicknavmenu) {
        if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
          mysurf.videoshowrotate = !mysurf.videoshowrotate;
          needanotherupdate = true;
          clickquicknavmenu = false;
        }
      }
      recti = new RectF(x1, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu1BgPainta);
      recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu1BgPainto);
      if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
        RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
        surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
      }
      surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

      mabase += model.GenericCaseH + model.GenericInterSpace;
    if (mabase > mysurf.myheight)
      return;

      if (mysurf.videoshowrotate) {

        ecritoption = "normal";
        y1 = mabase;
        y2 = mabase + model.GenericCaseH;
        taillex = model.Menu1TextePaint.measureText(ecritoption);
        x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
        x2 = mysurf.ScreenWidth - model.GenericInterSpace;
        if (x1 > mysurf.SettingsXmin) {
          x1 = mysurf.SettingsXmin;
        }
        ecritx = x2 - model.GenericInterSpace;
        ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
        if (clickquicknavmenu) {
          if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
            currmpvlib.videorotate = 0.0;
            currmpvlib.setrotate(currmpvlib.videorotate);
            needanotherupdate = true;
            clickquicknavmenu = false;
          }
        }
        recti = new RectF(x1, y1, x2, y2);
        surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
        recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
        surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
        if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
          RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
          surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
        }
        surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

        mabase += model.GenericCaseH + model.GenericInterSpace;
        if (mabase > mysurf.myheight)
          return;

        ecritoption = "90°";
        y1 = mabase;
        y2 = mabase + model.GenericCaseH;
        taillex = model.Menu1TextePaint.measureText(ecritoption);
        x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
        x2 = mysurf.ScreenWidth - model.GenericInterSpace;
        if (x1 > mysurf.SettingsXmin) {
          x1 = mysurf.SettingsXmin;
        }
        ecritx = x2 - model.GenericInterSpace;
        ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
        if (clickquicknavmenu) {
          if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
            currmpvlib.videorotate = 90.0;
            currmpvlib.setrotate(currmpvlib.videorotate);
            needanotherupdate = true;
            clickquicknavmenu = false;
          }
        }
        recti = new RectF(x1, y1, x2, y2);
        surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
        recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
        surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
        if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
          RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
          surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
        }
        surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

        mabase += model.GenericCaseH + model.GenericInterSpace;
        if (mabase > mysurf.myheight)
          return;

        ecritoption = "180°";
        y1 = mabase;
        y2 = mabase + model.GenericCaseH;
        taillex = model.Menu1TextePaint.measureText(ecritoption);
        x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
        x2 = mysurf.ScreenWidth - model.GenericInterSpace;
        if (x1 > mysurf.SettingsXmin) {
          x1 = mysurf.SettingsXmin;
        }
        ecritx = x2 - model.GenericInterSpace;
        ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
        if (clickquicknavmenu) {
          if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
            currmpvlib.videorotate = 180.0;
            currmpvlib.setrotate(currmpvlib.videorotate);
            needanotherupdate = true;
            clickquicknavmenu = false;
          }
        }
        recti = new RectF(x1, y1, x2, y2);
        surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
        recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
        surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
        if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
          RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
          surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
        }
        surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

        mabase += model.GenericCaseH + model.GenericInterSpace;
        if (mabase > mysurf.myheight)
          return;

        ecritoption = "270°";
        y1 = mabase;
        y2 = mabase + model.GenericCaseH;
        taillex = model.Menu1TextePaint.measureText(ecritoption);
        x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
        x2 = mysurf.ScreenWidth - model.GenericInterSpace;
        if (x1 > mysurf.SettingsXmin) {
          x1 = mysurf.SettingsXmin;
        }
        ecritx = x2 - model.GenericInterSpace;
        ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
        if (clickquicknavmenu) {
          if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
            currmpvlib.videorotate = 270.0;
            currmpvlib.setrotate(currmpvlib.videorotate);
            needanotherupdate = true;
            clickquicknavmenu = false;
          }
        }
        recti = new RectF(x1, y1, x2, y2);
        surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
        recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
        surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
        if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
          RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
          surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
        }
        surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

        mabase += model.GenericCaseH + model.GenericInterSpace;
        if (mabase > mysurf.myheight)
          return;

      }
  }

  private void menuzoom(Canvas surfacecanvas) {
    y1 = mabase;
    y2 = mabase + model.GenericCaseH;
    ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;

    ecritoption = "+";
    taillex = model.Menu1TextePaint.measureText(ecritoption);
    x1 = (mysurf.ScreenWidth - model.GenericInterSpace + mysurf.SettingsXmin) * 0.5f + model.GenericInterSpace * 0.5f;
    x2 = mysurf.ScreenWidth - model.GenericInterSpace;
    ecritx = x2;
    if (clickquicknavmenu) {
      if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
        currmpvlib.videozoom += zoompower[zoompoweri];
        currmpvlib.setzoom(currmpvlib.videozoom);
        needanotherupdate = true;
        clickquicknavmenu = false;
      }
    }
    recti = new RectF(x1, y1, x2, y2);
    //surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
    //recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
    surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
    if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
      RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
      surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
    }
    surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

    ecritoption = String.format("zoom %.2fx -", currmpvlib.videozoom);
    taillex = model.Menu1TextePaint.measureText(ecritoption);
    x2 = x1 - model.GenericInterSpace;
    x1 = x2 - model.GenericInterSpace * 2.0f - taillex;
      if (x1 > mysurf.SettingsXmin)
        x1 = mysurf.SettingsXmin;
    ecritx = x2 - model.GenericInterSpace;
    if (clickquicknavmenu) {
      if (mysurf.SettingsXmin < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
        currmpvlib.videozoom -= zoompower[zoompoweri];
        currmpvlib.setzoom(currmpvlib.videozoom);
        needanotherupdate = true;
        clickquicknavmenu = false;
      }
    }
    recti = new RectF(x1, y1, x2, y2);
    surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
    recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
    surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
    if ((model.isandroidtv || model.keypadnavactive) && mysurf.SettingsXmin < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
      RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
      surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
    }
    surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

    mabase += model.GenericCaseH + model.GenericInterSpace;
    if (mabase > mysurf.myheight)
      return;
  }

  private void menupanx(Canvas surfacecanvas) {
    y1 = mabase;
    y2 = mabase + model.GenericCaseH;
    ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;

    ecritoption = "+";
    taillex = model.Menu1TextePaint.measureText(ecritoption);
    x1 = (mysurf.ScreenWidth - model.GenericInterSpace + mysurf.SettingsXmin) * 0.5f + model.GenericInterSpace * 0.5f;
    x2 = mysurf.ScreenWidth - model.GenericInterSpace;
    ecritx = x2;
    if (clickquicknavmenu) {
      if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
        currmpvlib.videopanx += zoompower[zoompoweri];
        currmpvlib.setvideopanx(currmpvlib.videopanx);
        needanotherupdate = true;
        clickquicknavmenu = false;
      }
    }
    recti = new RectF(x1, y1, x2, y2);
    //surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
    //recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
    surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
    if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
      RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
      surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
    }
    surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

    ecritoption = String.format("pan x %.2f -", currmpvlib.videopanx);
    taillex = model.Menu1TextePaint.measureText(ecritoption);
    x2 = x1 - model.GenericInterSpace;
    x1 = x2 - model.GenericInterSpace * 2.0f - taillex;
      if (x1 > mysurf.SettingsXmin)
        x1 = mysurf.SettingsXmin;
    ecritx = x2 - model.GenericInterSpace;
    if (clickquicknavmenu) {
      if (mysurf.SettingsXmin < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
        currmpvlib.videopanx -= zoompower[zoompoweri];
        currmpvlib.setvideopanx(currmpvlib.videopanx);
        needanotherupdate = true;
        clickquicknavmenu = false;
      }
    }
    recti = new RectF(x1, y1, x2, y2);
    surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
    recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
    surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
    if ((model.isandroidtv || model.keypadnavactive) && mysurf.SettingsXmin < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
      RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
      surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
    }
    surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

    mabase += model.GenericCaseH + model.GenericInterSpace;
    if (mabase > mysurf.myheight)
      return;
  }

  private void menupany(Canvas surfacecanvas) {
    y1 = mabase;
    y2 = mabase + model.GenericCaseH;
    ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;

    ecritoption = "+";
    taillex = model.Menu1TextePaint.measureText(ecritoption);
    x1 = (mysurf.ScreenWidth - model.GenericInterSpace + mysurf.SettingsXmin) * 0.5f + model.GenericInterSpace * 0.5f;
    x2 = mysurf.ScreenWidth - model.GenericInterSpace;
    ecritx = x2;
    if (clickquicknavmenu) {
      if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
        currmpvlib.videopany += zoompower[zoompoweri];
        currmpvlib.setvideopany(currmpvlib.videopany);
        needanotherupdate = true;
        clickquicknavmenu = false;
      }
    }
    recti = new RectF(x1, y1, x2, y2);
    //surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
    //recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
    surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
    if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
      RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
      surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
    }
    surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

    ecritoption = String.format("pan y %.2f -", currmpvlib.videopany);
    taillex = model.Menu1TextePaint.measureText(ecritoption);
    x2 = x1 - model.GenericInterSpace;
    x1 = x2 - model.GenericInterSpace * 2.0f - taillex;
      if (x1 > mysurf.SettingsXmin)
        x1 = mysurf.SettingsXmin;
    ecritx = x2 - model.GenericInterSpace;
    if (clickquicknavmenu) {
      if (mysurf.SettingsXmin < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
        currmpvlib.videopany -= zoompower[zoompoweri];
        currmpvlib.setvideopany(currmpvlib.videopany);
        needanotherupdate = true;
        clickquicknavmenu = false;
      }
    }
    recti = new RectF(x1, y1, x2, y2);
    surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
    recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
    surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
    if ((model.isandroidtv || model.keypadnavactive) && mysurf.SettingsXmin < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
      RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
      surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
    }
    surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

    mabase += model.GenericCaseH + model.GenericInterSpace;
    if (mabase > mysurf.myheight)
      return;

    ecritoption = String.format("+/- %.2f", zoompower[zoompoweri]);
    y1 = mabase;
    y2 = mabase + model.GenericCaseH;
    taillex = model.Menu1TextePaint.measureText(ecritoption);
    x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
    x2 = mysurf.ScreenWidth - model.GenericInterSpace;
    if (x1 > mysurf.SettingsXmin) {
      x1 = mysurf.SettingsXmin;
    }
    ecritx = x2 - model.GenericInterSpace;
    ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
    if (clickquicknavmenu) {
      if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
        zoompoweri = (zoompoweri + 1) % zoompower.length;
        needanotherupdate = true;
        clickquicknavmenu = false;
      }
    }
    recti = new RectF(x1, y1, x2, y2);
    surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
    recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
    surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
    if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
      RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
      surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
    }
    surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

    mabase += model.GenericCaseH + model.GenericInterSpace;
    if (mabase > mysurf.myheight)
      return;

    if (currmpvlib.videozoom != 0 || currmpvlib.videopanx != 0 || currmpvlib.videopany != 0) {
      ecritoption = "original";
      y1 = mabase;
      y2 = mabase + model.GenericCaseH;
      taillex = model.Menu1TextePaint.measureText(ecritoption);
      x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
      x2 = mysurf.ScreenWidth - model.GenericInterSpace;
      if (x1 > mysurf.SettingsXmin) {
        x1 = mysurf.SettingsXmin;
      }
      ecritx = x2 - model.GenericInterSpace;
      ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
      if (clickquicknavmenu) {
        if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
          currmpvlib.videozoom = 0.0;
          currmpvlib.setzoom(currmpvlib.videozoom);
          currmpvlib.videopanx = 0.0;
          currmpvlib.setvideopanx(currmpvlib.videopanx);
          currmpvlib.videopany = 0.0;
          currmpvlib.setvideopany(currmpvlib.videopany);
          needanotherupdate = true;
          clickquicknavmenu = false;
        }
      }
      recti = new RectF(x1, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
      recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
      if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
        RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
        surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
      }
      surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

      mabase += model.GenericCaseH + model.GenericInterSpace;
      if (mabase > mysurf.myheight)
        return;
    }
  }


  private void menumodify(Canvas surfacecanvas) {

      if (mysurf.filterlist.length == 0) {
        int fl = (int) (Gallery.filterlist.length / 3);
        mysurf.filterprint = new String[fl];
        mysurf.filterlist = new String[fl];
        mysurf.filteractive = new boolean[fl];
        mysurf.filterinfo = new String[fl];
        for (int f = 0 ; f < fl ; f++) {
          mysurf.filterprint[f] = Gallery.filterlist[f * 3 + 0];
          mysurf.filterlist[f] = Gallery.filterlist[f * 3 + 1];
          mysurf.filterinfo[f] = Gallery.filterlist[f * 3 + 2];
          mysurf.filteractive[f] = false;
        }
      }

      int fl = mysurf.filterlist.length;
      for (int f = 0 ; f < fl ; f++) {

        if (f == Gallery.filterrotate) {
          ecritoption = "rotate";
          y1 = mabase;
          y2 = mabase + model.GenericCaseH;
          taillex = model.Menu1TextePaint.measureText(ecritoption);
          x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
          x2 = mysurf.ScreenWidth - model.GenericInterSpace;
          if (x1 > mysurf.SettingsXmin) {
            x1 = mysurf.SettingsXmin;
          }
          ecritx = x2 - model.GenericInterSpace;
          ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
          if (clickquicknavmenu) {
            if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
              mysurf.filteractive[f] = !mysurf.filteractive[f];
              String filter = null;
              for (int g = 0; g < fl; g++) {
                if (mysurf.filteractive[g]) {
                  if (filter == null)
                    filter = mysurf.filterlist[g];
                  else
                    filter += "," + mysurf.filterlist[g];
                }
              }
              mysurf.filter = filter;
              currmpvlib.setfilter(filter);
              needanotherupdate = true;
              clickquicknavmenu = false;
            }
          }
          recti = new RectF(x1, y1, x2, y2);
          surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
          recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
          surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
          if (mysurf.filteractive[f]) {
            RectF rectisel = new RectF(x1 + quatredixiememillimetre, y1 + quatredixiememillimetre, x2 - quatredixiememillimetre, y2 - quatredixiememillimetre);
            surfacecanvas.drawRoundRect(rectisel, roundedRectRatio, roundedRectRatio, model.SettingsSelPaint);
          }
          if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
            RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
            surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
          }
          surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

          mabase += model.GenericCaseH + model.GenericInterSpace;
          if (mabase > mysurf.myheight)
            return;

        } else if (f == Gallery.filterhflipvflip) {
          // 1 hflip 2 vflip

          y1 = mabase;
          y2 = mabase + model.GenericCaseH;
          ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;

          ecritoption = "v ";
          taillex = model.Menu1TextePaint.measureText(ecritoption);
          x1 = (mysurf.ScreenWidth - model.GenericInterSpace + mysurf.SettingsXmin) * 0.5f + model.GenericInterSpace * 0.5f;
          x2 = mysurf.ScreenWidth - model.GenericInterSpace;
          ecritx = x2;
          if (clickquicknavmenu) {
            if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
              mysurf.filteractive[f+1] = !mysurf.filteractive[f+1];
              String filter = null;
              for (int g = 0; g < fl; g++) {
                if (mysurf.filteractive[g]) {
                  if (filter == null)
                    filter = mysurf.filterlist[g];
                  else
                    filter += "," + mysurf.filterlist[g];
                }
              }
              mysurf.filter = filter;
              currmpvlib.setfilter(filter);
              needanotherupdate = true;
              clickquicknavmenu = false;
            }
          }
          recti = new RectF(x1, y1, x2, y2);
          //surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
          //recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
          surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
          if (mysurf.filteractive[f+1]) {
            RectF rectisel = new RectF(x1 + quatredixiememillimetre, y1 + quatredixiememillimetre, x2 - quatredixiememillimetre, y2 - quatredixiememillimetre);
            surfacecanvas.drawRoundRect(rectisel, roundedRectRatio, roundedRectRatio, model.SettingsSelPaint);
          }
          if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
            RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
            surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
          }
          surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

          ecritoption = "flip h";
          taillex = model.Menu1TextePaint.measureText(ecritoption);
          x2 = x1 - model.GenericInterSpace;
          x1 = x2 - model.GenericInterSpace * 2.0f - taillex;
      if (x1 > mysurf.SettingsXmin)
        x1 = mysurf.SettingsXmin;
          ecritx = x2 - model.GenericInterSpace;
          if (clickquicknavmenu) {
            if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
              mysurf.filteractive[f] = !mysurf.filteractive[f];
              String filter = null;
              for (int g = 0; g < fl; g++) {
                if (mysurf.filteractive[g]) {
                  if (filter == null)
                    filter = mysurf.filterlist[g];
                  else
                    filter += "," + mysurf.filterlist[g];
                }
              }
              mysurf.filter = filter;
              currmpvlib.setfilter(filter);
              needanotherupdate = true;
              clickquicknavmenu = false;
            }
          }
          recti = new RectF(x1, y1, x2, y2);
          surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
          recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
          surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
          if (mysurf.filteractive[f]) {
            RectF rectisel = new RectF(x1 + quatredixiememillimetre, y1 + quatredixiememillimetre, x2 - quatredixiememillimetre, y2 - quatredixiememillimetre);
            surfacecanvas.drawRoundRect(rectisel, roundedRectRatio, roundedRectRatio, model.SettingsSelPaint);
          }
          if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
            RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
            surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
          }
          surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

          mabase += model.GenericCaseH + model.GenericInterSpace;
          if (mabase > mysurf.myheight)
            return;

        } else if (f == Gallery.filtervflip) { // vflip

        } else if (Gallery.filterbcsgwi <= f && f <= Gallery.filterbcsgwf) {

          y1 = mabase;
          y2 = mabase + model.GenericCaseH;
          ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;

          ecritoption = "+ ";
          taillex = model.Menu1TextePaint.measureText(ecritoption);
          x1 = (mysurf.ScreenWidth - model.GenericInterSpace + mysurf.SettingsXmin) * 0.5f + model.GenericInterSpace * 0.5f;
          x2 = mysurf.ScreenWidth - model.GenericInterSpace;
          ecritx = x2;
          if (clickquicknavmenu) {
            if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
              if (f == Gallery.filterb) {
                mysurf.brightness += mysurf.deltabright[mysurf.deltabrighti];
                if (-0.001 < mysurf.brightness && mysurf.brightness < 0.001) {
                  mysurf.filteractive[f] = false;
                } else {
                  mysurf.filteractive[f] = true;
                  mysurf.filterlist[f] = String.format("eq=brightness=%.3f", mysurf.brightness);
                }
              } else if (f == Gallery.filterc) {
                mysurf.contrast += mysurf.deltabright[mysurf.deltabrighti];
                if (0.999 < mysurf.contrast && mysurf.contrast < 1.001) {
                  mysurf.filteractive[f] = false;
                } else {
                  mysurf.filteractive[f] = true;
                  mysurf.filterlist[f] = String.format("eq=contrast=%.3f", mysurf.contrast);
                }
              } else if (f == Gallery.filters) {
                mysurf.saturation += mysurf.deltabright[mysurf.deltabrighti];
                if (0.999 < mysurf.saturation && mysurf.saturation < 1.001) {
                  mysurf.filteractive[f] = false;
                } else {
                  mysurf.filteractive[f] = true;
                  mysurf.filterlist[f] = String.format("eq=saturation=%.3f", mysurf.saturation);
                }
              } else if (f == Gallery.filterg) {
                mysurf.gamma += mysurf.deltabright[mysurf.deltabrighti];
                if (0.999 < mysurf.gamma && mysurf.gamma < 1.001) {
                  mysurf.filteractive[f] = false;
                } else {
                  mysurf.filteractive[f] = true;
                  mysurf.filterlist[f] = String.format("eq=gamma=%.3f", mysurf.gamma);
                }
              } else if (f == Gallery.filterw) {
                mysurf.gamma_weight += mysurf.deltabright[mysurf.deltabrighti];
                if (0.999 < mysurf.gamma_weight && mysurf.gamma_weight < 1.001) {
                  mysurf.filteractive[f] = false;
                } else {
                  mysurf.filteractive[f] = true;
                  mysurf.filterlist[f] = String.format("eq=gamma_weight=%.3f", mysurf.gamma_weight);
                }
              }
              String filter = null;
              for (int g = 0; g < fl; g++) {
                if (mysurf.filteractive[g]) {
                  if (filter == null)
                    filter = mysurf.filterlist[g];
                  else
                    filter += "," + mysurf.filterlist[g];
                }
              }
              mysurf.filter = filter;
              currmpvlib.setfilter(filter);
              needanotherupdate = true;
              clickquicknavmenu = false;
            }
          }
          recti = new RectF(x1, y1, x2, y2);
          //surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
          //recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
          surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
          if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
            RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
            surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
          }
          surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);


          if (f == Gallery.filterb) {
            if (mysurf.filteractive[f])
              ecritoption = String.format("brightness %.3f -", mysurf.brightness);
            else
              ecritoption = "brightness -";
          } else if (f == Gallery.filterc) {
            if (mysurf.filteractive[f])
              ecritoption = String.format("contrast %.3f -", mysurf.contrast);
            else
              ecritoption = "contrast -";
          } else if (f == Gallery.filters) {
            if (mysurf.filteractive[f])
              ecritoption = String.format("saturation %.3f -", mysurf.saturation);
            else
              ecritoption = "saturation -";
          } else if (f == Gallery.filterg) {
            if (mysurf.filteractive[f])
              ecritoption = String.format("gamma %.3f -", mysurf.gamma);
            else
              ecritoption = "gamma -";
          } else if (f == Gallery.filterw) {
            if (mysurf.filteractive[f])
              ecritoption = String.format("gammaweight %.3f -", mysurf.gamma_weight);
            else
              ecritoption = "gammaweight -";
          }
          taillex = model.Menu1TextePaint.measureText(ecritoption);
          x2 = x1 - model.GenericInterSpace;
          x1 = x2 - model.GenericInterSpace * 2.0f - taillex;
      if (x1 > mysurf.SettingsXmin)
        x1 = mysurf.SettingsXmin;
          ecritx = x2 - model.GenericInterSpace;
          if (clickquicknavmenu) {
            if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
              if (f == Gallery.filterb) {
                mysurf.brightness -= mysurf.deltabright[mysurf.deltabrighti];
                if (-0.001 < mysurf.brightness && mysurf.brightness < 0.001) {
                  mysurf.filteractive[f] = false;
                } else {
                  mysurf.filteractive[f] = true;
                  mysurf.filterlist[f] = String.format("eq=brightness=%.3f", mysurf.brightness);
                }
              } else if (f == Gallery.filterc) {
                mysurf.contrast -= mysurf.deltabright[mysurf.deltabrighti];
                if (0.999 < mysurf.contrast && mysurf.contrast < 1.001) {
                  mysurf.filteractive[f] = false;
                } else {
                  mysurf.filteractive[f] = true;
                  mysurf.filterlist[f] = String.format("eq=contrast=%.3f", mysurf.contrast);
                }
              } else if (f == Gallery.filters) {
                mysurf.saturation -= mysurf.deltabright[mysurf.deltabrighti];
                if (0.999 < mysurf.saturation && mysurf.saturation < 1.001) {
                  mysurf.filteractive[f] = false;
                } else {
                  mysurf.filteractive[f] = true;
                  mysurf.filterlist[f] = String.format("eq=saturation=%.3f", mysurf.saturation);
                }
              } else if (f == Gallery.filterg) {
                mysurf.gamma -= mysurf.deltabright[mysurf.deltabrighti];
                if (0.999 < mysurf.gamma && mysurf.gamma < 1.001) {
                  mysurf.filteractive[f] = false;
                } else {
                  mysurf.filteractive[f] = true;
                  mysurf.filterlist[f] = String.format("eq=gamma=%.3f", mysurf.gamma);
                }
              } else if (f == Gallery.filterw) {
                mysurf.gamma_weight -= mysurf.deltabright[mysurf.deltabrighti];
                if (0.999 < mysurf.gamma_weight && mysurf.gamma_weight < 1.001) {
                  mysurf.filteractive[f] = false;
                } else {
                  mysurf.filteractive[f] = true;
                  mysurf.filterlist[f] = String.format("eq=gamma_weight=%.3f", mysurf.gamma_weight);
                }
              }
              String filter = null;
              for (int g = 0; g < fl; g++) {
                if (mysurf.filteractive[g]) {
                  if (filter == null)
                    filter = mysurf.filterlist[g];
                  else
                    filter += "," + mysurf.filterlist[g];
                }
              }
              mysurf.filter = filter;
              currmpvlib.setfilter(filter);
              needanotherupdate = true;
              clickquicknavmenu = false;
            }
          }
          recti = new RectF(x1, y1, x2, y2);
          surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
          recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
          surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
          if (mysurf.filteractive[f]) {
            RectF rectisel = new RectF(x1 + quatredixiememillimetre, y1 + quatredixiememillimetre, x2 - quatredixiememillimetre, y2 - quatredixiememillimetre);
            surfacecanvas.drawRoundRect(rectisel, roundedRectRatio, roundedRectRatio, model.SettingsSelPaint);
          }
          if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
            RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
            surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
          }
          surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

          mabase += model.GenericCaseH + model.GenericInterSpace;
          if (mabase > mysurf.myheight)
            return;

        } else if (f == Gallery.filterreset) {

          y1 = mabase;
          y2 = mabase + model.GenericCaseH;
          ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;

          ecritoption = "reset";
          taillex = model.Menu1TextePaint.measureText(ecritoption);
          x1 = (mysurf.ScreenWidth - model.GenericInterSpace + mysurf.SettingsXmin) * 0.5f + model.GenericInterSpace * 0.5f;
          x2 = mysurf.ScreenWidth - model.GenericInterSpace;
          ecritx = x2;
          if (clickquicknavmenu) {
            if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
              for (int ff = model.filterb ; ff <= model.filterw ; ff++) {
                mysurf.filteractive[ff] = false;
              }
              mysurf.brightness = 0.0f;
              mysurf.contrast = 1.0f;
              mysurf.saturation = 1.0f;
              mysurf.gamma = 1.0f;
              mysurf.gamma_weight = 1.0f;
              String filter = null;
              for (int g = 0; g < fl; g++) {
                if (mysurf.filteractive[g]) {
                  if (filter == null)
                    filter = mysurf.filterlist[g];
                  else
                    filter += "," + mysurf.filterlist[g];
                }
              }
              mysurf.filter = filter;
              currmpvlib.setfilter(filter);
              needanotherupdate = true;
              clickquicknavmenu = false;
            }
          }
          recti = new RectF(x1, y1, x2, y2);
          //surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
          //recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
          surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
          if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
            RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
            surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
          }
          surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);


          ecritoption = String.format("+/- %.3f", mysurf.deltabright[mysurf.deltabrighti]);
          taillex = model.Menu1TextePaint.measureText(ecritoption);
          x2 = x1 - model.GenericInterSpace;
          x1 = x2 - model.GenericInterSpace * 2.0f - taillex;
      if (x1 > mysurf.SettingsXmin)
        x1 = mysurf.SettingsXmin;
          ecritx = x2 - model.GenericInterSpace;
          if (clickquicknavmenu) {
            if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
              mysurf.deltabrighti = (mysurf.deltabrighti + 1) % Surf.deltabright.length;
              needanotherupdate = true;
              clickquicknavmenu = false;
            }
          }
          recti = new RectF(x1, y1, x2, y2);
          surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
          recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
          surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
          if (mysurf.filteractive[f]) {
            RectF rectisel = new RectF(x1 + quatredixiememillimetre, y1 + quatredixiememillimetre, x2 - quatredixiememillimetre, y2 - quatredixiememillimetre);
            surfacecanvas.drawRoundRect(rectisel, roundedRectRatio, roundedRectRatio, model.SettingsSelPaint);
          }
          if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
            RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
            surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
          }
          surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

          mabase += model.GenericCaseH + model.GenericInterSpace;
          if (mabase > mysurf.myheight)
            return;


        } else {

          y1 = mabase;
          y2 = mabase + model.GenericCaseH;
          ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;

          ecritoption = "... ";
          taillex = model.Menu1TextePaint.measureText(ecritoption);
          x1 = (mysurf.ScreenWidth - model.GenericInterSpace + mysurf.SettingsXmin) * 0.5f + model.GenericInterSpace * 0.5f;
          x2 = mysurf.ScreenWidth - model.GenericInterSpace;
          ecritx = x2;
          if (clickquicknavmenu) {
            if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
              model.message(
                  mysurf.filterprint[f] + "\n" +
                      mysurf.filterlist[f] + "\n" +
                      mysurf.filterinfo[f]
              );
              if (mysurf.filterprint[f].equals("crop width")) {
                /*mysurf.cropw = true;
                mysurf.croph = false;*/
                if (mysurf.srcrect.left < mysurf.srcrect.right) {
                  mysurf.filterlist[f] = "crop=w=" + (mysurf.srcrect.right - mysurf.srcrect.left) + ":x=" + mysurf.srcrect.left;
                }
              } else if (mysurf.filterprint[f].equals("crop height")) {
                /*mysurf.cropw = false;
                mysurf.croph = true;*/
                if (mysurf.srcrect.top < mysurf.srcrect.bottom) {
                  mysurf.filterlist[f] = "crop=h=" + (mysurf.srcrect.bottom - mysurf.srcrect.top) + ":y=" + mysurf.srcrect.top;
                }
              }/* else {
                mysurf.cropw = false;
                mysurf.croph = false;
              }*/
              if (mysurf.filterlastselected == f) {
                mysurf.filterlastselected = f;
                Intent intent = new Intent();
                intent.setAction(Gallery.broadcastname);
                intent.putExtra("goal", "filter");
                intent.putExtra("id", currid);
                intent.putExtra("video", true);
                LocalBroadcastManager.getInstance(model.activitycontext).sendBroadcast(intent);
              }
              mysurf.filterlastselected = f;
              needanotherupdate = true;
              clickquicknavmenu = false;
            }
          }
          recti = new RectF(x1, y1, x2, y2);
          //surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
          //recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
          surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
          if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
            RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
            surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
          }
          surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);


          if (mysurf.filterprint[f].equals(""))
            ecritoption = mysurf.filterlist[f];
          else
            ecritoption = mysurf.filterprint[f];
          taillex = model.Menu1TextePaint.measureText(ecritoption);
          x2 = x1 - model.GenericInterSpace;
          x1 = x2 - model.GenericInterSpace * 2.0f - taillex;
      if (x1 > mysurf.SettingsXmin)
        x1 = mysurf.SettingsXmin;
          ecritx = x2 - model.GenericInterSpace;
          if (clickquicknavmenu) {
            if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
              if (mysurf.filterlist[f].equals("")) {
                mysurf.filteractive[f] = false;
              } else {
                mysurf.filteractive[f] = !mysurf.filteractive[f];
                if (mysurf.filteractive[f]) {
                  if (mysurf.filterprint[f].equals("crop width")) {
                    if (mysurf.srcrect.left < mysurf.srcrect.right) {
                      mysurf.filterlist[f] = "crop=w=" + (mysurf.srcrect.right - mysurf.srcrect.left) + ":x=" + mysurf.srcrect.left;
                      mysurf.fichierprecedent = "dummy";
                      mysurf.bpx = 0;
                      mysurf.bpy = 0;
                      mysurf.bscale = 1;
                      mysurf.centeronscreen = true;
                    }
                  } else if (mysurf.filterprint[f].equals("crop height")) {
                    if (mysurf.srcrect.top < mysurf.srcrect.bottom) {
                      mysurf.filterlist[f] = "crop=h=" + (mysurf.srcrect.bottom - mysurf.srcrect.top) + ":y=" + mysurf.srcrect.top;
                      mysurf.fichierprecedent = "dummy";
                      mysurf.bpx = 0;
                      mysurf.bpy = 0;
                      mysurf.bscale = 1;
                      mysurf.centeronscreen = true;
                    }
                  }
                }
                String filter = null;
                for (int g = 0; g < fl; g++) {
                  if (mysurf.filteractive[g]) {
                    if (filter == null)
                      filter = mysurf.filterlist[g];
                    else
                      filter += "," + mysurf.filterlist[g];
                  }
                }
                mysurf.filter = filter;
                currmpvlib.setfilter(filter);
              }
              needanotherupdate = true;
              clickquicknavmenu = false;
            }
          }
          recti = new RectF(x1, y1, x2, y2);
          surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
          recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
          surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
          if (mysurf.filteractive[f]) {
            RectF rectisel = new RectF(x1 + quatredixiememillimetre, y1 + quatredixiememillimetre, x2 - quatredixiememillimetre, y2 - quatredixiememillimetre);
            surfacecanvas.drawRoundRect(rectisel, roundedRectRatio, roundedRectRatio, model.SettingsSelPaint);
          }
          if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
            RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
            surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
          }
          surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

          mabase += model.GenericCaseH + model.GenericInterSpace;
          if (mabase > mysurf.myheight)
            return;
        }
      }

      /*
      ecritoption = "save modified";
      y1 = mabase;
      y2 = mabase + model.GenericCaseH;
      taillex = model.Menu1TextePaint.measureText(ecritoption);
      x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
      x2 = mysurf.ScreenWidth - model.GenericInterSpace;
      if (x1 > mysurf.SettingsXmin) {
        x1 = mysurf.SettingsXmin;
      }
      ecritx = x2 - model.GenericInterSpace;
      ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
      if (clickquicknavmenu) {
        if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
          int folderfilecount = model.getFolderFileCount(mysurf.ordnerIndex);
          if (mysurf.mediaIndex < folderfilecount && folderfilecount > 0) {
            String fichname = model.getFileName(mysurf.ordnerIndex, mysurf.mediaIndex);
            if (mysurf.isincache(fichname)) {
              DateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmss");
              Date date = new Date();
              String destpath = model.dossierdessin + fichname.replaceAll(".+/", "") + dateFormat.format(date) + ".png";
              if (mysurf.saveCachedBitmap(fichname, destpath)) {
                model.message("picture saved as\n" + destpath);
                llog.d(TAG, "picture saved as " + destpath);
                try {
                  model.commandethreaddatabase.put(new String[]{"chercheFichierDansDatabaseSinonRescanSonDossier", model.dossierdessin, destpath, "-1"});
                } catch (InterruptedException e) {
                  e.printStackTrace();
                }
              } else {
                model.message("Could not save\n" + destpath);
                llog.d(TAG, "Error could not save " + destpath);
              }
            }
          }
          needanotherupdate = true;
          clickquicknavmenu = false;
        }
      }
      recti = new RectF(x1, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
      recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
      if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
        RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
        surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
      }
      surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

      mabase += model.GenericCaseH + model.GenericInterSpace;
      if (mabase > mysurf.myheight)
        return;
      */

  }

  private void menuvolume(Canvas surfacecanvas) {
    if (model.videomute)
      ecritoption = "muted " + String.format("%d%%", model.videovolume);
    else
      ecritoption = "volume " + String.format("%d%%", model.videovolume);
      y1 = mabase;
      y2 = mabase + model.GenericCaseH;
      taillex = model.Menu1TextePaint.measureText(ecritoption);
      x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
      x2 = mysurf.ScreenWidth - model.GenericInterSpace;
      if (x1 > mysurf.SettingsXmin) {
        x1 = mysurf.SettingsXmin;
      }
      ecritx = x2 - model.GenericInterSpace;
      ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
      if (clickquicknavmenu) {
        if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
          mysurf.videoshowvolume = !mysurf.videoshowvolume;
          needanotherupdate = true;
          clickquicknavmenu = false;
        }
      }
      recti = new RectF(x1, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu1BgPainta);
      recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu1BgPainto);
      if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
        RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
        surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
      }
      surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

      mabase += model.GenericCaseH + model.GenericInterSpace;
      if (mabase > mysurf.myheight)
        return;

      if (mysurf.videoshowvolume) {

        y1 = mabase;
        y2 = mabase + model.GenericCaseH;
        ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;

        ecritoption = "+";
        taillex = model.Menu1TextePaint.measureText(ecritoption);
        x1 = (mysurf.ScreenWidth - model.GenericInterSpace + mysurf.SettingsXmin) * 0.5f + model.GenericInterSpace * 0.5f;
        x2 = mysurf.ScreenWidth - model.GenericInterSpace;
        ecritx = x2;
        if (clickquicknavmenu) {
          if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
            model.videovolume += deltavolume;
            currmpvlib.setvolume(model.videovolume);
            model.preferences.edit().putInt("videovolume", model.videovolume).apply();
            needanotherupdate = true;
            clickquicknavmenu = false;
          }
        }
        recti = new RectF(x1, y1, x2, y2);
        //surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
        //recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
        surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
        if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
          RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
          surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
        }
        surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

        ecritoption = "-";
        taillex = model.Menu1TextePaint.measureText(ecritoption);
        x2 = x1 - model.GenericInterSpace;
        x1 = x2 - model.GenericInterSpace * 2.0f - taillex;
      if (x1 > mysurf.SettingsXmin)
        x1 = mysurf.SettingsXmin;
        ecritx = x2 - model.GenericInterSpace;
        if (clickquicknavmenu) {
          if (mysurf.SettingsXmin < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
            if (model.videovolume - deltavolume >= 0)
              model.videovolume -= deltavolume;
            else
              model.videovolume = 0;
            currmpvlib.setvolume(model.videovolume);
            model.preferences.edit().putInt("videovolume", model.videovolume).apply();
            needanotherupdate = true;
            clickquicknavmenu = false;
          }
        }
        recti = new RectF(x1, y1, x2, y2);
        surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
        recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
        surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
        if ((model.isandroidtv || model.keypadnavactive) && mysurf.SettingsXmin < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
          RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
          surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
        }
        surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

        mabase += model.GenericCaseH + model.GenericInterSpace;
        if (mabase > mysurf.myheight)
          return;

        /*ecritoption = "+/- " + deltavolume + "%";
        y1 = mabase;
        y2 = mabase + model.GenericCaseH;
        taillex = model.Menu1TextePaint.measureText(ecritoption);
        x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
        x2 = mysurf.ScreenWidth - model.GenericInterSpace;
        if (x1 > mysurf.SettingsXmin) {
          x1 = mysurf.SettingsXmin;
        }
        ecritx = x2 - model.GenericInterSpace;
        ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
        if (clickquicknavmenu) {
          if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
            if (deltavolume == 10)
              deltavolume = 1;
            else
              deltavolume = 10;
            needanotherupdate = true;
            clickquicknavmenu = false;
          }
        }
        recti = new RectF(x1, y1, x2, y2);
        surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
        recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
        surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
        if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
          RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
          surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
        }
        surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

        mabase += model.GenericCaseH + model.GenericInterSpace;
        if (mabase > mysurf.myheight)
          return;*/

        if (model.videomute)
          ecritoption = String.format("unmute");
        else
          ecritoption = String.format("mute");
        y1 = mabase;
        y2 = mabase + model.GenericCaseH;
        taillex = model.Menu1TextePaint.measureText(ecritoption);
        x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
        x2 = mysurf.ScreenWidth - model.GenericInterSpace;
        if (x1 > mysurf.SettingsXmin) {
          x1 = mysurf.SettingsXmin;
        }
        ecritx = x2 - model.GenericInterSpace;
        ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
        if (clickquicknavmenu) {
          if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
            model.videomute = !model.videomute;
            model.preferences.edit().putBoolean("videomute", model.videomute).apply();
            if (model.videomute) {
              currmpvlib.setmutereal(true);
              model.message("videos will start muted from now on");
            } else {
              currmpvlib.setmutereal(false);
            }
            needanotherupdate = true;
            clickquicknavmenu = false;
          }
        }
        recti = new RectF(x1, y1, x2, y2);
        surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
        recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
        surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
        if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
          RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
          surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
        }
        surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

        mabase += model.GenericCaseH + model.GenericInterSpace;
        if (mabase > mysurf.myheight)
          return;

      }
  }
  private int deltavolume = 1;

  private void menufpsasked(Canvas surfacecanvas) {
    y1 = mabase;
    y2 = mabase + model.GenericCaseH;
    ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;

    ecritoption = "+";
    taillex = model.Menu1TextePaint.measureText(ecritoption);
    x1 = (mysurf.ScreenWidth - model.GenericInterSpace + mysurf.SettingsXmin) * 0.5f + model.GenericInterSpace * 0.5f;
    x2 = mysurf.ScreenWidth - model.GenericInterSpace;
    ecritx = x2;
    if (clickquicknavmenu) {
      if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
        model.videofpsasked += 1;
        model.preferences.edit().putInt("videofpsasked", model.videofpsasked).apply();
        needanotherupdate = true;
        clickquicknavmenu = false;
      }
    }
    recti = new RectF(x1, y1, x2, y2);
    //surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
    //recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
    surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu1BgPainto);
    if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
      RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
      surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
    }
    surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

    ecritoption = "fps " + model.videofpsasked + " -";
    taillex = model.Menu1TextePaint.measureText(ecritoption);
    x2 = x1 - model.GenericInterSpace;
    x1 = x2 - model.GenericInterSpace * 2.0f - taillex;
      if (x1 > mysurf.SettingsXmin)
        x1 = mysurf.SettingsXmin;
    ecritx = x2 - model.GenericInterSpace;
    if (clickquicknavmenu) {
      if (mysurf.SettingsXmin < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
        if (model.videofpsasked > 0)
          model.videofpsasked -= 1;
        model.preferences.edit().putInt("videofpsasked", model.videofpsasked).apply();
        if (model.videofpsasked <= 0)
          model.message("do not force fps");
        needanotherupdate = true;
        clickquicknavmenu = false;
      }
    }
    recti = new RectF(x1, y1, x2, y2);
    surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu1BgPainta);
    recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
    surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu1BgPainto);
    if ((model.isandroidtv || model.keypadnavactive) && mysurf.SettingsXmin < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
      RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
      surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
    }
    surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

    mabase += model.GenericCaseH + model.GenericInterSpace;
    if (mabase > mysurf.myheight)
      return;
  }

  private void menubrightness(Canvas surfacecanvas) {
    y1 = mabase;
    y2 = mabase + model.GenericCaseH;
    ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;

    ecritoption = "+";
    taillex = model.Menu1TextePaint.measureText(ecritoption);
    x1 = (mysurf.ScreenWidth - model.GenericInterSpace + mysurf.SettingsXmin) * 0.5f + model.GenericInterSpace * 0.5f;
    x2 = mysurf.ScreenWidth - model.GenericInterSpace;
    ecritx = x2;
    if (clickquicknavmenu) {
      if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
        videobrightness += modifydelta;
        currmpvlib.setProperty("brightness", String.valueOf(videobrightness));
        needanotherupdate = true;
        clickquicknavmenu = false;
      }
    }
    recti = new RectF(x1, y1, x2, y2);
    //surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
    //recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
    surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu1BgPainto);
    if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
      RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
      surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
    }
    surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

    ecritoption = "brightness " + videobrightness + " -";
    taillex = model.Menu1TextePaint.measureText(ecritoption);
    x2 = x1 - model.GenericInterSpace;
    x1 = x2 - model.GenericInterSpace * 2.0f - taillex;
      if (x1 > mysurf.SettingsXmin)
        x1 = mysurf.SettingsXmin;
    ecritx = x2 - model.GenericInterSpace;
    if (clickquicknavmenu) {
      if (mysurf.SettingsXmin < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
        videobrightness -= modifydelta;
        currmpvlib.setProperty("brightness", String.valueOf(videobrightness));
        needanotherupdate = true;
        clickquicknavmenu = false;
      }
    }
    recti = new RectF(x1, y1, x2, y2);
    surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu1BgPainta);
    recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
    surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu1BgPainto);
    if ((model.isandroidtv || model.keypadnavactive) && mysurf.SettingsXmin < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
      RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
      surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
    }
    surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

    mabase += model.GenericCaseH + model.GenericInterSpace;
    if (mabase > mysurf.myheight)
      return;
  }
  private int videobrightness = 0;


  private void menucontrast(Canvas surfacecanvas) {
    y1 = mabase;
    y2 = mabase + model.GenericCaseH;
    ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;

    ecritoption = "+";
    taillex = model.Menu1TextePaint.measureText(ecritoption);
    x1 = (mysurf.ScreenWidth - model.GenericInterSpace + mysurf.SettingsXmin) * 0.5f + model.GenericInterSpace * 0.5f;
    x2 = mysurf.ScreenWidth - model.GenericInterSpace;
    ecritx = x2;
    if (clickquicknavmenu) {
      if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
        videocontrast += modifydelta;
        currmpvlib.setProperty("contrast", String.valueOf(videocontrast));
        needanotherupdate = true;
        clickquicknavmenu = false;
      }
    }
    recti = new RectF(x1, y1, x2, y2);
    //surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
    //recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
    surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu1BgPainto);
    if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
      RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
      surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
    }
    surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

    ecritoption = "contrast " + videocontrast + " -";
    taillex = model.Menu1TextePaint.measureText(ecritoption);
    x2 = x1 - model.GenericInterSpace;
    x1 = x2 - model.GenericInterSpace * 2.0f - taillex;
      if (x1 > mysurf.SettingsXmin)
        x1 = mysurf.SettingsXmin;
    ecritx = x2 - model.GenericInterSpace;
    if (clickquicknavmenu) {
      if (mysurf.SettingsXmin < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
        videocontrast -= modifydelta;
        currmpvlib.setProperty("contrast", String.valueOf(videocontrast));
        needanotherupdate = true;
        clickquicknavmenu = false;
      }
    }
    recti = new RectF(x1, y1, x2, y2);
    surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu1BgPainta);
    recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
    surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu1BgPainto);
    if ((model.isandroidtv || model.keypadnavactive) && mysurf.SettingsXmin < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
      RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
      surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
    }
    surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

    mabase += model.GenericCaseH + model.GenericInterSpace;
    if (mabase > mysurf.myheight)
      return;
  }
  private int videocontrast = 0;



  private void menusaturation(Canvas surfacecanvas) {
    y1 = mabase;
    y2 = mabase + model.GenericCaseH;
    ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;

    ecritoption = "+";
    taillex = model.Menu1TextePaint.measureText(ecritoption);
    x1 = (mysurf.ScreenWidth - model.GenericInterSpace + mysurf.SettingsXmin) * 0.5f + model.GenericInterSpace * 0.5f;
    x2 = mysurf.ScreenWidth - model.GenericInterSpace;
    ecritx = x2;
    if (clickquicknavmenu) {
      if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
        videosaturation += modifydelta;
        currmpvlib.setProperty("saturation", String.valueOf(videosaturation));
        needanotherupdate = true;
        clickquicknavmenu = false;
      }
    }
    recti = new RectF(x1, y1, x2, y2);
    //surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
    //recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
    surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu1BgPainto);
    if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
      RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
      surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
    }
    surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

    ecritoption = "saturation " + videosaturation + " -";
    taillex = model.Menu1TextePaint.measureText(ecritoption);
    x2 = x1 - model.GenericInterSpace;
    x1 = x2 - model.GenericInterSpace * 2.0f - taillex;
      if (x1 > mysurf.SettingsXmin)
        x1 = mysurf.SettingsXmin;
    ecritx = x2 - model.GenericInterSpace;
    if (clickquicknavmenu) {
      if (mysurf.SettingsXmin < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
        videosaturation -= modifydelta;
        currmpvlib.setProperty("saturation", String.valueOf(videosaturation));
        needanotherupdate = true;
        clickquicknavmenu = false;
      }
    }
    recti = new RectF(x1, y1, x2, y2);
    surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu1BgPainta);
    recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
    surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu1BgPainto);
    if ((model.isandroidtv || model.keypadnavactive) && mysurf.SettingsXmin < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
      RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
      surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
    }
    surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

    mabase += model.GenericCaseH + model.GenericInterSpace;
    if (mabase > mysurf.myheight)
      return;
  }
  private int videosaturation = 0;

  private void menugamma(Canvas surfacecanvas) {
    y1 = mabase;
    y2 = mabase + model.GenericCaseH;
    ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;

    ecritoption = "+";
    taillex = model.Menu1TextePaint.measureText(ecritoption);
    x1 = (mysurf.ScreenWidth - model.GenericInterSpace + mysurf.SettingsXmin) * 0.5f + model.GenericInterSpace * 0.5f;
    x2 = mysurf.ScreenWidth - model.GenericInterSpace;
    ecritx = x2;
    if (clickquicknavmenu) {
      if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
        videogamma += modifydelta;
        currmpvlib.setProperty("gamma", String.valueOf(videogamma));
        needanotherupdate = true;
        clickquicknavmenu = false;
      }
    }
    recti = new RectF(x1, y1, x2, y2);
    //surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
    //recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
    surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu1BgPainto);
    if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
      RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
      surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
    }
    surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

    ecritoption = "gamma " + videogamma + " -";
    taillex = model.Menu1TextePaint.measureText(ecritoption);
    x2 = x1 - model.GenericInterSpace;
    x1 = x2 - model.GenericInterSpace * 2.0f - taillex;
      if (x1 > mysurf.SettingsXmin)
        x1 = mysurf.SettingsXmin;
    ecritx = x2 - model.GenericInterSpace;
    if (clickquicknavmenu) {
      if (mysurf.SettingsXmin < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
        videogamma -= modifydelta;
        currmpvlib.setProperty("gamma", String.valueOf(videogamma));
        needanotherupdate = true;
        clickquicknavmenu = false;
      }
    }
    recti = new RectF(x1, y1, x2, y2);
    surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu1BgPainta);
    recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
    surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu1BgPainto);
    if ((model.isandroidtv || model.keypadnavactive) && mysurf.SettingsXmin < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
      RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
      surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
    }
    surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

    mabase += model.GenericCaseH + model.GenericInterSpace;
    if (mabase > mysurf.myheight)
      return;
  }
  private int videogamma = 0;

  private void menuhue(Canvas surfacecanvas) {
    y1 = mabase;
    y2 = mabase + model.GenericCaseH;
    ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;

    ecritoption = "+";
    taillex = model.Menu1TextePaint.measureText(ecritoption);
    x1 = (mysurf.ScreenWidth - model.GenericInterSpace + mysurf.SettingsXmin) * 0.5f + model.GenericInterSpace * 0.5f;
    x2 = mysurf.ScreenWidth - model.GenericInterSpace;
    ecritx = x2;
    if (clickquicknavmenu) {
      if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
        videohue += modifydelta;
        currmpvlib.setProperty("hue", String.valueOf(videohue));
        needanotherupdate = true;
        clickquicknavmenu = false;
      }
    }
    recti = new RectF(x1, y1, x2, y2);
    //surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
    //recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
    surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu1BgPainto);
    if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
      RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
      surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
    }
    surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

    ecritoption = "hue " + videohue + " -";
    taillex = model.Menu1TextePaint.measureText(ecritoption);
    x2 = x1 - model.GenericInterSpace;
    x1 = x2 - model.GenericInterSpace * 2.0f - taillex;
      if (x1 > mysurf.SettingsXmin)
        x1 = mysurf.SettingsXmin;
    ecritx = x2 - model.GenericInterSpace;
    if (clickquicknavmenu) {
      if (mysurf.SettingsXmin < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
        videohue -= modifydelta;
        currmpvlib.setProperty("hue", String.valueOf(videohue));
        needanotherupdate = true;
        clickquicknavmenu = false;
      }
    }
    recti = new RectF(x1, y1, x2, y2);
    surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu1BgPainta);
    recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
    surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu1BgPainto);
    if ((model.isandroidtv || model.keypadnavactive) && mysurf.SettingsXmin < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
      RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
      surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
    }
    surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

    mabase += model.GenericCaseH + model.GenericInterSpace;
    if (mabase > mysurf.myheight)
      return;
  }
  private int videohue = 0;

  private void menumodifydelta(Canvas surfacecanvas) {
    y1 = mabase;
    y2 = mabase + model.GenericCaseH;
    ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;

    ecritoption = "+";
    taillex = model.Menu1TextePaint.measureText(ecritoption);
    x1 = (mysurf.ScreenWidth - model.GenericInterSpace + mysurf.SettingsXmin) * 0.5f + model.GenericInterSpace * 0.5f;
    x2 = mysurf.ScreenWidth - model.GenericInterSpace;
    ecritx = x2;
    if (clickquicknavmenu) {
      if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
        modifydelta += 1;
        needanotherupdate = true;
        clickquicknavmenu = false;
      }
    }
    recti = new RectF(x1, y1, x2, y2);
    //surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu1BgPainta);
    //recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
    surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu1BgPainto);
    if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
      RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
      surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
    }
    surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

    ecritoption = "delta " + modifydelta + "   -";
    taillex = model.Menu1TextePaint.measureText(ecritoption);
    x2 = x1 - model.GenericInterSpace;
    x1 = x2 - model.GenericInterSpace * 2.0f - taillex;
      if (x1 > mysurf.SettingsXmin)
        x1 = mysurf.SettingsXmin;
    ecritx = x2 - model.GenericInterSpace;
    if (clickquicknavmenu) {
      if (mysurf.SettingsXmin < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
        modifydelta -= 1;
        needanotherupdate = true;
        clickquicknavmenu = false;
      }
    }
    recti = new RectF(x1, y1, x2, y2);
    surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu1BgPainta);
    recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
    surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu1BgPainto);
    if ((model.isandroidtv || model.keypadnavactive) && mysurf.SettingsXmin < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
      RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
      surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
    }
    surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

    mabase += model.GenericCaseH + model.GenericInterSpace;
    if (mabase > mysurf.myheight)
      return;

    if (videobrightness != 0 || videocontrast != 0 || videosaturation != 0 || videogamma != 0 || videohue != 0) {
      ecritoption = "original";
      y1 = mabase;
      y2 = mabase + model.GenericCaseH;
      taillex = model.Menu1TextePaint.measureText(ecritoption);
      x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
      x2 = mysurf.ScreenWidth - model.GenericInterSpace;
      if (x1 > mysurf.SettingsXmin) {
        x1 = mysurf.SettingsXmin;
      }
      ecritx = x2 - model.GenericInterSpace;
      ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
      if (clickquicknavmenu) {
        if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
          if (videobrightness != 0)
            currmpvlib.setProperty("brightness", String.valueOf(0));
          if (videocontrast != 0)
            currmpvlib.setProperty("contrast", String.valueOf(0));
          if (videosaturation != 0)
            currmpvlib.setProperty("saturation", String.valueOf(0));
          if (videogamma != 0)
            currmpvlib.setProperty("gamma", String.valueOf(0));
          if (videohue != 0)
            currmpvlib.setProperty("hue", String.valueOf(0));
          videobrightness = 0;
          videocontrast = 0;
          videosaturation = 0;
          videogamma = 0;
          videohue = 0;
          needanotherupdate = true;
          clickquicknavmenu = false;
        }
      }
      recti = new RectF(x1, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu1BgPainta);
      recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu1BgPainto);
      if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
        RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
        surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
      }
      surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

      mabase += model.GenericCaseH + model.GenericInterSpace;
      if (mabase > mysurf.myheight)
        return;
    }
  }
  private int modifydelta = 3;

  private void menuvratio(Canvas surfacecanvas) {
      ecritoption = String.format("format %.2f", currmpvlib.videoaspect);
      if (Math.abs(currmpvlib.videoaspect - 16.0f/9.0f) < 0.01f)
        ecritoption = "16:9";
      else if (Math.abs(currmpvlib.videoaspect - 4.0f/3.0f) < 0.01f)
        ecritoption = "4:3";
      else if (Math.abs(currmpvlib.videoaspect - 16.0f/10.0f) < 0.01f)
        ecritoption = "16:10";
      if (currmpvlib.videoaspecttype == MPVLib.videoaspectscreen)
        ecritoption += " screen";
      else if (currmpvlib.videoaspecttype == MPVLib.videoaspectvideo)
        ecritoption += " video";
      else if (currmpvlib.videoaspecttype == MPVLib.videoaspectboth)
        ecritoption += " vid&scr";
      y1 = mabase;
      y2 = mabase + model.GenericCaseH;
      taillex = model.Menu1TextePaint.measureText(ecritoption);
      x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
      x2 = mysurf.ScreenWidth - model.GenericInterSpace;
      if (x1 > mysurf.SettingsXmin) {
        x1 = mysurf.SettingsXmin;
      }
      ecritx = x2 - model.GenericInterSpace;
      ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
      if (clickquicknavmenu) {
        if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
          mysurf.videoshowaspect = !mysurf.videoshowaspect;
          needanotherupdate = true;
          clickquicknavmenu = false;
        }
      }
      recti = new RectF(x1, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu1BgPainta);
      recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu1BgPainto);
      if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
        RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
        surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
      }
      surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

      mabase += model.GenericCaseH + model.GenericInterSpace;
      if (mabase > mysurf.myheight)
        return;

      if (mysurf.videoshowaspect) {

        float screenaspect = 0.0f;
        if (currmpvlib.frameheight > 0) {
          screenaspect = (float) currmpvlib.framewidth / (float) currmpvlib.frameheight;
        }
        ecritoption = String.format("screen %.2f", screenaspect);
        y1 = mabase;
        y2 = mabase + model.GenericCaseH;
        taillex = model.Menu1TextePaint.measureText(ecritoption);
        x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
        x2 = mysurf.ScreenWidth - model.GenericInterSpace;
        if (x1 > mysurf.SettingsXmin) {
          x1 = mysurf.SettingsXmin;
        }
        ecritx = x2 - model.GenericInterSpace;
        ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
        if (clickquicknavmenu) {
          if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
            currmpvlib.videoformatautonomanualoverride = false;
            currmpvlib.setvideoaspectscreen();
            if (model.videoaaspectresize && currmpvlib.hwdec) {
              try {
                model.commandethreadvideo.put(new String[]{String.valueOf(currid), "videoaaspectresize"});
              } catch (InterruptedException e) {
                e.printStackTrace();
              }
            }
            needanotherupdate = true;
            clickquicknavmenu = false;
          }
        }
        recti = new RectF(x1, y1, x2, y2);
        surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
        recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
        surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
        if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
          RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
          surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
        }
        surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

        mabase += model.GenericCaseH + model.GenericInterSpace;
        if (mabase > mysurf.myheight)
          return;

        float videoaspect = 0.0f;
        if (currmpvlib.frameheight > 0) {
          videoaspect = (float) currmpvlib.videowidth / (float) currmpvlib.videoheight;
        }
        ecritoption = String.format("video %.2f", videoaspect);
        y1 = mabase;
        y2 = mabase + model.GenericCaseH;
        taillex = model.Menu1TextePaint.measureText(ecritoption);
        x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
        x2 = mysurf.ScreenWidth - model.GenericInterSpace;
        if (x1 > mysurf.SettingsXmin) {
          x1 = mysurf.SettingsXmin;
        }
        ecritx = x2 - model.GenericInterSpace;
        ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
        if (clickquicknavmenu) {
          if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
            currmpvlib.videoformatautonomanualoverride = false;
            currmpvlib.setvideoaspectvideo();
            if (model.videoaaspectresize && currmpvlib.hwdec) {
              try {
                model.commandethreadvideo.put(new String[]{String.valueOf(currid), "videoaaspectresize"});
              } catch (InterruptedException e) {
                e.printStackTrace();
              }
            }
            needanotherupdate = true;
            clickquicknavmenu = false;
          }
        }
        recti = new RectF(x1, y1, x2, y2);
        surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
        recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
        surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
        if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
          RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
          surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
        }
        surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

        mabase += model.GenericCaseH + model.GenericInterSpace;
        if (mabase > mysurf.myheight)
          return;

        ecritoption = String.format("4:3 1.33");
        y1 = mabase;
        y2 = mabase + model.GenericCaseH;
        taillex = model.Menu1TextePaint.measureText(ecritoption);
        x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
        x2 = mysurf.ScreenWidth - model.GenericInterSpace;
        if (x1 > mysurf.SettingsXmin) {
          x1 = mysurf.SettingsXmin;
        }
        ecritx = x2 - model.GenericInterSpace;
        ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
        if (clickquicknavmenu) {
          if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
            currmpvlib.videoformatautonomanualoverride = false;
            currmpvlib.setvideoaspect(4.0f/3.0f);
            if (model.videoaaspectresize && currmpvlib.hwdec) {
              try {
                model.commandethreadvideo.put(new String[]{String.valueOf(currid), "videoaaspectresize"});
              } catch (InterruptedException e) {
                e.printStackTrace();
              }
            }
            needanotherupdate = true;
            clickquicknavmenu = false;
          }
        }
        recti = new RectF(x1, y1, x2, y2);
        surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
        recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
        surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
        if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
          RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
          surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
        }
        surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

        mabase += model.GenericCaseH + model.GenericInterSpace;
        if (mabase > mysurf.myheight)
          return;

        ecritoption = String.format("16:9 1.78");
        y1 = mabase;
        y2 = mabase + model.GenericCaseH;
        taillex = model.Menu1TextePaint.measureText(ecritoption);
        x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
        x2 = mysurf.ScreenWidth - model.GenericInterSpace;
        if (x1 > mysurf.SettingsXmin) {
          x1 = mysurf.SettingsXmin;
        }
        ecritx = x2 - model.GenericInterSpace;
        ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
        if (clickquicknavmenu) {
          if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
            currmpvlib.videoformatautonomanualoverride = false;
            currmpvlib.setvideoaspect(16.0f/9.0f);
            if (model.videoaaspectresize && currmpvlib.hwdec) {
              try {
                model.commandethreadvideo.put(new String[]{String.valueOf(currid), "videoaaspectresize"});
              } catch (InterruptedException e) {
                e.printStackTrace();
              }
            }
            needanotherupdate = true;
            clickquicknavmenu = false;
          }
        }
        recti = new RectF(x1, y1, x2, y2);
        surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
        recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
        surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
        if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
          RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
          surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
        }
        surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

        mabase += model.GenericCaseH + model.GenericInterSpace;
        if (mabase > mysurf.myheight)
          return;

        ecritoption = String.format("1.85");
        y1 = mabase;
        y2 = mabase + model.GenericCaseH;
        taillex = model.Menu1TextePaint.measureText(ecritoption);
        x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
        x2 = mysurf.ScreenWidth - model.GenericInterSpace;
        if (x1 > mysurf.SettingsXmin) {
          x1 = mysurf.SettingsXmin;
        }
        ecritx = x2 - model.GenericInterSpace;
        ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
        if (clickquicknavmenu) {
          if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
            currmpvlib.videoformatautonomanualoverride = false;
            currmpvlib.setvideoaspect(1.85f);
            if (model.videoaaspectresize && currmpvlib.hwdec) {
              try {
                model.commandethreadvideo.put(new String[]{String.valueOf(currid), "videoaaspectresize"});
              } catch (InterruptedException e) {
                e.printStackTrace();
              }
            }
            needanotherupdate = true;
            clickquicknavmenu = false;
          }
        }
        recti = new RectF(x1, y1, x2, y2);
        surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
        recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
        surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
        if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
          RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
          surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
        }
        surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

        mabase += model.GenericCaseH + model.GenericInterSpace;
        if (mabase > mysurf.myheight)
          return;

        ecritoption = String.format("2.00");
        y1 = mabase;
        y2 = mabase + model.GenericCaseH;
        taillex = model.Menu1TextePaint.measureText(ecritoption);
        x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
        x2 = mysurf.ScreenWidth - model.GenericInterSpace;
        if (x1 > mysurf.SettingsXmin) {
          x1 = mysurf.SettingsXmin;
        }
        ecritx = x2 - model.GenericInterSpace;
        ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
        if (clickquicknavmenu) {
          if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
            currmpvlib.videoformatautonomanualoverride = false;
            currmpvlib.setvideoaspect(2.00f);
            if (model.videoaaspectresize && currmpvlib.hwdec) {
              try {
                model.commandethreadvideo.put(new String[]{String.valueOf(currid), "videoaaspectresize"});
              } catch (InterruptedException e) {
                e.printStackTrace();
              }
            }
            needanotherupdate = true;
            clickquicknavmenu = false;
          }
        }
        recti = new RectF(x1, y1, x2, y2);
        surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
        recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
        surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
        if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
          RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
          surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
        }
        surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

        mabase += model.GenericCaseH + model.GenericInterSpace;
        if (mabase > mysurf.myheight)
          return;

        ecritoption = String.format("panoramic 2.40");
        y1 = mabase;
        y2 = mabase + model.GenericCaseH;
        taillex = model.Menu1TextePaint.measureText(ecritoption);
        x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
        x2 = mysurf.ScreenWidth - model.GenericInterSpace;
        if (x1 > mysurf.SettingsXmin) {
          x1 = mysurf.SettingsXmin;
        }
        ecritx = x2 - model.GenericInterSpace;
        ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
        if (clickquicknavmenu) {
          if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
            currmpvlib.videoformatautonomanualoverride = false;
            currmpvlib.setvideoaspect(2.40f);
            if (model.videoaaspectresize && currmpvlib.hwdec) {
              try {
                model.commandethreadvideo.put(new String[]{String.valueOf(currid), "videoaaspectresize"});
              } catch (InterruptedException e) {
                e.printStackTrace();
              }
            }
            needanotherupdate = true;
            clickquicknavmenu = false;
          }
        }
        recti = new RectF(x1, y1, x2, y2);
        surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
        recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
        surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
        if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
          RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
          surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
        }
        surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

        mabase += model.GenericCaseH + model.GenericInterSpace;
      }
  }

  private void menuhwdec(Canvas surfacecanvas) {
      if (currmpvlib.hwdec)
        ecritoption = "hw dec";
      else
        ecritoption = "sw dec";
      y1 = mabase;
      y2 = mabase + model.GenericCaseH;
      taillex = model.Menu1TextePaint.measureText(ecritoption);
      x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
      x2 = mysurf.ScreenWidth - model.GenericInterSpace;
      if (x1 > mysurf.SettingsXmin) {
        x1 = mysurf.SettingsXmin;
      }
      ecritx = x2 - model.GenericInterSpace;
      ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
      if (clickquicknavmenu) {
        if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
          if (currmpvlib.hwdec) {
            currmpvlib.sethwdec(false);
            model.message("switched to software decoding");
          } else {
            currmpvlib.sethwdec(true);
            model.message("switched to hardware decoding");
          }
          needanotherupdate = true;
          clickquicknavmenu = false;
        }
      }
      recti = new RectF(x1, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu1BgPainta);
      recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu1BgPainto);
      if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
        RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
        surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
      }
      surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

      mabase += model.GenericCaseH + model.GenericInterSpace;
  }

  private void menusplit(Canvas surfacecanvas) {
    ecritoption = "split screen"; // keep same zoom between images for comic reading
    y1 = mabase;
    y2 = mabase + model.GenericCaseH;
    taillex = model.Menu1TextePaint.measureText(ecritoption);
    x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
    x2 = mysurf.ScreenWidth - model.GenericInterSpace;
    if (x1 > mysurf.SettingsXmin) {
      x1 = mysurf.SettingsXmin;
    }
    ecritx = x2 - model.GenericInterSpace;
    ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
    if (clickquicknavmenu) {
      if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
        mysurf.optionshowsplit = !mysurf.optionshowsplit;
        needanotherupdate = true;
        clickquicknavmenu = false;
      }
    }
    recti = new RectF(x1, y1, x2, y2);
    surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu1BgPainta);
    recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
    surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu1BgPainto);
    if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
      RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
      surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
    }
    surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

    mabase += model.GenericCaseH + model.GenericInterSpace;
    if (mabase > mysurf.myheight)
      return;

    if (mysurf.optionshowsplit) {

      ecritoption = "remove this split";
      y1 = mabase;
      y2 = mabase + model.GenericCaseH;
      taillex = model.Menu1TextePaint.measureText(ecritoption);
      x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
      x2 = mysurf.ScreenWidth - model.GenericInterSpace;
      if (x1 > mysurf.SettingsXmin) {
        x1 = mysurf.SettingsXmin;
      }
      ecritx = x2 - model.GenericInterSpace;
      ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
      if (clickquicknavmenu) {
        if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
          Intent intent = new Intent();
          intent.setAction(Gallery.broadcastname);
          intent.putExtra("goal", "splitscreen");
          intent.putExtra("splitnumber", 1);
          intent.putExtra("splitsurf", mysurf.myid);
          LocalBroadcastManager.getInstance(model.activitycontext).sendBroadcast(intent);
          needanotherupdate = true;
          clickquicknavmenu = false;
        }
      }
      recti = new RectF(x1, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
      recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
      if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
        RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
        surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
      }
      surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

      mabase += model.GenericCaseH + model.GenericInterSpace;
      if (mabase > mysurf.myheight)
        return;

      ecritoption = "split vertical";
      y1 = mabase;
      y2 = mabase + model.GenericCaseH;
      taillex = model.Menu1TextePaint.measureText(ecritoption);
      x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
      x2 = mysurf.ScreenWidth - model.GenericInterSpace;
      if (x1 > mysurf.SettingsXmin) {
        x1 = mysurf.SettingsXmin;
      }
      ecritx = x2 - model.GenericInterSpace;
      ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
      if (clickquicknavmenu) {
        if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
          Intent intent = new Intent();
          intent.setAction(Gallery.broadcastname);
          intent.putExtra("goal", "splitscreen");
          intent.putExtra("splitnumber", 2);
          intent.putExtra("splitsurf", mysurf.myid);
          intent.putExtra("splitratio", 0.50f);
          intent.putExtra("splitvertical", true);
          LocalBroadcastManager.getInstance(model.activitycontext).sendBroadcast(intent);
          //model.message("You can drag the location of the split.");
          needanotherupdate = true;
          clickquicknavmenu = false;
        }
      }
      recti = new RectF(x1, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
      recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
      if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
        RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
        surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
      }
      surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

      mabase += model.GenericCaseH + model.GenericInterSpace;
      if (mabase > mysurf.myheight)
        return;

      ecritoption = "split horizontal";
      y1 = mabase;
      y2 = mabase + model.GenericCaseH;
      taillex = model.Menu1TextePaint.measureText(ecritoption);
      x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
      x2 = mysurf.ScreenWidth - model.GenericInterSpace;
      if (x1 > mysurf.SettingsXmin) {
        x1 = mysurf.SettingsXmin;
      }
      ecritx = x2 - model.GenericInterSpace;
      ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
      if (clickquicknavmenu) {
        if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
          Intent intent = new Intent();
          intent.setAction(Gallery.broadcastname);
          intent.putExtra("goal", "splitscreen");
          intent.putExtra("splitnumber", 2);
          intent.putExtra("splitsurf", mysurf.myid);
          intent.putExtra("splitratio", 0.50f);
          intent.putExtra("splitvertical", false);
          LocalBroadcastManager.getInstance(model.activitycontext).sendBroadcast(intent);
          //model.message("You can drag the location of the split.");
          needanotherupdate = true;
          clickquicknavmenu = false;
        }
      }
      recti = new RectF(x1, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
      recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
      if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
        RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
        surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
      }
      surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

      mabase += model.GenericCaseH + model.GenericInterSpace;
      if (mabase > mysurf.myheight)
        return;

      // TODO : last split

    }

  }

  private void menulog(Canvas surfacecanvas) {
      ecritoption = "log " + currmpvlib.log.size();
      y1 = mabase;
      y2 = mabase + model.GenericCaseH;
      taillex = model.Menu1TextePaint.measureText(ecritoption);
      x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
      x2 = mysurf.ScreenWidth - model.GenericInterSpace;
      if (x1 > mysurf.SettingsXmin) {
        x1 = mysurf.SettingsXmin;
      }
      ecritx = x2 - model.GenericInterSpace;
      ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
      if (clickquicknavmenu) {
        if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
          mysurf.videoShowLog = !mysurf.videoShowLog;
          needanotherupdate = true;
          clickquicknavmenu = false;
        }
      }
      recti = new RectF(x1, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu1BgPainta);
      recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu1BgPainto);
      if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
        RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
        surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
      }
      surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

      mabase += model.GenericCaseH + model.GenericInterSpace;
      if (mabase > mysurf.myheight)
        return;
  }

  private void menuswcopy(Canvas surfacecanvas) {
      switch(model.videohardwaremode) {
        case Gallery.videohwaamodesw:
          ecritoption = "sw decode";
          break;
        case Gallery.videohwaamodehw:
          ecritoption = "hw copy to ram";
          break;
        case Gallery.videohwaamodehwp:
          ecritoption = "hw+";
          break;
        case Gallery.videohwaamodehwpp:
          ecritoption = "hw direct rendering";
          break;
        case Gallery.videohwaamodensw:
          ecritoption = "gpu-next sw decode";
          break;
        case Gallery.videohwaamodenhw:
          ecritoption = "gpu-next hw copy to ram";
          break;
        case Gallery.videohwaamodenhwp:
          ecritoption = "gpu-next hw+";
          break;
      }
      y1 = mabase;
      y2 = mabase + model.GenericCaseH;
      taillex = model.Menu1TextePaint.measureText(ecritoption);
      x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
      x2 = mysurf.ScreenWidth - model.GenericInterSpace;
      if (x1 > mysurf.SettingsXmin) {
        x1 = mysurf.SettingsXmin;
      }
      ecritx = x2 - model.GenericInterSpace;
      ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
      if (clickquicknavmenu) {
        if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
          model.videohardwaremode = (model.videohardwaremode + 1) % Gallery.videohwaamodecount;
          switch(model.videohardwaremode){
            case Gallery.videohwaamodensw:
            case Gallery.videohwaamodesw:
              model.message("software decoding\n" +
                  "always works");
              model.videoaaspectresize = false;
              break;
            case Gallery.videohwaamodenhw:
            case Gallery.videohwaamodehw:
              model.message("hardware copy to ram\n" +
                  "should perform better\n" +
                  "(mediacodec-copy)");
              model.videoaaspectresize = false;
              break;
            case Gallery.videohwaamodehwp:
            case Gallery.videohwaamodenhwp:
              model.message("hardware\n" +
                  "should perform even better\n" +
                  "(mediacodec)");
              model.videoaaspectresize = false;
              break;
            case Gallery.videohwaamodehwpp:
              model.message("hardware direct rendering\n" +
                  "default\n" +
                  "should play smoothly on all devices\n" +
                  "subtitles are rendered on an overlay\n" +
                  "(mediacodec-embed)\n" +
                  "Used with tweaks -> resize to video.");
              model.videoaaspectresize = true;
              break;
            default:
              model.message();
              model.videoaaspectresize = false;
              break;
          }
          model.preferences.edit().putInt("videohardwaremode", model.videohardwaremode).apply();
          model.preferences.edit().putBoolean("videoaaspectresize", model.videoaaspectresize).commit();
          needanotherupdate = true;
          clickquicknavmenu = false;
        }
      }
      recti = new RectF(x1, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu1BgPainta);
      recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu1BgPainto);
      if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
        RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
        surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
      }
      surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

      mabase += model.GenericCaseH + model.GenericInterSpace;
  }

  private void menufps(Canvas surfacecanvas) {
      if (!model.videofpsor)
        ecritoption = "no display-fps-override";
      else
        ecritoption = String.format("display-fps-override %.1f", (float) model.refreshrate);
      y1 = mabase;
      y2 = mabase + model.GenericCaseH;
      taillex = model.Menu1TextePaint.measureText(ecritoption);
      x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
      x2 = mysurf.ScreenWidth - model.GenericInterSpace;
      if (x1 > mysurf.SettingsXmin) {
        x1 = mysurf.SettingsXmin;
      }
      ecritx = x2 - model.GenericInterSpace;
      ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
      if (clickquicknavmenu) {
        if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
          model.videofpsor = !model.videofpsor;
          model.preferences.edit().putBoolean("videofpsor", model.videofpsor).apply();
          needanotherupdate = true;
          clickquicknavmenu = false;
        }
      }
      recti = new RectF(x1, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu1BgPainta);
      recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu1BgPainto);
      if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
        RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
        surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
      }
      surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

      mabase += model.GenericCaseH + model.GenericInterSpace;
  }

  private void menuloop(Canvas surfacecanvas) {
      if (model.videoloop == Gallery.videoloopLoop)
        ecritoption = "loop";
      else if (model.videoloop == Gallery.videoloopNoLoop)
        ecritoption = "no loop";
      else if (model.videoloop == Gallery.videoloopSequence)
        ecritoption = "sequence";
      y1 = mabase;
      y2 = mabase + model.GenericCaseH;
      taillex = model.Menu1TextePaint.measureText(ecritoption);
      x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
      x2 = mysurf.ScreenWidth - model.GenericInterSpace;
      if (x1 > mysurf.SettingsXmin) {
        x1 = mysurf.SettingsXmin;
      }
      ecritx = x2 - model.GenericInterSpace;
      ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
      if (clickquicknavmenu) {
        if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
          model.videoloop = (model.videoloop + 1) % Gallery.videoloopl;
          model.preferences.edit().putInt("videoloop", model.videoloop).apply();
          if (model.videoloop == Gallery.videoloopLoop)
            currmpvlib.setloop(true);
          else if (model.videoloop == Gallery.videoloopNoLoop)
            currmpvlib.setloop(false);
          else if (model.videoloop == Gallery.videoloopSequence) {
            currmpvlib.setloop(false);
            llog.d(TAG, "enqueue next");
          }
          needanotherupdate = true;
          clickquicknavmenu = false;
        }
      }
      recti = new RectF(x1, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu1BgPainta);
      recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu1BgPainto);
      if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
        RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
        surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
      }
      surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

      mabase += model.GenericCaseH + model.GenericInterSpace;
  }

  private void menuvideoresize(Canvas surfacecanvas) {
    if (model.videoaaspectresize)
      ecritoption = "resize to video";
    else
      ecritoption = "no resize";
    y1 = mabase;
    y2 = mabase + model.GenericCaseH;
    taillex = model.Menu1TextePaint.measureText(ecritoption);
    x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
    x2 = mysurf.ScreenWidth - model.GenericInterSpace;
    if (x1 > mysurf.SettingsXmin) {
      x1 = mysurf.SettingsXmin;
    }
    ecritx = x2 - model.GenericInterSpace;
    ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
    if (clickquicknavmenu) {
      if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
        model.videoaaspectresize = !model.videoaaspectresize;
        model.preferences.edit().putBoolean("videoaaspectresize", model.videoaaspectresize).apply();
        if (model.videoaaspectresize)
          model.message("resize video frame to the video size\n" +
                  "when it launches.\n" +
                  "Useful for hardware direct rendering."
          );
        else
          model.message("use all frame for the video." +
                  "\nDefault preset.");
        needanotherupdate = true;
        clickquicknavmenu = false;
      }
    }
    recti = new RectF(x1, y1, x2, y2);
    surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu1BgPainta);
    recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
    surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu1BgPainto);
    if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
      RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
      surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
    }
    surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

    mabase += model.GenericCaseH + model.GenericInterSpace;
  }

  private void menuvsync(Canvas surfacecanvas) {
      if (model.videosyncaudio == 0)
        ecritoption = "video-sync default";
      else if (model.videosyncaudio == 1)
        ecritoption = "video-sync audio";
      else if (model.videosyncaudio == 2)
        ecritoption = "video-sync display-resample";
      else if (model.videosyncaudio == 3)
        ecritoption = "video-sync display-resample-vdrop";
      else if (model.videosyncaudio == 4)
        ecritoption = "video-sync display-vdrop";
      else if (model.videosyncaudio == 5)
        ecritoption = "video-sync display-adrop";
      y1 = mabase;
      y2 = mabase + model.GenericCaseH;
      taillex = model.Menu1TextePaint.measureText(ecritoption);
      x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
      x2 = mysurf.ScreenWidth - model.GenericInterSpace;
      if (x1 > mysurf.SettingsXmin) {
        x1 = mysurf.SettingsXmin;
      }
      ecritx = x2 - model.GenericInterSpace;
      ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
      if (clickquicknavmenu) {
        if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
          model.videosyncaudio = (model.videosyncaudio + 1) % 6;
          model.preferences.edit().putInt("videosyncaudio", model.videosyncaudio).apply();
          if (model.videosyncaudio == 1)
            model.message("Time video frames to audio" +
                    "\nmost robust mode" +
                    "\noccasional frame drops or repeats" +
                    "\nThis is the default mode.");
          else if (model.videosyncaudio == 2)
            model.message("Resample audio to match the video.");
          else if (model.videosyncaudio == 3)
            model.message("Resample audio to match the video." +
                    "\nDrop video frames to compensate for drift.");
          else if (model.videosyncaudio == 4)
            model.message("Drop or repeat video frames" +
                    "\nto compensate desyncing video.");
          else if (model.videosyncaudio == 5)
            model.message("Drop or repeat audio data" +
                    "\nto compensate desyncing video.");
          needanotherupdate = true;
          clickquicknavmenu = false;
        }
      }
      recti = new RectF(x1, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu1BgPainta);
      recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu1BgPainto);
      if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
        RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
        surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
      }
      surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

      mabase += model.GenericCaseH + model.GenericInterSpace;
  }

  private void menuaudiotrack(Canvas surfacecanvas) {
    if (model.videoaudiotrack)
      ecritoption = "audiotrack";
    else
      ecritoption = "opensles";
    y1 = mabase;
    y2 = mabase + model.GenericCaseH;
    taillex = model.Menu1TextePaint.measureText(ecritoption);
    x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
    x2 = mysurf.ScreenWidth - model.GenericInterSpace;
    if (x1 > mysurf.SettingsXmin) {
      x1 = mysurf.SettingsXmin;
    }
    ecritx = x2 - model.GenericInterSpace;
    ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
    if (clickquicknavmenu) {
      if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
        model.videoaudiotrack = !model.videoaudiotrack;
        model.preferences.edit().putBoolean("videoaudiotrack", model.videoaudiotrack).apply();
        if (model.videoaudiotrack)
          model.message("enable audiotrack driver" +
                  "\ndefault but tends to crash" +
                  "\nwhen using multiple players");
        else
          model.message("safe opensles driver" +
                  "\nnever crashes");
        needanotherupdate = true;
        clickquicknavmenu = false;
      }
    }
    recti = new RectF(x1, y1, x2, y2);
    surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu1BgPainta);
    recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
    surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu1BgPainto);
    if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
      RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
      surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
    }
    surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

    mabase += model.GenericCaseH + model.GenericInterSpace;
  }

  private void menuqueue(Canvas surfacecanvas) {
    if (model.videoqueue)
      ecritoption = "vd-queue-enable yes";
    else
      ecritoption = "vd-queue-enable default";
    y1 = mabase;
    y2 = mabase + model.GenericCaseH;
    taillex = model.Menu1TextePaint.measureText(ecritoption);
    x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
    x2 = mysurf.ScreenWidth - model.GenericInterSpace;
    if (x1 > mysurf.SettingsXmin) {
      x1 = mysurf.SettingsXmin;
    }
    ecritx = x2 - model.GenericInterSpace;
    ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
    if (clickquicknavmenu) {
      if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
        model.videoqueue = !model.videoqueue;
        model.preferences.edit().putBoolean("videoqueue", model.videoqueue).apply();
        if (model.videoqueue)
          model.message("try to improve performance");
        else
          model.message("default preset");
        needanotherupdate = true;
        clickquicknavmenu = false;
      }
    }
    recti = new RectF(x1, y1, x2, y2);
    surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu1BgPainta);
    recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
    surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu1BgPainto);
    if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
      RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
      surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
    }
    surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

    mabase += model.GenericCaseH + model.GenericInterSpace;
  }

  private void menuvideoscale(Canvas surfacecanvas) {
    if (model.videoscale)
      ecritoption = "scale bilinear";
    else
      ecritoption = "scale default";
    y1 = mabase;
    y2 = mabase + model.GenericCaseH;
    taillex = model.Menu1TextePaint.measureText(ecritoption);
    x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
    x2 = mysurf.ScreenWidth - model.GenericInterSpace;
    if (x1 > mysurf.SettingsXmin) {
      x1 = mysurf.SettingsXmin;
    }
    ecritx = x2 - model.GenericInterSpace;
    ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
    if (clickquicknavmenu) {
      if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
        model.videoscale = !model.videoscale;
        model.preferences.edit().putBoolean("videoscale", model.videoscale).apply();
        if (model.videoscale)
          model.message("fast low quality presets" +
                  "\nscale=bilinear" +
                  "\ndeband=no");
        else
          model.message("default presets");
        needanotherupdate = true;
        clickquicknavmenu = false;
      }
    }
    recti = new RectF(x1, y1, x2, y2);
    surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu1BgPainta);
    recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
    surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu1BgPainto);
    if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
      RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
      surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
    }
    surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

    mabase += model.GenericCaseH + model.GenericInterSpace;
  }

  private void menuvideoperformance(Canvas surfacecanvas) {
    if (model.videoperformance)
      ecritoption = "vd-lavc-dr no";
    else
      ecritoption = "vd-lavc-dr default";
    y1 = mabase;
    y2 = mabase + model.GenericCaseH;
    taillex = model.Menu1TextePaint.measureText(ecritoption);
    x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
    x2 = mysurf.ScreenWidth - model.GenericInterSpace;
    if (x1 > mysurf.SettingsXmin) {
      x1 = mysurf.SettingsXmin;
    }
    ecritx = x2 - model.GenericInterSpace;
    ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
    if (clickquicknavmenu) {
      if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
        model.videoperformance = !model.videoperformance;
        model.preferences.edit().putBoolean("vdlavcdr", model.videoperformance).apply();
        if (model.videoperformance)
          model.message("DR is known to ruin performance" +
                  "\nat least on Exynos devices" +
                  "\nDR disabled" +
                  "\nimproves performance");
        else
          model.message("default preset : auto" +
                  "\nenable DR only if it's known" +
                  "\nto help performance");
        needanotherupdate = true;
        clickquicknavmenu = false;
      }
    }
    recti = new RectF(x1, y1, x2, y2);
    surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu1BgPainta);
    recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
    surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu1BgPainto);
    if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
      RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
      surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
    }
    surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

    mabase += model.GenericCaseH + model.GenericInterSpace;
  }

  private void menuvideoprofile(Canvas surfacecanvas) {
    if (model.videoprofile == 0)
      ecritoption = "profile fast";
    else if (model.videoprofile == 1)
      ecritoption = "profile medium";
    else if (model.videoprofile == 2)
      ecritoption = "profile quality";
    y1 = mabase;
    y2 = mabase + model.GenericCaseH;
    taillex = model.Menu1TextePaint.measureText(ecritoption);
    x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
    x2 = mysurf.ScreenWidth - model.GenericInterSpace;
    if (x1 > mysurf.SettingsXmin) {
      x1 = mysurf.SettingsXmin;
    }
    ecritx = x2 - model.GenericInterSpace;
    ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
    if (clickquicknavmenu) {
      if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
        model.videoprofile = (model.videoprofile + 1) % 3;
        model.preferences.edit().putInt("videoprofile", model.videoprofile).apply();
        if (model.videoprofile == 1)
          model.message("default");
        else
          model.message();
        needanotherupdate = true;
        clickquicknavmenu = false;
      }
    }
    recti = new RectF(x1, y1, x2, y2);
    surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu1BgPainta);
    recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
    surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu1BgPainto);
    if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
      RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
      surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
    }
    surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

    mabase += model.GenericCaseH + model.GenericInterSpace;
  }

  private void menuvideofastdecode(Canvas surfacecanvas) {
    if (model.videofastdecode)
      ecritoption = "video fastdecode";
    else
      ecritoption = "no fastdecode";
    y1 = mabase;
    y2 = mabase + model.GenericCaseH;
    taillex = model.Menu1TextePaint.measureText(ecritoption);
    x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
    x2 = mysurf.ScreenWidth - model.GenericInterSpace;
    if (x1 > mysurf.SettingsXmin) {
      x1 = mysurf.SettingsXmin;
    }
    ecritx = x2 - model.GenericInterSpace;
    ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
    if (clickquicknavmenu) {
      if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
        model.videofastdecode = !model.videofastdecode;
        model.preferences.edit().putBoolean("videofastdecode", model.videofastdecode).apply();
        if (model.videofastdecode)
          model.message("vd-lavc-fast=yes" +
                  "\nvd-lavc-skiploopfilter=nonkey");
        else
          model.message("default preset");
        needanotherupdate = true;
        clickquicknavmenu = false;
      }
    }
    recti = new RectF(x1, y1, x2, y2);
    surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu1BgPainta);
    recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
    surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu1BgPainto);
    if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
      RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
      surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
    }
    surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

    mabase += model.GenericCaseH + model.GenericInterSpace;
  }

  private void menucache(Canvas surfacecanvas) {
      ecritoption = "cache " + model.videocachesize + "M";
      y1 = mabase;
      y2 = mabase + model.GenericCaseH;
      taillex = model.Menu1TextePaint.measureText(ecritoption);
      x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
      x2 = mysurf.ScreenWidth - model.GenericInterSpace;
      if (x1 > mysurf.SettingsXmin) {
        x1 = mysurf.SettingsXmin;
      }
      ecritx = x2 - model.GenericInterSpace;
      ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
      if (clickquicknavmenu) {
        if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
          mysurf.videoshowcachesize = !mysurf.videoshowcachesize;
          needanotherupdate = true;
          clickquicknavmenu = false;
        }
      }
      recti = new RectF(x1, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu1BgPainta);
      recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu1BgPainto);
      if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
        RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
        surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
      }
      surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

      mabase += model.GenericCaseH + model.GenericInterSpace;
      if (mabase > mysurf.myheight)
        return;

      if (mysurf.videoshowcachesize) {

        y1 = mabase;
        y2 = mabase + model.GenericCaseH;
        ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;

        ecritoption = "+";
        taillex = model.Menu1TextePaint.measureText(ecritoption);
        x1 = (mysurf.ScreenWidth - model.GenericInterSpace + mysurf.SettingsXmin) * 0.5f + model.GenericInterSpace * 0.5f;
        x2 = mysurf.ScreenWidth - model.GenericInterSpace;
        ecritx = x2;
        if (clickquicknavmenu) {
          if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
            model.videocachesize += 10;
            model.preferences.edit().putInt("videocachesize", model.videocachesize).apply();
            needanotherupdate = true;
            clickquicknavmenu = false;
          }
        }
        recti = new RectF(x1, y1, x2, y2);
        //surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
        //recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
        surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
        if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
          RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
          surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
        }
        surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

        ecritoption = "-";
        taillex = model.Menu1TextePaint.measureText(ecritoption);
        x2 = x1 - model.GenericInterSpace;
        x1 = x2 - model.GenericInterSpace * 2.0f - taillex;
      if (x1 > mysurf.SettingsXmin)
        x1 = mysurf.SettingsXmin;
        ecritx = x2 - model.GenericInterSpace;
        if (clickquicknavmenu) {
          if (mysurf.SettingsXmin < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
            model.videocachesize -= 10;
            model.preferences.edit().putInt("videocachesize", model.videocachesize).apply();
            needanotherupdate = true;
            clickquicknavmenu = false;
          }
        }
        recti = new RectF(x1, y1, x2, y2);
        surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
        recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
        surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
        if ((model.isandroidtv || model.keypadnavactive) && mysurf.SettingsXmin < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
          RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
          surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
        }
        surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

        mabase += model.GenericCaseH + model.GenericInterSpace;

      }
  }

  private void menusubtitlesize(Canvas surfacecanvas) {
      y1 = mabase;
      y2 = mabase + model.GenericCaseH;
      ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;

      ecritoption = "+";
      taillex = model.Menu1TextePaint.measureText(ecritoption);
      x1 = (mysurf.ScreenWidth - model.GenericInterSpace + mysurf.SettingsXmin) * 0.5f + model.GenericInterSpace * 0.5f;
      x2 = mysurf.ScreenWidth - model.GenericInterSpace;
      ecritx = x2;
      if (clickquicknavmenu) {
        if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
          model.videosubfontsize += 1;
          currmpvlib.setsubfontsize(model.videosubfontsize);
          model.preferences.edit().putInt("videosubfontsize", model.videosubfontsize).apply();
          model.VideoSubPaint.setTextSize((model.GenericTextH * 2.0f * model.videosubfontsize) / 55.0f);
          needanotherupdate = true;
          clickquicknavmenu = false;
        }
      }
      recti = new RectF(x1, y1, x2, y2);
      //surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
      //recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu1BgPainto);
      if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
        RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
        surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
      }
      surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

      ecritoption = String.format("subtitle size %dpx -", model.videosubfontsize);
      taillex = model.Menu1TextePaint.measureText(ecritoption);
      x2 = x1 - model.GenericInterSpace;
      x1 = x2 - model.GenericInterSpace * 2.0f - taillex;
      if (x1 > mysurf.SettingsXmin)
        x1 = mysurf.SettingsXmin;
      ecritx = x2 - model.GenericInterSpace;
      if (clickquicknavmenu) {
        if (mysurf.SettingsXmin < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
          model.videosubfontsize -= 1;
          currmpvlib.setsubfontsize(model.videosubfontsize);
          model.preferences.edit().putInt("videosubfontsize", model.videosubfontsize).apply();
          model.VideoSubPaint.setTextSize((model.GenericTextH * 2.0f * model.videosubfontsize) / 55.0f);
          needanotherupdate = true;
          clickquicknavmenu = false;
        }
      }
      recti = new RectF(x1, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu1BgPainta);
      recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu1BgPainto);
      if ((model.isandroidtv || model.keypadnavactive) && mysurf.SettingsXmin < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
        RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
        surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
      }
      surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

      mabase += model.GenericCaseH + model.GenericInterSpace;
  }

  private void menuthreads(Canvas surfacecanvas) {
      ecritoption = "threads";
      y1 = mabase;
      y2 = mabase + model.GenericCaseH;
      taillex = model.Menu1TextePaint.measureText(ecritoption);
      x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
      x2 = mysurf.ScreenWidth - model.GenericInterSpace;
      if (x1 > mysurf.SettingsXmin) {
        x1 = mysurf.SettingsXmin;
      }
      ecritx = x2 - model.GenericInterSpace;
      ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
      if (clickquicknavmenu) {
        if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
          mysurf.videoshowthreads = !mysurf.videoshowthreads;
          needanotherupdate = true;
          clickquicknavmenu = false;
        }
      }
      recti = new RectF(x1, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu1BgPainta);
      recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu1BgPainto);
      if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
        RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
        surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
      }
      surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

      mabase += model.GenericCaseH + model.GenericInterSpace;
      if (mabase > mysurf.myheight)
        return;

      if (mysurf.videoshowthreads) {

        ecritoption = String.format("video %d", model.videovthreadcount);
        if (model.videovthreadcount == 0)
          ecritoption = "video default";
        y1 = mabase;
        y2 = mabase + model.GenericCaseH;
        taillex = model.Menu1TextePaint.measureText(ecritoption);
        x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
        x2 = mysurf.ScreenWidth - model.GenericInterSpace;
        if (x1 > mysurf.SettingsXmin) {
          x1 = mysurf.SettingsXmin;
        }
        ecritx = x2 - model.GenericInterSpace;
        ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
        if (clickquicknavmenu) {
          if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
            model.videovthreadcount = 0;
            needanotherupdate = true;
            clickquicknavmenu = false;
          }
        }
        recti = new RectF(x1, y1, x2, y2);
        surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
        recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
        surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
        if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
          RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
          surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
        }
        surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

        mabase += model.GenericCaseH + model.GenericInterSpace;
        if (mabase > mysurf.myheight)
          return;


        y1 = mabase;
        y2 = mabase + model.GenericCaseH;
        ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;

        ecritoption = "+";
        taillex = model.Menu1TextePaint.measureText(ecritoption);
        x1 = (mysurf.ScreenWidth - model.GenericInterSpace + mysurf.SettingsXmin) * 0.5f + model.GenericInterSpace * 0.5f;
        x2 = mysurf.ScreenWidth - model.GenericInterSpace;
        ecritx = x2;
        if (clickquicknavmenu) {
          if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
            model.videovthreadcount += 1;
            model.preferences.edit().putInt("videovthreadcount", model.videovthreadcount).apply();
            needanotherupdate = true;
            clickquicknavmenu = false;
          }
        }
        recti = new RectF(x1, y1, x2, y2);
        //surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
        //recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
        surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
        if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
          RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
          surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
        }
        surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

        ecritoption = "-";
        taillex = model.Menu1TextePaint.measureText(ecritoption);
        x2 = x1 - model.GenericInterSpace;
        x1 = x2 - model.GenericInterSpace * 2.0f - taillex;
      if (x1 > mysurf.SettingsXmin)
        x1 = mysurf.SettingsXmin;
        ecritx = x2 - model.GenericInterSpace;
        if (clickquicknavmenu) {
          if (mysurf.SettingsXmin < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
            model.videovthreadcount -= 1;
            model.preferences.edit().putInt("videovthreadcount", model.videovthreadcount).apply();
            needanotherupdate = true;
            clickquicknavmenu = false;
          }
        }
        recti = new RectF(x1, y1, x2, y2);
        surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
        recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
        surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
        if ((model.isandroidtv || model.keypadnavactive) && mysurf.SettingsXmin < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
          RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
          surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
        }
        surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

        mabase += model.GenericCaseH + model.GenericInterSpace;
        if (mabase > mysurf.myheight)
          return;

        ecritoption = String.format("audio %d", model.videoathreadcount);
        if (model.videoathreadcount == 0)
          ecritoption = "audio default";
        y1 = mabase;
        y2 = mabase + model.GenericCaseH;
        taillex = model.Menu1TextePaint.measureText(ecritoption);
        x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
        x2 = mysurf.ScreenWidth - model.GenericInterSpace;
        if (x1 > mysurf.SettingsXmin) {
          x1 = mysurf.SettingsXmin;
        }
        ecritx = x2 - model.GenericInterSpace;
        ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
        if (clickquicknavmenu) {
          if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
            model.videoathreadcount = 0;
            model.preferences.edit().putInt("videoathreadcount", model.videoathreadcount).apply();
            needanotherupdate = true;
            clickquicknavmenu = false;
          }
        }
        recti = new RectF(x1, y1, x2, y2);
        surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
        recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
        surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
        if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
          RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
          surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
        }
        surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

        mabase += model.GenericCaseH + model.GenericInterSpace;
        if (mabase > mysurf.myheight)
          return;


        y1 = mabase;
        y2 = mabase + model.GenericCaseH;
        ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;

        ecritoption = "+";
        taillex = model.Menu1TextePaint.measureText(ecritoption);
        x1 = (mysurf.ScreenWidth - model.GenericInterSpace + mysurf.SettingsXmin) * 0.5f + model.GenericInterSpace * 0.5f;
        x2 = mysurf.ScreenWidth - model.GenericInterSpace;
        ecritx = x2;
        if (clickquicknavmenu) {
          if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
            model.videoathreadcount += 1;
            model.preferences.edit().putInt("videoathreadcount", model.videoathreadcount).apply();
            needanotherupdate = true;
            clickquicknavmenu = false;
          }
        }
        recti = new RectF(x1, y1, x2, y2);
        //surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
        //recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
        surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
        if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
          RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
          surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
        }
        surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

        ecritoption = "-";
        taillex = model.Menu1TextePaint.measureText(ecritoption);
        x2 = x1 - model.GenericInterSpace;
        x1 = x2 - model.GenericInterSpace * 2.0f - taillex;
      if (x1 > mysurf.SettingsXmin)
        x1 = mysurf.SettingsXmin;
        ecritx = x2 - model.GenericInterSpace;
        if (clickquicknavmenu) {
          if (mysurf.SettingsXmin < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
            model.videoathreadcount -= 1;
            model.preferences.edit().putInt("videoathreadcount", model.videoathreadcount).apply();
            needanotherupdate = true;
            clickquicknavmenu = false;
          }
        }
        recti = new RectF(x1, y1, x2, y2);
        surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
        recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
        surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
        if ((model.isandroidtv || model.keypadnavactive) && mysurf.SettingsXmin < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
          RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
          surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
        }
        surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

        mabase += model.GenericCaseH + model.GenericInterSpace;

      }
  }

  private void menuluatoggle(Canvas surfacecanvas) {
      ecritoption = "lua toggle";
      y1 = mabase;
      y2 = mabase + model.GenericCaseH;
      taillex = model.Menu1TextePaint.measureText(ecritoption);
      x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
      x2 = mysurf.ScreenWidth - model.GenericInterSpace;
      if (x1 > mysurf.SettingsXmin) {
        x1 = mysurf.SettingsXmin;
      }
      ecritx = x2 - model.GenericInterSpace;
      ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
      if (clickquicknavmenu) {
        if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
          currmpvlib.command(new String[]{"script-binding", "stats/display-stats-toggle"});
          needanotherupdate = true;
          clickquicknavmenu = false;
        }
      }
      recti = new RectF(x1, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu1BgPainta);
      recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu1BgPainto);
      if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
        RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
        surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
      }
      surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

      mabase += model.GenericCaseH + model.GenericInterSpace;
      if (mabase > mysurf.myheight)
        return;

      ecritoption = "lua " + currmpvlib.statsluamode;
      switch(currmpvlib.statsluamode){
        case 1:
          ecritoption+=" global";
          break;
        case 2:
          ecritoption+=" frame";
          break;
        case 3:
          ecritoption+=" cache";
          break;
        case 4:
          ecritoption+=" perf";
          break;
        default:
          ecritoption+=" ?";
          break;
      }
      y1 = mabase;
      y2 = mabase + model.GenericCaseH;
      taillex = model.Menu1TextePaint.measureText(ecritoption);
      x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
      x2 = mysurf.ScreenWidth - model.GenericInterSpace;
      if (x1 > mysurf.SettingsXmin) {
        x1 = mysurf.SettingsXmin;
      }
      ecritx = x2 - model.GenericInterSpace;
      ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
      if (clickquicknavmenu) {
        if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
          currmpvlib.statsluamode += 1;
          if (currmpvlib.statsluamode > 4)
            currmpvlib.statsluamode = 1;
          currmpvlib.command(new String[]{"script-binding", "stats/display-page-" + currmpvlib.statsluamode});
          needanotherupdate = true;
          clickquicknavmenu = false;
        }
      }
      recti = new RectF(x1, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu1BgPainta);
      recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu1BgPainto);
      if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
        RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
        surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
      }
      surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

      mabase += model.GenericCaseH + model.GenericInterSpace;
  }

  private void menuscreenshot(Canvas surfacecanvas) {
      ecritoption = "screenshot";
      y1 = mabase;
      y2 = mabase + model.GenericCaseH;
      taillex = model.Menu1TextePaint.measureText(ecritoption);
      x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
      x2 = mysurf.ScreenWidth - model.GenericInterSpace;
      if (x1 > mysurf.SettingsXmin) {
        x1 = mysurf.SettingsXmin;
      }
      ecritx = x2 - model.GenericInterSpace;
      ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
      if (clickquicknavmenu) {
        if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
          String scrotfolder = model.dossierscreenshot;
          String scrotfile = scrotfolder + System.currentTimeMillis() + ".png";
          llog.d(TAG, "screenshot-to-file " + scrotfile);
          currmpvlib.command(new String[]{"screenshot-to-file", scrotfile, "video"});
          try {
            model.commandethreaddatabase.put(new String[]{"chercheFichierDansDatabaseSinonRescanSonDossier", scrotfolder, scrotfile, "-1"});
          } catch (InterruptedException e) {
            e.printStackTrace();
          }
          model.message("took screenshot\n" + scrotfile);
          needanotherupdate = true;
          clickquicknavmenu = false;
        }
      }
      recti = new RectF(x1, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu1BgPainta);
      recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu1BgPainto);
      if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
        RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
        surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
      }
      surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

      mabase += model.GenericCaseH + model.GenericInterSpace;
  }

  private void menuscreenshotraw(Canvas surfacecanvas) {
      ecritoption = "screenshotraw";
      y1 = mabase;
      y2 = mabase + model.GenericCaseH;
      taillex = model.Menu1TextePaint.measureText(ecritoption);
      x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
      x2 = mysurf.ScreenWidth - model.GenericInterSpace;
      if (x1 > mysurf.SettingsXmin) {
        x1 = mysurf.SettingsXmin;
      }
      ecritx = x2 - model.GenericInterSpace;
      ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
      if (clickquicknavmenu) {
        if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
          String scrotfolder = model.dossierscreenshot;
          String scrotfile = scrotfolder + System.currentTimeMillis() + ".png";
          llog.d(TAG, "screenshot-raw");

          int headersize = 15;
          int imagesize = 4 * currmpvlib.videowidth * currmpvlib.videoheight;
          ByteBuffer scrotbuf = ByteBuffer.allocateDirect(headersize + imagesize);
          int success = currmpvlib.grabThumbnail(scrotbuf);

          scrotbuf.rewind();
          int w = 0, h = 0, s = 0;
          for (int i = 0 ; i < 3 ; i++) {
            int t = scrotbuf.get();
            if (t == (byte) 'w')
              w = scrotbuf.getInt();
            else if (t == (byte) 'h')
              h = scrotbuf.getInt();
            else if (t == (byte) 's')
              s = scrotbuf.getInt();
          }
          llog.d(TAG, w + "x" + h + " stride " + s);
          byte arr[] = new byte[imagesize];
          scrotbuf.get(arr);

          Bitmap bitmap = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);
          ByteBuffer buf = ByteBuffer.wrap(arr);
          bitmap.copyPixelsFromBuffer(buf);

          FileOutputStream fos;
          try {
            fos = new FileOutputStream(scrotfile);
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, fos); // 100 ignored for png
            fos.close();
          } catch (IOException e) {
            e.printStackTrace();
          }
          bitmap.recycle();
          bitmap = null;
          buf = null;

          arr = null;
          scrotbuf = null;

          try {
            model.commandethreaddatabase.put(new String[]{"chercheFichierDansDatabaseSinonRescanSonDossier", scrotfolder, scrotfile, "-1"});
          } catch (InterruptedException e) {
            e.printStackTrace();
          }
          llog.d(TAG, "took screenshot " + scrotfile);
          model.message("took screenshot\n" + scrotfile);

          needanotherupdate = true;
          clickquicknavmenu = false;
        }
      }
      recti = new RectF(x1, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu1BgPainta);
      recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu1BgPainto);
      if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
        RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
        surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
      }
      surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

      mabase += model.GenericCaseH + model.GenericInterSpace;

      /*if ((model.isandroidtv || model.keypadnavactive)) {
        surfacecanvas.drawCircle(mysurf.cursorx, model.GenericInterSpace + model.GenericCaseH * 0.5f, model.GenericCaseH * 0.5f, model.SettingsHighlightBackPaint);
        surfacecanvas.drawCircle(mysurf.cursorx, model.GenericInterSpace + model.GenericCaseH * 0.5f, model.GenericCaseH * 0.5f, model.SettingsHighlightPaint);
      }*/

    }

  void deleteRecursive(File fileOrDirectory) {
    /*if (fileOrDirectory.isDirectory()) {
      for (File child : fileOrDirectory.listFiles()) {
        deleteRecursive(child);
      }
    }
    fileOrDirectory.delete();*/
  }



}
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
















