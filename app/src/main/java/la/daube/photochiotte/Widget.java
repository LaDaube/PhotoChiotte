package la.daube.photochiotte;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BlurMaskFilter;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.RectF;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RemoteViews;

import androidx.preference.PreferenceManager;

public class Widget extends AppWidgetProvider {
    private static final String TAG = "YYYwdg";

    private final static float miny = 0.62f;
    private final static float maxy = 0.90f;
    private static int buttontextsize = 0;

    @Override
    public void onEnabled(Context context) {
        super.onEnabled(context);
        llog.d(TAG, "onEnabled()");
    }

    @Override
    public void onDisabled(Context context) {
        super.onDisabled(context);
        llog.d(TAG, "onDisabled()");
    }

    @Override
    public void onDeleted(Context context, int[] appWidgetIds) {
        super.onDeleted(context, appWidgetIds);
        llog.d(TAG, "onDeleted() " + appWidgetIds.length);
    }

    @Override
    public void onAppWidgetOptionsChanged(Context context, AppWidgetManager appWidgetManager,
                                          int appWidgetId, Bundle newOptions) {
        super.onAppWidgetOptionsChanged(context, appWidgetManager,
                                          appWidgetId, newOptions);
        llog.d(TAG, "onAppWidgetOptionsChanged() ");
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        super.onReceive(context, intent);
        String action = intent.getAction();
        llog.d(TAG, "onReceive() " + action);
    }

    @Override
    public void onRestored(Context context, int[] oldWidgetIds, int[] newWidgetIds) {
        super.onRestored(context, oldWidgetIds, newWidgetIds);
        llog.d(TAG, "onRestored() ");
    }

    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        // Perform this loop procedure for each widget that belongs to this provider.
        llog.d(TAG, "onUpdate");
        for (int iii = 0; iii < appWidgetIds.length ; iii++) {
            int appWidgetId = appWidgetIds[iii];
            llog.d(TAG, "onUpdate " + appWidgetId);

            RemoteViews views = new RemoteViews(context.getPackageName(), R.layout.notification_collapsed);

            if (Gallery.isMyServiceRunning(backgroundService.class, context) && Gallery.backgroundService != null)
                drawwidget(views, context, Gallery.backgroundService.threadDiscover, Gallery.backgroundService.threadmusic);
            else
                drawwidget(views, context, null, null);

            // Instruct the widget manager to update the widget
            appWidgetManager.updateAppWidget(appWidgetId, views);
        }
    }

    public void upwidg(Context context, int appWidgetId) {
        llog.d(TAG, "upwidg " + appWidgetId);
        // Construct the RemoteViews object
        RemoteViews views = new RemoteViews(context.getPackageName(), R.layout.notification_collapsed);

        //SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        if (Gallery.isMyServiceRunning(backgroundService.class, context) && Gallery.backgroundService != null)
            drawwidget(views, context, Gallery.backgroundService.threadDiscover, Gallery.backgroundService.threadmusic);
        else
            drawwidget(views, context, null, null);

        AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(context);
        ComponentName thisAppWidget = new ComponentName(context.getPackageName(), Widget.class.getName());
        int[] appWidgetIds = appWidgetManager.getAppWidgetIds(thisAppWidget);
        for (int iii = 0; iii < appWidgetIds.length ; iii++)
            llog.d(TAG, "upwidg " + appWidgetId + " - " + appWidgetIds[iii]);

        // Instruct the widget manager to update the widget
        appWidgetManager.updateAppWidget(appWidgetId, views);
    }

    public static void drawwidget(RemoteViews notificationLayout, Context context,
                           ThreadDiscover mThreadDiscover, ThreadMusic mthreadmusic) {
        llog.d(TAG, "drawwidget");

        // inutile
        //myViewModel.isMyServiceRunning(myService.class, context);

        int flags;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
            flags = PendingIntent.FLAG_MUTABLE | PendingIntent.FLAG_UPDATE_CURRENT;
        } else {
            flags = PendingIntent.FLAG_UPDATE_CURRENT;
        }

        Intent intent;
        PendingIntent pIntent;

        // 1 = 40
        // n = 70*x-30

        //buttontextsize = (int) PreferenceManager.getDefaultSharedPreferences(context).getFloat("buttontextsize", 1.0f);
        if (buttontextsize <= 1.0f) {
            Button button = new Button(context);
            buttontextsize = (int) button.getTextSize();
            //llog.d(TAG, "buttontextsize " + buttontextsize);
        }

        float mult = 4.0f;
        //int ih = (int) (mult * 2 * buttontextsize);
        int ih = (int) (mult * buttontextsize); // définition
        //int iw = mult * 2 * buttontextsize * 2;
        int iw = (int) (mult * 1.0f * buttontextsize);
        int iwm = iw * 10; // longueur du texte


        Paint paintbacktext = new Paint();
        paintbacktext.setAntiAlias(true);
        Paint painttext = new Paint();
        painttext.setAntiAlias(true);
        float textsize = ih * 0.27f;
        painttext.setTextSize(textsize);
        float GenericInterSpace = textsize * 0.15f;
        float largeur = 0.0f;
        float hauteur = 0.0f;
        float boitelargeur = 0.0f;
        float boitehauteur = 0.0f;
        float posx = 0.0f;
        float posy = 0.0f;
        Rect rectinit = new Rect();
        Rect bounds = new Rect();
        String texte;
        RectF rect;
        Bitmap.Config config = Bitmap.Config.ARGB_8888;
        Bitmap bitmap = Bitmap.createBitmap(iwm, ih, config);
        Canvas canvas = new Canvas(bitmap); // Load the Bitmap to the Canvas

        painttext.setColor(Gallery.CouleurPastelBlack);
        painttext.getTextBounds("OqpZTj", 0, "OqpZTj".length(), bounds);
        boitehauteur = (int) (bounds.height() + GenericInterSpace * 2);
        float boitehauteurshifttext = boitehauteur * 1.10f - bounds.height() - bounds.top;



        if (mThreadDiscover != null) {

            texte = "discover";
            paintbacktext.setColor(Gallery.CouleurPastelFailed);
            if (backgroundService.torpubkey != null) {
                if (!backgroundService.torpubkey.equals("-")) {
                    texte = backgroundService.torpubkey;
                    paintbacktext.setColor(Gallery.CouleurPastelSuccess);
                }
            }
            painttext.setTextAlign(Paint.Align.LEFT);
            painttext.getTextBounds(texte, 0, texte.length(), bounds);
            boitelargeur = (int) (bounds.width() + GenericInterSpace * 2);
            rect = new RectF(posx, posy, posx + boitelargeur, posy + boitehauteur);
            canvas.drawRoundRect(rect, GenericInterSpace * 3.0f, GenericInterSpace * 3.0f, paintbacktext);
            canvas.drawText(texte, posx + GenericInterSpace, posy + boitehauteurshifttext - GenericInterSpace, painttext);

            posx += boitelargeur + GenericInterSpace;

            texte = backgroundService.STATUS_CLIENTSL + "nodes/" + backgroundService.STATUS_RANGESL;
            if (backgroundService.STATUS_CLIENTSL > 0)
                paintbacktext.setColor(Gallery.CouleurPastelSuccess);
            else
                paintbacktext.setColor(Gallery.CouleurPastelFailed);
            painttext.setTextAlign(Paint.Align.LEFT);
            painttext.getTextBounds(texte, 0, texte.length(), bounds);
            boitelargeur = (int) (bounds.width() + GenericInterSpace * 2);
            rect = new RectF(posx, posy, posx + boitelargeur, posy + boitehauteur);
            canvas.drawRoundRect(rect, GenericInterSpace * 3.0f, GenericInterSpace * 3.0f, paintbacktext);
            canvas.drawText(texte, posx + GenericInterSpace, posy + boitehauteurshifttext - GenericInterSpace, painttext);

            posx += boitelargeur + GenericInterSpace;

            texte = backgroundService.STATUS_FIND_OUR_POSITION_SUCCESS + "sit|" + backgroundService.STATUS_FIND_OUR_POSITION_FAILED;
            if (backgroundService.STATUS_FIND_OUR_POSITION_LAST)
                paintbacktext.setColor(Gallery.CouleurPastelSuccess);
            else
                paintbacktext.setColor(Gallery.CouleurPastelFailed);
            painttext.setTextAlign(Paint.Align.LEFT);
            painttext.getTextBounds(texte, 0, texte.length(), bounds);
            boitelargeur = (int) (bounds.width() + GenericInterSpace * 2);
            rect = new RectF(posx, posy, posx + boitelargeur, posy + boitehauteur);
            canvas.drawRoundRect(rect, GenericInterSpace * 3.0f, GenericInterSpace * 3.0f, paintbacktext);
            canvas.drawText(texte, posx + GenericInterSpace, posy + boitehauteurshifttext - GenericInterSpace, painttext);

            posx += boitelargeur + GenericInterSpace;

            texte = backgroundService.STATUS_HASHESL + "files";
            if (backgroundService.STATUS_HASHESL > 0)
                paintbacktext.setColor(Gallery.CouleurPastelSuccess);
            else
                paintbacktext.setColor(Gallery.CouleurPastelFailed);
            painttext.setTextAlign(Paint.Align.LEFT);
            painttext.getTextBounds(texte, 0, texte.length(), bounds);
            boitelargeur = (int) (bounds.width() + GenericInterSpace * 2);
            rect = new RectF(posx, posy, posx + boitelargeur, posy + boitehauteur);
            canvas.drawRoundRect(rect, GenericInterSpace * 3.0f, GenericInterSpace * 3.0f, paintbacktext);
            canvas.drawText(texte, posx + GenericInterSpace, posy + boitehauteurshifttext - GenericInterSpace, painttext);

            posx += boitelargeur + GenericInterSpace;

            texte = backgroundService.STATUS_ANNOUNCE_PEER_SUCCESS + "publish|" + backgroundService.STATUS_ANNOUNCE_PEER_FAILED;
            if (backgroundService.STATUS_ANNOUNCE_PEER_LAST)
                paintbacktext.setColor(Gallery.CouleurPastelSuccess);
            else
                paintbacktext.setColor(Gallery.CouleurPastelFailed);
            painttext.setTextAlign(Paint.Align.LEFT);
            painttext.getTextBounds(texte, 0, texte.length(), bounds);
            boitelargeur = (int) (bounds.width() + GenericInterSpace * 2);
            rect = new RectF(posx, posy, posx + boitelargeur, posy + boitehauteur);
            canvas.drawRoundRect(rect, GenericInterSpace * 3.0f, GenericInterSpace * 3.0f, paintbacktext);
            canvas.drawText(texte, posx + GenericInterSpace, posy + boitehauteurshifttext - GenericInterSpace, painttext);

            posx += boitelargeur + GenericInterSpace;

            texte = backgroundService.STATUS_GET_PEERS_SUCCESS + "peers|" + backgroundService.STATUS_GET_PEERS_FAILED;
            if (backgroundService.STATUS_GET_PEERS_LAST)
                paintbacktext.setColor(Gallery.CouleurPastelSuccess);
            else
                paintbacktext.setColor(Gallery.CouleurPastelFailed);
            painttext.setTextAlign(Paint.Align.LEFT);
            painttext.getTextBounds(texte, 0, texte.length(), bounds);
            boitelargeur = (int) (bounds.width() + GenericInterSpace * 2);
            rect = new RectF(posx, posy, posx + boitelargeur, posy + boitehauteur);
            canvas.drawRoundRect(rect, GenericInterSpace * 3.0f, GenericInterSpace * 3.0f, paintbacktext);
            canvas.drawText(texte, posx + GenericInterSpace, posy + boitehauteurshifttext - GenericInterSpace, painttext);

            posx += boitelargeur + GenericInterSpace;

            texte = backgroundService.STATUS_SEARCH_KEYWORDS_FOUND + "dl";
            if (backgroundService.STATUS_SEARCH_KEYWORDS_FOUND > 0)
                paintbacktext.setColor(Gallery.CouleurPastelSuccess);
            else
                paintbacktext.setColor(Gallery.CouleurPastelFailed);
            painttext.setTextAlign(Paint.Align.LEFT);
            painttext.getTextBounds(texte, 0, texte.length(), bounds);
            boitelargeur = (int) (bounds.width() + GenericInterSpace * 2);
            rect = new RectF(posx, posy, posx + boitelargeur, posy + boitehauteur);
            canvas.drawRoundRect(rect, GenericInterSpace * 3.0f, GenericInterSpace * 3.0f, paintbacktext);
            canvas.drawText(texte, posx + GenericInterSpace, posy + boitehauteurshifttext - GenericInterSpace, painttext);

            posx += boitelargeur + GenericInterSpace;

            texte = backgroundService.STATUS_DOWNLOAD_SUCCESS + "dl|" + backgroundService.STATUS_DOWNLOAD_FAILED;
            if (backgroundService.STATUS_DOWNLOAD_LAST)
                paintbacktext.setColor(Gallery.CouleurPastelSuccess);
            else
                paintbacktext.setColor(Gallery.CouleurPastelFailed);
            painttext.setTextAlign(Paint.Align.LEFT);
            painttext.getTextBounds(texte, 0, texte.length(), bounds);
            boitelargeur = (int) (bounds.width() + GenericInterSpace * 2);
            rect = new RectF(posx, posy, posx + boitelargeur, posy + boitehauteur);
            canvas.drawRoundRect(rect, GenericInterSpace * 3.0f, GenericInterSpace * 3.0f, paintbacktext);
            canvas.drawText(texte, posx + GenericInterSpace, posy + boitehauteurshifttext - GenericInterSpace, painttext);

            posx += boitelargeur + GenericInterSpace;

            posy += boitehauteur + GenericInterSpace;

        }

        if (mthreadmusic != null) {

            // posy = 0.328f * ih
            posx = iwm;

            if (backgroundService.musicstatusname != null) {
                String[] name = backgroundService.musicstatusname.split("/+");
                int couleurlen = Gallery.CouleurPastel.length;
                for (int i = name.length - 1 ; i >= 0 ; i--) {
                    paintbacktext.setColor(Gallery.CouleurPastel[i % couleurlen]);
                    texte = name[i];
                    painttext.setTextAlign(Paint.Align.RIGHT);
                    painttext.getTextBounds(texte, 0, texte.length(), bounds);
                    boitelargeur = (int) (bounds.width() + GenericInterSpace * 2);
                    rect = new RectF(posx - boitelargeur, posy, posx, posy + boitehauteur);
                    canvas.drawRoundRect(rect, GenericInterSpace * 3.0f, GenericInterSpace * 3.0f, paintbacktext);
                    canvas.drawText(texte, posx - GenericInterSpace, posy + boitehauteurshifttext - GenericInterSpace, painttext);

                    posx -= boitelargeur + GenericInterSpace;
                }
            }

        }

        notificationLayout.setImageViewBitmap(R.id.mediatitle, bitmap);



        if (mthreadmusic != null) {

            bitmap = Bitmap.createBitmap(iw, ih, config);
            canvas = new Canvas(bitmap);
            drawpathes(drawprevious(iw, ih), iw, ih , canvas);
            intent = new Intent(backgroundService.REQUEST + backgroundService.REQUEST_PREVIOUS);
            intent.setClass(context, backgroundService.class);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
                pIntent = PendingIntent.getForegroundService(context, backgroundService.REQUEST_PREVIOUS, intent, flags);
            else
                pIntent = PendingIntent.getService(context, backgroundService.REQUEST_PREVIOUS, intent, flags);
            notificationLayout.setImageViewBitmap(R.id.mediaprevious, bitmap);
            notificationLayout.setOnClickPendingIntent(R.id.mediaprevious, pIntent);

            if (backgroundService.musicstatuspaused) {
                bitmap = Bitmap.createBitmap(iw, ih, config);
                canvas = new Canvas(bitmap);
                drawpathes(drawplay(iw, ih), iw, ih , canvas);
                intent = new Intent(backgroundService.REQUEST + backgroundService.REQUEST_PAUSE);
                intent.setClass(context, backgroundService.class);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
                    pIntent = PendingIntent.getForegroundService(context, backgroundService.REQUEST_PAUSE, intent, flags);
                else
                    pIntent = PendingIntent.getService(context, backgroundService.REQUEST_PAUSE, intent, flags);
                notificationLayout.setImageViewBitmap(R.id.mediapause, bitmap);
                notificationLayout.setOnClickPendingIntent(R.id.mediapause, pIntent);
            } else {
                bitmap = Bitmap.createBitmap(iw, ih, config);
                canvas = new Canvas(bitmap);
                drawpathes(drawpause(iw, ih), iw, ih , canvas);
                intent = new Intent(backgroundService.REQUEST + backgroundService.REQUEST_PAUSE);
                intent.setClass(context, backgroundService.class);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
                    pIntent = PendingIntent.getForegroundService(context, backgroundService.REQUEST_PAUSE, intent, flags);
                else
                    pIntent = PendingIntent.getService(context, backgroundService.REQUEST_PAUSE, intent, flags);
                notificationLayout.setImageViewBitmap(R.id.mediapause, bitmap);
                notificationLayout.setOnClickPendingIntent(R.id.mediapause, pIntent);
            }

            bitmap = Bitmap.createBitmap(iw, ih, config);
            canvas = new Canvas(bitmap);
            drawpathes(drawnext(iw, ih), iw, ih , canvas);
            intent = new Intent(backgroundService.REQUEST + backgroundService.REQUEST_NEXT);
            intent.setClass(context, backgroundService.class);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
                pIntent = PendingIntent.getForegroundService(context, backgroundService.REQUEST_NEXT, intent, flags);
            else
                pIntent = PendingIntent.getService(context, backgroundService.REQUEST_NEXT, intent, flags);
            notificationLayout.setImageViewBitmap(R.id.medianext, bitmap);
            notificationLayout.setOnClickPendingIntent(R.id.medianext, pIntent);

            if (backgroundService.musicstatusrandom) {
                bitmap = Bitmap.createBitmap(iw, ih, config);
                canvas = new Canvas(bitmap);
                drawpathes(drawrandom(iw, ih), iw, ih , canvas);
                intent = new Intent(backgroundService.REQUEST + backgroundService.REQUEST_RANDOM);
                intent.setClass(context, backgroundService.class);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
                    pIntent = PendingIntent.getForegroundService(context, backgroundService.REQUEST_RANDOM, intent, flags);
                else
                    pIntent = PendingIntent.getService(context, backgroundService.REQUEST_RANDOM, intent, flags);
                notificationLayout.setImageViewBitmap(R.id.mediarandom, bitmap);
                notificationLayout.setOnClickPendingIntent(R.id.mediarandom, pIntent);
            } else {
                bitmap = Bitmap.createBitmap(iw, ih, config);
                canvas = new Canvas(bitmap);
                drawpathes(drawlinear(iw, ih), iw, ih , canvas);
                intent = new Intent(backgroundService.REQUEST + backgroundService.REQUEST_RANDOM);
                intent.setClass(context, backgroundService.class);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
                    pIntent = PendingIntent.getForegroundService(context, backgroundService.REQUEST_RANDOM, intent, flags);
                else
                    pIntent = PendingIntent.getService(context, backgroundService.REQUEST_RANDOM, intent, flags);
                notificationLayout.setImageViewBitmap(R.id.mediarandom, bitmap);
                notificationLayout.setOnClickPendingIntent(R.id.mediarandom, pIntent);
            }


            bitmap = Bitmap.createBitmap(iw, ih, config);
            canvas = new Canvas(bitmap);
            drawpathes(drawsearch(iw, ih), iw, ih , canvas);
            intent = new Intent(context, MainActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            intent.putExtra("SearchMusic", true);
            pIntent = PendingIntent.getActivity(context, 0, intent,PendingIntent.FLAG_UPDATE_CURRENT | PendingIntent.FLAG_IMMUTABLE);
            notificationLayout.setImageViewBitmap(R.id.mediasearch, bitmap);
            notificationLayout.setOnClickPendingIntent(R.id.mediasearch, pIntent);


            notificationLayout.setViewVisibility(R.id.mediaprevious, View.VISIBLE);
            notificationLayout.setViewVisibility(R.id.mediapause, View.VISIBLE);
            notificationLayout.setViewVisibility(R.id.medianext, View.VISIBLE);
            notificationLayout.setViewVisibility(R.id.mediarandom, View.VISIBLE);
            notificationLayout.setViewVisibility(R.id.mediasearch, View.VISIBLE);
            notificationLayout.setViewVisibility(R.id.mediacollection, View.VISIBLE);
        } else {
            notificationLayout.setViewVisibility(R.id.mediaprevious, View.GONE);
            notificationLayout.setViewVisibility(R.id.mediapause, View.GONE);
            notificationLayout.setViewVisibility(R.id.medianext, View.GONE);
            notificationLayout.setViewVisibility(R.id.mediarandom, View.GONE);
            notificationLayout.setViewVisibility(R.id.mediasearch, View.GONE);
            notificationLayout.setViewVisibility(R.id.mediacollection, View.GONE);
        }

        if (mthreadmusic != null) {
            bitmap = Bitmap.createBitmap(iw, ih, config);
            canvas = new Canvas(bitmap);
            drawpathes(drawmusic(iw, ih), iw, ih, canvas);
            intent = new Intent(backgroundService.REQUEST + backgroundService.REQUEST_QUIT);
            intent.setClass(context, backgroundService.class);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
                pIntent = PendingIntent.getForegroundService(context, backgroundService.REQUEST_QUIT, intent, flags);
            else
                pIntent = PendingIntent.getService(context, backgroundService.REQUEST_QUIT, intent, flags);
            notificationLayout.setImageViewBitmap(R.id.mediaquit, bitmap);
            notificationLayout.setOnClickPendingIntent(R.id.mediaquit, pIntent);
        } else {
            bitmap = Bitmap.createBitmap(iw, ih, config);
            canvas = new Canvas(bitmap);
            drawpathes(drawmusic(iw, ih), iw, ih, canvas);
            drawpathes(drawcross(iw, ih), iw, ih, canvas);
            intent = new Intent(backgroundService.REQUEST + backgroundService.REQUEST_QUIT);
            intent.setClass(context, backgroundService.class);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
                pIntent = PendingIntent.getForegroundService(context, backgroundService.REQUEST_QUIT, intent, flags);
            else
                pIntent = PendingIntent.getService(context, backgroundService.REQUEST_QUIT, intent, flags);
            notificationLayout.setImageViewBitmap(R.id.mediaquit, bitmap);
            notificationLayout.setOnClickPendingIntent(R.id.mediaquit, pIntent);
        }

        if (mThreadDiscover != null) {
            bitmap = Bitmap.createBitmap(iw, ih, config);
            canvas = new Canvas(bitmap);
            drawpathes(drawtor(iw, ih), iw, ih, canvas);
            intent = new Intent(backgroundService.REQUEST + backgroundService.REQUEST_DQUIT);
            intent.setClass(context, backgroundService.class);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
                pIntent = PendingIntent.getForegroundService(context, backgroundService.REQUEST_DQUIT, intent, flags);
            else
                pIntent = PendingIntent.getService(context, backgroundService.REQUEST_DQUIT, intent, flags);
            notificationLayout.setImageViewBitmap(R.id.discoverquit, bitmap);
            notificationLayout.setOnClickPendingIntent(R.id.discoverquit, pIntent);
        } else {
            bitmap = Bitmap.createBitmap(iw, ih, config);
            canvas = new Canvas(bitmap);
            drawpathes(drawtor(iw, ih), iw, ih, canvas);
            drawpathes(drawcross(iw, ih), iw, ih, canvas);
            intent = new Intent(backgroundService.REQUEST + backgroundService.REQUEST_DQUIT);
            intent.setClass(context, backgroundService.class);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
                pIntent = PendingIntent.getForegroundService(context, backgroundService.REQUEST_DQUIT, intent, flags);
            else
                pIntent = PendingIntent.getService(context, backgroundService.REQUEST_DQUIT, intent, flags);
            notificationLayout.setImageViewBitmap(R.id.discoverquit, bitmap);
            notificationLayout.setOnClickPendingIntent(R.id.discoverquit, pIntent);
        }
    }


    private static Path drawsearch(float iw, float ih){
        float ihi = miny * ih;
        float iha = maxy * ih;
        float ihm = (ihi + iha) * 0.5f;
        float ihd = (iha - ihi);
        float ihd2 = ihd * 0.5f;
        float iwm = iw * 0.50f;
        float iwi = iwm - ihd2;
        float iwa = iwm + ihd2;
        Point point2_draw = new Point((int) (iwa), (int) (ihm));
        Point point3_draw = new Point((int) (iwa + ihd), (int) (ihm));
        Path path = new Path();
        path.addCircle(iwm, ihm, ihm * 0.20f, Path.Direction.CW);
        path.moveTo(point2_draw.x, point2_draw.y);
        path.lineTo(point3_draw.x, point3_draw.y);
        path.close();
        return path;
    }

    private static Path drawquit(float iw, float ih){
        float ihi = miny * ih;
        float iha = maxy * ih;
        float ihm = (ihi + iha) * 0.5f;
        float ihd = (iha - ihi);
        float ihd2 = ihd * 0.5f;
        float iwm = iw * 0.50f;
        float iwi = iwm - ihd2;
        float iwa = iwm + ihd2;
        Point point1_draw = new Point((int) (iwi), (int) (ihi));
        Point point2_draw = new Point((int) (iwa), (int) (ihi));
        Point point3_draw = new Point((int) (iwa), (int) (iha));
        Point point4_draw = new Point((int) (iwi), (int) (iha));
        Path path = new Path();
        path.moveTo(point1_draw.x, point1_draw.y);
        path.lineTo(point2_draw.x, point2_draw.y);
        path.lineTo(point3_draw.x, point3_draw.y);
        path.lineTo(point4_draw.x, point4_draw.y);
        path.lineTo(point1_draw.x, point1_draw.y);
        path.close();
        return path;
    }

    private static Path drawlinear(float iw, float ih){
        float ihi = miny * ih;
        float iha = maxy * ih;
        float ihm = (ihi + iha) * 0.5f;
        float ihd = (iha - ihi);
        float ihd2 = ihd * 0.5f;
        float iwm = iw * 0.50f;
        float iwi = iwm - ihd2;
        float iwa = iwm + ihd2;
        Point point1_draw = new Point((int) (iwm - ihd), (int) (ihi));
        Point point3_draw = new Point((int) (iwm + ihd), (int) (ihi));
        Point point4_draw = new Point((int) (iwm - ihd), (int) (iha));
        Point point6_draw = new Point((int) (iwm + ihd), (int) (iha));
        Path path = new Path();
        path.moveTo(point1_draw.x, point1_draw.y);
        path.lineTo(point3_draw.x, point3_draw.y);
        path.moveTo(point4_draw.x, point4_draw.y);
        path.lineTo(point6_draw.x, point6_draw.y);
        return path;
    }

    private static Path drawrandom(float iw, float ih){
        float ihi = miny * ih;
        float iha = maxy * ih;
        float ihm = (ihi + iha) * 0.5f;
        float ihd = (iha - ihi);
        float ihd2 = ihd * 0.5f;
        float iwm = iw * 0.50f;
        float iwi = iwm - ihd2;
        float iwa = iwm + ihd2;
        Point point1_draw = new Point((int) (iwm - ihd), (int) (ihi));
        Point point2_draw = new Point((int) (iwm), (int) (ihi));
        Point point3_draw = new Point((int) (iwm + ihd), (int) (iha));
        Point point4_draw = new Point((int) (iwm - ihd), (int) (iha));
        Point point5_draw = new Point((int) (iwm), (int) (iha));
        Point point6_draw = new Point((int) (iwm + ihd), (int) (ihi));
        Path path = new Path();
        path.moveTo(point1_draw.x, point1_draw.y);
        path.lineTo(point2_draw.x, point2_draw.y);
        path.lineTo(point3_draw.x, point3_draw.y);
        path.moveTo(point4_draw.x, point4_draw.y);
        path.lineTo(point5_draw.x, point5_draw.y);
        path.lineTo(point6_draw.x, point6_draw.y);
        return path;
    }

    private static Path drawpause(float iw, float ih){
        float ihi = miny * ih;
        float iha = maxy * ih;
        float ihm = (ihi + iha) * 0.5f;
        float ihd = (iha - ihi);
        float ihd2 = ihd * 0.50f;
        float ihd4 = ihd * 0.25f;
        float ihd6 = ihd * 0.66f;
        float iwm = iw * 0.50f;
        float iwi = iwm - ihd2;
        float iwa = iwm + ihd2;
        Point point1_draw = new Point((int) (iwm - ihd6), (int) (ihi));
        Point point2_draw = new Point((int) (iwm - ihd4), (int) (ihi));
        Point point3_draw = new Point((int) (iwm - ihd4), (int) (iha));
        Point point4_draw = new Point((int) (iwm - ihd6), (int) (iha));
        Point point5_draw = new Point((int) (iwm + ihd4), (int) (ihi));
        Point point6_draw = new Point((int) (iwm + ihd6), (int) (ihi));
        Point point7_draw = new Point((int) (iwm + ihd6), (int) (iha));
        Point point8_draw = new Point((int) (iwm + ihd4), (int) (iha));
        Path path = new Path();
        path.moveTo(point1_draw.x, point1_draw.y);
        path.lineTo(point2_draw.x, point2_draw.y);
        path.lineTo(point3_draw.x, point3_draw.y);
        path.lineTo(point4_draw.x, point4_draw.y);
        path.lineTo(point1_draw.x, point1_draw.y);
        path.moveTo(point5_draw.x, point5_draw.y);
        path.lineTo(point6_draw.x, point6_draw.y);
        path.lineTo(point7_draw.x, point7_draw.y);
        path.lineTo(point8_draw.x, point8_draw.y);
        path.lineTo(point5_draw.x, point5_draw.y);
        path.close();
        return path;
    }

    private static Path drawplay(float iw, float ih){
        float ihi = miny * ih;
        float iha = maxy * ih;
        float ihm = (ihi + iha) * 0.5f;
        float ihd = (iha - ihi);
        float ihd2 = ihd * 0.5f;
        float ihd6 = ihd * 0.66f;
        float iwm = iw * 0.50f;
        float iwi = iwm - ihd2;
        float iwa = iwm + ihd2;
        Point point1_draw = new Point((int) (iwm - ihd6), (int) (ihi));
        Point point2_draw = new Point((int) (iwm + ihd6), (int) (ihm));
        Point point3_draw = new Point((int) (iwm - ihd6), (int) (iha));
        Path path = new Path();
        path.moveTo(point1_draw.x, point1_draw.y);
        path.lineTo(point2_draw.x, point2_draw.y);
        path.lineTo(point3_draw.x, point3_draw.y);
        path.lineTo(point1_draw.x, point1_draw.y);
        path.close();
        return path;
    }

    private static Path drawmusic(float iw, float ih){
        float ihi = miny * ih;
        float iha = maxy * ih;
        float ihm = (ihi + iha) * 0.5f;
        float ihd = (iha - ihi);
        float ihd2 = ihd * 0.5f;
        float iwm = iw * 0.50f;
        float iwi = iwm - ihd2;
        float iwa = iwm + ihd2;
        Point point1_draw = new Point((int) (iwm), (int) (ihi));
        Point point2_draw = new Point((int) (iwm + ihd * 0.33f), (int) (ihi));
        Point point3_draw = new Point((int) (iwm), (int) (iha - ihd * 0.10f));
        Point point4_draw = new Point((int) (iwm - ihd * 0.20f), (int) (iha - ihd * 0.10f));
        Path path = new Path();
        path.moveTo(point2_draw.x, point2_draw.y);
        path.lineTo(point1_draw.x, point1_draw.y);
        path.lineTo(point3_draw.x, point3_draw.y);
        path.addCircle(point4_draw.x, point4_draw.y, ihd * 0.20f, Path.Direction.CW);
        return path;
    }

    private static Path drawtor(float iw, float ih){
        float ihi = miny * ih;
        float iha = maxy * ih;
        float ihm = (ihi + iha) * 0.5f;
        float ihd = (iha - ihi);
        float ihd2 = ihd * 0.6f;
        float ihd7 = ihd * 1.10f;
        float iwm = iw * 0.50f;
        float iwi = iwm - ihd2;
        float iwa = iwm + ihd2;
        Point point1_draw = new Point((int) (iwm),                (int) (ihi + ihd * 0.50f));
        Point point2_draw = new Point((int) (iwm - ihd * 0.50f),  (int) (ihi + ihd * 0.85f));
        Point point3_draw = new Point((int) (iwm + ihd * 0.50f),  (int) (ihi + ihd * 0.85f));
        Point point4_draw = new Point((int) (iwm),                (int) (ihi - ihd * 0.15f));
        Path path = new Path();
        path.addCircle(point1_draw.x, point1_draw.y, ihd * 0.20f, Path.Direction.CW);
        path.addCircle(point2_draw.x, point2_draw.y, ihd * 0.15f, Path.Direction.CW);
        path.addCircle(point3_draw.x, point3_draw.y, ihd * 0.15f, Path.Direction.CW);
        path.addCircle(point4_draw.x, point4_draw.y, ihd * 0.15f, Path.Direction.CW);
        return path;
    }

    private static Path drawcross(float iw, float ih){
        float ihi = miny * ih;
        float iha = maxy * ih;
        float ihm = (ihi + iha) * 0.5f;
        float ihd = (iha - ihi);
        float ihd2 = ihd * 0.5f;
        float iwm = iw * 0.50f;
        float iwi = iwm - ihd2;
        float iwa = iwm + ihd2;
        Point point1_draw = new Point((int) (iwm - ihd2), (int) (ihi));
        Point point3_draw = new Point((int) (iwm + ihd2), (int) (iha));
        Point point4_draw = new Point((int) (iwm + ihd2), (int) (ihi));
        Point point6_draw = new Point((int) (iwm - ihd2), (int) (iha));
        Path path = new Path();
        path.moveTo(point1_draw.x, point1_draw.y);
        path.lineTo(point3_draw.x, point3_draw.y);
        path.moveTo(point4_draw.x, point4_draw.y);
        path.lineTo(point6_draw.x, point6_draw.y);
        return path;
    }

    private static Path drawnext(float iw, float ih){
        float ihi = miny * ih;
        float iha = maxy * ih;
        float ihm = (ihi + iha) * 0.5f;
        float ihd = (iha - ihi);
        float ihd2 = ihd * 0.5f;
        float ihd6 = ihd * 0.66f;
        float iwm = iw * 0.50f;
        float iwi = iwm - ihd2;
        float iwa = iwm + ihd2;
        Point point1_draw = new Point((int) (iwm - ihd), (int) (ihi));
        Point point2_draw = new Point((int) (iwm), (int) (ihm));
        Point point3_draw = new Point((int) (iwm - ihd), (int) (iha));
        Point point4_draw = new Point((int) (iwm), (int) (ihi));
        Point point5_draw = new Point((int) (iwm + ihd), (int) (ihm));
        Point point6_draw = new Point((int) (iwm), (int) (iha));
        Path path = new Path();
        path.moveTo(point1_draw.x, point1_draw.y);
        path.lineTo(point2_draw.x, point2_draw.y);
        path.lineTo(point3_draw.x, point3_draw.y);
        path.lineTo(point1_draw.x, point1_draw.y);
        path.moveTo(point4_draw.x, point4_draw.y);
        path.lineTo(point5_draw.x, point5_draw.y);
        path.lineTo(point6_draw.x, point6_draw.y);
        path.lineTo(point4_draw.x, point4_draw.y);
        path.close();
        return path;
    }

    private static Path drawprevious(float iw, float ih){
        float ihi = miny * ih;
        float iha = maxy * ih;
        float ihm = (ihi + iha) * 0.5f;
        float ihd = (iha - ihi);
        float ihd2 = ihd * 0.5f;
        float ihd6 = ihd * 0.66f;
        float iwm = iw * 0.50f;
        float iwi = iwm - ihd2;
        float iwa = iwm + ihd2;
        Point point1_draw = new Point((int) (iwm), (int) (ihi));
        Point point2_draw = new Point((int) (iwm - ihd), (int) (ihm));
        Point point3_draw = new Point((int) (iwm), (int) (iha));
        Point point4_draw = new Point((int) (iwm + ihd), (int) (ihi));
        Point point5_draw = new Point((int) (iwm), (int) (ihm));
        Point point6_draw = new Point((int) (iwm + ihd), (int) (iha));
        Path path = new Path();
        path.moveTo(point1_draw.x, point1_draw.y);
        path.lineTo(point2_draw.x, point2_draw.y);
        path.lineTo(point3_draw.x, point3_draw.y);
        path.lineTo(point1_draw.x, point1_draw.y);
        path.moveTo(point4_draw.x, point4_draw.y);
        path.lineTo(point5_draw.x, point5_draw.y);
        path.lineTo(point6_draw.x, point6_draw.y);
        path.lineTo(point4_draw.x, point4_draw.y);
        path.close();
        return path;
    }

    private static void drawpathes(Path path, int iw, int ih, Canvas canvas){
        float radius;
        BlurMaskFilter blurr;
        Paint paintbutton1, paintbutton2, paintbutton3;

        /*paintbutton1 = new Paint();
        paintbutton1.setColor(0x2f560087);
        paintbutton1.setStyle(Paint.Style.FILL);
        paintbutton1.setAntiAlias(true);
        paintbutton1.setStrokeJoin(Paint.Join.ROUND);
        paintbutton1.setStrokeCap(Paint.Cap.ROUND);

        canvas.drawPath(path, paintbutton1);*/

        paintbutton2 = new Paint();
        paintbutton2.setColor(0xff7f00c8);
        paintbutton2.setStyle(Paint.Style.STROKE);
        paintbutton2.setStrokeWidth(4.f);
        paintbutton2.setAntiAlias(true);
        paintbutton2.setStrokeJoin(Paint.Join.ROUND);
        paintbutton2.setStrokeCap(Paint.Cap.ROUND);
            /*radius = 2 * 4f;
            blurr = new BlurMaskFilter(radius, BlurMaskFilter.Blur.INNER);
            paintbutton2.setMaskFilter(blurr);*/

        //path.offset( -2.0f, -2.0f);
        //path = drawprevious(iw * 0.95f, ih * 0.95f);
        canvas.drawPath(path, paintbutton2);

        paintbutton3 = new Paint();
        paintbutton3.setColor(0xffcc00ff);
        paintbutton3.setStyle(Paint.Style.STROKE);
        paintbutton3.setStrokeWidth(4f);
        paintbutton3.setAntiAlias(true);
        paintbutton3.setStrokeJoin(Paint.Join.ROUND);
        paintbutton3.setStrokeCap(Paint.Cap.ROUND);
            /*radius = 2 * 4f;
            blurr = new BlurMaskFilter(radius, BlurMaskFilter.Blur.INNER);
            paintbutton3.setMaskFilter(blurr);*/

        path.offset( 3.0f, 3.0f);
        //path = drawprevious(iw * 1.05f, ih * 1.05f);
        canvas.drawPath(path, paintbutton3);

    }






























}

