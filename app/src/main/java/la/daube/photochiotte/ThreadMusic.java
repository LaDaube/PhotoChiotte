package la.daube.photochiotte;

import android.content.Context;
import android.support.v4.media.session.MediaSessionCompat;
import android.support.v4.media.session.PlaybackStateCompat;

import androidx.preference.PreferenceManager;
import androidx.room.Room;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ThreadMusic {
    private final static String TAG = "YYYmus";
    private final static int musicRandomMaxPickup = 200;
    private final static int musicRandomMaxPickupAlreadyPlayedRetry = 150;
    private final static float musicRandomFailedFolderMaxX = 1.25f;
    private final static int level1recountmax = 200;

    private volatile boolean optionaudioplayeractive = false;
    private volatile boolean optionaudioplayerrunning = false;
    private volatile ArrayList<int[]> optionaudioplayerplayinglist =  new ArrayList<>();
    private volatile int optionaudioplayerplayinglisti = -1;

    private int musiccollectionnamel = 0;
    private ArrayList<String> musiccollectionname = new ArrayList<>();
    private ArrayList<String> musiccollectionaddress = new ArrayList<>();

    public final Gallery model;
    private final Surf mysurf;
    private int currid = 0;
    private MPVLib mpvlib = null;

    private volatile String lastcommand = "next";

    private volatile boolean controlplay = false;
    private volatile boolean controlplaypause = false;
    private volatile boolean controlpause = false;
    private volatile boolean controlnext = false;
    private volatile boolean controlsearch = false;
    private volatile boolean controlprevious = false;
    private volatile boolean controlrandom = false;
    private volatile boolean controlrandomforce = false;
    private volatile boolean controllinearforce = false;
    private volatile boolean controlquit = false;
    private volatile boolean controlstop = false;

    private volatile String searchthis = null;
    private volatile int musiccollectionnamei = 0;

    private int eventfinishedfailedcurr = 0;
    private int eventfinishedfailedfoldermax = 0;
    private int eventfinishedfailedfolder = 0;
    private int level1ofirst = 0;
    private int level1ocount = 0;
    private int level1recount = 0;

    public volatile boolean optionaudioplayerrandom = true;
    public boolean ispaused() {
        if (mpvlib != null) {
            return mpvlib.ispaused;
        }
        return false;
    }
    public String getmetadataToPrettyName() {
        if (mpvlib != null) {
            String mapmetadata = mpvlib.metadataToPrettyName.replace('/', ' ');
            if (mapmetadata.length() > 0)
                return mapmetadata;
            else
                return null;
        }
        return null;
    }
    public String getname() {
        if (mpvlib != null) {
            String meta = mpvlib.streampath;
            if (Gallery.couldBeOnline(meta)) {
                try {
                    meta = URLDecoder.decode(mpvlib.streampath, "utf-8");
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            }
            //String meta = mpvlib.streampath.replaceAll("%[0-9a-fA-F]{2}", "");
            String mapmetadata = mpvlib.metadataToPrettyName;
            if (mapmetadata.length() > 0)
                meta += mapmetadata;
            return meta;
        }
        return null;
    }

    private MediaSessionCompat mediaSession;
    private final MediaSessionCompat.Callback mMediaSessionCallback = new MediaSessionCompat.Callback() {
        @Override
        public void onPlay() {
            super.onPlay();
            try {
                musicqueue.put("controlplay");
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            llog.d(TAG, "onPlay()");
        }

        @Override
        public void onPause() {
            super.onPause();
            try {
                musicqueue.put("controlpause");
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            llog.d(TAG, "onPause()");
        }

        @Override
        public void onStop() {
            super.onStop();
            try {
                musicqueue.put("controlquit");
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            llog.d(TAG, "onStop()");
        }

        @Override
        public void onSkipToNext() {
            super.onSkipToNext();
            try {
                musicqueue.put("controlnext");
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            llog.d(TAG, "onSkipToNext()");
        }

        @Override
        public void onFastForward() {
            super.onFastForward();
            try {
                musicqueue.put("controlnext");
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            llog.d(TAG, "onFastForward()");
        }

        @Override
        public void onSkipToPrevious() {
            super.onSkipToPrevious();
            try {
                musicqueue.put("controlprevious");
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            llog.d(TAG, "onSkipToPrevious()");
        }

        @Override
        public void onRewind() {
            super.onRewind();
            try {
                musicqueue.put("controlprevious");
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            llog.d(TAG, "onRewind()");
        }

        @Override
        public void onSetShuffleMode(int shuffleMode) {
            super.onSetShuffleMode(shuffleMode);
            try {
                musicqueue.put("controlrandom");
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            llog.d(TAG, "onSetShuffleMode()" + shuffleMode);
        }
    };


    public ThreadMusic(Context _context) {

    /*
           adb shell input keyevent 61 tab (menu)

        int i = KeyEvent.KEYCODE_MEDIA_NEXT;    // adb shell input keyevent 87
        i = KeyEvent.KEYCODE_MEDIA_PREVIOUS;    // adb shell input keyevent 88
        i = KeyEvent.KEYCODE_MEDIA_PLAY_PAUSE;  // adb shell input keyevent 85
        i = KeyEvent.KEYCODE_MEDIA_PLAY;        // adb shell input keyevent 126
        i = KeyEvent.KEYCODE_MEDIA_PAUSE;       // adb shell input keyevent 127
        i = KeyEvent.KEYCODE_MEDIA_STOP;        // adb shell input keyevent 86

     */
        model = new Gallery(null);
        mysurf = new Surf();
        mysurf.myid = 0;
        currid = 0;
        model.surf.add(mysurf);
        model.activitycontext = _context;

        llog.d(TAG, "+++++++++++++++++++++++ ThreadMusic");
        model.preferences = PreferenceManager.getDefaultSharedPreferences(_context);

        musiccollectionnamel = model.preferences.getInt("musiccollectionnamel", 0);

        if (musiccollectionnamel > 0) {

            model.dossiercache = model.preferences.getString("dossiercache", "/storage/dummy");
            model.dossierscreenshot = model.dossiercache + Gallery.staticdossierscreenshot;
            model.dossierdessin = model.dossiercache + Gallery.staticdossierdessin;
            model.dossiercollections = model.dossiercache + Gallery.staticdossiercollections;
            model.dossierconfig = model.dossiercache + Gallery.staticdossierconfig;
            model.dossierbitmaptemp = model.dossiercache + Gallery.staticdossierbitmaptemp;
            model.dossierminiature = model.dossiercache + Gallery.staticdossierminiature;
            model.internalStorageDir = model.preferences.getString("internalStorageDir", "/storage/dummy");

            musiccollectionnamei = model.preferences.getInt("musiccollectionnamei", 0);
            for (int i = 0 ; i < musiccollectionnamel ; i++) {
                musiccollectionname.add(model.preferences.getString("musiccollectionname" + i, null));
                musiccollectionaddress.add(model.preferences.getString("musiccollectionaddress" + i, null));
            }
            if (musiccollectionnamei >= musiccollectionnamel)
                musiccollectionnamei = musiccollectionnamel - 1;
            if (musiccollectionnamei < 0) {
                llog.d(TAG, "please create a music collection first");
            } else {
                model.db = Room.databaseBuilder(_context, AppDatabase.class, "database-name").fallbackToDestructiveMigration().build();
                llog.d(TAG, "playing collection " + musiccollectionname.get(musiccollectionnamei) + " " + model.dossiercollections);

                optionaudioplayeractive = true;
                if (mediaSession == null) {
                    mediaSession = new MediaSessionCompat(_context, _context.getPackageName());
                    mediaSession.setCallback(mMediaSessionCallback);
                    mediaSession.setFlags(MediaSessionCompat.FLAG_HANDLES_MEDIA_BUTTONS);
                    mediaSession.setActive(true);
                    PlaybackStateCompat state = new PlaybackStateCompat.Builder().setActions(
                        PlaybackStateCompat.ACTION_PLAY
                            | PlaybackStateCompat.ACTION_PLAY_PAUSE
                            | PlaybackStateCompat.ACTION_PAUSE
                            | PlaybackStateCompat.ACTION_STOP
                            | PlaybackStateCompat.ACTION_FAST_FORWARD
                            | PlaybackStateCompat.ACTION_REWIND
                            | PlaybackStateCompat.ACTION_SET_SHUFFLE_MODE
                            | PlaybackStateCompat.ACTION_SKIP_TO_NEXT
                            | PlaybackStateCompat.ACTION_SKIP_TO_PREVIOUS
                    ).build();
                    mediaSession.setPlaybackState(state);
                }
                if (!surveythreadrunning) {
                    llog.d(TAG, "starting surveythread");
                    surveythread.start();
                } else {
                    llog.d(TAG, "surveythread already running");
                }
            }
        }
    }

    public static final LinkedBlockingQueue<String> musicqueue = new LinkedBlockingQueue<>();
    public volatile boolean surveythreadrunning = false;
    private final Thread surveythread = new Thread(new Runnable() {
            @Override
            public void run() {
                surveythreadrunning = true;
                llog.d(TAG, "+++++survey thread starts");
                ByteBuffer buffer = ByteBuffer.allocateDirect(55 * 1024);

                musicalreadyplayed = model.preferences.getString("musicalreadyplayed", "");
                musicalreadyplayedl = 0;
                Pattern pattern = Pattern.compile("#(\\d+),(\\d+)");
                Matcher matcher = pattern.matcher(musicalreadyplayed);
                while (matcher.find()) {
                    int o = Integer.parseInt(matcher.group(1));
                    int m = Integer.parseInt(matcher.group(2));
                    optionaudioplayerplayinglist.add(new int[]{o, m});
                    if (optionaudioplayerplayinglisti < 0)
                        optionaudioplayerplayinglisti = 0;
                    optionaudioplayerplayinglisti += 1;
                    musicalreadyplayedl += 1;
                }
                llog.d(TAG, "musicalreadyplayed " + musicalreadyplayedl + " end of last session reloaded");

                optionaudioplayerrunning = true;
                boolean tryagain = !playmusicafterloadselection("next", false, false);
                String command = null;
                long pollsleeptime = 1000L;

                /*new Thread(new Runnable() {
                    @Override
                    public void run() {
                        int i = 0;
                        while (optionaudioplayeractive) {
                            try {
                                Thread.sleep(4000);
                                musicqueue.put("controlnext");
                                i += 1;
                                llog.d(TAG, "          - " + i);
                            } catch (InterruptedException e) {
                            }
                        }
                    }
                }).start();*/

                while (optionaudioplayeractive) {

                    try {
                        command = musicqueue.poll(pollsleeptime, TimeUnit.MILLISECONDS);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    pollsleeptime = 1000L;

                    if (command != null) {
                        llog.d(TAG, "new command received " + command);
                        if (command.equals("controlnext")) {
                            controlnext = true;
                        } else if (command.startsWith("controlsearch")) {
                            controlsearch = true;
                            searchthis = command.substring("controlsearch".length());
                        } else if (command.equals("controlprevious")) {
                            controlprevious = true;
                        } else if (command.equals("controlplaypause")) {
                            controlplaypause = true;
                        } else if (command.equals("controlplay")) {
                            controlplay = true;
                        } else if (command.equals("controlpause")) {
                            controlpause = true;
                        } else if (command.equals("controlvolume")) {
                            // this one needs the menu so we can change it here
                            int audiovolume = model.preferences.getInt("audiovolume", 100);
                            if (mpvlib != null)
                                mpvlib.setvolume(audiovolume);
                            else
                                llog.d(TAG, "mpvlib null");
                        } else if (command.equals("controlrandom")) {
                            controlrandom = true;
                        } else if (command.equals("controlrandomforce")) {
                            controlrandomforce = true;
                        } else if (command.equals("controllinearforce")) {
                            controllinearforce = true;
                        } else if (command.equals("controlstop")) {
                            controlstop = true;
                        } else if (command.equals("controlquit")) {
                            llog.d(TAG, "quitting music1 --------------------------------------");
                            controlquit = true;
                        }
                    }

                    if (controlnext) {
                        controlnext = false;
                        tryagain = !playmusicafterloadselection("next", false, false);
                        continue;
                    } else if (controlsearch) {
                        controlsearch = false;
                        tryagain = !playmusicafterloadselection("search", false, false);
                        continue;
                    } else if (controlprevious) {
                        controlprevious = false;
                        tryagain = !playmusicafterloadselection("previous", false, false);
                        continue;
                    } else if (controlplaypause) {
                        controlplaypause = false;
                        if (mpvlib != null)
                            mpvlib.cyclepause();
                        continue;
                    } else if (controlplay) { // sur le homescreen controlplay est détecté à la place de controlplaypause, mais pas si l'app est affichée
                        controlplay = false;
                        if (mpvlib != null)
                            mpvlib.cyclepause();
                        continue;
                    } else if (controlpause) {
                        controlpause = false;
                        if (mpvlib != null)
                            mpvlib.pause(true);
                        continue;
                    } else if (controlrandom) {
                        controlrandom = false;
                        optionaudioplayerrandom = !optionaudioplayerrandom;
                        continue;
                    } else if (controlrandomforce) {
                        controlrandomforce = false;
                        optionaudioplayerrandom = true;
                        continue;
                    } else if (controllinearforce) {
                        controllinearforce = false;
                        optionaudioplayerrandom = false;
                        continue;
                    } else if (controlstop) {
                        controlstop = false;
                        if (mpvlib != null) {
                            mpvlib.stop();
                            mpvlib.ispaused = false;
                            mpvlib.streampath = "-";
                            mpvlib.mapmetadata.clear();
                            mpvlib = null;
                        }
                        continue;
                    } else if (controlquit) {
                        controlquit = false;
                        optionaudioplayeractive = false;
                        model.preferences.edit().putBoolean("playmusic", optionaudioplayeractive).commit();
                        break;
                    }

                    if (mpvlib == null)
                        continue;
                    if (mpvlib.isshutdown)
                        continue;

                    if (tryagain) {
                        if (isonline != Media.online_no) {
                            if (!Gallery.isConnectedToInternet) {
                                llog.d(TAG, "no internet wait longer");
                                pollsleeptime = 10000L;
                                continue;
                            }
                        }
                        tryagain = !playmusicafterloadselection("next", false, false);
                        continue;
                    }

                    mpvlib.getdemuxercachestate();

                    ArrayList<Integer> rc = mpvlib.getEvents(buffer);
                    int rcl = rc.size();
                    if (rcl == 0)
                      continue;
                    else {
                        for (int i = 0; i < rcl; i++) {
                            int rcg = rc.get(i);
                            if (rcg == MPVLib.EVENT_END_FILE) {
                                llog.d(TAG, " +++++++ EVENT_END_FILE");
                                if (mpvlib.eventFinishedFailed > 0) {
                                    mpvlib.eventFinishedFailed = 0;
                                    llog.d(TAG, " +++++++ EVENT_END_FILE eventFinishedFailed redo " + lastcommand);
                                    tryagain = !playmusicafterloadselection(lastcommand, false, true);
                                } else {
                                    tryagain = !playmusicafterloadselection("next", false, false);
                                }
                                break;
                            } else if (rcg == MPVLib.EVENT_LOOK_LAST_PLAYED_POSITION) {
                                llog.d(TAG, " +++++++ EVENT_LOOK_LAST_PLAYED_POSITION");
                                if (mpvlib.jumptoposifbigaudiotrack) {
                                    if (18 * 60 < mpvlib.duration && mpvlib.duration < 3 * 60 * 60) {
                                        int rjumi = Gallery.rand.nextInt(6);
                                        int rjum = (int) ((mpvlib.duration * (float) rjumi) / 6.0f);
                                        llog.d(TAG, " been asked to jump to track pos " + rjum + " / " + mpvlib.duration);
                                        mpvlib.settimepos(rjum);
                                        mpvlib.jumptoposifbigaudiotrack = false;
                                    }
                                }
                            }
                        }
                    }

                }

                surveythreadrunning = false;
                llog.d(TAG, "-----------survey thread exited");

                llog.d(TAG, "quitting music2");
                optionaudioplayerrunning = false;
                if (mediaSession != null) {
                    mediaSession.release();
                    mediaSession = null;
                }
                playstop();
                buffer.clear();
                musicqueue.clear();
            }

        });

    private void playreinit(){
        if (mpvlib == null) {
            mpvlib = new MPVLib(19);
            mpvlib.setOptionString("ao", "opensles");
            mpvlib.setOptionString("eof", "yes");
            mpvlib.setOptionString("sub", "no");
            mpvlib.setOptionString("video", "no");
            mpvlib.setOptionString("cache", "yes");
            mpvlib.setOptionString("vo", "gpu");
            mpvlib.setOptionString("gpu-context", "android");
            mpvlib.setOptionString("opengl-es", "yes");
            mpvlib.setOptionString("hwdec", "no");
            mpvlib.setOptionString("input-default-bindings", "yes");
            mpvlib.setOptionString("save-position-on-quit", "no");
            mpvlib.setOptionString("force-window", "no");
            int audiovolume = model.preferences.getInt("audiovolume", (int) mpvlib.volume);
            mpvlib.setOptionString("volume", String.valueOf(audiovolume));

            mpvlib.init();

            mpvlib.observeProperty("time-pos", MPVLib.mpvFormat.MPV_FORMAT_INT64);
            mpvlib.observeProperty("duration", MPVLib.mpvFormat.MPV_FORMAT_INT64);
            mpvlib.observeProperty("pause", MPVLib.mpvFormat.MPV_FORMAT_FLAG);
            mpvlib.observeProperty("track-list", MPVLib.mpvFormat.MPV_FORMAT_STRING);
            mpvlib.observeProperty("playlist-pos", MPVLib.mpvFormat.MPV_FORMAT_STRING);
            mpvlib.observeProperty("playlist-count", MPVLib.mpvFormat.MPV_FORMAT_STRING);
            mpvlib.observeProperty("volume", MPVLib.mpvFormat.MPV_FORMAT_STRING);
            mpvlib.observeProperty("metadata", MPVLib.mpvFormat.MPV_FORMAT_STRING);
            mpvlib.observeProperty("stream-path", MPVLib.mpvFormat.MPV_FORMAT_STRING);
        }
    }

    private void playstop(){
        if (mpvlib != null) {
            mpvlib.stop();
            mpvlib.ispaused = false;
            mpvlib.streampath = "-";
            mpvlib.mapmetadata.clear();
            mpvlib = null;
        }
    }

    private int isonline = Media.online_no;
    private boolean playmusicafterloadselection(String commande, boolean append, boolean eventfinishedfailed){
        boolean success = false;
        if (!eventfinishedfailed) {
            eventfinishedfailedcurr = 0;
            level1ocount = 0;
            level1recount = 0;
        } else
            eventfinishedfailedcurr += 1;
        if (mpvlib == null) {
            playreinit();
        } else {
            mpvlib.ispaused = false;
            mpvlib.streampath = "-";
            mpvlib.mapmetadata.clear();
        }
        if (musiccollectionnamel == 0) {
            llog.d(TAG, "pmalc musiccollectionname == null");
            controlquit = true;
            return success;
        }
        if (musiccollectionnamei >= musiccollectionnamel)
            musiccollectionnamei = 0;
        if (model.folderCount <= 0) {
            model.collections.add(new Collection(musiccollectionaddress.get(musiccollectionnamei), musiccollectionname.get(musiccollectionnamei), model));
            int posstart = model.collectionsToDisplay(musiccollectionaddress.get(musiccollectionnamei), 0);
            if (posstart < 0 || model.folderCount <= 0) {
                llog.d(TAG, "pmalc collection == null " + musiccollectionaddress.get(musiccollectionnamei) + " i " + musiccollectionnamei);
                controlquit = true;
                return success;
            }
        }
        //llog.d(TAG, "pmalc " + model.folderCount);
        mpvlib.jumptoposifbigaudiotrack = false;
        boolean gotsomething = false;
        if (searchthis != null) {
            llog.d(TAG, "pmalc play search music <" + searchthis + ">");
            Pattern patterndictfromlist = Pattern.compile(searchthis, Pattern.CASE_INSENSITIVE);
            int[] trouvedans = model.findFolder(patterndictfromlist, mysurf.ordnerIndex, mysurf.mediaIndex);
            if (trouvedans[0] == -1) {
              llog.d(TAG, "pmalc did not find it");
            } else {
              model.changeBigPicture(currid, trouvedans[0], 0, trouvedans[1], 0, true, true);
              addtoplayed(mysurf.ordnerIndex, mysurf.mediaIndex);
              gotsomething = true;
              llog.d(TAG, "pmalc found " + mysurf.mediaIndexAddress);
            }
            searchthis = null;
        } else if (!optionaudioplayerrandom) {
            if (commande.equals("previous")) {
                for (int i = 0 ; i < 100 && !gotsomething ; i++) {
                    model.changeBigPicture(currid, -1, 0, -1, -1, true, true);
                    if (Media.typeAcceptedForMusic(mysurf.mediaIndexAddress))
                        gotsomething = true;
                }
                llog.d(TAG, "pmalc play linear previous " + mysurf.mediaIndexAddress);
            } else if (commande.equals("next")) {
                for (int i = 0 ; i < 100 && !gotsomething ; i++) {
                    model.changeBigPicture(currid, -1, 0, -1, +1, true, true);
                    if (Media.typeAcceptedForMusic(mysurf.mediaIndexAddress))
                        gotsomething = true;
                }
                llog.d(TAG, "pmalc play linear next " + mysurf.mediaIndexAddress);
            }
        } else {
            int listsize = optionaudioplayerplayinglist.size();
            if (commande.equals("previous") && listsize > 1) {
                //int[] prev = optionaudioplayerplayinglist.get(listsize - 2);
                if (optionaudioplayerplayinglisti - 1 >= 0 && optionaudioplayerplayinglisti - 1 < listsize) {
                    optionaudioplayerplayinglisti -= 1;
                    int[] prev = optionaudioplayerplayinglist.get(optionaudioplayerplayinglisti);
                    model.changeBigPicture(currid, prev[0], 0, prev[1], 0, true, true);
                    llog.d(TAG, "pmalc play random previous " + mysurf.mediaIndexAddress);
                    //optionaudioplayerplayinglist.remove(listsize - 1);
                    gotsomething = true;
                }
            } else if (listsize > 1 && optionaudioplayerplayinglisti >= 0 && optionaudioplayerplayinglisti < listsize - 1) {
                optionaudioplayerplayinglisti += 1;
                int[] prev = optionaudioplayerplayinglist.get(optionaudioplayerplayinglisti);
                model.changeBigPicture(currid, prev[0], 0, prev[1], 0, true, true);
                llog.d(TAG, "pmalc play random next " + mysurf.mediaIndexAddress);
                //optionaudioplayerplayinglist.remove(listsize - 1);
                gotsomething = true;
            } else {
                model.videoaudioweightroot1 = model.preferences.getBoolean("videoaudioweightroot1", model.videoaudioweightroot1);
                model.videoaudioweightlevel1 = model.preferences.getBoolean("videoaudioweightlevel1", model.videoaudioweightlevel1);
                model.musicrandminreplay = model.preferences.getInt("musicrandminreplay", model.musicrandminreplay);
                int i = 0;
                int ireloop = 0;
                final int imaxreloop = 5;
                while (i < musicRandomMaxPickup && !gotsomething && ireloop < imaxreloop) {
                    int d = -1, fl = -1, f = -1;
                    if (eventfinishedfailed
                        && eventfinishedfailedcurr < eventfinishedfailedfoldermax * musicRandomFailedFolderMaxX
                        && model.folderCount > 0
                        && eventfinishedfailedfolder >= 0
                        && eventfinishedfailedfolder < model.folderCount) {
                        d = eventfinishedfailedfolder;
                        llog.d(TAG, "pmalc eventfinishedfailed try again folder " + d + " " + eventfinishedfailedcurr + "<1.25*" + eventfinishedfailedfoldermax);

                    } else if (!model.videoaudioweightroot1 && model.videoaudioweightlevel1 && model.folderCount > 0) {
                        if (eventfinishedfailed && level1recount < level1recountmax && level1ocount > 0) {
                            int k = Gallery.rand.nextInt(level1ocount);
                            d = level1ofirst + k;
                            llog.d(TAG, "pmalc eventfinishedfailed level1recount " + level1recount + " try again folder " + level1ofirst + "<=" + d + "<=+" + level1ocount);
                            level1recount += 1;

                        } else {
                            // root folder has a bigger weight
                            fl = model.getFolderFileCount(0);
                            // squash first folder level
                            ArrayList<Integer> o = model.getOrdnersFirstLevel();
                            int tl = fl + o.size() - 1;
                            int od = Gallery.rand.nextInt(tl);
                            if (od < fl) {
                                f = od;
                                d = 0;
                                level1ocount = 0;
                            } else {
                                tl = o.size();
                                od = od - fl + 1;
                                int firstordner = o.get(od);
                                int lastordner;
                                if (od == tl - 1)
                                    lastordner = model.folderCount;
                                else
                                    lastordner = o.get(od + 1);
                                int count = lastordner - firstordner;
                                level1ofirst = firstordner;
                                level1ocount = count;
                                int k = Gallery.rand.nextInt(count);
                                d = firstordner + k;
                            }
                        }

                    } else if (!model.videoaudioweightroot1 && model.folderCount > 0) {
                        // root folder has a bigger weight
                        fl = model.getFolderFileCount(0);
                        int tl = fl + model.folderCount - 1;
                        int od = Gallery.rand.nextInt(tl);
                        if (od < fl) {
                            f = od;
                            d = 0;
                        } else {
                            d = od - fl + 1;
                        }

                    } else if (model.videoaudioweightlevel1 && model.folderCount > 0) {
                        if (eventfinishedfailed && level1recount < level1recountmax && level1ocount > 0) {
                            int k = Gallery.rand.nextInt(level1ocount);
                            d = level1ofirst + k;
                            llog.d(TAG, "pmalc eventfinishedfailed level1recount " + level1recount + " try again folder " + level1ofirst + "<=" + d + "<=+" + level1ocount);
                            level1recount += 1;

                        } else {
                            // squash first folder level
                            ArrayList<Integer> o = model.getOrdnersFirstLevel();
                            int tl = o.size();
                            int od = Gallery.rand.nextInt(tl);
                            int firstordner = o.get(od);
                            int lastordner;
                            if (od == tl - 1)
                                lastordner = model.folderCount;
                            else
                                lastordner = o.get(od + 1);
                            int count = lastordner - firstordner;
                            level1ofirst = firstordner;
                            level1ocount = count;
                            int k = Gallery.rand.nextInt(count);
                            d = firstordner + k;
                            //llog.d(TAG, "pmalc squash first folder level rand " + od + " / " + tl + " : " + firstordner + " to " + lastordner + " = count " + count + " + rand " + k + " = " + d);
                        }

                    } else {
                        d = Gallery.rand.nextInt(model.folderCount);
                    }
                    if (d != -1) {
                        if (f == -1) {
                            fl = model.getFolderFileCount(d);
                            if (fl <= 0) {
                                llog.d(TAG, "pmalc getFolderFileCount 0 for " + d);
                            } else {
                                f = Gallery.rand.nextInt(fl);
                            }
                        }
                        if (f != -1) {
                            eventfinishedfailedfolder = d;
                            eventfinishedfailedfoldermax = fl;
                            model.changeBigPicture(currid, d, 0, f, 0, true, true);
                            int pl = optionaudioplayerplayinglist.size();
                            int mr = model.musicrandminreplay;
                            int alreadyPlayedAtPosition = 0;
                            if (mr > 0) {
                                int[] pi;
                                for (int j = pl - 1; j >= pl - 1 - mr && j >= 0 && i < musicRandomMaxPickupAlreadyPlayedRetry; j--) {
                                    pi = optionaudioplayerplayinglist.get(j);
                                    if (pi[0] == mysurf.ordnerIndex && pi[1] == mysurf.mediaIndex) {
                                        alreadyPlayedAtPosition = pl - j;
                                        float repeat = (float) alreadyPlayedAtPosition / (float) mr;
                                        repeat *= repeat;
                                        repeat *= repeat;
                                        repeat *= repeat;
                                        float stillaccept = Gallery.rand.nextFloat();
                                        if (stillaccept < repeat) {
                                            llog.d(TAG, "pmalc " + i + " already played " + pi[0] + "," + pi[1] + " stillaccept " + stillaccept + "/" + repeat + " alreadyplayed at pos " + alreadyPlayedAtPosition + " / " + mr);
                                            alreadyPlayedAtPosition = 0;
                                        } else {
                                            llog.d(TAG, "pmalc " + i + " already played " + pi[0] + "," + pi[1] + " refused " + stillaccept + "/" + repeat + " alreadyplayed at pos " + alreadyPlayedAtPosition + " / " + mr);
                                        }
                                        break;
                                    }
                                }
                            }
                            if (i >= musicRandomMaxPickupAlreadyPlayedRetry) {
                                ireloop += 1;
                                llog.d(TAG, "pmalc " + i + " too much change level 1 folder ireloop " + ireloop + "/" + imaxreloop);
                                eventfinishedfailed = false;
                                level1ocount = 0;
                                level1recount = 0;
                                i = 0;
                            } else if (alreadyPlayedAtPosition == 0 && Media.typeAcceptedForMusic(mysurf.mediaIndexAddress)) {
                                addtoplayed(mysurf.ordnerIndex, mysurf.mediaIndex);
                                mpvlib.jumptoposifbigaudiotrack = true;
                                gotsomething = true;
                                eventfinishedfailed = false;
                                llog.d(TAG, "pmalc play random next " + mysurf.mediaIndexAddress);
                            } else {
                                if (alreadyPlayedAtPosition == 0)
                                    llog.d(TAG, "pmalc play random next refused typeAcceptedForMusic " + mysurf.mediaIndexAddress);
                                eventfinishedfailedcurr += 1;
                                eventfinishedfailed = true;
                            }
                        }
                    }
                    i++;
                }
            }
        }
        for (int k = 0 ; k < 5 && !gotsomething ; k++) {
            llog.d(TAG, "pmalc ERRORSHOULDNTHAPPEN !gotsomething " + k + " error !gotsomething play " + optionaudioplayerplayinglist.size() + " / " + model.folderCount + " " + mysurf.ordnerIndex + " " + mysurf.mediaIndex);
            int d = Gallery.rand.nextInt(model.folderCount);
            int fl = model.getFolderFileCount(d);
            if (fl <= 0) {
                llog.d(TAG, "pmalc ERRORSHOULDNTHAPPEN !gotsomething getFolderFileCount 0 for " + d);
            } else {
                eventfinishedfailedfolder = d;
                eventfinishedfailedfoldermax = fl;
                int f = Gallery.rand.nextInt(fl);
                model.changeBigPicture(currid, d, 0, f, 0, true, true);
                addtoplayed(mysurf.ordnerIndex, mysurf.mediaIndex);
                mpvlib.jumptoposifbigaudiotrack = true;
                gotsomething = true;
                llog.d(TAG, "pmalc ERRORSHOULDNTHAPPEN !gotsomething play random next " + mysurf.mediaIndexAddress);
            }
        }
        if (mysurf.mediaIndexAddress == null) {
            llog.d(TAG, "pmalc ERRORSHOULDNTHAPPEN error null play " + optionaudioplayerplayinglist.size() + " / " + model.folderCount + " " + mysurf.ordnerIndex + " " + mysurf.mediaIndex);
            return false;
        }
        isonline = model.getMediaIsOnline(mysurf.ordnerIndex, mysurf.mediaIndex);
        llog.d(TAG, "pmalc " + mysurf.ordnerIndex + ", " + mysurf.mediaIndex + " : play " + optionaudioplayerplayinglist.size() + " / " + model.folderCount + " " + mysurf.mediaIndexAddress);
        mpvlib.loadfile(mysurf.mediaIndexAddress, append);
        success = true;
        lastcommand = commande;

        /*new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(1000);
                    musicqueue.put("controlnext");
                } catch (InterruptedException e) {
                }
            }
        }).start();*/

        return success;
    }

    private volatile String musicalreadyplayed = "";
    private volatile int musicalreadyplayedl = 0;
    private void addtoplayed(int ordner, int media) {

        optionaudioplayerplayinglisti = optionaudioplayerplayinglist.size();
        optionaudioplayerplayinglist.add(new int[]{ordner, media});

        model.musicrandminreplay = model.preferences.getInt("musicrandminreplay", model.musicrandminreplay);
        if (model.musicrandminreplay > 0 || musicalreadyplayedl > model.musicrandminreplay) {
            musicalreadyplayed += "#" + ordner + "," + media;
            musicalreadyplayedl += 1;

            while (musicalreadyplayedl > model.musicrandminreplay) {
                int first = musicalreadyplayed.indexOf("#");
                if (first < 0) {
                    musicalreadyplayed = "";
                    musicalreadyplayedl = 0;
                    break;
                }
                int next = musicalreadyplayed.indexOf("#", 1);
                if (next < 0) {
                    musicalreadyplayed = "";
                    musicalreadyplayedl = 0;
                    break;
                }
                musicalreadyplayed = musicalreadyplayed.substring(next);
                musicalreadyplayedl -= 1;
            }

            model.preferences.edit().putString("musicalreadyplayed", musicalreadyplayed).apply();
        }

        if (Gallery.debugi >= 1) {
            if (model.oo == null) {
                String mmusicalreadyplayed = model.preferences.getString("musicalreadyplayed", "");
                Pattern pattern = Pattern.compile("#(\\d+),(\\d+)");
                Matcher matcher = pattern.matcher(mmusicalreadyplayed);
                while (matcher.find()) {
                    int o = Integer.parseInt(matcher.group(1));
                    int m = Integer.parseInt(matcher.group(2));
                    model.savePlayCountToFile(model.dossierminiature, o, m, null);
                }
            }
            model.savePlayCountToFile(model.dossierminiature, mysurf.ordnerIndex, mysurf.mediaIndex, mysurf.mediaIndexAddress);
        }

    }

}



































































