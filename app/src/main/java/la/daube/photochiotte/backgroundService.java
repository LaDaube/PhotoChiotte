package la.daube.photochiotte;

import static androidx.activity.result.ActivityResultCallerKt.registerForActivityResult;

import android.Manifest;
import android.app.AlertDialog;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.appwidget.AppWidgetManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.IBinder;
import android.text.InputType;
import android.widget.EditText;
import android.widget.RemoteViews;

import androidx.core.app.NotificationCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.preference.PreferenceManager;

import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

public class backgroundService extends Service {
    public static final String TAG = "YYYsvc";

    final int REQUEST_CODE = 3000;
    final int NOTIFICATION_ID = 3000;
    final String CHANNEL_ID = "ForegroundService_ID";
    final String CHANNEL_NAME = "ForegroundService Channel";
    private volatile String notificationText = "Discover";
    public void updatenotification(String text){
        llog.d(TAG, "Service updatenotification : " + text );
        // Layouts for the custom notification
        final Intent notificationIntent = new Intent(this, backgroundService.class);
        RemoteViews notificationLayout = new RemoteViews(getPackageName(), R.layout.notification_collapsed);
        //RemoteViews notificationLayoutExpanded = new RemoteViews(getPackageName(), R.layout.notification_expanded);

        int flags;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.S) {
            flags = PendingIntent.FLAG_MUTABLE | PendingIntent.FLAG_UPDATE_CURRENT;
        } else {
            flags = PendingIntent.FLAG_UPDATE_CURRENT;
        }

        final NotificationCompat.Builder mNotificationBuilder = new NotificationCompat.Builder(this, CHANNEL_ID);
        mNotificationBuilder
                .setOngoing(true)
                .setContentTitle(notificationText) //.setContentText(ContentText) //.setStyle(new NotificationCompat.BigTextStyle().bigText(ContentText))
                .setStyle(new NotificationCompat.DecoratedCustomViewStyle())
                .setTicker("discover service started")
                .setColor(0xff7f00c8)
                .setSmallIcon(R.drawable.xinvert) //.setLargeIcon(Bitmap.createScaledBitmap(BitmapFactory.decodeResource(getResources(), R.drawable.xinvert), 128, 128, true))
                .setCustomContentView(notificationLayout) //.setCustomBigContentView(notificationLayoutExpanded)
                .setAutoCancel(false);
        final PendingIntent pendingIntent = PendingIntent.getActivity(this, REQUEST_CODE, notificationIntent,  flags);

        Widget.drawwidget(notificationLayout, this, threadDiscover, threadmusic);

        mNotificationBuilder.setContentIntent(pendingIntent);
        startForeground(NOTIFICATION_ID, mNotificationBuilder.build());

        Intent intent = new Intent(this, Widget.class);
        intent.setAction(AppWidgetManager.ACTION_APPWIDGET_UPDATE);
        // Use an array and EXTRA_APPWIDGET_IDS instead of AppWidgetManager.EXTRA_APPWIDGET_ID,
        // since it seems the onUpdate() is only fired on that:
        AppWidgetManager widgetManager = AppWidgetManager.getInstance(this);
        int[] ids = widgetManager.getAppWidgetIds(new ComponentName(this, Widget.class));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB)
            widgetManager.notifyAppWidgetViewDataChanged(ids, android.R.id.list);
        intent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS, ids);
        this.sendBroadcast(intent);

    }

    public static backgroundService sInstance = null;
    public static backgroundService getInstance(){
        return sInstance;
    }
    public void stopService(){
        if (sInstance != null) {
            sInstance.stopSelf();
            sInstance = null;
        }
        llog.d(TAG, "--------------------------------stopService------------------------------------");
    }
    public int counter = 0;
    private final LinkedBlockingQueue<String> servicequeue = new LinkedBlockingQueue<>();
    private SharedPreferences preferences = null;
    public ThreadMusic threadmusic = null;
    public ThreadDiscover threadDiscover = null;


    public final static String REQUEST = "notif";
    public final static int REQUEST_PREVIOUS = 0;
    public final static int REQUEST_PAUSE = 1;
    public final static int REQUEST_PLAY = 2;
    public final static int REQUEST_NEXT = 3;
    public final static int REQUEST_RANDOM = 4;
    public final static int REQUEST_QUIT = 5;
    public final static int REQUEST_SEARCH = 6;
    public final static int REQUEST_STARTMUSIC = 7; // achtung
    public final static int REQUEST_STARTTOR = 8;
    public final static int REQUEST_STOPMUSIC = 9;
    public final static int REQUEST_STOPTOR = 10;
    public final static int REQUEST_DQUIT = 11;
    public static boolean musicstatusrandom = true;
    public static boolean musicstatuspaused = false;
    public static String torpubkey = "-";
    public static boolean torstatus = false;
    public static boolean STATUS_GET_PEERS_LAST = false;
    public static int STATUS_GET_PEERS_FAILED = 0;
    public static int STATUS_GET_PEERS_SUCCESS = 0;
    public static boolean STATUS_FIND_OUR_POSITION_LAST = false;
    public static int STATUS_FIND_OUR_POSITION_FAILED = 0;
    public static int STATUS_FIND_OUR_POSITION_SUCCESS = 0;
    public static boolean STATUS_ANNOUNCE_PEER_LAST = false;
    public static int STATUS_ANNOUNCE_PEER_FAILED = 0;
    public static int STATUS_ANNOUNCE_PEER_SUCCESS = 0;
    public static boolean STATUS_DOWNLOAD_LAST = false;
    public static int STATUS_DOWNLOAD_FAILED = 0;
    public static int STATUS_DOWNLOAD_SUCCESS = 0;
    public static int STATUS_SEARCH_KEYWORDS_FOUND = 0;
    public static int STATUS_CLIENTSL = 0;
    public static int STATUS_RANGESL = 0;
    public static int STATUS_HASHESL = 0;
    public static String musicstatusname = null;

    @Override
    public void onCreate() {
        super.onCreate();
        counter += 11;
        llog.d(TAG, "+++++++++++++++++++++++ onCreate service counter " + counter);

        preferences = PreferenceManager.getDefaultSharedPreferences(this);

        startnotifthreads();

    }

    public boolean musicpause(){
        return musichandle("controlpause");
    }
    public boolean musicplay(){
        return musichandle("controlplay");
    }
    public boolean musicplaypause(){
        return musichandle("controlplaypause");
    }
    public boolean musicprevious(){
        return musichandle("controlprevious");
    }
    public boolean musicvolume(){
        return musichandle("controlvolume");
    }
    public boolean musicrandom(){
        return musichandle("controlrandom");
    }
    public boolean musicrandomforce(){
        return musichandle("controlrandomforce");
    }
    public boolean musiclinearforce(){
        return musichandle("controllinearforce");
    }
    public boolean musicnext(){
        return musichandle("controlnext");
    }
    public boolean musicsearch(String search){
        return musichandle("controlsearch" + search);
    }
    public boolean musicstop(){
        return musichandle("controlstop");
    }
    public boolean musichandle(String command){
        if (threadmusic != null) {
            try {
                ThreadMusic.musicqueue.put(command);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            try {
                servicequeue.put("updatedelay");
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return true;
        }
        llog.d(TAG, "error thread music null");
        return false;
    }
    public boolean torsearch(String keywords){
        STATUS_SEARCH_KEYWORDS_FOUND = 0;
        String dossiercollections = preferences.getString("dossiercache", "") + Gallery.staticdossiercollections;
        return torhandle(new String[]{"search", keywords, dossiercollections});
    }
    public boolean torsearchfound(){
        if (STATUS_SEARCH_KEYWORDS_FOUND == 3) {
            if (threadDiscover != null)
                threadDiscover.retrieveSearchResults();
            STATUS_SEARCH_KEYWORDS_FOUND = 0;
            return true;
        }
        return false;
    }
    public boolean torhandle(String[] command){
        if (threadDiscover != null) {
            try {
                threadDiscover.torqueue.put(command);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            try {
                servicequeue.put("updatedelay");
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return true;
        }
        llog.d(TAG, "error thread tor null");
        return false;
    }
    public boolean musicquit(){
        if (threadmusic != null) {
            stopmusic();
            try {
                servicequeue.put("updatedelay");
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return true;
        }
        llog.d(TAG, "error thread music null");
        return false;
    }
    public boolean torquit(){
        if (threadDiscover != null) {
            stoptor();
            try {
                servicequeue.put("updatedelay");
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return true;
        }
        llog.d(TAG, "error thread tor null");
        return false;
    }


    private void startnotifthreads() {

        boolean optionaudioplayeractive = preferences.getBoolean("playmusic", false);
        boolean ThreadDiscoverActive = preferences.getBoolean("ThreadDiscoverActive", false);
        if ((optionaudioplayeractive && threadmusic == null) || (ThreadDiscoverActive && threadDiscover == null)) {

            sInstance = this;

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (checkSelfPermission(Manifest.permission.POST_NOTIFICATIONS) != PackageManager.PERMISSION_GRANTED) {
                    llog.d(TAG, " -------------------- > checkSelfPermission(Manifest.permission.POST_NOTIFICATIONS) != PackageManager.PERMISSION_GRANTED");
                    Intent intent = new Intent();
                    intent.setAction(Gallery.broadcastname);
                    intent.putExtra("goal", "asknotificationpermission");
                    LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
                } else {
                    llog.d(TAG, " -------------------- > checkSelfPermission(Manifest.permission.POST_NOTIFICATIONS) == PackageManager.PERMISSION_GRANTED");
                }
            }

            final NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                final NotificationChannel channel = new NotificationChannel(CHANNEL_ID, CHANNEL_NAME, NotificationManager.IMPORTANCE_LOW);
                channel.setShowBadge(false);
                channel.setImportance(NotificationManager.IMPORTANCE_LOW);
                mNotificationManager.createNotificationChannel(channel);
            }

            if (optionaudioplayeractive) {
                llog.d(TAG, "optionaudioplayeractive " + optionaudioplayeractive);
                startmusic();
            }
            if (ThreadDiscoverActive) {
                llog.d(TAG, "ThreadDiscoverActive " + optionaudioplayeractive);
                starttor();
            }

            if (!surveythreadrunning) {
                llog.d(TAG, "++++++++++launch surveythread");
                surveythread.start();
            } else {
                llog.d(TAG, "==========surveythread already running");
            }

            updatenotification("startnotifthreads()");



        } else {
            stopService();
        }

    }

    private volatile boolean surveythreadrunning = false;
    private final Thread surveythread = new Thread(new Runnable() {
            @Override
            public void run() {
                surveythreadrunning = true;
                llog.d(TAG, "+++++survey thread starts");
                String command = null;

                while (sInstance != null) {

                    try {
                        command = servicequeue.poll(3000, TimeUnit.MILLISECONDS);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    boolean updatenotif = false;

                    if (command != null) {
                        updatenotif = true;
                        if (command.equals("updatedelay")) {
                            try {
                                Thread.sleep(500);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                        }
                    }

                    notificationText = "Discover";

                    if (threadmusic != null) {

                        String name = threadmusic.getname();
                        if (name != null) {
                            if (musicstatusname == null)
                                updatenotif = true;
                            else if (!name.equals(musicstatusname))
                                updatenotif = true;
                            musicstatusname = name;
                            String metadatePrettyName = threadmusic.getmetadataToPrettyName();
                            if (metadatePrettyName != null)
                                notificationText = metadatePrettyName;
                        } else {
                            if (musicstatusname == null) {
                                updatenotif = true;
                                musicstatusname = "-";
                            } else if (!musicstatusname.equals("-")) {
                                updatenotif = true;
                                musicstatusname = "-";
                            }
                        }
                        boolean random = threadmusic.optionaudioplayerrandom;
                        if (random != musicstatusrandom)
                            updatenotif = true;
                        musicstatusrandom = random;
                        boolean paused = threadmusic.ispaused();
                        if (paused != musicstatuspaused)
                            updatenotif = true;
                        musicstatuspaused = paused;

                    }

                    if (threadDiscover != null) {

                        String hn = threadDiscover.PUBKEY;
                        if (hn != null && torpubkey != null) {
                            if (!torpubkey.equals(hn))
                                updatenotif = true;
                        } else if (hn != null)
                            updatenotif = true;
                        torpubkey = hn;
                        boolean status = threadDiscover.surveythreadrunning;
                        if (status != torstatus)
                            updatenotif = true;
                        torstatus = status;

                        int temp = 0;

                        temp = threadDiscover.STATUS_GET_PEERS_FAILED;
                        if (STATUS_GET_PEERS_FAILED != temp) {
                            STATUS_GET_PEERS_LAST = false;
                            updatenotif = true;
                        }
                        STATUS_GET_PEERS_FAILED = temp;
                        temp = threadDiscover.STATUS_GET_PEERS_SUCCESS;
                        if (STATUS_GET_PEERS_SUCCESS != temp) {
                            STATUS_GET_PEERS_LAST = true;
                            updatenotif = true;
                        }
                        STATUS_GET_PEERS_SUCCESS = temp;

                        temp = threadDiscover.STATUS_FIND_OUR_POSITION_FAILED;
                        if (STATUS_FIND_OUR_POSITION_FAILED != temp) {
                            STATUS_FIND_OUR_POSITION_LAST = false;
                            updatenotif = true;
                        }
                        STATUS_FIND_OUR_POSITION_FAILED = temp;
                        temp = threadDiscover.STATUS_FIND_OUR_POSITION_SUCCESS;
                        if (STATUS_FIND_OUR_POSITION_SUCCESS != temp) {
                            STATUS_FIND_OUR_POSITION_LAST = true;
                            updatenotif = true;
                        }
                        STATUS_FIND_OUR_POSITION_SUCCESS = temp;

                        temp = threadDiscover.STATUS_ANNOUNCE_PEER_FAILED;
                        if (STATUS_ANNOUNCE_PEER_FAILED != temp) {
                            STATUS_ANNOUNCE_PEER_LAST = false;
                            updatenotif = true;
                        }
                        STATUS_ANNOUNCE_PEER_FAILED = temp;
                        temp = threadDiscover.STATUS_ANNOUNCE_PEER_SUCCESS;
                        if (STATUS_ANNOUNCE_PEER_SUCCESS != temp) {
                            STATUS_ANNOUNCE_PEER_LAST = true;
                            updatenotif = true;
                        }
                        STATUS_ANNOUNCE_PEER_SUCCESS = temp;

                        temp = threadDiscover.STATUS_DOWNLOAD_FAILED;
                        if (STATUS_DOWNLOAD_FAILED != temp) {
                            STATUS_DOWNLOAD_LAST = false;
                            updatenotif = true;
                        }
                        STATUS_DOWNLOAD_FAILED = temp;
                        temp = threadDiscover.STATUS_DOWNLOAD_SUCCESS;
                        if (STATUS_DOWNLOAD_SUCCESS != temp) {
                            STATUS_DOWNLOAD_LAST = true;
                            updatenotif = true;
                        }
                        STATUS_DOWNLOAD_SUCCESS = temp;

                        STATUS_SEARCH_KEYWORDS_FOUND = threadDiscover.STATUS_SEARCH_KEYWORDS;
                        STATUS_CLIENTSL = threadDiscover.STATUS_CLIENTSL;
                        STATUS_RANGESL = threadDiscover.STATUS_RANGESL;
                        STATUS_HASHESL = threadDiscover.STATUS_HASHESL;

                    }
                    if (updatenotif) {
                        updatenotification("updatenotif : " + command);
                    }
                }
                surveythreadrunning = false;
                llog.d(TAG, "-----------survey thread exited");
            }
        });

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        llog.d(TAG, "++++++++++++++++++++++++++++++onStartCommand service startId " + startId);
        if (intent != null) {
            if (intent.hasExtra("BootDeviceReceiver")) {
                llog.d(TAG, "optionautostartboot BootDeviceReceiver");
                boolean optionautostartboot = preferences.getBoolean("optionautostartboot", false);
                if (optionautostartboot) {
                    llog.d(TAG, "optionautostartboot " + optionautostartboot + " " + BuildConfig.APPLICATION_ID);
                    Intent intent2 = getPackageManager().getLaunchIntentForPackage(BuildConfig.APPLICATION_ID);
                    if (intent2 == null)
                        llog.d(TAG, "activity not found");
                    else {
                        llog.d(TAG, "start on boot");
                        intent2.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent2);
                    }
                }
            }
            String action = intent.getAction();
            if (action != null) {
                llog.d(TAG, "onStartCommand startId " + startId + " action = " + intent.getAction());
                if (intent.getData() == null)
                    llog.d(TAG, "data null");
                if (intent.getExtras() == null)
                    llog.d(TAG, "extras null");
                if (action.equals(REQUEST + REQUEST_PREVIOUS)) {
                    musicprevious();
                } else if (action.equals(REQUEST + REQUEST_NEXT)) {
                    musicnext();
                } else if (action.equals(REQUEST + REQUEST_PLAY)) {
                    musicplaypause();
                } else if (action.equals(REQUEST + REQUEST_PAUSE)) {
                    musicplaypause();
                } else if (action.equals(REQUEST + REQUEST_QUIT)) {
                    if (threadmusic != null) {
                        preferences.edit().putBoolean("playmusic", false).commit();
                        musicquit();
                    } else {
                        preferences.edit().putBoolean("playmusic", true).commit();
                        startnotifthreads();
                    }
                } else if (action.equals(REQUEST + REQUEST_DQUIT)) {
                    if (threadDiscover != null) {
                        preferences.edit().putBoolean("ThreadDiscoverActive", false).commit();
                        torquit();
                    } else {
                        preferences.edit().putBoolean("ThreadDiscoverActive", true).commit();
                        startnotifthreads();
                    }
                } else if (action.equals(REQUEST + REQUEST_RANDOM)) {
                    musicrandom();
                } else if (action.equals(REQUEST + REQUEST_SEARCH)) {

                } else if (action.equals(REQUEST + REQUEST_STARTMUSIC)) {
                    if (threadmusic == null) {
                        preferences.edit().putBoolean("playmusic", true).commit();
                        startnotifthreads();
                    }
                } else if (action.equals(REQUEST + REQUEST_STARTTOR)) {
                    if (threadDiscover == null) {
                        preferences.edit().putBoolean("ThreadDiscoverActive", true).commit();
                        startnotifthreads();
                    }
                } else if (action.equals(REQUEST + REQUEST_STOPMUSIC)) {
                    if (threadmusic != null) {
                        preferences.edit().putBoolean("playmusic", false).commit();
                        musicquit();
                    }
                } else if (action.equals(REQUEST + REQUEST_STOPTOR)) {
                    if (threadDiscover != null) {
                        preferences.edit().putBoolean("ThreadDiscoverActive", false).commit();
                        torquit();
                    }
                }
                updatenotification("onStartCommand(" + action);
            } else {
                llog.d(TAG, "onStartCommand startId " + startId + " action = null");
            }
        }
        return Service.START_NOT_STICKY;
    }

    /*private final IBinder binder = new LocalBinder();
    public class LocalBinder extends Binder {
        public myService getService() {
            return sInstance;
        }
    }*/
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    private void startmusic() {
        if (threadmusic == null) {
            threadmusic = new ThreadMusic(this);
        }
    }
    private void stopmusic() {
        if (threadmusic != null) {
            try {
                ThreadMusic.musicqueue.put("controlquit");
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        threadmusic = null;
        if (threadDiscover == null) {
            stopService();
        }
    }
    private void starttor() {
        if (threadDiscover == null) {
            threadDiscover = new ThreadDiscover(this);
        }
    }
    private void stoptor() {
        if (threadDiscover != null) {
            threadDiscover.onDestroy();
        }
        threadDiscover = null;
        if (threadmusic == null) {
            stopService();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        stopmusic();
        stoptor();
        llog.d(TAG, "--------------------------------onDestroy service------------------------------------");
    }

}
