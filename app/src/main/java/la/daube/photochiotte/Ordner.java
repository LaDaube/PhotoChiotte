package la.daube.photochiotte;

import java.util.ArrayList;
import java.util.List;

public class Ordner {

    public String address = null; public static final int ORDNER_ADDRESS = 7; // or addressEncode to ask the app for url encoding
    public String printName = null; public static final int ORDNER_PRINTNAME = 10;
    public int byDefaultShowMediaNumber = 0; public static final int ORDNER_BYDEFAULTSHOWMEDIANUMBER = 9;
    public int isOnline = Media.online_no; public static final int ORDNER_ISONLINE = 8; // online means we need to load the whole folder from apache
    public int updateDeltaTime = 0; public static final int ORDNER_UPDATEDELTATIME = 1; // in seconds

    /** generated **/
    public List<Media> mediaList = null;
    public int mediaListCount = 0;
    public boolean hasNotBeenLoadedYet = true;
    public boolean fileListCanBeRemovedFromCache = true;
    public String addressEscaped = null;

    public Ordner() {
    }
    public Ordner(String mdossier, int mfichierpardefaut, List<Media> mfiles){
        address = mdossier;
        byDefaultShowMediaNumber = mfichierpardefaut;
        if (mfiles == null) {
            mediaList = new ArrayList<>();
            mediaListCount = 0;
            hasNotBeenLoadedYet = true; // achtung c'est pas une bonne idée
        } else {
            mediaList = mfiles;
            mediaListCount = mfiles.size();
            hasNotBeenLoadedYet = false;
        }
    }

    public Ordner copyOrdner() {
      Ordner ordner = new Ordner();
      ordner.address = address;
      ordner.printName = printName;
      ordner.byDefaultShowMediaNumber = byDefaultShowMediaNumber;
      ordner.isOnline = isOnline;
      ordner.updateDeltaTime = updateDeltaTime;
      return ordner;
    }

    public void copyOrdnerInto(Ordner ordner) {
      ordner.address = address;
      ordner.printName = printName;
      ordner.byDefaultShowMediaNumber = byDefaultShowMediaNumber;
      ordner.isOnline = isOnline;
      ordner.updateDeltaTime = updateDeltaTime;
    }

}
