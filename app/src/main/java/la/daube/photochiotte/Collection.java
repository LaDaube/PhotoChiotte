package la.daube.photochiotte;

import android.content.SharedPreferences;
import android.graphics.Bitmap;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Collection {

    public String printName = null; public static final int COLLECTION_PRINTNAME = 4;
    public String keywords = null; public static final int COLLECTION_KEYWORDS = 5;
    public boolean isPublic = false; public static final int COLLECTION_ISPUBLIC = 6;
    public int version = 0; public static final int COLLECTION_VERSION = 13;

    /** generated **/

    public String address = null; //public static final int COLLECTION_ADDRESS = 14;
    public int isOnline = Media.online_no;

    public int selectedCount = 0;
    public ArrayList<Element> elementList = new ArrayList<>();
    public ArrayList<String> displayedOrdnerList = new ArrayList<>();

    public String masterCollection = null;
    public String loadedByFileInFolderAddress = null;
    public String lastOrdnerAddress = "/dummy/root/";

    public int updateDeltaTime = 0;
    public long timestamplastitem = 0L;

    public Collection(){
    }
    public Collection(String _address, String _printName, Gallery model){
      if (_address == null) {
        llog.d(TAG, "new Collection address error " + _address + " _printName=" + _printName);
      } else {
        address = _address;
        printName = _printName;
        if (masterCollection == null)
          masterCollection = address;
        llog.d(TAG, "new Collection address " + address + " printName " + printName + " mastercollection " + masterCollection);
        if (!address.equals("temp")
            && !address.equals(Gallery.virtualBookmarkFolder)
            && !address.equals(Gallery.virtualAppFolder)
            && !address.equals(Gallery.virtualWidgetFolder)
            && address.startsWith("/")
            && isOnline == Media.online_no) {
          boolean success = loadFromDisk(model);
          if (elementList.size() == 0 || !success) {
            llog.d(TAG, "loadFromDisk failed " + address);
            try {
              FileReader fr = new FileReader(address);
              BufferedReader in = new BufferedReader(fr);
              loadFromDiscSource(in, model);
              in.close();
              fr.close();
            } catch (Exception e) {
              llog.d(TAG, "loadFromDiscSource failed " + address);
            }
          }
        }
      }
    }

    public Collection copyCollection(){
      llog.d(TAG, "copyCollection() " + address);
      Collection collection = new Collection();
      collection.address = address;
      collection.printName = printName;
      collection.keywords = keywords;
      collection.isPublic = isPublic;
      collection.version = version;
      return collection;
    }

    public void emptyCollectionRemoveAllMedia(){
      selectedCount = 0;
      elementList.clear();
      displayedOrdnerList.clear();
    }
    public void setNull(){
      selectedCount = 0;
      isOnline = Media.online_no;
      address = null;
      printName = null;
      elementList.clear();
      displayedOrdnerList.clear();
      masterCollection = null;
      updateDeltaTime = 0;
      timestamplastitem = 0L;
      loadedByFileInFolderAddress = null;
    }
    public void deleteDataStoredOnDisc(){
        if (address != null) {
          File fos = new File(address);
          if (fos.exists())
            fos.delete();
        }
    }

    public void writeBufferToDisk(ByteBuffer selected) {
        FileOutputStream fos;
        try {
            fos = new FileOutputStream(address, true);
            fos.write(selected.array(), 4, selected.limit());
            // ou ByteBuffer.wrap(buf);
            fos.close();
        } catch (IOException e) {
            llog.d(TAG, "selectfolder IOException " + e.toString());
        }
    }

    public void removeDataElement(int remove) {
        RandomAccessFile fis;
        try {
            fis = new RandomAccessFile(address, "rw");

            int elementlength;

            byte KEY_LENGTH = (byte) fis.read();
            int index = -1;
            int key = 0;
            while (key != -1) {
                key = fis.read();
                if (key == Element.ELEMENT_LENGTH) {
                    elementlength = toInt(fis);
                    fis.skipBytes(elementlength);
                }
                if (index == remove) {
                    break;
                }
                index++;
            }

            fis.close();
        } catch (Exception e) {
            llog.d(TAG, "loadcollection delete " + address + " Exception " + e.toString());
            //File fos = new File(collectionpath + collectionfilename);
            //if (fos.exists())
            //    fos.delete();
        }
    }

    public void copyCollectionToData(Collection collection, boolean selectit) {

      if (collection.address.equals("temp"))
        return;

      Element ele = new Element(selectit, Element.element_collection);
      ele.collection = collection.copyCollection();
      elementList.add(ele);

      int ELEMENT_LENGTH_POSITION;
      byte[] bbyte;
      ByteBuffer selected = ByteBuffer.allocateDirect(32768).order(ByteOrder.BIG_ENDIAN);

      if (!new File(address).exists())
        selected.put(Element.KEY_LENGTH);

      selected.put((byte) Element.ELEMENT_LENGTH);
      ELEMENT_LENGTH_POSITION = selected.position();
      selected.putInt(0).order(ByteOrder.BIG_ENDIAN);

      selected.put((byte) Element.ELEMENT_ADD);
      if (selectit)
          selected.put((byte) 1);
      else
          selected.put((byte) 0);

      selected.put((byte) Element.ELEMENT_TYPE);
      selected.put((byte) Element.element_collection);

      if (ele.collection.printName != null) {
          selected.put((byte) Collection.COLLECTION_PRINTNAME);
          bbyte = ele.collection.printName.getBytes(StandardCharsets.UTF_8);
          selected.putInt(bbyte.length).order(ByteOrder.BIG_ENDIAN);
          selected.put(bbyte);
      }

      if (ele.collection.keywords != null) {
          selected.put((byte) Collection.COLLECTION_KEYWORDS);
          bbyte = ele.collection.keywords.getBytes(StandardCharsets.UTF_8);
          selected.putInt(bbyte.length).order(ByteOrder.BIG_ENDIAN);
          selected.put(bbyte);
      }

      selected.put((byte) Collection.COLLECTION_ISPUBLIC);
      if (ele.collection.isPublic)
          selected.put((byte) 1);
      else
          selected.put((byte) 0);

      selected.put((byte) Collection.COLLECTION_VERSION);
      selected.putInt(ele.collection.version).order(ByteOrder.BIG_ENDIAN);

      int size = selected.position();
      selected.position(ELEMENT_LENGTH_POSITION);
      selected.putInt(size - 4 - ELEMENT_LENGTH_POSITION).order(ByteOrder.BIG_ENDIAN);
      selected.position(size);
      selected.flip();
      selected.position(0);
      writeBufferToDisk(selected);
      selected.clear();
    }

    public void copyOrdnerToData(Ordner ordner, boolean selectit){

      if (!address.equals("temp"))
        if (!new File(address).exists())
            copyCollectionToData(this, true);

      Element ele = new Element(selectit, Element.element_ordner);
      ele.ordner = ordner.copyOrdner();
      elementList.add(ele);

      if (selectit) {
        displayedOrdnerList.add(ele.ordner.address);
        //llog.d(TAG, "displayedOrdnerList copyOrdnerToData add " + ele.ordner.address);
      } else {
        int ol = displayedOrdnerList.size();
        for (int j = 0 ; j < ol ; j++) {
            if (displayedOrdnerList.get(j).equals(ordner.address)) {
                llog.d(TAG, "removing added folder " + j + " : " + displayedOrdnerList.get(j));
                displayedOrdnerList.remove(j);
                break;
            }
        }
      }

      if (address.equals("temp"))
        return;

      int ELEMENT_LENGTH_POSITION;
      byte[] bbyte;
      ByteBuffer selected = ByteBuffer.allocateDirect(32768).order(ByteOrder.BIG_ENDIAN);

      selected.put((byte) Element.ELEMENT_LENGTH);
      ELEMENT_LENGTH_POSITION = selected.position();
      selected.putInt(0).order(ByteOrder.BIG_ENDIAN);

      selected.put((byte) Element.ELEMENT_ADD);
      if (selectit)
          selected.put((byte) 1);
      else
          selected.put((byte) 0);

      selected.put((byte) Element.ELEMENT_TYPE);
      selected.put((byte) Element.element_ordner);

      selected.put((byte) Ordner.ORDNER_ADDRESS);
      bbyte = ele.ordner.address.getBytes(StandardCharsets.UTF_8);
      selected.putInt(bbyte.length).order(ByteOrder.BIG_ENDIAN);
      selected.put(bbyte);

      selected.put((byte) Ordner.ORDNER_ISONLINE);
      selected.putInt(ele.ordner.isOnline).order(ByteOrder.BIG_ENDIAN);

      int size = selected.position();
      selected.position(ELEMENT_LENGTH_POSITION);
      selected.putInt(size - 4 - ELEMENT_LENGTH_POSITION).order(ByteOrder.BIG_ENDIAN);
      selected.position(size);
      selected.flip();
      selected.position(0);
      writeBufferToDisk(selected);
      selected.clear();
    }

    public boolean copyMediaToData(Media media, String toOrdnerNamed, boolean selectit, boolean bailIfAlreadyPresent){

      if (!address.equals("temp"))
        if (!new File(address).exists())
            copyCollectionToData(this, true);

      int ordnerListl = displayedOrdnerList.size();
      int found = -1;
      for (int i = 0; i < ordnerListl; i++) {
        if (displayedOrdnerList.get(i).equals(toOrdnerNamed)) {
          found = i;
          break;
        }
      }
      if (ordnerListl == 0 || found == -1) {
        Ordner ordner = new Ordner(toOrdnerNamed, 0, null);
        copyOrdnerToData(ordner, true);
      }

      // if (bailIfAlreadyPresent) { keep for now, don't depend on the current status of the gallery
      if (found != -1) {
        boolean bail = false;
        int elementListl = elementList.size();
        for (int i = 0; i < elementListl; i++) {
          Element ele = elementList.get(i);
          if (ele.type == Element.element_media) {
            if (ele.media != null) {
              if (ele.media.address.equals(media.address)) {
                if (bailIfAlreadyPresent && selectit) { // adding keeps previous removals
                  //llog.d(TAG, "bailIfAlreadyPresent " + media.address);
                  return false;
                } else { // removal allowed if different
                  //llog.d(TAG, ele.media.isSelected + " " + media.isSelected + " -> " + selectit);
                  if (ele.media.isSelected == selectit) {
                    bail = true;
                  } else {
                    bail = false;
                  }
                }
              }
            }
          }
        }
        if (bail) {
          llog.d(TAG, "bail " + media.isSelected + " -> " + selectit + " " + media.address);
          return false;
        }
      }

      Element ele = new Element(selectit, Element.element_media);
      ele.media = media.copyMedia();
      ele.media.bookmarkToOrdner = ele.media.ordnerAddress;
      ele.media.ordnerAddress = toOrdnerNamed;
      ele.media.ordnerPrintName = null; // otherwise this will be shown instead

      media.isSelected = selectit;
      media.selectedchecked = true;
      ele.media.isSelected = selectit;
      ele.media.selectedchecked = true;
      if (selectit) {
        selectedCount += 1;
      } else {
        selectedCount -= 1;
      }

      elementList.add(ele);

      //llog.d(TAG, selectit + " " + media.address + " " + toOrdnerNamed + " " + media.addressToGetThumbnail);

      if (address.equals("temp"))
        return true;

      int ELEMENT_LENGTH_POSITION;
      byte[] bbyte;
      ByteBuffer selected = ByteBuffer.allocateDirect(32768).order(ByteOrder.BIG_ENDIAN);

      selected.put((byte) Element.ELEMENT_LENGTH);
      ELEMENT_LENGTH_POSITION = selected.position();
      selected.putInt(0).order(ByteOrder.BIG_ENDIAN);

      selected.put((byte) Element.ELEMENT_ADD);
      if (selectit)
          selected.put((byte) 1);
      else
          selected.put((byte) 0);

      selected.put((byte) Element.ELEMENT_TYPE);
      selected.put((byte) Element.element_media);

      ele.media.mediaToBuffer(selected);

      int size = selected.position();
      selected.position(ELEMENT_LENGTH_POSITION);
      selected.putInt(size - 4 - ELEMENT_LENGTH_POSITION).order(ByteOrder.BIG_ENDIAN);
      selected.position(size);
      selected.flip();
      selected.position(0);
      writeBufferToDisk(selected);
      selected.clear();

      return true;
    }

    final static Pattern keyval = Pattern.compile("^([a-zA-Z]+?)=(.+)");
    public void loadFromDiscSource(BufferedReader in, Gallery model) {

        selectedCount = 0;
        elementList.clear();
        displayedOrdnerList.clear();
        int elementlistsize = elementList.size();
        int ordnerlistsize = displayedOrdnerList.size();

        try {
            String line;
            Element e = null;
            Matcher m;
            while ((line = in.readLine()) != null) {
                if (line.length() > 0) {
                    m = keyval.matcher(line);
                    if (m.find()) {
                        String key = m.group(1);
                        String value = m.group(2);
                        if (key != null && value != null) {
                            key = key.toLowerCase();
                            if (key.equals("element")) {
                                if (e != null) {
                                    elementList.add(e);
                                    //llog.d(TAG, "add type " + e.type);
                                    if (e.type == Element.element_ordner) {
                                        if (e.add) {
                                          boolean newordner = true; // de toute façon il faudra faire un displaycoll pour avoir un displayedOrdnerList exact +/-/+ etc..
                                          for (int k = 0; k < displayedOrdnerList.size(); k++) {
                                            if (e.ordner.address.equals(displayedOrdnerList.get(k))) {
                                              newordner = false;
                                              break;
                                            }
                                          }
                                          if (!newordner) {
                                            llog.d(TAG, "error we already have this ornder " + e.ordner.address);
                                          } else {
                                            //llog.d(TAG, "add ornder " + e.ordner.address);
                                            displayedOrdnerList.add(e.ordner.address);
                                          }
                                        }
                                        if (e.ordner.address.endsWith(".sel")) {
                                          Collection collection = model.getCollection(e.ordner.address);
                                          if (collection == null) {
                                            //llog.d(TAG, "ordner wants to load a subcollection add it to our collections but do not load it " + e.ordner.address + " mastercollection " + masterCollection);
                                            collection = new Collection();
                                            collection.address = e.ordner.address;
                                            collection.printName = Gallery.colladdresstoname(e.ordner.address);
                                            collection.masterCollection = masterCollection;
                                            collection.isOnline = e.ordner.isOnline;
                                            model.collections.add(collection);
                                          }// else {
                                          //  llog.d(TAG, "ordner wants to load a subcollection already in our collections " + e.ordner.address + " mastercollection " + masterCollection);
                                          //}
                                        }
                                    } else if (e.type == Element.element_media) {
                                        //llog.d(TAG, "add media " + e.media.address);
                                        if (e.media.isALinkThatCreatesAnOrdner) {
                                          if (e.media.address.endsWith(".sel")) {
                                            Collection collection = model.getCollection(e.media.address);
                                            if (collection == null) {
                                              //llog.d(TAG, "media wants to load a subcollection add it to our collections but do not load it " + e.media.address + " mastercollection " + masterCollection);
                                              collection = new Collection();
                                              collection.address = e.media.address;
                                              collection.printName = Gallery.colladdresstoname(e.media.address);
                                              collection.masterCollection = masterCollection;
                                              collection.isOnline = e.media.isOnline;
                                              collection.loadedByFileInFolderAddress = e.media.ordnerAddress;
                                              model.collections.add(collection);
                                            }/* else {
                                              llog.d(TAG, "media wants to load a subcollection already in our collections " + e.media.address + " mastercollection " + masterCollection);
                                            }*/
                                          }
                                        }
                                    }
                                    if (e.add)
                                        selectedCount += 1;
                                    else
                                        selectedCount -= 1;
                                }
                                e = new Element();
                                char add = value.charAt(0);
                                if (add == '+') {
                                    e.add = true;
                                } else if (add == '-') {
                                    e.add = false;
                                }
                                value = value.substring(1);
                                switch (value) {
                                    case "collection":
                                    case "element_collection":
                                        e.type = Element.element_collection;
                                        //e.collection = new Collection();
                                        break;
                                    case "ordner":
                                    case "element_ordner":
                                        e.type = Element.element_ordner;
                                        e.ordner = new Ordner();
                                        break;
                                    case "media":
                                    case "element_media":
                                        e.type = Element.element_media;
                                        e.media = new Media();
                                        break;
                                }
                            } else if (e.type == Element.element_collection) {
                                switch (key) {
                                    case "printname":
                                        printName = value;
                                        break;
                                    case "keywords":
                                        keywords = value;
                                        break;
                                    case "ispublic":
                                        isPublic = Boolean.parseBoolean(value);
                                        break;
                                    case "version":
                                        version = Integer.parseInt(value);
                                        break;
                                }
                            } else if (e.type == Element.element_ordner) {
                                switch (key) {
                                    case "addressencode":
                                        e.ordner.addressEscaped = value;
                                        e.ordner.address = Gallery.urlEncode(value);
                                        //if (e.ordner.printName == null) non surtout pas
                                        //  e.ordner.printName = value;
                                        break;
                                    case "address":
                                        e.ordner.address = value;
                                        break;
                                    case "printname":
                                        e.ordner.printName = value;
                                        break;
                                    case "bydefaultshowmedianumber":
                                        e.ordner.byDefaultShowMediaNumber = Integer.parseInt(value);
                                        break;
                                    case "isonline":
                                        switch (value) {
                                            case "no":
                                            case "online_no":
                                                e.ordner.isOnline = Media.online_no;
                                                break;
                                            case "online":
                                            case "http":
                                                e.ordner.isOnline = Media.online;
                                                break;
                                            case "nginx":
                                            case "apache":
                                            case "online_apache":
                                            case "online_nginx":
                                                e.ordner.isOnline = Media.online_apache;
                                                break;
                                            case "json":
                                            case "online_json":
                                                e.ordner.isOnline = Media.online_json;
                                                break;
                                            case "forum":
                                            case "online_forum":
                                                e.ordner.isOnline = Media.online_forum;
                                                break;
                                            case "tor":
                                            case "online_tor":
                                                e.ordner.isOnline = Media.online_tor;
                                                break;
                                        }
                                        break;
                                    case "updatedeltatime":
                                        e.ordner.updateDeltaTime = Integer.parseInt(value);
                                        //llog.d(TAG, "updateDeltaTime " + e.ordner.updateDeltaTime + " != " + updateDeltaTime);
                                        if (0 < e.ordner.updateDeltaTime
                                            && (updateDeltaTime == 0 || e.ordner.updateDeltaTime < updateDeltaTime)) {
                                          updateDeltaTime = e.ordner.updateDeltaTime;
                                          if (timestamplastitem == 0)
                                            timestamplastitem = System.currentTimeMillis() - Gallery.rand.nextInt(e.ordner.updateDeltaTime * 500);
                                          else {
                                            llog.d(TAG, "updated " + address + " after dt=" + (timestamplastitem - System.currentTimeMillis()));
                                            timestamplastitem = System.currentTimeMillis();
                                          }
                                        }
                                        break;
                                }
                            } else if (e.type == Element.element_media) {
                                switch (key) {
                                    case "address":
                                        e.media.address = value;
                                        break;
                                    case "addressencode":
                                        e.media.addressescaped = value;
                                        e.media.address = Gallery.urlEncode(value);
                                        /*if (e.media.printName == null) { non surtout pas
                                          int lastslash = value.lastIndexOf("/");
                                          if (lastslash + 1 < value.length())
                                            e.media.printName = value.substring(lastslash + 1);
                                          else
                                            e.media.printName = value;
                                        }*/
                                        break;
                                    case "printname":
                                        e.media.printName = value;
                                        break;
                                    case "printdetails":
                                        e.media.printDetails = value;
                                        break;
                                    case "printfooter":
                                        e.media.printFooter = value;
                                        break;
                                    case "ordneraddress":
                                        e.media.ordnerAddress = value;
                                        break;
                                    case "ordneraddressencode":
                                        e.media.ordnerAddress = Gallery.urlEncode(value);
                                        break;
                                    case "ordnerprintname":
                                        e.media.ordnerPrintName = value;
                                        break;
                                    case "addresstogetpreviewfullsize":
                                        e.media.addressToGetPreviewFullSize = value;
                                        break;
                                    case "addresstogetpreviewfullsizeencode":
                                        e.media.addressToGetPreviewFullSize = Gallery.urlEncode(value);
                                        break;
                                    case "addresstogetthumbnail":
                                        e.media.addressToGetThumbnail = value;
                                        break;
                                    case "addresstogetthumbnailencode":
                                        e.media.addressToGetThumbnail = Gallery.urlEncode(value);
                                        break;
                                    case "bookmarktoordner":
                                        e.media.bookmarkToOrdner = value;
                                        break;
                                    case "bookmarktoordnerencode":
                                        e.media.bookmarkToOrdner = Gallery.urlEncode(value);
                                        break;
                                    case "isalinkthatcreatesanordner":
                                        e.media.isALinkThatCreatesAnOrdner = Boolean.parseBoolean(value);
                                        break;
                                    case "addresstogetlibextractorsthumbnail":
                                        e.media.addressToGetLibextractorsThumbnail = Integer.parseInt(value);
                                        break;
                                    case "type":
                                        switch (value) {
                                            case "refused":
                                            case "media_refused":
                                                e.media.type = Media.media_refused;
                                                break;
                                            case "picture":
                                            case "media_picture":
                                                e.media.type = Media.media_picture;
                                                break;
                                            case "video":
                                            case "media_video":
                                                e.media.type = Media.media_video;
                                                break;
                                            case "music":
                                            case "media_music":
                                                e.media.type = Media.media_music;
                                                break;
                                            case "eps":
                                            case "media_eps":
                                                e.media.type = Media.media_eps;
                                                break;
                                            case "app":
                                            case "media_app":
                                              e.media.type = Media.media_app;
                                              break;
                                            case "button":
                                            case "media_button":
                                              e.media.type = Media.media_button;
                                              break;
                                            case "collection":
                                            case "media_collection":
                                              e.media.type = Media.media_collection;
                                              break;
                                        }
                                        break;
                                    case "isonline":
                                        switch (value) {
                                            case "no":
                                            case "online_no":
                                                e.media.isOnline = Media.online_no;
                                                break;
                                            case "online":
                                            case "http":
                                                e.media.isOnline = Media.online;
                                                break;
                                            case "nginx":
                                            case "apache":
                                            case "online_apache":
                                            case "online_nginx":
                                                e.media.isOnline = Media.online_apache;
                                                break;
                                            case "json":
                                            case "online_json":
                                                e.media.isOnline = Media.online_json;
                                                break;
                                            case "forum":
                                            case "online_forum":
                                                e.media.isOnline = Media.online_forum;
                                                break;
                                            case "tor":
                                            case "online_tor":
                                                e.media.isOnline = Media.online_tor;
                                                break;
                                        }
                                        break;
                                    case "isinsideanarchive":
                                        e.media.isInsideAnArchive = Boolean.parseBoolean(value);
                                        break;
                                    case "isselected":
                                        e.media.isSelected = Boolean.parseBoolean(value);
                                        break;
                                    case "playinsequence":
                                        e.media.playInSequence = Boolean.parseBoolean(value);
                                        break;
                                    case "filmstripcount":
                                        e.media.filmstripCount = Integer.parseInt(value);
                                        break;
                                    case "playstartatposition":
                                        e.media.playStartAtPosition = Integer.parseInt(value);
                                        break;
                                    case "getdirectpictureaddressbypassads":
                                        e.media.getDirectPictureAddressBypassAds = Boolean.parseBoolean(value);
                                        break;
                                    case "isonlinelevel":
                                        e.media.isonlinelevel = Integer.parseInt(value);
                                        break;
                                    case "isonlineforumlevel":
                                        e.media.isonlineforumlevel = Integer.parseInt(value);
                                        break;
                                    case "isonlinelinktonextpagei":
                                        e.media.isonlinelinktonextpagei = Integer.parseInt(value);
                                        break;
                                    case "isonlinelinktonextpagetot":
                                        e.media.isonlinelinktonextpagetot = Integer.parseInt(value);
                                        break;
                                    case "isonlineparentlink":
                                        e.media.isonlineparentlink = value;
                                        break;
                                    case "isonlineparentlinkencode":
                                        e.media.isonlineparentlink = Gallery.urlEncode(value);
                                        break;
                                    case "subtitleaddress":
                                        if (e.media.subtitleAddress == null)
                                          e.media.subtitleAddress = new ArrayList<>();
                                        e.media.subtitleAddress.add(value);
                                        break;
                                    case "subtitleaddressencode":
                                        if (e.media.subtitleAddress == null)
                                          e.media.subtitleAddress = new ArrayList<>();
                                        e.media.subtitleAddress.add(Gallery.urlEncode(value));
                                        break;
                                }
                            }
                        }
                    }
                }
            }
            if (e != null) {
                elementList.add(e);
                //llog.d(TAG, "add type " + e.type);
                if (e.type == Element.element_ordner) {
                  if (e.add) {
                    boolean newordner = true;
                    for (int k = 0; k < displayedOrdnerList.size(); k++) {
                      if (e.ordner.address.equals(displayedOrdnerList.get(k))) {
                        newordner = false;
                        break;
                      }
                    }
                    if (!newordner) {
                      llog.d(TAG, "error we already have this ornder " + e.ordner.address);
                    } else {
                      //llog.d(TAG, "add ornder " + e.ordner.address);
                      displayedOrdnerList.add(e.ordner.address);
                    }
                  }
                  if (e.ordner.address.endsWith(".sel")) {
                    Collection collection = model.getCollection(e.ordner.address);
                    if (collection == null) {
                      //llog.d(TAG, "ordner wants to load a subcollection add it to our collections but do not load it " + e.ordner.address + " mastercollection " + masterCollection);
                      collection = new Collection();
                      collection.address = e.ordner.address;
                      collection.printName = Gallery.colladdresstoname(e.ordner.address);
                      collection.masterCollection = masterCollection;
                      model.collections.add(collection);
                    }
                  }
                } else if (e.type == Element.element_media) {
                  //llog.d(TAG, "add media " + e.media.address);
                  if (e.media.isALinkThatCreatesAnOrdner) {
                    if (e.media.address.endsWith(".sel")) {
                      Collection collection = model.getCollection(e.media.address);
                      if (collection == null) {
                        //llog.d(TAG, "media wants to load a subcollection add it to our collections but do not load it " + e.media.address + " mastercollection " + masterCollection);
                        collection = new Collection();
                        collection.address = e.media.address;
                        collection.printName = Gallery.colladdresstoname(e.media.address);
                        collection.masterCollection = masterCollection;
                        collection.isOnline = e.media.isOnline;
                        collection.loadedByFileInFolderAddress = e.media.ordnerAddress;
                        model.collections.add(collection);
                      }
                    }
                  }
                }
                if (e.add)
                    selectedCount += 1;
                else
                    selectedCount -= 1;
            }
        } catch (Exception e) {
            llog.d(TAG, "error exception " + e.toString());
            e.printStackTrace();
        }

        //llog.d(TAG, "loadFromDiscSource new size " + elementList.size() + " displayerdOrdnerList " + displayerdOrdnerList.size());

    }

    private boolean loadFromDisk(Gallery model){
        boolean success = false;
        selectedCount = 0;
        elementList.clear();
        displayedOrdnerList.clear();
        FileInputStream fis;
        try {
            fis = new FileInputStream(address);
            int filesize = (int) fis.getChannel().size();
            llog.d(TAG, "loadFromDisk file size = " + filesize);
            byte[] buffer = null;
            int n;
            int stringl;

            /** collection header **/

            byte KEY_LENGTH = (byte) fis.read();
            llog.d(TAG, "KEY_LENGTH " + KEY_LENGTH);

            selectedCount = 0;
            boolean readelement = true;
            int elementlength = 0;
            int i = 0;
            int key;
            Element e = null;
            while (readelement) {
                if (i == elementlength) {
                    if (e != null) {
                      if (e.type != -1) {
                        elementList.add(e);
                        if (e.type == Element.element_ordner) {
                          if (e.add) {
                            boolean newordner = true;
                            for (int k = 0; k < displayedOrdnerList.size(); k++) {
                              if (e.ordner.address.equals(displayedOrdnerList.get(k))) {
                                newordner = false;
                                break;
                              }
                            }
                            if (!newordner) {
                              llog.d(TAG, "error we already have this ornder " + e.ordner.address);
                            } else {
                              //llog.d(TAG, "add ornder " + e.ordner.address);
                              displayedOrdnerList.add(e.ordner.address);
                            }
                          }
                          //llog.d(TAG, "displayedOrdnerList loadFromDisk add " + e.ordner.address);
                          if (e.ordner.address.endsWith(".sel")) {
                            if (model == null) {
                              llog.d(TAG, "error no gallery to populate");
                            } else {
                              Collection collection = model.getCollection(e.ordner.address);
                              if (collection == null) {
                                //llog.d(TAG, "ordner wants to load a subcollection add it to our collections " + e.ordner.address + " mastercollection " + masterCollection);
                                collection = new Collection();
                                collection.address = e.ordner.address;
                                collection.printName = Gallery.colladdresstoname(e.ordner.address);
                                collection.masterCollection = masterCollection;
                                model.collections.add(collection);
                              }
                            }
                          }
                        } else if (e.type == Element.element_media) {
                          if (e.media.isALinkThatCreatesAnOrdner) {
                            if (e.media.address.endsWith(".sel")) {
                              if (model == null) {
                                llog.d(TAG, "error no gallery to populate");
                              } else {
                                Collection collection = model.getCollection(e.media.address);
                                if (collection == null) {
                                  //llog.d(TAG, "media wants to load a subcollection add it to our collections but do not load it " + e.media.address + " mastercollection " + masterCollection);
                                  collection = new Collection();
                                  collection.address = e.media.address;
                                  collection.printName = Gallery.colladdresstoname(e.media.address);
                                  collection.masterCollection = masterCollection;
                                  collection.isOnline = e.media.isOnline;
                                  collection.loadedByFileInFolderAddress = e.media.ordnerAddress;
                                  model.collections.add(collection);
                                }
                              }
                            }
                          }
                        }
                        if (e.add)
                          selectedCount += 1;
                        else
                          selectedCount -= 1;
                        //llog.d(TAG, selectedCount + " : element add " + e.add + " type " + e.type);
                      }
                    }
                    e = new Element();
                    key = fis.read();
                    if (key == -1)
                        break;
                } else {
                    key = buffer[i]; i += 1;
                }
                if (e.type == Element.element_collection) {
                    if (key == Collection.COLLECTION_PRINTNAME) {
                        stringl = toInt(buffer, i); i += 4;
                        printName = new String(buffer, i, stringl, StandardCharsets.UTF_8); i += stringl;
                    } else if (key == Collection.COLLECTION_KEYWORDS) {
                        stringl = toInt(buffer, i); i += 4;
                        keywords = new String(buffer, i, stringl, StandardCharsets.UTF_8); i += stringl;
                    } else if (key == Collection.COLLECTION_ISPUBLIC) {
                        if (buffer[i] == 1)
                            isPublic = true;
                        else
                            isPublic = false;
                        i += 1;
                    } else if (key == Collection.COLLECTION_VERSION) {
                        version = toInt(buffer, i); i += 4;
                    }
                } else if (e.type == Element.element_ordner) {
                    if (key == Ordner.ORDNER_ADDRESS) {
                        stringl = toInt(buffer, i); i += 4;
                        e.ordner.address = new String(buffer, i, stringl, StandardCharsets.UTF_8); i += stringl;
                    } else if (key == Ordner.ORDNER_PRINTNAME) {
                        stringl = toInt(buffer, i); i += 4;
                        e.ordner.printName = new String(buffer, i, stringl, StandardCharsets.UTF_8); i += stringl;
                    } else if (key == Ordner.ORDNER_BYDEFAULTSHOWMEDIANUMBER) {
                        e.ordner.byDefaultShowMediaNumber = toInt(buffer, i); i += 4;
                    } else if (key == Ordner.ORDNER_ISONLINE) {
                        e.ordner.isOnline = toInt(buffer, i); i += 4;
                    } else if (key == Ordner.ORDNER_UPDATEDELTATIME) {
                        e.ordner.updateDeltaTime = toInt(buffer, i); i += 4;
                        if (0 < e.ordner.updateDeltaTime
                            && (updateDeltaTime == 0 || e.ordner.updateDeltaTime < updateDeltaTime)) {
                          updateDeltaTime = e.ordner.updateDeltaTime;
                          if (timestamplastitem == 0)
                            timestamplastitem = System.currentTimeMillis() - Gallery.rand.nextInt(e.ordner.updateDeltaTime * 500);
                          else
                            timestamplastitem = System.currentTimeMillis();
                        }
                    }
                } else if (e.type == Element.element_media) {
                    switch (key) {
                        case Media.MEDIA_ADDRESS:
                            stringl = toInt(buffer, i); i += 4;
                            e.media.address = new String(buffer, i, stringl, StandardCharsets.UTF_8); i += stringl;
                            break;
                        case Media.MEDIA_PRINTNAME:
                            stringl = toInt(buffer, i); i += 4;
                            e.media.printName = new String(buffer, i, stringl, StandardCharsets.UTF_8); i += stringl;
                            break;
                        case Media.MEDIA_PRINTDETAILS:
                            stringl = toInt(buffer, i); i += 4;
                            e.media.printDetails = new String(buffer, i, stringl, StandardCharsets.UTF_8); i += stringl;
                            break;
                        case Media.MEDIA_PRINTFOOTER:
                            stringl = toInt(buffer, i); i += 4;
                            e.media.printFooter = new String(buffer, i, stringl, StandardCharsets.UTF_8); i += stringl;
                            break;
                        case Media.MEDIA_ORDNERADDRESS:
                            stringl = toInt(buffer, i); i += 4;
                            e.media.ordnerAddress = new String(buffer, i, stringl, StandardCharsets.UTF_8); i += stringl;
                            break;
                        case Media.MEDIA_ORDNERPRINTNAME:
                            stringl = toInt(buffer, i); i += 4;
                            e.media.ordnerPrintName = new String(buffer, i, stringl, StandardCharsets.UTF_8); i += stringl;
                            break;
                        case Media.MEDIA_ADDRESSTOGETPICTUREFULLSIZE:
                            stringl = toInt(buffer, i); i += 4;
                            e.media.addressToGetPreviewFullSize = new String(buffer, i, stringl, StandardCharsets.UTF_8); i += stringl;
                            break;
                        case Media.MEDIA_ADDRESSTOGETPICTURETHUMBNAILSIZE:
                            stringl = toInt(buffer, i); i += 4;
                            e.media.addressToGetThumbnail = new String(buffer, i, stringl, StandardCharsets.UTF_8); i += stringl;
                            break;
                        case Media.MEDIA_BOORKMARKTOORDNER:
                            stringl = toInt(buffer, i); i += 4;
                            e.media.bookmarkToOrdner = new String(buffer, i, stringl, StandardCharsets.UTF_8); i += stringl;
                            break;
                        case Media.MEDIA_ADDRESSTOGETLIBEXTRACTORSTHUMBNAIL:
                            e.media.addressToGetLibextractorsThumbnail = toInt(buffer, i); i += 4;
                            break;
                        case Media.MEDIA_TYPE:
                            e.media.type = toInt(buffer, i); i += 4;
                            break;
                        case Media.MEDIA_ISONLINE:
                            e.media.isOnline = toInt(buffer, i); i += 4;
                            break;
                        case Media.MEDIA_ISINSIDEANARCHIVE:
                            if (buffer[i] == 1)
                                e.media.isInsideAnArchive = true;
                            i += 1;
                            break;
                        case Media.MEDIA_ISSELECTED:
                            if (buffer[i] == 1)
                                e.media.isSelected = true;
                            i += 1;
                            break;
                        case Media.MEDIA_PLAYINSEQUENCE:
                            if (buffer[i] == 1)
                                e.media.playInSequence = true;
                            i += 1;
                            break;
                        case Media.MEDIA_FILMSTRIPCOUNT: // TODO : or reset to filmstripNotCheckedYet
                            e.media.filmstripCount = toInt(buffer, i); i += 4;
                            break;
                        case Media.MEDIA_PLAYSTARTATPOSITION:
                            e.media.playStartAtPosition = toInt(buffer, i); i += 4;
                            break;
                        case Media.getDirectPictureAddressBypassAds_:
                            if (buffer[i] == 1)
                                e.media.getDirectPictureAddressBypassAds = true;
                            i += 1;
                            break;
                        case Media.MEDIA_ISALINKTHATCREATESANORDNER:
                            if (buffer[i] == 1)
                                e.media.isALinkThatCreatesAnOrdner = true;
                            i += 1;
                            break;
                        case Media.isonlinelevel_:
                            e.media.isonlinelevel = toInt(buffer, i); i += 4;
                            break;
                        case Media.isonlineforumlevel_:
                            e.media.isonlineforumlevel = toInt(buffer, i); i += 4;
                            break;
                        case Media.isonlinelinktonextpagei_:
                            e.media.isonlinelinktonextpagei = toInt(buffer, i); i += 4;
                            break;
                        case Media.isonlinelinktonextpagetot_:
                            e.media.isonlinelinktonextpagetot = toInt(buffer, i); i += 4;
                            break;
                        case Media.isonlineparentlink_:
                            stringl = toInt(buffer, i); i += 4;
                            e.media.isonlineparentlink = new String(buffer, i, stringl, StandardCharsets.UTF_8); i += stringl;
                            break;
                        case Media.MEDIA_SUBTITLEADDRESS:
                            if (e.media.subtitleAddress == null)
                              e.media.subtitleAddress = new ArrayList<>();
                            stringl = toInt(buffer, i); i += 4;
                            e.media.subtitleAddress.add(new String(buffer, i, stringl, StandardCharsets.UTF_8)); i += stringl;
                            break;
                    }
                } else {
                    if (key == Element.ELEMENT_LENGTH) {
                        elementlength = toInt(fis);
                        //llog.d(TAG, "++ new element size " + elementlength);
                        buffer = new byte[elementlength];
                        n = fis.read(buffer);
                        i = 0;
                    } else if (key == Element.ELEMENT_TYPE) {
                        e.type = buffer[i]; i += 1;
                        //llog.d(TAG, "         type " + e.type);
                        if (e.type == Element.element_media) {
                            e.media = new Media();
                        } else if (e.type == Element.element_collection) {
                            //e.collection = new Collection();
                        } else if (e.type == Element.element_ordner) {
                            e.ordner = new Ordner();
                        }
                    } else if (key == Element.ELEMENT_ADD) {
                        if (buffer[i] == 1)
                            e.add = true;
                        else
                            e.add = false;
                        i += 1;
                        //llog.d(TAG, "          add " + e.add);
                    }
                }
            }

            //llog.d(TAG, "-- finished reading collection");

            fis.close();
            success = true;
        } catch (Exception e) {
            llog.d(TAG, "loadcollection delete " + address + " Exception " + e.toString());
        }

        /*int wasaddedatposition = 0;
        while (wasaddedatposition != -1) {
            wasaddedatposition = -1;
            int ll = elementList.size();
            for (int i = 0; i < ll; i++) {
                Element e = elementList.get(i);
                if (!e.add) {
                    wasaddedatposition = wasaddedatposition(e, i);
                    llog.d(TAG, "removing added " + wasaddedatposition + " removed " + i);
                    if (wasaddedatposition != -1) {
                        if (e.type == Element.element_ordner) {
                            int ol = displayerdOrdnerList.size();
                            for (int j = 0 ; j < ol ; j++) {
                                if (displayerdOrdnerList.get(j).equals(e.ordner.address)) {
                                    llog.d(TAG, "removing added folder " + j + " : " + displayerdOrdnerList.get(j));
                                    displayerdOrdnerList.remove(j);
                                    break;
                                }
                            }
                            ol = displayerdOrdnerList.size();
                            for (int j = 0 ; j < ol ; j++) {
                                if (displayerdOrdnerList.get(j).equals(e.ordner.address)) {
                                    llog.d(TAG, "removing removed folder " + j + " : " + displayerdOrdnerList.get(j));
                                    displayerdOrdnerList.remove(j);
                                    break;
                                }
                            }
                        }
                        elementList.remove(i);
                        elementList.remove(wasaddedatposition);
                        break;
                    } else {
                        llog.d(TAG, "error was never added");
                    }
                }
            }
        }*/
      return success;
    }

    public int wasaddedatposition(Element e, int ll) {
        for (int i = 0; i < ll ; i++) {
            Element el = elementList.get(i);
            if (el.add && el.type == e.type) {
                if (el.type == Element.element_ordner) {
                    if (el.ordner.address.equals(e.ordner.address)) {
                        return i;
                    }
                } else if (el.type == Element.element_media) {
                    if (el.media.address.equals(e.media.address)) {
                        return i;
                    }
                }
            }
        }
        return -1;
    }

    public static String TAG = "YYYcol";

    /*public static boolean mediaIsSelectedInData(int d, int f){
      int seldatasize = seldata.position();
      seldata.position(0);
      while (seldata.position() < seldatasize) {

        int fd = seldata.getInt();

        int fichierl = seldata.getInt();
        byte[] fichierb = new byte[fichierl];
        seldata.get(fichierb);
        String fichier = new String(fichierb, StandardCharsets.UTF_8);

        int dossierl = seldata.getInt();
        byte[] dossierb = new byte[dossierl];
        seldata.get(dossierb);
        String dossier = new String(fichierb, StandardCharsets.UTF_8);

        byte selected = seldata.get();
        boolean isselected = false;
        if (selected == 1)
          isselected = true;

        llog.d(TAG, fd + " : " + fichierl + " " + fichier + " : " + dossierl + " " + dossier);
      }
      return false;
    }*/

    public static int toInt(byte[] buf, int offset) {
        //llog.d(TAG, String.format("%02x%02x%02x%02x = %d", buf[offset+0], buf[offset+1], buf[offset+2], buf[offset+3], ret));
        return ((buf[offset] << 24) & 0xff000000)
                + ((buf[offset+1] << 16) & 0xff0000)
                + ((buf[offset+2] << 8) & 0xff00)
                + (buf[offset+3] & 0xff);
    }
    public static int toInt(ByteBuffer buf) {
        //llog.d(TAG, String.format("%02x%02x%02x%02x = %d", buf[offset+0], buf[offset+1], buf[offset+2], buf[offset+3], ret));
        return ((buf.get() << 24) & 0xff000000)
                + ((buf.get() << 16) & 0xff0000)
                + ((buf.get() << 8) & 0xff00)
                + (buf.get() & 0xff);
    }
    public static int toInt(FileInputStream buf) throws IOException {
        //llog.d(TAG, String.format("%02x%02x%02x%02x = %d", buf[offset+0], buf[offset+1], buf[offset+2], buf[offset+3], ret));
        return ((buf.read() << 24) & 0xff000000)
                + ((buf.read() << 16) & 0xff0000)
                + ((buf.read() << 8) & 0xff00)
                + (buf.read() & 0xff);
    }
    public static int toInt(RandomAccessFile buf) throws IOException {
        //llog.d(TAG, String.format("%02x%02x%02x%02x = %d", buf[offset+0], buf[offset+1], buf[offset+2], buf[offset+3], ret));
        return ((buf.read() << 24) & 0xff000000)
                + ((buf.read() << 16) & 0xff0000)
                + ((buf.read() << 8) & 0xff00)
                + (buf.read() & 0xff);
    }

    public static void deleteCollection(String fullpath){
        File fos = new File(fullpath);
        if (fos.exists())
            fos.delete();
    }

    public static void deleteCollection(String path, String filename){
        File fos = new File(path + filename);
        if (fos.exists())
            fos.delete();
    }

    public void reloadApps(Gallery model) {
        int nombredossierbookmarked = model.allappinfo.size();
        if (nombredossierbookmarked > 0) {
            Element ele;
            String dossiername;
            ele = new Element(true, Element.element_ordner);
            dossiername = Gallery.virtualAppFolder + "data/";
            ele.ordner.address = dossiername;
            ele.ordner.hasNotBeenLoadedYet = false;
            elementList.add(ele);
            displayedOrdnerList.add(dossiername);
            for (int i = 0; i < nombredossierbookmarked; i++) {
                AppInfo appinfo = model.allappinfo.get(i);
                if (appinfo.sourcedir.startsWith("/data/")) {
                    ele = new Element(true, Element.element_media);
                    ele.media.printName = appinfo.appname + " " + appinfo.versionname;
                    ele.media.printDetails = appinfo.sourcedir + "    " + appinfo.packagename;
                    for (int j = 0 ; j < appinfo.servicel ; j++) {
                        int l = appinfo.servicename[j].lastIndexOf(".");
                        if (l > 0)
                            ele.media.printDetails += " " + appinfo.servicename[j].substring(l+1);
                        else
                            ele.media.printDetails += " " + appinfo.servicename[j];
                    }
                    ele.media.printFooter = appinfo.servicel + " services " + appinfo.permissionl + " permissions";
                    ele.media.address = appinfo.packagename;
                    ele.media.ordnerAddress = dossiername;
                    ele.media.isOnline = Media.online_no;
                    ele.media.type = Media.media_app;
                    elementList.add(ele);
                }
            }
            ele = new Element(true, Element.element_ordner);
            dossiername = Gallery.virtualAppFolder + "system/";
            ele.ordner.address = dossiername;
            ele.ordner.hasNotBeenLoadedYet = false;
            elementList.add(ele);
            displayedOrdnerList.add(dossiername);
            for (int i = 0; i < nombredossierbookmarked; i++) {
                AppInfo appinfo = model.allappinfo.get(i);
                if (!appinfo.sourcedir.startsWith("/data/")
                        && !appinfo.packagename.startsWith("com.android")
                        && !appinfo.packagename.startsWith("com.google")
                ) {
                    ele = new Element(true, Element.element_media);
                    ele.media.printName = appinfo.appname + " " + appinfo.versionname;
                    ele.media.printDetails = appinfo.sourcedir + "    " + appinfo.packagename;
                    for (int j = 0 ; j < appinfo.servicel ; j++) {
                        int l = appinfo.servicename[j].lastIndexOf(".");
                        if (l > 1)
                            ele.media.printDetails += " " + appinfo.servicename[j].substring(l+1);
                        else
                            ele.media.printDetails += " " + appinfo.servicename[j];
                    }
                    ele.media.printFooter = appinfo.servicel + " services " + appinfo.permissionl + " permissions";
                    ele.media.address = appinfo.packagename;
                    ele.media.ordnerAddress = dossiername;
                    ele.media.isOnline = Media.online_no;
                    ele.media.type = Media.media_app;
                    elementList.add(ele);
                }
            }
            ele = new Element(true, Element.element_ordner);
            dossiername = Gallery.virtualAppFolder + "system/com.google.*/";
            ele.ordner.address = dossiername;
            ele.ordner.hasNotBeenLoadedYet = false;
            elementList.add(ele);
            displayedOrdnerList.add(dossiername);
            for (int i = 0; i < nombredossierbookmarked; i++) {
                AppInfo appinfo = model.allappinfo.get(i);
                if (!appinfo.sourcedir.startsWith("/data/")
                        && !appinfo.packagename.startsWith("com.android")
                        && appinfo.packagename.startsWith("com.google")
                ) {
                    ele = new Element(true, Element.element_media);
                    ele.media.printName = appinfo.appname + " " + appinfo.versionname;
                    ele.media.printDetails = appinfo.sourcedir + "    " + appinfo.packagename;
                    for (int j = 0 ; j < appinfo.servicel ; j++) {
                        int l = appinfo.servicename[j].lastIndexOf(".");
                        if (l > 0)
                            ele.media.printDetails += " " + appinfo.servicename[j].substring(l+1);
                        else
                            ele.media.printDetails += " " + appinfo.servicename[j];
                    }
                    ele.media.printFooter = appinfo.servicel + " services " + appinfo.permissionl + " permissions";
                    ele.media.address = appinfo.packagename;
                    ele.media.ordnerAddress = dossiername;
                    ele.media.isOnline = Media.online_no;
                    ele.media.type = Media.media_app;
                    elementList.add(ele);
                }
            }
            ele = new Element(true, Element.element_ordner);
            dossiername = Gallery.virtualAppFolder + "system/com.android.*/";
            ele.ordner.address = dossiername;
            ele.ordner.hasNotBeenLoadedYet = false;
            elementList.add(ele);
            displayedOrdnerList.add(dossiername);
            for (int i = 0; i < nombredossierbookmarked; i++) {
                AppInfo appinfo = model.allappinfo.get(i);
                if (!appinfo.sourcedir.startsWith("/data/")
                        && appinfo.packagename.startsWith("com.android")
                        && !appinfo.packagename.startsWith("com.google")
                ) {
                    ele = new Element(true, Element.element_media);
                    ele.media.printName = appinfo.appname + " " + appinfo.versionname;
                    ele.media.printDetails = appinfo.sourcedir + "    " + appinfo.packagename;
                    for (int j = 0 ; j < appinfo.servicel ; j++) {
                        int l = appinfo.servicename[j].lastIndexOf(".");
                        if (l > 0)
                            ele.media.printDetails += " " + appinfo.servicename[j].substring(l+1);
                        else
                            ele.media.printDetails += " " + appinfo.servicename[j];
                    }
                    ele.media.printFooter = appinfo.servicel + " services " + appinfo.permissionl + " permissions";
                    ele.media.address = appinfo.packagename;
                    ele.media.ordnerAddress = dossiername;
                    ele.media.isOnline = Media.online_no;
                    ele.media.type = Media.media_app;
                    elementList.add(ele);
                }
            }
        }
    }

    public void reloadWidgets(SharedPreferences preferences, Gallery model){
        Element ele = new Element(true, Element.element_ordner);
        String dossiername = Gallery.virtualCollectionFolder + "widgets/";
        ele.ordner.address = dossiername;
        elementList.add(ele);
        displayedOrdnerList.add(dossiername);
        int w = model.bigScreenWidth;
        int h = model.bigScreenHeight;
        int u = w;
        if (h < w)
            u = h;
        if (u > 600)
            u = 600;

        ele = new Element(true, Element.element_media);
        ele.media.address = dossiername + "time";
        ele.media.ordnerAddress = dossiername;
        ele.media.isOnline = Media.online_no;
        ele.media.type = Media.media_button;
        ele.media.addressToGetPreviewFullSize = ele.media.address;
        ele.media.addressToGetThumbnail = ele.media.address;
        ele.media.filmstripCount = 2;
        ele.media.buttonaction = new String[]{
                "wh= w " + u + " h " + u,
                "drawrect= r 0 g 0 b 0 s 0 xi 0 xf 1 yi 0 yf 1",
                "dateformat= r 1.0 g 0.7 b 0.4 s 0.07 r x 0.5 y 0.1 txt dd",
                "dateformat= r 1.0 g 0.4 b 0.4 s 0.07 l x 0.5 y 0.1 txt  MM",
                "dateformat= r 1.0 g 1.0 b 0.4 s 0.07 c x 0.5 y 0.17 txt EEEE",
                "drawtarc= r 0.4 g 0.6 b 0.4 s 0.01 xi 0.1 xf 0.9 yi 0.1 yf 0.9 a 0.5 t 13 c y",
                "drawtarc= r 0.5 g 0.9 b 0.5 s 0.01 xi 0.2 xf 0.8 yi 0.2 yf 0.8 a 2.5 t 12 c y",
                "drawtarc= r 0.6 g 1.0 b 0.6 s 0.01 xi 0.3 xf 0.7 yi 0.3 yf 0.7 a 5 t 10 c y",
                "drawtarc= r 1.0 g 1.0 b 0.4 s 0.01 xi 0.01 xf 0.99 yi 0.01 yf 0.99 a 1 t 7 c n",
                "drawtarc= r 1.0 g 0.7 b 0.4 s 0.01 xi 0.03 xf 0.97 yi 0.03 yf 0.97 a 1 t 5 c n",
                "drawtarc= r 1.0 g 0.4 b 0.4 s 0.01 xi 0.05 xf 0.95 yi 0.05 yf 0.95 a 1 t 2 c n",
                //"runcmd= cmd cat /sys/class/power_supply/battery/capacity r 0.6 g 0.6 b 0.6 s 0.066 c x 0.33 y 0.3",
        };
        elementList.add(ele);

        String lockscreencalendarwidgetset = model.preferences.getString("lockscreencalendarwidgetset", Gallery.lockscreencalendarwidgetset);
        String lockscreencountdownwidgetset = model.preferences.getString("lockscreencountdownwidgetset", Gallery.lockscreencountdownwidgetset);
        String lockscreenmonitorwidgetset = model.preferences.getString("lockscreenmonitorwidgetset", Gallery.lockscreenmonitorwidgetset);

        int wj = 3, wm = 2, wa = 2024, mt = 0;
        float wx = 0, wy = 0, ww = 0.1f, wh = 0.1f;
        int wt=0;
        int nm = 3;
        Matcher m;

        String cds = null;

        m = Gallery.lockscreencountdownwidgetsetpattern.matcher(lockscreencountdownwidgetset);
        while (m.find()) {
          wx = Integer.parseInt(m.group(1)) / 100.0f;
          wy = Integer.parseInt(m.group(2)) / 100.0f;
          ww = Integer.parseInt(m.group(3)) / 100.0f;
          wh = Integer.parseInt(m.group(4)) / 100.0f;
          wj = Integer.parseInt(m.group(5));
          wm = Integer.parseInt(m.group(6));
          wa = Integer.parseInt(m.group(7));
          if (cds == null)
            cds = String.format("%02d%02d%04d", wj, wm, wa);
          else
            cds += String.format("|%02d%02d%04d", wj, wm, wa);
        }

        m = Gallery.lockscreencalendarwidgetpattern.matcher(lockscreencalendarwidgetset);
        while (m.find()) {
          wx = Integer.parseInt(m.group(1)) / 100.0f;
          wy = Integer.parseInt(m.group(2)) / 100.0f;
          ww = Integer.parseInt(m.group(3)) / 100.0f;
          wh = Integer.parseInt(m.group(4)) / 100.0f;
          nm = Integer.parseInt(m.group(5));
          if (cds == null)
            cds = "";
          int hh = (int) ((u * ww) / (wh));
          ele = new Element(true, Element.element_media);
          ele.media.address = dossiername + "calendar " + nm;
          ele.media.ordnerAddress = dossiername;
          ele.media.isOnline = Media.online_no;
          ele.media.type = Media.media_button;
          ele.media.addressToGetPreviewFullSize = ele.media.address;
          ele.media.addressToGetThumbnail = ele.media.address;
          ele.media.filmstripCount = 1;
          ele.media.buttonaction = new String[]{
              "wh= w " + (hh*2) + " h " + (u*2),
              "calendar= t " + nm + " cd " + cds,
          };
          elementList.add(ele);
        }

        m = Gallery.lockscreencountdownwidgetsetpattern.matcher(lockscreencountdownwidgetset);
        while (m.find()) {
          wx = Integer.parseInt(m.group(1)) / 100.0f;
          wy = Integer.parseInt(m.group(2)) / 100.0f;
          ww = Integer.parseInt(m.group(3)) / 100.0f;
          wh = Integer.parseInt(m.group(4)) / 100.0f;
          wj = Integer.parseInt(m.group(5));
          wm = Integer.parseInt(m.group(6));
          wa = Integer.parseInt(m.group(7));
          if (cds == null)
            cds = String.format("%02d%02d%04d", wj, wm, wa);
          else
            cds += String.format("|%02d%02d%04d", wj, wm, wa);
          String ttyp = m.group(8);
          String wmsg = m.group(9);
          if (ttyp.equals("spiral") || ttyp.equals("s"))
            wt = WidgetCalendarCountdown.gridspiral;
          else if (ttyp.equals("grid") || ttyp.equals("g"))
            wt = WidgetCalendarCountdown.gridrectangle;
          else if (ttyp.equals("linear") || ttyp.equals("l"))
            wt = WidgetCalendarCountdown.gridbar;
          //int uu = (int) (0.80f * u);
          int hh = (int) (0.70f * ((u * ww) / (wh)));
          ele = new Element(true, Element.element_media);
          ele.media.address = dossiername + wmsg + " " + wj + " " + wm;
          ele.media.ordnerAddress = dossiername;
          ele.media.isOnline = Media.online_no;
          ele.media.type = Media.media_button;
          ele.media.addressToGetPreviewFullSize = ele.media.address;
          ele.media.addressToGetThumbnail = ele.media.address;
          ele.media.filmstripCount = 2;
          ele.media.buttonaction = new String[]{
              "wh= w " + hh + " h " + u,
              "countdown= j " + wj + " m " + wm + " a " + wa + " t " + wt + " txt " + wmsg,
          };
          elementList.add(ele);
        }

        ele = new Element(true, Element.element_media);
        ele.media.address = dossiername + "time1";
        ele.media.ordnerAddress = dossiername;
        ele.media.isOnline = Media.online_no;
        ele.media.type = Media.media_button;
        ele.media.addressToGetPreviewFullSize = ele.media.address;
        ele.media.addressToGetThumbnail = ele.media.address;
        ele.media.filmstripCount = 2;
        ele.media.buttonaction = new String[]{
                "wh= w " + u + " h " + u,
                "drawrect= r 0 g 0 b 0 s 0 xi 0 xf 1 yi 0 yf 1",
                "dateformat= r 1.0 g 0.7 b 0.4 s 0.07 r x 0.5 y 0.1 txt dd",
                "dateformat= r 1.0 g 0.4 b 0.4 s 0.07 l x 0.5 y 0.1 txt  MM",
                "dateformat= r 1.0 g 1.0 b 0.4 s 0.07 c x 0.5 y 0.17 txt EEEE",
                "drawtarc= r 0.4 g 0.6 b 0.4 s 0.01 xi 0.1 xf 0.9 yi 0.1 yf 0.9 a 0.5 t 13 c y",
                "drawtarc= r 0.5 g 0.9 b 0.5 s 0.01 xi 0.2 xf 0.8 yi 0.2 yf 0.8 a 2.5 t 12 c y",
                "drawtarc= r 0.6 g 1.0 b 0.6 s 0.01 xi 0.3 xf 0.7 yi 0.3 yf 0.7 a 5 t 10 c y",
                "drawtarc= r 1.0 g 1.0 b 0.4 s 0.01 xi 0.01 xf 0.99 yi 0.01 yf 0.99 a 1 t 7 c n",
                "drawtarc= r 1.0 g 0.7 b 0.4 s 0.01 xi 0.03 xf 0.97 yi 0.03 yf 0.97 a 1 t 5 c n",
                "drawtarc= r 1.0 g 0.4 b 0.4 s 0.01 xi 0.05 xf 0.95 yi 0.05 yf 0.95 a 1 t 2 c n",
                //"runcmd= cmd cat /sys/class/power_supply/battery/capacity r 0.6 g 0.6 b 0.6 s 0.066 c x 0.33 y 0.3",
        };
        elementList.add(ele);

        ele = new Element(true, Element.element_media);
        ele.media.address = dossiername + "monitor 1h";
        ele.media.ordnerAddress = dossiername;
        ele.media.isOnline = Media.online_no;
        ele.media.type = Media.media_button;
        ele.media.addressToGetPreviewFullSize = ele.media.address;
        ele.media.addressToGetThumbnail = ele.media.address;
        ele.media.filmstripCount = 2;
        ele.media.buttonaction = new String[]{
            "wh= w " + u + " h " + (u*2),
            "monitor= t 3600",
        };
        elementList.add(ele);

        ele = new Element(true, Element.element_media);
        ele.media.address = dossiername + "time2";
        ele.media.ordnerAddress = dossiername;
        ele.media.isOnline = Media.online_no;
        ele.media.type = Media.media_button;
        ele.media.addressToGetPreviewFullSize = ele.media.address;
        ele.media.addressToGetThumbnail = ele.media.address;
        ele.media.filmstripCount = 2;
        ele.media.buttonaction = new String[]{
                "wh= w " + u + " h " + u,
                "drawrect= r 0 g 0 b 0 s 0 xi 0 xf 1 yi 0 yf 1",
                "dateformat= r 1.0 g 0.7 b 0.4 s 0.07 r x 0.5 y 0.1 txt dd",
                "dateformat= r 1.0 g 0.4 b 0.4 s 0.07 l x 0.5 y 0.1 txt  MM",
                "dateformat= r 1.0 g 1.0 b 0.4 s 0.07 c x 0.5 y 0.17 txt EEEE",
                "drawtarc= r 0.4 g 0.6 b 0.4 s 0.01 xi 0.1 xf 0.9 yi 0.1 yf 0.9 a 0.5 t 13 c y",
                "drawtarc= r 0.5 g 0.9 b 0.5 s 0.01 xi 0.2 xf 0.8 yi 0.2 yf 0.8 a 2.5 t 12 c y",
                "drawtarc= r 0.6 g 1.0 b 0.6 s 0.01 xi 0.3 xf 0.7 yi 0.3 yf 0.7 a 5 t 10 c y",
                "drawtarc= r 1.0 g 1.0 b 0.4 s 0.01 xi 0.01 xf 0.99 yi 0.01 yf 0.99 a 1 t 7 c n",
                "drawtarc= r 1.0 g 0.7 b 0.4 s 0.01 xi 0.03 xf 0.97 yi 0.03 yf 0.97 a 1 t 5 c n",
                "drawtarc= r 1.0 g 0.4 b 0.4 s 0.01 xi 0.05 xf 0.95 yi 0.05 yf 0.95 a 1 t 2 c n",
                //"runcmd= cmd cat /sys/class/power_supply/battery/capacity r 0.6 g 0.6 b 0.6 s 0.066 c x 0.33 y 0.3",
        };
        elementList.add(ele);

        ele = new Element(true, Element.element_media);
        ele.media.address = dossiername + "monitor 24h";
        ele.media.ordnerAddress = dossiername;
        ele.media.isOnline = Media.online_no;
        ele.media.type = Media.media_button;
        ele.media.addressToGetPreviewFullSize = ele.media.address;
        ele.media.addressToGetThumbnail = ele.media.address;
        ele.media.filmstripCount = 2;
        ele.media.buttonaction = new String[]{
            "wh= w " + u + " h " + (u*2),
            "monitor= t 86400",
        };
        elementList.add(ele);

        ele = new Element(true, Element.element_media);
        ele.media.address = dossiername + "time3";
        ele.media.ordnerAddress = dossiername;
        ele.media.isOnline = Media.online_no;
        ele.media.type = Media.media_button;
        ele.media.addressToGetPreviewFullSize = ele.media.address;
        ele.media.addressToGetThumbnail = ele.media.address;
        ele.media.filmstripCount = 2;
        ele.media.buttonaction = new String[]{
                "wh= w " + u + " h " + u,
                "drawrect= r 0 g 0 b 0 s 0 xi 0 xf 1 yi 0 yf 1",
                "dateformat= r 1.0 g 0.7 b 0.4 s 0.07 r x 0.5 y 0.1 txt dd",
                "dateformat= r 1.0 g 0.4 b 0.4 s 0.07 l x 0.5 y 0.1 txt  MM",
                "dateformat= r 1.0 g 1.0 b 0.4 s 0.07 c x 0.5 y 0.17 txt EEEE",
                "drawtarc= r 0.4 g 0.6 b 0.4 s 0.01 xi 0.1 xf 0.9 yi 0.1 yf 0.9 a 0.5 t 13 c y",
                "drawtarc= r 0.5 g 0.9 b 0.5 s 0.01 xi 0.2 xf 0.8 yi 0.2 yf 0.8 a 2.5 t 12 c y",
                "drawtarc= r 0.6 g 1.0 b 0.6 s 0.01 xi 0.3 xf 0.7 yi 0.3 yf 0.7 a 5 t 10 c y",
                "drawtarc= r 1.0 g 1.0 b 0.4 s 0.01 xi 0.01 xf 0.99 yi 0.01 yf 0.99 a 1 t 7 c n",
                "drawtarc= r 1.0 g 0.7 b 0.4 s 0.01 xi 0.03 xf 0.97 yi 0.03 yf 0.97 a 1 t 5 c n",
                "drawtarc= r 1.0 g 0.4 b 0.4 s 0.01 xi 0.05 xf 0.95 yi 0.05 yf 0.95 a 1 t 2 c n",
                //"runcmd= cmd cat /sys/class/power_supply/battery/capacity r 0.6 g 0.6 b 0.6 s 0.066 c x 0.33 y 0.3",
        };
        elementList.add(ele);

        m = Gallery.lockscreenmonitorwidgetsetpattern.matcher(lockscreenmonitorwidgetset);
        while (m.find()) {
            mt = Integer.parseInt(m.group(5));
            ele = new Element(true, Element.element_media);
            ele.media.address = dossiername + "monitor " + mt;
            ele.media.ordnerAddress = dossiername;
            ele.media.isOnline = Media.online_no;
            ele.media.type = Media.media_button;
            ele.media.addressToGetPreviewFullSize = ele.media.address;
            ele.media.addressToGetThumbnail = ele.media.address;
            ele.media.filmstripCount = 2;
            ele.media.buttonaction = new String[]{
              "wh= w " + u + " h " + (u * 2),
              "monitor= t " + mt,
            };
            elementList.add(ele);
        }

    }

    public void reloadBookmarksFromPreferences(SharedPreferences preferences, AppDatabase db){
        int nombredossierbookmarked = preferences.getInt("nombredossierbookmarked", 0);
        if (nombredossierbookmarked > 0) {
            Element ele = new Element(true, Element.element_ordner);
            String dossiername = Gallery.virtualBookmarkFolder;
            ele.ordner.address = dossiername;
            elementList.add(ele);
            displayedOrdnerList.add(dossiername);
            for (int i = nombredossierbookmarked; i >= 0; --i) {
                ele = new Element(true, Element.element_media);
                ele.media.address = preferences.getString("fichierbookmarked" + i, null);
                ele.media.ordnerAddress = dossiername;
                ele.media.bookmarkToOrdner = preferences.getString("dossierbookmarked" + i, null);
                if (ele.media.address != null) {
                    if (Gallery.couldBeOnline(ele.media.address)) {
                        ele.media.isOnline = Media.online_apache;
                        ele.media.type = Media.getType(ele.media.address);
                        try {
                          ele.media.addressescaped = URLDecoder.decode(ele.media.address, "utf-8");
                        } catch (UnsupportedEncodingException e) {
                          e.printStackTrace();
                        }
                        //llog.d(TAG, ele.media.address + " " + ele.media.type);
                        //ele.media.addressToGetPreviewFullSize = ele.media.address;
                        //ele.media.addressToGetThumbnail = ele.media.address + ".thmb";
                    } else {
                        ele.media.isOnline = Media.online_no;
                        ele.media.type = Media.getType(ele.media.address);
                        ele.media.addressToGetLibextractorsThumbnail = db.dbFichier().fichierGetUID(ele.media.address);
                    }
                    elementList.add(ele);
                }
            }
        }
    }

    public List<Element> reloadTorCollectionFromDisk(String dossiercollections){
        List<Element> coll = new ArrayList<>();
        FileInputStream fis;
        try {
            fis = new FileInputStream(dossiercollections + "/tempcollection.pselsp");
            int c = (int) fis.getChannel().size();
            llog.d(TAG, "reloadTorCollectionFromDisk file size = " + c);

            byte[] buffer = new byte[c];
            int n = fis.read(buffer);
            llog.d(TAG, "reloadTorCollectionFromDisk read = " + n);

            int i = 0;

            /** collection header **/
            int printnamel = toInt(buffer, i);
            i += 4;
            String printname = new String(buffer, i, printnamel, StandardCharsets.UTF_8);
            i += printnamel;

            int keywordsl = toInt(buffer, i);
            i += 4;
            String keywords = new String(buffer, i, keywordsl, StandardCharsets.UTF_8);
            i += keywordsl;

            llog.d(TAG, "collection name " + printname + " keywords " + keywords);

            Element el = null;
            while (i < c) {

                boolean add = false;
                if (buffer[i] == 0x01)
                    add = true;
                i += 1;

                int type = Element.element_media;
                if (buffer[i] == 'u')
                    type = 3;
                else if (buffer[i] == 'c')
                    type = Element.element_collection;
                else if (buffer[i] == 'd')
                    type = Element.element_ordner;
                else
                    type = Element.element_media;
                i += 1;


                if (type == Element.element_ordner) {

                    printnamel = toInt(buffer, i);
                    i += 4;
                    printname = new String(buffer, i, printnamel, StandardCharsets.UTF_8);
                    i += printnamel;

                    el = new Element(add, type);
                    el.ordner.address = printname;
                    coll.add(el);

                } else if (type == 3 && el != null) { // updatablefile is actually list of medias with additionnal property

                    int count = toInt(buffer, i);
                    i += 4;
                    llog.d(TAG, "number of entries " + count);

                    for (int k = 0; k < count && i < c; k++) {
                        int addressl = toInt(buffer, i);
                        i += 4;
                        String address = new String(buffer, i, addressl, StandardCharsets.UTF_8);
                        i += addressl;
                        llog.d(TAG, addressl + " : address " + address);

                        Element ele = new Element(add, type);
                        ele.media.printName = ThreadMiniature.epochToDate(address);
                        ele.media.ordnerPrintName = el.ordner.address;
                        ele.media.address = address;
                        ele.media.ordnerAddress = el.ordner.address;
                        ele.media.isOnline = Media.online_apache;
                        ele.media.type = Media.getType(address);
                        ele.media.addressToGetPreviewFullSize = address + ".thmb";
                        // necessaire pour getonline if (wantminiature && media.minipic == null) {return null;
                        ele.media.addressToGetThumbnail = address + ".thmb";
                        ele.media.addressToGetLibextractorsThumbnail = 10000000;
                        ele.media.isInsideAnArchive = false;
                        ele.media.isSelected = false;
                        coll.add(ele);
                    }

                }



                /*
                collection.add(new Element(nom, isfolder, isselected, isapache, isjson));
                if (isselected)
                    selectionnumber += 1;
                else
                    selectionnumber -= 1;
                 */

                //i++;
            }

            fis.close();
        } catch (Exception e) {
            llog.d(TAG, "loadtorcollection Exception " + e.toString());
        }
        return coll;
    }

    private void reloadOldCollectionFromDisk(String dossdessin, String currcollection){
        FileInputStream fis;
        try {
            fis = new FileInputStream(dossdessin + currcollection + Gallery.collectionextension);
            int filesize = (int) fis.getChannel().size();
            llog.d(TAG, "reloadCollectionFromDisk file size = " + filesize);

            byte[] buffer;

            int n = 0;
            int t = 0;
            int i = 0;
            int c = 0;
            while (t < filesize) {

                //ByteBuffer data = ByteBuffer.wrap(buffer);
                //llog.d(TAG, "pos " + data.position() + " lim " + data.limit());

                //String prout = "";

                buffer = new byte[4];
                n = fis.read(buffer);
                if (n != 4)
                    break;
                int entryl = toInt(buffer, 0);
                //for (int j = 0 ; j < 4 ; j++)
                //  prout += String.format("%02x", buffer[j]);
                //llog.d(TAG, prout);

                buffer = new byte[entryl - 4]; // enlève les 4 avant
                n = fis.read(buffer);
                if (n != entryl - 4)
                    break;

                t += n + 4;
                c += 1;


                //llog.d(TAG, "entry " +c + " size " + entryl + " " + t + " / " + filesize);
                i = 0;

                boolean isfolder = false;
                if (buffer[i] == 1)
                    isfolder = true;
                i += 1;

                int isonline = Media.online_no;
                String fichier = "";
                String dossier = "";
                if (isfolder) {
                    int dossierl = toInt(buffer, i);
                    i += 4;
                    dossier = new String(buffer, i, dossierl, StandardCharsets.UTF_8);
                    i += dossierl;

                    if (buffer[i] == 1)
                        isonline = Media.online_apache;
                    i += 1;

                    if (buffer[i] == 1)
                        isonline = Media.online_json;
                    i += 1;
                } else {
                    int fichierl = toInt(buffer, i);
                    i += 4;
                    fichier = new String(buffer, i, fichierl, StandardCharsets.UTF_8);
                    i += fichierl;

                    int dossierl = toInt(buffer, i);
                    i += 4;
                    dossier = new String(buffer, i, dossierl, StandardCharsets.UTF_8);
                    i += dossierl;
                }

                boolean isselected = false;
                if (buffer[i] == 1)
                    isselected = true;
                i += 1;

                /*Element ele = new Element(isselected, isfolder);
                if (ele.isadirectory) {
                    ele.dossier = dossier;
                    ele.isonline = isonline;
                } else {
                    ele.media.fichier = fichier;
                    ele.media.dossier = dossier;
                }
                collection.add(ele);

                if (isselected)
                    selectionnumber += 1;
                else
                    selectionnumber -= 1;*/

                //llog.d(TAG, fichier + " " + dossier + " " + isselected);
                //llog.d(TAG, prout);

            }

            fis.close();
        } catch (Exception e) {
            llog.d(TAG, "loadcollection delete " + currcollection + ".sel Exception " + e.toString());
        }
    }

}
