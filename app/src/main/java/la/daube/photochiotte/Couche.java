package la.daube.photochiotte;

import android.graphics.Bitmap;
import android.graphics.Canvas;

import java.io.Serializable;
import java.util.ArrayList;

class Couche {
  
  public boolean active = false;
  
  public boolean isaphoto;
  
  public Bitmap originalbitmap;
  public Bitmap currentbitmap;
  public Canvas canvas;
  
  public float x;
  public float y;
  public float originalw;
  public float originalh;
  public float scale;
  
}