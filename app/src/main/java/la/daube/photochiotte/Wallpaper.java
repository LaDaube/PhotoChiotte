package la.daube.photochiotte;

import android.app.WallpaperManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.os.Build;
import android.os.Handler;
import androidx.preference.PreferenceManager;

import android.service.wallpaper.WallpaperService;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Surface;
import android.view.SurfaceHolder;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import androidx.room.Room;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Wallpaper extends WallpaperService {
  
  private final static String TAG = "YYYwp";
  public final static String lockscreenprefixe = "lockscreenimg";
  public final static String wallpaperprefixe = "wallpaperimg";

  @Override
  public Engine onCreateEngine() {
    llog.d(TAG, "onCreateEngine");
    return new MyWallpaperEngine();
  }

  @Override
  public void onDestroy() {
    llog.d(TAG, "WallpaperService ondestroy");
  }

  private class MyWallpaperEngine extends Engine {

    private volatile Bitmap lockscreenbitmap = null;
    private volatile Canvas lockscreencanvas = null;
    private volatile Bitmap wallpaperbitmap = null;
    private volatile Canvas wallpapercanvas = null;
  
    private volatile boolean mVisible = false;
  
    private volatile boolean lockscreen = false;
    private volatile int lockscreenimgl = 0;
  
    private volatile boolean wallpaper = false;
    private volatile int wallpaperimgl = 0;

    private final WallpaperManager wallpaperManager;

    private volatile ArrayList<Ordner> wallpapercollection = null;
    private volatile String wallpapercollectionname = null;
    private volatile ArrayList<Ordner> lockscreencollection = null;
    private volatile String lockscreencollectionname = null;

    private volatile boolean justbooted = false;

    private volatile int drawspeed;   // thread call delay time
    private volatile int drawspeeddefault;   // thread call delay time
    private volatile boolean touchenabled;
    private volatile boolean cleareachtime;
    private volatile boolean optionhardwareaccelerationb;
    private volatile float wallpaperratioscreenmin;
    private volatile float wallpaperratioscreenmax;
    private volatile float lockscreenratioscreenmin;
    private volatile float lockscreenratioscreenmax;

    private boolean lockscreencalendarwidget;
    private boolean lockscreencountdownwidget;
    private boolean lockscreenmonitorwidget;
    private boolean wallpapercalendarwidget;
    private boolean wallpapercountdownwidget;
    private boolean wallpapermonitorwidget;
    private String lockscreencalendarwidgetset;
    private String lockscreencountdownwidgetset;
    private String lockscreenmonitorwidgetset;

    private volatile int lockscreenw;
    private volatile int lockscreenh;
    private volatile int wallpaperw;
    private volatile int wallpaperh;
    private volatile int screenmin;
    private volatile int screenmax;

    private volatile boolean recycleallpictures = true;

    private final Paint paint = new Paint();

    private volatile long lastupdate = System.currentTimeMillis() - 1000000L;

    private final static Handler handler = new Handler();; // this is to handle the thread

    private final Gallery model;
    private final Surf mysurf;


    public MyWallpaperEngine() {
      llog.d(TAG, "MyWallpaperEngine +++++++++++++++++++++++ create base bitmap");


      model = new Gallery(null);
      mysurf = new Surf();
      mysurf.myid = 0;
      model.surf.add(mysurf);

      model.activitycontext = getBaseContext();

      model.db = Room.databaseBuilder(model.activitycontext, AppDatabase.class, "database-name").fallbackToDestructiveMigration().build();
      wallpaperManager = WallpaperManager.getInstance(model.activitycontext);

      model.preferences = PreferenceManager.getDefaultSharedPreferences(model.activitycontext);
      model.dossiercache = model.preferences.getString("dossiercache", "/storage/dummy");
      model.dossierscreenshot = model.dossiercache + Gallery.staticdossierscreenshot;
      model.dossierdessin = model.dossiercache + Gallery.staticdossierdessin;
      model.dossiercollections = model.dossiercache + Gallery.staticdossiercollections;
      model.dossierconfig = model.dossiercache + Gallery.staticdossierconfig;
      model.dossierbitmaptemp = model.dossiercache + Gallery.staticdossierbitmaptemp;
      model.dossierminiature = model.dossiercache + Gallery.staticdossierminiature;
      model.internalStorageDir = model.preferences.getString("internalStorageDir", "/storage/dummy");
      model.allowfakebitmaps = false;

      touchenabled = model.preferences.getBoolean("touchenabled",false);
      cleareachtime = model.preferences.getBoolean("cleareachtime",false);
      wallpaperratioscreenmin = model.preferences.getFloat("wallpaperminratio", 20.0f) / 100.0f;
      wallpaperratioscreenmax = model.preferences.getFloat("wallpapermaxratio", 60.0f) / 100.0f;
      drawspeeddefault = model.preferences.getInt("wallpaperspeed", 30) * 1000;
      lockscreenratioscreenmin = model.preferences.getFloat("lockscreenminratio", 20.0f) / 100.0f;
      lockscreenratioscreenmax = model.preferences.getFloat("lockscreenmaxratio", 60.0f) / 100.0f;
      drawspeed = drawspeeddefault;
      wallpapercalendarwidget = model.preferences.getBoolean("wallpapercalendarwidget", false);
      wallpapercountdownwidget = model.preferences.getBoolean("wallpapercountdownwidget", false);
      wallpapermonitorwidget = model.preferences.getBoolean("wallpapermonitorwidget", false);
      lockscreencalendarwidget = model.preferences.getBoolean("lockscreencalendarwidget", false);
      lockscreencountdownwidget = model.preferences.getBoolean("lockscreencountdownwidget", false);
      lockscreenmonitorwidget = model.preferences.getBoolean("lockscreenmonitorwidget", false);
      lockscreencalendarwidgetset = model.preferences.getString("lockscreencalendarwidgetset", Gallery.lockscreencalendarwidgetset);
      lockscreencountdownwidgetset = model.preferences.getString("lockscreencountdownwidgetset", Gallery.lockscreencountdownwidgetset);
      lockscreenmonitorwidgetset = model.preferences.getString("lockscreenmonitorwidgetset", Gallery.lockscreenmonitorwidgetset);


      justbooted = true;

      DisplayMetrics displayMetrics = getApplicationContext().getResources().getDisplayMetrics();
      lockscreenw = displayMetrics.widthPixels;
      lockscreenh = displayMetrics.heightPixels;
      model.unmillimetre = displayMetrics.xdpi / 25.4f;
      model.bigScreenWidth = lockscreenw;
      model.bigScreenHeight = lockscreenh;
      mysurf.mywidth = lockscreenw;
      mysurf.myheight = lockscreenh;
      model.buttontextsize = model.preferences.getFloat("buttontextsize", 1.0f);
      wallpaperw = lockscreenw;
      wallpaperh = lockscreenh;
      //screenmin = model.preferences.getInt("screenmin", 1);
      //screenmax = model.preferences.getInt("screenmax", 1);
      model.setallthepaints();
      llog.d(TAG, "       -> "
              + lockscreenw + "x" + lockscreenh
              + " : " + screenmin + "x" + screenmax
      );

      //paint.setDither(true);

      handler.removeCallbacks(drawRunner);
      handler.post(drawRunner);

    }

    // the tread responsibe for drawing this thread get calls every time
    // drawspeed vars set the execution speed
    private final Runnable drawRunner = new Runnable() {
      @Override
      public void run() {
        //llog.d(TAG, "drawrunner " + mVisible);
        if (mVisible) {
          new Thread(new Runnable() {
            public void run() {

              /*if (recycleallpictures) {
                recycleallpictures = false;
                if (lockscreenbitmap != null) {
                  if (!lockscreenbitmap.isRecycled())
                    lockscreenbitmap.recycle();
                  lockscreenbitmap = null;
                }
                if (wallpaperbitmap != null) {
                  if (!wallpaperbitmap.isRecycled())
                    wallpaperbitmap.recycle();
                  wallpaperbitmap = null;
                }
              }*/

              long currtime = System.currentTimeMillis();
              long filmstripreftime = currtime - (currtime % model.filmstripanimatetime);
              int surfl = model.surf.size();
              for (int i = 0; i < surfl ; i++) {
                Surf thissurf = model.surf.get(i);
                thissurf.filmstripreftime = filmstripreftime;
              }

              boolean pushupdate = false;

              optionhardwareaccelerationb = model.preferences.getBoolean("optionhardwareaccelerationbrowser", optionhardwareaccelerationb);
              wallpaperratioscreenmin = model.preferences.getFloat("wallpaperminratio", 20.0f) / 100.0f;
              wallpaperratioscreenmax = model.preferences.getFloat("wallpapermaxratio", 60.0f) / 100.0f;
              lockscreenratioscreenmin = model.preferences.getFloat("lockscreenminratio", 20.0f) / 100.0f;
              lockscreenratioscreenmax = model.preferences.getFloat("lockscreenmaxratio", 60.0f) / 100.0f;
              if (drawspeed == drawspeeddefault) {
                drawspeeddefault = model.preferences.getInt("wallpaperspeed", 30) * 1000;
                drawspeed = drawspeeddefault;
              }
              wallpapercalendarwidget = model.preferences.getBoolean("wallpapercalendarwidget", false);
              wallpapercountdownwidget = model.preferences.getBoolean("wallpapercountdownwidget", false);
              wallpapermonitorwidget = model.preferences.getBoolean("wallpapermonitorwidget", false);
              lockscreencalendarwidget = model.preferences.getBoolean("lockscreencalendarwidget", false);
              lockscreencountdownwidget = model.preferences.getBoolean("lockscreencountdownwidget", false);
              lockscreenmonitorwidget = model.preferences.getBoolean("lockscreenmonitorwidget", false);
              lockscreencalendarwidgetset = model.preferences.getString("lockscreencalendarwidgetset", Gallery.lockscreencalendarwidgetset);
              lockscreencountdownwidgetset = model.preferences.getString("lockscreencountdownwidgetset", Gallery.lockscreencountdownwidgetset);
              lockscreenmonitorwidgetset = model.preferences.getString("lockscreenmonitorwidgetset", Gallery.lockscreenmonitorwidgetset);

              lockscreen = model.preferences.getBoolean(lockscreenprefixe, false);
              if (lockscreen) {
                if (lockscreencollectionname == null)
                  lockscreencollectionname = model.preferences.getString("lockscreencollection", null);
                if (lockscreencollectionname != null) {
                  if (lockscreencollection == null) {
                    model.collections.add(new Collection(model.dossiercache + Gallery.staticdossiercollections + lockscreencollectionname + Gallery.collectionextension, lockscreencollectionname, model));
                    lockscreencollection = model.mergeCollectionsRemoveEmpty(model.dossiercache + Gallery.staticdossiercollections + lockscreencollectionname + Gallery.collectionextension);
                    if (lockscreencollection != null)
                      lockscreenimgl = lockscreencollection.size();
                    llog.d(TAG, "reload lockscreen collection " + lockscreencollectionname + " : " + lockscreenimgl);
                  }
                  if (lockscreencollection != null) {
                    try {
                      drawlockscreen();
                    } catch (Exception e) {
                      e.printStackTrace();
                      llog.d(TAG, "error lockscreen " + e);
                    }
                    pushupdate = true;
                  }
                }
              }

              wallpaper = model.preferences.getBoolean(wallpaperprefixe, false);
              if (wallpaper) {
                if (wallpapercollectionname == null)
                  wallpapercollectionname = model.preferences.getString("wallpapercollection", null);
                if (wallpapercollectionname != null) {
                  if (wallpapercollection == null) {
                    model.collections.add(new Collection(model.dossiercache + Gallery.staticdossiercollections + wallpapercollectionname + Gallery.collectionextension, wallpapercollectionname, model));
                    wallpapercollection = model.mergeCollectionsRemoveEmpty(model.dossiercache + Gallery.staticdossiercollections + wallpapercollectionname + Gallery.collectionextension);
                    if (wallpapercollection != null)
                      wallpaperimgl = wallpapercollection.size();
                    llog.d(TAG, "reload wallpaper collection " + wallpapercollectionname + " : " + wallpaperimgl);
                  }
                  if (wallpapercollection != null) {
                    try {
                      drawwallpaper();
                    } catch (Exception e) {
                      e.printStackTrace();
                      llog.d(TAG, "error wallpaper " + e);
                    }
                    pushupdate = true;
                  }
                }
              }

              if (recycleallpictures)
                recycleallpictures = false;

              lastupdate = System.currentTimeMillis();

              if (pushupdate) {
                handler.removeCallbacks(drawRunner);
                handler.postDelayed(drawRunner, drawspeed);
              }


            }
          }).start();
        }
        
      }
    };
    
    /*
    @Override
    public void onTouchEvent(MotionEvent event) {
      //llog.d(TAG, "onTouchEvent");
      if (touchenabled) {
        float x = event.getX();
        float y = event.getY();
        if(buffercanvas != null) {
          buffercanvas = null;
          buffercanvasbitmap.recycle();
        }
        super.onTouchEvent(event);
      }
    }
    */

    @Override
    public void onCreate(SurfaceHolder surfaceHolder) {
      llog.d(TAG, "onCreate");
      setTouchEventsEnabled(false);
      setOffsetNotificationsEnabled(true);
      super.onCreate(surfaceHolder);
    }
  
    @Override
    public void onSurfaceRedrawNeeded(SurfaceHolder holder) {
      llog.d(TAG, "onSurfaceRedrawNeeded");
      handler.removeCallbacks(drawRunner);
      handler.post(drawRunner);
      super.onSurfaceRedrawNeeded(holder);
    }
  
    //@Override
    //public void onOffsetsChanged(float xOffset, float yOffset, float xStep, float yStep, int xPixels, int yPixels) {
      //Log.i(TAG, "onOffsetsChanged offset "+xOffset+" "+yOffset+" ; step "+xStep+" "+yStep+" ; pixels "+xPixels+" "+yPixels);
      /**
       *    1600x2560
       *    vertical à gauche :
       *
       *    si on ferme le cycle c'est direct :
       *    onOffsetsChanged offset 1.0 0.5 ; step 1.0 1.0 ; pixels -1600 0
       *    onOffsetsChanged offset 0.0 0.5 ; step 1.0 1.0 ; pixels 0 0
       *
       *    si on passe d'un écran à l'autre normal c'est progressif tout le temps où l'on touche :
       *    onOffsetsChanged offset 1.0 0.5 ; step 1.0 1.0 ; pixels -1600 0
       *    onOffsetsChanged offset 0.985 0.5 ; step 1.0 1.0 ; pixels -1576 0
       *    ... tout le temps où l'on bouge l'écran ...
       *    ... si ça charge l'écran on lag
       *    onOffsetsChanged offset 0.0025 0.5 ; step 1.0 1.0 ; pixels -4 0
       *    onOffsetsChanged offset 0.0 0.5 ; step 1.0 1.0 ; pixels 0 0
       *
       *
       *    horizontal :
       *    0.0         0.5 ; step 1.0 1.0 ; pixels 0 -480
       *    0.008203125 0.5 ; step 1.0 1.0 ; pixels -5 -480
       *    ... touche bouge écran ...
       *    0.9964844   0.5 ; step 1.0 1.0 ; pixels -638 -480
       *    1.0         0.5 ; step 1.0 1.0 ; pixels -640 -480
       *
       *
       *    3 écrans :
       *    onOffsetsChanged offset 0.0        0.5 ; step 0.5 1.0 ; pixels 0 -480
       *    onOffsetsChanged offset 0.4904297  0.5 ; step 0.5 1.0 ; pixels -314 -480
       *    ...
       *    onOffsetsChanged offset 0.49746093 0.5 ; step 0.5 1.0 ; pixels -318 -480
       *    onOffsetsChanged offset 0.5        0.5 ; step 0.5 1.0 ; pixels -320 -480
       *    onOffsetsChanged offset 0.50820315 0.5 ; step 0.5 1.0 ; pixels -325 -480
       *    ...
       *    onOffsetsChanged offset 0.99746096 0.5 ; step 0.5 1.0 ; pixels -638 -480
       *    onOffsetsChanged offset 1.0        0.5 ; step 0.5 1.0 ; pixels -640 -480
       *
       *    ferme cycle :
       *    onOffsetsChanged offset 0.0 0.5 ; step 0.5 1.0 ; pixels 0 -480
       *
       *
       */
    //  super.onOffsetsChanged(xOffset, yOffset, xStep, yStep, xPixels, yPixels);
    //}
  
    @Override
    public void onSurfaceCreated(SurfaceHolder holder) {
      recycleallpictures = true;
      if (holder != null) {
        Rect rect = holder.getSurfaceFrame();
        if (rect != null) {
          if (rect.width() > 0 && rect.height() > 0) {
            lockscreenw = rect.width();
            lockscreenh = rect.height();
            wallpaperw = rect.width();
            wallpaperh = rect.height();
          }
        }
      }
      llog.d(TAG, "onSurfaceCreated " + lockscreenw + "x" + lockscreenh);
      super.onSurfaceCreated(holder);
    }

    @Override
    public void onSurfaceChanged(SurfaceHolder holder, int format, int mwidth, int mheight) {
      handler.removeCallbacks(drawRunner);
      llog.d(TAG, "onSurfaceChanged " + lockscreenw + "x" + lockscreenh + " -> " + mwidth + "x" + mheight);
      lockscreenw = mwidth;
      lockscreenh = mheight;
      wallpaperw = mwidth;
      wallpaperh = mheight;
      /*if (wallpaperw > wallpaperh) {
        if (screenmax > wallpaperw) {
          wallpaperw = screenmax;
          lockscreenw = screenmax;
        }
        if (screenmin > wallpaperh) {
          wallpaperh = screenmin;
          lockscreenh = screenmin;
        }
      } else {
        if (screenmax > wallpaperh) {
          wallpaperh = screenmax;
          lockscreenh = screenmax;
        }
        if (screenmin > wallpaperw) {
          wallpaperw = screenmin;
          lockscreenw = screenmin;
        }
      }*/
      recycleallpictures = true;
      handler.post(drawRunner);
      super.onSurfaceChanged(holder, format, mwidth, mheight);
    }

    @Override
    public void onSurfaceDestroyed(SurfaceHolder holder) {
      llog.d(TAG, "onSurfaceDestroyed");
      handler.removeCallbacks(drawRunner);
      recycleallpictures = true;
      mVisible = false;
      if (wallpaperbitmap != null) {
        if (!wallpaperbitmap.isRecycled())
          wallpaperbitmap.recycle();
        wallpaperbitmap = null;
      }
      if (lockscreenbitmap != null) {
        if (!lockscreenbitmap.isRecycled())
          lockscreenbitmap.recycle();
        lockscreenbitmap = null;
      }
      lastupdate = System.currentTimeMillis() - 1000000L;
      stopSelf();
      super.onSurfaceDestroyed(holder);
    }

    @Override
    public void onDestroy(){
      llog.d(TAG, "MyWallpaperEngine ondestroy");
    }
    
    @Override
    public void onVisibilityChanged(boolean visible) {
      //llog.d(TAG, "MyWallpaperEngine onvisibilitychanged " + visible);
      handler.removeCallbacks(drawRunner);
      mVisible = visible;
      if (mVisible) {
        long diftemps = System.currentTimeMillis() - lastupdate;
        if (diftemps < drawspeed) {
          long difference = drawspeed - diftemps;
          if (difference < 10000) {
            difference = 10000;
          }
          handler.postDelayed(drawRunner, difference);
          //llog.d(TAG, "onVisibilityChanged load in "+difference);
        } else {
          handler.post(drawRunner);
          //llog.d(TAG, "onVisibilityChanged load now");
        }
      }
      super.onVisibilityChanged(visible);
    }
    
    private void drawwallpaper() {
      //llog.d(TAG, "MyWallpaperEngine drawwallpaper");
      if (wallpaperimgl<=0 || wallpaperw < 4 || wallpaperh < 4) {
        llog.d(TAG, "drawwallpaper wallpaperimgl<="+wallpaperimgl+" || wallpaperw < "+wallpaperw+" || wallpaperh < "+wallpaperh);
        return;
      }
      int selfichl = wallpapercollection.size();
      if (selfichl == 0) {
        llog.d(TAG, "selfichl 0");
        return;
      }
      int cetteimage = Gallery.rand.nextInt(selfichl);
      String cebitmap = null;
      int isonline = Media.online_no;
      Ordner ordner = wallpapercollection.get(cetteimage);
      if (ordner.mediaListCount <= 0) { // can be a sel to another coll, load
        llog.d(TAG, "pick a file inside this folder " + ordner.address);
        return;
      }
      cetteimage = Gallery.rand.nextInt(ordner.mediaListCount);
      Media media = ordner.mediaList.get(cetteimage);
      cebitmap = media.address;
      isonline = media.isOnline;
      drawspeed = drawspeeddefault;
      if (cebitmap == null) {
        llog.d(TAG, "media.address = null");
        if (Gallery.isConnectedToInternet) {
          if ((isonline == Media.online_apache || isonline == Media.online_json) && justbooted) {
            llog.d(TAG, "try again soon");
            drawspeed = 3000;
          } else {
            drawspeed /= 2;
          }
        }
        return;
      }
      for (String encache : mysurf.getcachekeyset())
        mysurf.removekeyfromcache(encache);
      boolean mediasuccess = ThreadMiniature.getbigpicture(media, mysurf.myid, model);
      if (!mediasuccess) {
        llog.d(TAG, "could not get big picture");
        return;
      }
      Bitmap bmap = null;
      if (!mysurf.isincache(media.address)) {
        llog.d(TAG, "could not get big picture in cache");
        return;
      }
      bmap = mysurf.getcachedbitmap(media.address);
      if (bmap == null) {
        llog.d(TAG, "getcachedbitmap null " + cebitmap);
        if (Gallery.isConnectedToInternet) {
          if ((isonline == Media.online_apache || isonline == Media.online_json) && justbooted) {
            llog.d(TAG, "try again soon");
            drawspeed = 3000;
          } else {
            drawspeed /= 2;
          }
        }
        return;
      }
      if (bmap.isRecycled()) {
        llog.d(TAG, "getthefullbitmap isRecycled");
        return;
      }
      justbooted = false;
      int bmpw = bmap.getWidth();
      int bmph = bmap.getHeight();
      float scalew = ((float)wallpaperw) / ((float)bmpw);
      float scaleh = ((float)wallpaperh) / ((float)bmph);
      float bscale;
      if (0.98f < wallpaperratioscreenmin && wallpaperratioscreenmin < 1.02f) {
        if (wallpaperw >= wallpaperh) {
          if (bmpw >= bmph) {
            if (scalew > scaleh)
              bscale = scalew;
            else
              bscale = scaleh;
          } else {
            if (scalew < scaleh)
              bscale = scalew;
            else
              bscale = scaleh;
          }
        } else {
          if (bmpw < bmph) {
            if (scalew > scaleh)
              bscale = scalew;
            else
              bscale = scaleh;
          } else {
            if (scalew < scaleh)
              bscale = scalew;
            else
              bscale = scaleh;
          }
        }
        bmpw = (int) ((float) bmpw * bscale);
        bmph = (int) ((float) bmph * bscale);
      } else {
        float reduit = wallpaperratioscreenmin + (wallpaperratioscreenmax - wallpaperratioscreenmin) * Gallery.rand.nextFloat();
        if (scalew < scaleh)
          bscale = scalew;
        else
          bscale = scaleh;
        bmpw = (int) ((float) bmpw * bscale * reduit);
        bmph = (int) ((float) bmph * bscale * reduit);
      }
      if (bmap.isRecycled()) {
        llog.d(TAG, "getthefullbitmap(cebitmap, lockscreenw, lockscreenh) isRecycled");
        return;
      } // crash bmap is recycled
      Bitmap bitmap = Bitmap.createScaledBitmap(bmap, bmpw, bmph, false);
      int x = 0;
      int dx = wallpaperw - bmpw;
      int y = 0;
      int dy = wallpaperh - bmph;
      if (0.98f < wallpaperratioscreenmin && wallpaperratioscreenmin < 1.02f) {
        x = (int) ((wallpaperw - bmpw) * 0.5f);
        y = (int) ((wallpaperh - bmph) * 0.5f);
      } else if (dx > 0 && dy > 0) {
        x = Gallery.rand.nextInt(dx);
        y = Gallery.rand.nextInt(dy);
        if (Gallery.rand.nextInt(100) < 33) {
          for (int h = 0; h < 3; h++) {
            if (dx * 0.20f < x && x < dx * 0.80f) {
              x = Gallery.rand.nextInt(dx);
            } else {
              break;
            }
          }
        } else if (Gallery.rand.nextInt(100) < 33) {
          for (int h = 0; h < 3; h++) {
            if (dy * 0.20f < y && y < dy * 0.80f) {
              y = Gallery.rand.nextInt(dy);
            } else {
              break;
            }
          }
        }
      } else {
        llog.d(TAG, "error dx dy " + dx + "," + dy);
      }
      if (wallpaperratioscreenmin >= 0.95f || recycleallpictures) {
        if (wallpaperbitmap != null) {
          if (!wallpaperbitmap.isRecycled())
            wallpaperbitmap.recycle();
          wallpaperbitmap = null;
        }
      }
      if (wallpaperbitmap != null)
        if (wallpaperbitmap.isRecycled())
          wallpaperbitmap = null;

      if (wallpaperbitmap == null) {

        Libextractor lib = new Libextractor();
        Bitmap miniature = lib.blurrgb(cebitmap, wallpaperw, wallpaperh);
        if (miniature == null) {
          if (bitmap != null)
            if (!bitmap.isRecycled())
              bitmap.recycle();
          if (bmap != null)
            if (!bmap.isRecycled())
              bmap.recycle();
          return;
        }

        wallpaperbitmap = Bitmap.createBitmap(wallpaperw, wallpaperh, Bitmap.Config.ARGB_8888);
        wallpapercanvas = new Canvas(wallpaperbitmap);
        wallpapercanvas.drawBitmap(miniature, 0, 0, paint);

        if (!miniature.isRecycled())
          miniature.recycle();

      }

      if (bitmap != null)
        if (!bitmap.isRecycled())
          wallpapercanvas.drawBitmap(bitmap, x, y, paint);

      Bitmap fullcopytempbitmap = null;
      if (wallpapercalendarwidget || wallpapercountdownwidget || wallpapermonitorwidget) {
        if (wallpaperbitmap != null) {
          if (!wallpaperbitmap.isRecycled()) {
            fullcopytempbitmap = Bitmap.createBitmap(wallpaperbitmap);
            Canvas canv = new Canvas(fullcopytempbitmap);
            drawwidgets(canv, wallpaperw, wallpaperh, wallpapercalendarwidget, wallpapercountdownwidget, wallpapermonitorwidget);
          }
        }
      }
      try {
        final SurfaceHolder holder = getSurfaceHolder();
        if (holder != null) {
          Surface surface = holder.getSurface();
          if (surface != null) {
            if (surface.isValid()) {
              Canvas canvas;
              if (optionhardwareaccelerationb && Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
                canvas = holder.lockHardwareCanvas();
              else
                canvas = holder.lockCanvas();
              if (canvas != null) {
                if (fullcopytempbitmap != null)
                  canvas.drawBitmap(fullcopytempbitmap, 0, 0, paint);
                else if (wallpaperbitmap != null)
                  canvas.drawBitmap(wallpaperbitmap, 0, 0, paint);
              }
              if (canvas != null)
                holder.unlockCanvasAndPost(canvas);
            }
          }
        }
      } catch (Exception e) {
        llog.d(TAG, "Exception " + e.toString());
        e.printStackTrace();
      }
      if (fullcopytempbitmap != null)
        if (!fullcopytempbitmap.isRecycled())
          fullcopytempbitmap.recycle();
      if (bitmap != null)
        if (!bitmap.isRecycled())
          bitmap.recycle();
      if (bmap != null)
        if (!bmap.isRecycled())
          bmap.recycle();
      mysurf.removekeysfromcache();
      mysurf.removefromcache();
    }


    private void drawlockscreen() {
      if (lockscreenimgl<=0 || lockscreenw < 4 || lockscreenh < 4) {
        llog.d(TAG, "drawlockscreen lockscreenimgl<="+lockscreenimgl+" || lockscreenw < "+lockscreenw+" || lockscreenh < "+lockscreenh);
        return;
      }
      int selfichl = lockscreencollection.size();
      if (selfichl == 0) {
        llog.d(TAG, "selfichl 0");
        return;
      }
      int cetteimage = Gallery.rand.nextInt(selfichl);
      String cebitmap = null;
      int isonline = Media.online_no;
      Ordner ordner = lockscreencollection.get(cetteimage);
      if (ordner.mediaListCount <= 0) { // can be a sel to another coll, load
        llog.d(TAG, "pick a file inside this folder " + ordner.address);
        return;
      }
      cetteimage = Gallery.rand.nextInt(ordner.mediaListCount);
      Media media = ordner.mediaList.get(cetteimage);
      cebitmap = media.address;
      isonline = media.isOnline;
      if (cebitmap == null) {
        llog.d(TAG, "media.address = null");
        return;
      }
      for (String encache : mysurf.getcachekeyset())
        mysurf.removekeyfromcache(encache);
      boolean mediasuccess = ThreadMiniature.getbigpicture(media, mysurf.myid, model);
      if (!mediasuccess) {
        llog.d(TAG, "could not get big picture");
        return;
      }
      Bitmap bmap = null;
      if (!mysurf.isincache(media.address)) {
        llog.d(TAG, "could not get big picture in cache");
        return;
      }
      bmap = mysurf.getcachedbitmap(media.address);
      if (bmap == null) {
        llog.d(TAG, "getcachedbitmap null");
        return;
      }
      if (bmap.isRecycled()) {
        llog.d(TAG, "getcachedbitmap isRecycled");
        return;
      }
      int bmpw = bmap.getWidth();
      int bmph = bmap.getHeight();
      float scalew = ((float)lockscreenw) / ((float)bmpw);
      float scaleh = ((float)lockscreenh) / ((float)bmph);
      float bscale;
      if (0.98f < lockscreenratioscreenmin && lockscreenratioscreenmin < 1.02f) {
        if (lockscreenw >= lockscreenh) {
          if (bmpw >= bmph) {
            if (scalew > scaleh)
              bscale = scalew;
            else
              bscale = scaleh;
          } else {
            if (scalew < scaleh)
              bscale = scalew;
            else
              bscale = scaleh;
          }
        } else {
          if (bmpw < bmph) {
            if (scalew > scaleh)
              bscale = scalew;
            else
              bscale = scaleh;
          } else {
            if (scalew < scaleh)
              bscale = scalew;
            else
              bscale = scaleh;
          }
        }
        bmpw = (int) ((float) bmpw * bscale);
        bmph = (int) ((float) bmph * bscale);
      } else {
        float reduit = lockscreenratioscreenmin + (lockscreenratioscreenmax - lockscreenratioscreenmin) * Gallery.rand.nextFloat();
        if (scalew < scaleh)
          bscale = scalew;
        else
          bscale = scaleh;
        bmpw = (int) ((float) bmpw * bscale * reduit);
        bmph = (int) ((float) bmph * bscale * reduit);
      }
      if (bmap.isRecycled()) {
        llog.d(TAG, "getthefullbitmap(cebitmap, lockscreenw, lockscreenh) isRecycled");
        return;
      } // crash bmap is recycled
      Bitmap bitmap = Bitmap.createScaledBitmap(bmap, bmpw, bmph, false);
      int x = 0;
      int dx = lockscreenw - bmpw;
      int y = 0;
      int dy = lockscreenh - bmph;
      if (0.98f < lockscreenratioscreenmin && lockscreenratioscreenmin < 1.02f) {
        x = (int) ((lockscreenw - bmpw) * 0.5f);
        y = (int) ((lockscreenh - bmph) * 0.5f);
      } else if (dx > 0 && dy > 0) {
        x = Gallery.rand.nextInt(dx);
        y = Gallery.rand.nextInt(dy);
        if (Gallery.rand.nextInt(100) < 33) {
          for (int h = 0; h < 3; h++) {
            if (dx * 0.20f < x && x < dx * 0.80f) {
              x = Gallery.rand.nextInt(dx);
            } else {
              break;
            }
          }
        } else if (Gallery.rand.nextInt(100) < 33) {
          for (int h = 0; h < 3; h++) {
            if (dy * 0.20f < y && y < dy * 0.80f) {
              y = Gallery.rand.nextInt(dy);
            } else {
              break;
            }
          }
        }
      } else {
        llog.d(TAG, "error dx dy " + dx + "," + dy);
      }
      if (lockscreenratioscreenmin >= 0.95f || recycleallpictures) {
        if (lockscreenbitmap != null) {
          if (!lockscreenbitmap.isRecycled())
            lockscreenbitmap.recycle();
          lockscreenbitmap = null;
        }
      }
      if (lockscreenbitmap != null)
        if (lockscreenbitmap.isRecycled())
          lockscreenbitmap = null;

      if (lockscreenbitmap == null){

        Libextractor lib = new Libextractor();
        Bitmap miniature = lib.blurrgb(cebitmap, lockscreenw, lockscreenh);
        if (miniature == null) {
          if (bitmap != null)
            if (!bitmap.isRecycled())
              bitmap.recycle();
          if (bmap != null)
            if (!bmap.isRecycled())
              bmap.recycle();
          return;
        }

        lockscreenbitmap = Bitmap.createBitmap(lockscreenw, lockscreenh, Bitmap.Config.ARGB_8888);
        lockscreencanvas = new Canvas(lockscreenbitmap);
        lockscreencanvas.drawBitmap(miniature, 0, 0, paint);

        if (!miniature.isRecycled())
          miniature.recycle();
      }
      if (bitmap != null)
        if (!bitmap.isRecycled())
          lockscreencanvas.drawBitmap(bitmap, x, y, paint);
  
      if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N && lockscreenbitmap != null) {
        try {
           if (!lockscreenbitmap.isRecycled()) {
             if (lockscreencalendarwidget || lockscreencountdownwidget || lockscreenmonitorwidget) {
               Bitmap fullcopytempbitmap = Bitmap.createBitmap(lockscreenbitmap);
               Canvas canv = new Canvas(fullcopytempbitmap);
               drawwidgets(canv, lockscreenw, lockscreenh, lockscreencalendarwidget, lockscreencountdownwidget, lockscreenmonitorwidget);
               wallpaperManager.setBitmap(fullcopytempbitmap, null, true, WallpaperManager.FLAG_LOCK);
               if (fullcopytempbitmap != null)
                 if (!fullcopytempbitmap.isRecycled())
                    fullcopytempbitmap.recycle();
             } else {
               wallpaperManager.setBitmap(lockscreenbitmap, null, true, WallpaperManager.FLAG_LOCK);
             }
           }
        } catch (Exception e) {
          llog.d(TAG, "Exception " + e.toString());
          e.printStackTrace();
        }
      }
      if (bitmap != null)
        if (!bitmap.isRecycled())
          bitmap.recycle();
      if (bmap != null)
        if (!bmap.isRecycled())
          bmap.recycle();
      mysurf.removekeysfromcache();
      mysurf.removefromcache();
    }

    private void drawwidgets(Canvas canv, float width, float height, boolean calendarwidget, boolean countdownwidget, boolean monitorwidget){
      ArrayList<String> cds = new ArrayList<>();
      if (countdownwidget) {
        int wj = 3, wm = 2, wa = 2024;
        float wx = 0, wy = 0, ww = 0.1f, wh = 0.1f;
        int wt=0;
        if (lockscreencountdownwidgetset != null) {
          Matcher m = Gallery.lockscreencountdownwidgetsetpattern.matcher(lockscreencountdownwidgetset);
          while (m.find()) {
            wx = Integer.parseInt(m.group(1)) / 100.0f;
            wy = Integer.parseInt(m.group(2)) / 100.0f;
            ww = Integer.parseInt(m.group(3)) / 100.0f;
            wh = Integer.parseInt(m.group(4)) / 100.0f;
            wj = Integer.parseInt(m.group(5));
            wm = Integer.parseInt(m.group(6));
            wa = Integer.parseInt(m.group(7));
            cds.add(String.format("%02d%02d%04d", wj, wm, wa));
            String ttyp = m.group(8);
            if (ttyp.equals("spiral") || ttyp.equals("s"))
              wt = WidgetCalendarCountdown.gridspiral;
            else if (ttyp.equals("grid") || ttyp.equals("g"))
              wt = WidgetCalendarCountdown.gridrectangle;
            else if (ttyp.equals("linear") || ttyp.equals("l"))
              wt = WidgetCalendarCountdown.gridbar;
            String wmsg = m.group(9);
            Bitmap bmpc = WidgetCalendarCountdown.drawCountdown(model.unmillimetre,
                wj, wm, wa, wmsg, wt,
                (int) (width * ww), (int) (height * wh));
            if (bmpc != null) {
              canv.drawBitmap(bmpc, width * wx, height * wy, paint);
              bmpc.recycle();
            }
          }
        }
      }
      if (calendarwidget) {
        String []cd = null;
        int cdsl = cds.size();
        if (countdownwidget && cdsl > 0) {
          cd = cds.toArray(new String[cdsl]);
        }
        float wx = 0, wy = 0, ww = 0.1f, wh = 0.1f;
        int nm = 3;
        if (lockscreencalendarwidgetset != null) {
          Matcher m = Gallery.lockscreencalendarwidgetpattern.matcher(lockscreencalendarwidgetset);
          while (m.find()) {
            wx = Integer.parseInt(m.group(1)) / 100.0f;
            wy = Integer.parseInt(m.group(2)) / 100.0f;
            ww = Integer.parseInt(m.group(3)) / 100.0f;
            wh = Integer.parseInt(m.group(4)) / 100.0f;
            nm = Integer.parseInt(m.group(5));
            Bitmap bmpc = WidgetCalendarCountdown.drawCalendar(nm, cd, width * ww, height * wh);
            if (bmpc != null) {
              canv.drawBitmap(bmpc, width * wx, height * wy, paint);
              bmpc.recycle();
            }
          }
        }
      }
      if (monitorwidget) {
        float wx = 0, wy = 0, ww = 0.1f, wh = 0.1f; int maxtime = 0;
        if (lockscreenmonitorwidgetset != null) {
          Matcher m = Gallery.lockscreenmonitorwidgetsetpattern.matcher(lockscreenmonitorwidgetset);
          while (m.find()) {
            wx = Integer.parseInt(m.group(1)) / 100.0f;
            wy = Integer.parseInt(m.group(2)) / 100.0f;
            ww = Integer.parseInt(m.group(3)) / 100.0f;
            wh = Integer.parseInt(m.group(4)) / 100.0f;
            maxtime = Integer.parseInt(m.group(5));
            Bitmap bmpc = WidgetMonitor.drawgraph(model.internalStorageDir, width * ww, height * wh, maxtime, model.GenericTextH);
            if (bmpc != null) {
              canv.drawBitmap(bmpc, width * wx, height * wy, paint);
              bmpc.recycle();
            }
          }
        }
      }
    }
  
    /*private Bitmap getthefullbitmap(String file, int width, int height, int isonline){
      boolean getbitmapfrombuffer = false;
      byte[] buffer = null;
      //llog.d(TAG, file + " isonline " + isonline);
      if (isonline == Media.online_apache || isonline == Media.online_json) {
        getbitmapfrombuffer = true;
        buffer = InternetSession.getPicture(model, file, null, null, null, null);
        if (buffer == null) {
          return null;
        }
      }
      BitmapFactory.Options options = new BitmapFactory.Options();
      options.inJustDecodeBounds = true;
      if (getbitmapfrombuffer) {
        int bufferl = buffer.length;
        BitmapFactory.decodeByteArray(buffer, 0, bufferl, options);
      } else {
        BitmapFactory.decodeFile(file, options);
      }
      int originalwidth = options.outWidth;
      if (originalwidth < 10) {
        return null;
      }
      int originalheight = options.outHeight;
      String imageType = options.outMimeType;
      //options.inSampleSize = calculateInSampleSize(originalwidth, originalheight, ScreenWidth, ScreenHeight);
      float reduit = imageratioscreenmin + (imageratioscreenmax - imageratioscreenmin) * Gallery.rand.nextFloat();
      //llog.d(TAG,imageratioscreenmin+" "+imageratioscreenmax+" "+reduit);
      float scalew = (((float)originalwidth) / ((float)width));
      float scaleh = (((float)originalheight) / ((float)height));
      float imagefullscreenbscale;
      if(scaleh > scalew){
        imagefullscreenbscale=scaleh;
      }else{
        imagefullscreenbscale=scalew;
      }
      int newwidth = (int)(((float) originalwidth * reduit) / imagefullscreenbscale);
      int newheight = (int)(((float) originalheight * reduit) / imagefullscreenbscale);
      options.inPreferredConfig = Bitmap.Config.ARGB_8888;
      options.inJustDecodeBounds = false;
      Bitmap originalbitmap;
      if (getbitmapfrombuffer) {
        int bufferl = buffer.length;
        originalbitmap = BitmapFactory.decodeByteArray(buffer, 0, bufferl, options);
      } else {
        originalbitmap = BitmapFactory.decodeFile(file, options);
      }
      if (originalbitmap == null) {
        return null;
      }
      if (originalbitmap.isRecycled()) {
        return null;
      }
      Bitmap scaledbitmap = null;
      try {
        scaledbitmap = Bitmap.createScaledBitmap(originalbitmap, newwidth, newheight, false);
      } catch (java.lang.OutOfMemoryError e) {
        llog.d(TAG, "OutOfMemoryError");
      }
      originalbitmap.recycle();
      return scaledbitmap;
    }*/

  }

}

















