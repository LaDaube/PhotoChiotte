package la.daube.photochiotte;


import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;
import androidx.room.Delete;

import java.util.List;

@Dao
public interface DBFichier {
  @Query("SELECT * FROM fichier WHERE dossier LIKE :first ORDER BY fichier")
  List<Fichier> getAll(String first);

  @Query("SELECT dossier FROM fichier WHERE fichier LIKE :first LIMIT 1")
  String fichierDejaDansDB(String first);

  @Query("SELECT * FROM fichier WHERE fichier LIKE '%' || :first")
  List<Fichier> fichierNomDejaDansDB(String first);

  @Query("SELECT uid FROM fichier WHERE fichier LIKE :first LIMIT 1")
  int fichierGetUID(String first);

  @Query("SELECT * FROM fichier WHERE uid IN (:userIds)")
  List<Fichier> loadAllByIds(int[] userIds);

  @Query("DELETE FROM fichier WHERE fichier LIKE :first")
  void deleteFichierByName(String first);

  @Query("DELETE FROM fichier WHERE dossier LIKE :first")
  void deleteFolderOrZip(String first);

  @Query("DELETE FROM fichier WHERE dossier LIKE :first || '%'")
  void deleteFolderOrZipStartswith(String first);

  @Insert
  void insertAll(List<Fichier> fichiers);

  @Update
  void update(Fichier fichier);

  @Insert
  void insert(Fichier fichier);

  @Delete
  void delete(Fichier fichier);
}
