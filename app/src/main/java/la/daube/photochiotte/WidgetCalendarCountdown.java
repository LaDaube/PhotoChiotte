package la.daube.photochiotte;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.util.Log;

import java.text.DecimalFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;

public class WidgetCalendarCountdown {

  static final String TAG = "YYYcal";

  private static int calculej(String datez){

    Calendar now= GregorianCalendar.getInstance();
    now.set(Calendar.HOUR_OF_DAY,00);
    now.set(Calendar.MINUTE ,00);
    now.set(Calendar.SECOND,00);
    now.set(Calendar.MILLISECOND,00);
    int anneenow=now.get(GregorianCalendar.YEAR);

    int jour=Integer.parseInt(datez.substring(0,2));
    int mois=Integer.parseInt(datez.substring(2,4));
    int annee=Integer.parseInt(datez.substring(4,8));

    now.set(Calendar.DAY_OF_MONTH,jour);
    now.set(Calendar.MONTH,mois-1);
    now.set(Calendar.YEAR,annee);
    int jnow=now.get(GregorianCalendar.DAY_OF_YEAR);

    int jajoute=0;
    for(int j=anneenow;j<annee;j++){
      now.set(Calendar.MONTH, 11);
      now.set(Calendar.DAY_OF_MONTH, 31);
      now.set(Calendar.YEAR,j);
      jajoute+=now.get(GregorianCalendar.DAY_OF_YEAR);
    }

    jnow+=jajoute;

    return jnow;
  }

  public static Bitmap drawCalendar(int numbermonth, String[] datez, float largeur, float hauteur){
    Bitmap graphbmp = Bitmap.createBitmap((int) (largeur), (int) (hauteur), Bitmap.Config.ARGB_8888);
    Canvas canv = new Canvas(graphbmp);

    int jnowzn=0;
    int []jnowz=null;

		if(datez!=null){
			jnowzn=datez.length;
			if(jnowzn>0){
				jnowz=new int[jnowzn];
				for(int ou=0;ou<jnowzn;ou++){
					jnowz[ou]=calculej(datez[ou]);
				}
			}
		}

    //int largeur=120;
    //int hauteur=175;
    int months=12;
    float c1=(float)(Math.random()*360.0);
    float c20=c1+72.0f;
    if(c20>360.0f)
      c20-=360.0f;
    float c21=c20+72.0f;
    if(c21>360.0f)
      c21-=360.0f;
    float c22=c21+72.0f;
    if(c22>360.0f)
      c22-=360.0f;
    float c23=c22+72.0f;
    if(c23>360.0f)
      c23-=360.0f;
    int col1= Color.HSVToColor(new float[]{c1,1.0f,0.8f});
    int col20=Color.HSVToColor(new float[]{c20,1.0f,1.0f});
    int col21=Color.HSVToColor(new float[]{c21,1.0f,1.0f});
    int col22=Color.HSVToColor(new float[]{c22,1.0f,1.0f});
    int col23=Color.HSVToColor(new float[]{c23,1.0f,1.0f});
    int col4=Color.YELLOW;

    months = numbermonth;

    Calendar now=GregorianCalendar.getInstance();
    now.set(Calendar.HOUR_OF_DAY,00);
    now.set(Calendar.MINUTE ,00);
    now.set(Calendar.SECOND,00);
    now.set(Calendar.MILLISECOND,00);
    int annee=now.get(GregorianCalendar.YEAR);
    int jnow=now.get(GregorianCalendar.DAY_OF_YEAR);
    int mstart=now.get(GregorianCalendar.MONTH);
    int mstop=mstart+months;
    now.set(Calendar.MONTH, mstart);
    now.set(Calendar.DAY_OF_MONTH, 1);
    int jstart=now.get(GregorianCalendar.DAY_OF_YEAR);
    int anneefinaleajoute=0;
    int jstop=jstart+1;
    while(mstop>=12){
      mstop-=12;
      anneefinaleajoute++;
      now.set(Calendar.MONTH, 11);
      now.set(Calendar.DAY_OF_MONTH, 31);
      if(now.get(GregorianCalendar.YEAR)==annee)
        jstop+=now.get(GregorianCalendar.DAY_OF_YEAR)-jstart;
      else
        jstop+=now.get(GregorianCalendar.DAY_OF_YEAR);
      now.set(Calendar.YEAR, annee+anneefinaleajoute);
    }
    now.set(Calendar.MONTH, mstop);
    now.set(Calendar.DAY_OF_MONTH, 10);
    jstop+=now.get(GregorianCalendar.DAY_OF_YEAR);

    Calendar cal2=GregorianCalendar.getInstance();
    cal2.set(Calendar.HOUR_OF_DAY,00);
    cal2.set(Calendar.MINUTE ,00);
    cal2.set(Calendar.SECOND,00);
    cal2.set(Calendar.MILLISECOND,00);
    cal2.set(Calendar.YEAR,annee);

    canv.drawColor(Color.alpha(0));
    int fond=col20;
    Paint donf=new Paint();
    donf.setColor(fond);
    Paint paint=new Paint();
    Paint paints=new Paint();
    Paint paintd=new Paint();
    paint.setColor(fond);
    paint.setStrokeWidth(1.0f);
    paints.setColor(col1);
    paintd.setColor(fond);
    paint.setAntiAlias(true);
    paints.setAntiAlias(true);
    paintd.setAntiAlias(true);
    Paint techt=new Paint();
    techt.setAntiAlias(true);
    techt.setColor(Color.BLACK);


    float colonne=((float)largeur)/((float)months);
    float ligne=((float)hauteur)/32.0f;
    float decax=colonne*0.1f;
    float decay=ligne*0.1f;
    colonne-=decax*2.0f;
    ligne-=decay*2.0f;
    float indx=0.0f;
    float indy=0.0f;

    Paint paintxt=new Paint();
    paintxt.setTextAlign(Paint.Align.CENTER);
    paintxt.setTypeface(Typeface.SANS_SERIF);
    paintxt.setTextSize(ligne);
    paintxt.setColor(col4);
    paintxt.setShadowLayer(2.0f, 1.0f, 1.0f, Color.BLACK);
    float bas=ligne;
    float gauche=colonne/2.0f;
    String []moish={"Jr","Fr","Mh","Ap","My","Jn","Jl","At","Se","Oe","Ne","De"};
    for(int j=mstart;j<mstart+months;j++){
      gauche+=decax;
      int k=j;
      while(k>11)
        k-=12;
      canv.drawText(moish[k],gauche,bas,paintxt);
      gauche+=colonne+decax;
    }

    paint.setAlpha(120);
    paints.setShadowLayer(2.0f, 1.0f, 1.0f, Color.BLACK);
    paintd.setShadowLayer(2.0f, 1.0f, 1.0f, Color.BLACK);
    paint.setShadowLayer(2.0f, 1.0f, 1.0f, Color.BLACK);
    paint.setStyle(Paint.Style.STROKE);
    String []sem={"","m","tu","w","th","f","sa","su"};
    int jsem=1;
    int jmois=0;
    int k=jstart-1;

    Paint pp=new Paint();
    pp.setAntiAlias(true);
    pp.setColor(Color.RED);
    pp.setTypeface(Typeface.DEFAULT_BOLD);
    pp.setTextAlign(Paint.Align.CENTER);
    pp.setTextSize(ligne*1.0f);


    Calendar calsai=GregorianCalendar.getInstance();
    calsai.set(Calendar.HOUR_OF_DAY,00);
    calsai.set(Calendar.MINUTE ,00);
    calsai.set(Calendar.SECOND,00);
    calsai.set(Calendar.MILLISECOND,00);
    calsai.set(Calendar.YEAR,annee);
    calsai.set(Calendar.DAY_OF_MONTH,21);
    int []saisonsi=new int[4];
    for(int kk=0;kk<4;kk++){
      calsai.set(Calendar.MONTH,2+(kk*3));
      saisonsi[kk]=calsai.get(Calendar.DAY_OF_YEAR);
    }
    int []saisonsc={col20,col21,col22,col23};
    int moiz=0;

    for(int kk=0;kk<=2;kk++){
      if((saisonsi[kk]<=jstart&&jstart<=saisonsi[kk+1])||jstart<=saisonsi[0]){
        fond=saisonsc[kk];
        donf.setColor(fond);
        paint.setColor(fond);
        paintd.setColor(fond);
        paint.setAlpha(80);
      }
    }

    for(int j=jstart;j<jstop;j++){
      k++;
      if(moiz==11&&jmois==31){
        k=1;
        cal2.set(GregorianCalendar.YEAR,annee+1);
      }
      cal2.set(GregorianCalendar.DAY_OF_YEAR,k);
      jmois=cal2.get(GregorianCalendar.DAY_OF_MONTH);
      moiz=cal2.get(GregorianCalendar.MONTH);

      indy+=decay;
      if(jmois==1){
        indx+=decax;
        indy=decay*2.0f+ligne;
        if(j!=jstart){
          indx+=colonne+decax;
        }
      }
      jsem=cal2.get(GregorianCalendar.DAY_OF_WEEK);
      if(jsem==1)
        jsem=8;
      jsem-=1;//lundi=1

      if(j==jnow){
        paint.setAlpha(80);
      }

      float left=indx;
      float top=indy;
      float right=left+colonne;
      float bottom=top+ligne;

      for(int kk=0;kk<4;kk++){
        if(k==saisonsi[kk]){
          fond=saisonsc[kk];
          donf.setColor(fond);
          paint.setColor(fond);
          paintd.setColor(fond);
          paint.setAlpha(80);
        }
      }
		        /*
		        if(j==jnow){
		        	canv.drawRect(left,top,right,bottom,paintd);
		        }*/

      if(j>=jnow){

        canv.drawRect(left-decax,top-decay,right+decax,top,donf);
        canv.drawRect(left-decax,top,left,bottom,donf);
        canv.drawRect(right,top,right+decax,bottom,donf);
        canv.drawRect(left-decax,bottom,right+decax,bottom+decay,donf);

        canv.drawLine(right,top,right,bottom,paintd);
        canv.drawLine(left,bottom,right,bottom,paintd);
        canv.drawRect(left,top,right,bottom,paint);
      }


      if(j<jnow){
        canv.drawCircle((left+right)/2.0f, (top+bottom)/2.0f, ligne*0.3f, paintd);
      }




      float lisere=left+colonne/5.0f;
      if(jsem==6){
        canv.drawRect(left, top, lisere, bottom, paintd);
      }
      else if(jsem==7){
        canv.drawRect(left, top, lisere, bottom, paints);
      }
      else if(jsem==3 && j!=jnow){
        float mitext=left+colonne/2.0f;
        float hautext=bottom-decay;
        //paintxt.setTextSize(ligne*6.0f/7.0f);
        String jdm=""+cal2.get(GregorianCalendar.DAY_OF_MONTH);
        canv.drawText(jdm, mitext, hautext, paintxt);
        //paintxt.setTextSize(ligne);
      }

      if(jnowzn>0){
        for(int ou=0;ou<jnowzn;ou++){
          if(j==jnowz[ou]){
            canv.drawRect(left,top,right,bottom,paintxt);
            canv.drawLine(left,top,right,bottom,paints);
            canv.drawLine(left,bottom,right,top,paints);
            //canv.drawText("***",left+colonne/2.0f,top+ligne*0.9f,pp);
          }
        }
      }
/*
		        if(j==157){
		        	canv.drawRect(left,top,right,bottom,paintxt);
		        	canv.drawCircle((left+right)/2.0f, (top+bottom)/2.0f, ligne*0.3f, paints);
			    }*/

      if(j==jnow){
        float mitext=left+colonne/2.0f;
        float hautext=bottom-decay;
        String jdm=sem[jsem]+cal2.get(GregorianCalendar.DAY_OF_MONTH);
        //paintxt.setShadowLayer(2.0f, 1.0f, 1.0f, Color.BLACK);
        canv.drawText(jdm, mitext, hautext, paintxt);
        //paintxt.setShadowLayer(0.0f, 1.0f, 1.0f, Color.BLACK);
      }


      indy+=ligne;
      indy+=decay;
    }

    return graphbmp;
  }

  public final static int gridbar = 0;
  public final static int gridspiral = 1;
  public final static int gridrectangle = 2;

  public static Bitmap drawCountdown(float unmillimetre, int jour3, int mois3, int annee3, String nomm, int grid, int largeur, int hauteur){
    Bitmap graphbmp = Bitmap.createBitmap((int) (largeur), (int) (hauteur), Bitmap.Config.ARGB_8888);
    Canvas canv = new Canvas(graphbmp);
    //canv.drawColor(Color.YELLOW);

    Calendar now=GregorianCalendar.getInstance();

    //Calendar now=GregorianCalendar.getInstance();
    float heure=now.get(GregorianCalendar.HOUR_OF_DAY)+(float)now.get(GregorianCalendar.MINUTE)/60.0f;
    Calendar cal2=GregorianCalendar.getInstance();
    cal2.set(Calendar.HOUR_OF_DAY, 8);
    cal2.set(Calendar.MINUTE , 0);
    cal2.set(Calendar.SECOND, 0);
    cal2.set(Calendar.MILLISECOND, 0);
    cal2.set(Calendar.DAY_OF_MONTH, jour3);
    cal2.set(Calendar.MONTH, mois3-1);
    cal2.set(Calendar.YEAR, annee3);
    long t1=cal2.getTimeInMillis();
    long t2=now.getTimeInMillis();
    long difcall=t1-t2;
    float difcal=(float)(difcall)/1000.0f;
    difcal/=3600.0f;
    int difheu=(int)difcal;
    difcal/=24.0f;

    float difcaa=difcal;
    float difsem=difcal/7.0f;
    float difmoi=difcal/30.5f;

    String diftempss=new DecimalFormat("0.00000").format(difcaa);
    String difsems=new DecimalFormat("0.000000").format(difsem);
    String difmois=new DecimalFormat("0.000000").format(difmoi);

    //llog.d(TAG, t1+" - "+t2+" = j "+difcaa+" : s "+difsem+" : m "+difmoi);

    //System.out.println(difcal);
    int difca=(int)Math.ceil(difcal-8.0f/24.0f);

    int couleur=Color.alpha(0);
    canv.drawColor(couleur);

    int depart=now.get(Calendar.DAY_OF_WEEK);
    if(depart==1)
      depart=8;
    depart-=1;//lundi=1
    int ncolonnes=(int)((Math.ceil((difcal+depart)/7.)));
    int nlignes=7;
    float colonne=((float)largeur)/((float)ncolonnes);
    float ligne=((float)hauteur)/((float)nlignes);
    float espcolonne=colonne*0.075f;
    float espligne=ligne*0.075f;
    colonne*=0.85f;
    ligne*=0.85f;
    Paint paint=new Paint();
    paint.setAntiAlias(true);
    paint.setColor(Color.YELLOW);
    paint.setShadowLayer(4.0f, 1.0f, 1.0f, Color.BLACK);
    Paint pai=new Paint();
    pai.setAntiAlias(true);
    pai.setShadowLayer(4.0f, 1.0f, 1.0f, Color.BLACK);

    // KESAKO ????????????????????????
    float randone=1.0f;
    float randtwo=1.0f;
    float wides=200.0f*randone+100.0f;
    float weak=wides/((float)ncolonnes);
    float startweak=(360.0f-wides)*randtwo+weak;
    float currweak=startweak;

    //
    pai.setColor(Color.HSVToColor(new float[]{currweak,1.0f,1.0f}));
    if(grid==gridrectangle){
      int total=-depart;
      float reperex=0.0f;
      float reperey=0.0f;
      currweak=startweak+ncolonnes*weak-weak;
      for(int col=1;col<=ncolonnes;col++){
        reperex+=espcolonne;
        for(int lig=1;lig<=nlignes;lig++){
          float currfadeweek=0.5f+((float)lig)/14.0f;
          pai.setColor(Color.HSVToColor(new float[]{currweak,currfadeweek,1.8f-currfadeweek}));
          reperey+=espligne;
          total++;
          if(total>=0 && total<=difca) {
            float left = reperex;
            float top = reperey;
            float right = reperex + colonne;
            float bottom = reperey + ligne;
            if (total == 0) {
              left = right - (float) Math.sqrt(1.0f - heure / 24.0f) * colonne;
              top = bottom - (float) Math.sqrt(1.0f - heure / 24.0f) * ligne;
              canv.drawRect(left, top, right, bottom, pai);
            } else {
              bottom=reperey+ligne/4.0f;
              canv.drawRect(left, top, right, bottom, pai);
              top = reperey+(ligne*3.0f)/4.0f;
              bottom=reperey+ligne;
              canv.drawRect(left, top, right, bottom, pai);
              top = reperey;
              right = reperex + colonne/4.0f;
              canv.drawRect(left, top, right, bottom, pai);
              left = reperex+(colonne*3.0f)/4.0f;
              right = reperex + colonne;
              canv.drawRect(left, top, right, bottom, pai);
            }
            if (total == difca) {
              left = reperex;
              top = reperey;
              right = reperex + colonne;
              bottom = reperey + ligne;
              canv.drawRect(left, top, right, bottom, pai);
              canv.drawCircle(left + colonne / 2.0f, top + ligne / 2.0f, Math.min(ligne / 4.0f, colonne / 4.0f), paint);
            }

          }
          reperey+=ligne;
          reperey+=espligne;
        }
        currweak-=weak;
        reperey=0.0f;
        reperex+=colonne;
        reperex+=espcolonne;
      }
      paint.setTextAlign(Paint.Align.CENTER);
      float heiht=((float)hauteur);
      float wiiht=((float)largeur)/2.0f;
      paint.setTypeface(Typeface.SANS_SERIF);
      paint.setTextSize(unmillimetre*7.0f);
      canv.drawText(nomm, wiiht, heiht*0.2f, paint);
      //canv.drawText(joursem, wiiht, 3.0f*heiht, paint);
      //canv.drawText(nssom, wiiht, 4.0f*heiht, paint);
      paint.setTypeface(Typeface.DEFAULT_BOLD);
      paint.setTextSize(unmillimetre*7.0f);
      //canv.drawText(difmin+"m", wiiht, heiht*0.4f, paint);
      //canv.drawText(difheu+"", wiiht, heiht*0.6f, paint);
      if(difsem>=2.0f){
        canv.drawText(difsems, wiiht, heiht*0.8f, paint);
      } else {
        canv.drawText(diftempss, wiiht, heiht*0.8f, paint);
      }
    }else if (grid==gridspiral){
      //SPIRAL
      float decalagex=((float)largeur)/2.0f;
      float decalagey=((float)hauteur)/2.0f;
      float minhautlarg=((float)Math.min(hauteur,largeur));
      float startfade=(heure*360.0f)/24.0f;
      float fade=360.0f-startfade;
      if(startfade<180.0f)
        startfade=startfade+180.0f;
      else
        startfade=startfade-180.0f;

      //equation de la taille des cercles
      float dif=(float)difca;
      float plusgros;//en fonction du nombre de jours
      float [][]reperes={
          {3.0f,4.0f},   // 1/4 à 3 jours
          {60.0f,15.0f}  // 1/15 à 45 jours
      };
      float pente=(reperes[0][1]-reperes[1][1])/(reperes[0][0]-reperes[1][0]);
      float ordonneorig=reperes[0][1]-pente*reperes[0][0];
      plusgros=pente*dif+ordonneorig;
      float ratiogrospetit=1.3f;
      float firstpos=minhautlarg/plusgros;
      float lastpos=firstpos/ratiogrospetit;
      float ra=(lastpos-firstpos)/dif;
      float rb=firstpos;

      //equation de la position des cercles
      double lastpos2=((double)(minhautlarg/2.0f-rayon(difca,ra,rb)));
      double firstpos2=(double)(rayon(0,ra,rb));
      double declage=0.2;    //3.0  vide au centre de la spire
      double andeclage=0.98;//1.07 deviation angulaire après chaque jour
      double angle=3.5;     //3.5  necessaire pour la semaine 7 jours = 1 tour de spire
      double alpha0=(Math.PI)/angle;
      double alpha0andeclage=alpha0*andeclage;
      double dif2=(double)difca;
      double ra2=(lastpos2-firstpos2)/(dif2+declage);
      double rb2=firstpos2+declage*ra2;

      //equation de la position des cercles

      int j=depart;
      for(int u=0;u<difca;u++){
        j++;
        if(j==8)
          j=1;
      }
      String []nj={"","m","","w","","f","","su"};
      float [][]njp=new float[2][7];
      float x=0.0f;
      float y=0.0f;
      pai.setStrokeWidth(5.0f);
      for(int u=difca;u>=0;u--){
        //if(u==0)
        //pain[j-1].setAlpha(fade);
        float currfadeweek=0.5f+((float)j)/14.0f;
        pai.setColor(Color.HSVToColor(new float[]{currweak,currfadeweek,1.8f-currfadeweek}));
        double axy[]=xy(u,ra2,rb2,alpha0andeclage);
        float ray=rayon(u,ra,rb);
        float x0=x;
        float y0=y;
        x=((float)axy[0])+decalagex;
        y=((float)axy[1])+decalagey;/*
                if(u!=1 && u!=difca){
                	float aa=2.0f;
                	float bb=3.0f;
                	float cc=aa+bb;
                	float x1=(aa*x+bb*x0)/cc;
                	float y1=(aa*y+bb*y0)/cc;
                	float x2=(bb*x+aa*x0)/cc;
                	float y2=(bb*y+aa*y0)/cc;
                	pai.setAlpha(130);
                	canv.drawLine(x1,y1,x2,y2,pai);
                }
            	pai.setAlpha(255);*/
        if(u!=0)
          canv.drawCircle(x,y,ray,pai);
        if(u==difca){/*
                	canv.drawCircle(x,y,ray/2.0f,paint);
                    paint.setColor(Color.RED);*/
          canv.drawCircle(x,y,ray/4.0f,paint);
          paint.setColor(Color.YELLOW);
        }else if(u==0){
          RectF oval=new RectF(x-ray,y-ray,x+ray,y+ray);
          canv.drawArc(oval, startfade, fade, true, pai);
        }
        if(u>difca-7 && u<=difca){
          njp[0][j-1]=x;
          njp[1][j-1]=y;
        }
        j--;
        if(j==0){
          currweak+=weak;
          j=7;
          //alpha0andeclage=(Math.PI)/angle*andeclage;
        }
      }
      paint.setTextAlign(Paint.Align.CENTER);
      float wiiht=((float)largeur)/2.0f;
      paint.setTextSize(wiiht/6.0f);
      paint.setTextSkewX(-0.25f);
      paint.setShadowLayer(10.0f, 3.0f, 3.0f, Color.BLACK);
      for (int yp=0;yp<7;yp++){
        canv.drawText(nj[yp+1], njp[0][yp], njp[1][yp], paint);
      }
      paint.setTextSkewX(0.0f);
      paint.setTextAlign(Paint.Align.LEFT);
      paint.setTypeface(Typeface.SANS_SERIF);
      paint.setTextSize(wiiht/4.0f);
      canv.drawText(nomm, 15.0f, wiiht/4.0f+15.0f, paint);
      paint.setTypeface(Typeface.DEFAULT_BOLD);
      paint.setTextAlign(Paint.Align.RIGHT);
      paint.setTextSize(wiiht/6.0f);
      if(difmoi>=10.0f)
        canv.drawText(difmois+"m", largeur-15.0f, hauteur-15.0f, paint);
      else if(difsem>=4.0f)
        canv.drawText(difsems+"w", largeur-15.0f, hauteur-15.0f, paint);
      else
        canv.drawText(diftempss+"d", largeur-15.0f, hauteur-15.0f, paint);
      paint.setTextAlign(Paint.Align.LEFT);
      paint.setTextAlign(Paint.Align.LEFT);
    }else{

      if(hauteur<=largeur){

        float reperex=0.0f;
        float reperey=0.0f;
        ligne=hauteur*0.9f;
        float decalagey=hauteur*0.05f;
        float decalagex=((float)largeur)/((float)(difca+1));
        float largeureff=decalagex*0.9f*(float)(difca+1);
        decalagex=decalagex*0.05f;
        float prems=(float)Math.sqrt(1.0f-((float)heure)/24.0f);
        colonne=((float)largeureff)/(((float)difca)+prems);
        float premsh=ligne*prems;
        prems=colonne*prems;

        reperex=largeur;
        reperey=decalagey;
        int j=depart;
        for(int u=0;u<difca;u++){
          j++;
          if(j==8)
            j=1;
        }
        for(int u=difca;u>=0;u--){
          reperex-=decalagex;
          float currfadeweek=0.5f+((float)j)/14.0f;
          pai.setColor(Color.HSVToColor(new float[]{currweak,currfadeweek,1.8f-currfadeweek}));
          if(u==0){
            reperex-=prems;
            colonne=prems;
            ligne=premsh;
          }
          else
            reperex-=colonne;
          float left=reperex;
          float top=reperey;
          float right=reperex+colonne;
          float bottom=reperey+ligne;
          //Log.d("node",left+" "+ top+" "+ right+" "+ bottom);
          canv.drawRect(left, top, right, bottom, pai);
          if(u==difca)
            canv.drawCircle(left+colonne/2.0f, top+ligne/2.0f, Math.min(ligne/4.0f,colonne/4.0f), paint);
          j--;
          if(j==0){
            currweak+=weak;
            j=7;
          }
          reperex-=decalagex;
        }
        paint.setTextAlign(Paint.Align.LEFT);
        float wiiht=((float)hauteur);
        paint.setTypeface(Typeface.SANS_SERIF);
        paint.setTypeface(Typeface.DEFAULT_BOLD);
        paint.setTextSize(wiiht/3.0f);
        canv.drawText(nomm, largeur*0.1f, hauteur*0.9f, paint);
        if(difmoi>=1.0f)
          canv.drawText(difmois+"m", largeur*0.05f, hauteur*0.8f, paint);
        else if(difsem>=4.0f)
          canv.drawText(difsems+"w", largeur*0.05f, hauteur*0.8f, paint);
        else
          canv.drawText(diftempss+"d", largeur*0.05f, hauteur*0.8f, paint);

      }else{
        float reperey=0.0f;
        float reperex=0.0f;
        colonne=largeur*0.9f;
        float decalagex=largeur*0.05f;
        float decalagey=((float)hauteur)/((float)(difca+1));
        float hauteurff=decalagey*0.9f*(float)(difca+1);
        decalagey=decalagey*0.05f;
        float prems=(1.0f-((float)heure)/24.0f);
        ligne=((float)hauteurff)/(((float)difca)+prems);
        float premsh=colonne*prems;
        prems=ligne*prems;

        reperey=hauteur;
        reperex=decalagex;
        int j=depart;
        for(int u=0;u<difca;u++){
          j++;
          if(j==8)
            j=1;
        }
        for(int u=difca;u>=0;u--){
          reperey-=decalagey;
          float currfadeweek=0.5f+((float)j)/14.0f;
          pai.setColor(Color.HSVToColor(new float[]{currweak,currfadeweek,1.8f-currfadeweek}));
          if(u==0){
            reperey-=prems;
            ligne=prems;
            colonne=premsh;
          }
          else
            reperey-=ligne;
          float left=reperex;
          float top=reperey;
          float right=reperex+colonne;
          float bottom=reperey+ligne;
          //Log.d("node",left+" "+ top+" "+ right+" "+ bottom);
          canv.drawRect(left, top, right, bottom, pai);
          if(u==difca)
            canv.drawCircle(left+colonne/2.0f, top+ligne/2.0f, Math.min(ligne/4.0f,colonne/4.0f), paint);
          j--;
          if(j==0){
            currweak+=weak;
            j=7;
          }
          reperey-=decalagey;
        }
        paint.setTextAlign(Paint.Align.LEFT);
        float wiiht=((float)largeur)/2.0f;
        paint.setTypeface(Typeface.SANS_SERIF);
        paint.setTypeface(Typeface.DEFAULT_BOLD);
        paint.setTextSize(wiiht/6.0f);
        canv.drawText(nomm, largeur*0.05f, hauteur*0.2f, paint);
        if(difmoi>=1.0f)
          canv.drawText(difmois+"m", largeur*0.05f, hauteur*0.8f, paint);
        else if(difsem>=1.0f)
          canv.drawText(difsems+"w", largeur*0.05f, hauteur*0.8f, paint);
        else
          canv.drawText(diftempss+"d", largeur*0.05f, hauteur*0.8f, paint);
      }
    }



    //Log.d("node","READ Drawn! "+appWidgetId);
    //Toast.makeText(context, "READ Drawn! "+appWidgetId, Toast.LENGTH_LONG).show();
    return graphbmp;
  }

  private static float rayon (int nn,float ra, float rb){
    float n =(float)nn;
    float r=ra*n+rb;
    return r;
  }

  private static double[] xy(int nn,double ra,double rb, double alpha0andeclage){

    double []axy=new double[2];
    double n =(double)nn;
    double r=ra*n+rb;

    double alpha=alpha0andeclage*n;

    axy[0]=r*Math.sin(alpha);
    axy[1]=r*Math.cos(alpha);
    return axy;
  }

}
