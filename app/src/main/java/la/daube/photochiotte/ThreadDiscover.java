package la.daube.photochiotte;


import android.content.Context;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.URL;
import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;


//    adb shell dumpsys activity services la.daube.photochiotte.c

public class ThreadDiscover {
  public static final String TAG = "YYYtor";

  static {
    System.loadLibrary("discover");
  }

  private static native int GetDiscoverStats(ByteBuffer buf);
  private static native int SearchDiscover(String keywords, String dossiercollections);
  private static native int RetrievedSearchResults();
  private static native int LaunchServer(String path, String bootstraps, int socksport, int serveurporti, int serveurportf, int clientporti, int clientportf);
  private static native int StopServer();

  private Context context = null;

  public ThreadDiscover(Context _context) {
    context = _context;
    llog.d(TAG, "+++++++++++++++++++++++ onCreate ThreadDiscover");
    surveythread.start();
  }

  private volatile String basedir = "";
  private static final int TORSOCKSPORT = 9053;
  private static final int TORCLIENTPORT = 8083;
  private static final int TORSERVERPORT = 8003;

  public void onDestroy() {
    llog.d(TAG, "--------------------------------onDestroy ThreadDiscover------------------------------------");
    StopServer();
    llog.d(TAG, "onDestroy STATUS_OFF");
  }

  public String PUBKEY = "-";
  public int STATUS_GET_PEERS_FAILED = 0;
  public int STATUS_GET_PEERS_SUCCESS = 0;
  public int STATUS_FIND_OUR_POSITION_FAILED = 0;
  public int STATUS_FIND_OUR_POSITION_SUCCESS = 0;
  public int STATUS_ANNOUNCE_PEER_FAILED = 0;
  public int STATUS_ANNOUNCE_PEER_SUCCESS = 0;
  public int STATUS_DOWNLOAD_FAILED = 0;
  public int STATUS_DOWNLOAD_SUCCESS = 0;
  public int STATUS_SEARCH_KEYWORDS = 0;
  public int STATUS_CLIENTSL = 0;
  public int STATUS_RANGESL = 0;
  public int STATUS_HASHESL = 0;

  public static final LinkedBlockingQueue<String[]> torqueue = new LinkedBlockingQueue<>();
  public volatile boolean surveythreadrunning = false;
  private final Thread surveythread = new Thread(new Runnable() {
    @Override
    public void run() {
      surveythreadrunning = true;
      llog.d(TAG, "+++++survey thread starts");

      basedir = context.getDataDir().getAbsolutePath() + "/";
      llog.d(TAG, basedir);

      new Thread(new Runnable() {
        @Override
        public void run() {
          llog.d(TAG, "+++++++++++++++++++++server starting... ");
          int resultcode = LaunchServer(
              basedir, "bootstraps",
              TORSOCKSPORT,
              TORSERVERPORT, TORSERVERPORT,
              TORCLIENTPORT, TORCLIENTPORT
          );
          llog.d(TAG, "---------------------...server exited " + resultcode);
          surveythreadrunning = false;
        }
      }).start();

      String[] command = null;
      long pollsleeptime = 1000L;

      final int size = 4 * 12 + 6;
      ByteBuffer statsbuf = ByteBuffer.allocateDirect(size);

      while (surveythreadrunning) {

        try {
          command = torqueue.poll(pollsleeptime, TimeUnit.MILLISECONDS);
        } catch (Exception e) {
          e.printStackTrace();
        }
        if (command != null)
          if (command[0].equals("refreshstats"))
            command = null;
        pollsleeptime = 1000L;

        if (command != null) {
          llog.d(TAG, "new command received " + command[0]);
          if (command.length >= 3) {
            if (command[0].equals("search")) {
              int c = SearchDiscover(command[1], command[2]);
            }
          }
        } else {

          int c = GetDiscoverStats(statsbuf);
          if (c >= size) {
            statsbuf.rewind();
            PUBKEY = String.format("%02x%02x%02x%02x%02x%02x",
                statsbuf.get(), statsbuf.get(), statsbuf.get(), statsbuf.get(), statsbuf.get(), statsbuf.get()
            );
            STATUS_GET_PEERS_FAILED = Collection.toInt(statsbuf);
            STATUS_GET_PEERS_SUCCESS = Collection.toInt(statsbuf);
            STATUS_FIND_OUR_POSITION_FAILED = Collection.toInt(statsbuf);
            STATUS_FIND_OUR_POSITION_SUCCESS = Collection.toInt(statsbuf);
            STATUS_ANNOUNCE_PEER_FAILED = Collection.toInt(statsbuf);
            STATUS_ANNOUNCE_PEER_SUCCESS = Collection.toInt(statsbuf);
            STATUS_DOWNLOAD_FAILED = Collection.toInt(statsbuf);
            STATUS_DOWNLOAD_SUCCESS = Collection.toInt(statsbuf);
            STATUS_SEARCH_KEYWORDS = Collection.toInt(statsbuf);
            STATUS_CLIENTSL = Collection.toInt(statsbuf);
            STATUS_RANGESL = Collection.toInt(statsbuf);
            STATUS_HASHESL = Collection.toInt(statsbuf);
          }
        }

      }

      statsbuf.clear();
      llog.d(TAG, "-----------survey thread exited");
      torqueue.clear();
    }
  });

  public void retrieveSearchResults() {
    RetrievedSearchResults();
    try {
      torqueue.put(new String[]{"refreshstats"});
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
  }

  private boolean isPortAvailable(int port) {
    try {
      (new ServerSocket(port)).close();
      return true;
    } catch (IOException e) {
      // Could not connect.
      return false;
    }
  }

  private boolean canConnectToSocket(String host, int port) {
    try {
      Socket socket = new Socket();
      socket.connect(new InetSocketAddress(host, port), 120);
      socket.close();
      return true;
    } catch (IOException e) {
      // Could not connect.
      return false;
    }
  }

  private boolean isServerSocketInUse(int port) {
    llog.d(TAG, "isServerSocketInUse: " + port);
    try {
      (new ServerSocket(port)).close();
      return false;
    } catch (IOException e) {
      // Could not connect.
      return true;
    }
  }

  private String retrievethroughtor(String url) {
    String answer = null;
    try {
      Proxy proxy = new Proxy(Proxy.Type.SOCKS, new InetSocketAddress("127.0.0.1", TORSOCKSPORT));
      HttpURLConnection connection = (HttpURLConnection) new URL(url).openConnection(proxy);

      llog.d(TAG, " created connection " + url);

      String requestmethod = "GET";
      connection.setRequestMethod(requestmethod);
            /*Map<String, String> requestHeaders = new HashMap<>();
            //requestHeaders.put("User-Agent", "Chrome");
            for (Map.Entry<String, String> requestHeader : requestHeaders.entrySet()) {
                connection.setRequestProperty(requestHeader.getKey(), requestHeader.getValue());
            }*/

      // transform response to required format for WebResourceResponse parameters
      InputStream in = new BufferedInputStream(connection.getInputStream());

      String encoding = connection.getContentEncoding();
      llog.d(TAG, " encoding " + encoding);
      connection.getHeaderFields();
            /*Map<String, String> responseHeaders = new HashMap<>();
            String resphead = "";
            for (String key : connection.getHeaderFields().keySet()) {
                resphead += " <" + key + "=" + connection.getHeaderField(key)+">";
                responseHeaders.put(key, connection.getHeaderField(key));
            }
            llog.d(TAG, resphead);
            String mimeType = "text/plain";
            if (connection.getContentType() != null && !connection.getContentType().isEmpty()) {
                mimeType = connection.getContentType().split("; ")[0];
                llog.d(TAG, " mimeType " + mimeType);
            }*/
      int responsecode = connection.getResponseCode();
      llog.d(TAG, " responsecode " + responsecode);
      String message = connection.getResponseMessage();
      llog.d(TAG, " message " + message);

      int allsize = 0;
      int bufferl = 128;
      StringBuffer s = new StringBuffer();
      byte[] buffer = new byte[bufferl];
      while (true) {
        int datal = in.read(buffer);
        s.append(Arrays.toString(buffer));
        allsize += datal;
        if (datal < bufferl)
          break;
      }
      llog.d(TAG, " inputstream size " + allsize);
      in.close();
      answer = s.toString();

    } catch (UnsupportedEncodingException e) {
      llog.d(TAG, "UnsupportedEncodingException " + e.toString());
    } catch (IOException e) {
      llog.d(TAG, "IOException " + e.toString());
    }
    return answer;
  }


}




