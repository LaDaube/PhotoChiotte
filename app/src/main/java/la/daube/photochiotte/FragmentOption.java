package la.daube.photochiotte;

import android.annotation.SuppressLint;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link FragmentOption.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link FragmentOption#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FragmentOption extends Fragment {
  private static final String ARG_PARAM1 = "param1";
  private static final String ARG_PARAM2 = "param2";
  private static final String TAG = "YYYfo";

  private Gallery model;

  private volatile float deltaminmove;
  private volatile float deltaminmove2;

  public FragmentOption() {
    // Required empty public constructor
  }

  public static FragmentOption newInstance(String param1, String param2) {
    FragmentOption fragment = new FragmentOption();
    Bundle args = new Bundle();
    args.putString(ARG_PARAM1, param1);
    args.putString(ARG_PARAM2, param2);
    fragment.setArguments(args);
    return fragment;
  }

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    llog.d(TAG, "onCreate()");
    if (getArguments() != null) {
    }

    Context context = getActivity().getApplicationContext();
    //model = ViewModelProviders.of(getActivity()).get(myViewModel.class);
    model = new ViewModelProvider(requireActivity()).get(Gallery.class);

  }


  @SuppressLint("ClickableViewAccessibility")
  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
    final View pictureview = inflater.inflate(R.layout.fragment_option, container, false);
    llog.d(TAG, "onCreateView()");

    final Context context = getActivity().getApplicationContext();
    final SurfaceView imageviewpicture = pictureview.findViewById(R.id.imageViewImageOption);
    final SurfaceHolder mysurface = imageviewpicture.getHolder();
    mysurface.addCallback(new SurfaceHolder.Callback() {
      @Override
      public void surfaceCreated(SurfaceHolder surfaceHolder) {
        mysurface.setSizeFromLayout();
      }

      @Override
      public void surfaceChanged(SurfaceHolder surfaceHolder, int i, int i1, int i2) {

        DisplayMetrics displayMetrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        float xdpi = displayMetrics.xdpi;
        float ydpi = displayMetrics.ydpi;
        model.bigScreenWidth = displayMetrics.widthPixels;
        model.bigScreenHeight = displayMetrics.heightPixels;
        model.unmillimetre = xdpi/25.4f;
        llog.d(TAG, "surfaceChanged() "+model.bigScreenWidth +"x"+model.bigScreenHeight);

        deltaminmove = model.unmillimetre * 2.0f;
        deltaminmove2 = deltaminmove * deltaminmove;

      }

      @Override
      public void surfaceDestroyed(SurfaceHolder surfaceHolder) {
        llog.d(TAG, "surfaceDestroyed()");
      }
    });

    imageviewpicture.setOnTouchListener(new View.OnTouchListener() {
      float pos0ix = 0.0f;
      float pos0iy = 0.0f;
      float pos0fx = 0.0f;
      float pos0fy = 0.0f;
      float pos1ix = 0.0f;
      float pos1iy = 0.0f;
      float pos1fx = 0.0f;
      float pos1fy = 0.0f;
      float delta10ix = 0.0f;
      float delta10iy = 0.0f;
      float delta10isqr = 0.0f;
      long timetouchdown = System.currentTimeMillis();
      long lasttime = System.currentTimeMillis();
      int maxpointerdown=0;
      boolean wemoved = false;
      boolean wepinched = false;
      long minmovingtime = 1000;
      @Override
      public boolean onTouch(View v, MotionEvent ev) {
        if(model.bigScreenWidth <4||model.bigScreenHeight <4){
          return true;
        }
        int action = ev.getAction() & MotionEvent.ACTION_MASK;
        int pointercount=ev.getPointerCount();
        if(pointercount>maxpointerdown){
          maxpointerdown=pointercount;
        }
        switch(action) {
          case MotionEvent.ACTION_DOWN:
            pos0ix = ev.getX(0);
            pos0iy = ev.getY(0);
            wemoved = false;
            wepinched = false;
            maxpointerdown = 0;
            lasttime = System.currentTimeMillis();
            timetouchdown = System.currentTimeMillis();
            break;
          case MotionEvent.ACTION_POINTER_DOWN :
            if(maxpointerdown == 2) {
              pos1ix = ev.getX(1);
              pos1iy = ev.getY(1);
              delta10ix = pos1ix - pos0ix;
              delta10iy = pos1iy - pos0iy;
              delta10isqr = (float) Math.sqrt(delta10ix * delta10ix + delta10iy * delta10iy);
            }
            wemoved = false;
            wepinched = false;
            break;
          case MotionEvent.ACTION_MOVE:
            long temps = System.currentTimeMillis();
            if(temps - lasttime > 30) {
              pos0fx = ev.getX(0);
              pos0fy = ev.getY(0);
              float delta0fix = pos0fx - pos0ix;
              float delta0fiy = pos0fy - pos0iy;
              float delta0fisqr = delta0fix * delta0fix + delta0fiy * delta0fiy;
              if(!wemoved) {
                if (delta0fisqr > deltaminmove2 || temps - timetouchdown > minmovingtime) {
                  wemoved = true;
                }
              }
              if (pointercount == 1 && maxpointerdown == 1) {

              }
              lasttime = temps;
            }
            break;
          case MotionEvent.ACTION_POINTER_UP:
            break;
          case MotionEvent.ACTION_UP:
            pos0fx = ev.getX();
            pos0fy = ev.getY();
            if(System.currentTimeMillis() - timetouchdown > minmovingtime){
              wemoved = true;
            }
            if (maxpointerdown == 1 && !wemoved) {

            }
            break;
          default :
            llog.d(TAG, "on ordonne autre");
            break;
        }
        return true;
      }
    });

    // Inflate the layout for this fragment
    return pictureview;
  }

  @Override
  public void onResume() {
    super.onResume();
  }

  @Override
  public void onStart() {
    super.onStart();
  }

  @Override
  public void onPause() {
    super.onPause();
  }

  @Override
  public void onDestroy() {
    super.onDestroy();
  }

  @Override
  public void onAttach(Context context) {
    super.onAttach(context);
  }

  /**
   * This interface must be implemented by activities that contain this
   * fragment to allow an interaction in this fragment to be communicated
   * to the activity and potentially other fragments contained in that
   * activity.
   * <p>
   * See the Android Training lesson <a href=
   * "http://developer.android.com/training/basics/fragments/communicating.html"
   * >Communicating with Other Fragments</a> for more information.
   */
  public interface OnFragmentInteractionListener {
    // TODO: Update argument type and name
    void onFragmentInteraction(Uri uri);
  }
}
