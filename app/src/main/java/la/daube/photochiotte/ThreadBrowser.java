package la.daube.photochiotte;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.os.Build;
import android.view.Surface;

import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class ThreadBrowser implements Runnable {
  private static final String TAG = "YYYtb";

 /* private long[] tempsinit = new long[30];
  private long[] tempsdiff = new long[30];
  private int[] tempscompte = new int[30];*/

  private Surf mysurf;
  private int currid = -1;

  private volatile int threadbrowser = 0;

  private boolean clickquicknav = false;
  private boolean clickquicknavmenu = false;
  private boolean clicktohandle = false;
  private float clicktohandlex = 0.0f;
  private float clicktohandley = 0.0f;

  private Gallery model;
  private volatile long POLL_TIMEOUT = Gallery.polltimeoutdefault;

  private float bpx = 0;
  private float bpy = 0;
  private float bscale = 0;
  private int rotation = 0;

  private boolean selectiona = false;
  private boolean selectionz = false;
  private int selectionadoss = -1;
  private int selectionafich = -1;
  private int selectionzdoss = -1;
  private int selectionzfich = -1;
  private boolean selectionplus = false;
  private boolean selectionmoins = false;
  //private boolean selectfilesonlynofolder = false;

  private String[] nworder = null;

  protected ThreadBrowser(Gallery mmodel) {
    model = mmodel;
  }

  private int recheckThreadDiscoverRunning = 0;
  private int wallpaperspeed;
  private boolean wallpapertouchenabled;
  private boolean wallpapercleareachtime;
  private boolean wallpapercalendarwidget;
  private boolean wallpapercountdownwidget;
  private boolean wallpapermonitorwidget;
  private boolean lockscreencalendarwidget;
  private boolean lockscreencountdownwidget;
  private boolean lockscreenmonitorwidget;
  private float lockscreenminratio;
  private float lockscreenmaxratio;
  private float wallpaperminratio;
  private float wallpapermaxratio;
  private String wallpapercollection = null;
  private String lockscreencollection = null;
  private int musiccollectionnamel = 0;
  private ArrayList<String> musiccollectionname = new ArrayList<>();
  private ArrayList<String> musiccollectionaddress = new ArrayList<>();
  private long currtime = System.currentTimeMillis();

  boolean checkready() {
    for (int i = 0 ; i < 1000 ; i++) {
      if (model.dossierminiature != null
          && model.surf.size() > 0)
        return true;
      try {
        Thread.sleep(10);
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
      if (i % 50 == 0) {
        llog.d(TAG, "error checkready() " + i);
      }
    }
    return false;
  }

  @Override
  public void run() {
    model.threadbrowserrunning = true;
    llog.d(TAG, currid + " ++++++++++++++++++++ ThreadBrowser");
    if (!checkready()) {
      llog.d(TAG, "error checkready bail out");
      model.threadbrowserrunning = false;
      return;
    }

    musiccollectionnamel = model.preferences.getInt("musiccollectionnamel", 0);
    for (int i = 0 ; i < musiccollectionnamel ; i++) {
      musiccollectionname.add(model.preferences.getString("musiccollectionname" + i, null));
      musiccollectionaddress.add(model.preferences.getString("musiccollectionaddress" + i, null));
    }
    wallpapertouchenabled = model.preferences.getBoolean("wallpapertouchenabled", false);
    wallpapercleareachtime = model.preferences.getBoolean("wallpapercleareachtime", false);
    lockscreencalendarwidget = model.preferences.getBoolean("lockscreencalendarwidget", false);
    lockscreencountdownwidget = model.preferences.getBoolean("lockscreencountdownwidget", false);
    lockscreenmonitorwidget = model.preferences.getBoolean("lockscreenmonitorwidget", false);
    wallpapercalendarwidget = model.preferences.getBoolean("wallpapercalendarwidget", false);
    wallpapercountdownwidget = model.preferences.getBoolean("wallpapercountdownwidget", false);
    wallpapermonitorwidget = model.preferences.getBoolean("wallpapermonitorwidget", false);
    wallpaperspeed = model.preferences.getInt("wallpaperspeed", 30);
    wallpaperminratio = model.preferences.getFloat("wallpaperminratio", 20.0f);
    wallpapermaxratio = model.preferences.getFloat("wallpapermaxratio",  60.0f);
    wallpapercollection = model.preferences.getString("wallpapercollection",  null);
    lockscreenminratio = model.preferences.getFloat("lockscreenminratio", 20.0f);
    lockscreenmaxratio = model.preferences.getFloat("lockscreenmaxratio",  60.0f);
    lockscreencollection = model.preferences.getString("lockscreencollection",  null);

    long currtime = System.currentTimeMillis();
    model.filmstripreftime = currtime - (currtime % model.filmstripanimatetime);

    long threadbrowserstarttime = currtime;

    //boolean nextpictureposition0 = false;
    boolean updateonefinaltime = false;
    try {
      model.commandethreaddatabase.put(new String[]{"dossiersplitview"});
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
    currid = 0;
    mysurf = model.surf.get(currid);
    mysurf.putbigpictureinmemory = true;
    //mysurf.centeronscreen = true;

    if (model.availCollectionsOnDiskl == -1) {
      try {
        model.commandethreaddatabase.put(new String[]{"rescancollectionsondisk"});
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
    }

    Gallery.isMyServiceRunning(backgroundService.class, model.activitycontext);

    /*
    
        String lienhref = "";
        String lienminiature = "";
        
        //String testlink = InternetSession.gethtml(lienhref, lienminiature);
        String finalink = InternetSession.getthepicturelink(lienhref, lienminiature);
        llog.d(TAG, currid + " final link = " + finalink);

     */

    // adb -s emulator-5554 pull /storage/emulated/0/out.html ; sed -i 's/</\n</g' out.html ; kate out.html ; rm out.html

    /*
      String lienhref = "";
      String lienminiature = "";
      
      String finalink = InternetSession.getthepicturelink(lienhref, lienminiature);
      llog.d(TAG, currid + " final link = " + finalink);
      //String html = InternetSession.gethtml(finalink, lienminiature);
      //InternetSession.savehtml(html);
      //html = InternetSession.gethtml(finalink, lienminiature);
      //InternetSession.savehtml(html);
      byte[] mesbyte = InternetSession.getPicture(model, finalink, new media(), false);
      llog.d(TAG, String.format("%x%x%x%x", mesbyte[0], mesbyte[1], mesbyte[2], mesbyte[3]));
    */
    
    /*
      Intent mintent = new Intent();
      mintent.setAction(myViewModel.broadcastname);
      mintent.putExtra("goal", "status");
      mintent.putExtra("status", "(goto) recaptcha : http://example.com (cancel)");
      mintent.putExtra("data", "recaptcha");
      mintent.putExtra("lienhref", "http://example.com");
      mintent.putExtra("lienminiature","http://example.com");
      LocalBroadcastManager.getInstance(model.activitycontext).sendBroadcast(mintent);

    */


    /*new Thread(new Runnable() {
      @Override
      public void run() {
        try {
          Thread.sleep(5000);
        } catch (InterruptedException e) {
        }
        try {
          model.commandethreadminiature.put(new String[]{"supercleanup"});
        } catch (InterruptedException e) {
          e.printStackTrace();
        }
        model.changeBigPicture(0, 3, 0, 0, 0, true, false);
        try {
          Thread.sleep(1000);
        } catch (InterruptedException e) {
        }
        model.changeBigPicture(0, 5, 0, 0, 0, true, false);
        try {
          Thread.sleep(1000);
        } catch (InterruptedException e) {
        }
        model.changeBigPicture(0, 7, 0, 0, 0, true, false);
        try {
          Thread.sleep(1000);
        } catch (InterruptedException e) {
        }
        model.changeBigPicture(0, 9, 0, 0, 0, true, false);
        try {
          Thread.sleep(1000);
        } catch (InterruptedException e) {
        }
        model.changeBigPicture(0, 10, 0, 0, 0, true, false);
        try {
          Thread.sleep(1000);
        } catch (InterruptedException e) {
        }
        llog.d(TAG, "playvid " + model.getFolderName(10));
        Intent intent = new Intent();
        intent.setAction(myViewModel.broadcastname);
        intent.putExtra("goal", "playvid");
        intent.putExtra("id", 0);
        intent.putExtra("dossierprincipal", 10);
        intent.putExtra("fichierprincipal", 0);
        LocalBroadcastManager.getInstance(model.activitycontext).sendBroadcast(intent);
        llog.d(TAG, "playvid");
      }
    }).start();*/

    /*new Thread(new Runnable() {
      @Override
      public void run() {
        try {
          Thread.sleep(3000);
        } catch (InterruptedException e) {
        }

        try {
          model.commandethreaddatabase.put(new String[]{"chercheFichierDansDatabaseSinonRescanSonDossier", "/storage/emulated/0/Down8/", null, "-1"});
        } catch (InterruptedException e) {
          e.printStackTrace();
        }

      }
    }).start();*/

    /*model.iswatch = true;
    String mshg = "";
    String mshg2 = "\naaaaaaa";
    for (int i = 0 ; i < 80 ; i++)
      mshg += mshg2 + " " + i;
    model.message(mshg);*/

    recheckThreadDiscoverRunning = 1;

    while (true) {
      boolean encore = true;
      while (encore) {

        // on vide la liste
        if (model.commandethreadbrowser.isEmpty() && updateonefinaltime) {
          /**
           *   on rafraîchit l'écran une dernière fois
           *   tout à la fin si jamais on est disponible
           *   car les options sont modifiées en même temps que l'on dessine
           *   et on risque donc de ne pas avoir la version finale de l'écran
           */
          updateonefinaltime = false;
          nworder = new String[]{"-1", "update"};
          // on ne modifie pas myid on reste sur le précédent
        } else {
          try {
            nworder = model.commandethreadbrowser.poll(POLL_TIMEOUT, TimeUnit.MILLISECONDS);
          } catch (InterruptedException e) {
            e.printStackTrace();
          }
          model.threadbrowserrunninglastpoll = System.currentTimeMillis();
          updateonefinaltime = false;
        }

        if (nworder == null) {
          nworder = new String[]{"-1", "poll"};
          int surfl = model.surf.size();
          for (int i = 0 ; i < surfl ; i++) {
            currid = (currid + 1) % surfl;
            mysurf = model.surf.get(currid);
            if (mysurf.fragmenttype == Surf.FRAGMENT_TYPE_BROWSER)
              break;
          }
        }
        int targetid = Integer.parseInt(nworder[0]);
        if (targetid != -1 && targetid < model.surf.size()) {
          if (targetid != currid)
            encore = false;
          currid = targetid;
          mysurf = model.surf.get(currid);
        }

        if (nworder[1].equals("quit")) {
          if (model.threadbrowseron) {
            llog.d(TAG, currid + " ////////////////////// thread browser keep running");
          } else {
            if (model.tempBitmap != null)
              if (!model.tempBitmap.isRecycled())
                model.tempBitmap.recycle();
            model.tempBitmap = null;
            model.threadbrowserrunning = false;
            llog.d(TAG, currid + " ---------------------- thread browser");
            return;
          }

        } else if (!model.threadbrowserrunning) {
          // si on pause on efface toutes les commandes moins importantes suivantes dans la queue


        } else if (nworder[1].equals("surfacechanged")) {
          // il faut recréer ça si on modifie la hauteur
          mysurf.redrawsplitviewbitmap = true;
          // on ne veut pas que ça bouge quand on modifie le split ou la rotation
          if (model.isandroidtv || mysurf.showfoldernamesforce) {
            // sauf si AndroidTV autrement démarre en haut à gauche
            mysurf.putbigpictureinmemory = true;
            mysurf.centeronscreen = true;
          }
          try {
            model.commandethreaddatabase.put(new String[]{"dummy"});
          } catch (InterruptedException e) {
            e.printStackTrace();
          }
          updateonefinaltime = true;
          encore = false;

        } else if (nworder[1].equals("start")) {
          llog.d(TAG, currid + " : start fragmenttype FRAGMENT_TYPE_BROWSER");
          mysurf.fragmenttype = Surf.FRAGMENT_TYPE_BROWSER;

          mysurf.zoomFitMode = model.preferences.getInt("zoomFitMode", mysurf.zoomFitMode);
          mysurf.zoomComicMode = model.preferences.getInt("zoomComicMode", mysurf.zoomComicMode);

          updateonefinaltime = true;
          encore = false;

        } else if (nworder[1].equals("playvid")) {
          Intent intent = new Intent();
          intent.setAction(Gallery.broadcastname);
          intent.putExtra("goal", "playvid");
          intent.putExtra("id", currid);
          intent.putExtra("dossierprincipal", Integer.parseInt(nworder[2]));
          intent.putExtra("fichierprincipal", Integer.parseInt(nworder[3]));
          LocalBroadcastManager.getInstance(model.activitycontext).sendBroadcast(intent);

        } else if (nworder[1].equals("poll")) {
          // ne pas dépendre de currid ici ça peut venir sans currid
          int surfl = model.surf.size();
          currtime = System.currentTimeMillis();

          /** peu importe le fragmenttype */
          if (model.lastmessagetime != 0) {
            if (currtime - model.lastmessagetime > Gallery.messagetimestay) {
              model.message();
            }
          }



          //mysurf.drawgraph = true; // TODO : dummy remove
          if (mysurf.drawgraph) {
            if (System.currentTimeMillis() - model.lastgraph > Gallery.monitordeltatime * 1000) {

              /*String lockscreenmonitorwidgetset = model.preferences.getString("screenmonitorwidgetset", Gallery.lockscreenmonitorwidgetset);
              if (lockscreenmonitorwidgetset != null) {
                Matcher m = Gallery.lockscreenmonitorwidgetsetpattern.matcher(lockscreenmonitorwidgetset);
                if (m.find()) {
                  model.monitorwx = mysurf.mywidth * Integer.parseInt(m.group(1)) / 100.0f;
                  model.monitorwy = mysurf.myheight * Integer.parseInt(m.group(2)) / 100.0f;
                  model.monitorww = mysurf.mywidth * Integer.parseInt(m.group(3)) / 100.0f;
                  model.monitorwh = mysurf.myheight * Integer.parseInt(m.group(4)) / 100.0f;
                  model.monitormaxtime = Integer.parseInt(m.group(5));
                }
              }*/

              model.lastgraph = System.currentTimeMillis();
              try {
                //model.commandethreadminiature.put(new String[]{"drawgraph", model.dossierconfig}); // TODO : dummy remove
                model.commandethreadminiature.put(new String[]{"drawgraph", model.internalStorageDir});
              } catch (InterruptedException e) {
              }
            }
          }

          long filmstripreftime = currtime - (currtime % model.filmstripanimatetime);
          if (filmstripreftime != model.filmstripreftime) {
            //llog.d(TAG, filmstripreftime + " != " + model.filmstripreftime);
            model.filmstripreftime = filmstripreftime;
            for (int i = 0; i < surfl; i++) {
              Surf thissurf = model.surf.get(i);
              thissurf.filmstripreftime = filmstripreftime;
            }

            if (model.thereIsAnUpdatableFolder) {
              int collectionsl = model.collections.size();
              for (int i = 0 ; i < collectionsl ; i++) {
                Collection collection = model.collections.get(i);
                if (collection != null) {
                  if (collection.address != null) {
                    if (collection.updateDeltaTime > 0) {
                      if (currtime - collection.timestamplastitem > 1000L * ((long) collection.updateDeltaTime)) {
                        llog.d(TAG, "update " + collection.address + " " + (currtime - collection.timestamplastitem) + " > " + collection.updateDeltaTime + "*1000");
                        collection.timestamplastitem = System.currentTimeMillis();
                        try {
                          model.commandethreaddatabase.put(new String[]{"updatecollection", String.valueOf(currid), collection.address});
                        } catch (InterruptedException e) {
                          e.printStackTrace();
                        }
                      }
                    }
                  }
                }
              }
            }
          }

          /** uniquement si browser (ou si none = obligatoire) */
          if (mysurf.fragmenttype != Surf.FRAGMENT_TYPE_BROWSER && mysurf.fragmenttype != Surf.FRAGMENT_TYPE_NONE)
            continue;

          for (int i = 0; i < surfl; i++) {
            Surf thissurf = model.surf.get(i);
            if (thissurf.optiondiaporamaactive) {
              if (thissurf.OptionMenuShown && thissurf.diaporamalastchange > 0) {
                thissurf.diaporamalastchange = currtime;
              } else if (!thissurf.OptionMenuShown) {
                if (thissurf.fragmenttype == Surf.FRAGMENT_TYPE_BROWSER) {
                  if (currtime - thissurf.diaporamalastchange > thissurf.diaporamadeltatime * 1000
                          || thissurf.diaporamalastchange == -1) {
                    if (thissurf.diaporamalastchange > 0) {
                      //llog.d(TAG, "changeBigPicture diaporamalastchange " + i);
                      if (thissurf.optiondiaporamalinear == Surf.DIAPORAMA_RANDOM_FOLDER) {
                        int fc = model.getFolderFileCount(thissurf.ordnerIndex);
                        if (thissurf.mediaIndex == fc - 1) {
                          int d = 0;
                          for (int k = 0 ; k < 10 ; k++) {
                            d = Gallery.rand.nextInt(model.folderCount);
                            if (model.getFolderIsOnline(thissurf.ordnerIndex) == model.getFolderIsOnline(d))
                              break;
                          }
                          model.changeBigPicture(i, d, 0, 0, 0, true, false);
                        } else {
                          model.changeBigPicture(i, -1, 0, -1, +1, true, true);
                        }
                      } else if (thissurf.optiondiaporamalinear == Surf.DIAPORAMA_RANDOM) {
                        int d = 0;
                        for (int k = 0 ; k < 10 ; k++) {
                          d = Gallery.rand.nextInt(model.folderCount);
                          if (model.getFolderIsOnline(thissurf.ordnerIndex) == model.getFolderIsOnline(d))
                            break;
                        }
                        model.showrandomfilewhenfolderready = true;
                        model.changeBigPicture(i, d, 0, 0, 0, true, false);
                      } else if (thissurf.optiondiaporamalinear == Surf.DIAPORAMA_LINEAR) {
                        model.changeBigPicture(i, -1, 0, -1, +1, true, true);
                      }
                    } else {
                      thissurf.diaporamalastchange = currtime;
                    }
                    thissurf.cachefromlefttoright = true;
                    if (Media.media_video == model.getMediaType(thissurf.ordnerIndex, thissurf.mediaIndex)) {
                      try {
                        model.commandethreadbrowser.put(new String[]{String.valueOf(i), "playvid",
                            String.valueOf(thissurf.ordnerIndex), String.valueOf(thissurf.mediaIndex)});
                      } catch (InterruptedException e) {
                      }
                    }
                  }
                }
              }
            }
          }

          if (recheckThreadDiscoverRunning > 0) {
            recheckThreadDiscoverRunning--;
            Gallery.isMyServiceRunning(backgroundService.class, model.activitycontext);
          }

          if (Gallery.backgroundService != null) {
            if (Gallery.backgroundService.torsearchfound()) {
              model.message("found searched value.");
              Collection collection = new Collection();
              collection.printName = Gallery.virtualCollectionFolder + "tor";
              collection.reloadTorCollectionFromDisk(model.dossiercollections);
              if (collection.elementList.size() == 0) {
                model.message("error loading collection.");
              } else {
                /*List<Ordner> gallery = collection.collectionToDisplay(null);
                for (int i = 0 ; i < gallery.size() ; i++) {
                  Ordner ordner = gallery.get(i);
                  model.addFolder(model.folderCount, ordner);
                }*/
                model.collectionsToDisplay(collection.address, model.folderCount);
              }
            }
          }

          if (model.redrawsplitviewbitmapm) {
            model.redrawsplitviewbitmapm = false;
            for (int i = 0; i < surfl; i++) {
              Surf thissurf = model.surf.get(i);
              thissurf.redrawsplitviewbitmap = true;
            }
          }

          encore = false;

        } else if (nworder[1].equals("musicsearch")) {

          if (Gallery.backgroundService == null) {
            llog.d(TAG, "error no music background service");
          } else {
            int ordnerIndex = 0;
            int mediaIndex = 0;
            if (Gallery.backgroundService.threadmusic != null) {
              if (Gallery.backgroundService.threadmusic.model.surf.size() > 0) {
                Surf currsurf = Gallery.backgroundService.threadmusic.model.surf.get(0);
                ordnerIndex = currsurf.ordnerIndex;
                mediaIndex = currsurf.mediaIndex;
                try {
                  model.commandethreaddatabase.put(new String[]{"searchfile", currsurf.mediaIndexAddress, String.valueOf(currid),
                      String.valueOf(ordnerIndex), String.valueOf(mediaIndex)});
                } catch (InterruptedException e) {
                }
              }
            }
            if (nworder[2].equals("askwhat")) {
              Intent intent2 = new Intent();
              intent2.setAction(Gallery.broadcastname);
              intent2.putExtra("goal", "musicsearch");
              intent2.putExtra("id", 0);
              intent2.putExtra("ordnerIndex", ordnerIndex);
              intent2.putExtra("mediaIndex", mediaIndex);
              intent2.putExtra("searchmusic", true);
              LocalBroadcastManager.getInstance(model.activitycontext).sendBroadcast(intent2);
            }
          }

        } else if (nworder[1].equals("ChangeCurrentDisplayedPicture")) {
          int d = Integer.parseInt(nworder[2]);
          int f = Integer.parseInt(nworder[3]);
          if (nworder[4].equals("-1")) {
            int surfl = model.surf.size();
            for (int i = 0; i < surfl; i++) {
              model.changeBigPicture(i, d, 0, f, 0, true, false);
              llog.d(TAG, i + " ChangeCurrentDisplayedPicture " + d + " " + f);
            }
          } else {
            int i = Integer.parseInt(nworder[4]);
            model.changeBigPicture(i, d, 0, f, 0, true, false);
            llog.d(TAG, i + " ChangeCurrentDisplayedPicture " + d + " " + f);
          }
          updateonefinaltime = true;
          encore = false;

        } else if (nworder[1].equals("reloadthisfolder")) {
          updateonefinaltime = true;
          encore = false;

        } else if (nworder[1].equals("displayinfo")) {

          StringBuilder msg = new StringBuilder();
          msg.append(String.format("%d/%d\n", (mysurf.mediaIndex + 1), model.getFolderFileCount(mysurf.ordnerIndex)));

          String ordneraddress = model.getOrnderAddressEscaped(mysurf.ordnerIndex);
          String address = model.getMediaAddress(mysurf.ordnerIndex, mysurf.mediaIndex);
          String addressescaped = model.getMediaAddressEscaped(mysurf.ordnerIndex, mysurf.mediaIndex);
          String fichname = "";
          if (ordneraddress != null && addressescaped.length() > 0) {
            msg.append(ordneraddress);
            msg.append("\n");
            int dossi = addressescaped.lastIndexOf("/");
            if (dossi >= 0) {
              String doss = addressescaped.substring(0, dossi + 1);
              if (!ordneraddress.equals(doss)) {
                if (!(ordneraddress + "/").equals(doss)) {
                  msg.append(doss);
                  msg.append("\n");
                }
              }
              fichname = addressescaped.substring(dossi + 1);
              msg.append(fichname);
              msg.append("\n");
            }
          }

          String printname = model.getMediaPrintName(mysurf.ordnerIndex, mysurf.mediaIndex);
          if (printname != null) {
            if (!printname.equals(fichname)) {
              msg.append(printname);
              msg.append("\n");
            }
          }

          if (mysurf.isincache(address)) {
            float[] cachedbitmapnfo = mysurf.getcachedbitmapnfo(address);
            msg.append(String.format("%.1fMpx %dx%d\n", (cachedbitmapnfo[0] * cachedbitmapnfo[1]) / 1000000.0f, (int) cachedbitmapnfo[0], (int) cachedbitmapnfo[1]));
          }

          String metadata = model.getMediaMetadata(mysurf.ordnerIndex, mysurf.mediaIndex);
          if (metadata != null) {
            msg.append(metadata);
          }

          model.message(msg.toString());
          updateonefinaltime = true;
          encore = false;

        } else if (nworder[1].equals("menudessiner")) {
          /**
           *    on attrape tous les menudessiner init on n'en rate jamais un seul
           *    move on en rate pour ne pas traîner
           */
          updateonefinaltime = true;
          encore = false;

        } else if (nworder[1].equals("filter")) {
          /**
           *    on attrape tous les filter on n'en rate jamais un seul
           */
          try {
            model.commandebigimagethreadqueue.put(new String[]{"cachebitmap", String.valueOf(currid), String.valueOf(mysurf.ordnerIndex), String.valueOf(mysurf.mediaIndex), String.valueOf(mysurf.bigimagecurrentlydisplayed), String.valueOf(true), "0"});
          } catch (InterruptedException e) {
            e.printStackTrace();
          }
          updateonefinaltime = true;
          encore = false;
        } else if (nworder[1].equals("option")) {
          /**
           *    on attrape tous les option on n'en rate jamais un seul
           */
          updateonefinaltime = true;
          encore = false;
        } else if (nworder[1].equals("next tv")) {
          /**
           *    on attrape tous les next on n'en rate jamais un seul
           */
          encore = false;
        } else if (nworder[1].equals("next")) {
          /**
           *    on attrape tous les next on n'en rate jamais un seul
           */
          encore = false;
        } else if (nworder[1].equals("dontmissupdate")) {
          /**
           *    on attrape tous les initupdate on n'en rate jamais un seul
           */
          try {
            model.commandethreaddatabase.put(new String[]{"dummy"});
          } catch (InterruptedException e) {
            e.printStackTrace();
          }
          updateonefinaltime = true;
          encore = false;
        } else if (model.commandethreadbrowser.isEmpty()) {
          /**
           toutes les autres commandes :
           on attend que la queue soit vide
           et on ne prend que la dernière
           */
          encore = false;
        }
      }

      if (mysurf.fragmenttype != Surf.FRAGMENT_TYPE_BROWSER && mysurf.fragmenttype != Surf.FRAGMENT_TYPE_NONE) {
        llog.d(TAG, currid + " fragmenttype " + mysurf.fragmenttype);
        continue;
      }

      if (mysurf.ScreenWidth < 4 || mysurf.ScreenHeight < 4 || mysurf.browserSurfaceHolder == null) {
        llog.d(TAG, currid + " Screen too small " + mysurf.ScreenWidth + "x" + mysurf.ScreenHeight + " or mysurf.browserSurfaceHolder == null ");
        continue;
      }

      /**
       *   on bloque ces valeurs car le thread principal les change en continu
       */
      bpx = (int) mysurf.bpx;
      bpy = (int) mysurf.bpy;
      bscale = mysurf.bscale;
      mysurf.bpxmax = bpx + mysurf.imagewidth; // achtung kein lust
      mysurf.bpymax = bpy + mysurf.imageheight;
      boolean fitpicturetowindow = false;
      boolean fitfull = false;

      if (model.isandroidtv || mysurf.showfoldernamesforce) {
        mysurf.showfoldernames = true;
      } else {
        mysurf.showfoldernames = mysurf.OptionMenuShown;
      }

      //threadbrowser += 1;
      //if (threadbrowser % 100 == 0)
      if (Gallery.debugi >= 2) {
        String msg = currid + " :"
            + " " + mysurf.bscale + "x"
            + " " + mysurf.bpx + "," + mysurf.bpy + " -> " + mysurf.bpxmax + "," + mysurf.bpymax;
        msg += " order " + nworder[1]
            + " d" + mysurf.ordnerIndex + " f" + mysurf.mediaIndex
            + " " + mysurf.ordnerIndexAddress + " " + mysurf.mediaIndexAddress
            + " precedent " + mysurf.fichierprecedent;
        msg += " zoommode " + mysurf.zoommode;
        if (mysurf.centeronscreen)
          msg += " centeronscreen";
        if (mysurf.putbigpictureinmemory)
          msg += " putbigpictureinmemory";
        if (model.showthisfilelockjump)
          msg += " showthisfilelockjump";
        llog.w(TAG, msg);
      }

      clicktohandle = false;
      clickquicknav = false;
      clickquicknavmenu = false;
      // pour ceux-là on attend que la liste soit vide et on ne prend que le dernier
      // on se permet donc d'en rater plein

      if (nworder[1].equals("next tv")) {
        if (nworder[2].equals("droite")) {
          if (mysurf.OptionMenuShown) {
            if (mysurf.cursorx < mysurf.SettingsXmin)
              mysurf.cursorx = mysurf.SettingsXmin + mysurf.SettingsWidth * 0.20f;
            else
              mysurf.cursorx = mysurf.SettingsXmin + mysurf.SettingsWidth * 0.80f;
            mysurf.cursory = model.settingsYmin + model.GenericCaseH * 0.5f;
          } else {
            model.changeBigPicture(currid, -1, 0, -1, +1, true, false);
            mysurf.cachefromlefttoright = true;
          }
        } else if (nworder[2].equals("gauche")) {
          if (mysurf.OptionMenuShown) {
            if (mysurf.cursorx > mysurf.SettingsXmin + mysurf.SettingsWidth * 0.50f) {
              mysurf.cursorx = mysurf.SettingsXmin + mysurf.SettingsWidth * 0.20f;
              mysurf.cursory = model.settingsYmin + model.GenericCaseH * 0.5f;
            } else {
              mysurf.cursorx = mysurf.mywidth * 0.5f;
              mysurf.cursory = mysurf.myheight * 0.5f;
            }
          } else {
            model.changeBigPicture(currid, -1, 0, -1, -1, true, false);
            mysurf.cachefromlefttoright = false;
          }
        } else if (nworder[2].equals("haut")) {
          model.weLastClickedDPadUp = -1;
          if (mysurf.OptionMenuShown && mysurf.cursorx > mysurf.SettingsXmin) {
            mysurf.SettingsYmin += model.GenericCaseH + model.GenericInterSpace;
          } else {
            model.changeBigPicture(currid, -1, -1, -1, 0, true, false);
          }
        } else if (nworder[2].equals("bas")) {
          model.weLastClickedDPadUp = 1;
          if (mysurf.OptionMenuShown && mysurf.cursorx > mysurf.SettingsXmin) {
            mysurf.SettingsYmin -= model.GenericCaseH + model.GenericInterSpace;
          } else {
            model.changeBigPicture(currid, -1, +1, -1, 0, true, false);
          }
        } else if (nworder[2].equals("ok")) {
          if (mysurf.OptionMenuShown) {
            /*if (mysurf.cursorx == -1.0f)
              mysurf.cursorx = mysurf.SettingsXmin + mysurf.SettingsWidth * 0.20f;
            mysurf.cursory = model.settingsYmin + model.GenericCaseH * 0.5f;*/
          } else {
            float x1 = mysurf.bpx;
            float x2 = mysurf.bpx + mysurf.imagewidth;
            if (x1 < 0 || x1 > mysurf.mywidth)
              x1 = 0;
            if (x2 < 0 || x2 > mysurf.mywidth)
              x2 = mysurf.mywidth;
            mysurf.cursorx = (x1 + x2) * 0.5f;
            float y1 = mysurf.bpy;
            float y2 = mysurf.bpy + mysurf.imageheight;
            if (y1 < 0 || y1 > mysurf.myheight)
              y1 = 0;
            if (y2 < 0 || y2 > mysurf.myheight)
              y2 = mysurf.myheight;
            mysurf.cursory = (y1 + y2) * 0.5f;
          }
          nworder = new String[]{""+currid, "next", String.valueOf(mysurf.cursorx), String.valueOf(mysurf.cursory)};
        } else if (nworder[2].equals("menu")) {
          mysurf.OptionMenuShown = !mysurf.OptionMenuShown;
          if (model.isandroidtv || mysurf.showfoldernamesforce) {
            mysurf.showfoldernames = true;
          } else {
            mysurf.showfoldernames = mysurf.OptionMenuShown;
          }
          if (mysurf.OptionMenuShown) {
            //mysurf.SettingsYmin = model.settingsYmin;
            mysurf.cursorx = mysurf.SettingsXmin + mysurf.SettingsWidth * 0.20f;
            mysurf.cursory = model.settingsYmin + model.GenericCaseH * 0.5f;
          } else {
            mysurf.cursorx = mysurf.mywidth * 0.5f;
            mysurf.cursory = mysurf.myheight * 0.5f;
          }
          if (!mysurf.OptionMenuShown) {
            //mysurf.foldoptions();
          }
        }
      }

      if (nworder[1].equals("update")) {

      } else if (nworder[1].equals("move")) {

      } else if (nworder[1].equals("rescale")) {

      } else if (nworder[1].equals("next")) {
        float posfx = Float.parseFloat(nworder[2]);
        float posfy = Float.parseFloat(nworder[3]);
        boolean onnapasbouge = false;
        if (!mysurf.OptionMenuShown && !model.isandroidtv && mysurf.justnexted != 0 &&
                Math.abs(mysurf.lastposx - posfx) < 10 && Math.abs(mysurf.lastposy - posfy) < 10) {
          onnapasbouge = true;
        } else {
          mysurf.justnexted = 0;
        }
        mysurf.lastposx = posfx;
        mysurf.lastposy = posfy;

        if (onnapasbouge && mysurf.justnexted == 1) {
          /**
           *   on a cliqué la grosse suivant
           */
          model.changeBigPicture(currid, -1, 0, -1, +1, true, false);
          mysurf.cachefromlefttoright = true;
          mysurf.justnexted = 1;
        } else if (onnapasbouge && mysurf.justnexted == -1) {
          /**
           *   on a cliqué la grosse précédent
           */
          model.changeBigPicture(currid, -1, 0, -1, -1, true, false);
          mysurf.cachefromlefttoright = false;
          mysurf.justnexted = -1;
        } else if (mysurf.OptionMenuShown && posfx < mysurf.GraphWidth) {
          /**
           *   graphic splitviewbitmap
           */
          clickquicknav = true;
          clicktohandlex = posfx;
          clicktohandley = posfy;

        } else if (mysurf.OptionMenuShown && posfx > mysurf.SettingsXmin && mysurf.SettingsYmin < posfy && posfy < mysurf.SettingsYmax) {
        //} else if (mysurf.showquicknavbar && mysurf.SettingsYmin < posfy && posfy < mysurf.SettingsYmax && clickedmenu()) {
          /**
           *   on a cliqué un bouton du menu
           */
          clickquicknavmenu = true;
          clicktohandlex = posfx;
          clicktohandley = posfy;

        } else if (model.showmenuprevnextbutton && !mysurf.OptionMenuShown && posfx < mysurf.CaseInvisiblePrecXmax && mysurf.CaseInvisiblePrecSuivYmin < posfy) {
          /**
           *   on a cliqué le bouton précédent
           */
          model.changeBigPicture(currid, -1, 0, -1, -1, true, false);
          mysurf.cachefromlefttoright = false;
        } else if (model.showmenuprevnextbutton && !mysurf.OptionMenuShown && mysurf.CaseInvisibleSuivXmin < posfx && mysurf.CaseInvisiblePrecSuivYmin < posfy) {
          /**
           *   on a cliqué le bouton suivant
           */
          model.changeBigPicture(currid, -1, 0, -1, +1, true, false);
          mysurf.cachefromlefttoright = true;
        } else if (model.showmenuprevnextbutton && !mysurf.OptionMenuShown && posfx < mysurf.CaseInvisibleOptionXmax && posfy < mysurf.CaseInvisibleOptionYmax) {
          /**
           *   on a cliqué le bouton menu en haut à gauche
           */
          mysurf.OptionMenuShown = true;
          mysurf.showfoldernames = true;
          //mysurf.SettingsYmax = mysurf.SettingsYmax - mysurf.SettingsYmin + model.GenericInterSpace;
          mysurf.SettingsYmin = model.settingsYmin;

        } else if (bpx <= posfx && posfx <= mysurf.bpxmax && bpy <= posfy && posfy <= mysurf.bpymax) {
          float imgwidth = mysurf.dstrect.right - mysurf.dstrect.left;
          float imgscreenleft = mysurf.dstrect.left + imgwidth * Gallery.percentofpicturecenter;
          float imgscreenright = mysurf.dstrect.right - imgwidth * Gallery.percentofpicturecenter;
          boolean executed = false;
          int mediatype = model.getMediaType(mysurf.ordnerIndex, mysurf.mediaIndex);
          if (mediatype == Media.media_button) {
            String[] buttonaction = model.getMediaIsAButtonAction(mysurf.ordnerIndex, mysurf.mediaIndex);
            if (buttonaction != null) {
              float xx = (posfx - bpx) / (float) mysurf.imagewidth;
              float yy = (posfy - bpy) / (float) mysurf.imageheight;
              executed = ThreadMiniature.buttonaction(buttonaction, model, xx, yy);
              if (executed) {
                if (Media.online_no == model.getMediaIsOnline(mysurf.ordnerIndex, mysurf.mediaIndex))
                  model.setMediaButtonAction(mysurf.ordnerIndex, mysurf.mediaIndex, null);
                mysurf.bitmapaskedforceredraw = true;
              }
            }
          }
          if (executed) {
            // consumed nothing left to do
          } else if (posfx > imgscreenright && imgwidth > 4) {
            /**
             *   on a cliqué la grosse suivant
             */
            model.changeBigPicture(currid, -1, 0, -1, +1, true, false);
            mysurf.cachefromlefttoright = true;
            mysurf.justnexted = 1;
          } else if (posfx < imgscreenleft && imgwidth > 4) {
            /**
             *   on a cliqué la grosse précédent
             */
            model.changeBigPicture(currid, -1, 0, -1, -1, true, false);
            mysurf.cachefromlefttoright = false;
            mysurf.justnexted = -1;
          } else if (mysurf.optionshowselectionactive) {
            /**
             *   on sélectionne la grande image
             */
            boolean onrefaitundernierupdate = false;
            if (selectiona) {
              selectionadoss = mysurf.ordnerIndex;
              selectionafich = mysurf.mediaIndex;
            } else if (selectionz) {
              selectionzdoss = mysurf.ordnerIndex;
              selectionzfich = mysurf.mediaIndex;
            } else if (selectionplus) {
              model.getFileSwitchSelected(mysurf.ordnerIndex, mysurf.mediaIndex);
              onrefaitundernierupdate = true;
            } else if (selectionmoins) {
              model.getFileSwitchSelected(mysurf.ordnerIndex, mysurf.mediaIndex);
              onrefaitundernierupdate = true;
            }
            if (selectionadoss >= 0 && selectionafich >= 0 && selectionzdoss >= 0 && selectionzfich >= 0) {
              for (int d = selectionadoss; d <= selectionzdoss; d++) {
                int debut = 0;
                int count = model.getFolderFileCount(d);
                int fin = count - 1;
                if (d == selectionadoss) {
                  debut = selectionafich;
                }
                if (d == selectionzdoss) {
                  fin = selectionzfich;
                }
                /*if (model.currCollectionAddress != null && !selectfilesonlynofolder && ((debut == 0 && fin == count - 1) || count == 0)) {
                  model.getFolderSetSelected(d, selectionplus);
                } else {*/
                  for (int f = debut; f <= fin; f++) {
                    model.getFileSetSelected(d, f, selectionplus);
                  }
                //}
                onrefaitundernierupdate = true;
              }
            }
            if (onrefaitundernierupdate) {
              try {
                model.commandethreadbrowser.put(new String[]{String.valueOf(currid), "update"});
              } catch (InterruptedException e) {
                e.printStackTrace();
              }
            }
          } else {
            /**   on a cliqué le centre de l'image sans bouger
             *    si on était en mode fitpicture on passe en mode originalsize centré
             *    et inversement
             */
            if (mysurf.optiondiaporamaactive && mysurf.diaporamalastchange > 0)
              mysurf.diaporamalastchange = System.currentTimeMillis();
            boolean lanceviewer = false;
            boolean isonlinelink = false;
            if (model.getMediaIsALinkThatCreatesAnOrdner(mysurf.ordnerIndex, mysurf.mediaIndex)) {
              isonlinelink = true;
            } else {
              if (mediatype != Media.media_picture
                  && mediatype != Media.media_app
                  && mediatype != Media.media_button
                  && mediatype != Media.media_collection) {
                lanceviewer = true;
              }
            }
            if (mediatype == Media.media_collection) {
              String colladdress = mysurf.mediaIndexAddress;
              try {
                model.commandethreaddatabase.put(new String[]{"LoadCollectionFromDiskOrTemp", "load", colladdress, "false"});
                model.commandethreaddatabase.put(new String[]{"addcollectionfolderstodisplay", String.valueOf(currid), colladdress, String.valueOf(mysurf.ordnerIndex + 1)});
              } catch (InterruptedException e) {
                e.printStackTrace();
              }
            } else if (lanceviewer) {
              if (model.searchMusicAskedFromWidget) {
                if (Gallery.backgroundService != null) {
                  llog.d(TAG, "search music backgroundService " + mysurf.mediaIndexAddress);
                  model.musiclastsearch = mysurf.mediaIndexAddress;
                  model.preferences.edit().putString("musiclastsearch", mysurf.mediaIndexAddress).apply();
                  Gallery.backgroundService.musicsearch(mysurf.mediaIndexAddress);
                } else {
                  llog.d(TAG, "search music null " + mysurf.mediaIndexAddress);
                }
              } else {
                try {
                  model.commandethreadbrowser.put(new String[]{String.valueOf(currid), "playvid",
                      String.valueOf(mysurf.ordnerIndex), String.valueOf(mysurf.mediaIndex)});
                } catch (InterruptedException e) {
                }
              }
              continue;
            } else if (isonlinelink) {
              try {
                model.commandethreaddatabase.put(new String[]{"browseonlinefolder", String.valueOf(currid), String.valueOf(mysurf.ordnerIndex), String.valueOf(mysurf.mediaIndex)});
              } catch (InterruptedException e) {
                e.printStackTrace();
              }
            } else {
              if (mysurf.zoommode == Gallery.ZoomFitToWindow) { // remove fit
                if (mysurf.zoomsetbyuserto == Gallery.ZoomComic) { // restore comic mode
                  bscale = mysurf.zoomsetbyusertobscale;
                  bpx = 0.0f;
                  bpy = 0.0f;
                  if (mysurf.mediaIndexAddress != null) {
                    if (mysurf.mediaIndexAddress.equals(mysurf.zoomsetbyusertoaddress)) {
                      bpx = mysurf.zoomsetbyusertobpx;
                      bpy = mysurf.zoomsetbyusertobpy;
                    } else {
                      String fichiername = mysurf.mediaIndexAddress;
                      if (mysurf.isincache(fichiername)) {
                        float[] cachedbitmapnfo = mysurf.getcachedbitmapnfo(fichiername);
                        float newwidthwillbe = cachedbitmapnfo[0] * bscale;
                        if (newwidthwillbe < mysurf.mywidth) {
                          bpx = (mysurf.mywidth - newwidthwillbe) * 0.5f;
                        } else {
                          if (mysurf.zoomComicMode == Gallery.ZoomComicTopLeft)
                            bpx = 0.0f;
                          else
                            bpx = mysurf.mywidth - newwidthwillbe;
                        }
                        float newheightwillbe = cachedbitmapnfo[1] * bscale;
                        if (newheightwillbe < mysurf.myheight) {
                          bpy = (mysurf.myheight - newheightwillbe) * 0.5f;
                        } else {
                          bpy = 0.0f;
                        }
                      }
                    }
                  }
                  mysurf.bscale = bscale;
                  mysurf.bpx = bpx;
                  mysurf.bpy = bpy;
                  mysurf.zoommode = Gallery.ZoomComic;
                } else {
                  if (model.isandroidtv) { // on retourne à la taille par défaut pour faciliter le zoom
                    bscale *= model.zoomconfortratio;
                    mysurf.bscale = bscale;
                  } else {                 // taille originale de l'image
                    bscale = 1.0f;
                    mysurf.bscale = bscale;
                  }
                  mysurf.centeronscreen = true;
                  mysurf.zoommode = Gallery.ZoomKeep;
                }
              } else { // fit
                if (mysurf.zoommode == Gallery.ZoomComic && mysurf.mediaIndexAddress != null) { // save bscale, bpx, bpy and then restore
                  mysurf.zoomsetbyusertoaddress = mysurf.mediaIndexAddress;
                  mysurf.zoomsetbyusertobscale = mysurf.bscale;
                  mysurf.zoomsetbyusertobpx = mysurf.bpx;
                  mysurf.zoomsetbyusertobpy = mysurf.bpy;
                }
                mysurf.zoomsetbyuserto = mysurf.zoommode;
                mysurf.zoommode = Gallery.ZoomFitToWindow;
                if (mysurf.zoomFitMode == Gallery.ZoomFitFill)
                  fitfull = true;
                else
                  fitpicturetowindow = true;
              }
            }
          }
        } else {
          clicktohandle = true;
          clicktohandlex = posfx;
          clicktohandley = posfy;
        }
      }

      int mtouslesdossiersl = model.folderCount;
      if (mtouslesdossiersl <= 0) {
        llog.d(TAG, "model.folderCount " + mtouslesdossiersl + " <= 0 sleep 30");
        drawall();
        try {
          Thread.sleep(30);
        } catch (InterruptedException e) {
        }
        continue;
      }

      if (mysurf.ordnerIndex >= mtouslesdossiersl) {
        llog.d(TAG, currid + " ordnerIndex = " + mysurf.ordnerIndex + " >= " + mtouslesdossiersl + " change ordner to " + (mtouslesdossiersl - 1));
        model.changeBigPicture(currid, mtouslesdossiersl - 1, 0, -1, 0, true, false);
        continue;
      }
      if (mysurf.ordnerIndex < 0) {
        llog.d(TAG, currid + " ordnerIndex = " + mysurf.ordnerIndex + " <0 " + mtouslesdossiersl + " change ordner to 0");
        model.changeBigPicture(currid, 0, 0, -1, 0, true, false);
        continue;
      }
      int folderFileCount = model.getFolderFileCount(mysurf.ordnerIndex);
      if (model.getFolderNotLoadedYet(mysurf.ordnerIndex)) {
        llog.d(TAG, currid + " folderNotLoadedYet commandethreaddatabase rechargetoutledossier " + mysurf.ordnerIndex);
        try {
          model.commandethreaddatabase.put(new String[]{"rechargetoutledossier", String.valueOf(currid), String.valueOf(mysurf.ordnerIndex), String.valueOf(false)}); // true;
        } catch (InterruptedException e) {
          e.printStackTrace();
        }
      } else if (folderFileCount == 0) {
        llog.d(TAG, currid + " main folder is empty " + mysurf.ordnerIndex + " " + mysurf.ordnerIndexAddress);
      } else {
        if (model.bookmarkshowthisfilewhenfolderready != null) {
          int f = model.findMediaAddress(mysurf.ordnerIndex, model.bookmarkshowthisfilewhenfolderready);
          if (f < 0) { // we can set it to 0 because the folder should have already been loaded at this point
            llog.d(TAG, currid + " bookmarkshowthisfilewhenfolderready error not found " + mysurf.ordnerIndex+ " " + mysurf.ordnerIndexAddress + " : " + model.bookmarkshowthisfilewhenfolderready + " : " + f);
          } else {
            llog.d(TAG, currid + " bookmarkshowthisfilewhenfolderready ok found " + mysurf.ordnerIndex+ " " + mysurf.ordnerIndexAddress + " : " + model.bookmarkshowthisfilewhenfolderready + " : " + f);
            model.bookmarkshowthisfilewhenfolderready = null;
            model.changeBigPicture(currid, mysurf.ordnerIndex, 0, f, 0, true, false);
            continue;
          }
        }
        if (model.showrandomfilewhenfolderready) {
          int f = Gallery.rand.nextInt(folderFileCount);
          llog.d(TAG, currid + " showrandomfilewhenfolderready ok found " + mysurf.ordnerIndex+ " " + mysurf.ordnerIndexAddress + " : " + model.showrandomfilewhenfolderready + " : " + f);
          model.showrandomfilewhenfolderready = false;
          model.changeBigPicture(currid, mysurf.ordnerIndex, 0, f, 0, true, false);
          continue;
        }
        if (mysurf.mediaIndex >= folderFileCount) {
          llog.d(TAG, currid + " error mysurf.mediaIndex >= folderFileCount " + mysurf.ordnerIndex + "," + mysurf.mediaIndex + " >= " + folderFileCount + " " + mysurf.ordnerIndexAddress + " ");
          model.changeBigPicture(currid, mysurf.ordnerIndex, 0, (folderFileCount-1), 0, true, false);
          continue;
        }
        if (mysurf.mediaIndex < 0) {
          llog.d(TAG, currid + " fichier principal < 0 taille du dossier " + folderFileCount + " on corrige son index " + mysurf.ordnerIndex + "," + mysurf.mediaIndex);
          model.changeBigPicture(currid, mysurf.ordnerIndex, 0, -1, 0, true, false);
          continue;
        }
        String fichiername = mysurf.mediaIndexAddress;
        if (!mysurf.isincache(fichiername)) {
          // si le fichier n'est pas dans le cache on a sans doute bougé sans le vouloir
          if (mysurf.bigimagecurrentlydisplayed > 999999998)
            mysurf.bigimagecurrentlydisplayed = 1;
          else
            mysurf.bigimagecurrentlydisplayed += 1;
          try {
            model.commandebigimagethreadqueue.put(new String[]{"cachebitmap", String.valueOf(currid), String.valueOf(mysurf.ordnerIndex), String.valueOf(mysurf.mediaIndex), String.valueOf(mysurf.bigimagecurrentlydisplayed),
                    String.valueOf(false), "-1"});
          } catch (InterruptedException e) {
            e.printStackTrace();
          }
          //llog.d(TAG, "is not in cache continue");
          //continue;
        }
        if (mysurf.putbigpictureinmemory) {
          if (mysurf.isincache(fichiername)) {
            if (model.getMediaIsALinkThatCreatesAnOrdner(mysurf.ordnerIndex, mysurf.mediaIndex) && model.getMediaIsOnlineLevel(mysurf.ordnerIndex, mysurf.mediaIndex) == 2) {
              // on charge sans avoir à cliquer
              if (mysurf.mediaIndex + 1 == model.getFolderFileCount(mysurf.ordnerIndex)) {
                // on est sur le dernier lien de ce dossier, il faut charger la suite
                try {
                  model.commandethreaddatabase.put(new String[]{"browseonlinefolder", String.valueOf(currid), String.valueOf(mysurf.ordnerIndex), String.valueOf(mysurf.mediaIndex)});
                } catch (InterruptedException e) {
                  e.printStackTrace();
                }
              }
            }

            if (Media.online_no == model.getMediaIsOnline(mysurf.ordnerIndex, mysurf.mediaIndex)) {
              if (!model.showthisfilelockjump) {
                SharedPreferences.Editor edit = model.preferences.edit();
                edit.putString("showthisfile", fichiername);
                edit.putString("showthisfolder", mysurf.ordnerIndexAddress);
                edit.apply();
              } else {
                llog.d(TAG, "do not store " + fichiername + " keep instead " + mysurf.mediaIndexAddress);
              }
              model.showthisfilelockjump = false;
              try {
                model.commandethreaddatabase.put(new String[]{"nouveaufichierpardefaut",
                    String.valueOf(mysurf.ordnerIndex), String.valueOf(mysurf.mediaIndex)});
              } catch (InterruptedException e) {
                e.printStackTrace();
              }
            }
            int type = model.getMediaType(mysurf.ordnerIndex, mysurf.mediaIndex);
            boolean isonlinelinktonextpage = model.getMediaIsALinkThatCreatesAnOrdner(mysurf.ordnerIndex, mysurf.mediaIndex);
            float[] cachedbitmapnfo = mysurf.getcachedbitmapnfo(fichiername);
            if (fichiername.equals(mysurf.fichierprecedent)) {
              /**
               *   on est toujours sur la même image, ne pas toucher le zoom ou la position de l'image
               */
              llog.d(TAG, fichiername + " fichiername.equals(mysurf.fichierprecedent) ne pas toucher le zoom ou la position de l'image");
            } else if (model.isandroidtv && (type != Media.media_picture || isonlinelinktonextpage)) {
              /**
               *   c 'est pas une image on remet le zoomconfortratio et on centre sur l'écran
               */
              if (model.bigScreenWidth < model.bigScreenHeight)
                bscale = ((float) model.bigScreenWidth * model.zoomconfortratio) / cachedbitmapnfo[0];
              else
                bscale = ((float) model.bigScreenHeight * model.zoomconfortratio) / cachedbitmapnfo[1];
              mysurf.bscale = bscale;
              mysurf.centeronscreen = true;
            } else if (mysurf.zoommode == Gallery.ZoomFitToWindow) {
              /**
               *   on change d'image si on était en plein écran on remet en plein écran sauf si antroidTV
               */
              if (model.isandroidtv && (type != Media.media_picture || isonlinelinktonextpage)) {
                if (model.bigScreenWidth < model.bigScreenHeight)
                  bscale = ((float) model.bigScreenWidth * model.zoomconfortratio) / cachedbitmapnfo[0];
                else
                  bscale = ((float) model.bigScreenHeight * model.zoomconfortratio) / cachedbitmapnfo[1];
                mysurf.bscale = bscale;
                mysurf.centeronscreen = true;
              } else {
                if (mysurf.zoomFitMode == Gallery.ZoomFitFill)
                  fitfull = true;
                else
                  fitpicturetowindow = true;
              }
            } else if (mysurf.zoommode == Gallery.ZoomComic) {// && !isonlinelinktonextpage && type == Media.media_picture) { nope, lost in space otherwise
              /** mode BD :
               *     on replace l'image en haut à gauche à l'image suivante
               *     mais si plus petite on la centre
               */
              float newwidthwillbe = cachedbitmapnfo[0] * bscale;
              if (newwidthwillbe < mysurf.mywidth) {
                bpx = (mysurf.mywidth - newwidthwillbe) * 0.5f;
              } else {
                if (mysurf.zoomComicMode == Gallery.ZoomComicTopLeft)
                  bpx = 0.0f;
                else
                  bpx = mysurf.mywidth - newwidthwillbe;
              }
              float newheightwillbe = cachedbitmapnfo[1] * bscale;
              if (newheightwillbe < mysurf.myheight) {
                bpy = (mysurf.myheight - newheightwillbe) * 0.5f;
              } else {
                bpy = 0.0f;
              }
              mysurf.bpx = bpx;
              mysurf.bpy = bpy;
            } else if (mysurf.zoommode == Gallery.ZoomKeep) {
              /**
               *   pour notre nouvelle image on garde la même largeur mais pas le même zoom
               *   donc on redessine dans le même cadre (et si nvo dimensions de full screen suivant sera fullscreen)
               *   on descend un peu le cadre si portrait/paysage
               */
              if (mysurf.initial) {
                if (model.isandroidtv) {
                  mysurf.imageheight = (int) (model.bigScreenHeight * model.zoomconfortratio);
                  //mysurf.imageheight = (int) (mysurf.myheight * model.zoomconfortratio);
                } else {
                  if (mysurf.zoomFitMode == Gallery.ZoomFitFill)
                    fitfull = true;
                  else
                    fitpicturetowindow = true;
                }
                mysurf.initial = false;
              }
              bscale = ((float) mysurf.imageheight) / cachedbitmapnfo[1];
              if (bscale * cachedbitmapnfo[0] < 100.0f) {
                llog.d(TAG, "correct size width too small " + bscale);
                bscale = 100.0f / cachedbitmapnfo[0];
              }
              if (bscale * cachedbitmapnfo[1] < 100.0f) {
                llog.d(TAG, "correct size height too small " + bscale);
                bscale = 100.0f / cachedbitmapnfo[1];
              }
              mysurf.bscale = bscale;
              // TODO : removed screenwidth check
              if (bpx + mysurf.imagewidth < model.deltacoin || bpy + mysurf.imageheight < model.deltacoin
                      || bpx >= mysurf.mywidth - model.deltacoin || bpy >= mysurf.myheight - model.deltacoin) {
                // l 'ancienne image est en dehors de notre écran
                if (mysurf.imagewidth <= mysurf.mywidth || mysurf.imageheight <= mysurf.myheight) {
                  // l'image est plus petite que l'écran
                  mysurf.centeronscreen = true;
                } else {
                  // l'image est plus grande que l'écran
                  bpx = 0.0f;
                  bpy = 0.0f;
                  mysurf.bpx = bpx;
                  mysurf.bpy = bpy;
                }
              } else {
                // l'ancienne image est dans notre écran
                // on corrige pour que le switch portrait/paysage soit toujours centré
                float correctionverticale = (mysurf.imagewidth - cachedbitmapnfo[0] * bscale) / 2.0f;
                bpx += correctionverticale;
                mysurf.bpx = bpx;
              }
            } else {
              llog.d(TAG, "zoom not handled");
            }
            mysurf.fichierprecedent = fichiername;
            mysurf.putbigpictureinmemory = false;
          }
        }
        if (mysurf.isincache(fichiername)) {
          float[] cachedbitmapnfo = mysurf.getcachedbitmapnfo(fichiername);
          if (fitpicturetowindow) {
            //if (rotation == 270 || rotation == 90) {
            //  scalew = (((float) mysurf.ScreenHeight) / ((float) originalwidth));
            //  scaleh = (((float) mysurf.ScreenWidth) / ((float) originalheight));
            float scalew = (((float) mysurf.ScreenWidth) / ((float) cachedbitmapnfo[0]));
            float scaleh = (((float) mysurf.ScreenHeight) / ((float) cachedbitmapnfo[1]));
            if (scalew < scaleh) {
              bscale = scalew;
            } else {
              bscale = scaleh;
            }
            mysurf.bscale = bscale;
            mysurf.centeronscreen = true;
          }
          if (fitfull) {
            float scalew = (((float) mysurf.ScreenWidth) / ((float) cachedbitmapnfo[0]));
            float scaleh = (((float) mysurf.ScreenHeight) / ((float) cachedbitmapnfo[1]));
            if (mysurf.ScreenWidth > mysurf.ScreenHeight) {
              if (cachedbitmapnfo[0] > cachedbitmapnfo[1]) {
                bscale = scalew;
              } else {
                bscale = scaleh;
              }
            } else {
              if (cachedbitmapnfo[0] < cachedbitmapnfo[1]) {
                bscale = scaleh;
              } else {
                bscale = scalew;
              }
            }
            mysurf.bscale = bscale;
            mysurf.centeronscreen = true;
          }
          // seul endroit où l'on corrige model.imagewidth et model.imageheight
          mysurf.imagewidth = (int) (bscale * cachedbitmapnfo[0]);
          mysurf.imageheight = (int) (bscale * cachedbitmapnfo[1]);
          if (mysurf.centeronscreen) {
            bpx = (int) ((float) (mysurf.ScreenWidth - mysurf.imagewidth) / 2.0f);
            bpy = (int) ((float) (mysurf.ScreenHeight - mysurf.imageheight) / 2.0f);
            mysurf.bpx = bpx;
            mysurf.bpy = bpy;
            mysurf.centeronscreen = false;
          }
          mysurf.bpxmax = bpx + mysurf.imagewidth;
          mysurf.bpymax = bpy + mysurf.imageheight;
        }
      }

      /*int removed = mysurf.removefromcache();
      if (removed > 0) {
        try {
          model.commandethreadbrowser.put(new String[]{String.valueOf(currid), "update"});
        } catch (InterruptedException e) {
          e.printStackTrace();
        }
      }*/

      drawall();

    }
  }

  private boolean drawall() {

    /**
     *   fond + miniatures + image principale
     *   + options
     */

    mysurf.surfaceIsCurrentlyDrawing = true;
    if (mysurf.mysurfacestopdrawing) {
      mysurf.surfaceIsCurrentlyDrawing = false;
      return false;
    }
    if (mysurf.browserSurfaceHolder == null) {
      mysurf.surfaceIsCurrentlyDrawing = false;
      return false;
    }
    Surface surface = mysurf.browserSurfaceHolder.getSurface();
    if (surface == null) {
      mysurf.surfaceIsCurrentlyDrawing = false;
      return false;
    }
    if (!surface.isValid()) {
      mysurf.surfaceIsCurrentlyDrawing = false;
      return false;
    }
    Canvas canv;
    try {
      if (model.optionhardwareaccelerationb && Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
        canv = mysurf.browserSurfaceHolder.lockHardwareCanvas();
      } else {
        canv = mysurf.browserSurfaceHolder.lockCanvas();
      }
    } catch (Exception e) {
      llog.d(TAG, "IllegalStateException " + e.toString());
      e.printStackTrace();
      canv = null;
      return false;
    }
    if (canv == null) {
      mysurf.surfaceIsCurrentlyDrawing = false;
      return false;
    }


    canv.drawColor(Gallery.CouleurBackground);
    //canv.drawColor(Color.rgb(34, 8, 0));

    if (model.showmenuprevnextbutton && !mysurf.OptionMenuShown && mysurf.showthumbnails) {
      RectF recti;
      float m = model.strokewidth * 2.0f;
      float arrondi = mysurf.CaseInvisibleW * 0.2f;

      recti = new RectF(m, m + mysurf.CaseInvisiblePrecSuivYmin, mysurf.CaseInvisiblePrecXmax - m, mysurf.ScreenHeight - m);
      canv.drawRoundRect(recti, arrondi, arrondi, model.PaintMenuButton);
      recti = new RectF(m + mysurf.CaseInvisibleSuivXmin, m + mysurf.CaseInvisiblePrecSuivYmin, mysurf.ScreenWidth - m, mysurf.ScreenHeight - m);
      canv.drawRoundRect(recti, arrondi, arrondi, model.PaintMenuButton);

      recti = new RectF(m, m + mysurf.CaseInvisibleOptionYmax * 0.37375f, mysurf.CaseInvisibleOptionXmax - m, mysurf.CaseInvisibleOptionYmax * 0.62375f - m);
      canv.drawRoundRect(recti, arrondi, arrondi, model.PaintMenuButton);
    }

    if (0 <= mysurf.ordnerIndex && mysurf.ordnerIndex < model.folderCount) {
      int folderfilecount = model.getFolderFileCount(mysurf.ordnerIndex);
      if (0 <= mysurf.mediaIndex && mysurf.mediaIndex < folderfilecount) {
        String fichname = model.getMediaAddress(mysurf.ordnerIndex, mysurf.mediaIndex);
        if (mysurf.isincache(fichname)) {
          float[] cachedbitmapnfo = mysurf.getcachedbitmapnfo(fichname);
          boolean scaled = scalebitmaponcanvas(bpx, bpy, cachedbitmapnfo[0], cachedbitmapnfo[1], bscale, mysurf.ScreenWidth, mysurf.ScreenHeight);
          if (scaled) {
            mysurf.drawCachedBitmap(fichname, canv, mysurf.srcrect, mysurf.dstrect, model.filmstripanimatetime, model);
            if (mysurf.optionshowselection) {
              if (model.getFileIsBitmapSelected(mysurf.ordnerIndex, mysurf.mediaIndex)) {
                canv.drawRect(mysurf.dstrect.left + model.strokewidth, mysurf.dstrect.top + model.strokewidth, mysurf.dstrect.right - model.strokewidth, mysurf.dstrect.bottom - model.strokewidth, model.BitmapSelectedBgPaint);
                canv.drawRect(mysurf.dstrect.left + model.strokewidth, mysurf.dstrect.top + model.strokewidth, mysurf.dstrect.right - model.strokewidth, mysurf.dstrect.bottom - model.strokewidth, model.BitmapSelectedPaint);
              }
            }
            /*if (mysurf.cropw) {
              canv.drawRect(mysurf.dstrect.left + mysurf.mywidth * 0.15f + model.strokewidth, -10.0f,
                  mysurf.dstrect.right - mysurf.mywidth * 0.15f - model.strokewidth, mysurf.myheight + 10.0f, model.BitmapSelectedPaint);
            }
            if (mysurf.croph) {
              canv.drawRect(-10.0f, mysurf.dstrect.top + mysurf.myheight * 0.15f + model.strokewidth,
                  mysurf.mywidth + 10.0f, mysurf.dstrect.bottom - mysurf.myheight * 0.15f - model.strokewidth, model.BitmapSelectedPaint);
            }*/
          }// else if (Gallery.debugi >= 2)
           // llog.w(TAG, "drawall : not scaled " + bpx + " " + bpy + " " + cachedbitmapnfo[0] + " " + cachedbitmapnfo[1] + " " + bscale + " " + mysurf.ScreenWidth + " " + mysurf.ScreenHeight);
        } else if (Gallery.debugi >= 2)
          llog.w(TAG, "drawall : not in cache " + fichname);
      } else
        llog.d(TAG, "drawall : error ordnerIndex " + mysurf.ordnerIndex + " mediaIndex " + mysurf.mediaIndex + " / folderfilecount " + folderfilecount);
    } else
      llog.d(TAG, "drawall : ordnerIndex " + mysurf.ordnerIndex + " mediaIndex " + mysurf.mediaIndex + " " + model.folderCount);

    drawthumbsbarname(canv);

    /*if (bmpc == null || System.currentTimeMillis() - lastbmpc > 10000) {
      lastbmpc = System.currentTimeMillis();
      if (bmpc != null)
        bmpc.recycle();
      bmpc = WidgetCalendarCountdown.drawCalendar(3, new String[]{"03072024"},
          mysurf.myheight * 0.90f * 0.25f, mysurf.myheight * 0.90f);
    }
    if (bmpc != null) {
      canv.drawBitmap(bmpc, 0, 0, null);
    }

    if (bmpcd == null || System.currentTimeMillis() - lastbmpcd > 10000) {
      lastbmpcd = System.currentTimeMillis();
      if (bmpcd != null)
        bmpcd.recycle();
      bmpcd = WidgetCalendarCountdown.drawCountdown(model.unmillimetre, 3, 7, 2024, "hello",
          WidgetCalendarCountdown.gridspiral, (int) (mysurf.myheight * 0.90f * 0.25f), (int) (mysurf.myheight * 0.90f * 0.25f));
    }
    if (bmpcd != null) {
      canv.drawBitmap(bmpcd, mysurf.mywidth - 500, 0, null);
    }*/


    try {
      mysurf.browserSurfaceHolder.unlockCanvasAndPost(canv);
    } catch (Exception e) {
      llog.d(TAG, " drawall unlockCanvasAndPost " + e.toString());
      e.printStackTrace();
    }
    mysurf.surfaceIsCurrentlyDrawing = false;

    return true;
  }

  private boolean scalebitmaponcanvas(float ix0, float iy0, float iw, float ih, float scale, int screenwidth, int screenheight) {
    float ix1 = ix0 + iw * scale;
    float iy1 = iy0 + ih * scale;
    float fwidth = screenwidth;
    float fheight = screenheight;
    if (ix0 < fwidth && iy0 < fheight && ix1 >= 0 && iy1 >= 0) {
      float srcx0, srcy0, srcx1, srcy1;
      float dstx0, dsty0, dstx1, dsty1;
      if (ix0 >= 0.0f) {
        srcx0 = 0.0f;                           // dès le début
        if (ix1 <= fwidth) {
          srcx1 = iw;                                // jusqu'à la fin
          dstx1 = ix1;
        } else {
          srcx1 = (fwidth - ix0) / scale;            // pas jusqu'à la fin
          dstx1 = fwidth;
        }
        dstx0 = ix0;                                      // on placera en ix0
      } else {
        srcx0 = -ix0 / scale;                  // pas depuis le début
        if (ix1 <= fwidth) {
          srcx1 = iw;                                // jusqu'à la fin
          dstx1 = ix1;
        } else {
          srcx1 = srcx0 + fwidth / scale;            // pas juqu'à la fin
          dstx1 = fwidth;
        }
        dstx0 = 0.0f;                                      // on placera en 0
      }
      if (iy0 >= 0.0f) {
        srcy0 = 0.0f;                           // dès le début
        if (iy1 <= fheight) {
          srcy1 = ih;                                // jusqu'à la fin
          dsty1 = iy1;
        } else {
          srcy1 = (fheight - iy0) / scale;            // pas jusqu'à la fin
          dsty1 = fheight;
        }
        dsty0 = iy0;                                      // on placera en iy0
      } else {
        srcy0 = -iy0 / scale;                  // pas depuis le début
        if (iy1 <= fheight) {
          srcy1 = ih;                                // jusqu'à la fin
          dsty1 = iy1;
        } else {
          srcy1 = srcy0 + fheight / scale;            // pas juqu'à la fin
          dsty1 = fheight;
        }
        dsty0 = 0.0f;                                      // on placera en 0
      }

      mysurf.srcrect = new Rect((int) Math.floor(srcx0),
              (int) Math.floor(srcy0),
              (int) Math.floor(srcx1),
              (int) Math.floor(srcy1));
      mysurf.dstrect = new RectF(dstx0, dsty0, dstx1, dsty1);
      return true;
    }
    return false;
  }

  private volatile float[] zoompower = new float[]{0.1f, 1.0f, 10.0f, 0.001f, 0.01f};
  private volatile int zoompoweri = 0;
  private volatile float[] diaporamapower = new float[]{1.0f, 0.5f, 0.1f, 10.0f};
  private volatile int diaporamapoweri = 0;
  private List<int[]> askloadthumbnail = new ArrayList<>();

  private boolean drawthumbsbarname(Canvas surfacecanvas) {

    needanotherupdate = false;
    askloadthumbnail.clear();

    int foldercount = model.folderCount;
    int ddessusi = -9;
    int ddessusf = -9;
    int ddessousi = -9;
    int ddessousf = -9;
    int fdi = -9;
    int fdf = -9;
    int fgi = -9;
    int fgf = -9;
    int dossierprincipal = mysurf.ordnerIndex;
    int fichierprincipal = mysurf.mediaIndex;
    int cdi = 0;

    if (foldercount > 0 && dossierprincipal < 0) {
      llog.d(TAG, "error 1 drawthumbsbarname foldercount " + foldercount + " dossierprincipal " + dossierprincipal + " fichierprincipal " + fichierprincipal);
      dossierprincipal = 0;
    }
    if (fichierprincipal < 0) {
      llog.d(TAG, "error 2 drawthumbsbarname foldercount " + foldercount + " dossierprincipal " + dossierprincipal + " fichierprincipal " + fichierprincipal);
      fichierprincipal = 0;
    }

    if (!(foldercount > 0 && dossierprincipal >= 0 && fichierprincipal >= 0)) {
      llog.d(TAG, "error 3 drawthumbsbarname foldercount " + foldercount + " dossierprincipal " + dossierprincipal + " fichierprincipal " + fichierprincipal);
    } else {

      /***********************
       *
       *     THUMBNAILS
       *
       * *********************
       */

      String libere = "";
      boolean freememory = false;
      //int[] surfcurrentfolder = null;
      if (model.numberthumsincache > model.maxnumberthumsincache && model.currentlyDisplayed.size() == 0) {
        freememory = true;
        libere = "free memory " + model.numberthumsincache + " > " + model.maxnumberthumsincache;
      }

      int premierdossier = 0;
      int dernierdossier = 0;
      int premierfichier = 0;
      int dernierfichier = 0;
      int dossiercourant = 0;
      int fichiercourant = 0;
      int nbfichier = 0;
      int fichierpardefaut = 0;

      float width = mysurf.ScreenWidth;
      float height = mysurf.ScreenHeight;
      float imagewidth = mysurf.imagewidth;
      float imageheight = mysurf.imageheight;
      float thumbsize = model.thumbsize;
      if (!mysurf.showthumbnails)
        thumbsize = 0;
      float decalagew = model.GenericInterSpace;
      float decalageh = model.GenericInterSpace;
      if (mysurf.showfoldernames) {
        decalageh += model.GenericCaseH;
      }
      float thumbsizedecalagew = thumbsize + decalagew;
      float thumbsizedecalageh = thumbsize + decalageh;
      float posx, posy;

      /**
       *    en-dessous dossiers suivants
       */
      float positionaubout = bpy + imageheight;
      if (imageheight < thumbsize) { // image plus petite qu'un thumbsize
        positionaubout = bpy + thumbsize;
      }
      premierdossier = 0;
      dernierdossier = -1;
      ddessousi = dossierprincipal + (int) Math.ceil((- positionaubout) / thumbsizedecalageh);
      ddessousf = dossierprincipal + (int) Math.ceil((height - positionaubout) / thumbsizedecalageh);
      if (positionaubout < height) {
        premierdossier = ddessousi;
        dernierdossier = ddessousf;
        if (premierdossier <= dossierprincipal) {
          premierdossier = dossierprincipal + 1;
        }
        if (dernierdossier >= foldercount) {
          dernierdossier = foldercount - 1;
        }
      }

      for (dossiercourant = premierdossier; dossiercourant <= dernierdossier; dossiercourant++) {
        posy = positionaubout + decalageh + (dossiercourant - dossierprincipal - 1) * thumbsizedecalageh;

        if (mysurf.showfoldernames) {
          float ecrity1 = posy - model.GenericCaseH - model.GenericInterSpace / 2.0f;
          float ecrity2 = ecrity1 + model.GenericCaseH;
          float ecrity = ecrity1 + (model.GenericCaseH + model.GenericTextH) * 0.5f;
          String[] tosplit = model.getOrdnerSplitName(dossiercourant);
          if (tosplit != null) {
            float ecritxx = bpx + decalagew;
            int[] p = new int[]{0, 0, 0};
            int[] paddress = new int[]{0, 0, 0};
            while (p[0] != -1) {
              p = strpos(tosplit[0], p[2]);
              if (p[0] == -1)
                break;
              if (paddress[0] != -1)
                paddress = strpos(tosplit[1], paddress[2]);
              String split = tosplit[0].substring(p[0], p[1]);
              float taillex = model.FolderNamePaint.measureText(split);
              float ecritx1 = ecritxx;
              float ecritx2 = ecritxx + taillex + decalagew * 2.0f;
              if (clicktohandle) {
                if (ecritx1 <= clicktohandlex && clicktohandlex <= ecritx2 && ecrity1 <= clicktohandley && clicktohandley <= ecrity2) {
                  if (0 < paddress[2] && paddress[2] < tosplit[1].length())
                    mysurf.basisfolderclicked = tosplit[1].substring(0, paddress[2]);
                  else
                    mysurf.basisfolderclicked = tosplit[1];
                  llog.d(TAG, "clicked " + mysurf.basisfolderclicked);
                  if (mysurf.optionshowselectionactive) {
                    try {
                      model.commandethreaddatabase.put(new String[]{"selectfolder", mysurf.basisfolderclicked, String.valueOf(dossiercourant), String.valueOf(selectionplus)});
                    } catch (InterruptedException e) {
                      e.printStackTrace();
                    }
                  } else if (!(mysurf.optionshowrescan || mysurf.optionshowselectionactive || mysurf.optionshowhidden)) {
                    int foundparentfolder = model.findFolderStartsWith(mysurf.basisfolderclicked);
                    mysurf.fichierprecedent = "dummy";
                    if (foundparentfolder != -1) // si -1 alors on reste sur le même
                      model.changeBigPicture(currid, foundparentfolder, 0, -1, 0, true, false);
                    clicktohandle = false;
                    needanotherupdate = true;
                  }
                }
              }
              RectF rect = new RectF(ecritx1, ecrity1, ecritx2, ecrity2);
              surfacecanvas.drawRoundRect(rect, decalagew * 2.0f, decalagew * 2.0f, model.FolderNameBgPaint);
              surfacecanvas.drawText(split, ecritxx + decalagew, ecrity, model.FolderNamePaint);
              ecritxx += taillex + decalagew * 3.0f;
            }
          }
        }

        nbfichier = model.getFolderFileCount(dossiercourant);
        if (model.getFolderNotLoadedYet(dossiercourant)) {
          /**
           *  ce répertoire n'a pas encore été chargé
           *  on regarde le suivant
           */
          //llog.d(TAG, "rechargetoutledossier sous2 " + dossiercourant);
          try {
            model.commandethreaddatabase.put(new String[]{"rechargetoutledossier", String.valueOf(currid), String.valueOf(dossiercourant), String.valueOf(false)});
          } catch (InterruptedException e) {
            e.printStackTrace();
          }
          continue;
        } else if (nbfichier == 1 && model.getFolderIsOnline(dossiercourant) != Media.online_no && model.getMediaIsOnlineLevel(dossiercourant, 0) == 2) {
          //llog.d(TAG, "rechargetoutledossier sous3 " + dossiercourant);
          try {
            model.commandethreaddatabase.put(new String[]{"rechargetoutledossier", String.valueOf(currid), String.valueOf(dossiercourant), String.valueOf(false)});
          } catch (InterruptedException e) {
            e.printStackTrace();
          }
          continue;
        } else if (nbfichier <= 0) {
          //llog.d(TAG, "rechargetoutledossier sous4 " + dossiercourant + " nbfichier " + nbfichier);
          continue;
        }
        fichierpardefaut = model.getFolderDefaultFile(dossiercourant);
        if (fichierpardefaut >= nbfichier) {
          fichierpardefaut = 0;
          model.setFolderDefaultFile(dossiercourant, 0);
        }

        if (mysurf.showthumbnails) {

          float posduzero = bpx - thumbsizedecalagew * (float) fichierpardefaut;
          premierfichier = (int) Math.ceil((decalagew - posduzero) / thumbsizedecalagew) - 1;
          if (premierfichier < 0) {
            premierfichier = 0;
          }
          posx = posduzero + thumbsizedecalagew * (float) premierfichier;
          dernierfichier = premierfichier - 1 + (int) Math.ceil((width - posx) / thumbsizedecalagew);
          if (dernierfichier >= nbfichier) {
            dernierfichier = nbfichier - 1;
          }

          for (fichiercourant = premierfichier; fichiercourant <= dernierfichier; fichiercourant++) {
            if (model.getFileBitmapInMemory(dossiercourant, fichiercourant)) {
              float[] position = model.getMediaDrawingPosition(dossiercourant, fichiercourant);
              float posx1 = posx + position[0];
              float posy1 = posy + position[1];
              if (clicktohandle) {
                float posx2 = posx1 + position[2];
                float posy2 = posy1 + position[3];
                if (posx1 <= clicktohandlex && clicktohandlex <= posx2 && posy1 <= clicktohandley && clicktohandley <= posy2) {
                  float pdx = (posx2 - posx1) * Gallery.percentofpicturecenter;
                  if (mysurf.optionshowselectionactive && posx1 + pdx <= clicktohandlex && clicktohandlex <= posx2 - pdx) {
                    if (selectiona) {
                      selectionadoss = dossiercourant;
                      selectionafich = fichiercourant;
                    } else if (selectionz) {
                      selectionzdoss = dossiercourant;
                      selectionzfich = fichiercourant;
                    } else if (selectionplus || selectionmoins) {
                      model.getFileSwitchSelected(dossiercourant, fichiercourant);
                    }
                    if (selectionadoss >= 0 && selectionafich >= 0 && selectionzdoss >= 0 && selectionzfich >= 0) {
                      for (int d = selectionadoss; d <= selectionzdoss; d++) {
                        int debut = 0;
                        int count = model.getFolderFileCount(d);
                        int fin = count - 1;
                        if (d == selectionadoss) {
                          debut = selectionafich;
                        }
                        if (d == selectionzdoss) {
                          fin = selectionzfich;
                        }
                        /*if (model.currCollectionAddress != null && !selectfilesonlynofolder && ((debut == 0 && fin == count - 1) || count == 0)) {
                          model.getFolderSetSelected(d, selectionplus);
                        } else {*/
                          for (int f = debut; f <= fin; f++) {
                            model.getFileSetSelected(d, f, selectionplus);
                          }
                        //}
                      }
                    }
                    clicktohandle = false;
                    needanotherupdate = true;
                  } else {
                    model.changeBigPicture(currid, dossiercourant, 0, fichiercourant, 0, true, false);
                    clicktohandle = false;
                    needanotherupdate = true;
                  }
                }
              }
              //myViewModel.drawTempBitmap(surfacecanvas, posx, posy);
              if (posx1 < mysurf.mywidth
                      && posy1 < mysurf.myheight
                      && posx1 + thumbsize > 0
                      && posy1 + thumbsize > 0
              )
                model.drawFileBitmap(dossiercourant, fichiercourant, surfacecanvas, posx1, posy1, currid, mysurf.thumbcurrentlydisplayed);
              if (freememory) {
                if (cdi * 3 == model.currentlyDisplayed.size()) { // nouveau dossier affiché
                  model.currentlyDisplayed.add(dossiercourant);
                  model.currentlyDisplayed.add(fichiercourant);
                  model.currentlyDisplayed.add(fichiercourant);
                } else {                      // on écrase le dernier fichier
                  model.currentlyDisplayed.set(cdi * 3 + 2, fichiercourant);
                }
              }
              if (mysurf.optionshowselection)
                if (model.getFileIsBitmapSelected(dossiercourant, fichiercourant)) {
                  surfacecanvas.drawRect(posx + model.unmillimetre, posy + model.unmillimetre, posx + thumbsize - model.unmillimetre, posy + thumbsize - model.unmillimetre, model.BitmapSelectedBgPaint);
                  surfacecanvas.drawRect(posx + model.unmillimetre, posy + model.unmillimetre, posx + thumbsize - model.unmillimetre, posy + thumbsize - model.unmillimetre, model.BitmapSelectedPaint);
                }
            } else {
              askloadthumbnail.add(new int[]{1, dossiercourant, fichiercourant});
              model.drawTempBitmap(surfacecanvas, posx, posy);
            }
            posx += thumbsizedecalagew;
          }
        }
        if (freememory) {
          if (cdi * 3 < model.currentlyDisplayed.size()) { // nouveau dossier affiché
            cdi++;
          }
        }
      }

      /**
       *    au-dessus dossiers précédents
       */
      premierdossier = 0;
      dernierdossier = -1;
      ddessusi = dossierprincipal - (int) Math.ceil((bpy - decalageh) / thumbsizedecalageh);
      ddessusf = dossierprincipal - (int) Math.ceil((bpy - decalageh - height) / thumbsizedecalageh);
      if (bpy - decalageh > 0) {
        premierdossier = ddessusi;
        dernierdossier = ddessusf;
        if (dernierdossier >= dossierprincipal) {
          dernierdossier = dossierprincipal - 1;
        }
        if (premierdossier < 0) {
          premierdossier = 0;
        }
      }

      for (dossiercourant = premierdossier; dossiercourant <= dernierdossier; dossiercourant++) {
        posy = bpy - (dossierprincipal - dossiercourant) * thumbsizedecalageh;

        if (mysurf.showfoldernames) {
          float ecrity1 = posy - model.GenericCaseH - model.GenericInterSpace / 2.0f;
          float ecrity2 = ecrity1 + model.GenericCaseH;
          float ecrity = ecrity1 + (model.GenericCaseH + model.GenericTextH) * 0.5f;
          String[] tosplit = model.getOrdnerSplitName(dossiercourant);
          if (tosplit != null) {
            float ecritxx = bpx + decalagew;
            int[] p = new int[]{0, 0, 0};
            int[] paddress = new int[]{0, 0, 0};
            while (p[0] != -1) {
              p = strpos(tosplit[0], p[2]);
              if (p[0] == -1)
                break;
              if (paddress[0] != -1)
                paddress = strpos(tosplit[1], paddress[2]);
              String split = tosplit[0].substring(p[0], p[1]);
              float taillex = model.FolderNamePaint.measureText(split);
              float ecritx1 = ecritxx;
              float ecritx2 = ecritxx + taillex + decalagew * 2.0f;
              if (clicktohandle) {
                if (ecritx1 <= clicktohandlex && clicktohandlex <= ecritx2 && ecrity1 <= clicktohandley && clicktohandley <= ecrity2) {
                  if (0 < paddress[2] && paddress[2] < tosplit[1].length())
                    mysurf.basisfolderclicked = tosplit[1].substring(0, paddress[2]);
                  else
                    mysurf.basisfolderclicked = tosplit[1];
                  llog.d(TAG, "clicked " + mysurf.basisfolderclicked);
                  if (mysurf.optionshowselectionactive) {
                    try {
                      model.commandethreaddatabase.put(new String[]{"selectfolder", mysurf.basisfolderclicked, String.valueOf(dossiercourant), String.valueOf(selectionplus)});
                    } catch (InterruptedException e) {
                      e.printStackTrace();
                    }
                  } else if (!(mysurf.optionshowrescan || mysurf.optionshowselectionactive || mysurf.optionshowhidden)) {
                    int foundparentfolder = model.findFolderStartsWith(mysurf.basisfolderclicked);
                    mysurf.fichierprecedent = "dummy";
                    if (foundparentfolder != -1)
                      model.changeBigPicture(currid, foundparentfolder, 0, -1, 0, true, false);
                    clicktohandle = false;
                    needanotherupdate = true;
                  }
                }
              }
              RectF rect = new RectF(ecritx1, ecrity1, ecritx2, ecrity2);
              surfacecanvas.drawRoundRect(rect, decalagew * 2.0f, decalagew * 2.0f, model.FolderNameBgPaint);
              surfacecanvas.drawText(split, ecritxx + decalagew, ecrity, model.FolderNamePaint);
              ecritxx += taillex + decalagew * 3.0f;
            }
          }
        }

        nbfichier = model.getFolderFileCount(dossiercourant);
        if (model.getFolderNotLoadedYet(dossiercourant)) {
          /**
           *  ce répertoire n'a pas encore été chargé
           *  on regarde le suivant
           */
          //llog.d(TAG, "rechargetoutledossier sur2 " + dossiercourant);
          try {
            model.commandethreaddatabase.put(new String[]{"rechargetoutledossier", String.valueOf(currid), String.valueOf(dossiercourant), String.valueOf(false)});
          } catch (InterruptedException e) {
            e.printStackTrace();
          }
          continue;
        } else if (nbfichier == 1 && model.getFolderIsOnline(dossiercourant) != Media.online_no && model.getMediaIsOnlineLevel(dossiercourant, 0) == 2) {
          //llog.d(TAG, "rechargetoutledossier sur3 " + dossiercourant);
          try {
            model.commandethreaddatabase.put(new String[]{"rechargetoutledossier", String.valueOf(currid), String.valueOf(dossiercourant), String.valueOf(false)});
          } catch (InterruptedException e) {
            e.printStackTrace();
          }
          continue;
        } else if (nbfichier <= 0) {
          //llog.d(TAG, "rechargetoutledossier sur4 " + dossiercourant + " nbfichier " + nbfichier);
          continue;
        }
        fichierpardefaut = model.getFolderDefaultFile(dossiercourant);
        if (fichierpardefaut >= nbfichier) {
          fichierpardefaut = 0;
          model.setFolderDefaultFile(dossiercourant, 0);
        }

        if (mysurf.showthumbnails) {



          float posduzero = bpx - thumbsizedecalagew * (float) fichierpardefaut;
          premierfichier = (int) Math.ceil((decalagew - posduzero) / thumbsizedecalagew) - 1;
          if (premierfichier < 0) {
            premierfichier = 0;
          }
          posx = posduzero + thumbsizedecalagew * (float) premierfichier;
          dernierfichier = premierfichier - 1 + (int) Math.ceil((width - posx) / thumbsizedecalagew);
          if (dernierfichier >= nbfichier) {
            dernierfichier = nbfichier - 1;
          }

          for (fichiercourant = premierfichier; fichiercourant <= dernierfichier; fichiercourant++) {
            if (model.getFileBitmapInMemory(dossiercourant, fichiercourant)) {
              float[] position = model.getMediaDrawingPosition(dossiercourant, fichiercourant);
              float posx1 = posx + position[0];
              float posy1 = posy + position[1];
              if (clicktohandle) {
                float posx2 = posx1 + position[2];
                float posy2 = posy1 + position[3];
                if (posx1 <= clicktohandlex && clicktohandlex <= posx2 && posy1 <= clicktohandley && clicktohandley <= posy2) {
                  float pdx = (posx2 - posx1) * Gallery.percentofpicturecenter;
                  if (mysurf.optionshowselectionactive && posx1 + pdx <= clicktohandlex && clicktohandlex <= posx2 - pdx) {
                    if (selectiona) {
                      selectionadoss = dossiercourant;
                      selectionafich = fichiercourant;
                    } else if (selectionz) {
                      selectionzdoss = dossiercourant;
                      selectionzfich = fichiercourant;
                    } else if (selectionplus || selectionmoins) {
                      model.getFileSwitchSelected(dossiercourant, fichiercourant);
                    }
                    if (selectionadoss >= 0 && selectionafich >= 0 && selectionzdoss >= 0 && selectionzfich >= 0) {
                      for (int d = selectionadoss; d <= selectionzdoss; d++) {
                        int debut = 0;
                        int count = model.getFolderFileCount(d);
                        int fin = count - 1;
                        if (d == selectionadoss) {
                          debut = selectionafich;
                        }
                        if (d == selectionzdoss) {
                          fin = selectionzfich;
                        }
                        /*if (model.currCollectionAddress != null && !selectfilesonlynofolder && ((debut == 0 && fin == count - 1) || count == 0)) {
                          model.getFolderSetSelected(d, selectionplus);
                        } else {*/
                          for (int f = debut; f <= fin; f++) {
                            model.getFileSetSelected(d, f, selectionplus);
                          }
                        //}
                      }
                    }
                    clicktohandle = false;
                    needanotherupdate = true;
                  } else {
                    model.changeBigPicture(currid, dossiercourant, 0, fichiercourant, 0, true, false);
                    clicktohandle = false;
                    needanotherupdate = true;
                  }
                }
              }
              if (posx1 < mysurf.mywidth
                      && posy1 < mysurf.myheight
                      && posx1 + thumbsize > 0
                      && posy1 + thumbsize > 0
              )
                model.drawFileBitmap(dossiercourant, fichiercourant, surfacecanvas, posx1, posy1, currid, mysurf.thumbcurrentlydisplayed);
              if (freememory) {
                if (cdi * 3 == model.currentlyDisplayed.size()) { // nouveau dossier affiché
                  model.currentlyDisplayed.add(dossiercourant);
                  model.currentlyDisplayed.add(fichiercourant);
                  model.currentlyDisplayed.add(fichiercourant);
                } else {                      // on écrase le dernier fichier
                  model.currentlyDisplayed.set(cdi * 3 + 2, fichiercourant);
                }
              }
              if (mysurf.optionshowselection)
                if (model.getFileIsBitmapSelected(dossiercourant, fichiercourant)) {
                  surfacecanvas.drawRect(posx + model.unmillimetre, posy + model.unmillimetre, posx + thumbsize - model.unmillimetre, posy + thumbsize - model.unmillimetre, model.BitmapSelectedBgPaint);
                  surfacecanvas.drawRect(posx + model.unmillimetre, posy + model.unmillimetre, posx + thumbsize - model.unmillimetre, posy + thumbsize - model.unmillimetre, model.BitmapSelectedPaint);
                }
            } else {
              askloadthumbnail.add(new int[]{-1, dossiercourant, fichiercourant});
              model.drawTempBitmap(surfacecanvas, posx, posy);
            }
            posx += thumbsizedecalagew;
          }
        }
        if (freememory) {
          if (cdi < 3) {
            if (cdi * 3 < model.currentlyDisplayed.size()) { // nouveau dossier affiché
              cdi++;
            }
          }
        }
      }

      nbfichier = model.getFolderFileCount(dossierprincipal);
      dossiercourant = dossierprincipal;
      fichiercourant = fichierprincipal;
      fichierpardefaut = fichierprincipal;

      if (nbfichier <= 0) {
        llog.d(TAG, "rechargetoutledossier prcipl1 " + dossiercourant + " nbfichier " + nbfichier);
      }

      /*if (!(ddessusf >= dossierprincipal && ddessousi <= dossierprincipal)) {
        llog.d(TAG, "not shown " + ddessusf + " < " + dossierprincipal + " < " + ddessousi);
      }*/

      if (model.getFolderNotLoadedYet(dossiercourant)) {
        /**
         *  ce répertoire n'a pas encore été chargé
         *  on l'ignore pour l'instant
         */
        llog.d(TAG, "rechargetoutledossier prcipl1 " + dossiercourant);
        try {
          model.commandethreaddatabase.put(new String[]{"rechargetoutledossier", String.valueOf(currid), String.valueOf(dossiercourant), String.valueOf(false)});
        } catch (InterruptedException e) {
          e.printStackTrace();
        }
          /*
        } else if (nbfichier == 2 && model.mtouslesdossiers.get(dossiercourant).isonline && model.mtouslesdossiers.get(dossiercourant).isonlinelevel == 2) {
          llog.d(TAG, "rechargetoutledossier prcipl2 " + dossiercourant);
          try {
            model.commandethreaddatabase.put(new String[]{"rechargetoutledossier", String.valueOf(currid), String.valueOf(dossiercourant), String.valueOf(false)});
          } catch (InterruptedException e) {
            e.printStackTrace();
          }
          */
      } else if (ddessusf >= dossierprincipal && ddessousi <= dossierprincipal) {

        if (mysurf.showfoldernames) {
          float ecrity1 = bpy - model.GenericCaseH - model.GenericInterSpace / 2.0f;
          float ecrity2 = ecrity1 + model.GenericCaseH;
          float ecrity = ecrity1 + (model.GenericCaseH + model.GenericTextH) * 0.5f;
          String[] tosplit = model.getOrdnerSplitName(dossiercourant);
          if (tosplit != null) {
            float ecritxx = bpx + decalagew;
            float ecritx1 = 0.0f;
            float ecritx2 = 0.0f;
            int[] p = new int[]{0, 0, 0};
            int[] paddress = new int[]{0, 0, 0};
            while (p[0] != -1) {
              p = strpos(tosplit[0], p[2]);
              if (p[0] == -1)
                break;
              if (paddress[0] != -1)
                paddress = strpos(tosplit[1], paddress[2]);
              String split = tosplit[0].substring(p[0], p[1]);
              float taillex = model.FolderNamePaint.measureText(split);
              ecritx1 = ecritxx;
              ecritx2 = ecritxx + taillex + decalagew * 2.0f;
              if (clicktohandle) {
                if (ecritx1 <= clicktohandlex && clicktohandlex <= ecritx2 && ecrity1 <= clicktohandley && clicktohandley <= ecrity2) {
                  if (0 < paddress[2] && paddress[2] < tosplit[1].length())
                    mysurf.basisfolderclicked = tosplit[1].substring(0, paddress[2]);
                  else
                    mysurf.basisfolderclicked = tosplit[1];
                  llog.d(TAG, "clicked " + mysurf.basisfolderclicked);
                  if (mysurf.optionshowselectionactive) {
                    try {
                      model.commandethreaddatabase.put(new String[]{"selectfolder", mysurf.basisfolderclicked, String.valueOf(dossierprincipal), String.valueOf(selectionplus)});
                    } catch (InterruptedException e) {
                      e.printStackTrace();
                    }
                  } else if (!(mysurf.optionshowrescan || mysurf.optionshowselectionactive || mysurf.optionshowhidden)) {
                    int foundparentfolder = model.findFolderStartsWith(mysurf.basisfolderclicked);
                    mysurf.fichierprecedent = "dummy";
                    if (foundparentfolder != -1)
                      model.changeBigPicture(currid, foundparentfolder, 0, -1, 0, true, false);
                    clicktohandle = false;
                    needanotherupdate = true;
                  }
                }
              }
              RectF rect = new RectF(ecritx1, ecrity1, ecritx2, ecrity2);
              surfacecanvas.drawRoundRect(rect, decalagew * 2.0f, decalagew * 2.0f, model.FolderNameBgPaint);
              surfacecanvas.drawText(split, ecritxx + decalagew, ecrity, model.FolderNamePaint);
              ecritxx += taillex + decalagew * 3.0f;
            }
            if (dossierprincipal >= 0 && fichierprincipal >= 0 && foldercount > 0) {
              int dossierl = model.getFolderFileCount(dossierprincipal);
              if (fichierprincipal < dossierl && dossierl > 0) {
                String cetexte;
                float taillex;
                RectF rect;

                ecritxx += decalagew * 4.5f;

                cetexte = model.getMediaSplitName(dossierprincipal, fichierprincipal);

                taillex = model.FolderNamePaint.measureText(cetexte);
                surfacecanvas.drawText(cetexte, ecritxx + decalagew, ecrity, model.FolderNamePaint);

                int numerofichier = fichierprincipal + 1;
                cetexte = numerofichier + "/" + dossierl;
                taillex = model.FolderNamePaint.measureText(cetexte);
                ecritxx = bpx - decalagew * 4.5f - taillex;
                surfacecanvas.drawText(cetexte, ecritxx, ecrity, model.FolderNamePaint);

              }
            }
          }
        }

        float top, bottom;
        float maxthumbheight = imageheight;
        if (maxthumbheight < thumbsizedecalagew) {
          maxthumbheight = thumbsizedecalagew;
          top = bpy;
          bottom = bpy + thumbsizedecalagew;
        }
        if (bpy < 0 && bpy + imageheight < height) {
          maxthumbheight = bpy + imageheight;
          top = 0.0f;
          bottom = bpy + imageheight;
        } else if (bpy < 0 && bpy + imageheight >= height) {
          maxthumbheight = height;
          top = 0.0f;
          bottom = height;
        } else if (bpy >= 0 && bpy + imageheight >= height) {
          maxthumbheight = height - bpy;
          top = bpy;
          bottom = height;
        } else {
          top = bpy;
          bottom = bpy + imageheight;
        }
        int thumbpercol = (int) Math.floor(maxthumbheight / thumbsize);
        if (thumbpercol < 1) {
          thumbpercol = 1;
        }

        /**
         *    à droite
         */
        float posduzero = bpx + imagewidth + decalagew;
        dernierfichier = fichierpardefaut + (int) Math.ceil((width - posduzero) / thumbsizedecalagew);
        premierfichier = fichierpardefaut + (int) Math.ceil((decalagew - posduzero) / thumbsizedecalagew);
        if (premierfichier <= fichierpardefaut + 1) {
          posx = posduzero;
        } else {
          posx = posduzero + thumbsizedecalagew * (float) (premierfichier - (fichierpardefaut + 1));
        }

        premierfichier = (fichierpardefaut + 1) + (premierfichier - (fichierpardefaut + 1)) * thumbpercol;
        if (premierfichier <= fichierpardefaut + 1) {
          premierfichier = fichierpardefaut + 1;
        }
        // premier en haut de la colonne suivante - 1 pour en bas de la colonne
        dernierfichier = (fichierpardefaut + 1) + ((dernierfichier + 1) - (fichierpardefaut + 1)) * thumbpercol - 1;
        if (dernierfichier >= nbfichier) {
          dernierfichier = nbfichier - 1;
        }

        fdi = premierfichier;
        fdf = dernierfichier;


        if (mysurf.showthumbnails) {
          if (bpx + imagewidth < width) {
            float yreference = top;
            if (bottom - thumbsize < 0.0f) {
              yreference = bottom - thumbsize;
            }
            posy = yreference;
            int nbthumbinthiscol = 0;
            for (fichiercourant = premierfichier; fichiercourant <= dernierfichier; fichiercourant++) {
              if (model.getFileBitmapInMemory(dossierprincipal, fichiercourant)) {
                //Paint paint = new Paint();
                //paint.setColor(Color.GREEN);
                //surfacecanvas.drawRect(posx, posy, posx+thumbsize, posy+thumbsize, paint);
                float[] position = model.getMediaDrawingPosition(dossierprincipal, fichiercourant);
                float posx1 = posx + position[0];
                float posy1 = posy + position[1];
                if (clicktohandle) {
                  float posx2 = posx1 + position[2];
                  float posy2 = posy1 + position[3];
                  if (posx1 <= clicktohandlex && clicktohandlex <= posx2 && posy1 <= clicktohandley && clicktohandley <= posy2) {
                    float pdx = (posx2 - posx1) * Gallery.percentofpicturecenter;
                    if (mysurf.optionshowselectionactive && posx1 + pdx <= clicktohandlex && clicktohandlex <= posx2 - pdx) {
                      if (selectiona) {
                        selectionadoss = dossierprincipal;
                        selectionafich = fichiercourant;
                      } else if (selectionz) {
                        selectionzdoss = dossierprincipal;
                        selectionzfich = fichiercourant;
                      } else if (selectionplus || selectionmoins) {
                        model.getFileSwitchSelected(dossierprincipal, fichiercourant);
                      }
                      if (selectionadoss >= 0 && selectionafich >= 0 && selectionzdoss >= 0 && selectionzfich >= 0) {
                        for (int d = selectionadoss; d <= selectionzdoss; d++) {
                          int debut = 0;
                          int count = model.getFolderFileCount(d);
                          int fin = count - 1;
                          if (d == selectionadoss) {
                            debut = selectionafich;
                          }
                          if (d == selectionzdoss) {
                            fin = selectionzfich;
                          }
                          /*if (model.currCollectionAddress != null && !selectfilesonlynofolder && ((debut == 0 && fin == count - 1) || count == 0)) {
                            model.getFolderSetSelected(d, selectionplus);
                          } else {*/
                            for (int f = debut; f <= fin; f++) {
                              model.getFileSetSelected(d, f, selectionplus);
                            }
                          //}
                        }
                      }
                      clicktohandle = false;
                      needanotherupdate = true;
                    } else {
                      model.changeBigPicture(currid, -1, 0, fichiercourant, 0, true, false);
                      clicktohandle = false;
                      needanotherupdate = true;
                    }
                  }
                }
                if (posx1 < mysurf.mywidth
                        && posy1 < mysurf.myheight
                        && posx1 + thumbsize > 0
                        && posy1 + thumbsize > 0
                )
                  model.drawFileBitmap(dossierprincipal, fichiercourant, surfacecanvas, posx1, posy1, currid, mysurf.thumbcurrentlydisplayed);
                if (freememory) {
                  if (cdi * 3 == model.currentlyDisplayed.size()) { // nouveau dossier affiché
                    model.currentlyDisplayed.add(dossierprincipal);
                    model.currentlyDisplayed.add(fichiercourant);
                    model.currentlyDisplayed.add(fichiercourant);
                  } else {                      // on écrase le dernier fichier
                    model.currentlyDisplayed.set(cdi * 3 + 2, fichiercourant);
                  }
                }
                if (mysurf.optionshowselection)
                  if (model.getFileIsBitmapSelected(dossierprincipal, fichiercourant)) {
                    surfacecanvas.drawRect(posx + model.unmillimetre, posy + model.unmillimetre, posx + thumbsize - model.unmillimetre, posy + thumbsize - model.unmillimetre, model.BitmapSelectedBgPaint);
                    surfacecanvas.drawRect(posx + model.unmillimetre, posy + model.unmillimetre, posx + thumbsize - model.unmillimetre, posy + thumbsize - model.unmillimetre, model.BitmapSelectedPaint);
                  }
              } else {
                askloadthumbnail.add(new int[]{0, dossierprincipal, fichiercourant});
                model.drawTempBitmap(surfacecanvas, posx, posy);
              }
              posy += thumbsizedecalagew;
              nbthumbinthiscol += 1;
              if (nbthumbinthiscol >= thumbpercol) {
                posy = yreference;
                posx += thumbsizedecalagew;
                nbthumbinthiscol = 0;
              }
            }
          }
        }
        if (freememory) {
          if (cdi * 3 < model.currentlyDisplayed.size()) { // nouveau dossier affiché
            cdi++;
          }
        }

        /**
         *    à gauche
         */

        int nombredecolonnes = (int) Math.ceil(((float) fichierpardefaut) / ((float) thumbpercol));
        posduzero = bpx - thumbsizedecalagew * (float) nombredecolonnes;
        premierfichier = (int) Math.ceil((decalagew - posduzero) / thumbsizedecalagew) - 1;
        if (premierfichier <= 0) {
          posx = posduzero;
        } else {
          posx = posduzero + thumbsizedecalagew * (float) premierfichier;
        }
        if (bpx < width) {
          dernierfichier = (int) Math.ceil((bpx - posduzero) / thumbsizedecalagew) - 1;
        } else {
          dernierfichier = (int) Math.ceil((width - posduzero) / thumbsizedecalagew) - 1;
        }

        premierfichier = premierfichier * thumbpercol;
        if (premierfichier < 0) {
          premierfichier = 0;
        }
        dernierfichier = (dernierfichier + 1) * thumbpercol - 1;
        if (dernierfichier >= fichierpardefaut) {
          dernierfichier = fichierpardefaut - 1;
        }

        // il faut décaler pour la première colonne
        int restedivision = fichierpardefaut % thumbpercol;
        int nbdedecalage = fichierpardefaut % thumbpercol;
        if (restedivision != 0) {
          nbdedecalage = thumbpercol - restedivision;
        }
        premierfichier -= nbdedecalage;

        float yreference = top;
        if (bottom - thumbsize < 0.0f) {
          yreference = bottom - thumbsize;
        }
        posy = yreference;
        int nbthumbinthiscol = 0;
        if (premierfichier < 0) {
          nbthumbinthiscol = nbdedecalage;
          premierfichier = 0;
          posy += nbdedecalage * thumbsizedecalagew;
        }

        fgi = premierfichier;
        fgf = dernierfichier;

        if (mysurf.showthumbnails) {
          if (bpx > 0 && premierfichier <= dernierfichier) {

            for (fichiercourant = premierfichier; fichiercourant <= dernierfichier; fichiercourant++) {
              if (model.getFileBitmapInMemory(dossierprincipal, fichiercourant)) {
                float[] position = model.getMediaDrawingPosition(dossierprincipal, fichiercourant);
                float posx1 = posx + position[0];
                float posy1 = posy + position[1];
                if (clicktohandle) {
                  float posx2 = posx1 + position[2];
                  float posy2 = posy1 + position[3];
                  if (posx1 <= clicktohandlex && clicktohandlex <= posx2 && posy1 <= clicktohandley && clicktohandley <= posy2) {
                    float pdx = (posx2 - posx1) * Gallery.percentofpicturecenter;
                    if (mysurf.optionshowselectionactive && posx1 + pdx <= clicktohandlex && clicktohandlex <= posx2 - pdx) {
                      if (selectiona) {
                        selectionadoss = dossierprincipal;
                        selectionafich = fichiercourant;
                      } else if (selectionz) {
                        selectionzdoss = dossierprincipal;
                        selectionzfich = fichiercourant;
                      } else if (selectionplus || selectionmoins) {
                        model.getFileSwitchSelected(dossierprincipal, fichiercourant);
                      }
                      if (selectionadoss >= 0 && selectionafich >= 0 && selectionzdoss >= 0 && selectionzfich >= 0) {
                        for (int d = selectionadoss; d <= selectionzdoss; d++) {
                          int debut = 0;
                          int count = model.getFolderFileCount(d);
                          int fin = count - 1;
                          if (d == selectionadoss) {
                            debut = selectionafich;
                          }
                          if (d == selectionzdoss) {
                            fin = selectionzfich;
                          }
                          /*if (model.currCollectionAddress != null && !selectfilesonlynofolder && ((debut == 0 && fin == count - 1) || count == 0)) {
                            model.getFolderSetSelected(d, selectionplus);
                          } else {*/
                            for (int f = debut; f <= fin; f++) {
                              model.getFileSetSelected(d, f, selectionplus);
                            }
                          //}
                        }
                      }
                      clicktohandle = false;
                      needanotherupdate = true;
                    } else {
                      model.changeBigPicture(currid, -1, 0, fichiercourant, 0, true, false);
                      clicktohandle = false;
                      needanotherupdate = true;
                    }
                  }
                }
                if (posx1 < mysurf.mywidth
                        && posy1 < mysurf.myheight
                        && posx1 + thumbsize > 0
                        && posy1 + thumbsize > 0
                )
                  model.drawFileBitmap(dossierprincipal, fichiercourant, surfacecanvas, posx1, posy1, currid, mysurf.thumbcurrentlydisplayed);
                if (freememory) {
                  if (cdi * 3 == model.currentlyDisplayed.size()) { // nouveau dossier affiché
                    model.currentlyDisplayed.add(dossierprincipal);
                    model.currentlyDisplayed.add(-1);
                    model.currentlyDisplayed.add(-1);
                  }
                  if (model.currentlyDisplayed.get(cdi * 3 + 1) == -1) { // pas encore de premier fichier
                    model.currentlyDisplayed.set(cdi * 3 + 1, fichiercourant);
                    model.currentlyDisplayed.set(cdi * 3 + 2, fichiercourant);
                  } else {                      // on écrase le dernier fichier
                    model.currentlyDisplayed.set(cdi * 3 + 2, fichiercourant);
                  }
                }
                if (mysurf.optionshowselection)
                  if (model.getFileIsBitmapSelected(dossierprincipal, fichiercourant)) {
                    surfacecanvas.drawRect(posx + model.unmillimetre, posy + model.unmillimetre, posx + thumbsize - model.unmillimetre, posy + thumbsize - model.unmillimetre, model.BitmapSelectedBgPaint);
                    surfacecanvas.drawRect(posx + model.unmillimetre, posy + model.unmillimetre, posx + thumbsize - model.unmillimetre, posy + thumbsize - model.unmillimetre, model.BitmapSelectedPaint);
                  }
              } else {
                askloadthumbnail.add(new int[]{-2, dossierprincipal, fichiercourant});
                model.drawTempBitmap(surfacecanvas, posx, posy);
              }
              posy += thumbsizedecalagew;
              nbthumbinthiscol += 1;
              if (nbthumbinthiscol >= thumbpercol) {
                posy = yreference;
                posx += thumbsizedecalagew;
                nbthumbinthiscol = 0;
              }
            }
          }
        }
      }
      if (freememory) {
        if (cdi * 3 < model.currentlyDisplayed.size()) { // nouveau dossier affiché
          cdi++;
        }
      }

      if (freememory) {
        try {
          model.commandethreadminiature.put(new String[]{"cleanup", "donotforce"});
        } catch (InterruptedException e) {
          e.printStackTrace();
        }
        llog.d(TAG, libere + " remaining : " + model.numberthumsincache);
      }
    }

    /*llog.d(TAG, String.format("dessus   %4d %4d   (%4d/%4d)   %4d %4d   dessous     :     gauche %4d %4d  droite  %4d %4d",
        ddessusi, ddessusf, dossierprincipal, (foldercount-1), ddessousi, ddessousf,
        fgi, fgf, fdi, fdf));*/


    /***********************
     *
     *    NAVIGATION BAR
     *
     * *********************
     */

    if (!mysurf.OptionMenuShown) {
      //menurecti = 0;
      //menurectn = 0;
      //menurect = new RectF[0];
      if (mysurf.touslesdossierssplitviewbitmap != null) {
        if (!mysurf.touslesdossierssplitviewbitmap.isRecycled()) {
          mysurf.touslesdossierssplitviewbitmap.recycle();
          mysurf.touslesdossierssplitviewbitmap = null;
        }
      }
    } else {
      if (foldercount > 0 && dossierprincipal >= 0 && fichierprincipal >= 0
          && !mysurf.optionshowmodifypic
      ) {
        float posxdrawsplit = model.GenericInterSpace;
        int graphwidth = mysurf.GraphWidth;
        int screenheight = mysurf.ScreenHeight;
        long currenttime = System.currentTimeMillis();
        if ((mysurf.redrawsplitviewbitmap || mysurf.touslesdossierssplitviewbitmap == null) && (currenttime - mysurf.lastsplitviewredraw > 1000)
                && graphwidth > 4 && screenheight > 4 && foldercount > 0) {
          if (mysurf.touslesdossierssplitviewbitmap != null) {
            if (!mysurf.touslesdossierssplitviewbitmap.isRecycled()) {
              mysurf.touslesdossierssplitviewbitmap.recycle();
              mysurf.touslesdossierssplitviewbitmap = null;
            }
          }
          mysurf.redrawsplitviewbitmap = false;
          mysurf.lastsplitviewredraw = currenttime;
          String[] lastdossiersplitted = new String[0];
          int lastsplitsize = 0;
          int plusgrandsplit = 0;
          List<boolean[]> dossiersplitview = new ArrayList<>(foldercount);
          for (int i = 0; i < foldercount; i++) {
            String mondossier = model.getOrnderAddress(i);
            if (mondossier == null) // vu si on en supprime beaucoup
              break;
            String[] dossiersplitted = mondossier.split("/+");
            int dossiersplittedl = dossiersplitted.length - 1; // on ignore le dernier qui est différent à chaque fois de toute façon

            boolean[] splitview = new boolean[dossiersplittedl];
            if (i == 0) {
              for (int j = 0; j < dossiersplittedl; j++) {
                splitview[j] = true;
              }
            } else {
              for (int j = 0; j < dossiersplittedl; j++) {
                if (j < lastsplitsize) {
                  if (dossiersplitted[j].equals(lastdossiersplitted[j])) {
                    splitview[j] = dossiersplitview.get(i - 1)[j];
                  } else {
                    splitview[j] = !dossiersplitview.get(i - 1)[j];
                  }
                } else if (i >= 1 && lastsplitsize >= 1) {
                  splitview[j] = !dossiersplitview.get(i - 1)[lastsplitsize - 1];
                }
              }
            }
            lastsplitsize = dossiersplittedl;
            lastdossiersplitted = dossiersplitted.clone();
            dossiersplitview.add(splitview);

            if (dossiersplitted.length > plusgrandsplit) {
              plusgrandsplit = dossiersplitted.length;
            }
          }

          float widtht = ((float) graphwidth) / ((float) plusgrandsplit);
          float pposx = 0.0f;
          mysurf.touslesdossierssplitviewbitmap = Bitmap.createBitmap(graphwidth, screenheight, Bitmap.Config.ARGB_8888);
          Canvas piccanv = new Canvas(mysurf.touslesdossierssplitviewbitmap);
          for (int i = 0; i < foldercount; i++) {
            String foldername = model.getOrnderAddress(i);
            if (foldername == null)
              break;
            float pposx1 = 0.0f;
            float pposx2 = graphwidth;
            float pposy1 = ((float) screenheight * i) / ((float) foldercount);
            float pposy2 = ((float) screenheight * (i + 1.0f)) / ((float) foldercount);
            //piccanv.drawRect(pposx1, pposy1, pposx2, pposy2, model.GraphBookmarkPaint);
            if (i >= dossiersplitview.size())
              break;
            int viewl = dossiersplitview.get(i).length;
            pposx1 = graphwidth;
            for (int j = 0; j < viewl; j++) {
              pposx2 = pposx1 - widtht;
              pposy1 = ((float) screenheight * i) / ((float) foldercount);
              pposy2 = ((float) screenheight * (i + 1.0f)) / ((float) foldercount);
              if (dossiersplitview.get(i)[j]) {
                piccanv.drawRect(pposx1, pposy1, pposx2, pposy2, model.GraphOnePaint);
              } else {
                piccanv.drawRect(pposx1, pposy1, pposx2, pposy2, model.GraphTwoPaint);
              }
              pposx1 = pposx2;
            }
          }
          needanotherupdate = true;
        }

        if (mysurf.touslesdossierssplitviewbitmap != null) {
          surfacecanvas.drawBitmap(mysurf.touslesdossierssplitviewbitmap, posxdrawsplit, 0.0f, null);

          Paint paint = model.GraphCurrentScreenPaint;
          float rangei = -1;
          float rangef = -0;
          if (ddessusf < 0) {
            //llog.d(TAG, "out of scope above");
            rangei = 0;
            rangef = 0.5f;
            paint = model.GraphCurrentScreenPaintWarn;
          } else if (ddessousi >= foldercount) {
            //llog.d(TAG, "out of scope below");
            rangei = foldercount - 0.5f;
            rangef = foldercount;
            paint = model.GraphCurrentScreenPaintWarn;
          } else {
            if (ddessusi <= dossierprincipal && ddessousf >= dossierprincipal) {
              rangei = ddessusi;
              rangef = ddessousf + 1;
            } else if (ddessusf <= dossierprincipal) {
              rangei = ddessusi;
              rangef = ddessusf + 1;
            } else if (ddessousi >= dossierprincipal) {
              rangei = ddessousi;
              rangef = ddessousf + 1;
            } else if (ddessusi < dossierprincipal) {
              rangei = ddessusi;
              rangef = dossierprincipal + 1;
            } else if (ddessousf > dossierprincipal) {
              rangei = dossierprincipal;
              rangef = ddessousf + 1;
            } else {
              rangei = dossierprincipal;
              rangef = dossierprincipal + 1;
            }
          }

          float pposx1;
          float pposx2;
          if (bpx >= mysurf.mywidth) {
            pposx1 = posxdrawsplit;
            pposx2 = posxdrawsplit + graphwidth * 0.5f;
          } else if (bpx + mysurf.imagewidth < 0) {
            pposx1 = posxdrawsplit + graphwidth * 0.5f;
            pposx2 = posxdrawsplit + graphwidth;
          } else {
            pposx1 = posxdrawsplit;
            pposx2 = posxdrawsplit + graphwidth;
          }
          float pposy2 = ((float) screenheight * rangei) / ((float) foldercount);
          float pposy3 = ((float) screenheight * rangef) / ((float) foldercount);
          if (pposy3 - pposy2 < 4.0f) {
            pposy2 -= 2.0f;
            pposy3 += 2.0f;
          }
          surfacecanvas.drawRect(pposx1, pposy2, pposx2, pposy3, paint);

          pposx2 = posxdrawsplit + graphwidth;
          pposy2 = ((float) screenheight * (dossierprincipal + 0.5f)) / ((float) foldercount);
          surfacecanvas.drawLine(posxdrawsplit, pposy2, pposx2, pposy2, model.GraphCurrPosPaint);
          surfacecanvas.drawCircle(pposx2, pposy2, model.unmillimetre, model.GraphCurrPosPaint);

          if (clickquicknav) {
            if (posxdrawsplit <= clicktohandlex && clicktohandlex <= pposx2) {
              int cetteimage = (int) Math.floor(((float) (foldercount * clicktohandley)) / ((float) screenheight));
              if (cetteimage >= foldercount) {
                cetteimage = foldercount - 1;
              }
              if (cetteimage < 0) {
                cetteimage = 0;
              }
              mysurf.fichierprecedent = "dummy";
              llog.d(TAG, currid + " splitview click : loading " + cetteimage);
              model.changeBigPicture(currid, cetteimage, 0, -1, 0, true, false);
              needanotherupdate = true;
              clickquicknav = false;
            }
          }
        }
      }

      mabase = mysurf.SettingsYmin;
      roundedRectRatio = model.GenericCaseH * 0.2f;
      quatredixiememillimetre = model.GenericCaseH * 0.12f;
      mysurf.SettingsXmin = mysurf.ScreenWidth - mysurf.SettingsWidth;
      //menurecti = 0;
      //menurectn = 50;
      //menurect = new RectF[menurectn];

      if (mabase < mysurf.myheight && model.surfzappercommander == currid && model.videotemps.size() >= 1)
        menuvideotemps(surfacecanvas, foldercount);
      if (mabase < mysurf.myheight)
        menuzoom(surfacecanvas, foldercount);
      if (model.isandroidtv || model.iswatch || model.isauto) {
        if (mabase < mysurf.myheight)
          menuinfo(surfacecanvas, foldercount);
      }
      if (model.iswatch || model.isauto)
        if (mabase < mysurf.myheight)
          menuquit(surfacecanvas);
      if (model.isandroidtv && mysurf.zoommode == Gallery.ZoomComic) {
        if (mabase < mysurf.myheight)
          menuthumbs(surfacecanvas, foldercount);
        if (mabase < mysurf.myheight)
          menunames(surfacecanvas, foldercount);
      }
      if (mabase < mysurf.myheight)
        menumodify(surfacecanvas, foldercount);
      if (mabase < mysurf.myheight)
        menusplit(surfacecanvas, foldercount);
      if (mabase < mysurf.myheight)
        menusearch(surfacecanvas, foldercount);
      if (mabase < mysurf.myheight)
        menubookmark(surfacecanvas, foldercount);
      if (model.availCollectionsOnDiskl > 0)
        if (mabase < mysurf.myheight)
          menucollection(surfacecanvas, foldercount);
      if (mabase < mysurf.myheight)
        menuonline(surfacecanvas, foldercount);
      if (mabase < mysurf.myheight)
        menudiaporama(surfacecanvas, foldercount);
      if (mabase < mysurf.myheight)
        menugame(surfacecanvas, foldercount);
      if (mabase < mysurf.myheight)
        menuselection(surfacecanvas, foldercount);
      if (mabase < mysurf.myheight)
        menushare(surfacecanvas, foldercount);
      if (mabase < mysurf.myheight)
        menuwallpaper(surfacecanvas, foldercount);
      if (mabase < mysurf.myheight)
        menudraw(surfacecanvas, foldercount);
      if (mabase < mysurf.myheight)
        menuaudioplayer(surfacecanvas, foldercount);
      if (mabase < mysurf.myheight)
        menuwidget(surfacecanvas, foldercount);
      if (mabase < mysurf.myheight)
        menurescan(surfacecanvas, foldercount);
      if (mabase < mysurf.myheight)
        menuhide(surfacecanvas, foldercount);
      if (mabase < mysurf.myheight)
        menudiscover(surfacecanvas, foldercount);
      if (mabase < mysurf.myheight)
        menuroot(surfacecanvas, foldercount);
      if (mysurf.optionshowroot) {
        if (mabase < mysurf.myheight)
          menurecord(surfacecanvas, foldercount);
        if (mabase < mysurf.myheight)
          menuapp(surfacecanvas, foldercount);
        if (mabase < mysurf.myheight)
          menumonitor(surfacecanvas, foldercount);
        if (mabase < mysurf.myheight)
          menurootcmd(surfacecanvas, foldercount);
        if (mabase < mysurf.myheight)
          menurootupdate(surfacecanvas, foldercount);
      }
      if (mabase < mysurf.myheight)
        menutweak(surfacecanvas, foldercount);
      if (mysurf.optionshowtweak) {
        if (mabase < mysurf.myheight)
          menuandroidtv(surfacecanvas, foldercount);
        if (mabase < mysurf.myheight)
          menuthumbs(surfacecanvas, foldercount);
        if (mabase < mysurf.myheight)
          menunames(surfacecanvas, foldercount);
        if (mabase < mysurf.myheight)
          menuflagsecure(surfacecanvas, foldercount);
        if (mabase < mysurf.myheight)
          menubuttonsize(surfacecanvas, foldercount);
        if (mabase < mysurf.myheight)
          menufilmstriptime(surfacecanvas, foldercount);
        //if (mabase < mysurf.myheight)
        //  menuatvzoombtn(surfacecanvas, foldercount);
        if (mabase < mysurf.myheight)
          menuswcopy(surfacecanvas);
        //if (mabase < mysurf.myheight)
        //  menusort(surfacecanvas, foldercount);
        if (mabase < mysurf.myheight)
          menumpv(surfacecanvas, foldercount);
        if (mabase < mysurf.myheight)
          menuhwaccel(surfacecanvas, foldercount);
        if (mabase < mysurf.myheight)
          menuautostartboot(surfacecanvas, foldercount);
        if (mabase < mysurf.myheight)
          menushowmenuprevnext(surfacecanvas, foldercount);
        if (mabase < mysurf.myheight)
          menudecodelibextractor(surfacecanvas, foldercount);
        if (mabase < mysurf.myheight)
          menucleanup(surfacecanvas, foldercount);
        if (mabase < mysurf.myheight)
          menuhidesurface(surfacecanvas, foldercount);
        if (mabase < mysurf.myheight)
          menulog(surfacecanvas, foldercount);
        if (mabase < mysurf.myheight)
          menukill(surfacecanvas, foldercount);
      }
      if (mabase < mysurf.myheight)
        menuhelp(surfacecanvas, foldercount);
      if (mysurf.optionshowhowto) {
        if (mabase < mysurf.myheight)
          menuhowto(surfacecanvas, foldercount);
        if (mabase < mysurf.myheight)
          menureadme(surfacecanvas, foldercount);
        if (mabase < mysurf.myheight)
          menufdroid(surfacecanvas, foldercount);
        if (mabase < mysurf.myheight)
          menucheckupdates(surfacecanvas, foldercount);
        if (thereisanupdate != null) {
          if (mabase < mysurf.myheight)
            menurootupdate(surfacecanvas, foldercount);
        }
      }

      if (mysurf.SettingsYmin > mysurf.myheight - model.GenericCaseH)
        mysurf.SettingsYmin = model.settingsYmin;
      mysurf.SettingsYmax = mabase;
      if (mysurf.SettingsYmax < model.GenericCaseH)
        mysurf.SettingsYmin = model.settingsYmin;

    }

    int askloadthumbnailsize = askloadthumbnail.size();
    if (askloadthumbnailsize > 0) {
      if (mysurf.thumbcurrentlydisplayed > 999999998)
        mysurf.thumbcurrentlydisplayed = 1;
      else
        mysurf.thumbcurrentlydisplayed += 1;
      /**
       *   0  notre dossier à droite
       *  -2  notre dossier à gauche
       *   1  dossiers en-dessous
       *  -1  dossiers au-dessus
       */
      // d'abord notre dossier le dernier à droite
      for (int i = askloadthumbnailsize - 1; i >= 0; i--) {
        int[] df = askloadthumbnail.get(i);
        if (df[0] == 0) {
          try {
            model.commandeminiaturethreadqueue.put(new String[]{"loadminiature", String.valueOf(currid), String.valueOf(df[1]), String.valueOf(df[2]), String.valueOf(mysurf.thumbcurrentlydisplayed)});
          } catch (InterruptedException e) {
            e.printStackTrace();
          }
          askloadthumbnail.remove(i);
          askloadthumbnailsize -= 1;
          break;
        }
      }
      // un de chaque dossier-
      int lastfolder = -1;
      for (int i = askloadthumbnailsize - 1; i >= 0; i--) {
        int[] df = askloadthumbnail.get(i);
        if (df[1] != lastfolder) {
          try {
            model.commandeminiaturethreadqueue.put(new String[]{"loadminiature", String.valueOf(currid), String.valueOf(df[1]), String.valueOf(df[2]), String.valueOf(mysurf.thumbcurrentlydisplayed)});
          } catch (InterruptedException e) {
            e.printStackTrace();
          }
          askloadthumbnail.remove(i);
          askloadthumbnailsize -= 1;
          lastfolder = df[1];
        }
      }
      // notre dossier à droite
      for (int i = askloadthumbnailsize - 1; i >= 0; i--) {
        int[] df = askloadthumbnail.get(i);
        if (df[0] == 0) {
          try {
            model.commandeminiaturethreadqueue.put(new String[]{"loadminiature", String.valueOf(currid), String.valueOf(df[1]), String.valueOf(df[2]), String.valueOf(mysurf.thumbcurrentlydisplayed)});
          } catch (InterruptedException e) {
            e.printStackTrace();
          }
          askloadthumbnail.remove(i);
          askloadthumbnailsize -= 1;
        }
      }
      // notre dossier à gauche
      for (int i = askloadthumbnailsize - 1; i >= 0; i--) {
        int[] df = askloadthumbnail.get(i);
        if (df[0] == -2) {
          try {
            model.commandeminiaturethreadqueue.put(new String[]{"loadminiature", String.valueOf(currid), String.valueOf(df[1]), String.valueOf(df[2]), String.valueOf(mysurf.thumbcurrentlydisplayed)});
          } catch (InterruptedException e) {
            e.printStackTrace();
          }
          askloadthumbnail.remove(i);
          askloadthumbnailsize -= 1;
        }
      }
      // un de chaque dossier de droite à gauche
      while (askloadthumbnailsize > 0) {
        lastfolder = -1;
        for (int i = askloadthumbnailsize - 1; i >= 0; i--) {
          int[] df = askloadthumbnail.get(i);
          if (df[1] != lastfolder) {
            try {
              model.commandeminiaturethreadqueue.put(new String[]{"loadminiature", String.valueOf(currid), String.valueOf(df[1]), String.valueOf(df[2]), String.valueOf(mysurf.thumbcurrentlydisplayed)});
            } catch (InterruptedException e) {
              e.printStackTrace();
            }
            askloadthumbnail.remove(i);
            askloadthumbnailsize -= 1;
            lastfolder = df[1];
          }
        }
      }
    }

    if (needanotherupdate) {
      try {
        model.commandethreadbrowser.put(new String[]{String.valueOf(currid), "update"});
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
    }
    return needanotherupdate;
  }

  private int[] strpos(String s, int i) {
    //int r = i;
    int sl = s.length();
    int j = -1;
    int k = -1;
    int l = -1;
    if (i >= sl)
      return new int[]{j, k, l};
    while (i < sl) {
      char c = s.charAt(i);

      if (c != '/') {
        if (j == -1) {
          j = i;
          k = i+1;
        } else {
          k = i+1;
        }
      } else {
        if (j != -1 && k != -1) {
          i++;
          break;
        }
      }

      i++;
    }
    if (j != -1 && k != -1) {
      l = i;
      if (k > 1 && k > j + 1) {
        if (s.charAt(k-1) == ':') {
          k -= 1;
        }
      }
    }
    /*if (j >= 0 && r < sl) {
      llog.d(TAG, s
          + "\n   " + s.substring(r, sl)
          + "                " + s.substring(j, k)
          + "      " + s.substring(0, l)
          + "      " + j + " " + k + " " + l
      );
    }*/
    return new int[]{j, k, l};
  }

  private float mabase;
  private float roundedRectRatio;
  private float quatredixiememillimetre;
  private float y1, y2, taillex, x1, x2, ecritx, ecrity;
  private String ecritoption = "";
  private RectF recti;
  private boolean needanotherupdate;

  private int menurecti = 0;
  private int menurectn = 0;
  private RectF[] menurect = new RectF[0];
  private boolean clickedmenu(){
    for (int i = 0 ; i < menurectn ; i++) {
      if (menurect[i] == null)
        return false;
      if (menurect[i].top < clicktohandley && clicktohandley < menurect[i].bottom)
        if (menurect[i].left < clicktohandlex && clicktohandlex < menurect[i].right)
          return true;
    }
    return false;
  }

  private void menudiaporama(Canvas surfacecanvas, int foldercount) {
    if (!mysurf.optiondiaporamaactive) {
      ecritoption = "slideshow";
    } else {
      if (mysurf.diaporamadeltatime <= 0.001f)
        mysurf.diaporamadeltatime = model.preferences.getFloat("diaporamadeltatime", 6.0f);
      ecritoption = String.format("slideshow %.1fs" , mysurf.diaporamadeltatime);
    }
    y1 = mabase;
    y2 = mabase + model.GenericCaseH;
    taillex = model.Menu1TextePaint.measureText(ecritoption);
    x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
    x2 = mysurf.ScreenWidth - model.GenericInterSpace;
    if (x1 > mysurf.SettingsXmin) {
      x1 = mysurf.SettingsXmin;
    }
    ecritx = x2 - model.GenericInterSpace;
    ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
    if (clickquicknavmenu) {
      if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
        mysurf.diaporamalastchange = -1;
        mysurf.optiondiaporamaactive = !mysurf.optiondiaporamaactive;
        if (mysurf.optiondiaporamaactive)
          model.message("Slideshow started.\nPlease hide the menu to resume.");
        else
          model.message("Slideshow stopped.");
        /*if (mysurf.optiondiaporamaactive) {
          if (model.getFileIsVideo(mysurf.dossierprincipal, mysurf.fichierprincipal)) {
                try {
                  model.commandethreadbrowser.put(new String[]{String.valueOf(currid), "playvid", String.valueOf(mysurf.ordnerIndex), String.valueOf(mysurf.mediaIndex)});
                } catch (InterruptedException e) {
                }
          }
        }*/
        needanotherupdate = true;
        clickquicknavmenu = false;
      }
    }
    //menurect[menurecti] = new RectF(x1, y1, x2, y2);
    //surfacecanvas.drawRoundRect(menurect[menurecti++], roundedRectRatio, roundedRectRatio, model.Menu1BgPainto);
    recti = new RectF(x1, y1, x2, y2);
    surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu1BgPainta);
    recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
    surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu1BgPainto);
    if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
      RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
      surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
    }
    surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);
    mabase += model.GenericCaseH + model.GenericInterSpace;

    if (mabase > mysurf.myheight)
      return;

    if (mysurf.optiondiaporamaactive) {
      if (mysurf.diaporamadeltatime <= 0.001f)
        mysurf.diaporamadeltatime = model.preferences.getFloat("diaporamadeltatime", 6.0f);

      y1 = mabase;
      y2 = mabase + model.GenericCaseH;
      ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;

      ecritoption = "+";
      taillex = model.Menu1TextePaint.measureText(ecritoption);
      x1 = (mysurf.ScreenWidth - model.GenericInterSpace + mysurf.SettingsXmin) * 0.5f + model.GenericInterSpace * 0.5f;
      x2 = mysurf.ScreenWidth - model.GenericInterSpace;
      ecritx = x2;
      if (clickquicknavmenu) {
        if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
          mysurf.diaporamadeltatime += diaporamapower[diaporamapoweri];
          mysurf.diaporamalastchange = -1;
          model.preferences.edit().putFloat("diaporamadeltatime", mysurf.diaporamadeltatime).apply();
          needanotherupdate = true;
          clickquicknavmenu = false;
        }
      }
      //menurect[menurecti] = new RectF(x1, y1, x2, y2);
      //surfacecanvas.drawRoundRect(menurect[menurecti++], roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
      recti = new RectF(x1, y1, x2, y2);
      //surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
      //recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
      if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
        RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
        surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
      }
      surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

      ecritoption = "-";
      taillex = model.Menu1TextePaint.measureText(ecritoption);
      x2 = x1 - model.GenericInterSpace;
      x1 = x2 - model.GenericInterSpace * 2.0f - taillex;
      if (x1 > mysurf.SettingsXmin)
        x1 = mysurf.SettingsXmin;
      ecritx = x2 - model.GenericInterSpace;
      if (clickquicknavmenu) {
        if (mysurf.SettingsXmin < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
          if (mysurf.diaporamadeltatime - diaporamapower[diaporamapoweri] > 0)
            mysurf.diaporamadeltatime -= diaporamapower[diaporamapoweri];
          mysurf.diaporamalastchange = -1;
          model.preferences.edit().putFloat("diaporamadeltatime", mysurf.diaporamadeltatime).apply();
          needanotherupdate = true;
          clickquicknavmenu = false;
        }
      }
      //menurect[menurecti] = new RectF(Math.min(mysurf.SettingsXmin, x1), y1, x2, y2);
      //surfacecanvas.drawRoundRect(menurect[menurecti++], roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
      recti = new RectF(x1, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
      recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
      if ((model.isandroidtv || model.keypadnavactive) && mysurf.SettingsXmin < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
        RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
        surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
      }
      surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

      mabase += model.GenericCaseH + model.GenericInterSpace;
      if (mabase > mysurf.myheight)
        return;

      ecritoption = String.format("+/- %.3f", diaporamapower[diaporamapoweri]);
      y1 = mabase;
      y2 = mabase + model.GenericCaseH;
      taillex = model.Menu1TextePaint.measureText(ecritoption);
      x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
      x2 = mysurf.ScreenWidth - model.GenericInterSpace;
      if (x1 > mysurf.SettingsXmin) {
        x1 = mysurf.SettingsXmin;
      }
      ecritx = x2 - model.GenericInterSpace;
      ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
      if (clickquicknavmenu) {
        if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
          diaporamapoweri = (diaporamapower.length + diaporamapoweri + 1) % diaporamapower.length;
          needanotherupdate = true;
          clickquicknavmenu = false;
        }
      }
      //menurect[menurecti] = new RectF(x1, y1, x2, y2);
      //surfacecanvas.drawRoundRect(menurect[menurecti++], roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
      recti = new RectF(x1, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
      recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
      if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
        RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
        surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
      }
      surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

      mabase += model.GenericCaseH + model.GenericInterSpace;
      if (mabase > mysurf.myheight)
        return;

      if (mysurf.optiondiaporamalinear == Surf.DIAPORAMA_LINEAR)
        ecritoption = "linear";
      else if (mysurf.optiondiaporamalinear == Surf.DIAPORAMA_RANDOM_FOLDER)
        ecritoption = "random folder";
      else if (mysurf.optiondiaporamalinear == Surf.DIAPORAMA_RANDOM)
        ecritoption = "random";
      y1 = mabase;
      y2 = mabase + model.GenericCaseH;
      taillex = model.Menu1TextePaint.measureText(ecritoption);
      x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
      x2 = mysurf.ScreenWidth - model.GenericInterSpace;
      if (x1 > mysurf.SettingsXmin) {
        x1 = mysurf.SettingsXmin;
      }
      ecritx = x2 - model.GenericInterSpace;
      ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
      if (clickquicknavmenu) {
        if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
          mysurf.optiondiaporamalinear = (mysurf.optiondiaporamalinear + 1) % 3;
          if (mysurf.optiondiaporamalinear == Surf.DIAPORAMA_RANDOM_FOLDER)
            model.message("picks a random folder\nand plays it fully");
          else
            model.message("");
          needanotherupdate = true;
          clickquicknavmenu = false;
        }
      }
      //menurect[menurecti] = new RectF(x1, y1, x2, y2);
      //surfacecanvas.drawRoundRect(menurect[menurecti++], roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
      recti = new RectF(x1, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
      recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
      if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
        RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
        surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
      }
      surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

      mabase += model.GenericCaseH + model.GenericInterSpace;
      if (mabase > mysurf.myheight)
        return;

      ecritoption = "rjump";
      y1 = mabase;
      y2 = mabase + model.GenericCaseH;
      taillex = model.Menu1TextePaint.measureText(ecritoption);
      x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
      x2 = mysurf.ScreenWidth - model.GenericInterSpace;
      if (x1 > mysurf.SettingsXmin) {
        x1 = mysurf.SettingsXmin;
      }
      ecritx = x2 - model.GenericInterSpace;
      ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
      if (clickquicknavmenu) {
        if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
          int d = Gallery.rand.nextInt(model.folderCount);
          if (mysurf.optiondiaporamalinear == Surf.DIAPORAMA_RANDOM)
            model.showrandomfilewhenfolderready = true;
          model.changeBigPicture(currid, d, 0, 0, 0, true, false);
          needanotherupdate = true;
          clickquicknavmenu = false;
        }
      }
      //menurect[menurecti] = new RectF(x1, y1, x2, y2);
      //surfacecanvas.drawRoundRect(menurect[menurecti++], roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
      recti = new RectF(x1, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
      recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
      if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
        RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
        surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
      }
      surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

      mabase += model.GenericCaseH + model.GenericInterSpace;
      if (mabase > mysurf.myheight)
        return;
    }

  }

  private void menuaudioplayer(Canvas surfacecanvas, int foldercount) {
    if (!model.optionaudioplayeractive) {
      ecritoption = "music";
      /*if (mysurf.optionshowmusic) {
        if (model.currCollectionPrintName != null) {
          ecritoption += " <" + model.currCollectionPrintName + ">";
        } else if (musiccollectionname != null) {
          ecritoption += " <" + musiccollectionname + ">";
        }
      }*/
    } else {
      ecritoption = "music on";
      if (mysurf.optionshowmusic)
        if (Gallery.backgroundService != null)
          if (Gallery.backgroundService.threadmusic != null)
            if (backgroundService.musicstatusname != null)
              if (backgroundService.musicstatusname.length() > 4)
                ecritoption = backgroundService.musicstatusname;
    }
    y1 = mabase;
    y2 = mabase + model.GenericCaseH;
    taillex = model.Menu1TextePaint.measureText(ecritoption);
    x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
    x2 = mysurf.ScreenWidth - model.GenericInterSpace;
    if (x1 > mysurf.SettingsXmin) {
      x1 = mysurf.SettingsXmin;
    }
    ecritx = x2 - model.GenericInterSpace;
    ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
    if (clickquicknavmenu) {
      if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
        if (!mysurf.optionshowmusic && musiccollectionnamel == 0 && model.currCollectionAddress == null) {
          model.message("Please create a music collection first.");
          mysurf.optionshowselection = true;
        } else {
          mysurf.optionshowmusic = !mysurf.optionshowmusic;
        }
        needanotherupdate = true;
        clickquicknavmenu = false;
      }
    }
    //menurect[menurecti] = new RectF(x1, y1, x2, y2);
    //surfacecanvas.drawRoundRect(menurect[menurecti++], roundedRectRatio, roundedRectRatio, model.Menu1BgPainto);
    recti = new RectF(x1, y1, x2, y2);
    surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu1BgPainta);
    recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
    surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu1BgPainto);
    if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
      RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
      surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
    }
    surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

    mabase += model.GenericCaseH + model.GenericInterSpace;
    if (mabase > mysurf.myheight)
      return;

    if (mysurf.optionshowmusic) {

      if (!model.optionaudioplayeractive) {
        if (model.currCollectionPrintName != null) {
          ecritoption = "play <" + model.currCollectionPrintName + ">";
          y1 = mabase;
          y2 = mabase + model.GenericCaseH;
          taillex = model.Menu1TextePaint.measureText(ecritoption);
          x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
          x2 = mysurf.ScreenWidth - model.GenericInterSpace;
          if (x1 > mysurf.SettingsXmin) {
            x1 = mysurf.SettingsXmin;
          }
          ecritx = x2 - model.GenericInterSpace;
          ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
          if (clickquicknavmenu) {
            if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
              model.optionaudioplayeractive = true;
              model.message("start playing music on startup");
              if (musiccollectionnamel == 0 && model.currCollectionAddress == null) {
                model.message("please load a music collection first");
                model.optionaudioplayeractive = false;
                model.preferences.edit().putBoolean("playmusic", model.optionaudioplayeractive).commit();
              } else {
                model.preferences.edit().putBoolean("playmusic", model.optionaudioplayeractive).commit();
                if (model.currCollectionPrintName != null) {
                  musiccollectionname.add(model.currCollectionPrintName);
                  musiccollectionaddress.add(model.currCollectionAddress);
                  model.preferences.edit().putString("musiccollectionname" + musiccollectionnamel, model.currCollectionPrintName).commit();
                  model.preferences.edit().putString("musiccollectionaddress" + musiccollectionnamel, model.currCollectionAddress).commit();
                  model.preferences.edit().putInt("musiccollectionnamei", musiccollectionnamel).commit();
                  musiccollectionnamel += 1;
                  model.preferences.edit().putInt("musiccollectionnamel", musiccollectionnamel).commit();
                }
                if (!Gallery.isMyServiceRunning(backgroundService.class, model.activitycontext)) {
                  model.message("music service started");
                  Intent intent = new Intent(model.activitycontext, backgroundService.class);
                  if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
                    model.activitycontext.startForegroundService(intent);
                  else
                    model.activitycontext.startService(intent);
                } else {
                  model.message("music service restarted");
                  Intent intent = new Intent(backgroundService.REQUEST + backgroundService.REQUEST_STARTMUSIC);
                  intent.setClass(model.activitycontext, backgroundService.class);
                  if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
                    model.activitycontext.startForegroundService(intent);
                  else
                    model.activitycontext.startService(intent);
                }
              }
              recheckThreadDiscoverRunning = 10;
              needanotherupdate = true;
              clickquicknavmenu = false;
            }
          }
          recti = new RectF(x1, y1, x2, y2);
          surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
          recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
          surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
          if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
            RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
            surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
          }
          surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

          mabase += model.GenericCaseH + model.GenericInterSpace;
          if (mabase > mysurf.myheight)
            return;
        }

        for (int i = 0 ; i < musiccollectionnamel ; i++) {

          y1 = mabase;
          y2 = mabase + model.GenericCaseH;
          ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;

          ecritoption = "X";
          taillex = model.Menu1TextePaint.measureText(ecritoption);
          x1 = (mysurf.ScreenWidth - model.GenericInterSpace + mysurf.SettingsXmin) * 0.5f + model.GenericInterSpace * 0.5f;
          x2 = mysurf.ScreenWidth - model.GenericInterSpace;
          ecritx = x2;
          if (clickquicknavmenu) {
            if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
              SharedPreferences.Editor editor = model.preferences.edit();
              for (int j = i ; j < musiccollectionnamel-1 ; j++) {
                editor.putString("musiccollectionname" + j, model.preferences.getString("musiccollectionname" + (j+1), null));
                editor.putString("musiccollectionaddress" + j, model.preferences.getString("musiccollectionaddress" + (j+1), null));
              }
              editor.remove("musiccollectionname" + (i-1));
              editor.remove("musiccollectionaddress" + (i-1));
              editor.putInt("musiccollectionnamel", (musiccollectionnamel-1));
              editor.apply();
              musiccollectionnamel -= 1;
              musiccollectionname.remove(i);
              musiccollectionaddress.remove(i);
              needanotherupdate = true;
              clickquicknavmenu = false;
              break;
            }
          }
          recti = new RectF(x1, y1, x2, y2);
          //surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
          //recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
          surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
          if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
            RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
            surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
          }
          surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

          ecritoption = "play <" + musiccollectionname.get(i) + ">";
          taillex = model.Menu1TextePaint.measureText(ecritoption);
          x2 = x1 - model.GenericInterSpace;
          x1 = x2 - model.GenericInterSpace * 2.0f - taillex;
          if (x1 > mysurf.SettingsXmin)
            x1 = mysurf.SettingsXmin;
          ecritx = x2 - model.GenericInterSpace;
          if (clickquicknavmenu) {
            if (mysurf.SettingsXmin < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
              if (musiccollectionaddress.get(i).startsWith("/") && !new File(musiccollectionaddress.get(i)).exists()) {
                SharedPreferences.Editor editor = model.preferences.edit();
                for (int j = i ; j < musiccollectionnamel-1 ; j++) {
                  editor.putString("musiccollectionname" + j, model.preferences.getString("musiccollectionname" + (j+1), null));
                  editor.putString("musiccollectionaddress" + j, model.preferences.getString("musiccollectionaddress" + (j+1), null));
                }
                editor.remove("musiccollectionname" + (i-1));
                editor.remove("musiccollectionaddress" + (i-1));
                editor.putInt("musiccollectionnamel", (musiccollectionnamel-1));
                editor.apply();
                musiccollectionnamel -= 1;
                musiccollectionname.remove(i);
                musiccollectionaddress.remove(i);
                // on ne supprime pas la collection ici
                needanotherupdate = true;
                clickquicknavmenu = false;
                break;
              } else {
                model.preferences.edit().putInt("musiccollectionnamei", i).commit();
                model.optionaudioplayeractive = true;
                model.message("start playing music on startup");
                if (musiccollectionnamel == 0 && model.currCollectionAddress == null) {
                  model.message("please load a music collection first");
                  model.optionaudioplayeractive = false;
                  model.preferences.edit().putBoolean("playmusic", model.optionaudioplayeractive).commit();
                } else {
                  model.preferences.edit().putBoolean("playmusic", model.optionaudioplayeractive).commit();
                  if (model.currCollectionPrintName != null) {
                    //musiccollectionname = model.currCollectionPrintName;
                    //model.preferences.edit().putString("musiccollectionname", musiccollectionname).commit();
                  }
                  if (!Gallery.isMyServiceRunning(backgroundService.class, model.activitycontext)) {
                    model.message("music service started");
                    Intent intent = new Intent(model.activitycontext, backgroundService.class);
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
                      model.activitycontext.startForegroundService(intent);
                    else
                      model.activitycontext.startService(intent);
                  } else {
                    model.message("music service restarted");
                    Intent intent = new Intent(backgroundService.REQUEST + backgroundService.REQUEST_STARTMUSIC);
                    intent.setClass(model.activitycontext, backgroundService.class);
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
                      model.activitycontext.startForegroundService(intent);
                    else
                      model.activitycontext.startService(intent);
                  }
                }
                recheckThreadDiscoverRunning = 10;
              }
              needanotherupdate = true;
              clickquicknavmenu = false;
            }
          }
          recti = new RectF(x1, y1, x2, y2);
          surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
          recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
          surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
          if ((model.isandroidtv || model.keypadnavactive) && mysurf.SettingsXmin < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
            RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
            surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
          }
          surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

          mabase += model.GenericCaseH + model.GenericInterSpace;
          if (mabase > mysurf.myheight)
            return;

        }
      }

      if (model.optionaudioplayeractive) {
        ecritoption = "stop";
        y1 = mabase;
        y2 = mabase + model.GenericCaseH;
        taillex = model.Menu1TextePaint.measureText(ecritoption);
        x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
        x2 = mysurf.ScreenWidth - model.GenericInterSpace;
        if (x1 > mysurf.SettingsXmin) {
          x1 = mysurf.SettingsXmin;
        }
        ecritx = x2 - model.GenericInterSpace;
        ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
        if (clickquicknavmenu) {
          if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
            model.optionaudioplayeractive = false;
            model.message("music doesn't start playing automatically");
            model.preferences.edit().putBoolean("playmusic", model.optionaudioplayeractive).commit();
            if (Gallery.isMyServiceRunning(backgroundService.class, model.activitycontext)) {
              model.message("music service stopped");
              Intent intent = new Intent(backgroundService.REQUEST + backgroundService.REQUEST_STOPMUSIC);
              intent.setClass(model.activitycontext, backgroundService.class);
              if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
                model.activitycontext.startForegroundService(intent);
              else
                model.activitycontext.startService(intent);
            }
            recheckThreadDiscoverRunning = 10;
            needanotherupdate = true;
            clickquicknavmenu = false;
          }
        }
        recti = new RectF(x1, y1, x2, y2);
        surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
        recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
        surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
        if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
          RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
          surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
        }
        surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

        mabase += model.GenericCaseH + model.GenericInterSpace;
        if (mabase > mysurf.myheight)
          return;
      }

      //llog.d(TAG, "optionaudioplayeractive " + model.optionaudioplayeractive + " Gallery.backgroundService " + (Gallery.backgroundService!=null));
      if (model.optionaudioplayeractive && Gallery.backgroundService != null) {

        ecritoption = "pause";
        if (backgroundService.musicstatuspaused)
          ecritoption = "play";
        y1 = mabase;
        y2 = mabase + model.GenericCaseH;
        taillex = model.Menu1TextePaint.measureText(ecritoption);
        x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
        x2 = mysurf.ScreenWidth - model.GenericInterSpace;
        if (x1 > mysurf.SettingsXmin) {
          x1 = mysurf.SettingsXmin;
        }
        ecritx = x2 - model.GenericInterSpace;
        ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
        if (clickquicknavmenu) {
          if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
            if (Gallery.backgroundService != null)
              Gallery.backgroundService.musicplaypause();
            needanotherupdate = true;
            clickquicknavmenu = false;
          }
        }
        recti = new RectF(x1, y1, x2, y2);
        surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
        recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
        surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
        if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
          RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
          surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
        }
        surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

        mabase += model.GenericCaseH + model.GenericInterSpace;
        if (mabase > mysurf.myheight)
          return;

        ecritoption = "next";
        y1 = mabase;
        y2 = mabase + model.GenericCaseH;
        taillex = model.Menu1TextePaint.measureText(ecritoption);
        x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
        x2 = mysurf.ScreenWidth - model.GenericInterSpace;
        if (x1 > mysurf.SettingsXmin) {
          x1 = mysurf.SettingsXmin;
        }
        ecritx = x2 - model.GenericInterSpace;
        ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
        if (clickquicknavmenu) {
          if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
            Gallery.backgroundService.musicnext();
            needanotherupdate = true;
            clickquicknavmenu = false;
          }
        }
        recti = new RectF(x1, y1, x2, y2);
        surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
        recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
        surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
        if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
          RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
          surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
        }
        surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

        mabase += model.GenericCaseH + model.GenericInterSpace;
        if (mabase > mysurf.myheight)
          return;

        /*int optionaudioplayerplayinglistl = model.optionaudioplayerplayinglist.size();
        if (optionaudioplayerplayinglistl > 0) {
          ecritoption = model.optionaudioplayerplayinglist.get(optionaudioplayerplayinglistl - 1) + " current";
          y1 = mabase;
          y2 = mabase + model.GenericCaseH;
          taillex = model.Menu1TextePaint.measureText(ecritoption);
          x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
          x2 = mysurf.ScreenWidth - model.GenericInterSpace;
          if (x1 > mysurf.SettingsXmin) {
            x1 = mysurf.SettingsXmin;
          }
          ecritx = x2 - model.GenericInterSpace;
          ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
          if (clickquicknavmenu) {
            if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
              model.optionaudioplayerrunning = true;
              try {
                model.commandethreaddatabase.put(new String[]{"playmusicafterloadselection", "noappend", "next"});
              } catch (InterruptedException e) {
                e.printStackTrace();
              }
              needanotherupdate = true;
              clickquicknavmenu = false;
            }
          }
          recti = new RectF(x1, y1, x2, y2);
          surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
          recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
          surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
          if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
            RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
            surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
          }
          surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

          mabase += model.GenericCaseH + model.GenericInterSpace;
          if (mabase > mysurf.myheight)
            return;
        }*/

        ecritoption = "previous";
        y1 = mabase;
        y2 = mabase + model.GenericCaseH;
        taillex = model.Menu1TextePaint.measureText(ecritoption);
        x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
        x2 = mysurf.ScreenWidth - model.GenericInterSpace;
        if (x1 > mysurf.SettingsXmin) {
          x1 = mysurf.SettingsXmin;
        }
        ecritx = x2 - model.GenericInterSpace;
        ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
        if (clickquicknavmenu) {
          if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
            Gallery.backgroundService.musicprevious();
            needanotherupdate = true;
            clickquicknavmenu = false;
          }
        }
        recti = new RectF(x1, y1, x2, y2);
        surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
        recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
        surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
        if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
          RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
          surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
        }
        surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

        mabase += model.GenericCaseH + model.GenericInterSpace;
        if (mabase > mysurf.myheight)
          return;

        ecritoption = "random";
        if (!backgroundService.musicstatusrandom)
          ecritoption = "linear";
        y1 = mabase;
        y2 = mabase + model.GenericCaseH;
        taillex = model.Menu1TextePaint.measureText(ecritoption);
        x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
        x2 = mysurf.ScreenWidth - model.GenericInterSpace;
        if (x1 > mysurf.SettingsXmin) {
          x1 = mysurf.SettingsXmin;
        }
        ecritx = x2 - model.GenericInterSpace;
        ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
        if (clickquicknavmenu) {
          if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
            Gallery.backgroundService.musicrandom();
            needanotherupdate = true;
            clickquicknavmenu = false;
          }
        }
        recti = new RectF(x1, y1, x2, y2);
        surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
        recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
        surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
        if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
          RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
          surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
        }
        surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

        mabase += model.GenericCaseH + model.GenericInterSpace;
        if (mabase > mysurf.myheight)
          return;

        ecritoption = "search";
        y1 = mabase;
        y2 = mabase + model.GenericCaseH;
        taillex = model.Menu1TextePaint.measureText(ecritoption);
        x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
        x2 = mysurf.ScreenWidth - model.GenericInterSpace;
        if (x1 > mysurf.SettingsXmin) {
          x1 = mysurf.SettingsXmin;
        }
        ecritx = x2 - model.GenericInterSpace;
        ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
        if (clickquicknavmenu) {
          if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
            Intent intent = new Intent();
            intent.setAction(Gallery.broadcastname);
            intent.putExtra("goal", "musicsearch");
            intent.putExtra("id", currid);
            LocalBroadcastManager.getInstance(model.activitycontext).sendBroadcast(intent);
            needanotherupdate = true;
            clickquicknavmenu = false;
          }
        }
        recti = new RectF(x1, y1, x2, y2);
        surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
        recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
        surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
        if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
          RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
          surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
        }
        surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

        mabase += model.GenericCaseH + model.GenericInterSpace;
        if (mabase > mysurf.myheight)
          return;

        if (model.musiclastsearch != null) {
          ecritoption = "next " + model.musiclastsearch;
          y1 = mabase;
          y2 = mabase + model.GenericCaseH;
          taillex = model.Menu1TextePaint.measureText(ecritoption);
          x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
          x2 = mysurf.ScreenWidth - model.GenericInterSpace;
          if (x1 > mysurf.SettingsXmin) {
            x1 = mysurf.SettingsXmin;
          }
          ecritx = x2 - model.GenericInterSpace;
          ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
          if (clickquicknavmenu) {
            if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
              if (Gallery.backgroundService != null)
                Gallery.backgroundService.musicsearch(model.musiclastsearch);
              needanotherupdate = true;
              clickquicknavmenu = false;
            }
          }
          recti = new RectF(x1, y1, x2, y2);
          surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
          recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
          surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
          if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
            RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
            surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
          }
          surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

          mabase += model.GenericCaseH + model.GenericInterSpace;
          if (mabase > mysurf.myheight)
            return;
        }

        y1 = mabase;
        y2 = mabase + model.GenericCaseH;
        ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;

        ecritoption = "+";
        taillex = model.Menu1TextePaint.measureText(ecritoption);
        x1 = (mysurf.ScreenWidth - model.GenericInterSpace + mysurf.SettingsXmin) * 0.5f + model.GenericInterSpace * 0.5f;
        x2 = mysurf.ScreenWidth - model.GenericInterSpace;
        ecritx = x2;
        if (clickquicknavmenu) {
          if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
            model.videoaudiovolume += 1;
            model.preferences.edit().putInt("audiovolume", model.videoaudiovolume).apply();
            Gallery.backgroundService.musicvolume();
            needanotherupdate = true;
            clickquicknavmenu = false;
          }
        }
        recti = new RectF(x1, y1, x2, y2);
        //surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
        //recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
        surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
        if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
          RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
          surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
        }
        surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

        ecritoption = "vol " + model.videoaudiovolume + "% -";
        taillex = model.Menu1TextePaint.measureText(ecritoption);
        x2 = x1 - model.GenericInterSpace;
        x1 = x2 - model.GenericInterSpace * 2.0f - taillex;
        if (x1 > mysurf.SettingsXmin)
          x1 = mysurf.SettingsXmin;
        ecritx = x2 - model.GenericInterSpace;
        if (clickquicknavmenu) {
          if (mysurf.SettingsXmin < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
            if (model.videoaudiovolume - 1 >= 0)
              model.videoaudiovolume -= 1;
            else
              model.videoaudiovolume = 0;
            model.preferences.edit().putInt("audiovolume", model.videoaudiovolume).apply();
            Gallery.backgroundService.musicvolume();
            needanotherupdate = true;
            clickquicknavmenu = false;
          }
        }
        recti = new RectF(x1, y1, x2, y2);
        surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
        recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
        surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
        if ((model.isandroidtv || model.keypadnavactive) && mysurf.SettingsXmin < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
          RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
          surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
        }
        surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

        mabase += model.GenericCaseH + model.GenericInterSpace;
        if (mabase > mysurf.myheight)
          return;

        if (model.videoaudioweightlevel1)
          ecritoption = "level 1";
        else
          ecritoption = "level default";
        y1 = mabase;
        y2 = mabase + model.GenericCaseH;
        taillex = model.Menu1TextePaint.measureText(ecritoption);
        x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
        x2 = mysurf.ScreenWidth - model.GenericInterSpace;
        if (x1 > mysurf.SettingsXmin) {
          x1 = mysurf.SettingsXmin;
        }
        ecritx = x2 - model.GenericInterSpace;
        ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
        if (clickquicknavmenu) {
          if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
            model.videoaudioweightlevel1 = !model.videoaudioweightlevel1;
            if (model.videoaudioweightlevel1) {
              model.message("all level 1 base folders\n" +
                  "have the same weight\n" +
                  "for random pick");
            } else {
              model.message();
            }
            model.preferences.edit().putBoolean("videoaudioweightlevel1", model.videoaudioweightlevel1).apply();
            needanotherupdate = true;
            clickquicknavmenu = false;
          }
        }
        recti = new RectF(x1, y1, x2, y2);
        surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
        recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
        surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
        if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
          RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
          surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
        }
        surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

        mabase += model.GenericCaseH + model.GenericInterSpace;
        if (mabase > mysurf.myheight)
          return;

        if (!model.videoaudioweightroot1)
          ecritoption = "weight n";
        else
          ecritoption = "weight default";
        y1 = mabase;
        y2 = mabase + model.GenericCaseH;
        taillex = model.Menu1TextePaint.measureText(ecritoption);
        x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
        x2 = mysurf.ScreenWidth - model.GenericInterSpace;
        if (x1 > mysurf.SettingsXmin) {
          x1 = mysurf.SettingsXmin;
        }
        ecritx = x2 - model.GenericInterSpace;
        ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
        if (clickquicknavmenu) {
          if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
            model.videoaudioweightroot1 = !model.videoaudioweightroot1;
            if (!model.videoaudioweightroot1) {
              model.message("root folder" +
                  "\nhas weight = number of medias" +
                  "\nother folders have weight 1" +
                  "\nfor random pick");
            } else {
              model.message();
            }
            model.preferences.edit().putBoolean("videoaudioweightroot1", model.videoaudioweightroot1).apply();
            needanotherupdate = true;
            clickquicknavmenu = false;
          }
        }
        recti = new RectF(x1, y1, x2, y2);
        surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
        recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
        surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
        if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
          RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
          surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
        }
        surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

        mabase += model.GenericCaseH + model.GenericInterSpace;
        if (mabase > mysurf.myheight)
          return;


        y1 = mabase;
        y2 = mabase + model.GenericCaseH;
        ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;

        ecritoption = "+";
        taillex = model.Menu1TextePaint.measureText(ecritoption);
        x1 = (mysurf.ScreenWidth - model.GenericInterSpace + mysurf.SettingsXmin) * 0.5f + model.GenericInterSpace * 0.5f;
        x2 = mysurf.ScreenWidth - model.GenericInterSpace;
        ecritx = x2;
        if (clickquicknavmenu) {
          if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
            model.musicrandminreplay += 10;
            model.preferences.edit().putInt("musicrandminreplay", model.musicrandminreplay).apply();
            model.message("avoid playing the same file" +
                "\nbefore " + model.musicrandminreplay + " songs" +
                "\nchances = (x/" + model.musicrandminreplay + ") power 4");
            needanotherupdate = true;
            clickquicknavmenu = false;
          }
        }
        recti = new RectF(x1, y1, x2, y2);
        //surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
        //recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
        surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
        if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
          RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
          surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
        }
        surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

        ecritoption = "replay after " + model.musicrandminreplay + " -";
        taillex = model.Menu1TextePaint.measureText(ecritoption);
        x2 = x1 - model.GenericInterSpace;
        x1 = x2 - model.GenericInterSpace * 2.0f - taillex;
        if (x1 > mysurf.SettingsXmin)
          x1 = mysurf.SettingsXmin;
        ecritx = x2 - model.GenericInterSpace;
        if (clickquicknavmenu) {
          if (mysurf.SettingsXmin < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
            if (model.musicrandminreplay - 10 >= 0)
              model.musicrandminreplay -= 10;
            else
              model.musicrandminreplay = 0;
            model.preferences.edit().putInt("musicrandminreplay", model.musicrandminreplay).apply();
            if (model.musicrandminreplay > 0)
              model.message("avoid playing the same file" +
                  "\nbefore " + model.musicrandminreplay + " songs" +
                  "\nchances = (x/" + model.musicrandminreplay + ") power 4");
            else
              model.message("default random playback");
            needanotherupdate = true;
            clickquicknavmenu = false;
          }
        }
        recti = new RectF(x1, y1, x2, y2);
        surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
        recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
        surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
        if ((model.isandroidtv || model.keypadnavactive) && mysurf.SettingsXmin < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
          RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
          surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
        }
        surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

        mabase += model.GenericCaseH + model.GenericInterSpace;
        if (mabase > mysurf.myheight)
          return;

      }

    }

  }

  private void menuzoom(Canvas surfacecanvas, int foldercount) {
    if (mysurf.zoommode == Gallery.ZoomKeep) {
      ecritoption = String.format("keep %.2fx", mysurf.bscale);
    } else if (mysurf.zoommode == Gallery.ZoomFitToWindow) {
      ecritoption =  String.format("fit %.2fx", mysurf.bscale); // fit window each time for slideshow
    } else if (mysurf.zoommode == Gallery.ZoomComic) {
      ecritoption = String.format("comic %.2fx", mysurf.bscale); // keep same zoom between images for comic reading
    }
    y1 = mabase;
    y2 = mabase + model.GenericCaseH;
    taillex = model.Menu1TextePaint.measureText(ecritoption);
    x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
    x2 = mysurf.ScreenWidth - model.GenericInterSpace;
    if (x1 > mysurf.SettingsXmin) {
      x1 = mysurf.SettingsXmin;
    }
    ecritx = x2 - model.GenericInterSpace;
    ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
    if (clickquicknavmenu) {
      if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
        mysurf.optionshowzoom = !mysurf.optionshowzoom;
        needanotherupdate = true;
        clickquicknavmenu = false;
      }
    }
    recti = new RectF(x1, y1, x2, y2);
    surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu1BgPainta);
    recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
    surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu1BgPainto);
    if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
      RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
      surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
    }
    surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

    mabase += model.GenericCaseH + model.GenericInterSpace;
    if (mabase > mysurf.myheight)
      return;

    if (mysurf.optionshowzoom) {

      if (mysurf.zoommode == Gallery.ZoomKeep) {
        ecritoption = "zoom : keep";
      } else if (mysurf.zoommode == Gallery.ZoomFitToWindow) {
        ecritoption = "zoom : fit"; // fit window each time for slideshow
      } else if (mysurf.zoommode == Gallery.ZoomComic) {
        ecritoption = "zoom : comic"; // keep same zoom between images for comic reading
      }
      y1 = mabase;
      y2 = mabase + model.GenericCaseH;
      taillex = model.Menu1TextePaint.measureText(ecritoption);
      x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
      x2 = mysurf.ScreenWidth - model.GenericInterSpace;
      if (x1 > mysurf.SettingsXmin) {
        x1 = mysurf.SettingsXmin;
      }
      ecritx = x2 - model.GenericInterSpace;
      ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
      if (clickquicknavmenu) {
        if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
          if (mysurf.zoommode == Gallery.ZoomKeep) {
            mysurf.zoommode = Gallery.ZoomFitToWindow;
            model.message("fit strict or fit fill\nmodes available below");
            String fichiername = mysurf.mediaIndexAddress;
            if (mysurf.isincache(fichiername)) {
              float[] cachedbitmapnfo = mysurf.getcachedbitmapnfo(fichiername);
              float scalew = (((float) mysurf.ScreenWidth) / ((float) cachedbitmapnfo[0]));
              float scaleh = (((float) mysurf.ScreenHeight) / ((float) cachedbitmapnfo[1]));
              if (scalew < scaleh)
                bscale = scalew;
              else
                bscale = scaleh;
              bpx = 0.0f;
              bpy = 0.0f;
              mysurf.bscale = bscale;
              mysurf.bpx = bpx;
              mysurf.bpy = bpy;
              mysurf.centeronscreen = true;
            }
          } else if (mysurf.zoommode == Gallery.ZoomFitToWindow) {
            mysurf.zoommode = Gallery.ZoomComic;
            if (mysurf.zoomComicMode == Gallery.ZoomComicTopLeft)
              model.message("keep zoom\nstart top left");
            else
              model.message("keep zoom\nstart top right");
            String fichiername = mysurf.mediaIndexAddress;
            if (mysurf.isincache(fichiername)) {
              float[] cachedbitmapnfo = mysurf.getcachedbitmapnfo(fichiername);
              float scalew = (((float) mysurf.ScreenWidth) / ((float) cachedbitmapnfo[0]));
              //float scaleh = (((float) mysurf.ScreenHeight) / ((float) cachedbitmapnfo[1]));
              bscale = scalew;
              float newwidthwillbe = cachedbitmapnfo[0] * bscale;
              if (mysurf.zoomComicMode == Gallery.ZoomComicTopLeft)
                bpx = 0.0f;
              else
                bpx = mysurf.mywidth - newwidthwillbe;
              bpy = 0.0f;
              mysurf.bscale = bscale;
              mysurf.bpx = bpx;
              mysurf.bpy = bpy;
            }
          } else if (mysurf.zoommode == Gallery.ZoomComic) {
            mysurf.zoommode = Gallery.ZoomKeep;
            model.message("keep frame size");
          }
          needanotherupdate = true;
          clickquicknavmenu = false;
        }
      }
      recti = new RectF(x1, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
      recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
      if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
        RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
        surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
      }
      surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

      mabase += model.GenericCaseH + model.GenericInterSpace;
      if (mabase > mysurf.myheight)
        return;

      ecritoption = "fit width";
      y1 = mabase;
      y2 = mabase + model.GenericCaseH;
      taillex = model.Menu1TextePaint.measureText(ecritoption);
      x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
      x2 = mysurf.ScreenWidth - model.GenericInterSpace;
      if (x1 > mysurf.SettingsXmin) {
        x1 = mysurf.SettingsXmin;
      }
      ecritx = x2 - model.GenericInterSpace;
      ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
      if (clickquicknavmenu) {
        if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
          String fichiername = mysurf.mediaIndexAddress;
          if (mysurf.isincache(fichiername)) {
            float[] cachedbitmapnfo = mysurf.getcachedbitmapnfo(fichiername);
            float scalew = (((float) mysurf.ScreenWidth) / ((float) cachedbitmapnfo[0]));
            //float scaleh = (((float) mysurf.ScreenHeight) / ((float) cachedbitmapnfo[1]));
            bscale = scalew;
            bpx = 0.0f;
            bpy = 0.0f;
            mysurf.bscale = bscale;
            mysurf.bpx = bpx;
            mysurf.bpy = bpy;
          }
          needanotherupdate = true;
          clickquicknavmenu = false;
        }
      }
      recti = new RectF(x1, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
      recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
      if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
        RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
        surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
      }
      surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

      mabase += model.GenericCaseH + model.GenericInterSpace;
      if (mabase > mysurf.myheight)
        return;

      y1 = mabase;
      y2 = mabase + model.GenericCaseH;
      ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;

      ecritoption = "+";
      taillex = model.Menu1TextePaint.measureText(ecritoption);
      x1 = (mysurf.ScreenWidth - model.GenericInterSpace + mysurf.SettingsXmin) * 0.5f + model.GenericInterSpace * 0.5f;
      x2 = mysurf.ScreenWidth - model.GenericInterSpace;
      ecritx = x2;
      if (clickquicknavmenu) {
        if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
          float centerscreenx = mysurf.mywidth * 0.5f;
          float centerscreeny = mysurf.myheight * 0.5f;
          float ratio = 1.0f + zoompower[zoompoweri];
          mysurf.bpx = centerscreenx - ratio * (centerscreenx - mysurf.bpx);
          mysurf.bpy = centerscreeny - ratio * (centerscreeny - mysurf.bpy);
          mysurf.bscale *= ratio;
          needanotherupdate = true;
          clickquicknavmenu = false;
        }
      }
      recti = new RectF(x1, y1, x2, y2);
      //surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
      //recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
      if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
        RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
        surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
      }
      surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

      ecritoption = "-";
      taillex = model.Menu1TextePaint.measureText(ecritoption);
      x2 = x1 - model.GenericInterSpace;
      x1 = x2 - model.GenericInterSpace * 2.0f - taillex;
      if (x1 > mysurf.SettingsXmin)
        x1 = mysurf.SettingsXmin;
      ecritx = x2 - model.GenericInterSpace;
      if (clickquicknavmenu) {
        if (mysurf.SettingsXmin < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
          float centerscreenx = mysurf.mywidth * 0.5f;
          float centerscreeny = mysurf.myheight * 0.5f;
          float ratio = 1.0f - zoompower[zoompoweri];
          mysurf.bpx = centerscreenx - ratio * (centerscreenx - mysurf.bpx);
          mysurf.bpy = centerscreeny - ratio * (centerscreeny - mysurf.bpy);
          mysurf.bscale *= ratio;
          needanotherupdate = true;
          clickquicknavmenu = false;
        }
      }
      recti = new RectF(x1, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
      recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
      if ((model.isandroidtv || model.keypadnavactive) && mysurf.SettingsXmin < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
        RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
        surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
      }
      surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

      mabase += model.GenericCaseH + model.GenericInterSpace;
      if (mabase > mysurf.myheight)
        return;

      ecritoption = String.format("+/- %.3f", zoompower[zoompoweri]);
      y1 = mabase;
      y2 = mabase + model.GenericCaseH;
      taillex = model.Menu1TextePaint.measureText(ecritoption);
      x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
      x2 = mysurf.ScreenWidth - model.GenericInterSpace;
      if (x1 > mysurf.SettingsXmin) {
        x1 = mysurf.SettingsXmin;
      }
      ecritx = x2 - model.GenericInterSpace;
      ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
      if (clickquicknavmenu) {
        if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
          zoompoweri = (zoompower.length + zoompoweri + 1) % zoompower.length;
          needanotherupdate = true;
          clickquicknavmenu = false;
        }
      }
      recti = new RectF(x1, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
      recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
      if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
        RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
        surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
      }
      surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

      mabase += model.GenericCaseH + model.GenericInterSpace;
      if (mabase > mysurf.myheight)
        return;

      ecritoption = "center";
      y1 = mabase;
      y2 = mabase + model.GenericCaseH;
      taillex = model.Menu1TextePaint.measureText(ecritoption);
      x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
      x2 = mysurf.ScreenWidth - model.GenericInterSpace;
      if (x1 > mysurf.SettingsXmin) {
        x1 = mysurf.SettingsXmin;
      }
      ecritx = x2 - model.GenericInterSpace;
      ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
      if (clickquicknavmenu) {
        if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
          mysurf.centeronscreen = true;
          needanotherupdate = true;
          clickquicknavmenu = false;
        }
      }
      recti = new RectF(x1, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
      recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
      if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
        RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
        surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
      }
      surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

      mabase += model.GenericCaseH + model.GenericInterSpace;
      if (mabase > mysurf.myheight)
        return;

      if (model.forceatvmode || model.isandroidtv) {
        ecritoption = "tv mode on";
      } else {
        ecritoption = "tv mode off"; // fit window each time for slideshow
      }
      y1 = mabase;
      y2 = mabase + model.GenericCaseH;
      taillex = model.Menu1TextePaint.measureText(ecritoption);
      x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
      x2 = mysurf.ScreenWidth - model.GenericInterSpace;
      if (x1 > mysurf.SettingsXmin) {
        x1 = mysurf.SettingsXmin;
      }
      ecritx = x2 - model.GenericInterSpace;
      ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
      if (clickquicknavmenu) {
        if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
          model.forceatvmode = !model.forceatvmode;
          model.isandroidtv = model.forceatvmode;
          if (model.forceatvmode) {
            model.message("AndroidTV mode enabled (forced).");
          } else {
            model.message("AndroidTV mode disabled.");
          }
          model.preferences.edit().putBoolean("forceatvmode", model.forceatvmode).apply();
          model.setallthepaints();
          needanotherupdate = true;
          clickquicknavmenu = false;
        }
      }
      recti = new RectF(x1, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
      recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
      if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
        RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
        surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
      }
      surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

      mabase += model.GenericCaseH + model.GenericInterSpace;
      if (mabase > mysurf.myheight)
        return;


      //if (mysurf.zoommode == Gallery.ZoomFitToWindow) {
      if (mysurf.zoomFitMode == Gallery.ZoomFitStrict)
        ecritoption = "fit strict";
      else if (mysurf.zoomFitMode == Gallery.ZoomFitFill)
        ecritoption = "fit fill";
      y1 = mabase;
      y2 = mabase + model.GenericCaseH;
      taillex = model.Menu1TextePaint.measureText(ecritoption);
      x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
      x2 = mysurf.ScreenWidth - model.GenericInterSpace;
      if (x1 > mysurf.SettingsXmin) {
        x1 = mysurf.SettingsXmin;
      }
      ecritx = x2 - model.GenericInterSpace;
      ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
      if (clickquicknavmenu) {
        if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
          if (mysurf.zoomFitMode == Gallery.ZoomFitStrict) {
            mysurf.zoomFitMode = Gallery.ZoomFitFill;
            model.message("try to fit height\nsome overflow allowed");
            String fichiername = mysurf.mediaIndexAddress;
            if (mysurf.isincache(fichiername)) {
              float[] cachedbitmapnfo = mysurf.getcachedbitmapnfo(fichiername);
              float scalew = (((float) mysurf.ScreenWidth) / ((float) cachedbitmapnfo[0]));
              float scaleh = (((float) mysurf.ScreenHeight) / ((float) cachedbitmapnfo[1]));
              if (mysurf.ScreenWidth >= mysurf.ScreenHeight) {
                if (cachedbitmapnfo[0] >= cachedbitmapnfo[1]) {
                  if (scalew > scaleh)
                    bscale = scalew;
                  else
                    bscale = scaleh;
                } else {
                  if (scalew < scaleh)
                    bscale = scalew;
                  else
                    bscale = scaleh;
                }
              } else {
                if (cachedbitmapnfo[0] < cachedbitmapnfo[1]) {
                  if (scalew > scaleh)
                    bscale = scalew;
                  else
                    bscale = scaleh;
                } else {
                  if (scalew < scaleh)
                    bscale = scalew;
                  else
                    bscale = scaleh;
                }
              }
              bpx = 0.0f;
              bpy = 0.0f;
              mysurf.bscale = bscale;
              mysurf.bpx = bpx;
              mysurf.bpy = bpy;
              mysurf.centeronscreen = true;
            }
          } else if (mysurf.zoomFitMode == Gallery.ZoomFitFill) {
            mysurf.zoomFitMode = Gallery.ZoomFitStrict;
            model.message();
            String fichiername = mysurf.mediaIndexAddress;
            if (mysurf.isincache(fichiername)) {
              float[] cachedbitmapnfo = mysurf.getcachedbitmapnfo(fichiername);
              float scalew = (((float) mysurf.ScreenWidth) / ((float) cachedbitmapnfo[0]));
              float scaleh = (((float) mysurf.ScreenHeight) / ((float) cachedbitmapnfo[1]));
              if (scalew < scaleh)
                bscale = scalew;
              else
                bscale = scaleh;
              bpx = 0.0f;
              bpy = 0.0f;
              mysurf.bscale = bscale;
              mysurf.bpx = bpx;
              mysurf.bpy = bpy;
              mysurf.centeronscreen = true;
            }
          }
          model.preferences.edit().putInt("zoomFitMode", mysurf.zoomFitMode).apply();
          needanotherupdate = true;
          clickquicknavmenu = false;
        }
      }
      recti = new RectF(x1, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
      recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
      if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
        RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
        surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
      }
      surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

      mabase += model.GenericCaseH + model.GenericInterSpace;
      if (mabase > mysurf.myheight)
        return;
      //}


      if (mysurf.zoomComicMode == Gallery.ZoomComicTopLeft)
        ecritoption = "top left";
      else if (mysurf.zoomComicMode == Gallery.ZoomComicTopRight)
        ecritoption = "top right";
      y1 = mabase;
      y2 = mabase + model.GenericCaseH;
      taillex = model.Menu1TextePaint.measureText(ecritoption);
      x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
      x2 = mysurf.ScreenWidth - model.GenericInterSpace;
      if (x1 > mysurf.SettingsXmin) {
        x1 = mysurf.SettingsXmin;
      }
      ecritx = x2 - model.GenericInterSpace;
      ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
      if (clickquicknavmenu) {
        if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
          if (mysurf.zoomComicMode == Gallery.ZoomComicTopLeft) {
            mysurf.zoomComicMode = Gallery.ZoomComicTopRight;
            model.message("start top right");
            String fichiername = mysurf.mediaIndexAddress;
            if (mysurf.isincache(fichiername)) {
              float[] cachedbitmapnfo = mysurf.getcachedbitmapnfo(fichiername);
              float newwidthwillbe = cachedbitmapnfo[0] * bscale;
              if (newwidthwillbe < mysurf.mywidth)
                bpx = (mysurf.mywidth - newwidthwillbe) * 0.5f;
              else
                bpx = mysurf.mywidth - newwidthwillbe;
              bpy = 0.0f;
              mysurf.bpx = bpx;
              mysurf.bpy = bpy;
            }
          } else if (mysurf.zoomComicMode == Gallery.ZoomComicTopRight) {
            mysurf.zoomComicMode = Gallery.ZoomComicTopLeft;
            model.message("start top left");
            String fichiername = mysurf.mediaIndexAddress;
            if (mysurf.isincache(fichiername)) {
              float[] cachedbitmapnfo = mysurf.getcachedbitmapnfo(fichiername);
              float newwidthwillbe = cachedbitmapnfo[0] * bscale;
              if (newwidthwillbe < mysurf.mywidth)
                bpx = (mysurf.mywidth - newwidthwillbe) * 0.5f;
              else
                bpx = 0.0f;
              bpy = 0.0f;
              mysurf.bpx = bpx;
              mysurf.bpy = bpy;
            }
          }
          model.preferences.edit().putInt("zoomComicMode", mysurf.zoomComicMode).apply();
          needanotherupdate = true;
          clickquicknavmenu = false;
        }
      }
      recti = new RectF(x1, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
      recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
      if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
        RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
        surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
      }
      surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

      mabase += model.GenericCaseH + model.GenericInterSpace;
      if (mabase > mysurf.myheight)
        return;

    }
  }

  private float lastyp = 0.0f;
  private float lastym = 0.0f;
  private void menubuttonsize(Canvas surfacecanvas, int foldercount) {
      y1 = mabase;
      y2 = mabase + model.GenericCaseH;
      ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;

      if (lastyp != 0.0f) {
        mysurf.SettingsYmin -= (y2 - lastyp);
        lastyp = 0.0f;
      }
      if (lastym != 0.0f) {
        mysurf.SettingsYmin -= (y2 - lastym);
        lastym = 0.0f;
      }

      ecritoption = "+";
      taillex = model.Menu1TextePaint.measureText(ecritoption);
      x1 = (mysurf.ScreenWidth - model.GenericInterSpace + mysurf.SettingsXmin) * 0.5f + model.GenericInterSpace * 0.5f;
      x2 = mysurf.ScreenWidth - model.GenericInterSpace;
      ecritx = x2;
      if (clickquicknavmenu) {
        if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
          lastyp = y2;
          model.buttontextsize += 1.0f;
          model.preferences.edit().putFloat("buttontextsize", model.buttontextsize).apply();
          model.setallthepaints();
          needanotherupdate = true;
          clickquicknavmenu = false;
        }
      }
      recti = new RectF(x1, y1, x2, y2);
      //surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
      //recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu1BgPainto);
      if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
        RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
        surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
      }
      surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

      ecritoption = String.format("button size %.0fpx -", model.buttontextsize);
      taillex = model.Menu1TextePaint.measureText(ecritoption);
      x2 = x1 - model.GenericInterSpace;
      x1 = x2 - model.GenericInterSpace * 2.0f - taillex;
      if (x1 > mysurf.SettingsXmin)
        x1 = mysurf.SettingsXmin;
      ecritx = x2 - model.GenericInterSpace;
      if (clickquicknavmenu) {
        if (mysurf.SettingsXmin < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
          lastym = y2;
          if (model.buttontextsize > 1.0f) {
            model.buttontextsize -= 1.0f;
          } else {
            model.buttontextsize = 1.0f;
          }
          model.preferences.edit().putFloat("buttontextsize", model.buttontextsize).apply();
          model.setallthepaints();
          needanotherupdate = true;
          clickquicknavmenu = false;
        }
      }
      recti = new RectF(x1, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu1BgPainta);
      recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu1BgPainto);
      if ((model.isandroidtv || model.keypadnavactive) && mysurf.SettingsXmin < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
        RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
        surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
      }
      surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

      mabase += model.GenericCaseH + model.GenericInterSpace;
      if (mabase > mysurf.myheight)
        return;

  }

  private void menufilmstriptime(Canvas surfacecanvas, int foldercount) {
    y1 = mabase;
    y2 = mabase + model.GenericCaseH;
    ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;

    ecritoption = "+";
    taillex = model.Menu1TextePaint.measureText(ecritoption);
    x1 = (mysurf.ScreenWidth - model.GenericInterSpace + mysurf.SettingsXmin) * 0.5f + model.GenericInterSpace * 0.5f;
    x2 = mysurf.ScreenWidth - model.GenericInterSpace;
    ecritx = x2;
    if (clickquicknavmenu) {
      if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
        model.filmstripanimatetime += 500L;
        model.preferences.edit().putLong("filmstripanimatetime", model.filmstripanimatetime).apply();
        needanotherupdate = true;
        clickquicknavmenu = false;
      }
    }
    recti = new RectF(x1, y1, x2, y2);
    //surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu1BgPainta);
    //recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
    surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu1BgPainto);
    if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
      RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
      surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
    }
    surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

    ecritoption = String.format("animate %.1fs -", ((float) model.filmstripanimatetime) / 1000.0f);
    taillex = model.Menu1TextePaint.measureText(ecritoption);
    x2 = x1 - model.GenericInterSpace;
    x1 = x2 - model.GenericInterSpace * 2.0f - taillex;
      if (x1 > mysurf.SettingsXmin)
        x1 = mysurf.SettingsXmin;
    ecritx = x2 - model.GenericInterSpace;
    if (clickquicknavmenu) {
      if (mysurf.SettingsXmin < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
        if (model.filmstripanimatetime > 500L) {
          model.filmstripanimatetime -= 500L;
        } else {
          model.filmstripanimatetime = 500L;
        }
        model.preferences.edit().putLong("filmstripanimatetime", model.filmstripanimatetime).apply();
        needanotherupdate = true;
        clickquicknavmenu = false;
      }
    }
    recti = new RectF(x1, y1, x2, y2);
    surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu1BgPainta);
    recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
    surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu1BgPainto);
    if ((model.isandroidtv || model.keypadnavactive) && mysurf.SettingsXmin < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
      RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
      surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
    }
    surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

    mabase += model.GenericCaseH + model.GenericInterSpace;
    if (mabase > mysurf.myheight)
      return;

  }

  private void menuvideotemps(Canvas surfacecanvas, int foldercount) {
    ecritoption = "top ten";
    y1 = mabase;
    y2 = mabase + model.GenericCaseH;
    taillex = model.Menu1TextePaint.measureText(ecritoption);
    x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
    x2 = mysurf.ScreenWidth - model.GenericInterSpace;
    if (x1 > mysurf.SettingsXmin) {
      x1 = mysurf.SettingsXmin;
    }
    ecritx = x2 - model.GenericInterSpace;
    ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
    if (clickquicknavmenu) {
      if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
        mysurf.optionshowtopten = !mysurf.optionshowtopten;
        if (mysurf.optionshowtopten) {
          if (videotempsclassement == null) {
            videotempsclassement = new Gallery.Videotemps[10];
          }
          for (int i = 0; i < 10; i++) {
            videotempsclassement[i] = model.findlongestvideotemps(i + 1);
            if (videotempsclassement[i] == null)
              break;
            if (i > 0) {
              if (videotempsclassement[i].fichiername.equals(videotempsclassement[i - 1].fichiername)) {
                videotempsclassement[i] = null;
                break;
              }
            }
            //llog.d(TAG, "videotempsclassement " + i + " : " + videotempsclassement[i].totaltime + " " + videotempsclassement[i].fichiername);
          }
        }
        needanotherupdate = true;
        clickquicknavmenu = false;
      }
    }
    recti = new RectF(x1, y1, x2, y2);
    surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu1BgPainta);
    recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
    surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu1BgPainto);
    if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
      RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
      surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
    }
    surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

    mabase += model.GenericCaseH + model.GenericInterSpace;
    if (mabase > mysurf.myheight)
      return;

    if (mysurf.optionshowtopten) {

      int surfl = model.surf.size();
      Surf thissurf;
      for (int i = 0 ; i < surfl ; i++) {
        thissurf = model.surf.get(i);
        if (thissurf != null) {
          MPVLib mympvlib = thissurf.mpvlib;
          if (mympvlib != null) {
            if (mympvlib.lastfolderadded != null && mympvlib.lastfileadded != null) {
              ecritoption = mympvlib.lastfileadded + " " + i;
              y1 = mabase;
              y2 = mabase + model.GenericCaseH;
              taillex = model.Menu1TextePaint.measureText(ecritoption);
              x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
              x2 = mysurf.ScreenWidth - model.GenericInterSpace;
              if (x1 > mysurf.SettingsXmin) {
                x1 = mysurf.SettingsXmin;
              }
              ecritx = x2 - model.GenericInterSpace;
              ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
              if (clickquicknavmenu) {
                if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
                  int d = model.findFolder(mympvlib.lastfolderadded);
                  int f = model.findMediaAddress(d, mympvlib.lastfileadded);
                  if (d == -1 || f == -1)
                    llog.d(TAG, "folder file not found " + d + " " + f + " " + mympvlib.lastfileadded + " " + mympvlib.lastfolderadded);
                  else {
                    model.changeBigPicture(model.surfzappercommander, d, 0, f, 0, true, false);
                    mysurf.centeronscreen = true;
                  }
                  //model.changeBigPicture(model.surfzappercommander, videotempsclassement[i].dossierprincipal, 0, videotempsclassement[i].fichierprincipal, 0, true, false);
                  needanotherupdate = true;
                  clickquicknavmenu = false;
                }
              }
              recti = new RectF(x1, y1, x2, y2);
              surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
              recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
              surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
              if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
                RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
                surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
              }
              surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

              mabase += model.GenericCaseH + model.GenericInterSpace;
              if (mabase > mysurf.myheight)
                return;
            }
          }
        }
      }

      if (videotempsclassement != null) {
        for (int i = 0 ; i < 10 ; i++) {
          if (videotempsclassement[i] != null) {
            ecritoption = String.format("%s %ds", videotempsclassement[i].fichiername, videotempsclassement[i].totaltime / 1000L);
            y1 = mabase;
            y2 = mabase + model.GenericCaseH;
            taillex = model.Menu1TextePaint.measureText(ecritoption);
            x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
            x2 = mysurf.ScreenWidth - model.GenericInterSpace;
            if (x1 > mysurf.SettingsXmin) {
              x1 = mysurf.SettingsXmin;
            }
            ecritx = x2 - model.GenericInterSpace;
            ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
            if (clickquicknavmenu) {
              if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
                model.changeBigPicture(model.surfzappercommander, videotempsclassement[i].dossierprincipal, 0, videotempsclassement[i].fichierprincipal, 0, true, false);
                needanotherupdate = true;
                clickquicknavmenu = false;
              }
            }
            recti = new RectF(x1, y1, x2, y2);
            surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
            recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
            surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
            if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
              RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
              surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
            }
            surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

            mabase += model.GenericCaseH + model.GenericInterSpace;
            if (mabase > mysurf.myheight)
              break;
          }
        }
      }
    }
  }
  private Gallery.Videotemps[] videotempsclassement = null;


  private void menuquit(Canvas surfacecanvas) {
    ecritoption = "quit";
    y1 = mabase;
    y2 = mabase + model.GenericCaseH;
    taillex = model.Menu1TextePaint.measureText(ecritoption);
    x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
    x2 = mysurf.ScreenWidth - model.GenericInterSpace;
    if (x1 > mysurf.SettingsXmin) {
      x1 = mysurf.SettingsXmin;
    }
    ecritx = x2 - model.GenericInterSpace;
    ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
    if (clickquicknavmenu) {
      if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
        if (mysurf.optiondiaporamaactive) {
          mysurf.optiondiaporamaactive = false;
        }
        Intent intent = new Intent();
        intent.setAction(Gallery.broadcastname);
        intent.putExtra("goal", "quit");
        intent.putExtra("id", currid);
        LocalBroadcastManager.getInstance(model.activitycontext).sendBroadcast(intent);
        needanotherupdate = true;
        clickquicknavmenu = false;
      }
    }
    recti = new RectF(x1, y1, x2, y2);
    surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu1BgPainta);
    recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
    surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu1BgPainto);
    if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
      RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
      surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
    }
    surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

    mabase += model.GenericCaseH + model.GenericInterSpace;
  }

  private void menumodify(Canvas surfacecanvas, int foldercount) {
    ecritoption = "modify";
    y1 = mabase;
    y2 = mabase + model.GenericCaseH;
    taillex = model.Menu1TextePaint.measureText(ecritoption);
    x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
    x2 = mysurf.ScreenWidth - model.GenericInterSpace;
    if (x1 > mysurf.SettingsXmin) {
      x1 = mysurf.SettingsXmin;
    }
    ecritx = x2 - model.GenericInterSpace;
    ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
    if (clickquicknavmenu) {
      if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
        mysurf.optionshowmodifypic = !mysurf.optionshowmodifypic;
        //mysurf.cropw = false;
        //mysurf.croph = false;
        needanotherupdate = true;
        clickquicknavmenu = false;
      }
    }
    recti = new RectF(x1, y1, x2, y2);
    surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu1BgPainta);
    recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
    surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu1BgPainto);
    if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
      RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
      surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
    }
    surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

    mabase += model.GenericCaseH + model.GenericInterSpace;
    if (mabase > mysurf.myheight)
      return;

    if (mysurf.optionshowmodifypic) {

      if (!mysurf.optionshowsubcomment) {

        ecritoption = "comment";
        y1 = mabase;
        y2 = mabase + model.GenericCaseH;
        taillex = model.Menu1TextePaint.measureText(ecritoption);
        x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
        x2 = mysurf.ScreenWidth - model.GenericInterSpace;
        if (x1 > mysurf.SettingsXmin) {
          x1 = mysurf.SettingsXmin;
        }
        ecritx = x2 - model.GenericInterSpace;
        ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
        if (clickquicknavmenu) {
          if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
            mysurf.optionshowsubcomment = !mysurf.optionshowsubcomment;
            needanotherupdate = true;
            clickquicknavmenu = false;
          }
        }
        recti = new RectF(x1, y1, x2, y2);
        surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
        recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
        surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
        if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
          RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
          surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
        }
        surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

        mabase += model.GenericCaseH + model.GenericInterSpace;
        if (mabase > mysurf.myheight)
          return;

      } else {

        ecritoption = "-";
        y1 = mabase;
        y2 = mabase + model.GenericCaseH;
        taillex = model.Menu1TextePaint.measureText(ecritoption);
        x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
        x2 = mysurf.ScreenWidth - model.GenericInterSpace;
        if (x1 > mysurf.SettingsXmin) {
          x1 = mysurf.SettingsXmin;
        }
        ecritx = x2 - model.GenericInterSpace;
        ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
        if (clickquicknavmenu) {
          if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
            mysurf.optionshowsubcomment = !mysurf.optionshowsubcomment;
            needanotherupdate = true;
            clickquicknavmenu = false;
          }
        }
        recti = new RectF(x1, y1, x2, y2);
        surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
        recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
        surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
        if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
          RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
          surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
        }
        surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

        mabase += model.GenericCaseH + model.GenericInterSpace;
        if (mabase > mysurf.myheight)
          return;

        ecritoption = "printName";
        y1 = mabase;
        y2 = mabase + model.GenericCaseH;
        taillex = model.Menu1TextePaint.measureText(ecritoption);
        x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
        x2 = mysurf.ScreenWidth - model.GenericInterSpace;
        if (x1 > mysurf.SettingsXmin) {
          x1 = mysurf.SettingsXmin;
        }
        ecritx = x2 - model.GenericInterSpace;
        ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
        if (clickquicknavmenu) {
          if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
            Intent intent = new Intent();
            intent.setAction(Gallery.broadcastname);
            intent.putExtra("goal", "commentmedia");
            intent.putExtra("id", currid);
            intent.putExtra("d", mysurf.ordnerIndex);
            intent.putExtra("f", mysurf.mediaIndex);
            intent.putExtra("type", ecritoption);
            LocalBroadcastManager.getInstance(model.activitycontext).sendBroadcast(intent);
            needanotherupdate = true;
            clickquicknavmenu = false;
          }
        }
        recti = new RectF(x1, y1, x2, y2);
        surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
        recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
        surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
        if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
          RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
          surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
        }
        surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

        mabase += model.GenericCaseH + model.GenericInterSpace;
        if (mabase > mysurf.myheight)
          return;

        ecritoption = "printDetails";
        y1 = mabase;
        y2 = mabase + model.GenericCaseH;
        taillex = model.Menu1TextePaint.measureText(ecritoption);
        x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
        x2 = mysurf.ScreenWidth - model.GenericInterSpace;
        if (x1 > mysurf.SettingsXmin) {
          x1 = mysurf.SettingsXmin;
        }
        ecritx = x2 - model.GenericInterSpace;
        ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
        if (clickquicknavmenu) {
          if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
            Intent intent = new Intent();
            intent.setAction(Gallery.broadcastname);
            intent.putExtra("goal", "commentmedia");
            intent.putExtra("id", currid);
            intent.putExtra("d", mysurf.ordnerIndex);
            intent.putExtra("f", mysurf.mediaIndex);
            intent.putExtra("type", ecritoption);
            LocalBroadcastManager.getInstance(model.activitycontext).sendBroadcast(intent);
            needanotherupdate = true;
            clickquicknavmenu = false;
          }
        }
        recti = new RectF(x1, y1, x2, y2);
        surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
        recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
        surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
        if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
          RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
          surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
        }
        surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

        mabase += model.GenericCaseH + model.GenericInterSpace;
        if (mabase > mysurf.myheight)
          return;

        ecritoption = "printFooter";
        y1 = mabase;
        y2 = mabase + model.GenericCaseH;
        taillex = model.Menu1TextePaint.measureText(ecritoption);
        x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
        x2 = mysurf.ScreenWidth - model.GenericInterSpace;
        if (x1 > mysurf.SettingsXmin) {
          x1 = mysurf.SettingsXmin;
        }
        ecritx = x2 - model.GenericInterSpace;
        ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
        if (clickquicknavmenu) {
          if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
            Intent intent = new Intent();
            intent.setAction(Gallery.broadcastname);
            intent.putExtra("goal", "commentmedia");
            intent.putExtra("id", currid);
            intent.putExtra("d", mysurf.ordnerIndex);
            intent.putExtra("f", mysurf.mediaIndex);
            intent.putExtra("type", ecritoption);
            LocalBroadcastManager.getInstance(model.activitycontext).sendBroadcast(intent);
            needanotherupdate = true;
            clickquicknavmenu = false;
          }
        }
        recti = new RectF(x1, y1, x2, y2);
        surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
        recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
        surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
        if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
          RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
          surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
        }
        surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

        mabase += model.GenericCaseH + model.GenericInterSpace;
        if (mabase > mysurf.myheight)
          return;

      }

      if (mysurf.filterlist.length == 0) {
        int fl = (int) (Gallery.filterlist.length / 3);
        mysurf.filterprint = new String[fl];
        mysurf.filterlist = new String[fl];
        mysurf.filteractive = new boolean[fl];
        mysurf.filterinfo = new String[fl];
        for (int f = 0 ; f < fl ; f++) {
          mysurf.filterprint[f] = Gallery.filterlist[f * 3 + 0];
          mysurf.filterlist[f] = Gallery.filterlist[f * 3 + 1];
          mysurf.filterinfo[f] = Gallery.filterlist[f * 3 + 2];
          mysurf.filteractive[f] = false;
        }
      }

      boolean effects = false;
      boolean advanced = false;

      int fl = mysurf.filterlist.length;
      for (int f = 0 ; f < fl ; f++) {

        if (f == Gallery.filterrotate) {
          ecritoption = "rotate";
          y1 = mabase;
          y2 = mabase + model.GenericCaseH;
          taillex = model.Menu1TextePaint.measureText(ecritoption);
          x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
          x2 = mysurf.ScreenWidth - model.GenericInterSpace;
          if (x1 > mysurf.SettingsXmin) {
            x1 = mysurf.SettingsXmin;
          }
          ecritx = x2 - model.GenericInterSpace;
          ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
          if (clickquicknavmenu) {
            if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
              mysurf.filteractive[f] = !mysurf.filteractive[f];
              String filter = null;
                for (int g = 0; g < fl; g++) {
                  if (mysurf.filteractive[g]) {
                    if (filter == null)
                      filter = mysurf.filterlist[g];
                    else
                      filter += "," + mysurf.filterlist[g];
                  }
                }
                mysurf.filter = filter;
                try {
                  model.commandebigimagethreadqueue.put(new String[]{"updatebigpicture", String.valueOf(currid), String.valueOf(mysurf.ordnerIndex), String.valueOf(mysurf.mediaIndex), String.valueOf(mysurf.bigimagecurrentlydisplayed), String.valueOf(false), "0"});
                } catch (InterruptedException e) {
                  e.printStackTrace();
                }
              needanotherupdate = true;
              clickquicknavmenu = false;
            }
          }
          recti = new RectF(x1, y1, x2, y2);
          surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
          recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
          surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
          if (mysurf.filteractive[f]) {
            RectF rectisel = new RectF(x1 + quatredixiememillimetre, y1 + quatredixiememillimetre, x2 - quatredixiememillimetre, y2 - quatredixiememillimetre);
            surfacecanvas.drawRoundRect(rectisel, roundedRectRatio, roundedRectRatio, model.SettingsSelPaint);
          }
          if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
            RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
            surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
          }
          surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

          mabase += model.GenericCaseH + model.GenericInterSpace;
          if (mabase > mysurf.myheight)
            return;

        } else if (f == Gallery.filterhflipvflip) {
          // 1 hflip 2 vflip

          y1 = mabase;
          y2 = mabase + model.GenericCaseH;
          ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;

          ecritoption = "v ";
          taillex = model.Menu1TextePaint.measureText(ecritoption);
          x1 = (mysurf.ScreenWidth - model.GenericInterSpace + mysurf.SettingsXmin) * 0.5f + model.GenericInterSpace * 0.5f;
          x2 = mysurf.ScreenWidth - model.GenericInterSpace;
          ecritx = x2;
          if (clickquicknavmenu) {
            if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
              mysurf.filteractive[f+1] = !mysurf.filteractive[f+1];
              String filter = null;
              for (int g = 0; g < fl; g++) {
                if (mysurf.filteractive[g]) {
                  if (filter == null)
                    filter = mysurf.filterlist[g];
                  else
                    filter += "," + mysurf.filterlist[g];
                }
              }
              mysurf.filter = filter;
              try {
                model.commandebigimagethreadqueue.put(new String[]{"updatebigpicture", String.valueOf(currid), String.valueOf(mysurf.ordnerIndex), String.valueOf(mysurf.mediaIndex), String.valueOf(mysurf.bigimagecurrentlydisplayed), String.valueOf(false), "0"});
              } catch (InterruptedException e) {
                e.printStackTrace();
              }
              needanotherupdate = true;
              clickquicknavmenu = false;
            }
          }
          recti = new RectF(x1, y1, x2, y2);
          //surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
          //recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
          surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
          if (mysurf.filteractive[f+1]) {
            RectF rectisel = new RectF(x1 + quatredixiememillimetre, y1 + quatredixiememillimetre, x2 - quatredixiememillimetre, y2 - quatredixiememillimetre);
            surfacecanvas.drawRoundRect(rectisel, roundedRectRatio, roundedRectRatio, model.SettingsSelPaint);
          }
          if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
            RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
            surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
          }
          surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

          ecritoption = "flip h";
          taillex = model.Menu1TextePaint.measureText(ecritoption);
          x2 = x1 - model.GenericInterSpace;
          x1 = x2 - model.GenericInterSpace * 2.0f - taillex;
          if (x1 > mysurf.SettingsXmin)
            x1 = mysurf.SettingsXmin;
          ecritx = x2 - model.GenericInterSpace;
          if (clickquicknavmenu) {
            if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
              mysurf.filteractive[f] = !mysurf.filteractive[f];
              String filter = null;
              for (int g = 0; g < fl; g++) {
                if (mysurf.filteractive[g]) {
                  if (filter == null)
                    filter = mysurf.filterlist[g];
                  else
                    filter += "," + mysurf.filterlist[g];
                }
              }
              mysurf.filter = filter;
              try {
                model.commandebigimagethreadqueue.put(new String[]{"updatebigpicture", String.valueOf(currid), String.valueOf(mysurf.ordnerIndex), String.valueOf(mysurf.mediaIndex), String.valueOf(mysurf.bigimagecurrentlydisplayed), String.valueOf(false), "0"});
              } catch (InterruptedException e) {
                e.printStackTrace();
              }
              needanotherupdate = true;
              clickquicknavmenu = false;
            }
          }
          recti = new RectF(x1, y1, x2, y2);
          surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
          recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
          surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
          if (mysurf.filteractive[f]) {
            RectF rectisel = new RectF(x1 + quatredixiememillimetre, y1 + quatredixiememillimetre, x2 - quatredixiememillimetre, y2 - quatredixiememillimetre);
            surfacecanvas.drawRoundRect(rectisel, roundedRectRatio, roundedRectRatio, model.SettingsSelPaint);
          }
          if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
            RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
            surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
          }
          surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

          mabase += model.GenericCaseH + model.GenericInterSpace;
          if (mabase > mysurf.myheight)
            return;

        } else if (f == Gallery.filtervflip) { // vflip

        } else if (!mysurf.optionshowsubeffect && Gallery.filterbcsgwi <= f && f <= Gallery.filterbcsgwf) {

            if (!effects) {
              effects = true;
              ecritoption = "effects";
              y1 = mabase;
              y2 = mabase + model.GenericCaseH;
              taillex = model.Menu1TextePaint.measureText(ecritoption);
              x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
              x2 = mysurf.ScreenWidth - model.GenericInterSpace;
              if (x1 > mysurf.SettingsXmin) {
                x1 = mysurf.SettingsXmin;
              }
              ecritx = x2 - model.GenericInterSpace;
              ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
              if (clickquicknavmenu) {
                if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
                  mysurf.optionshowsubeffect = !mysurf.optionshowsubeffect;
                  needanotherupdate = true;
                  clickquicknavmenu = false;
                }
              }
              recti = new RectF(x1, y1, x2, y2);
              surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
              recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
              surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
              if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
                RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
                surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
              }
              surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

              mabase += model.GenericCaseH + model.GenericInterSpace;
              if (mabase > mysurf.myheight)
                return;
            }

        } else if (mysurf.optionshowsubeffect && Gallery.filterbcsgwi <= f && f <= Gallery.filterbcsgwf) {

          if (!effects) {
            effects = true;
            ecritoption = "-";
            y1 = mabase;
            y2 = mabase + model.GenericCaseH;
            taillex = model.Menu1TextePaint.measureText(ecritoption);
            x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
            x2 = mysurf.ScreenWidth - model.GenericInterSpace;
            if (x1 > mysurf.SettingsXmin) {
              x1 = mysurf.SettingsXmin;
            }
            ecritx = x2 - model.GenericInterSpace;
            ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
            if (clickquicknavmenu) {
              if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
                mysurf.optionshowsubeffect = !mysurf.optionshowsubeffect;
                needanotherupdate = true;
                clickquicknavmenu = false;
              }
            }
            recti = new RectF(x1, y1, x2, y2);
            surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
            recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
            surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
            if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
              RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
              surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
            }
            surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

            mabase += model.GenericCaseH + model.GenericInterSpace;
            if (mabase > mysurf.myheight)
              return;
          }

              y1 = mabase;
              y2 = mabase + model.GenericCaseH;
              ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;

              ecritoption = "+ ";
              taillex = model.Menu1TextePaint.measureText(ecritoption);
              x1 = (mysurf.ScreenWidth - model.GenericInterSpace + mysurf.SettingsXmin) * 0.5f + model.GenericInterSpace * 0.5f;
              x2 = mysurf.ScreenWidth - model.GenericInterSpace;
              ecritx = x2;
              if (clickquicknavmenu) {
                if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
                  if (f == Gallery.filterb) {
                    mysurf.brightness += mysurf.deltabright[mysurf.deltabrighti];
                    if (-0.001 < mysurf.brightness && mysurf.brightness < 0.001) {
                      mysurf.filteractive[f] = false;
                    } else {
                      mysurf.filteractive[f] = true;
                      mysurf.filterlist[f] = String.format("eq=brightness=%.3f", mysurf.brightness);
                    }
                  } else if (f == Gallery.filterc) {
                    mysurf.contrast += mysurf.deltabright[mysurf.deltabrighti];
                    if (0.999 < mysurf.contrast && mysurf.contrast < 1.001) {
                      mysurf.filteractive[f] = false;
                    } else {
                      mysurf.filteractive[f] = true;
                      mysurf.filterlist[f] = String.format("eq=contrast=%.3f", mysurf.contrast);
                    }
                  } else if (f == Gallery.filters) {
                    mysurf.saturation += mysurf.deltabright[mysurf.deltabrighti];
                    if (0.999 < mysurf.saturation && mysurf.saturation < 1.001) {
                      mysurf.filteractive[f] = false;
                    } else {
                      mysurf.filteractive[f] = true;
                      mysurf.filterlist[f] = String.format("eq=saturation=%.3f", mysurf.saturation);
                    }
                  } else if (f == Gallery.filterg) {
                    mysurf.gamma += mysurf.deltabright[mysurf.deltabrighti];
                    if (0.999 < mysurf.gamma && mysurf.gamma < 1.001) {
                      mysurf.filteractive[f] = false;
                    } else {
                      mysurf.filteractive[f] = true;
                      mysurf.filterlist[f] = String.format("eq=gamma=%.3f", mysurf.gamma);
                    }
                  } else if (f == Gallery.filterw) {
                    mysurf.gamma_weight += mysurf.deltabright[mysurf.deltabrighti];
                    if (0.999 < mysurf.gamma_weight && mysurf.gamma_weight < 1.001) {
                      mysurf.filteractive[f] = false;
                    } else {
                      mysurf.filteractive[f] = true;
                      mysurf.filterlist[f] = String.format("eq=gamma_weight=%.3f", mysurf.gamma_weight);
                    }
                  }
                  String filter = null;
                  for (int g = 0; g < fl; g++) {
                    if (mysurf.filteractive[g]) {
                      if (filter == null)
                        filter = mysurf.filterlist[g];
                      else
                        filter += "," + mysurf.filterlist[g];
                    }
                  }
                  mysurf.filter = filter;
                  try {
                    model.commandebigimagethreadqueue.put(new String[]{"updatebigpicture", String.valueOf(currid), String.valueOf(mysurf.ordnerIndex), String.valueOf(mysurf.mediaIndex), String.valueOf(mysurf.bigimagecurrentlydisplayed), String.valueOf(false), "0"});
                  } catch (InterruptedException e) {
                    e.printStackTrace();
                  }
                  needanotherupdate = true;
                  clickquicknavmenu = false;
                }
              }
              recti = new RectF(x1, y1, x2, y2);
              //surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
              //recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
              surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
              if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
                RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
                surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
              }
              surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);


              if (f == Gallery.filterb) {
                if (mysurf.filteractive[f])
                  ecritoption = String.format("brightness %.3f -", mysurf.brightness);
                else
                  ecritoption = "brightness -";
              } else if (f == Gallery.filterc) {
                if (mysurf.filteractive[f])
                  ecritoption = String.format("contrast %.3f -", mysurf.contrast);
                else
                  ecritoption = "contrast -";
              } else if (f == Gallery.filters) {
                if (mysurf.filteractive[f])
                  ecritoption = String.format("saturation %.3f -", mysurf.saturation);
                else
                  ecritoption = "saturation -";
              } else if (f == Gallery.filterg) {
                if (mysurf.filteractive[f])
                  ecritoption = String.format("gamma %.3f -", mysurf.gamma);
                else
                  ecritoption = "gamma -";
              } else if (f == Gallery.filterw) {
                if (mysurf.filteractive[f])
                  ecritoption = String.format("gammaweight %.3f -", mysurf.gamma_weight);
                else
                  ecritoption = "gammaweight -";
              }
              taillex = model.Menu1TextePaint.measureText(ecritoption);
              x2 = x1 - model.GenericInterSpace;
              x1 = x2 - model.GenericInterSpace * 2.0f - taillex;
              if (x1 > mysurf.SettingsXmin)
                x1 = mysurf.SettingsXmin;
              ecritx = x2 - model.GenericInterSpace;
              if (clickquicknavmenu) {
                if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
                  if (f == Gallery.filterb) {
                    mysurf.brightness -= mysurf.deltabright[mysurf.deltabrighti];
                    if (-0.001 < mysurf.brightness && mysurf.brightness < 0.001) {
                      mysurf.filteractive[f] = false;
                    } else {
                      mysurf.filteractive[f] = true;
                      mysurf.filterlist[f] = String.format("eq=brightness=%.3f", mysurf.brightness);
                    }
                  } else if (f == Gallery.filterc) {
                    mysurf.contrast -= mysurf.deltabright[mysurf.deltabrighti];
                    if (0.999 < mysurf.contrast && mysurf.contrast < 1.001) {
                      mysurf.filteractive[f] = false;
                    } else {
                      mysurf.filteractive[f] = true;
                      mysurf.filterlist[f] = String.format("eq=contrast=%.3f", mysurf.contrast);
                    }
                  } else if (f == Gallery.filters) {
                    mysurf.saturation -= mysurf.deltabright[mysurf.deltabrighti];
                    if (0.999 < mysurf.saturation && mysurf.saturation < 1.001) {
                      mysurf.filteractive[f] = false;
                    } else {
                      mysurf.filteractive[f] = true;
                      mysurf.filterlist[f] = String.format("eq=saturation=%.3f", mysurf.saturation);
                    }
                  } else if (f == Gallery.filterg) {
                    mysurf.gamma -= mysurf.deltabright[mysurf.deltabrighti];
                    if (0.999 < mysurf.gamma && mysurf.gamma < 1.001) {
                      mysurf.filteractive[f] = false;
                    } else {
                      mysurf.filteractive[f] = true;
                      mysurf.filterlist[f] = String.format("eq=gamma=%.3f", mysurf.gamma);
                    }
                  } else if (f == Gallery.filterw) {
                    mysurf.gamma_weight -= mysurf.deltabright[mysurf.deltabrighti];
                    if (0.999 < mysurf.gamma_weight && mysurf.gamma_weight < 1.001) {
                      mysurf.filteractive[f] = false;
                    } else {
                      mysurf.filteractive[f] = true;
                      mysurf.filterlist[f] = String.format("eq=gamma_weight=%.3f", mysurf.gamma_weight);
                    }
                  }
                  String filter = null;
                  for (int g = 0; g < fl; g++) {
                    if (mysurf.filteractive[g]) {
                      if (filter == null)
                        filter = mysurf.filterlist[g];
                      else
                        filter += "," + mysurf.filterlist[g];
                    }
                  }
                  mysurf.filter = filter;
                  try {
                    model.commandebigimagethreadqueue.put(new String[]{"updatebigpicture", String.valueOf(currid), String.valueOf(mysurf.ordnerIndex), String.valueOf(mysurf.mediaIndex), String.valueOf(mysurf.bigimagecurrentlydisplayed), String.valueOf(false), "0"});
                  } catch (InterruptedException e) {
                    e.printStackTrace();
                  }
                  needanotherupdate = true;
                  clickquicknavmenu = false;
                }
              }
              recti = new RectF(x1, y1, x2, y2);
              surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
              recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
              surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
              if (mysurf.filteractive[f]) {
                RectF rectisel = new RectF(x1 + quatredixiememillimetre, y1 + quatredixiememillimetre, x2 - quatredixiememillimetre, y2 - quatredixiememillimetre);
                surfacecanvas.drawRoundRect(rectisel, roundedRectRatio, roundedRectRatio, model.SettingsSelPaint);
              }
              if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
                RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
                surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
              }
              surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

              mabase += model.GenericCaseH + model.GenericInterSpace;
              if (mabase > mysurf.myheight)
                return;

        } else if (!mysurf.optionshowsubeffect && f == Gallery.filterreset) {

        } else if (mysurf.optionshowsubeffect && f == Gallery.filterreset) {

          y1 = mabase;
          y2 = mabase + model.GenericCaseH;
          ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;

          ecritoption = "reset";
          taillex = model.Menu1TextePaint.measureText(ecritoption);
          x1 = (mysurf.ScreenWidth - model.GenericInterSpace + mysurf.SettingsXmin) * 0.5f + model.GenericInterSpace * 0.5f;
          x2 = mysurf.ScreenWidth - model.GenericInterSpace;
          ecritx = x2;
          if (clickquicknavmenu) {
            if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
              for (int ff = model.filterb ; ff <= model.filterw ; ff++) {
                mysurf.filteractive[ff] = false;
              }
              mysurf.brightness = 0.0f;
              mysurf.contrast = 1.0f;
              mysurf.saturation = 1.0f;
              mysurf.gamma = 1.0f;
              mysurf.gamma_weight = 1.0f;
              String filter = null;
              for (int g = 0; g < fl; g++) {
                if (mysurf.filteractive[g]) {
                  if (filter == null)
                    filter = mysurf.filterlist[g];
                  else
                    filter += "," + mysurf.filterlist[g];
                }
              }
              mysurf.filter = filter;
              try {
                model.commandebigimagethreadqueue.put(new String[]{"updatebigpicture", String.valueOf(currid), String.valueOf(mysurf.ordnerIndex), String.valueOf(mysurf.mediaIndex), String.valueOf(mysurf.bigimagecurrentlydisplayed), String.valueOf(false), "0"});
              } catch (InterruptedException e) {
                e.printStackTrace();
              }
              needanotherupdate = true;
              clickquicknavmenu = false;
            }
          }
          recti = new RectF(x1, y1, x2, y2);
          //surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
          //recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
          surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
          if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
            RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
            surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
          }
          surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);


          ecritoption = String.format("+/- %.3f", mysurf.deltabright[mysurf.deltabrighti]);
          taillex = model.Menu1TextePaint.measureText(ecritoption);
          x2 = x1 - model.GenericInterSpace;
          x1 = x2 - model.GenericInterSpace * 2.0f - taillex;
          if (x1 > mysurf.SettingsXmin)
            x1 = mysurf.SettingsXmin;
          ecritx = x2 - model.GenericInterSpace;
          if (clickquicknavmenu) {
            if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
              mysurf.deltabrighti = (mysurf.deltabrighti + 1) % Surf.deltabright.length;
              needanotherupdate = true;
              clickquicknavmenu = false;
            }
          }
          recti = new RectF(x1, y1, x2, y2);
          surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
          recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
          surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
          if (mysurf.filteractive[f]) {
            RectF rectisel = new RectF(x1 + quatredixiememillimetre, y1 + quatredixiememillimetre, x2 - quatredixiememillimetre, y2 - quatredixiememillimetre);
            surfacecanvas.drawRoundRect(rectisel, roundedRectRatio, roundedRectRatio, model.SettingsSelPaint);
          }
          if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
            RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
            surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
          }
          surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

          mabase += model.GenericCaseH + model.GenericInterSpace;
          if (mabase > mysurf.myheight)
            return;


        } else if (!mysurf.optionshowsubeffectadvanced && f >= Gallery.filteradvanced) {

            if (!advanced) {
              advanced = true;
              ecritoption = "advanced effects";
              y1 = mabase;
              y2 = mabase + model.GenericCaseH;
              taillex = model.Menu1TextePaint.measureText(ecritoption);
              x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
              x2 = mysurf.ScreenWidth - model.GenericInterSpace;
              if (x1 > mysurf.SettingsXmin) {
                x1 = mysurf.SettingsXmin;
              }
              ecritx = x2 - model.GenericInterSpace;
              ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
              if (clickquicknavmenu) {
                if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
                  mysurf.optionshowsubeffectadvanced = !mysurf.optionshowsubeffectadvanced;
                  needanotherupdate = true;
                  clickquicknavmenu = false;
                }
              }
              recti = new RectF(x1, y1, x2, y2);
              surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
              recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
              surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
              if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
                RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
                surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
              }
              surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

              mabase += model.GenericCaseH + model.GenericInterSpace;
              if (mabase > mysurf.myheight)
                return;
            }

          } else {

          if (mysurf.optionshowsubeffectadvanced && f >= Gallery.filteradvanced) {
            if (!advanced) {
              advanced = true;
              ecritoption = "-";
              y1 = mabase;
              y2 = mabase + model.GenericCaseH;
              taillex = model.Menu1TextePaint.measureText(ecritoption);
              x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
              x2 = mysurf.ScreenWidth - model.GenericInterSpace;
              if (x1 > mysurf.SettingsXmin) {
                x1 = mysurf.SettingsXmin;
              }
              ecritx = x2 - model.GenericInterSpace;
              ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
              if (clickquicknavmenu) {
                if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
                  mysurf.optionshowsubeffectadvanced = !mysurf.optionshowsubeffectadvanced;
                  needanotherupdate = true;
                  clickquicknavmenu = false;
                }
              }
              recti = new RectF(x1, y1, x2, y2);
              surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
              recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
              surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
              if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
                RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
                surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
              }
              surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

              mabase += model.GenericCaseH + model.GenericInterSpace;
              if (mabase > mysurf.myheight)
                return;
            }
          }

              y1 = mabase;
              y2 = mabase + model.GenericCaseH;
              ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;

              ecritoption = "... ";
              taillex = model.Menu1TextePaint.measureText(ecritoption);
              x1 = (mysurf.ScreenWidth - model.GenericInterSpace + mysurf.SettingsXmin) * 0.5f + model.GenericInterSpace * 0.5f;
              x2 = mysurf.ScreenWidth - model.GenericInterSpace;
              ecritx = x2;
              if (clickquicknavmenu) {
                if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
                  model.message(
                      mysurf.filterprint[f] + "\n" +
                          mysurf.filterlist[f] + "\n" +
                          mysurf.filterinfo[f]
                  );
                  if (mysurf.filterprint[f].equals("crop width")) {
                /*mysurf.cropw = true;
                mysurf.croph = false;*/
                    if (mysurf.srcrect.left < mysurf.srcrect.right) {
                      mysurf.filterlist[f] = "crop=w=" + (mysurf.srcrect.right - mysurf.srcrect.left) + ":x=" + mysurf.srcrect.left;
                    }
                  } else if (mysurf.filterprint[f].equals("crop height")) {
                /*mysurf.cropw = false;
                mysurf.croph = true;*/
                    if (mysurf.srcrect.top < mysurf.srcrect.bottom) {
                      mysurf.filterlist[f] = "crop=h=" + (mysurf.srcrect.bottom - mysurf.srcrect.top) + ":y=" + mysurf.srcrect.top;
                    }
                  }/* else {
                mysurf.cropw = false;
                mysurf.croph = false;
              }*/
                  if (mysurf.filterlastselected == f) {
                    mysurf.filterlastselected = f;
                    Intent intent = new Intent();
                    intent.setAction(Gallery.broadcastname);
                    intent.putExtra("goal", "filter");
                    intent.putExtra("id", currid);
                    LocalBroadcastManager.getInstance(model.activitycontext).sendBroadcast(intent);
                  }
                  mysurf.filterlastselected = f;
                  needanotherupdate = true;
                  clickquicknavmenu = false;
                }
              }
              recti = new RectF(x1, y1, x2, y2);
              //surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
              //recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
              surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
              if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
                RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
                surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
              }
              surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);


              if (mysurf.filterprint[f].equals(""))
                ecritoption = mysurf.filterlist[f];
              else
                ecritoption = mysurf.filterprint[f];
              taillex = model.Menu1TextePaint.measureText(ecritoption);
              x2 = x1 - model.GenericInterSpace;
              x1 = x2 - model.GenericInterSpace * 2.0f - taillex;
              if (x1 > mysurf.SettingsXmin)
                x1 = mysurf.SettingsXmin;
              ecritx = x2 - model.GenericInterSpace;
              if (clickquicknavmenu) {
                if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
                  if (mysurf.filterlist[f].equals("")) {
                    mysurf.filteractive[f] = false;
                  } else {
                    mysurf.filteractive[f] = !mysurf.filteractive[f];
                    if (mysurf.filteractive[f]) {
                      if (mysurf.filterprint[f].equals("crop width")) {
                        if (mysurf.srcrect.left < mysurf.srcrect.right) {
                          mysurf.filterlist[f] = "crop=w=" + (mysurf.srcrect.right - mysurf.srcrect.left) + ":x=" + mysurf.srcrect.left;
                          mysurf.fichierprecedent = "dummy";
                          mysurf.bpx = 0;
                          mysurf.bpy = 0;
                          mysurf.bscale = 1;
                          mysurf.centeronscreen = true;
                        }
                      } else if (mysurf.filterprint[f].equals("crop height")) {
                        if (mysurf.srcrect.top < mysurf.srcrect.bottom) {
                          mysurf.filterlist[f] = "crop=h=" + (mysurf.srcrect.bottom - mysurf.srcrect.top) + ":y=" + mysurf.srcrect.top;
                          mysurf.fichierprecedent = "dummy";
                          mysurf.bpx = 0;
                          mysurf.bpy = 0;
                          mysurf.bscale = 1;
                          mysurf.centeronscreen = true;
                        }
                      }
                    }
                    String filter = null;
                    for (int g = 0; g < fl; g++) {
                      if (mysurf.filteractive[g]) {
                        if (filter == null)
                          filter = mysurf.filterlist[g];
                        else
                          filter += "," + mysurf.filterlist[g];
                      }
                    }
                    mysurf.filter = filter;
                    try {
                      model.commandebigimagethreadqueue.put(new String[]{"updatebigpicture", String.valueOf(currid), String.valueOf(mysurf.ordnerIndex), String.valueOf(mysurf.mediaIndex), String.valueOf(mysurf.bigimagecurrentlydisplayed), String.valueOf(false), "0"});
                    } catch (InterruptedException e) {
                      e.printStackTrace();
                    }
                  }
                  needanotherupdate = true;
                  clickquicknavmenu = false;
                }
              }
              recti = new RectF(x1, y1, x2, y2);
              surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
              recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
              surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
              if (mysurf.filteractive[f]) {
                RectF rectisel = new RectF(x1 + quatredixiememillimetre, y1 + quatredixiememillimetre, x2 - quatredixiememillimetre, y2 - quatredixiememillimetre);
                surfacecanvas.drawRoundRect(rectisel, roundedRectRatio, roundedRectRatio, model.SettingsSelPaint);
              }
              if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
                RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
                surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
              }
              surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

              mabase += model.GenericCaseH + model.GenericInterSpace;
              if (mabase > mysurf.myheight)
                return;
            }

      }

      ecritoption = "save modified";
      y1 = mabase;
      y2 = mabase + model.GenericCaseH;
      taillex = model.Menu1TextePaint.measureText(ecritoption);
      x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
      x2 = mysurf.ScreenWidth - model.GenericInterSpace;
      if (x1 > mysurf.SettingsXmin) {
        x1 = mysurf.SettingsXmin;
      }
      ecritx = x2 - model.GenericInterSpace;
      ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
      if (clickquicknavmenu) {
        if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
          int folderfilecount = model.getFolderFileCount(mysurf.ordnerIndex);
          if (mysurf.mediaIndex < folderfilecount && folderfilecount > 0) {
            String fichname = model.getMediaAddress(mysurf.ordnerIndex, mysurf.mediaIndex);
            if (mysurf.isincache(fichname)) {
              DateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmss");
              Date date = new Date();
              String destpath = model.dossierdessin + fichname.replaceAll(".+/", "") + dateFormat.format(date) + ".png";
              if (mysurf.saveCachedBitmap(fichname, destpath)) {
                model.message("picture saved as\n" + destpath);
                llog.d(TAG, "picture saved as " + destpath);
                try {
                  model.commandethreaddatabase.put(new String[]{"chercheFichierDansDatabaseSinonRescanSonDossier", model.dossierdessin, destpath, "-1"});
                } catch (InterruptedException e) {
                  e.printStackTrace();
                }
              } else {
                model.message("Could not save\n" + destpath);
                llog.d(TAG, "Error could not save " + destpath);
              }
            }
          }
          needanotherupdate = true;
          clickquicknavmenu = false;
        }
      }
      recti = new RectF(x1, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
      recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
      if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
        RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
        surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
      }
      surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

      mabase += model.GenericCaseH + model.GenericInterSpace;
      if (mabase > mysurf.myheight)
        return;

      /*
      ecritoption = "no rotation";
      y1 = mabase;
      y2 = mabase + model.GenericCaseH;
      taillex = model.Menu1TextePaint.measureText(ecritoption);
      x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
      x2 = mysurf.ScreenWidth - model.GenericInterSpace;
      if (x1 > mysurf.SettingsXmin) {
        x1 = mysurf.SettingsXmin;
      }
      ecritx = x2 - model.GenericInterSpace;
      ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
      if (clickquicknavmenu) {
        if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
          mysurf.rotate = 0;
          try {
            model.bigimagethreadqueue.put(new String[]{"cachebitmap", String.valueOf(currid), String.valueOf(mysurf.ordnerIndex), String.valueOf(mysurf.mediaIndex), String.valueOf(mysurf.bigimagecurrentlydisplayed),
                    String.valueOf(true), "0"});
          } catch (InterruptedException e) {
            e.printStackTrace();
          }
          needanotherupdate = true;
          clickquicknavmenu = false;
        }
      }
      recti = new RectF(x1, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
      recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
      if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
        RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
        surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
      }
      surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

      mabase += model.GenericCaseH + model.GenericInterSpace;
      if (mabase > mysurf.myheight)
        return;

      ecritoption = "90° right";
      y1 = mabase;
      y2 = mabase + model.GenericCaseH;
      taillex = model.Menu1TextePaint.measureText(ecritoption);
      x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
      x2 = mysurf.ScreenWidth - model.GenericInterSpace;
      if (x1 > mysurf.SettingsXmin) {
        x1 = mysurf.SettingsXmin;
      }
      ecritx = x2 - model.GenericInterSpace;
      ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
      if (clickquicknavmenu) {
        if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
          mysurf.rotate = 90;
          try {
            model.bigimagethreadqueue.put(new String[]{"cachebitmap", String.valueOf(currid), String.valueOf(mysurf.ordnerIndex), String.valueOf(mysurf.mediaIndex), String.valueOf(mysurf.bigimagecurrentlydisplayed),
                    String.valueOf(true), "90"});
          } catch (InterruptedException e) {
            e.printStackTrace();
          }
          needanotherupdate = true;
          clickquicknavmenu = false;
        }
      }
      recti = new RectF(x1, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
      recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
      if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
        RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
        surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
      }
      surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

      mabase += model.GenericCaseH + model.GenericInterSpace;
      if (mabase > mysurf.myheight)
        return;

      ecritoption = "90° left";
      y1 = mabase;
      y2 = mabase + model.GenericCaseH;
      taillex = model.Menu1TextePaint.measureText(ecritoption);
      x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
      x2 = mysurf.ScreenWidth - model.GenericInterSpace;
      if (x1 > mysurf.SettingsXmin) {
        x1 = mysurf.SettingsXmin;
      }
      ecritx = x2 - model.GenericInterSpace;
      ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
      if (clickquicknavmenu) {
        if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
          mysurf.rotate = 270;
          try {
            model.bigimagethreadqueue.put(new String[]{"cachebitmap", String.valueOf(currid), String.valueOf(mysurf.ordnerIndex), String.valueOf(mysurf.mediaIndex), String.valueOf(mysurf.bigimagecurrentlydisplayed),
                    String.valueOf(true), "270"});
          } catch (InterruptedException e) {
            e.printStackTrace();
          }
          needanotherupdate = true;
          clickquicknavmenu = false;
        }
      }
      recti = new RectF(x1, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
      recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
      if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
        RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
        surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
      }
      surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

      mabase += model.GenericCaseH + model.GenericInterSpace;
      if (mabase > mysurf.myheight)
        return;

      ecritoption = "upsidedown";
      y1 = mabase;
      y2 = mabase + model.GenericCaseH;
      taillex = model.Menu1TextePaint.measureText(ecritoption);
      x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
      x2 = mysurf.ScreenWidth - model.GenericInterSpace;
      if (x1 > mysurf.SettingsXmin) {
        x1 = mysurf.SettingsXmin;
      }
      ecritx = x2 - model.GenericInterSpace;
      ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
      if (clickquicknavmenu) {
        if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
          mysurf.rotate = 180;
          try {
            model.bigimagethreadqueue.put(new String[]{"cachebitmap", String.valueOf(currid), String.valueOf(mysurf.ordnerIndex), String.valueOf(mysurf.mediaIndex), String.valueOf(mysurf.bigimagecurrentlydisplayed),
                    String.valueOf(true), "180"});
          } catch (InterruptedException e) {
            e.printStackTrace();
          }
          needanotherupdate = true;
          clickquicknavmenu = false;
        }
      }
      recti = new RectF(x1, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
      recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
      if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
        RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
        surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
      }
      surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

      mabase += model.GenericCaseH + model.GenericInterSpace;
      if (mabase > mysurf.myheight)
        return;

      ecritoption = "mirror";
      y1 = mabase;
      y2 = mabase + model.GenericCaseH;
      taillex = model.Menu1TextePaint.measureText(ecritoption);
      x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
      x2 = mysurf.ScreenWidth - model.GenericInterSpace;
      if (x1 > mysurf.SettingsXmin) {
        x1 = mysurf.SettingsXmin;
      }
      ecritx = x2 - model.GenericInterSpace;
      ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
      if (clickquicknavmenu) {
        if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
          mysurf.mirror = -mysurf.mirror;
          try {
            model.bigimagethreadqueue.put(new String[]{"cachebitmap", String.valueOf(currid), String.valueOf(mysurf.ordnerIndex), String.valueOf(mysurf.mediaIndex), String.valueOf(mysurf.bigimagecurrentlydisplayed),
                    String.valueOf(true), "-1"});
          } catch (InterruptedException e) {
            e.printStackTrace();
          }
          needanotherupdate = true;
          clickquicknavmenu = false;
        }
      }
      recti = new RectF(x1, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
      recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
      if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
        RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
        surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
      }
      surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

      mabase += model.GenericCaseH + model.GenericInterSpace;
      if (mabase > mysurf.myheight)
        return;

      int brightnessdelta = model.brightnessdelta;
      int contrastdelta = model.brightnessdelta;
      int saturationdelta = model.brightnessdelta;
      int huedelta = model.brightnessdelta;

      y1 = mabase;
      y2 = mabase + model.GenericCaseH;
      ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;

      ecritoption = "+";
      taillex = model.Menu1TextePaint.measureText(ecritoption);
      x1 = (mysurf.ScreenWidth - model.GenericInterSpace + mysurf.SettingsXmin) * 0.5f + model.GenericInterSpace * 0.5f;
      x2 = mysurf.ScreenWidth - model.GenericInterSpace;
      ecritx = x2;
      if (clickquicknavmenu) {
        if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
          mysurf.brightness += brightnessdelta;
          try {
            model.bigimagethreadqueue.put(new String[]{"cachebitmap", String.valueOf(currid), String.valueOf(mysurf.ordnerIndex), String.valueOf(mysurf.mediaIndex), String.valueOf(mysurf.bigimagecurrentlydisplayed),
                    String.valueOf(true), "-1"});
          } catch (InterruptedException e) {
            e.printStackTrace();
          }
          needanotherupdate = true;
          clickquicknavmenu = false;
        }
      }
      recti = new RectF(x1, y1, x2, y2);
      //surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
      //recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
      if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
        RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
        surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
      }
      surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

      ecritoption = "brightness " + mysurf.brightness + "   -";
      taillex = model.Menu1TextePaint.measureText(ecritoption);
      x2 = x1 - model.GenericInterSpace;
      x1 = x2 - model.GenericInterSpace * 2.0f - taillex;
      if (x1 > mysurf.SettingsXmin)
        x1 = mysurf.SettingsXmin;
      ecritx = x2 - model.GenericInterSpace;
      if (clickquicknavmenu) {
        if (mysurf.SettingsXmin < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
          mysurf.brightness -= brightnessdelta;
          try {
            model.bigimagethreadqueue.put(new String[]{"cachebitmap", String.valueOf(currid), String.valueOf(mysurf.ordnerIndex), String.valueOf(mysurf.mediaIndex), String.valueOf(mysurf.bigimagecurrentlydisplayed),
                    String.valueOf(true), "-1"});
          } catch (InterruptedException e) {
            e.printStackTrace();
          }
          needanotherupdate = true;
          clickquicknavmenu = false;
        }
      }
      recti = new RectF(x1, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
      recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
      if ((model.isandroidtv || model.keypadnavactive) && mysurf.SettingsXmin < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
        RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
        surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
      }
      surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

      mabase += model.GenericCaseH + model.GenericInterSpace;
      if (mabase > mysurf.myheight)
        return;

      y1 = mabase;
      y2 = mabase + model.GenericCaseH;
      ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;

      ecritoption = "+";
      taillex = model.Menu1TextePaint.measureText(ecritoption);
      x1 = (mysurf.ScreenWidth - model.GenericInterSpace + mysurf.SettingsXmin) * 0.5f + model.GenericInterSpace * 0.5f;
      x2 = mysurf.ScreenWidth - model.GenericInterSpace;
      ecritx = x2;
      if (clickquicknavmenu) {
        if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
          mysurf.contrast += contrastdelta;
          try {
            model.bigimagethreadqueue.put(new String[]{"cachebitmap", String.valueOf(currid), String.valueOf(mysurf.ordnerIndex), String.valueOf(mysurf.mediaIndex), String.valueOf(mysurf.bigimagecurrentlydisplayed),
                    String.valueOf(true), "-1"});
          } catch (InterruptedException e) {
            e.printStackTrace();
          }
          needanotherupdate = true;
          clickquicknavmenu = false;
        }
      }
      recti = new RectF(x1, y1, x2, y2);
      //surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
      //recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
      if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
        RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
        surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
      }
      surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

      ecritoption = "contrast " + mysurf.contrast + "   -";
      taillex = model.Menu1TextePaint.measureText(ecritoption);
      x2 = x1 - model.GenericInterSpace;
      x1 = x2 - model.GenericInterSpace * 2.0f - taillex;
      if (x1 > mysurf.SettingsXmin)
        x1 = mysurf.SettingsXmin;
      ecritx = x2 - model.GenericInterSpace;
      if (clickquicknavmenu) {
        if (mysurf.SettingsXmin < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
          mysurf.contrast -= contrastdelta;
          try {
            model.bigimagethreadqueue.put(new String[]{"cachebitmap", String.valueOf(currid), String.valueOf(mysurf.ordnerIndex), String.valueOf(mysurf.mediaIndex), String.valueOf(mysurf.bigimagecurrentlydisplayed),
                    String.valueOf(true), "-1"});
          } catch (InterruptedException e) {
            e.printStackTrace();
          }
          needanotherupdate = true;
          clickquicknavmenu = false;
        }
      }
      recti = new RectF(x1, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
      recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
      if ((model.isandroidtv || model.keypadnavactive) && mysurf.SettingsXmin < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
        RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
        surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
      }
      surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

      mabase += model.GenericCaseH + model.GenericInterSpace;
      if (mabase > mysurf.myheight)
        return;

      y1 = mabase;
      y2 = mabase + model.GenericCaseH;
      ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;

      ecritoption = "+";
      taillex = model.Menu1TextePaint.measureText(ecritoption);
      x1 = (mysurf.ScreenWidth - model.GenericInterSpace + mysurf.SettingsXmin) * 0.5f + model.GenericInterSpace * 0.5f;
      x2 = mysurf.ScreenWidth - model.GenericInterSpace;
      ecritx = x2;
      if (clickquicknavmenu) {
        if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
          mysurf.saturation += saturationdelta;
          try {
            model.bigimagethreadqueue.put(new String[]{"cachebitmap", String.valueOf(currid), String.valueOf(mysurf.ordnerIndex), String.valueOf(mysurf.mediaIndex), String.valueOf(mysurf.bigimagecurrentlydisplayed),
                    String.valueOf(true), "-1"});
          } catch (InterruptedException e) {
            e.printStackTrace();
          }
          needanotherupdate = true;
          clickquicknavmenu = false;
        }
      }
      recti = new RectF(x1, y1, x2, y2);
      //surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
      //recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
      if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
        RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
        surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
      }
      surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

      ecritoption = "saturation " + mysurf.saturation + "   -";
      taillex = model.Menu1TextePaint.measureText(ecritoption);
      x2 = x1 - model.GenericInterSpace;
      x1 = x2 - model.GenericInterSpace * 2.0f - taillex;
      if (x1 > mysurf.SettingsXmin)
        x1 = mysurf.SettingsXmin;
      ecritx = x2 - model.GenericInterSpace;
      if (clickquicknavmenu) {
        if (mysurf.SettingsXmin < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
          mysurf.saturation -= saturationdelta;
          try {
            model.bigimagethreadqueue.put(new String[]{"cachebitmap", String.valueOf(currid), String.valueOf(mysurf.ordnerIndex), String.valueOf(mysurf.mediaIndex), String.valueOf(mysurf.bigimagecurrentlydisplayed),
                    String.valueOf(true), "-1"});
          } catch (InterruptedException e) {
            e.printStackTrace();
          }
          needanotherupdate = true;
          clickquicknavmenu = false;
        }
      }
      recti = new RectF(x1, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
      recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
      if ((model.isandroidtv || model.keypadnavactive) && mysurf.SettingsXmin < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
        RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
        surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
      }
      surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

      mabase += model.GenericCaseH + model.GenericInterSpace;
      if (mabase > mysurf.myheight)
        return;

      y1 = mabase;
      y2 = mabase + model.GenericCaseH;
      ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;

      ecritoption = "+";
      taillex = model.Menu1TextePaint.measureText(ecritoption);
      x1 = (mysurf.ScreenWidth - model.GenericInterSpace + mysurf.SettingsXmin) * 0.5f + model.GenericInterSpace * 0.5f;
      x2 = mysurf.ScreenWidth - model.GenericInterSpace;
      ecritx = x2;
      if (clickquicknavmenu) {
        if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
          mysurf.hue += huedelta;
          try {
            model.bigimagethreadqueue.put(new String[]{"cachebitmap", String.valueOf(currid), String.valueOf(mysurf.ordnerIndex), String.valueOf(mysurf.mediaIndex), String.valueOf(mysurf.bigimagecurrentlydisplayed),
                    String.valueOf(true), "-1"});
          } catch (InterruptedException e) {
            e.printStackTrace();
          }
          needanotherupdate = true;
          clickquicknavmenu = false;
        }
      }
      recti = new RectF(x1, y1, x2, y2);
      //surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
      //recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
      if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
        RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
        surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
      }
      surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

      ecritoption = "hue " + mysurf.hue + "   -";
      taillex = model.Menu1TextePaint.measureText(ecritoption);
      x2 = x1 - model.GenericInterSpace;
      x1 = x2 - model.GenericInterSpace * 2.0f - taillex;
      if (x1 > mysurf.SettingsXmin)
        x1 = mysurf.SettingsXmin;
      ecritx = x2 - model.GenericInterSpace;
      if (clickquicknavmenu) {
        if (mysurf.SettingsXmin < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
          mysurf.hue -= huedelta;
          try {
            model.bigimagethreadqueue.put(new String[]{"cachebitmap", String.valueOf(currid), String.valueOf(mysurf.ordnerIndex), String.valueOf(mysurf.mediaIndex), String.valueOf(mysurf.bigimagecurrentlydisplayed),
                    String.valueOf(true), "-1"});
          } catch (InterruptedException e) {
            e.printStackTrace();
          }
          needanotherupdate = true;
          clickquicknavmenu = false;
        }
      }
      recti = new RectF(x1, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
      recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
      if ((model.isandroidtv || model.keypadnavactive) && mysurf.SettingsXmin < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
        RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
        surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
      }
      surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

      mabase += model.GenericCaseH + model.GenericInterSpace;
      if (mabase > mysurf.myheight)
        return;

      y1 = mabase;
      y2 = mabase + model.GenericCaseH;
      ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;

      ecritoption = "+";
      taillex = model.Menu1TextePaint.measureText(ecritoption);
      x1 = (mysurf.ScreenWidth - model.GenericInterSpace + mysurf.SettingsXmin) * 0.5f + model.GenericInterSpace * 0.5f;
      x2 = mysurf.ScreenWidth - model.GenericInterSpace;
      ecritx = x2;
      if (clickquicknavmenu) {
        if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
          model.brightnessdelta += 1;
          needanotherupdate = true;
          clickquicknavmenu = false;
        }
      }
      recti = new RectF(x1, y1, x2, y2);
      //surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
      //recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
      if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
        RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
        surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
      }
      surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

      ecritoption = "delta " + model.brightnessdelta + "   -";
      taillex = model.Menu1TextePaint.measureText(ecritoption);
      x2 = x1 - model.GenericInterSpace;
      x1 = x2 - model.GenericInterSpace * 2.0f - taillex;
      if (x1 > mysurf.SettingsXmin)
        x1 = mysurf.SettingsXmin;
      ecritx = x2 - model.GenericInterSpace;
      if (clickquicknavmenu) {
        if (mysurf.SettingsXmin < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
          model.brightnessdelta -= 1;
          needanotherupdate = true;
          clickquicknavmenu = false;
        }
      }
      recti = new RectF(x1, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
      recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
      if ((model.isandroidtv || model.keypadnavactive) && mysurf.SettingsXmin < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
        RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
        surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
      }
      surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

      mabase += model.GenericCaseH + model.GenericInterSpace;
      if (mabase > mysurf.myheight)
        return;

      if (mysurf.brightness != 0 || mysurf.contrast != 0 || mysurf.hue != 0 || mysurf.saturation != 0) {
        ecritoption = "original";
        y1 = mabase;
        y2 = mabase + model.GenericCaseH;
        taillex = model.Menu1TextePaint.measureText(ecritoption);
        x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
        x2 = mysurf.ScreenWidth - model.GenericInterSpace;
        if (x1 > mysurf.SettingsXmin) {
          x1 = mysurf.SettingsXmin;
        }
        ecritx = x2 - model.GenericInterSpace;
        ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
        if (clickquicknavmenu) {
          if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
            mysurf.brightness = 0;
            mysurf.saturation = 0;
            mysurf.contrast = 0;
            mysurf.hue = 0;
            try {
              model.bigimagethreadqueue.put(new String[]{"cachebitmap", String.valueOf(currid), String.valueOf(mysurf.ordnerIndex), String.valueOf(mysurf.mediaIndex), String.valueOf(mysurf.bigimagecurrentlydisplayed),
                      String.valueOf(true), "-1"});
            } catch (InterruptedException e) {
              e.printStackTrace();
            }
            needanotherupdate = true;
            clickquicknavmenu = false;
          }
        }
        recti = new RectF(x1, y1, x2, y2);
        surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
        recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
        surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
        if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
          RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
          surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
        }
        surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

        mabase += model.GenericCaseH + model.GenericInterSpace;
        if (mabase > mysurf.myheight)
          return;
      }
      */

      if (mysurf.paintwantsdither)
        ecritoption = "dither on";
      else
        ecritoption = "dither off";
      y1 = mabase;
      y2 = mabase + model.GenericCaseH;
      taillex = model.Menu1TextePaint.measureText(ecritoption);
      x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
      x2 = mysurf.ScreenWidth - model.GenericInterSpace;
      if (x1 > mysurf.SettingsXmin) {
        x1 = mysurf.SettingsXmin;
      }
      ecritx = x2 - model.GenericInterSpace;
      ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
      if (clickquicknavmenu) {
        if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
          mysurf.paintwantsdither = !mysurf.paintwantsdither;
          model.preferences.edit().putBoolean("paintwantsdither", mysurf.paintwantsdither).apply();
          mysurf.paintdither.setDither(mysurf.paintwantsdither);
          try {
            model.commandebigimagethreadqueue.put(new String[]{"cachebitmap", String.valueOf(currid), String.valueOf(mysurf.ordnerIndex), String.valueOf(mysurf.mediaIndex), String.valueOf(mysurf.bigimagecurrentlydisplayed),
                    String.valueOf(true), "0"});
          } catch (InterruptedException e) {
            e.printStackTrace();
          }
          needanotherupdate = true;
          clickquicknavmenu = false;
        }
      }
      recti = new RectF(x1, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
      recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
      if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
        RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
        surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
      }
      surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

      mabase += model.GenericCaseH + model.GenericInterSpace;
      if (mabase > mysurf.myheight)
        return;

    }

  }

  private void menusplit(Canvas surfacecanvas, int foldercount) {
    ecritoption = "split screen"; // keep same zoom between images for comic reading
    y1 = mabase;
    y2 = mabase + model.GenericCaseH;
    taillex = model.Menu1TextePaint.measureText(ecritoption);
    x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
    x2 = mysurf.ScreenWidth - model.GenericInterSpace;
    if (x1 > mysurf.SettingsXmin) {
      x1 = mysurf.SettingsXmin;
    }
    ecritx = x2 - model.GenericInterSpace;
    ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
    if (clickquicknavmenu) {
      if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
        mysurf.optionshowsplit = !mysurf.optionshowsplit;
        needanotherupdate = true;
        clickquicknavmenu = false;
      }
    }
    recti = new RectF(x1, y1, x2, y2);
    surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu1BgPainta);
    recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
    surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu1BgPainto);
    if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
      RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
      surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
    }
    surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

    mabase += model.GenericCaseH + model.GenericInterSpace;
    if (mabase > mysurf.myheight)
      return;

    if (mysurf.optionshowsplit) {

      ecritoption = "remove this split";
      y1 = mabase;
      y2 = mabase + model.GenericCaseH;
      taillex = model.Menu1TextePaint.measureText(ecritoption);
      x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
      x2 = mysurf.ScreenWidth - model.GenericInterSpace;
      if (x1 > mysurf.SettingsXmin) {
        x1 = mysurf.SettingsXmin;
      }
      ecritx = x2 - model.GenericInterSpace;
      ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
      if (clickquicknavmenu) {
        if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
          Intent intent = new Intent();
          intent.setAction(Gallery.broadcastname);
          intent.putExtra("goal", "splitscreen");
          intent.putExtra("splitnumber", 1);
          intent.putExtra("splitsurf", mysurf.myid);
          LocalBroadcastManager.getInstance(model.activitycontext).sendBroadcast(intent);
          needanotherupdate = true;
          clickquicknavmenu = false;
        }
      }
      recti = new RectF(x1, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
      recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
      if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
        RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
        surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
      }
      surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

      mabase += model.GenericCaseH + model.GenericInterSpace;
      if (mabase > mysurf.myheight)
        return;

      ecritoption = "split vertical";
      y1 = mabase;
      y2 = mabase + model.GenericCaseH;
      taillex = model.Menu1TextePaint.measureText(ecritoption);
      x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
      x2 = mysurf.ScreenWidth - model.GenericInterSpace;
      if (x1 > mysurf.SettingsXmin) {
        x1 = mysurf.SettingsXmin;
      }
      ecritx = x2 - model.GenericInterSpace;
      ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
      if (clickquicknavmenu) {
        if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
          Intent intent = new Intent();
          intent.setAction(Gallery.broadcastname);
          intent.putExtra("goal", "splitscreen");
          intent.putExtra("splitnumber", 2);
          intent.putExtra("splitsurf", mysurf.myid);
          intent.putExtra("splitratio", 0.50f);
          intent.putExtra("splitvertical", true);
          LocalBroadcastManager.getInstance(model.activitycontext).sendBroadcast(intent);
          if (!model.alreadymessagedexpandsplitscreen) {
            model.message("You can drag the location of the split.");
            model.alreadymessagedexpandsplitscreen = true;
          }
          needanotherupdate = true;
          clickquicknavmenu = false;
        }
      }
      recti = new RectF(x1, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
      recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
      if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
        RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
        surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
      }
      surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

      mabase += model.GenericCaseH + model.GenericInterSpace;
      if (mabase > mysurf.myheight)
        return;

      ecritoption = "split horizontal";
      y1 = mabase;
      y2 = mabase + model.GenericCaseH;
      taillex = model.Menu1TextePaint.measureText(ecritoption);
      x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
      x2 = mysurf.ScreenWidth - model.GenericInterSpace;
      if (x1 > mysurf.SettingsXmin) {
        x1 = mysurf.SettingsXmin;
      }
      ecritx = x2 - model.GenericInterSpace;
      ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
      if (clickquicknavmenu) {
        if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
          Intent intent = new Intent();
          intent.setAction(Gallery.broadcastname);
          intent.putExtra("goal", "splitscreen");
          intent.putExtra("splitnumber", 2);
          intent.putExtra("splitsurf", mysurf.myid);
          intent.putExtra("splitratio", 0.50f);
          intent.putExtra("splitvertical", false);
          LocalBroadcastManager.getInstance(model.activitycontext).sendBroadcast(intent);
          if (!model.alreadymessagedexpandsplitscreen) {
            model.message("You can drag the location of the split.");
            model.alreadymessagedexpandsplitscreen = true;
          }
          needanotherupdate = true;
          clickquicknavmenu = false;
        }
      }
      recti = new RectF(x1, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
      recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
      if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
        RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
        surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
      }
      surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

      mabase += model.GenericCaseH + model.GenericInterSpace;
      if (mabase > mysurf.myheight)
        return;

      // TODO : last split

    }

  }

  private int optionlastpicture = 1;
  private int optionlastvideo = 1;
  private void menusearch(Canvas surfacecanvas, int foldercount) {
    ecritoption = "search";
    y1 = mabase;
    y2 = mabase + model.GenericCaseH;
    taillex = model.Menu1TextePaint.measureText(ecritoption);
    x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
    x2 = mysurf.ScreenWidth - model.GenericInterSpace;
    if (x1 > mysurf.SettingsXmin) {
      x1 = mysurf.SettingsXmin;
    }
    ecritx = x2 - model.GenericInterSpace;
    ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
    if (clickquicknavmenu) {
      if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
        mysurf.optionshowsearch = !mysurf.optionshowsearch;
        optionlastpicture = 1;
        optionlastvideo = 1;
        needanotherupdate = true;
        clickquicknavmenu = false;
      }
    }
    recti = new RectF(x1, y1, x2, y2);
    surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu1BgPainta);
    recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
    surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu1BgPainto);
    if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
      RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
      surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
    }
    surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

    mabase += model.GenericCaseH + model.GenericInterSpace;
    if (mabase > mysurf.myheight)
      return;

    if (mysurf.optionshowsearch) {

      ecritoption = "search";
      y1 = mabase;
      y2 = mabase + model.GenericCaseH;
      taillex = model.Menu1TextePaint.measureText(ecritoption);
      x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
      x2 = mysurf.ScreenWidth - model.GenericInterSpace;
      if (x1 > mysurf.SettingsXmin) {
        x1 = mysurf.SettingsXmin;
      }
      ecritx = x2 - model.GenericInterSpace;
      ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
      if (clickquicknavmenu) {
        if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
          Intent intent = new Intent();
          intent.setAction(Gallery.broadcastname);
          intent.putExtra("goal", "search");
          intent.putExtra("id", currid);
          intent.putExtra("ordnerIndex", mysurf.ordnerIndex);
          intent.putExtra("mediaIndex", mysurf.mediaIndex);
          LocalBroadcastManager.getInstance(model.activitycontext).sendBroadcast(intent);
          needanotherupdate = true;
          clickquicknavmenu = false;
        }
      }
      recti = new RectF(x1, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
      recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
      if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
        RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
        surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
      }
      surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

      mabase += model.GenericCaseH + model.GenericInterSpace;
      if (mabase > mysurf.myheight)
        return;

      if (model.searchthis != null) {
        ecritoption = "next " + model.searchthis;
        y1 = mabase;
        y2 = mabase + model.GenericCaseH;
        taillex = model.Menu1TextePaint.measureText(ecritoption);
        x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
        x2 = mysurf.ScreenWidth - model.GenericInterSpace;
        if (x1 > mysurf.SettingsXmin) {
          x1 = mysurf.SettingsXmin;
        }
        ecritx = x2 - model.GenericInterSpace;
        ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
        if (clickquicknavmenu) {
          if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
            try {
              model.commandethreaddatabase.put(new String[]{"search", model.searchthis, String.valueOf(currid), String.valueOf(mysurf.ordnerIndex), String.valueOf(mysurf.mediaIndex)});
            } catch (InterruptedException e) {
              e.printStackTrace();
            }
            needanotherupdate = true;
            clickquicknavmenu = false;
          }
        }
        recti = new RectF(x1, y1, x2, y2);
        surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
        recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
        surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
        if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
          RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
          surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
        }
        surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

        mabase += model.GenericCaseH + model.GenericInterSpace;
        if (mabase > mysurf.myheight)
          return;
      }

      ecritoption = "last picture";
      if (optionlastpicture > 1)
        ecritoption += " (-" + optionlastpicture + ")";
      y1 = mabase;
      y2 = mabase + model.GenericCaseH;
      taillex = model.Menu1TextePaint.measureText(ecritoption);
      x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
      x2 = mysurf.ScreenWidth - model.GenericInterSpace;
      if (x1 > mysurf.SettingsXmin) {
        x1 = mysurf.SettingsXmin;
      }
      ecritx = x2 - model.GenericInterSpace;
      ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
      if (clickquicknavmenu) {
        if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
          Intent intent = new Intent();
          intent.setAction(Gallery.broadcastname);
          intent.putExtra("goal", "shownextfileinmediastore");
          intent.putExtra("id", currid);
          intent.putExtra("numberlast", optionlastpicture);
          intent.putExtra("wantpicture", true);
          LocalBroadcastManager.getInstance(model.activitycontext).sendBroadcast(intent);
          optionlastpicture++;
          /*
          String last = model.preferences.getString("lastimage", null);
          if (last != null) {
            int lastfolderslash = last.lastIndexOf("/");
            String lastfolder = last.substring(0, lastfolderslash);
            try {
              model.commandethreaddatabase.put(new String[]{"chercheFichierDansDatabaseSinonRescanSonDossier", lastfolder, last, ""+currid});
            } catch (InterruptedException e) {
              e.printStackTrace();
            }
          }*/
          needanotherupdate = true;
          clickquicknavmenu = false;
        }
      }
      recti = new RectF(x1, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
      recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
      if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
        RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
        surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
      }
      surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

      mabase += model.GenericCaseH + model.GenericInterSpace;
      if (mabase > mysurf.myheight)
        return;

      ecritoption = "last video";
      if (optionlastvideo > 1)
        ecritoption += " (-" + optionlastvideo + ")";
      y1 = mabase;
      y2 = mabase + model.GenericCaseH;
      taillex = model.Menu1TextePaint.measureText(ecritoption);
      x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
      x2 = mysurf.ScreenWidth - model.GenericInterSpace;
      if (x1 > mysurf.SettingsXmin) {
        x1 = mysurf.SettingsXmin;
      }
      ecritx = x2 - model.GenericInterSpace;
      ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
      if (clickquicknavmenu) {
        if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
          Intent intent = new Intent();
          intent.setAction(Gallery.broadcastname);
          intent.putExtra("goal", "shownextfileinmediastore");
          intent.putExtra("id", currid);
          intent.putExtra("numberlast", optionlastvideo);
          intent.putExtra("wantpicture", false);
          LocalBroadcastManager.getInstance(model.activitycontext).sendBroadcast(intent);
          optionlastvideo++;
          needanotherupdate = true;
          clickquicknavmenu = false;
        }
      }
      recti = new RectF(x1, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
      recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
      if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
        RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
        surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
      }
      surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

      mabase += model.GenericCaseH + model.GenericInterSpace;
      if (mabase > mysurf.myheight)
        return;

      ecritoption = "random";
      y1 = mabase;
      y2 = mabase + model.GenericCaseH;
      taillex = model.Menu1TextePaint.measureText(ecritoption);
      x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
      x2 = mysurf.ScreenWidth - model.GenericInterSpace;
      if (x1 > mysurf.SettingsXmin) {
        x1 = mysurf.SettingsXmin;
      }
      ecritx = x2 - model.GenericInterSpace;
      ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
      if (clickquicknavmenu) {
        if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
          int d = Gallery.rand.nextInt(model.folderCount);
          model.showrandomfilewhenfolderready = true;
          model.changeBigPicture(currid, d, 0, 0, 0, true, false);
          needanotherupdate = true;
          clickquicknavmenu = false;
        }
      }
      recti = new RectF(x1, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
      recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
      if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
        RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
        surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
      }
      surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

      mabase += model.GenericCaseH + model.GenericInterSpace;
      if (mabase > mysurf.myheight)
        return;

    }

  }

  private void menuonline(Canvas surfacecanvas, int foldercount) {
    ecritoption = "online media";
    y1 = mabase;
    y2 = mabase + model.GenericCaseH;
    taillex = model.Menu1TextePaint.measureText(ecritoption);
    x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
    x2 = mysurf.ScreenWidth - model.GenericInterSpace;
    if (x1 > mysurf.SettingsXmin) {
      x1 = mysurf.SettingsXmin;
    }
    ecritx = x2 - model.GenericInterSpace;
    ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
    if (clickquicknavmenu) {
      if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
        mysurf.optionshowonline = !mysurf.optionshowonline;
        needanotherupdate = true;
        clickquicknavmenu = false;
      }
    }
    recti = new RectF(x1, y1, x2, y2);
    surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu1BgPainta);
    recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
    surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu1BgPainto);
    if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
      RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
      surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
    }
    surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

    mabase += model.GenericCaseH + model.GenericInterSpace;
    if (mabase > mysurf.myheight)
      return;

    if (mysurf.optionshowonline) {

      ecritoption = "add online media source";
      y1 = mabase;
      y2 = mabase + model.GenericCaseH;
      taillex = model.Menu1TextePaint.measureText(ecritoption);
      x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
      x2 = mysurf.ScreenWidth - model.GenericInterSpace;
      if (x1 > mysurf.SettingsXmin) {
        x1 = mysurf.SettingsXmin;
      }
      ecritx = x2 - model.GenericInterSpace;
      ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
      if (clickquicknavmenu) {
        if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
          Intent intent = new Intent();
          intent.setAction(Gallery.broadcastname);
          intent.putExtra("goal", "addonline");
          intent.putExtra("id", currid);
          LocalBroadcastManager.getInstance(model.activitycontext).sendBroadcast(intent);
          needanotherupdate = true;
          clickquicknavmenu = false;
        }
      }
      recti = new RectF(x1, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
      recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
      if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
        RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
        surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
      }
      surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

      mabase += model.GenericCaseH + model.GenericInterSpace;
      if (mabase > mysurf.myheight)
        return;

      ecritoption = "close online folders";
      y1 = mabase;
      y2 = mabase + model.GenericCaseH;
      taillex = model.Menu1TextePaint.measureText(ecritoption);
      x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
      x2 = mysurf.ScreenWidth - model.GenericInterSpace;
      if (x1 > mysurf.SettingsXmin) {
        x1 = mysurf.SettingsXmin;
      }
      ecritx = x2 - model.GenericInterSpace;
      ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
      if (clickquicknavmenu) {
        if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
          try {
            model.commandethreaddatabase.put(new String[]{"closeonlinefolders"});
          } catch (InterruptedException e) {
            e.printStackTrace();
          }
          needanotherupdate = true;
          clickquicknavmenu = false;
        }
      }
      recti = new RectF(x1, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
      recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
      if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
        RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
        surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
      }
      surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

      mabase += model.GenericCaseH + model.GenericInterSpace;
      if (mabase > mysurf.myheight)
        return;

      for (int i = 0; i < model.nombreonline ; i++) {
        String monoption = model.preferences.getString("mediaonline" + i, null);
        if (monoption == null)
          continue;
        boolean autoload = false;
        boolean update = false;
        if (monoption.endsWith(".txt"))
          update = true;

        y1 = mabase;
        y2 = mabase + model.GenericCaseH;
        ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;

        String texte = "edit";
        if (Gallery.onlinemediaupdateapachefolderlist == model.onlinemediabutton) {
          if (update)
            texte = "upd";
          else
            texte = "-";
        } else if (model.onlinemediabutton == Gallery.onlinemediaautoloadatstartup) {
          autoload = model.preferences.getBoolean("autoload" + i, false);
          if (autoload) {
            texte = "auto";
          } else {
            texte = "-";
          }
        }
        taillex = model.Menu1TextePaint.measureText("E");
        x1 = (mysurf.ScreenWidth - model.GenericInterSpace + mysurf.SettingsXmin) * 0.5f + model.GenericInterSpace * 0.5f;
        x2 = mysurf.ScreenWidth - model.GenericInterSpace;
        ecritx = x2;
        if (clickquicknavmenu) {
          if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
            if (model.onlinemediabutton == Gallery.onlinemediaupdateapachefolderlist) {
              if (monoption.endsWith(".txt")) {
                try {
                  model.commandethreaddatabase.put(new String[]{"updateapachefolderlist", String.valueOf(currid), monoption});
                } catch (InterruptedException e) {
                  e.printStackTrace();
                }
              } else {
                model.message("only for *.txt folderlists");
              }
            } else if (model.onlinemediabutton == Gallery.onlinemediaautoloadatstartup) {
              autoload = !autoload;
              if (autoload) {
                model.message("automatic load at startup");
              } else {
                model.message("click it in the menu\nto load it");
              }
              model.preferences.edit().putBoolean("autoload" + i, autoload).apply();
            } else {
              Intent intent = new Intent();
              intent.setAction(Gallery.broadcastname);
              intent.putExtra("goal", "addonline");
              intent.putExtra("id", currid);
              intent.putExtra("memory", i);
              LocalBroadcastManager.getInstance(model.activitycontext).sendBroadcast(intent);
            }
            needanotherupdate = true;
            clickquicknavmenu = false;
          }
        }
        //recti = new RectF(x1, y1, x2, y2);
        //surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
        recti = new RectF(x1, y1, x2, y2);
        surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
        surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
        if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
          RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
          surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
        }
        surfacecanvas.drawText(texte, ecritx, ecrity, model.Menu1TextePaint);

        taillex = model.Menu1TextePaint.measureText(monoption);
        x2 = x1 - model.GenericInterSpace * 0.50f;
        x1 = x2 - model.GenericInterSpace * 2.0f - taillex;
        if (x1 > mysurf.SettingsXmin)
          x1 = mysurf.SettingsXmin;
        ecritx = x2 - model.GenericInterSpace;
        if (clickquicknavmenu) {
          if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
            try {
              model.commandethreaddatabase.put(new String[]{"browseonline", String.valueOf(currid), monoption});
            } catch (InterruptedException e) {
              e.printStackTrace();
            }
            needanotherupdate = true;
            clickquicknavmenu = false;
          }
        }
        recti = new RectF(x1, y1, x2, y2);
        surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
        recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
        surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
        if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
          RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
          surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
        }
        surfacecanvas.drawText(monoption, ecritx, ecrity, model.Menu1TextePaint);

        mabase += model.GenericCaseH + model.GenericInterSpace;
        if (mabase > mysurf.myheight)
          break;

      }

      if (model.onlinemediabutton == Gallery.onlinemediaremove)
        ecritoption = "menu button : edit/remove";
      else if (model.onlinemediabutton == Gallery.onlinemediaautoloadatstartup)
        ecritoption = "menu button : autostart";
      else if (model.onlinemediabutton == Gallery.onlinemediaupdateapachefolderlist)
        ecritoption = "menu button : update";
      y1 = mabase;
      y2 = mabase + model.GenericCaseH;
      taillex = model.Menu1TextePaint.measureText(ecritoption);
      x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
      x2 = mysurf.ScreenWidth - model.GenericInterSpace;
      if (x1 > mysurf.SettingsXmin) {
        x1 = mysurf.SettingsXmin;
      }
      ecritx = x2 - model.GenericInterSpace;
      ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
      if (clickquicknavmenu) {
        if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
          model.onlinemediabutton = (model.onlinemediabutton + 1) % 3;
          if (model.onlinemediabutton == Gallery.onlinemediaremove)
            model.message("edit or remove entries\nwith E");
          else if (model.onlinemediabutton == Gallery.onlinemediaautoloadatstartup)
            model.message("load entry\non app startup\nwith A");
          else if (model.onlinemediabutton == Gallery.onlinemediaupdateapachefolderlist)
            model.message("automatically updates\nyour servers folder lists *.txt\nwith U");
          needanotherupdate = true;
          clickquicknavmenu = false;
        }
      }
      recti = new RectF(x1, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
      recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
      if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
        RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
        surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
      }
      surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

      mabase += model.GenericCaseH + model.GenericInterSpace;
      if (mabase > mysurf.myheight)
        return;

      if (model.autoloadlastonlinemediaviewed)
        ecritoption = "autoload last viewed";
      else
        ecritoption = "no autoload";
      y1 = mabase;
      y2 = mabase + model.GenericCaseH;
      taillex = model.Menu1TextePaint.measureText(ecritoption);
      x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
      x2 = mysurf.ScreenWidth - model.GenericInterSpace;
      if (x1 > mysurf.SettingsXmin) {
        x1 = mysurf.SettingsXmin;
      }
      ecritx = x2 - model.GenericInterSpace;
      ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
      if (clickquicknavmenu) {
        if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
          model.autoloadlastonlinemediaviewed = !model.autoloadlastonlinemediaviewed;
          if (model.autoloadlastonlinemediaviewed) {
            model.message("Last viewed online media" +
                "\nwill be shown on app startup.");
          } else {
            model.message("Last viewed online media" +
                "\nwon't be shown on app startup.");
            model.forgetlastbrowsedonline();
          }
          model.preferences.edit().putBoolean("autoloadlastonlinemediaviewed", model.autoloadlastonlinemediaviewed).apply();
          needanotherupdate = true;
          clickquicknavmenu = false;
        }
      }
      recti = new RectF(x1, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
      recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
      if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
        RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
        surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
      }
      surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

      mabase += model.GenericCaseH + model.GenericInterSpace;
      if (mabase > mysurf.myheight)
        return;

    }

  }

  private void menubookmark(Canvas surfacecanvas, int foldercount) {
    if (mysurf.optionshowbookmarked && model.nombredossierbookmarked > 0) {
      if (model.nombredossierbookmarked > 1)
        ecritoption = model.nombredossierbookmarked + " bookmarks";
      else
        ecritoption = model.nombredossierbookmarked + " bookmark";
    } else
      ecritoption = "bookmark";
    y1 = mabase;
    y2 = mabase + model.GenericCaseH;
    taillex = model.Menu1TextePaint.measureText(ecritoption);
    x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
    x2 = mysurf.ScreenWidth - model.GenericInterSpace;
    if (x1 > mysurf.SettingsXmin) {
      x1 = mysurf.SettingsXmin;
    }
    ecritx = x2 - model.GenericInterSpace;
    ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
    if (clickquicknavmenu) {
      if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
        mysurf.optionshowbookmarked = !mysurf.optionshowbookmarked;
        needanotherupdate = true;
        clickquicknavmenu = false;
      }
    }
    recti = new RectF(x1, y1, x2, y2);
    surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu1BgPainta);
    recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
    surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu1BgPainto);
    if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
      RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
      surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
    }
    surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

    mabase += model.GenericCaseH + model.GenericInterSpace;
    if (mabase > mysurf.myheight)
      return;

    if (mysurf.optionshowbookmarked) {

      ecritoption = "add bookmark";
      y1 = mabase;
      y2 = mabase + model.GenericCaseH;
      taillex = model.Menu1TextePaint.measureText(ecritoption);
      x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
      x2 = mysurf.ScreenWidth - model.GenericInterSpace;
      if (x1 > mysurf.SettingsXmin) {
        x1 = mysurf.SettingsXmin;
      }
      ecritx = x2 - model.GenericInterSpace;
      ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
      if (clickquicknavmenu) {
        if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
          try {
            model.commandethreaddatabase.put(new String[]{"addbookmark", model.getOrnderAddress(mysurf.ordnerIndex), model.getMediaAddress(mysurf.ordnerIndex, mysurf.mediaIndex)});
          } catch (InterruptedException e) {
            e.printStackTrace();
          }
          needanotherupdate = true;
          clickquicknavmenu = false;
        }
      }
      recti = new RectF(x1, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
      recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
      if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
        RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
        surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
      }
      surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

      mabase += model.GenericCaseH + model.GenericInterSpace;
      if (mabase > mysurf.myheight)
        return;


      if (model.optionshowbookmarkeddisplay)
        ecritoption = "hide";
      else
        ecritoption = "display";
      y1 = mabase;
      y2 = mabase + model.GenericCaseH;
      taillex = model.Menu1TextePaint.measureText(ecritoption);
      x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
      x2 = mysurf.ScreenWidth - model.GenericInterSpace;
      if (x1 > mysurf.SettingsXmin) {
        x1 = mysurf.SettingsXmin;
      }
      ecritx = x2 - model.GenericInterSpace;
      ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
      if (clickquicknavmenu) {
        if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
          model.optionshowbookmarkeddisplay = !model.optionshowbookmarkeddisplay;
          if (model.optionshowbookmarkeddisplay) {
            String colladdress = Gallery.virtualBookmarkFolder;
            try {
              model.commandethreaddatabase.put(new String[]{"LoadCollectionFromDiskOrTemp", "load", colladdress, "false"});
              model.commandethreaddatabase.put(new String[]{"addcollectionfolderstodisplay", String.valueOf(currid), colladdress, String.valueOf(mysurf.ordnerIndex + 1)});
            } catch (InterruptedException e) {
              e.printStackTrace();
            }
          } else {
            String colladdress = Gallery.virtualBookmarkFolder;
            try {
              model.commandethreaddatabase.put(new String[]{"removedisplayedcollectionfolders", colladdress});
              model.commandethreaddatabase.put(new String[]{"closecollection", colladdress});
            } catch (InterruptedException e) {
              e.printStackTrace();
            }
          }
          /*if (model.optionshowbookmarkeddisplay) {
            Collection collection = new Collection();
            collection.address = Gallery.virtualBookmarkFolder + ".sel";
            collection.printName = "bookmarks";
            collection.reloadBookmarksFromPreferences(model.preferences, model.db);
            if (collection.elementList.size() == 0) {
              model.message("empty bookmark list");
              model.optionshowbookmarkeddisplay = false;
            } else {
              int currfolder = mysurf.ordnerIndex + 1;
              model.collectionsToDisplay(collection.address, currfolder);
              List<Ordner> gallery = collection.collectionToDisplay(null);
              for (int i = 0 ; i < gallery.size() ; i++) {
                Ordner ordner = gallery.get(i);
                model.addFolder(currfolder + i, ordner);
              }
              model.changeBigPicture(currid, currfolder, 0, -1, 0, false, false);
              model.optionshowbookmarkeddisplay = true;
            }
          } else {
            int found = model.findFolder(Gallery.virtualBookmarkFolder);
            if (found != -1)
              model.removeFolder(found);
            model.optionshowbookmarkeddisplay = false;
          }*/
          needanotherupdate = true;
          clickquicknavmenu = false;
        }
      }
      recti = new RectF(x1, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
      recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
      if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
        RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
        surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
      }
      surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

      mabase += model.GenericCaseH + model.GenericInterSpace;
      if (mabase > mysurf.myheight)
        return;

      if (model.optionshowbookmarkeddisplay
          && mysurf.ordnerIndexAddress.equals(Gallery.virtualBookmarkFolder)) {

        ecritoption = "goto";
        y1 = mabase;
        y2 = mabase + model.GenericCaseH;
        taillex = model.Menu1TextePaint.measureText(ecritoption);
        x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
        x2 = mysurf.ScreenWidth - model.GenericInterSpace;
        if (x1 > mysurf.SettingsXmin) {
          x1 = mysurf.SettingsXmin;
        }
        ecritx = x2 - model.GenericInterSpace;
        ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
        if (clickquicknavmenu) {
          if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
            String mondossier = "";
            String monfichier = "";
            for (int i = 0; i < model.nombredossierbookmarked ; i++) {
              monfichier = model.preferences.getString("fichierbookmarked" + String.valueOf(i), "");
              if (monfichier.equals(mysurf.mediaIndexAddress)) {
                mondossier = model.preferences.getString("dossierbookmarked" + String.valueOf(i), "");
                break;
              }
            }
            try {
              model.commandethreaddatabase.put(new String[]{"findMediaInAnotherOrdner", String.valueOf(currid), mondossier, monfichier});
            } catch (InterruptedException e) {
              e.printStackTrace();
            }
            needanotherupdate = true;
            clickquicknavmenu = false;
          }
        }
        recti = new RectF(x1, y1, x2, y2);
        surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
        recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
        surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
        if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
          RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
          surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
        }
        surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

        mabase += model.GenericCaseH + model.GenericInterSpace;
        if (mabase > mysurf.myheight)
          return;

        ecritoption = "remove";
        y1 = mabase;
        y2 = mabase + model.GenericCaseH;
        taillex = model.Menu1TextePaint.measureText(ecritoption);
        x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
        x2 = mysurf.ScreenWidth - model.GenericInterSpace;
        if (x1 > mysurf.SettingsXmin) {
          x1 = mysurf.SettingsXmin;
        }
        ecritx = x2 - model.GenericInterSpace;
        ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
        if (clickquicknavmenu) {
          if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
            for (int i = 0; i < model.nombredossierbookmarked ; i++) {
              String monfichier = model.preferences.getString("fichierbookmarked" + String.valueOf(i), "");
              if (monfichier.equals(mysurf.mediaIndexAddress)) {
                try {
                  model.commandethreaddatabase.put(new String[]{"deletebookmark", String.valueOf(i)});
                } catch (InterruptedException e) {
                  e.printStackTrace();
                }
                if (model.optionshowbookmarkeddisplay) {
                  int d = model.findFolder(Gallery.virtualBookmarkFolder);
                  if (d == -1) {
                    llog.d(TAG, "bookmark folder not found");
                  } else {
                    int f = model.findMediaAddress(d, monfichier);
                    if (f < 0) {
                      llog.d(TAG, currid + " found bookmark " + d + " : " + Gallery.virtualBookmarkFolder + " but not fichier " + monfichier);
                    } else {
                      model.removeMediaFromGallery(Gallery.virtualBookmarkFolder, monfichier, d, f);
                    }
                  }
                }
                model.message("bookmark removed");
                break;
              }
            }
            model.nombredossierbookmarked = model.preferences.getInt("nombredossierbookmarked", model.nombredossierbookmarked);
            needanotherupdate = true;
            clickquicknavmenu = false;
          }
        }
        recti = new RectF(x1, y1, x2, y2);
        surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
        recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
        surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
        if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
          RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
          surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
        }
        surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

        mabase += model.GenericCaseH + model.GenericInterSpace;
        if (mabase > mysurf.myheight)
          return;

      }

      ecritoption = "list";
      y1 = mabase;
      y2 = mabase + model.GenericCaseH;
      taillex = model.Menu1TextePaint.measureText(ecritoption);
      x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
      x2 = mysurf.ScreenWidth - model.GenericInterSpace;
      if (x1 > mysurf.SettingsXmin) {
        x1 = mysurf.SettingsXmin;
      }
      ecritx = x2 - model.GenericInterSpace;
      ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
      if (clickquicknavmenu) {
        if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
          model.optionshowbookmarkedlist = !model.optionshowbookmarkedlist;
          model.preferences.edit().putBoolean("optionshowbookmarkedlist", model.optionshowbookmarkedlist).apply();
          needanotherupdate = true;
          clickquicknavmenu = false;
        }
      }
      recti = new RectF(x1, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
      recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
      if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
        RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
        surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
      }
      surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

      mabase += model.GenericCaseH + model.GenericInterSpace;
      if (mabase > mysurf.myheight)
        return;

      if (model.optionshowbookmarkedlist) {

        for (int i = 0; i < model.nombredossierbookmarked ; i++) {
          String mondossier = model.preferences.getString("dossierbookmarked" + String.valueOf(i), "");
          String monfichier = model.preferences.getString("fichierbookmarked" + String.valueOf(i), "");
          String monpprint = monfichier;
          if (monfichier.startsWith("http")) {
            try {
              monpprint = URLDecoder.decode(monfichier, "utf-8");
            } catch (UnsupportedEncodingException e) {
              e.printStackTrace();
              monpprint = monfichier;
            }
          }
          String monoption = monpprint;

          y1 = mabase;
          y2 = mabase + model.GenericCaseH;
          ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;

          taillex = model.Menu1TextePaint.measureText("x");
          x1 = (mysurf.ScreenWidth - model.GenericInterSpace + mysurf.SettingsXmin) * 0.5f + model.GenericInterSpace * 0.5f;
          x2 = mysurf.ScreenWidth - model.GenericInterSpace;
          ecritx = x2;
          if (clickquicknavmenu) {
            if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
              try {
                model.commandethreaddatabase.put(new String[]{"deletebookmark", String.valueOf(i)});
              } catch (InterruptedException e) {
                e.printStackTrace();
              }
              needanotherupdate = true;
              clickquicknavmenu = false;
            }
          }
          //recti = new RectF(x1, y1, x2, y2);
          //surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
          recti = new RectF(x1, y1, x2, y2);
          surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
          if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
            RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
            surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
          }
          surfacecanvas.drawText("X", ecritx, ecrity, model.Menu1TextePaint);

          taillex = model.Menu1TextePaint.measureText(monoption);
          x2 = x1 - model.GenericInterSpace * 0.50f;
          x1 = x2 - model.GenericInterSpace * 2.0f - taillex;
          if (x1 > mysurf.SettingsXmin)
            x1 = mysurf.SettingsXmin;
          ecritx = x2 - model.GenericInterSpace;
          if (clickquicknavmenu) {
            if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
              try {
                model.commandethreaddatabase.put(new String[]{"findMediaInAnotherOrdner", String.valueOf(currid), mondossier, monfichier});
              } catch (InterruptedException e) {
                e.printStackTrace();
              }
              /*int d = model.findFolder(mondossier);
              if (d != -1) {
                llog.d(TAG, currid + " found bookmark " + d + " : " + mondossier + " on cherche fichier " + monfichier);

                int f = model.findMediaAddress(d, monfichier);
                if (f < 0) {
                  model.changeBigPicture(currid, d, 0, f, 0, false, false);
                  model.bookmarkshowthisfilewhenfolderready = monfichier;
                  try {
                    model.commandethreadbrowser.put(new String[]{String.valueOf(currid), "dontmissupdate"});
                    //model.commandethreaddatabase.put(new String[]{"searchfile", monfichier, String.valueOf(currid), String.valueOf(d), String.valueOf(0)});
                  } catch (InterruptedException e) {
                    e.printStackTrace();
                  }
                } else {
                  model.changeBigPicture(currid, d, 0, f, 0, true, false);
                }

              }*/

              needanotherupdate = true;
              clickquicknavmenu = false;
            }
          }
          recti = new RectF(x1, y1, x2, y2);
          surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
          recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
          surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
          if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
            RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
            surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
          }
          surfacecanvas.drawText(monoption, ecritx, ecrity, model.Menu1TextePaint);

          mabase += model.GenericCaseH + model.GenericInterSpace;
          if (mabase > mysurf.myheight)
            break;

        }
      }
    }

  }

  private void menuhide(Canvas surfacecanvas, int foldercount) {
    ecritoption = "hide";
    y1 = mabase;
    y2 = mabase + model.GenericCaseH;
    taillex = model.Menu1TextePaint.measureText(ecritoption);
    x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
    x2 = mysurf.ScreenWidth - model.GenericInterSpace;
    if (x1 > mysurf.SettingsXmin) {
      x1 = mysurf.SettingsXmin;
    }
    ecritx = x2 - model.GenericInterSpace;
    ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
    if (clickquicknavmenu) {
      if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
        mysurf.optionshowhidden = !mysurf.optionshowhidden;
        if (mysurf.optionshowhidden) {
          model.message("please click on a folder name section\nto hide it along with all its subfolders");
        }
        needanotherupdate = true;
        clickquicknavmenu = false;
      }
    }
    recti = new RectF(x1, y1, x2, y2);
    surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu1BgPainta);
    recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
    surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu1BgPainto);
    if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
      RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
      surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
    }
    surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

    mabase += model.GenericCaseH + model.GenericInterSpace;
    if (mabase > mysurf.myheight)
      return;

    if (mysurf.optionshowhidden) {

      if (mysurf.basisfolderclicked == null) {
        ecritoption = "hide this folder";
      } else {
        ecritoption = "hide " + mysurf.basisfolderclicked;
      }
      y1 = mabase;
      y2 = mabase + model.GenericCaseH;
      taillex = model.Menu1TextePaint.measureText(ecritoption);
      x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
      x2 = mysurf.ScreenWidth - model.GenericInterSpace;
      if (x1 > mysurf.SettingsXmin) {
        x1 = mysurf.SettingsXmin;
      }
      ecritx = x2 - model.GenericInterSpace;
      ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
      if (clickquicknavmenu) {
        if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
          if (mysurf.basisfolderclicked == null) {
            String foldername = model.getOrnderAddress(mysurf.ordnerIndex);
            if (foldername != null) {
              try {
                model.commandethreaddatabase.put(new String[]{"foldthis", foldername});
              } catch (InterruptedException e) {
                e.printStackTrace();
              }
            }
          } else {
            try {
              model.commandethreaddatabase.put(new String[]{"foldthis", mysurf.basisfolderclicked});
            } catch (InterruptedException e) {
              e.printStackTrace();
            }
          }
          needanotherupdate = true;
          clickquicknavmenu = false;
        }
      }
      recti = new RectF(x1, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
      recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
      if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
        RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
        surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
      }
      surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

      mabase += model.GenericCaseH + model.GenericInterSpace;
      if (mabase > mysurf.myheight)
        return;

    }

  }

  private void menudraw(Canvas surfacecanvas, int foldercount) {
    ecritoption = "draw";
    y1 = mabase;
    y2 = mabase + model.GenericCaseH;
    taillex = model.Menu1TextePaint.measureText(ecritoption);
    x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
    x2 = mysurf.ScreenWidth - model.GenericInterSpace;
    if (x1 > mysurf.SettingsXmin) {
      x1 = mysurf.SettingsXmin;
    }
    ecritx = x2 - model.GenericInterSpace;
    ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
    if (clickquicknavmenu) {
      if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
        mysurf.OptionMenuShown = false;
        mysurf.showfoldernames = false;
        mysurf.optionshowbookmarked = false;
        mysurf.optionshowdelete = false;
        mysurf.optionshowlockscreen = false;
        mysurf.optionshowmove = false;
        mysurf.optionshowrescan = false;
        mysurf.optionshowselection = false;
        mysurf.optionshowshare = false;
        mysurf.optionshowwallpaper = false;
        try {
          model.commandethreadminiature.put(new String[]{"cleanup", "force", "donotupdateafterwards"});
        } catch (InterruptedException e) {
          e.printStackTrace();
        }
        if (mysurf.touslesdossierssplitviewbitmap != null) {
          if (!mysurf.touslesdossierssplitviewbitmap.isRecycled()) {
            mysurf.touslesdossierssplitviewbitmap.recycle();
            mysurf.touslesdossierssplitviewbitmap = null;
          }
        }
        Intent intent = new Intent();
        intent.setAction(Gallery.broadcastname);
        intent.putExtra("goal", "startdrawing");
        intent.putExtra("id", currid);
        intent.putExtra("cefichier", model.getMediaAddress(mysurf.ordnerIndex, mysurf.mediaIndex));
        LocalBroadcastManager.getInstance(model.activitycontext).sendBroadcast(intent);
        needanotherupdate = true;
        clickquicknavmenu = false;
      }
    }
    recti = new RectF(x1, y1, x2, y2);
    surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu1BgPainta);
    recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
    surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu1BgPainto);
    if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
      RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
      surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
    }
    surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

    mabase += model.GenericCaseH + model.GenericInterSpace;

  }

  private void menugame(Canvas surfacecanvas, int foldercount) {
    ecritoption = "mini game";
    y1 = mabase;
    y2 = mabase + model.GenericCaseH;
    taillex = model.Menu1TextePaint.measureText(ecritoption);
    x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
    x2 = mysurf.ScreenWidth - model.GenericInterSpace;
    if (x1 > mysurf.SettingsXmin) {
      x1 = mysurf.SettingsXmin;
    }
    ecritx = x2 - model.GenericInterSpace;
    ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
    if (clickquicknavmenu) {
      if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
        mysurf.optionshowminigame = !mysurf.optionshowminigame;
        needanotherupdate = true;
        clickquicknavmenu = false;
      }
    }
    recti = new RectF(x1, y1, x2, y2);
    surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu1BgPainta);
    recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
    surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu1BgPainto);
    if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
      RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
      surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
    }
    surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

    mabase += model.GenericCaseH + model.GenericInterSpace;
    if (mabase > mysurf.myheight)
      return;

    if (mysurf.optionshowminigame) {

      ecritoption = "puzzle";
      y1 = mabase;
      y2 = mabase + model.GenericCaseH;
      taillex = model.Menu1TextePaint.measureText(ecritoption);
      x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
      x2 = mysurf.ScreenWidth - model.GenericInterSpace;
      if (x1 > mysurf.SettingsXmin) {
        x1 = mysurf.SettingsXmin;
      }
      ecritx = x2 - model.GenericInterSpace;
      ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
      if (clickquicknavmenu) {
        if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
          if (mysurf.touslesdossierssplitviewbitmap != null) {
            if (!mysurf.touslesdossierssplitviewbitmap.isRecycled()) {
              mysurf.touslesdossierssplitviewbitmap.recycle();
              mysurf.touslesdossierssplitviewbitmap = null;
            }
          }
          Intent intent = new Intent();
          intent.setAction(Gallery.broadcastname);
          intent.putExtra("goal", "startplaying");
          intent.putExtra("id", currid);
          intent.putExtra("game", "puzzle");
          intent.putExtra("cefichier", model.getMediaAddress(mysurf.ordnerIndex, mysurf.mediaIndex));
          LocalBroadcastManager.getInstance(model.activitycontext).sendBroadcast(intent);
          needanotherupdate = true;
          clickquicknavmenu = false;
        }
      }
      recti = new RectF(x1, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
      recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
      if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
        RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
        surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
      }
      surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

      mabase += model.GenericCaseH + model.GenericInterSpace;
      if (mabase > mysurf.myheight)
        return;

      ecritoption = "swap tiles";
      y1 = mabase;
      y2 = mabase + model.GenericCaseH;
      taillex = model.Menu1TextePaint.measureText(ecritoption);
      x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
      x2 = mysurf.ScreenWidth - model.GenericInterSpace;
      if (x1 > mysurf.SettingsXmin) {
        x1 = mysurf.SettingsXmin;
      }
      ecritx = x2 - model.GenericInterSpace;
      ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
      if (clickquicknavmenu) {
        if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
          if (mysurf.touslesdossierssplitviewbitmap != null) {
            if (!mysurf.touslesdossierssplitviewbitmap.isRecycled()) {
              mysurf.touslesdossierssplitviewbitmap.recycle();
              mysurf.touslesdossierssplitviewbitmap = null;
            }
          }
          Intent intent = new Intent();
          intent.setAction(Gallery.broadcastname);
          intent.putExtra("goal", "startplaying");
          intent.putExtra("id", currid);
          intent.putExtra("game", "swap");
          intent.putExtra("cefichier", model.getMediaAddress(mysurf.ordnerIndex, mysurf.mediaIndex));
          LocalBroadcastManager.getInstance(model.activitycontext).sendBroadcast(intent);
          needanotherupdate = true;
          clickquicknavmenu = false;
        }
      }
      recti = new RectF(x1, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
      recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
      if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
        RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
        surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
      }
      surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

      mabase += model.GenericCaseH + model.GenericInterSpace;
      if (mabase > mysurf.myheight)
        return;

      ecritoption = "bounce hit";
      y1 = mabase;
      y2 = mabase + model.GenericCaseH;
      taillex = model.Menu1TextePaint.measureText(ecritoption);
      x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
      x2 = mysurf.ScreenWidth - model.GenericInterSpace;
      if (x1 > mysurf.SettingsXmin) {
        x1 = mysurf.SettingsXmin;
      }
      ecritx = x2 - model.GenericInterSpace;
      ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
      if (clickquicknavmenu) {
        if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
          if (mysurf.touslesdossierssplitviewbitmap != null) {
            if (!mysurf.touslesdossierssplitviewbitmap.isRecycled()) {
              mysurf.touslesdossierssplitviewbitmap.recycle();
              mysurf.touslesdossierssplitviewbitmap = null;
            }
          }
          Intent intent = new Intent();
          intent.setAction(Gallery.broadcastname);
          intent.putExtra("goal", "startplaying");
          intent.putExtra("id", currid);
          intent.putExtra("game", "hit");
          intent.putExtra("cefichier", model.getMediaAddress(mysurf.ordnerIndex, mysurf.mediaIndex));
          LocalBroadcastManager.getInstance(model.activitycontext).sendBroadcast(intent);
          needanotherupdate = true;
          clickquicknavmenu = false;
        }
      }
      recti = new RectF(x1, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
      recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
      if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
        RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
        surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
      }
      surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

      mabase += model.GenericCaseH + model.GenericInterSpace;
      if (mabase > mysurf.myheight)
        return;

      ecritoption = "bounce curve";
      y1 = mabase;
      y2 = mabase + model.GenericCaseH;
      taillex = model.Menu1TextePaint.measureText(ecritoption);
      x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
      x2 = mysurf.ScreenWidth - model.GenericInterSpace;
      if (x1 > mysurf.SettingsXmin) {
        x1 = mysurf.SettingsXmin;
      }
      ecritx = x2 - model.GenericInterSpace;
      ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
      if (clickquicknavmenu) {
        if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
          if (mysurf.touslesdossierssplitviewbitmap != null) {
            if (!mysurf.touslesdossierssplitviewbitmap.isRecycled()) {
              mysurf.touslesdossierssplitviewbitmap.recycle();
              mysurf.touslesdossierssplitviewbitmap = null;
            }
          }
          Intent intent = new Intent();
          intent.setAction(Gallery.broadcastname);
          intent.putExtra("goal", "startplaying");
          intent.putExtra("id", currid);
          intent.putExtra("game", "curve");
          intent.putExtra("cefichier", model.getMediaAddress(mysurf.ordnerIndex, mysurf.mediaIndex));
          LocalBroadcastManager.getInstance(model.activitycontext).sendBroadcast(intent);
          needanotherupdate = true;
          clickquicknavmenu = false;
        }
      }
      recti = new RectF(x1, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
      recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
      if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
        RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
        surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
      }
      surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

      mabase += model.GenericCaseH + model.GenericInterSpace;
      if (mabase > mysurf.myheight)
        return;

      ecritoption = "tilt balls";
      y1 = mabase;
      y2 = mabase + model.GenericCaseH;
      taillex = model.Menu1TextePaint.measureText(ecritoption);
      x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
      x2 = mysurf.ScreenWidth - model.GenericInterSpace;
      if (x1 > mysurf.SettingsXmin) {
        x1 = mysurf.SettingsXmin;
      }
      ecritx = x2 - model.GenericInterSpace;
      ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
      if (clickquicknavmenu) {
        if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
          if (mysurf.touslesdossierssplitviewbitmap != null) {
            if (!mysurf.touslesdossierssplitviewbitmap.isRecycled()) {
              mysurf.touslesdossierssplitviewbitmap.recycle();
              mysurf.touslesdossierssplitviewbitmap = null;
            }
          }
          Intent intent = new Intent();
          intent.setAction(Gallery.broadcastname);
          intent.putExtra("goal", "startplaying");
          intent.putExtra("id", currid);
          intent.putExtra("game", "tilt");
          intent.putExtra("cefichier", model.getMediaAddress(mysurf.ordnerIndex, mysurf.mediaIndex));
          LocalBroadcastManager.getInstance(model.activitycontext).sendBroadcast(intent);
          needanotherupdate = true;
          clickquicknavmenu = false;
        }
      }
      recti = new RectF(x1, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
      recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
      if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
        RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
        surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
      }
      surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

      mabase += model.GenericCaseH + model.GenericInterSpace;
      if (mabase > mysurf.myheight)
        return;

      ecritoption = "tilt avoid";
      y1 = mabase;
      y2 = mabase + model.GenericCaseH;
      taillex = model.Menu1TextePaint.measureText(ecritoption);
      x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
      x2 = mysurf.ScreenWidth - model.GenericInterSpace;
      if (x1 > mysurf.SettingsXmin) {
        x1 = mysurf.SettingsXmin;
      }
      ecritx = x2 - model.GenericInterSpace;
      ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
      if (clickquicknavmenu) {
        if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
          if (mysurf.touslesdossierssplitviewbitmap != null) {
            if (!mysurf.touslesdossierssplitviewbitmap.isRecycled()) {
              mysurf.touslesdossierssplitviewbitmap.recycle();
              mysurf.touslesdossierssplitviewbitmap = null;
            }
          }
          Intent intent = new Intent();
          intent.setAction(Gallery.broadcastname);
          intent.putExtra("goal", "startplaying");
          intent.putExtra("id", currid);
          intent.putExtra("game", "avoid");
          intent.putExtra("cefichier", model.getMediaAddress(mysurf.ordnerIndex, mysurf.mediaIndex));
          LocalBroadcastManager.getInstance(model.activitycontext).sendBroadcast(intent);
          needanotherupdate = true;
          clickquicknavmenu = false;
        }
      }
      recti = new RectF(x1, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
      recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
      if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
        RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
        surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
      }
      surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

      mabase += model.GenericCaseH + model.GenericInterSpace;
      if (mabase > mysurf.myheight)
        return;



      y1 = mabase;
      y2 = mabase + model.GenericCaseH;
      ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;

      ecritoption = "+";
      taillex = model.Menu1TextePaint.measureText(ecritoption);
      x1 = (mysurf.ScreenWidth - model.GenericInterSpace + mysurf.SettingsXmin) * 0.5f + model.GenericInterSpace * 0.5f;
      x2 = mysurf.ScreenWidth - model.GenericInterSpace;
      ecritx = x2;
      if (clickquicknavmenu) {
        if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
          model.gamecolumns += 1;
          model.preferences.edit().putInt("gamecolumns", model.gamecolumns).apply();
          needanotherupdate = true;
          clickquicknavmenu = false;
        }
      }
      recti = new RectF(x1, y1, x2, y2);
      //surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
      //recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
      if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
        RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
        surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
      }
      surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

      ecritoption = model.gamecolumns + " -";
      taillex = model.Menu1TextePaint.measureText(ecritoption);
      x2 = x1 - model.GenericInterSpace;
      x1 = x2 - model.GenericInterSpace * 2.0f - taillex;
      if (x1 > mysurf.SettingsXmin)
        x1 = mysurf.SettingsXmin;
      ecritx = x2 - model.GenericInterSpace;
      if (clickquicknavmenu) {
        if (mysurf.SettingsXmin < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
          if (model.gamecolumns > 1)
            model.gamecolumns -= 1;
          model.preferences.edit().putInt("gamecolumns", model.gamecolumns).apply();
          needanotherupdate = true;
          clickquicknavmenu = false;
        }
      }
      recti = new RectF(x1, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
      recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
      if ((model.isandroidtv || model.keypadnavactive) && mysurf.SettingsXmin < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
        RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
        surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
      }
      surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

      mabase += model.GenericCaseH + model.GenericInterSpace;
      if (mabase > mysurf.myheight)
        return;

      y1 = mabase;
      y2 = mabase + model.GenericCaseH;
      ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;

      ecritoption = "+";
      taillex = model.Menu1TextePaint.measureText(ecritoption);
      x1 = (mysurf.ScreenWidth - model.GenericInterSpace + mysurf.SettingsXmin) * 0.5f + model.GenericInterSpace * 0.5f;
      x2 = mysurf.ScreenWidth - model.GenericInterSpace;
      ecritx = x2;
      if (clickquicknavmenu) {
        if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
          model.gamelines += 1;
          model.preferences.edit().putInt("gamelines", model.gamelines).apply();
          needanotherupdate = true;
          clickquicknavmenu = false;
        }
      }
      recti = new RectF(x1, y1, x2, y2);
      //surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
      //recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
      if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
        RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
        surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
      }
      surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

      ecritoption = model.gamelines + " -";
      taillex = model.Menu1TextePaint.measureText(ecritoption);
      x2 = x1 - model.GenericInterSpace;
      x1 = x2 - model.GenericInterSpace * 2.0f - taillex;
      if (x1 > mysurf.SettingsXmin)
        x1 = mysurf.SettingsXmin;
      ecritx = x2 - model.GenericInterSpace;
      if (clickquicknavmenu) {
        if (mysurf.SettingsXmin < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
          if (model.gamelines > 1)
            model.gamelines -= 1;
          model.preferences.edit().putInt("gamelines", model.gamelines).apply();
          needanotherupdate = true;
          clickquicknavmenu = false;
        }
      }
      recti = new RectF(x1, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
      recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
      if ((model.isandroidtv || model.keypadnavactive) && mysurf.SettingsXmin < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
        RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
        surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
      }
      surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

      mabase += model.GenericCaseH + model.GenericInterSpace;
      if (mabase > mysurf.myheight)
        return;

    }
  }

  private void menucollection(Canvas surfacecanvas, int foldercount) {
    ecritoption = "collections";
    y1 = mabase;
    y2 = mabase + model.GenericCaseH;
    taillex = model.Menu1TextePaint.measureText(ecritoption);
    x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
    x2 = mysurf.ScreenWidth - model.GenericInterSpace;
    if (x1 > mysurf.SettingsXmin) {
      x1 = mysurf.SettingsXmin;
    }
    ecritx = x2 - model.GenericInterSpace;
    ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
    if (clickquicknavmenu) {
      if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
        mysurf.optionshowcollection = !mysurf.optionshowcollection;
        needanotherupdate = true;
        clickquicknavmenu = false;
      }
    }
    recti = new RectF(x1, y1, x2, y2);
    surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu1BgPainta);
    recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
    surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu1BgPainto);
    if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
      RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
      surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
    }
    surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

    mabase += model.GenericCaseH + model.GenericInterSpace;
    if (mabase > mysurf.myheight)
      return;

    if (mysurf.optionshowcollection) {

      if (model.collections.size() > 0) {

        ecritoption = "goto";
        y1 = mabase;
        y2 = mabase + model.GenericCaseH;
        taillex = model.Menu1TextePaint.measureText(ecritoption);
        x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
        x2 = mysurf.ScreenWidth - model.GenericInterSpace;
        if (x1 > mysurf.SettingsXmin) {
          x1 = mysurf.SettingsXmin;
        }
        ecritx = x2 - model.GenericInterSpace;
        ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
        if (clickquicknavmenu) {
          if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
            String mondossier = model.getBookmarkToOrdner(mysurf.ordnerIndex, mysurf.mediaIndex);
            String monfichier = mysurf.mediaIndexAddress;
            try {
              model.commandethreaddatabase.put(new String[]{"findMediaInAnotherOrdner", String.valueOf(currid), mondossier, monfichier});
            } catch (InterruptedException e) {
              e.printStackTrace();
            }
            needanotherupdate = true;
            clickquicknavmenu = false;
          }
        }
        recti = new RectF(x1, y1, x2, y2);
        surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
        recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
        surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
        if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
          RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
          surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
        }
        surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

        mabase += model.GenericCaseH + model.GenericInterSpace;
        if (mabase > mysurf.myheight)
          return;

        /* utiliser selection - (load this collection first)
        ecritoption = "remove";
        y1 = mabase;
        y2 = mabase + model.GenericCaseH;
        taillex = model.Menu1TextePaint.measureText(ecritoption);
        x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
        x2 = mysurf.ScreenWidth - model.GenericInterSpace;
        if (x1 > mysurf.SettingsXmin) {
          x1 = mysurf.SettingsXmin;
        }
        ecritx = x2 - model.GenericInterSpace;
        ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
        if (clickquicknavmenu) {
          if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
            String originalfolder = model.getOriginalFolder(mysurf.ordnerIndex, mysurf.mediaIndex);
            if (originalfolder == null) {
              model.message("original not found " + mysurf.ordnerIndexAddress);
            } else {
              try {
                model.commandethreaddatabase.put(new String[]{"selectfile", String.valueOf(mysurf.ordnerIndex), String.valueOf(mysurf.mediaIndex),
                    String.valueOf(false), model.currCollectionAddress, String.valueOf(true)});
              } catch (InterruptedException e) {
                e.printStackTrace();
              }
              model.removeFileFromFolder(mysurf.ordnerIndex, mysurf.mediaIndex);
            }
            needanotherupdate = true;
            clickquicknavmenu = false;
          }
        }
        recti = new RectF(x1, y1, x2, y2);
        surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
        recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
        surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
        if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
          RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
          surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
        }
        surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

        mabase += model.GenericCaseH + model.GenericInterSpace;
        if (mabase > mysurf.myheight)
          return;
        */
      }

      int availableCollectionsOnDiskl = model.availCollectionsOnDiskl;
      for (int i = 0; i < availableCollectionsOnDiskl ; i++) {
        String colladdress = model.getAvailCollectionsOnDiskI(i); //model.preferences.getString("collection" + i, null);
        if (colladdress.length() == 0)
          continue;
        String monoption = Gallery.colladdresstoname(colladdress);
        if (monoption == null)
          continue;

        y1 = mabase;
        y2 = mabase + model.GenericCaseH;
        ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;

        taillex = model.Menu1TextePaint.measureText("hide");
        x1 = (mysurf.ScreenWidth - model.GenericInterSpace + mysurf.SettingsXmin) * 0.5f + model.GenericInterSpace * 0.5f;
        x2 = mysurf.ScreenWidth - model.GenericInterSpace;
        ecritx = x2;
        if (clickquicknavmenu) {
          if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
            try {
              model.commandethreaddatabase.put(new String[]{"removedisplayedcollectionfolders", colladdress});
            } catch (InterruptedException e) {
              e.printStackTrace();
            }
            needanotherupdate = true;
            clickquicknavmenu = false;
          }
        }
        //recti = new RectF(x1, y1, x2, y2);
        //surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
        recti = new RectF(x1, y1, x2, y2);
        surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
        if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
          RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
          surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
        }
        surfacecanvas.drawText("hide", ecritx, ecrity, model.Menu1TextePaint);

        taillex = model.Menu1TextePaint.measureText(monoption);
        x2 = x1 - model.GenericInterSpace * 0.50f;
        x1 = x2 - model.GenericInterSpace * 2.0f - taillex;
        if (x1 > mysurf.SettingsXmin)
          x1 = mysurf.SettingsXmin;
        ecritx = x2 - model.GenericInterSpace;
        if (clickquicknavmenu) {
          if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
            try {
              model.commandethreaddatabase.put(new String[]{"LoadCollectionFromDiskOrTemp", "load", colladdress, "false"});
              model.commandethreaddatabase.put(new String[]{"addcollectionfolderstodisplay", String.valueOf(currid), colladdress});
            } catch (InterruptedException e) {
              e.printStackTrace();
            }
            needanotherupdate = true;
            clickquicknavmenu = false;
          }
        }
        recti = new RectF(x1, y1, x2, y2);
        surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
        recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
        surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
        if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
          RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
          surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
        }
        surfacecanvas.drawText(monoption, ecritx, ecrity, model.Menu1TextePaint);

        mabase += model.GenericCaseH + model.GenericInterSpace;
        if (mabase > mysurf.myheight)
          break;

      }
    }
  }

  private void menuselection(Canvas surfacecanvas, int foldercount) {
    if (model.currCollectionAddress == null) {
      ecritoption = "selection";
    } else {
      ecritoption = "<" + model.currCollectionPrintName + "> " + model.currCollectionSelectedCount;
    }

    y1 = mabase;
    y2 = mabase + model.GenericCaseH;
    taillex = model.Menu1TextePaint.measureText(ecritoption);
    x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
    x2 = mysurf.ScreenWidth - model.GenericInterSpace;
    if (x1 > mysurf.SettingsXmin) {
      x1 = mysurf.SettingsXmin;
    }
    ecritx = x2 - model.GenericInterSpace;
    ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
    if (clickquicknavmenu) {
      if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
        mysurf.optionshowselection = !mysurf.optionshowselection;
        mysurf.optionshowselectionactive = false;
        if (!model.alreadymessagedthumbnailselection) {
          model.message("  center  select\n" +
              "  sides   jump to\n" +
              "Folder name portions\n" +
              "are also selectable.\n" +
              "You can hide the menu."
          );
          model.alreadymessagedthumbnailselection = true;
        }
        model.createnewfolder = null;
        selectiona = false;
        selectionz = false;
        selectionplus = false;
        selectionmoins = false;
        needanotherupdate = true;
        clickquicknavmenu = false;
      }
    }
    recti = new RectF(x1, y1, x2, y2);
    surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu1BgPainta);
    recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
    surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu1BgPainto);
    if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
      RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
      surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
    }
    surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

    mabase += model.GenericCaseH + model.GenericInterSpace;
    if (mabase > mysurf.myheight)
      return;

    if (mysurf.optionshowselection) {

      y1 = mabase;
      y2 = mabase + model.GenericCaseH;
      ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;

      ecritoption = "+";
      taillex = model.Menu1TextePaint.measureText(ecritoption);
      x1 = (mysurf.ScreenWidth - model.GenericInterSpace + mysurf.SettingsXmin) * 0.5f + model.GenericInterSpace * 0.5f;
      x2 = mysurf.ScreenWidth - model.GenericInterSpace;
      ecritx = x2;
      if (clickquicknavmenu) {
        if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
          if (model.currCollectionAddress == null) {
            //selectfilesonlynofolder = true;
            try {
              model.commandethreaddatabase.put(new String[]{"LoadCollectionFromDiskOrTemp", "load", "temp", "true"});
            } catch (InterruptedException e) {
              e.printStackTrace();
            }
            model.message("share, copy, delete"
                + "\nwith a <temp> selection");
          }
          selectionplus = !selectionplus;
          mysurf.optionshowselectionactive = selectionplus;
          selectionmoins = false;
          selectiona = false;
          selectionz = false;
          selectionadoss = -1;
          selectionafich = -1;
          selectionzdoss = -1;
          selectionzfich = -1;
          needanotherupdate = true;
          clickquicknavmenu = false;
        }
      }
      recti = new RectF(x1, y1, x2, y2);
      //surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
      //recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
      if (selectionplus) {
        RectF rectisel = new RectF(x1 + quatredixiememillimetre, y1 + quatredixiememillimetre, x2 - quatredixiememillimetre, y2 - quatredixiememillimetre);
        surfacecanvas.drawRoundRect(rectisel, roundedRectRatio, roundedRectRatio, model.SettingsSelPaint);
      }
      if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
        RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
        surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
      }
      surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

      ecritoption = "-";
      taillex = model.Menu1TextePaint.measureText(ecritoption);
      x2 = x1 - model.GenericInterSpace;
      x1 = x2 - model.GenericInterSpace * 2.0f - taillex;
      if (x1 > mysurf.SettingsXmin)
        x1 = mysurf.SettingsXmin;
      if (x1 > mysurf.SettingsXmin)
        x1 = mysurf.SettingsXmin;
      ecritx = x2 - model.GenericInterSpace;
      if (clickquicknavmenu) {
        if (mysurf.SettingsXmin < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
          selectionmoins = !selectionmoins;
          mysurf.optionshowselectionactive = selectionmoins;
          selectionplus = false;
          selectiona = false;
          selectionz = false;
          selectionadoss = -1;
          selectionafich = -1;
          selectionzdoss = -1;
          selectionzfich = -1;
          needanotherupdate = true;
          clickquicknavmenu = false;
        }
      }
      recti = new RectF(x1, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
      recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
      if (selectionmoins) {
        RectF rectisel = new RectF(x1 + quatredixiememillimetre, y1 + quatredixiememillimetre, x2 - quatredixiememillimetre, y2 - quatredixiememillimetre);
        surfacecanvas.drawRoundRect(rectisel, roundedRectRatio, roundedRectRatio, model.SettingsSelPaint);
      }
      if ((model.isandroidtv || model.keypadnavactive) && mysurf.SettingsXmin < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
        RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
        surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
      }
      surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

      mabase += model.GenericCaseH + model.GenericInterSpace;
      if (mabase > mysurf.myheight)
        return;

      if (selectionmoins || selectionplus) {
        if (selectionadoss >= 0 && selectionafich >= 0) {
          ecritoption = "from " + selectionadoss + " " + selectionafich;
        } else {
          ecritoption = "from";
        }
        y1 = mabase;
        y2 = mabase + model.GenericCaseH;
        taillex = model.Menu1TextePaint.measureText(ecritoption);
        x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
        x2 = mysurf.ScreenWidth - model.GenericInterSpace;
        if (x1 > mysurf.SettingsXmin) {
          x1 = mysurf.SettingsXmin;
        }
        ecritx = x2 - model.GenericInterSpace;
        ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
        if (clickquicknavmenu) {
          if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
            selectiona = !selectiona;
            selectionz = false;
            needanotherupdate = true;
            clickquicknavmenu = false;
          }
        }
        recti = new RectF(x1, y1, x2, y2);
        surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
        recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
        surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
        if (selectiona) {
          RectF rectisel = new RectF(x1 + quatredixiememillimetre, y1 + quatredixiememillimetre, x2 - quatredixiememillimetre, y2 - quatredixiememillimetre);
          surfacecanvas.drawRoundRect(rectisel, roundedRectRatio, roundedRectRatio, model.SettingsSelPaint);
        }
        if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
          RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
          surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
        }
        surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

        mabase += model.GenericCaseH + model.GenericInterSpace;
        if (mabase > mysurf.myheight)
          return;

        if (selectionzdoss >= 0 && selectionzfich >= 0) {
          ecritoption = "to " + selectionzdoss + " " + selectionzfich;
        } else {
          ecritoption = "to";
        }
        y1 = mabase;
        y2 = mabase + model.GenericCaseH;
        taillex = model.Menu1TextePaint.measureText(ecritoption);
        x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
        x2 = mysurf.ScreenWidth - model.GenericInterSpace;
        if (x1 > mysurf.SettingsXmin) {
          x1 = mysurf.SettingsXmin;
        }
        ecritx = x2 - model.GenericInterSpace;
        ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
        if (clickquicknavmenu) {
          if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
            selectionz = !selectionz;
            selectiona = false;
            needanotherupdate = true;
            clickquicknavmenu = false;
          }
        }
        recti = new RectF(x1, y1, x2, y2);
        surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
        recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
        surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
        if (selectionz) {
          RectF rectisel = new RectF(x1 + quatredixiememillimetre, y1 + quatredixiememillimetre, x2 - quatredixiememillimetre, y2 - quatredixiememillimetre);
          surfacecanvas.drawRoundRect(rectisel, roundedRectRatio, roundedRectRatio, model.SettingsSelPaint);
        }
        if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
          RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
          surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
        }
        surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

        mabase += model.GenericCaseH + model.GenericInterSpace;
        if (mabase > mysurf.myheight)
          return;
      }

      boolean temp = false;
      if (model.currCollectionAddress != null) {
        if (model.currCollectionAddress.equals("temp"))
          temp = true;

        if (model.confirmswitch)
          ecritoption = "confirm clear";
        else
          ecritoption = "clear";
        y1 = mabase;
        y2 = mabase + model.GenericCaseH;
        taillex = model.Menu1TextePaint.measureText(ecritoption);
        x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
        x2 = mysurf.ScreenWidth - model.GenericInterSpace;
        if (x1 > mysurf.SettingsXmin) {
          x1 = mysurf.SettingsXmin;
        }
        ecritx = x2 - model.GenericInterSpace;
        ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
        if (clickquicknavmenu) {
          if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
            needanotherupdate = true;
            clickquicknavmenu = false;
            if (!model.confirmswitch) {
              model.confirmswitch = true;
              new Thread(new Runnable() {
                @Override
                public void run() {
                  model.message("Please click again within 6s to confirm clearing");
                  for (int i = 12; i > 0 && model.confirmswitch; i--) {
                    try {
                      Thread.sleep(500);
                    } catch (InterruptedException e) {
                      e.printStackTrace();
                    }
                  }
                  model.confirmswitch = false;
                }
              }).start();
            } else {
              model.confirmswitch = false;
              try {
                model.commandethreaddatabase.put(new String[]{"removedisplayedcollectionfolders", model.currCollectionAddress});
                model.commandethreaddatabase.put(new String[]{"clearselection", model.currCollectionAddress});
              } catch (InterruptedException e) {
                e.printStackTrace();
              }
            }
          }
        }
        recti = new RectF(x1, y1, x2, y2);
        surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
        recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
        surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
        if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
          RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
          surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
        }
        surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

        mabase += model.GenericCaseH + model.GenericInterSpace;
        if (mabase > mysurf.myheight)
          return;

        if (selectionmoins || selectionplus) {
          if (!temp) {
            /*if (selectfilesonlynofolder)
              ecritoption = "as files";
            else
              ecritoption = "as folders";
            y1 = mabase;
            y2 = mabase + model.GenericCaseH;
            taillex = model.Menu1TextePaint.measureText(ecritoption);
            x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
            x2 = mysurf.ScreenWidth - model.GenericInterSpace;
            if (x1 > mysurf.SettingsXmin) {
              x1 = mysurf.SettingsXmin;
            }
            ecritx = x2 - model.GenericInterSpace;
            ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
            if (clickquicknavmenu) {
              if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
                selectfilesonlynofolder = !selectfilesonlynofolder;
                if (!selectfilesonlynofolder)
                  model.message("folders selected\nwill be added as whole :\nany change to their files\nwill be reflected");
                else
                  model.message();
                needanotherupdate = true;
                clickquicknavmenu = false;
              }
            }
            recti = new RectF(x1, y1, x2, y2);
            surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
            recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
            surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
            if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
              RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
              surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
            }
            surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

            mabase += model.GenericCaseH + model.GenericInterSpace;
            if (mabase > mysurf.myheight)
              return;*/

            int currCollectionOrdnerListl = model.currCollectionOrdnerListl;
            if (currCollectionOrdnerListl > 1) {
              if (model.currCollectionAddToFolderi >= currCollectionOrdnerListl)
                model.currCollectionAddToFolderi = 0;

              y1 = mabase;
              y2 = mabase + model.GenericCaseH;
              ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;

              taillex = model.Menu1TextePaint.measureText(">");
              x1 = (mysurf.ScreenWidth - model.GenericInterSpace + mysurf.SettingsXmin) * 0.5f + model.GenericInterSpace * 0.5f;
              x2 = mysurf.ScreenWidth - model.GenericInterSpace;
              ecritx = x2;
              if (clickquicknavmenu) {
                if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
                  if (model.currCollectionAddToFolderi < currCollectionOrdnerListl - 1)
                    model.currCollectionAddToFolderi = model.currCollectionAddToFolderi + 1;
                  needanotherupdate = true;
                  clickquicknavmenu = false;
                }
              }
              //recti = new RectF(x1, y1, x2, y2);
              //surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
              recti = new RectF(x1, y1, x2, y2);
              surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
              if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
                RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
                surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
              }
              surfacecanvas.drawText(">", ecritx, ecrity, model.Menu1TextePaint);

              String destfold = model.getCurrCollectionOrdnerListI(model.currCollectionAddToFolderi, null);
              if (destfold.length() > 0)
                ecritoption = "to " + destfold + " < " + (model.currCollectionAddToFolderi+1) + "/" + currCollectionOrdnerListl;
              else
                ecritoption = "to " + Gallery.virtualCollectionFolder + model.currCollectionPrintName  + " < " + (model.currCollectionAddToFolderi+1) + "/" + currCollectionOrdnerListl;
              taillex = model.Menu1TextePaint.measureText(ecritoption);
              x2 = x1 - model.GenericInterSpace * 0.50f;
              x1 = x2 - model.GenericInterSpace * 2.0f - taillex;
              if (x1 > mysurf.SettingsXmin)
                x1 = mysurf.SettingsXmin;
              ecritx = x2 - model.GenericInterSpace;
              if (clickquicknavmenu) {
                if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
                  if (model.currCollectionAddToFolderi > 0)
                    model.currCollectionAddToFolderi = model.currCollectionAddToFolderi - 1;
                  needanotherupdate = true;
                  clickquicknavmenu = false;
                }
              }
              recti = new RectF(x1, y1, x2, y2);
              surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
              recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
              surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
              if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
                RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
                surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
              }
              surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

              mabase += model.GenericCaseH + model.GenericInterSpace;
              if (mabase > mysurf.myheight)
                return;
            }

            String destinationfolder;
            if (model.createnewfolder != null)
              destinationfolder = model.createnewfolder;
            else
              destinationfolder = Gallery.virtualCollectionFolder + model.currCollectionPrintName + "/";

            ecritoption = "+ create folder";
            y1 = mabase;
            y2 = mabase + model.GenericCaseH;
            taillex = model.Menu1TextePaint.measureText(ecritoption);
            x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
            x2 = mysurf.ScreenWidth - model.GenericInterSpace;
            if (x1 > mysurf.SettingsXmin) {
              x1 = mysurf.SettingsXmin;
            }
            ecritx = x2 - model.GenericInterSpace;
            ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
            if (clickquicknavmenu) {
              if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
                Intent intent = new Intent();
                intent.setAction(Gallery.broadcastname);
                intent.putExtra("goal", "createfolder");
                intent.putExtra("id", currid);
                intent.putExtra("default", destinationfolder);
                LocalBroadcastManager.getInstance(model.activitycontext).sendBroadcast(intent);
                needanotherupdate = true;
                clickquicknavmenu = false;
              }
            }
            recti = new RectF(x1, y1, x2, y2);
            surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
            recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
            surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
            if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
              RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
              surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
            }
            surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

            mabase += model.GenericCaseH + model.GenericInterSpace;
            if (mabase > mysurf.myheight)
              return;

          }
        }

        ecritoption = "close <" + model.currCollectionPrintName + ">";
        y1 = mabase;
        y2 = mabase + model.GenericCaseH;
        taillex = model.Menu1TextePaint.measureText(ecritoption);
        x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
        x2 = mysurf.ScreenWidth - model.GenericInterSpace;
        if (x1 > mysurf.SettingsXmin) {
          x1 = mysurf.SettingsXmin;
        }
        ecritx = x2 - model.GenericInterSpace;
        ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
        if (clickquicknavmenu) {
          if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
            //selectfilesonlynofolder = false;
            try {
              model.commandethreaddatabase.put(new String[]{"closecollection", model.currCollectionAddress});
            } catch (InterruptedException e) {
              e.printStackTrace();
            }
            needanotherupdate = true;
            clickquicknavmenu = false;
          }
        }
        recti = new RectF(x1, y1, x2, y2);
        surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
        recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
        surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
        if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
          RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
          surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
        }
        surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

        mabase += model.GenericCaseH + model.GenericInterSpace;
        if (mabase > mysurf.myheight)
          return;

      } else {

        ecritoption = "create collection";
        y1 = mabase;
        y2 = mabase + model.GenericCaseH;
        taillex = model.Menu1TextePaint.measureText(ecritoption);
        x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
        x2 = mysurf.ScreenWidth - model.GenericInterSpace;
        if (x1 > mysurf.SettingsXmin) {
          x1 = mysurf.SettingsXmin;
        }
        ecritx = x2 - model.GenericInterSpace;
        ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
        if (clickquicknavmenu) {
          if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
            Intent intent = new Intent();
            intent.setAction(Gallery.broadcastname);
            intent.putExtra("goal", "CreateCollectionOnDisk");
            intent.putExtra("id", currid);
            LocalBroadcastManager.getInstance(model.activitycontext).sendBroadcast(intent);
            needanotherupdate = true;
            clickquicknavmenu = false;
          }
        }
        recti = new RectF(x1, y1, x2, y2);
        surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
        recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
        surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
        if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
          RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
          surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
        }
        surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

        mabase += model.GenericCaseH + model.GenericInterSpace;
        if (mabase > mysurf.myheight)
          return;

        int availableCollectionsOnDiskl = model.availCollectionsOnDiskl;
        for (int i = 0; i < availableCollectionsOnDiskl ; i++) {
          String colladdress = model.getAvailCollectionsOnDiskI(i); //model.preferences.getString("collection" + i, null);
          if (colladdress.length() == 0)
            continue;
          String monoption = Gallery.colladdresstoname(colladdress);
          if (monoption == null)
            continue;

          y1 = mabase;
          y2 = mabase + model.GenericCaseH;
          ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;

          taillex = model.Menu1TextePaint.measureText("x");
          x1 = (mysurf.ScreenWidth - model.GenericInterSpace + mysurf.SettingsXmin) * 0.5f + model.GenericInterSpace * 0.5f;
          x2 = mysurf.ScreenWidth - model.GenericInterSpace;
          ecritx = x2;
          if (clickquicknavmenu) {
            if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
              try {
                model.commandethreaddatabase.put(new String[]{"removedisplayedcollectionfolders", colladdress});
                model.commandethreaddatabase.put(new String[]{"DeleteCollectionFromDisk", colladdress});
              } catch (InterruptedException e) {
                e.printStackTrace();
              }
              needanotherupdate = true;
              clickquicknavmenu = false;
            }
          }
          //recti = new RectF(x1, y1, x2, y2);
          //surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
          recti = new RectF(x1, y1, x2, y2);
          surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
          if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
            RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
            surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
          }
          surfacecanvas.drawText("X", ecritx, ecrity, model.Menu1TextePaint);

          taillex = model.Menu1TextePaint.measureText(monoption);
          x2 = x1 - model.GenericInterSpace * 0.50f;
          x1 = x2 - model.GenericInterSpace * 2.0f - taillex;
          if (x1 > mysurf.SettingsXmin)
            x1 = mysurf.SettingsXmin;
          ecritx = x2 - model.GenericInterSpace;
          if (clickquicknavmenu) {
            if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
              try {
                model.commandethreaddatabase.put(new String[]{"LoadCollectionFromDiskOrTemp", "load", colladdress, "true"});
              } catch (InterruptedException e) {
                e.printStackTrace();
              }
              needanotherupdate = true;
              clickquicknavmenu = false;
            }
          }
          recti = new RectF(x1, y1, x2, y2);
          surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
          recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
          surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
          if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
            RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
            surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
          }
          surfacecanvas.drawText(monoption, ecritx, ecrity, model.Menu1TextePaint);

          mabase += model.GenericCaseH + model.GenericInterSpace;
          if (mabase > mysurf.myheight)
            break;
        }
      }
    }

  }

  private void menuwallpaper(Canvas surfacecanvas, int foldercount) {
    ecritoption = "wallpaper";
    y1 = mabase;
    y2 = mabase + model.GenericCaseH;
    taillex = model.Menu1TextePaint.measureText(ecritoption);
    x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
    x2 = mysurf.ScreenWidth - model.GenericInterSpace;
    if (x1 > mysurf.SettingsXmin) {
      x1 = mysurf.SettingsXmin;
    }
    ecritx = x2 - model.GenericInterSpace;
    ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
    if (clickquicknavmenu) {
      if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
        needanotherupdate = true;
        clickquicknavmenu = false;
        mysurf.optionshowwallpaper = !mysurf.optionshowwallpaper;
      }
    }
    recti = new RectF(x1, y1, x2, y2);
    surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu1BgPainta);
    recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
    surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu1BgPainto);
    if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
      RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
      surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
    }
    surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

    mabase += model.GenericCaseH + model.GenericInterSpace;
    if (mabase > mysurf.myheight)
      return;

    if (mysurf.optionshowwallpaper) {

      y1 = mabase;
      y2 = mabase + model.GenericCaseH;
      ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;

      ecritoption = "+";
      taillex = model.Menu1TextePaint.measureText(ecritoption);
      x1 = (mysurf.ScreenWidth - model.GenericInterSpace + mysurf.SettingsXmin) * 0.5f + model.GenericInterSpace * 0.5f;
      x2 = mysurf.ScreenWidth - model.GenericInterSpace;
      ecritx = x2;
      if (clickquicknavmenu) {
        if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
          wallpaperspeed += 5;
          model.preferences.edit().putInt("wallpaperspeed", wallpaperspeed).commit();
          needanotherupdate = true;
          clickquicknavmenu = false;
        }
      }
      recti = new RectF(x1, y1, x2, y2);
      //surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
      //recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
      if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
        RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
        surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
      }
      surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

      ecritoption = "speed " + wallpaperspeed + "s -";
      taillex = model.Menu1TextePaint.measureText(ecritoption);
      x2 = x1 - model.GenericInterSpace;
      x1 = x2 - model.GenericInterSpace * 2.0f - taillex;
      if (x1 > mysurf.SettingsXmin)
        x1 = mysurf.SettingsXmin;
      ecritx = x2 - model.GenericInterSpace;
      if (clickquicknavmenu) {
        if (mysurf.SettingsXmin < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
          if (wallpaperspeed > 5)
            wallpaperspeed -= 5;
          model.preferences.edit().putInt("wallpaperspeed", wallpaperspeed).commit();
          needanotherupdate = true;
          clickquicknavmenu = false;
        }
      }
      recti = new RectF(x1, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
      recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
      if ((model.isandroidtv || model.keypadnavactive) && mysurf.SettingsXmin < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
        RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
        surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
      }
      surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

      mabase += model.GenericCaseH + model.GenericInterSpace;
      if (mabase > mysurf.myheight)
        return;

      y1 = mabase;
      y2 = mabase + model.GenericCaseH;
      ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;

      ecritoption = "+";
      taillex = model.Menu1TextePaint.measureText(ecritoption);
      x1 = (mysurf.ScreenWidth - model.GenericInterSpace + mysurf.SettingsXmin) * 0.5f + model.GenericInterSpace * 0.5f;
      x2 = mysurf.ScreenWidth - model.GenericInterSpace;
      ecritx = x2;
      if (clickquicknavmenu) {
        if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
          wallpaperminratio += 5;
          if (wallpapermaxratio < wallpaperminratio) {
            wallpapermaxratio = wallpaperminratio;
            model.preferences.edit().putFloat("wallpapermaxratio", wallpapermaxratio).commit();
          }
          model.preferences.edit().putFloat("wallpaperminratio", wallpaperminratio).commit();
          if (98 < wallpaperminratio && wallpaperminratio < 102)
            model.message("pictures will fill the screen");
          needanotherupdate = true;
          clickquicknavmenu = false;
        }
      }
      recti = new RectF(x1, y1, x2, y2);
      //surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
      //recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
      if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
        RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
        surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
      }
      surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

      ecritoption = "size min " + wallpaperminratio + "% -";
      taillex = model.Menu1TextePaint.measureText(ecritoption);
      x2 = x1 - model.GenericInterSpace;
      x1 = x2 - model.GenericInterSpace * 2.0f - taillex;
      if (x1 > mysurf.SettingsXmin)
        x1 = mysurf.SettingsXmin;
      ecritx = x2 - model.GenericInterSpace;
      if (clickquicknavmenu) {
        if (mysurf.SettingsXmin < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
          if (wallpaperminratio > 5)
            wallpaperminratio -= 5;
          model.preferences.edit().putFloat("wallpaperminratio", wallpaperminratio).commit();
          needanotherupdate = true;
          clickquicknavmenu = false;
        }
      }
      recti = new RectF(x1, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
      recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
      if ((model.isandroidtv || model.keypadnavactive) && mysurf.SettingsXmin < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
        RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
        surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
      }
      surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

      mabase += model.GenericCaseH + model.GenericInterSpace;
      if (mabase > mysurf.myheight)
        return;

      y1 = mabase;
      y2 = mabase + model.GenericCaseH;
      ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;

      ecritoption = "+";
      taillex = model.Menu1TextePaint.measureText(ecritoption);
      x1 = (mysurf.ScreenWidth - model.GenericInterSpace + mysurf.SettingsXmin) * 0.5f + model.GenericInterSpace * 0.5f;
      x2 = mysurf.ScreenWidth - model.GenericInterSpace;
      ecritx = x2;
      if (clickquicknavmenu) {
        if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
          wallpapermaxratio += 5;
          model.preferences.edit().putFloat("wallpapermaxratio", wallpapermaxratio).commit();
          needanotherupdate = true;
          clickquicknavmenu = false;
        }
      }
      recti = new RectF(x1, y1, x2, y2);
      //surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
      //recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
      if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
        RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
        surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
      }
      surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

      ecritoption = "size max " + wallpapermaxratio + "% -";
      taillex = model.Menu1TextePaint.measureText(ecritoption);
      x2 = x1 - model.GenericInterSpace;
      x1 = x2 - model.GenericInterSpace * 2.0f - taillex;
      if (x1 > mysurf.SettingsXmin)
        x1 = mysurf.SettingsXmin;
      ecritx = x2 - model.GenericInterSpace;
      if (clickquicknavmenu) {
        if (mysurf.SettingsXmin < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
          if (wallpapermaxratio > 5)
            wallpapermaxratio -= 5;
          model.preferences.edit().putFloat("wallpapermaxratio", wallpapermaxratio).commit();
          needanotherupdate = true;
          clickquicknavmenu = false;
        }
      }
      recti = new RectF(x1, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
      recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
      if ((model.isandroidtv || model.keypadnavactive) && mysurf.SettingsXmin < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
        RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
        surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
      }
      surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

      mabase += model.GenericCaseH + model.GenericInterSpace;
      if (mabase > mysurf.myheight)
        return;

      boolean validcoll = false;
      if (model.currCollectionAddress != null)
        if (!model.currCollectionAddress.equals("temp"))
          validcoll = true;

      if (validcoll)
          ecritoption = "wallpaper <" + model.currCollectionPrintName + ">";
      else if (wallpapercollection != null)
        ecritoption = "wallpaper <" + wallpapercollection + ">";
      else
        ecritoption = "wallpaper 0 selected";
      y1 = mabase;
      y2 = mabase + model.GenericCaseH;
      taillex = model.Menu1TextePaint.measureText(ecritoption);
      x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
      x2 = mysurf.ScreenWidth - model.GenericInterSpace;
      if (x1 > mysurf.SettingsXmin) {
        x1 = mysurf.SettingsXmin;
      }
      ecritx = x2 - model.GenericInterSpace;
      ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
      if (clickquicknavmenu) {
        if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
          needanotherupdate = true;
          clickquicknavmenu = false;
          if (validcoll) {
            SharedPreferences.Editor editor = model.preferences.edit();
            editor.putString("wallpapercollection", model.currCollectionPrintName);
            editor.commit();
            Intent intent = new Intent();
            intent.setAction(Gallery.broadcastname);
            intent.putExtra("goal", "launchwallpaper");
            LocalBroadcastManager.getInstance(model.activitycontext).sendBroadcast(intent);
          } else {
            if (wallpapercollection == null)
              wallpapercollection = model.preferences.getString("wallpapercollection", null);
            if (wallpapercollection != null) {
              String path = model.dossiercache + Gallery.staticdossiercollections + wallpapercollection + Gallery.collectionextension;
              if (new File(path).exists()) {
                Intent intent = new Intent();
                intent.setAction(Gallery.broadcastname);
                intent.putExtra("goal", "launchwallpaper");
                LocalBroadcastManager.getInstance(model.activitycontext).sendBroadcast(intent);
              } else {
                wallpapercollection = null;
                model.preferences.edit().remove("wallpapercollection").commit();
                model.message("Please create a music collection first.");
                mysurf.optionshowselection = true;
              }
            } else {
              model.message("Please create a music collection first.");
              mysurf.optionshowselection = true;
            }
          }
        }
      }
      recti = new RectF(x1, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
      recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
      if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
        RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
        surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
      }
      surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

      mabase += model.GenericCaseH + model.GenericInterSpace;
      if (mabase > mysurf.myheight)
        return;

      y1 = mabase;
      y2 = mabase + model.GenericCaseH;
      ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;

      ecritoption = "+";
      taillex = model.Menu1TextePaint.measureText(ecritoption);
      x1 = (mysurf.ScreenWidth - model.GenericInterSpace + mysurf.SettingsXmin) * 0.5f + model.GenericInterSpace * 0.5f;
      x2 = mysurf.ScreenWidth - model.GenericInterSpace;
      ecritx = x2;
      if (clickquicknavmenu) {
        if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
          lockscreenminratio += 5;
          if (lockscreenmaxratio < lockscreenminratio) {
            lockscreenmaxratio = lockscreenminratio;
            model.preferences.edit().putFloat("lockscreenmaxratio", lockscreenmaxratio).commit();
          }
          model.preferences.edit().putFloat("lockscreenminratio", lockscreenminratio).commit();
          if (98 < lockscreenminratio && lockscreenminratio < 102)
            model.message("pictures will fill the screen");
          needanotherupdate = true;
          clickquicknavmenu = false;
        }
      }
      recti = new RectF(x1, y1, x2, y2);
      //surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
      //recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
      if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
        RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
        surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
      }
      surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

      ecritoption = "size min " + lockscreenminratio + "% -";
      taillex = model.Menu1TextePaint.measureText(ecritoption);
      x2 = x1 - model.GenericInterSpace;
      x1 = x2 - model.GenericInterSpace * 2.0f - taillex;
      if (x1 > mysurf.SettingsXmin)
        x1 = mysurf.SettingsXmin;
      ecritx = x2 - model.GenericInterSpace;
      if (clickquicknavmenu) {
        if (mysurf.SettingsXmin < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
          if (lockscreenminratio > 5)
            lockscreenminratio -= 5;
          model.preferences.edit().putFloat("lockscreenminratio", lockscreenminratio).commit();
          needanotherupdate = true;
          clickquicknavmenu = false;
        }
      }
      recti = new RectF(x1, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
      recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
      if ((model.isandroidtv || model.keypadnavactive) && mysurf.SettingsXmin < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
        RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
        surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
      }
      surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

      mabase += model.GenericCaseH + model.GenericInterSpace;
      if (mabase > mysurf.myheight)
        return;

      y1 = mabase;
      y2 = mabase + model.GenericCaseH;
      ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;

      ecritoption = "+";
      taillex = model.Menu1TextePaint.measureText(ecritoption);
      x1 = (mysurf.ScreenWidth - model.GenericInterSpace + mysurf.SettingsXmin) * 0.5f + model.GenericInterSpace * 0.5f;
      x2 = mysurf.ScreenWidth - model.GenericInterSpace;
      ecritx = x2;
      if (clickquicknavmenu) {
        if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
          lockscreenmaxratio += 5;
          model.preferences.edit().putFloat("lockscreenmaxratio", lockscreenmaxratio).commit();
          needanotherupdate = true;
          clickquicknavmenu = false;
        }
      }
      recti = new RectF(x1, y1, x2, y2);
      //surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
      //recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
      if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
        RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
        surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
      }
      surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

      ecritoption = "size max " + lockscreenmaxratio + "% -";
      taillex = model.Menu1TextePaint.measureText(ecritoption);
      x2 = x1 - model.GenericInterSpace;
      x1 = x2 - model.GenericInterSpace * 2.0f - taillex;
      if (x1 > mysurf.SettingsXmin)
        x1 = mysurf.SettingsXmin;
      ecritx = x2 - model.GenericInterSpace;
      if (clickquicknavmenu) {
        if (mysurf.SettingsXmin < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
          if (lockscreenmaxratio > 5)
            lockscreenmaxratio -= 5;
          model.preferences.edit().putFloat("lockscreenmaxratio", lockscreenmaxratio).commit();
          needanotherupdate = true;
          clickquicknavmenu = false;
        }
      }
      recti = new RectF(x1, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
      recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
      if ((model.isandroidtv || model.keypadnavactive) && mysurf.SettingsXmin < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
        RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
        surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
      }
      surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

      mabase += model.GenericCaseH + model.GenericInterSpace;
      if (mabase > mysurf.myheight)
        return;

      if (validcoll)
        ecritoption = "lockscreen <" + model.currCollectionPrintName + ">";
      else if (lockscreencollection != null)
        ecritoption = "lockscreen <" + lockscreencollection + ">";
      else
        ecritoption = "lockscreen 0 selected";
      y1 = mabase;
      y2 = mabase + model.GenericCaseH;
      taillex = model.Menu1TextePaint.measureText(ecritoption);
      x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
      x2 = mysurf.ScreenWidth - model.GenericInterSpace;
      if (x1 > mysurf.SettingsXmin) {
        x1 = mysurf.SettingsXmin;
      }
      ecritx = x2 - model.GenericInterSpace;
      ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
      if (clickquicknavmenu) {
        if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
          needanotherupdate = true;
          clickquicknavmenu = false;
          if (validcoll) {
            SharedPreferences.Editor editor = model.preferences.edit();
            editor.putString("lockscreencollection", model.currCollectionPrintName);
            editor.commit();
            Intent intent = new Intent();
            intent.setAction(Gallery.broadcastname);
            intent.putExtra("goal", "launchlockscreen");
            LocalBroadcastManager.getInstance(model.activitycontext).sendBroadcast(intent);
          } else {
            if (lockscreencollection == null)
              lockscreencollection = model.preferences.getString("lockscreencollection", null);
            if (lockscreencollection != null) {
              String path = model.dossiercache + Gallery.staticdossiercollections + lockscreencollection + Gallery.collectionextension;
              if (new File(path).exists()) {
                Intent intent = new Intent();
                intent.setAction(Gallery.broadcastname);
                intent.putExtra("goal", "launchlockscreen");
                LocalBroadcastManager.getInstance(model.activitycontext).sendBroadcast(intent);
              } else {
                lockscreencollection = null;
                model.preferences.edit().remove("lockscreencollection").commit();
                model.message("Please create a music collection first.");
                mysurf.optionshowselection = true;
              }
            } else {
              model.message("Please create a music collection first.");
              mysurf.optionshowselection = true;
            }
          }
        }
      }
      recti = new RectF(x1, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
      recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
      if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
        RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
        surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
      }
      surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

      mabase += model.GenericCaseH + model.GenericInterSpace;
      if (mabase > mysurf.myheight)
        return;


      y1 = mabase;
      y2 = mabase + model.GenericCaseH;
      ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;

      ecritoption = "...";
      taillex = model.Menu1TextePaint.measureText(ecritoption);
      x1 = (mysurf.ScreenWidth - model.GenericInterSpace + mysurf.SettingsXmin) * 0.5f + model.GenericInterSpace * 0.5f;
      x2 = mysurf.ScreenWidth - model.GenericInterSpace;
      ecritx = x2;
      if (clickquicknavmenu) {
        if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
          Intent intent = new Intent();
          intent.setAction(Gallery.broadcastname);
          intent.putExtra("goal", "lockscreenwidgetconf");
          intent.putExtra("id", currid);
          intent.putExtra("type", Gallery.lockscreencalendarwidget);
          LocalBroadcastManager.getInstance(model.activitycontext).sendBroadcast(intent);
          needanotherupdate = true;
          clickquicknavmenu = false;
        }
      }
      recti = new RectF(x1, y1, x2, y2);
      //surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
      //recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
      if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
        RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
        surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
      }
      surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

      if (lockscreencalendarwidget && wallpapercalendarwidget)
        ecritoption = "both calendar";
      else if (lockscreencalendarwidget)
        ecritoption = "lockscreen calendar";
      else if (wallpapercalendarwidget)
        ecritoption = "wallpaper calendar";
      else
        ecritoption = "calendar off";
      taillex = model.Menu1TextePaint.measureText(ecritoption);
      x2 = x1 - model.GenericInterSpace;
      x1 = x2 - model.GenericInterSpace * 2.0f - taillex;
      if (x1 > mysurf.SettingsXmin)
        x1 = mysurf.SettingsXmin;
      ecritx = x2 - model.GenericInterSpace;
      if (clickquicknavmenu) {
        if (mysurf.SettingsXmin < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
          if (lockscreencalendarwidget && wallpapercalendarwidget) {
            lockscreencalendarwidget = false;
            wallpapercalendarwidget = false;
          } else if (lockscreencalendarwidget) {
            lockscreencalendarwidget = false;
            wallpapercalendarwidget = true;
          } else if (wallpapercalendarwidget) {
            lockscreencalendarwidget = true;
            wallpapercalendarwidget = true;
          } else {
            lockscreencalendarwidget = true;
            wallpapercalendarwidget = false;
          }
          model.preferences.edit().putBoolean("lockscreencalendarwidget", lockscreencalendarwidget).commit();
          model.preferences.edit().putBoolean("wallpapercalendarwidget", wallpapercalendarwidget).commit();
          needanotherupdate = true;
          clickquicknavmenu = false;
        }
      }
      recti = new RectF(x1, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
      recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
      if ((model.isandroidtv || model.keypadnavactive) && mysurf.SettingsXmin < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
        RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
        surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
      }
      surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

      mabase += model.GenericCaseH + model.GenericInterSpace;
      if (mabase > mysurf.myheight)
        return;

      y1 = mabase;
      y2 = mabase + model.GenericCaseH;
      ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;

      ecritoption = "...";
      taillex = model.Menu1TextePaint.measureText(ecritoption);
      x1 = (mysurf.ScreenWidth - model.GenericInterSpace + mysurf.SettingsXmin) * 0.5f + model.GenericInterSpace * 0.5f;
      x2 = mysurf.ScreenWidth - model.GenericInterSpace;
      ecritx = x2;
      if (clickquicknavmenu) {
        if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
          Intent intent = new Intent();
          intent.setAction(Gallery.broadcastname);
          intent.putExtra("goal", "lockscreenwidgetconf");
          intent.putExtra("id", currid);
          intent.putExtra("type", Gallery.lockscreencountdownwidget);
          LocalBroadcastManager.getInstance(model.activitycontext).sendBroadcast(intent);
          needanotherupdate = true;
          clickquicknavmenu = false;
        }
      }
      recti = new RectF(x1, y1, x2, y2);
      //surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
      //recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
      if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
        RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
        surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
      }
      surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

      if (lockscreencountdownwidget && wallpapercountdownwidget)
        ecritoption = "both countdown";
      else if (lockscreencountdownwidget)
        ecritoption = "lockscreen countdown";
      else if (wallpapercountdownwidget)
        ecritoption = "wallpaper countdown";
      else
        ecritoption = "countdown off";
      taillex = model.Menu1TextePaint.measureText(ecritoption);
      x2 = x1 - model.GenericInterSpace;
      x1 = x2 - model.GenericInterSpace * 2.0f - taillex;
      if (x1 > mysurf.SettingsXmin)
        x1 = mysurf.SettingsXmin;
      ecritx = x2 - model.GenericInterSpace;
      if (clickquicknavmenu) {
        if (mysurf.SettingsXmin < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
          if (lockscreencountdownwidget && wallpapercountdownwidget) {
            lockscreencountdownwidget = false;
            wallpapercountdownwidget = false;
          } else if (lockscreencountdownwidget) {
            lockscreencountdownwidget = false;
            wallpapercountdownwidget = true;
          } else if (wallpapercountdownwidget) {
            lockscreencountdownwidget = true;
            wallpapercountdownwidget = true;
          } else {
            lockscreencountdownwidget = true;
            wallpapercountdownwidget = false;
          }
          model.preferences.edit().putBoolean("lockscreencountdownwidget", lockscreencountdownwidget).commit();
          model.preferences.edit().putBoolean("wallpapercountdownwidget", wallpapercountdownwidget).commit();
          needanotherupdate = true;
          clickquicknavmenu = false;
        }
      }
      recti = new RectF(x1, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
      recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
      if ((model.isandroidtv || model.keypadnavactive) && mysurf.SettingsXmin < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
        RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
        surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
      }
      surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

      mabase += model.GenericCaseH + model.GenericInterSpace;
      if (mabase > mysurf.myheight)
        return;

      y1 = mabase;
      y2 = mabase + model.GenericCaseH;
      ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;

      ecritoption = "...";
      taillex = model.Menu1TextePaint.measureText(ecritoption);
      x1 = (mysurf.ScreenWidth - model.GenericInterSpace + mysurf.SettingsXmin) * 0.5f + model.GenericInterSpace * 0.5f;
      x2 = mysurf.ScreenWidth - model.GenericInterSpace;
      ecritx = x2;
      if (clickquicknavmenu) {
        if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
          Intent intent = new Intent();
          intent.setAction(Gallery.broadcastname);
          intent.putExtra("goal", "lockscreenwidgetconf");
          intent.putExtra("id", currid);
          intent.putExtra("type", Gallery.lockscreenmonitorwidget);
          LocalBroadcastManager.getInstance(model.activitycontext).sendBroadcast(intent);
          needanotherupdate = true;
          clickquicknavmenu = false;
        }
      }
      recti = new RectF(x1, y1, x2, y2);
      //surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
      //recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
      if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
        RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
        surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
      }
      surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

      if (lockscreenmonitorwidget && wallpapermonitorwidget)
        ecritoption = "both monitor";
      else if (lockscreenmonitorwidget)
        ecritoption = "lockscreen monitor";
      else if (wallpapermonitorwidget)
        ecritoption = "wallpaper monitor";
      else
        ecritoption = "monitor off";
      taillex = model.Menu1TextePaint.measureText(ecritoption);
      x2 = x1 - model.GenericInterSpace;
      x1 = x2 - model.GenericInterSpace * 2.0f - taillex;
      if (x1 > mysurf.SettingsXmin)
        x1 = mysurf.SettingsXmin;
      ecritx = x2 - model.GenericInterSpace;
      if (clickquicknavmenu) {
        if (mysurf.SettingsXmin < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
          if (lockscreenmonitorwidget && wallpapermonitorwidget) {
            lockscreenmonitorwidget = false;
            wallpapermonitorwidget = false;
          } else if (lockscreenmonitorwidget) {
            lockscreenmonitorwidget = false;
            wallpapermonitorwidget = true;
          } else if (wallpapermonitorwidget) {
            lockscreenmonitorwidget = true;
            wallpapermonitorwidget = true;
          } else {
            lockscreenmonitorwidget = true;
            wallpapermonitorwidget = false;
          }
          model.preferences.edit().putBoolean("lockscreenmonitorwidget", lockscreenmonitorwidget).commit();
          model.preferences.edit().putBoolean("wallpapermonitorwidget", wallpapermonitorwidget).commit();
          needanotherupdate = true;
          clickquicknavmenu = false;
        }
      }
      recti = new RectF(x1, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
      recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
      if ((model.isandroidtv || model.keypadnavactive) && mysurf.SettingsXmin < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
        RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
        surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
      }
      surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

      mabase += model.GenericCaseH + model.GenericInterSpace;
      if (mabase > mysurf.myheight)
        return;



    }

  }

  private volatile String destinationfolder = "";
  private volatile String destinationfolderpprint = "fghfh";

  private void menushare(Canvas surfacecanvas, int foldercount) {
    if (model.uploadthreadstatus != null)
      ecritoption = model.uploadthreadstatus;
    else
      ecritoption = "share/copy";
    y1 = mabase;
    y2 = mabase + model.GenericCaseH;
    taillex = model.Menu1TextePaint.measureText(ecritoption);
    x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
    x2 = mysurf.ScreenWidth - model.GenericInterSpace;
    if (x1 > mysurf.SettingsXmin) {
      x1 = mysurf.SettingsXmin;
    }
    ecritx = x2 - model.GenericInterSpace;
    ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
    if (clickquicknavmenu) {
      if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
        mysurf.optionshowmove = !mysurf.optionshowmove;
        needanotherupdate = true;
        clickquicknavmenu = false;
      }
    }
    recti = new RectF(x1, y1, x2, y2);
    surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu1BgPainta);
    recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
    surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu1BgPainto);
    if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
      RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
      surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
    }
    surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

    mabase += model.GenericCaseH + model.GenericInterSpace;
    if (mabase > mysurf.myheight)
      return;

    if (mysurf.optionshowmove) {
      ecritoption = "share current";
      if (model.currCollectionAddress != null)
        ecritoption = "share <" + model.currCollectionPrintName + ">";
      y1 = mabase;
      y2 = mabase + model.GenericCaseH;
      taillex = model.Menu1TextePaint.measureText(ecritoption);
      x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
      x2 = mysurf.ScreenWidth - model.GenericInterSpace;
      if (x1 > mysurf.SettingsXmin) {
        x1 = mysurf.SettingsXmin;
      }
      ecritx = x2 - model.GenericInterSpace;
      ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
      if (clickquicknavmenu) {
        if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
          needanotherupdate = true;
          clickquicknavmenu = false;
          try {
            model.commandethreaddatabase.put(new String[]{"sharefiles", mysurf.mediaIndexAddress});
          } catch (InterruptedException e) {
            e.printStackTrace();
          }
        }
      }
      recti = new RectF(x1, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
      recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
      if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
        RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
        surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
      }
      surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

      mabase += model.GenericCaseH + model.GenericInterSpace;
      if (mabase > mysurf.myheight)
        return;

      y1 = mabase;
      y2 = mabase + model.GenericCaseH;
      ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;

      if (model.nombredestinationsaved == 0 || model.optiondestinationmemory == -1) {
        destinationfolder = mysurf.ordnerIndexAddress;
      }
      if (!destinationfolder.equals(model.odestinationfolder)) {
        if (model.optiondestinationmemory >= 0)
          destinationfolder = model.preferences.getString("destinationsaved" + model.optiondestinationmemory, null);
        if (destinationfolder == null)
          destinationfolder = "";
        else {
          model.odestinationfolder = destinationfolder;
          destinationfolderpprint = Gallery.ifUrlDecode(destinationfolder);
        }
      }

      if (model.optiondestinationmemory >= 0)
        ecritoption = "m" + model.optiondestinationmemory;
      else
        ecritoption = "curr";
      taillex = model.Menu1TextePaint.measureText(ecritoption);
      x1 = (mysurf.ScreenWidth - model.GenericInterSpace + mysurf.SettingsXmin) * 0.5f + model.GenericInterSpace * 0.5f;
      x2 = mysurf.ScreenWidth - model.GenericInterSpace;
      ecritx = x2;
      if (clickquicknavmenu) {
        if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
          if (model.nombredestinationsaved > 0) {
            model.optiondestinationmemory = ((model.optiondestinationmemory + 2) % (model.nombredestinationsaved + 1)) - 1;
            if (model.optiondestinationmemory >= 0) {
              destinationfolder = model.preferences.getString("destinationsaved" + model.optiondestinationmemory, null);
              if (destinationfolder == null)
                destinationfolder = "";
              else {
                model.odestinationfolder = destinationfolder;
                destinationfolderpprint = Gallery.ifUrlDecode(destinationfolder);
              }
              model.message("destination path\n" + destinationfolderpprint + "\n(memory slot " + model.optiondestinationmemory + ")");
            } else if (model.optiondestinationmemory == -1) {
              destinationfolder = mysurf.ordnerIndexAddress;
              if (!destinationfolder.equals(model.odestinationfolder)) {
                model.odestinationfolder = destinationfolder;
                destinationfolderpprint = Gallery.ifUrlDecode(destinationfolder);
              }
              model.message("destination path\n" + destinationfolderpprint + "\n(current path)");
            }
          } else {
            model.optiondestinationmemory = -1;
            destinationfolder = mysurf.ordnerIndexAddress;
            if (!destinationfolder.equals(model.odestinationfolder)) {
              model.odestinationfolder = destinationfolder;
              destinationfolderpprint = Gallery.ifUrlDecode(destinationfolder);
            }
            model.message("Please click on the button on the left" +
                "\nto save a new destination path" +
                "\nto the next memory slot.");
          }
          model.preferences.edit().putInt("optiondestinationmemory", model.optiondestinationmemory).commit();
          needanotherupdate = true;
          clickquicknavmenu = false;
        }
      }
      //recti = new RectF(x1, y1, x2, y2);
      //surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
      recti = new RectF(x1, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
      if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
        RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
        surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
      }
      surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

      ecritoption = "destination " + destinationfolderpprint;
      taillex = model.Menu1TextePaint.measureText(ecritoption);
      x2 = x1 - model.GenericInterSpace * 0.50f;
      x1 = x2 - model.GenericInterSpace * 2.0f - taillex;
      if (x1 > mysurf.SettingsXmin)
        x1 = mysurf.SettingsXmin;
      ecritx = x2 - model.GenericInterSpace;
      if (clickquicknavmenu) {
        if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
          Intent intent = new Intent();
          intent.setAction(Gallery.broadcastname);
          intent.putExtra("goal", "destinationfolder");
          intent.putExtra("id", currid);
          intent.putExtra("memory", model.optiondestinationmemory);
          intent.putExtra("default", destinationfolder);
          LocalBroadcastManager.getInstance(model.activitycontext).sendBroadcast(intent);
          needanotherupdate = true;
          clickquicknavmenu = false;
        }
      }
      recti = new RectF(x1, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
      recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
      if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
        RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
        surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
      }
      surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

      mabase += model.GenericCaseH + model.GenericInterSpace;
      if (mabase > mysurf.myheight)
        return;

      boolean tocurrentdir = destinationfolder.equals(mysurf.ordnerIndexAddress);

      if (model.currCollectionAddress == null)
        ecritoption = "copy current ";
      else
        ecritoption = "copy <" + model.currCollectionPrintName + "> ";
      if (tocurrentdir)
        ecritoption += "here";
      else
        ecritoption += "to destination";
      y1 = mabase;
      y2 = mabase + model.GenericCaseH;
      taillex = model.Menu1TextePaint.measureText(ecritoption);
      x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
      x2 = mysurf.ScreenWidth - model.GenericInterSpace;
      if (x1 > mysurf.SettingsXmin) {
        x1 = mysurf.SettingsXmin;
      }
      ecritx = x2 - model.GenericInterSpace;
      ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
      if (clickquicknavmenu) {
        if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
          needanotherupdate = true;
          clickquicknavmenu = false;
          if (model.currCollectionAddress == null && tocurrentdir) {
            model.message("Please make a selection first" +
                "\nand then navigate to the folder" +
                "\nwhere you want the files to be copied to.");
            mysurf.optionshowselection = true;
          } else if (!model.confirmswitch) {
            model.confirmswitch = true;
            new Thread(new Runnable() {
              @Override
              public void run() {
                if (model.usesaf)
                  model.message("Please click again within 6s to confirm copy to\n"
                      + destinationfolder + "\n" // can also be partially online where no SAF is needed
                      + "Please grant permission to the folder\n"
                      + "where you want the files to be copied to.");
                else
                  model.message("Please click again within 6s to confirm copy to\n"
                      + destinationfolder);
                for (int i = 12; i > 0 && model.confirmswitch; i--) {
                  try {
                    Thread.sleep(500);
                  } catch (InterruptedException e) {
                    e.printStackTrace();
                  }
                }
                model.confirmswitch = false;
              }
            }).start();
          } else {
            model.confirmswitch = false;
            int libthumb = -1;
            int isonline = Media.online_no;
            if (model.currCollectionAddress == null) {
              libthumb = model.getMediaAddressToGetLibextractorsThumbnail(mysurf.ordnerIndex, mysurf.mediaIndex);
              isonline = model.getMediaIsOnline(mysurf.ordnerIndex, mysurf.mediaIndex);
            }
            try {
              model.commandethreaddatabase.put(new String[]{"copyfiles", destinationfolder, "copyonly",
                  mysurf.ordnerIndexAddress, mysurf.mediaIndexAddress, String.valueOf(isonline), String.valueOf(libthumb)});
            } catch (InterruptedException e) {
              e.printStackTrace();
            }
          }
        }
      }
      recti = new RectF(x1, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
      recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
      if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
        RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
        surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
      }
      surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

      mabase += model.GenericCaseH + model.GenericInterSpace;
      if (mabase > mysurf.myheight)
        return;

      if (model.currCollectionAddress == null)
        ecritoption = "move current ";
      else
        ecritoption = "move <" + model.currCollectionPrintName + "> ";
      if (tocurrentdir)
        ecritoption += "here";
      else
        ecritoption += "to destination";
      y1 = mabase;
      y2 = mabase + model.GenericCaseH;
      taillex = model.Menu1TextePaint.measureText(ecritoption);
      x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
      x2 = mysurf.ScreenWidth - model.GenericInterSpace;
      if (x1 > mysurf.SettingsXmin) {
        x1 = mysurf.SettingsXmin;
      }
      ecritx = x2 - model.GenericInterSpace;
      ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
      if (clickquicknavmenu) {
        if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
          needanotherupdate = true;
          clickquicknavmenu = false;
          if (model.currCollectionAddress == null && tocurrentdir) {
            model.message("Please make a selection first" +
                "\nand then navigate to the folder" +
                "\nwhere you want the files to be moved to.");
            mysurf.optionshowselection = true;
          } else if (!model.confirmswitch) {
            model.confirmswitch = true;
            new Thread(new Runnable() {
              @Override
              public void run() {
                if (model.usesaf)
                  model.message("Please click again within 6s to confirm move to\n"
                      + destinationfolder + "\n" // can also be partially online where no SAF is needed
                      + "Please grant permission to the folder\n"
                      + "where you want the files to be moved to.");
                else
                  model.message("Please click again within 6s to confirm move to\n"
                      + destinationfolder);
                for (int i = 12; i > 0 && model.confirmswitch; i--) {
                  try {
                    Thread.sleep(500);
                  } catch (InterruptedException e) {
                    e.printStackTrace();
                  }
                }
                model.confirmswitch = false;
              }
            }).start();
          } else {
            model.confirmswitch = false;
            int libthumb = -1;
            int isonline = Media.online_no;
            if (model.currCollectionAddress == null) {
              libthumb = model.getMediaAddressToGetLibextractorsThumbnail(mysurf.ordnerIndex, mysurf.mediaIndex);
              isonline = model.getMediaIsOnline(mysurf.ordnerIndex, mysurf.mediaIndex);
            }
            try {
              model.commandethreaddatabase.put(new String[]{"copyfiles", destinationfolder, "remove",
                  mysurf.ordnerIndexAddress, mysurf.mediaIndexAddress, String.valueOf(isonline), String.valueOf(libthumb)});
            } catch (InterruptedException e) {
              e.printStackTrace();
            }
          }
        }
      }
      recti = new RectF(x1, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
      recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
      if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
        RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
        surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
      }
      surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

      mabase += model.GenericCaseH + model.GenericInterSpace;
      if (mabase > mysurf.myheight)
        return;

      ecritoption = "delete current";
      if (model.currCollectionAddress != null)
        ecritoption = "delete <" + model.currCollectionPrintName + ">";
      y1 = mabase;
      y2 = mabase + model.GenericCaseH;
      taillex = model.Menu1TextePaint.measureText(ecritoption);
      x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
      x2 = mysurf.ScreenWidth - model.GenericInterSpace;
      if (x1 > mysurf.SettingsXmin) {
        x1 = mysurf.SettingsXmin;
      }
      ecritx = x2 - model.GenericInterSpace;
      ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
      if (clickquicknavmenu) {
        if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
          needanotherupdate = true;
          clickquicknavmenu = false;
          if (!model.confirmswitch) {
            model.confirmswitch = true;
            new Thread(new Runnable() {
              @Override
              public void run() {
                if (model.currCollectionAddress == null) {
                  if (model.usesaf)
                    model.message("Please click again within 6s to confirm deletion of \n"
                        + mysurf.ordnerIndexAddress + "\n" // can also be partially online where no SAF is needed
                        + "Please grant permission to their folder\n"
                        + "or any of their parent.");
                  else
                    model.message("Please click again within 6s to confirm deletion of\n"
                        + mysurf.mediaIndexAddress);
                } else {
                  if (model.usesaf)
                    model.message("Please click again within 6s to confirm deletion of \n"
                        + "<" + model.currCollectionPrintName + ">\n" // can also be partially online where no SAF is needed
                        + "Please grant permission to their folder\n"
                        + "or any of their parent. ");
                  else
                    model.message("Please click again within 6s to confirm deletion of \n"
                        + "<" + model.currCollectionPrintName + ">");
                }
                for (int i = 12; i > 0 && model.confirmswitch; i--) {
                  try {
                    Thread.sleep(500);
                  } catch (InterruptedException e) {
                    e.printStackTrace();
                  }
                }
                model.confirmswitch = false;
              }
            }).start();
          } else {
            model.confirmswitch = false;
            try {
              model.commandethreaddatabase.put(new String[]{"deletefiles", mysurf.ordnerIndexAddress, mysurf.mediaIndexAddress});
            } catch (InterruptedException e) {
              e.printStackTrace();
            }
          }
        }
      }
      recti = new RectF(x1, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
      recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
      if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
        RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
        surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
      }
      surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

      mabase += model.GenericCaseH + model.GenericInterSpace;
      if (mabase > mysurf.myheight)
        return;

      if (model.currCollectionAddress == null) {
        if (model.renameprefix == null)
          ecritoption = "rename current";
        else
          ecritoption = "rename to " + model.renameprefix;
      } else {
        if (model.renameprefix == null)
          ecritoption = "rename prefix";
        else
          ecritoption = "rename prefix " + model.renameprefix;
      }
      y1 = mabase;
      y2 = mabase + model.GenericCaseH;
      taillex = model.Menu1TextePaint.measureText(ecritoption);
      x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
      x2 = mysurf.ScreenWidth - model.GenericInterSpace;
      if (x1 > mysurf.SettingsXmin) {
        x1 = mysurf.SettingsXmin;
      }
      ecritx = x2 - model.GenericInterSpace;
      ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
      if (clickquicknavmenu) {
        if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
          needanotherupdate = true;
          clickquicknavmenu = false;
          String filename = mysurf.mediaIndexAddress;
          if (filename != null) {
            int lastslash = filename.lastIndexOf('/');
            if (lastslash >= 0) {
              filename = filename.substring(lastslash + 1);
            }
          }
          Intent intent = new Intent();
          intent.setAction(Gallery.broadcastname);
          intent.putExtra("goal", "renameprefix");
          intent.putExtra("id", currid);
          intent.putExtra("default", filename);
          LocalBroadcastManager.getInstance(model.activitycontext).sendBroadcast(intent);
        }
      }
      recti = new RectF(x1, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
      recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
      if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
        RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
        surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
      }
      surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

      mabase += model.GenericCaseH + model.GenericInterSpace;
      if (mabase > mysurf.myheight)
        return;

      if (model.renameprefix != null) {
        if (model.currCollectionAddress == null)
          ecritoption = "rename current";
        else
          ecritoption = "batch rename <" + model.currCollectionPrintName + "> ";
        if (tocurrentdir)
          ecritoption += "here";
        else
          ecritoption += "to destination";
        y1 = mabase;
        y2 = mabase + model.GenericCaseH;
        taillex = model.Menu1TextePaint.measureText(ecritoption);
        x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
        x2 = mysurf.ScreenWidth - model.GenericInterSpace;
        if (x1 > mysurf.SettingsXmin) {
          x1 = mysurf.SettingsXmin;
        }
        ecritx = x2 - model.GenericInterSpace;
        ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
        if (clickquicknavmenu) {
          if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
            needanotherupdate = true;
            clickquicknavmenu = false;
            if (!model.confirmswitch) {
              model.confirmswitch = true;
              new Thread(new Runnable() {
                @Override
                public void run() {
                  if (model.currCollectionAddress == null) {
                    if (model.usesaf)
                      model.message("Please click again within 6s to confirm rename to\n"
                          + destinationfolder + model.renameprefix + "\n"
                          + "Please grant permission to the folder\n"
                          + "where you want the files to be moved to.");
                    else
                      model.message("Please click again within 6s to confirm rename to\n"
                          + destinationfolder + model.renameprefix);
                  } else {
                    if (model.usesaf)
                      model.message("Please click again within 6s to confirm batch rename\n" +
                          "of <" + model.currCollectionPrintName + "> to\n"
                          + destinationfolder + model.renameprefix + "\n"
                          + "Please grant permission to the folder\n"
                          + "where you want the files to be moved to.");
                    else
                      model.message("Please click again within 6s to confirm batch rename \n" +
                          "of <" + model.currCollectionPrintName + "> to\n"
                          + destinationfolder + model.renameprefix);
                  }
                  for (int i = 12; i > 0 && model.confirmswitch; i--) {
                    try {
                      Thread.sleep(500);
                    } catch (InterruptedException e) {
                      e.printStackTrace();
                    }
                  }
                  model.confirmswitch = false;
                }
              }).start();
            } else {
              model.confirmswitch = false;
              int libthumb = -1;
              int isonline = Media.online_no;
              if (model.currCollectionAddress == null) {
                libthumb = model.getMediaAddressToGetLibextractorsThumbnail(mysurf.ordnerIndex, mysurf.mediaIndex);
                isonline = model.getMediaIsOnline(mysurf.ordnerIndex, mysurf.mediaIndex);
              }
              try {
                model.commandethreaddatabase.put(new String[]{"copyfiles", destinationfolder, model.renameprefix,
                    mysurf.ordnerIndexAddress, mysurf.mediaIndexAddress, String.valueOf(isonline), String.valueOf(libthumb)});
              } catch (InterruptedException e) {
                e.printStackTrace();
              }
            }
          }
        }
        recti = new RectF(x1, y1, x2, y2);
        surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
        recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
        surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
        if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
          RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
          surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
        }
        surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

        mabase += model.GenericCaseH + model.GenericInterSpace;
        if (mabase > mysurf.myheight)
          return;
      }

      if (model.copyoverwritenosync)
        ecritoption = "copy mode : overwrite";
      else
        ecritoption = "copy mode : sync";
      y1 = mabase;
      y2 = mabase + model.GenericCaseH;
      taillex = model.Menu1TextePaint.measureText(ecritoption);
      x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
      x2 = mysurf.ScreenWidth - model.GenericInterSpace;
      if (x1 > mysurf.SettingsXmin) {
        x1 = mysurf.SettingsXmin;
      }
      ecritx = x2 - model.GenericInterSpace;
      ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
      if (clickquicknavmenu) {
        if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
          needanotherupdate = true;
          clickquicknavmenu = false;
          model.copyoverwritenosync = !model.copyoverwritenosync;
          if (model.copyoverwritenosync)
            model.message("overwrite existing files");
          else
            model.message("overwrite existing files" +
                "\nonly if their size" +
                "\nis different");
        }
      }
      recti = new RectF(x1, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
      recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
      if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
        RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
        surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
      }
      surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

      mabase += model.GenericCaseH + model.GenericInterSpace;
      if (mabase > mysurf.myheight)
        return;

    }

  }

  private void menurescan(Canvas surfacecanvas, int foldercount) {
    ecritoption = "rescan";
    y1 = mabase;
    y2 = mabase + model.GenericCaseH;
    taillex = model.Menu1TextePaint.measureText(ecritoption);
    x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
    x2 = mysurf.ScreenWidth - model.GenericInterSpace;
    if (x1 > mysurf.SettingsXmin) {
      x1 = mysurf.SettingsXmin;
    }
    ecritx = x2 - model.GenericInterSpace;
    ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
    if (clickquicknavmenu) {
      if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
        mysurf.optionshowrescan = !mysurf.optionshowrescan;
        if (mysurf.optionshowrescan) {
          model.message(
                  "Force a folder rescan now or\n" +
                          "Mark a folder for automatic rescan at startup\n" +
                          "    to rescan it along with all its subfolders.\n" +
                          "You can also click on a folder name section to select it.");
        }
        needanotherupdate = true;
        clickquicknavmenu = false;
      }
    }
    recti = new RectF(x1, y1, x2, y2);
    surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu1BgPainta);
    recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
    surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu1BgPainto);
    if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
      RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
      surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
    }
    surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

    mabase += model.GenericCaseH + model.GenericInterSpace;
    if (mabase > mysurf.myheight)
      return;

    if (mysurf.optionshowrescan) {

      if (foldercount > 0 && mysurf.ordnerIndex >= 0 && mysurf.mediaIndex >= 0) {
        String foldername = model.getOrnderAddress(mysurf.ordnerIndex);
        if (foldername != null)
          ecritoption = "scan " + foldername;
        else
          ecritoption = "scan null";
        y1 = mabase;
        y2 = mabase + model.GenericCaseH;
        taillex = model.Menu1TextePaint.measureText(ecritoption);
        x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
        x2 = mysurf.ScreenWidth - model.GenericInterSpace;
        if (x1 > mysurf.SettingsXmin) {
          x1 = mysurf.SettingsXmin;
        }
        ecritx = x2 - model.GenericInterSpace;
        ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
        if (clickquicknavmenu) {
          if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
            model.message(ecritoption + "...");
            try {
              model.commandethreaddatabase.put(new String[]{"chercheFichierDansDatabaseSinonRescanSonDossier", model.getOrnderAddress(mysurf.ordnerIndex), null, "-1"});
            } catch (InterruptedException e) {
              e.printStackTrace();
            }
            needanotherupdate = true;
            clickquicknavmenu = false;
          }
        }
        recti = new RectF(x1, y1, x2, y2);
        surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
        recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
        surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
        if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
          RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
          surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
        }
        surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

        mabase += model.GenericCaseH + model.GenericInterSpace;
        if (mabase > mysurf.myheight)
          return;
      }

      if (mysurf.optionshowrescan && mysurf.basisfolderclicked == null) {
        ecritoption = "click any folder to pick it";
      } else {
        ecritoption = "scan " + mysurf.basisfolderclicked;
      }
      y1 = mabase;
      y2 = mabase + model.GenericCaseH;
      taillex = model.Menu1TextePaint.measureText(ecritoption);
      x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
      x2 = mysurf.ScreenWidth - model.GenericInterSpace;
      if (x1 > mysurf.SettingsXmin) {
        x1 = mysurf.SettingsXmin;
      }
      ecritx = x2 - model.GenericInterSpace;
      ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
      if (clickquicknavmenu) {
        if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
          if (mysurf.basisfolderclicked != null) {
            model.message(ecritoption + "...");
            try {
              model.commandethreaddatabase.put(new String[]{"chercheFichierDansDatabaseSinonRescanSonDossier", mysurf.basisfolderclicked, null, "-1"});
            } catch (InterruptedException e) {
              e.printStackTrace();
            }
          }
          needanotherupdate = true;
          clickquicknavmenu = false;
        }
      }
      recti = new RectF(x1, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
      recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
      if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
        RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
        surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
      }
      surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

      mabase += model.GenericCaseH + model.GenericInterSpace;
      if (mabase > mysurf.myheight)
        return;

      for (int icartesd = 0; icartesd < model.basefolder.size(); icartesd++) {
        ecritoption = "scan " + model.basefolder.get(icartesd);
        y1 = mabase;
        y2 = mabase + model.GenericCaseH;
        taillex = model.Menu1TextePaint.measureText(ecritoption);
        x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
        x2 = mysurf.ScreenWidth - model.GenericInterSpace;
        if (x1 > mysurf.SettingsXmin) {
          x1 = mysurf.SettingsXmin;
        }
        ecritx = x2 - model.GenericInterSpace;
        ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
        if (clickquicknavmenu) {
          if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
            model.message(ecritoption + "...");
            try {
              model.commandethreaddatabase.put(new String[]{"chercheFichierDansDatabaseSinonRescanSonDossier", model.basefolder.get(icartesd), null, "-1"});
            } catch (InterruptedException e) {
              e.printStackTrace();
            }
            needanotherupdate = true;
            clickquicknavmenu = false;
          }
        }
        recti = new RectF(x1, y1, x2, y2);
        surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
        recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
        surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
        if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
          RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
          surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
        }
        surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

        mabase += model.GenericCaseH + model.GenericInterSpace;
        if (mabase > mysurf.myheight)
          break;
      }

      ecritoption = "auto rescan";
      y1 = mabase;
      y2 = mabase + model.GenericCaseH;
      taillex = model.Menu1TextePaint.measureText(ecritoption);
      x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
      x2 = mysurf.ScreenWidth - model.GenericInterSpace;
      if (x1 > mysurf.SettingsXmin) {
        x1 = mysurf.SettingsXmin;
      }
      ecritx = x2 - model.GenericInterSpace;
      ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
      if (clickquicknavmenu) {
        if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
          Intent intent = new Intent();
          intent.setAction(Gallery.broadcastname);
          intent.putExtra("goal", "autorescan");
          intent.putExtra("id", currid);
          LocalBroadcastManager.getInstance(model.activitycontext).sendBroadcast(intent);
          needanotherupdate = true;
          clickquicknavmenu = false;
        }
      }
      recti = new RectF(x1, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
      recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
      if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
        RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
        surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
      }
      surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

      mabase += model.GenericCaseH + model.GenericInterSpace;
      if (mabase > mysurf.myheight)
        return;

      int nombredossierbookmarked = model.preferences.getInt("nombredossierautorescan", 0);
      for (int i = 0; i < nombredossierbookmarked; i++) {
        // TODO : delete dans le même thread sinon on risque le crash
        String monoption = model.preferences.getString("dossierautorescan" + i, null);
        if (monoption == null)
          continue;

        y1 = mabase;
        y2 = mabase + model.GenericCaseH;
        ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;

        taillex = model.Menu1TextePaint.measureText("x");
        x1 = (mysurf.ScreenWidth - model.GenericInterSpace + mysurf.SettingsXmin) * 0.5f + model.GenericInterSpace * 0.5f;
        x2 = mysurf.ScreenWidth - model.GenericInterSpace;
        ecritx = x2;
        if (clickquicknavmenu) {
          if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
            try {
              model.commandethreaddatabase.put(new String[]{"deleteautorescan", String.valueOf(i)});
            } catch (InterruptedException e) {
              e.printStackTrace();
            }
            needanotherupdate = true;
            clickquicknavmenu = false;
          }
        }
        recti = new RectF(x1, y1, x2, y2);
        //surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
        //recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
        surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
        if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
          RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
          surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
        }
        surfacecanvas.drawText("X", ecritx, ecrity, model.Menu1TextePaint);

        taillex = model.Menu1TextePaint.measureText(monoption);
        x2 = x1 - model.GenericInterSpace * 0.50f;
        x1 = x2 - model.GenericInterSpace * 2.0f - taillex;
        if (x1 > mysurf.SettingsXmin)
          x1 = mysurf.SettingsXmin;
        ecritx = x2 - model.GenericInterSpace;
        if (clickquicknavmenu) {
          if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
            try {
              model.commandethreaddatabase.put(new String[]{"chercheFichierDansDatabaseSinonRescanSonDossier", monoption, null, "-1"});
            } catch (InterruptedException e) {
              e.printStackTrace();
            }
            needanotherupdate = true;
            clickquicknavmenu = false;
          }
        }
        recti = new RectF(x1, y1, x2, y2);
        surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
        recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
        surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
        if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
          RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
          surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
        }
        surfacecanvas.drawText(monoption, ecritx, ecrity, model.Menu1TextePaint);

        mabase += model.GenericCaseH + model.GenericInterSpace;
        if (mabase > mysurf.myheight)
          break;
      }
    }
  }

  private void menutweak(Canvas surfacecanvas, int foldercount) {
    if (!mysurf.optionshowtweak)
      ecritoption = "tweaks";
    else
      ecritoption = "_";
    y1 = mabase;
    y2 = mabase + model.GenericCaseH;
    taillex = model.Menu1TextePaint.measureText(ecritoption);
    x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
    x2 = mysurf.ScreenWidth - model.GenericInterSpace;
    if (x1 > mysurf.SettingsXmin) {
      x1 = mysurf.SettingsXmin;
    }
    ecritx = x2 - model.GenericInterSpace;
    ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
    if (clickquicknavmenu) {
      if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
        mysurf.optionshowtweak = !mysurf.optionshowtweak;
        needanotherupdate = true;
        clickquicknavmenu = false;
      }
    }
    recti = new RectF(x1, y1, x2, y2);
    surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu1BgPainta);
    recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
    surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu1BgPainto);
    if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
      RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
      surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
    }
    surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);
    mabase += model.GenericCaseH + model.GenericInterSpace;
  }

  private void menuroot(Canvas surfacecanvas, int foldercount) {
    if (!mysurf.optionshowroot)
      ecritoption = "root";
    else
      ecritoption = "_";
    y1 = mabase;
    y2 = mabase + model.GenericCaseH;
    taillex = model.Menu1TextePaint.measureText(ecritoption);
    x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
    x2 = mysurf.ScreenWidth - model.GenericInterSpace;
    if (x1 > mysurf.SettingsXmin) {
      x1 = mysurf.SettingsXmin;
    }
    ecritx = x2 - model.GenericInterSpace;
    ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
    if (clickquicknavmenu) {
      if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
        mysurf.optionshowroot = !mysurf.optionshowroot;
        needanotherupdate = true;
        clickquicknavmenu = false;
      }
    }
    recti = new RectF(x1, y1, x2, y2);
    surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu1BgPainta);
    recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
    surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu1BgPainto);
    if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
      RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
      surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
    }
    surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);
    mabase += model.GenericCaseH + model.GenericInterSpace;
  }

  private void menuhelp(Canvas surfacecanvas, int foldercount) {
    if (!mysurf.optionshowhowto)
      ecritoption = "help";
    else
      ecritoption = "_";
    y1 = mabase;
    y2 = mabase + model.GenericCaseH;
    taillex = model.Menu1TextePaint.measureText(ecritoption);
    x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
    x2 = mysurf.ScreenWidth - model.GenericInterSpace;
    if (x1 > mysurf.SettingsXmin) {
      x1 = mysurf.SettingsXmin;
    }
    ecritx = x2 - model.GenericInterSpace;
    ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
    if (clickquicknavmenu) {
      if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
        mysurf.optionshowhowto = !mysurf.optionshowhowto;
        needanotherupdate = true;
        clickquicknavmenu = false;
      }
    }
    recti = new RectF(x1, y1, x2, y2);
    surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu1BgPainta);
    recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
    surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu1BgPainto);
    if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
      RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
      surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
    }
    surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);
    mabase += model.GenericCaseH + model.GenericInterSpace;
  }

  private void menuthumbs(Canvas surfacecanvas, int foldercount) {
    if (mysurf.showthumbnails) {
      ecritoption = "show thumbnails";
    } else {
      ecritoption = "hide thumbnails";
    }
    y1 = mabase;
    y2 = mabase + model.GenericCaseH;
    taillex = model.Menu1TextePaint.measureText(ecritoption);
    x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
    x2 = mysurf.ScreenWidth - model.GenericInterSpace;
    if (x1 > mysurf.SettingsXmin) {
      x1 = mysurf.SettingsXmin;
    }
    ecritx = x2 - model.GenericInterSpace;
    ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
    if (clickquicknavmenu) {
      if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
        mysurf.showthumbnails = !mysurf.showthumbnails;
        needanotherupdate = true;
        clickquicknavmenu = false;
      }
    }
    recti = new RectF(x1, y1, x2, y2);
    surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu1BgPainta);
    recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
    surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu1BgPainto);
    if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
      RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
      surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
    }
    surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

    mabase += model.GenericCaseH + model.GenericInterSpace;

  }

  private void menunames(Canvas surfacecanvas, int foldercount) {
    if (mysurf.showfoldernamesforce) {
      ecritoption = "show folder names";
    } else {
      ecritoption = "hide folder names";
    }
    y1 = mabase;
    y2 = mabase + model.GenericCaseH;
    taillex = model.Menu1TextePaint.measureText(ecritoption);
    x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
    x2 = mysurf.ScreenWidth - model.GenericInterSpace;
    if (x1 > mysurf.SettingsXmin) {
      x1 = mysurf.SettingsXmin;
    }
    ecritx = x2 - model.GenericInterSpace;
    ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
    if (clickquicknavmenu) {
      if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
        mysurf.showfoldernamesforce = !mysurf.showfoldernamesforce;
        //mysurf.showfoldernames = mysurf.showfoldernamesforce;
        needanotherupdate = true;
        clickquicknavmenu = false;
      }
    }
    recti = new RectF(x1, y1, x2, y2);
    surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu1BgPainta);
    recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
    surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu1BgPainto);
    if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
      RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
      surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
    }
    surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

    mabase += model.GenericCaseH + model.GenericInterSpace;

  }

  private void menumpv(Canvas surfacecanvas, int foldercount) {
    if (model.miniaturevideo >= 1) {
      ecritoption = "mini mpv";
    } else {
      ecritoption = "ext. player";
    }
    y1 = mabase;
    y2 = mabase + model.GenericCaseH;
    taillex = model.Menu1TextePaint.measureText(ecritoption);
    x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
    x2 = mysurf.ScreenWidth - model.GenericInterSpace;
    if (x1 > mysurf.SettingsXmin) {
      x1 = mysurf.SettingsXmin;
    }
    ecritx = x2 - model.GenericInterSpace;
    ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
    if (clickquicknavmenu) {
      if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
        if (model.miniaturevideo >= 1) {
          model.miniaturevideo = 0;
          model.message("Videos will be played in an external player.");
        } else {
          model.miniaturevideo = 1;
          model.message("Videos will be played with the integrated player (mpv).");
        }
        needanotherupdate = true;
        clickquicknavmenu = false;
      }
    }
    recti = new RectF(x1, y1, x2, y2);
    surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu1BgPainta);
    recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
    surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu1BgPainto);
    if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
      RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
      surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
    }
    surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

    mabase += model.GenericCaseH + model.GenericInterSpace;

  }

  private void menuandroidtv(Canvas surfacecanvas, int foldercount) {
    if (model.forceatvmode || model.isandroidtv) {
      ecritoption = "tv mode on";
    } else {
      ecritoption = "tv mode off"; // fit window each time for slideshow
    }
    y1 = mabase;
    y2 = mabase + model.GenericCaseH;
    taillex = model.Menu1TextePaint.measureText(ecritoption);
    x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
    x2 = mysurf.ScreenWidth - model.GenericInterSpace;
    if (x1 > mysurf.SettingsXmin) {
      x1 = mysurf.SettingsXmin;
    }
    ecritx = x2 - model.GenericInterSpace;
    ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
    if (clickquicknavmenu) {
      if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
        model.forceatvmode = !model.forceatvmode;
        model.isandroidtv = model.forceatvmode;
        if (model.forceatvmode) {
          model.message("AndroidTV mode enabled (forced).");
        } else {
          model.message("AndroidTV mode disabled.");
        }
        model.preferences.edit().putBoolean("forceatvmode", model.forceatvmode).apply();
        model.setallthepaints();
        needanotherupdate = true;
        clickquicknavmenu = false;
      }
    }
    recti = new RectF(x1, y1, x2, y2);
    surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu1BgPainta);
    recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
    surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu1BgPainto);
    if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
      RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
      surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
    }
    surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

    mabase += model.GenericCaseH + model.GenericInterSpace;

  }

  private void menuwidget(Canvas surfacecanvas, int foldercount) {
    ecritoption = "widgets";
    y1 = mabase;
    y2 = mabase + model.GenericCaseH;
    taillex = model.Menu1TextePaint.measureText(ecritoption);
    x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
    x2 = mysurf.ScreenWidth - model.GenericInterSpace;
    if (x1 > mysurf.SettingsXmin) {
      x1 = mysurf.SettingsXmin;
    }
    ecritx = x2 - model.GenericInterSpace;
    ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
    if (clickquicknavmenu) {
      if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
        mysurf.optionshowwidgets = !mysurf.optionshowwidgets;
        needanotherupdate = true;
        clickquicknavmenu = false;
      }
    }
    recti = new RectF(x1, y1, x2, y2);
    surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu1BgPainta);
    recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
    surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu1BgPainto);
    if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
      RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
      surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
    }
    surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

    mabase += model.GenericCaseH + model.GenericInterSpace;
    if (mabase > mysurf.myheight)
      return;

    if (mysurf.optionshowwidgets) {

      if (model.optionshowwidgetsdisplay) {
        ecritoption = "hide";
      } else {
        ecritoption = "show";
      }
      y1 = mabase;
      y2 = mabase + model.GenericCaseH;
      taillex = model.Menu1TextePaint.measureText(ecritoption);
      x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
      x2 = mysurf.ScreenWidth - model.GenericInterSpace;
      if (x1 > mysurf.SettingsXmin) {
        x1 = mysurf.SettingsXmin;
      }
      ecritx = x2 - model.GenericInterSpace;
      ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
      if (clickquicknavmenu) {
        if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
          model.optionshowwidgetsdisplay = !model.optionshowwidgetsdisplay;
          if (model.optionshowwidgetsdisplay) {
            String colladdress = Gallery.virtualWidgetFolder;
            try {
              model.commandethreaddatabase.put(new String[]{"LoadCollectionFromDiskOrTemp", "load", colladdress, "false"});
              model.commandethreaddatabase.put(new String[]{"addcollectionfolderstodisplay", String.valueOf(currid), colladdress, String.valueOf(mysurf.ordnerIndex + 1)});
            } catch (InterruptedException e) {
              e.printStackTrace();
            }
          } else {
            String colladdress = Gallery.virtualWidgetFolder;
            try {
              model.commandethreaddatabase.put(new String[]{"removedisplayedcollectionfolders", colladdress});
              model.commandethreaddatabase.put(new String[]{"closecollection", colladdress});
            } catch (InterruptedException e) {
              e.printStackTrace();
            }
            model.allappinfo.clear();
          }
          needanotherupdate = true;
          clickquicknavmenu = false;
        }
      }
      recti = new RectF(x1, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
      recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
      if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
        RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
        surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
      }
      surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

      mabase += model.GenericCaseH + model.GenericInterSpace;
      if (mabase > mysurf.myheight)
        return;

      ecritoption = "calendar";
      y1 = mabase;
      y2 = mabase + model.GenericCaseH;
      taillex = model.Menu1TextePaint.measureText(ecritoption);
      x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
      x2 = mysurf.ScreenWidth - model.GenericInterSpace;
      if (x1 > mysurf.SettingsXmin) {
        x1 = mysurf.SettingsXmin;
      }
      ecritx = x2 - model.GenericInterSpace;
      ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
      if (clickquicknavmenu) {
        if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
          Intent intent = new Intent();
          intent.setAction(Gallery.broadcastname);
          intent.putExtra("goal", "lockscreenwidgetconf");
          intent.putExtra("id", currid);
          intent.putExtra("type", Gallery.lockscreencalendarwidget);
          LocalBroadcastManager.getInstance(model.activitycontext).sendBroadcast(intent);
          needanotherupdate = true;
          clickquicknavmenu = false;
        }
      }
      recti = new RectF(x1, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
      recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
      if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
        RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
        surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
      }
      surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

      mabase += model.GenericCaseH + model.GenericInterSpace;
      if (mabase > mysurf.myheight)
        return;

      ecritoption = "countdown";
      y1 = mabase;
      y2 = mabase + model.GenericCaseH;
      taillex = model.Menu1TextePaint.measureText(ecritoption);
      x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
      x2 = mysurf.ScreenWidth - model.GenericInterSpace;
      if (x1 > mysurf.SettingsXmin) {
        x1 = mysurf.SettingsXmin;
      }
      ecritx = x2 - model.GenericInterSpace;
      ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
      if (clickquicknavmenu) {
        if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
          Intent intent = new Intent();
          intent.setAction(Gallery.broadcastname);
          intent.putExtra("goal", "lockscreenwidgetconf");
          intent.putExtra("id", currid);
          intent.putExtra("type", Gallery.lockscreencountdownwidget);
          LocalBroadcastManager.getInstance(model.activitycontext).sendBroadcast(intent);
          needanotherupdate = true;
          clickquicknavmenu = false;
        }
      }
      recti = new RectF(x1, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
      recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
      if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
        RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
        surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
      }
      surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

      mabase += model.GenericCaseH + model.GenericInterSpace;
      if (mabase > mysurf.myheight)
        return;

      ecritoption = "monitor";
      y1 = mabase;
      y2 = mabase + model.GenericCaseH;
      taillex = model.Menu1TextePaint.measureText(ecritoption);
      x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
      x2 = mysurf.ScreenWidth - model.GenericInterSpace;
      if (x1 > mysurf.SettingsXmin) {
        x1 = mysurf.SettingsXmin;
      }
      ecritx = x2 - model.GenericInterSpace;
      ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
      if (clickquicknavmenu) {
        if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
          Intent intent = new Intent();
          intent.setAction(Gallery.broadcastname);
          intent.putExtra("goal", "lockscreenwidgetconf");
          intent.putExtra("id", currid);
          intent.putExtra("type", Gallery.lockscreenmonitorwidget);
          LocalBroadcastManager.getInstance(model.activitycontext).sendBroadcast(intent);
          needanotherupdate = true;
          clickquicknavmenu = false;
        }
      }
      recti = new RectF(x1, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
      recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
      if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
        RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
        surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
      }
      surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

      mabase += model.GenericCaseH + model.GenericInterSpace;
      if (mabase > mysurf.myheight)
        return;

    }

  }


  private void menuapp(Canvas surfacecanvas, int foldercount) {
    ecritoption = "apps";
    y1 = mabase;
    y2 = mabase + model.GenericCaseH;
    taillex = model.Menu1TextePaint.measureText(ecritoption);
    x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
    x2 = mysurf.ScreenWidth - model.GenericInterSpace;
    if (x1 > mysurf.SettingsXmin) {
      x1 = mysurf.SettingsXmin;
    }
    ecritx = x2 - model.GenericInterSpace;
    ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
    if (clickquicknavmenu) {
      if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
        mysurf.optionshowapps = !mysurf.optionshowapps;
        needanotherupdate = true;
        clickquicknavmenu = false;
      }
    }
    recti = new RectF(x1, y1, x2, y2);
    surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu1BgPainta);
    recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
    surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu1BgPainto);
    if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
      RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
      surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
    }
    surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

    mabase += model.GenericCaseH + model.GenericInterSpace;
    if (mabase > mysurf.myheight)
      return;

    if (mysurf.optionshowapps) {
      if (model.optionshowappsdisplay)
        ecritoption = "hide";
      else
        ecritoption = "display";
      y1 = mabase;
      y2 = mabase + model.GenericCaseH;
      taillex = model.Menu1TextePaint.measureText(ecritoption);
      x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
      x2 = mysurf.ScreenWidth - model.GenericInterSpace;
      if (x1 > mysurf.SettingsXmin) {
        x1 = mysurf.SettingsXmin;
      }
      ecritx = x2 - model.GenericInterSpace;
      ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
      if (clickquicknavmenu) {
        if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
          model.optionshowappsdisplay = !model.optionshowappsdisplay;
          if (model.optionshowappsdisplay) {
            String colladdress = Gallery.virtualAppFolder;
            try {
              model.commandethreaddatabase.put(new String[]{"scanapps", String.valueOf(currid), "reinitialize", "null", "null"});
              model.commandethreaddatabase.put(new String[]{"LoadCollectionFromDiskOrTemp", "load", colladdress, "false"});
              model.commandethreaddatabase.put(new String[]{"addcollectionfolderstodisplay", String.valueOf(currid), colladdress, String.valueOf(mysurf.ordnerIndex + 1)});
            } catch (InterruptedException e) {
              e.printStackTrace();
            }
          } else {
            String colladdress = Gallery.virtualAppFolder;
            try {
              model.commandethreaddatabase.put(new String[]{"removedisplayedcollectionfolders", colladdress});
              model.commandethreaddatabase.put(new String[]{"closecollection", colladdress});
            } catch (InterruptedException e) {
              e.printStackTrace();
            }
            model.allappinfo.clear();
          }
          /*if (model.optionshowbookmarkeddisplay) {
            Collection collection = new Collection();
            collection.address = Gallery.virtualBookmarkFolder + ".sel";
            collection.printName = "bookmarks";
            collection.reloadBookmarksFromPreferences(model.preferences, model.db);
            if (collection.elementList.size() == 0) {
              model.message("empty bookmark list");
              model.optionshowbookmarkeddisplay = false;
            } else {
              int currfolder = mysurf.ordnerIndex + 1;
              model.collectionsToDisplay(collection.address, currfolder);
              List<Ordner> gallery = collection.collectionToDisplay(null);
              for (int i = 0 ; i < gallery.size() ; i++) {
                Ordner ordner = gallery.get(i);
                model.addFolder(currfolder + i, ordner);
              }
              model.changeBigPicture(currid, currfolder, 0, -1, 0, false, false);
              model.optionshowbookmarkeddisplay = true;
            }
          } else {
            int found = model.findFolder(Gallery.virtualBookmarkFolder);
            if (found != -1)
              model.removeFolder(found);
            model.optionshowbookmarkeddisplay = false;
          }*/
          needanotherupdate = true;
          clickquicknavmenu = false;
        }
      }
      recti = new RectF(x1, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
      recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
      if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
        RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
        surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
      }
      surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

      mabase += model.GenericCaseH + model.GenericInterSpace;
      if (mabase > mysurf.myheight)
        return;


      if (model.optionshowappsdisplay) {

        ecritoption = "enable";
        y1 = mabase;
        y2 = mabase + model.GenericCaseH;
        taillex = model.Menu1TextePaint.measureText(ecritoption);
        x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
        x2 = mysurf.ScreenWidth - model.GenericInterSpace;
        if (x1 > mysurf.SettingsXmin) {
          x1 = mysurf.SettingsXmin;
        }
        ecritx = x2 - model.GenericInterSpace;
        ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
        if (clickquicknavmenu) {
          if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
            String command = mysurf.mediaIndexAddress;
            try {
              model.commandethreaddatabase.put(new String[]{"scanapps", String.valueOf(currid), "enable.sh", command, "refreshappsthumbs"});
            } catch (InterruptedException e) {
              e.printStackTrace();
            }
            needanotherupdate = true;
            clickquicknavmenu = false;
          }
        }
        recti = new RectF(x1, y1, x2, y2);
        surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
        recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
        surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
        if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
          RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
          surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
        }
        surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

        mabase += model.GenericCaseH + model.GenericInterSpace;
        if (mabase > mysurf.myheight)
          return;

        ecritoption = "disable";
        y1 = mabase;
        y2 = mabase + model.GenericCaseH;
        taillex = model.Menu1TextePaint.measureText(ecritoption);
        x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
        x2 = mysurf.ScreenWidth - model.GenericInterSpace;
        if (x1 > mysurf.SettingsXmin) {
          x1 = mysurf.SettingsXmin;
        }
        ecritx = x2 - model.GenericInterSpace;
        ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
        if (clickquicknavmenu) {
          if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
            String command = mysurf.mediaIndexAddress;
            try {
              model.commandethreaddatabase.put(new String[]{"scanapps", String.valueOf(currid), "disable.sh", command, "refreshappsthumbs"});
            } catch (InterruptedException e) {
              e.printStackTrace();
            }
            needanotherupdate = true;
            clickquicknavmenu = false;
          }
        }
        recti = new RectF(x1, y1, x2, y2);
        surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
        recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
        surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
        if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
          RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
          surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
        }
        surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

        mabase += model.GenericCaseH + model.GenericInterSpace;
        if (mabase > mysurf.myheight)
          return;

        ecritoption = "stop";
        y1 = mabase;
        y2 = mabase + model.GenericCaseH;
        taillex = model.Menu1TextePaint.measureText(ecritoption);
        x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
        x2 = mysurf.ScreenWidth - model.GenericInterSpace;
        if (x1 > mysurf.SettingsXmin) {
          x1 = mysurf.SettingsXmin;
        }
        ecritx = x2 - model.GenericInterSpace;
        ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
        if (clickquicknavmenu) {
          if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
            String command = mysurf.mediaIndexAddress;
            try {
              model.commandethreaddatabase.put(new String[]{"scanapps", String.valueOf(currid), "stop.sh", command, "refreshappsthumbs"});
            } catch (InterruptedException e) {
              e.printStackTrace();
            }
            needanotherupdate = true;
            clickquicknavmenu = false;
          }
        }
        recti = new RectF(x1, y1, x2, y2);
        surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
        recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
        surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
        if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
          RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
          surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
        }
        surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

        mabase += model.GenericCaseH + model.GenericInterSpace;
        if (mabase > mysurf.myheight)
          return;

        ecritoption = "clear";
        y1 = mabase;
        y2 = mabase + model.GenericCaseH;
        taillex = model.Menu1TextePaint.measureText(ecritoption);
        x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
        x2 = mysurf.ScreenWidth - model.GenericInterSpace;
        if (x1 > mysurf.SettingsXmin) {
          x1 = mysurf.SettingsXmin;
        }
        ecritx = x2 - model.GenericInterSpace;
        ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
        if (clickquicknavmenu) {
          if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
            String command = mysurf.mediaIndexAddress;
            try {
              model.commandethreaddatabase.put(new String[]{"scanapps", String.valueOf(currid), "clear.sh", command, "refreshappsthumbs"});
            } catch (InterruptedException e) {
              e.printStackTrace();
            }
            needanotherupdate = true;
            clickquicknavmenu = false;
          }
        }
        recti = new RectF(x1, y1, x2, y2);
        surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
        recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
        surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
        if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
          RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
          surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
        }
        surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

        mabase += model.GenericCaseH + model.GenericInterSpace;
        if (mabase > mysurf.myheight)
          return;

        ecritoption = "start";
        y1 = mabase;
        y2 = mabase + model.GenericCaseH;
        taillex = model.Menu1TextePaint.measureText(ecritoption);
        x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
        x2 = mysurf.ScreenWidth - model.GenericInterSpace;
        if (x1 > mysurf.SettingsXmin) {
          x1 = mysurf.SettingsXmin;
        }
        ecritx = x2 - model.GenericInterSpace;
        ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
        if (clickquicknavmenu) {
          if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
            String command = mysurf.mediaIndexAddress;
            try {
              model.commandethreaddatabase.put(new String[]{"scanapps", String.valueOf(currid), "start.sh", command, "refreshappsthumbs"});
            } catch (InterruptedException e) {
              e.printStackTrace();
            }
            needanotherupdate = true;
            clickquicknavmenu = false;
          }
        }
        recti = new RectF(x1, y1, x2, y2);
        surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
        recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
        surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
        if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
          RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
          surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
        }
        surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

        mabase += model.GenericCaseH + model.GenericInterSpace;
        if (mabase > mysurf.myheight)
          return;

        ecritoption = "uninstall fully";
        y1 = mabase;
        y2 = mabase + model.GenericCaseH;
        taillex = model.Menu1TextePaint.measureText(ecritoption);
        x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
        x2 = mysurf.ScreenWidth - model.GenericInterSpace;
        if (x1 > mysurf.SettingsXmin) {
          x1 = mysurf.SettingsXmin;
        }
        ecritx = x2 - model.GenericInterSpace;
        ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
        if (clickquicknavmenu) {
          if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
            String command = mysurf.mediaIndexAddress + " full";
            try {
              model.commandethreaddatabase.put(new String[]{"scanapps", String.valueOf(currid), "uninstall.sh", command, "refreshappsthumbs"});
            } catch (InterruptedException e) {
              e.printStackTrace();
            }
            needanotherupdate = true;
            clickquicknavmenu = false;
          }
        }
        recti = new RectF(x1, y1, x2, y2);
        surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
        recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
        surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
        if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
          RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
          surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
        }
        surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

        mabase += model.GenericCaseH + model.GenericInterSpace;
        if (mabase > mysurf.myheight)
          return;

        ecritoption = "uninstall update";
        y1 = mabase;
        y2 = mabase + model.GenericCaseH;
        taillex = model.Menu1TextePaint.measureText(ecritoption);
        x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
        x2 = mysurf.ScreenWidth - model.GenericInterSpace;
        if (x1 > mysurf.SettingsXmin) {
          x1 = mysurf.SettingsXmin;
        }
        ecritx = x2 - model.GenericInterSpace;
        ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
        if (clickquicknavmenu) {
          if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
            String command = mysurf.mediaIndexAddress + " update";
            try {
              model.commandethreaddatabase.put(new String[]{"scanapps", String.valueOf(currid), "uninstall.sh", command, "refreshappsthumbs"});
            } catch (InterruptedException e) {
              e.printStackTrace();
            }
            needanotherupdate = true;
            clickquicknavmenu = false;
          }
        }
        recti = new RectF(x1, y1, x2, y2);
        surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
        recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
        surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
        if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
          RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
          surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
        }
        surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

        mabase += model.GenericCaseH + model.GenericInterSpace;
        if (mabase > mysurf.myheight)
          return;

        ecritoption = "uninstall keep data";
        y1 = mabase;
        y2 = mabase + model.GenericCaseH;
        taillex = model.Menu1TextePaint.measureText(ecritoption);
        x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
        x2 = mysurf.ScreenWidth - model.GenericInterSpace;
        if (x1 > mysurf.SettingsXmin) {
          x1 = mysurf.SettingsXmin;
        }
        ecritx = x2 - model.GenericInterSpace;
        ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
        if (clickquicknavmenu) {
          if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
            String command = mysurf.mediaIndexAddress + " keep";
            try {
              model.commandethreaddatabase.put(new String[]{"scanapps", String.valueOf(currid), "uninstall.sh", command, "refreshappsthumbs"});
            } catch (InterruptedException e) {
              e.printStackTrace();
            }
            needanotherupdate = true;
            clickquicknavmenu = false;
          }
        }
        recti = new RectF(x1, y1, x2, y2);
        surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
        recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
        surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
        if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
          RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
          surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
        }
        surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

        mabase += model.GenericCaseH + model.GenericInterSpace;
        if (mabase > mysurf.myheight)
          return;

        ecritoption = "magisk uninstall";
        y1 = mabase;
        y2 = mabase + model.GenericCaseH;
        taillex = model.Menu1TextePaint.measureText(ecritoption);
        x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
        x2 = mysurf.ScreenWidth - model.GenericInterSpace;
        if (x1 > mysurf.SettingsXmin) {
          x1 = mysurf.SettingsXmin;
        }
        ecritx = x2 - model.GenericInterSpace;
        ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
        if (clickquicknavmenu) {
          if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
            int allapinfol = model.allappinfo.size();
            for (int i = 0; i < allapinfol; i++) {
              AppInfo appinfo = model.allappinfo.get(i);
              if (appinfo.packagename.equals(mysurf.mediaIndexAddress)) {
                String command = mysurf.mediaIndexAddress + " " + appinfo.sourcedir;
                try {
                  model.commandethreaddatabase.put(new String[]{"scanapps", String.valueOf(currid), "magiskuninstall.sh", command, "refreshappsthumbs"});
                } catch (InterruptedException e) {
                  e.printStackTrace();
                }
                break;
              }
            }
            needanotherupdate = true;
            clickquicknavmenu = false;
          }
        }
        recti = new RectF(x1, y1, x2, y2);
        surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
        recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
        surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
        if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
          RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
          surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
        }
        surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

        mabase += model.GenericCaseH + model.GenericInterSpace;
        if (mabase > mysurf.myheight)
          return;

        ecritoption = "magisk restore";
        y1 = mabase;
        y2 = mabase + model.GenericCaseH;
        taillex = model.Menu1TextePaint.measureText(ecritoption);
        x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
        x2 = mysurf.ScreenWidth - model.GenericInterSpace;
        if (x1 > mysurf.SettingsXmin) {
          x1 = mysurf.SettingsXmin;
        }
        ecritx = x2 - model.GenericInterSpace;
        ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
        if (clickquicknavmenu) {
          if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
            int allapinfol = model.allappinfo.size();
            for (int i = 0; i < allapinfol; i++) {
              AppInfo appinfo = model.allappinfo.get(i);
              if (appinfo.packagename.equals(mysurf.mediaIndexAddress)) {
                String command = mysurf.mediaIndexAddress + " " + appinfo.sourcedir;
                try {
                  model.commandethreaddatabase.put(new String[]{"scanapps", String.valueOf(currid), "magiskrestore.sh", command, "refreshappsthumbs"});
                } catch (InterruptedException e) {
                  e.printStackTrace();
                }
                break;
              }
            }
            needanotherupdate = true;
            clickquicknavmenu = false;
          }
        }
        recti = new RectF(x1, y1, x2, y2);
        surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
        recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
        surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
        if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
          RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
          surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
        }
        surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

        mabase += model.GenericCaseH + model.GenericInterSpace;
        if (mabase > mysurf.myheight)
          return;

        int allappinfol = model.allappinfo.size();
        int item = -1;
        for (int i = 0; i < allappinfol; i++) {
          if (model.allappinfo.get(i).packagename.equals(mysurf.mediaIndexAddress)) {
            item = i;
            break;
          }
        }
        if (item >= 0) {
          String packagename = mysurf.mediaIndexAddress;
          AppInfo appinfo = model.allappinfo.get(item);

          for (int j = 0; j < appinfo.servicel; j++) {
            boolean prefdis = false;
            String command = packagename + " " + appinfo.servicename[j];
            if (model.preferences.contains("servicedisabled" + command))
              prefdis = true;
            if (appinfo.servicestatuscurrent[j] == ShellExecuter.isENABLED) {
              ecritoption = appinfo.servicename[j] + "    ";
              y1 = mabase;
              y2 = mabase + model.GenericCaseH;
              taillex = model.Menu1TextePaint.measureText(ecritoption);
              x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
              x2 = mysurf.ScreenWidth - model.GenericInterSpace;
              if (x1 > mysurf.SettingsXmin) {
                x1 = mysurf.SettingsXmin;
              }
              ecritx = x2 - model.GenericInterSpace;
              ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
              if (clickquicknavmenu) {
                if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
                  if (appinfo.servicestatuscurrent[j] == ShellExecuter.isDISABLED || prefdis) {
                    try {
                      model.commandethreaddatabase.put(new String[]{"scanapps", String.valueOf(currid), "enable.sh", command, "refreshappsthumbs"});
                      model.commandethreaddatabase.put(new String[]{"deleteservicedisabled", command});
                    } catch (InterruptedException e) {
                      e.printStackTrace();
                    }
                  } else {
                    try {
                      model.commandethreaddatabase.put(new String[]{"scanapps", String.valueOf(currid), "disable.sh", command, "refreshappsthumbs"});
                      model.commandethreaddatabase.put(new String[]{"addservicedisabled", command});
                    } catch (InterruptedException e) {
                      e.printStackTrace();
                    }
                  }
                  needanotherupdate = true;
                  clickquicknavmenu = false;
                }
              }
              recti = new RectF(x1, y1, x2, y2);
              surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
              recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
              surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
              if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
                RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
                surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
              }
              model.MenuStatusPaint.setColor(Color.rgb(53, 235, 53));
              surfacecanvas.drawLine(x2 - model.GenericInterSpace * 2, y1, x2 - model.GenericInterSpace * 2, y2, model.MenuStatusPaint);
              if (prefdis) {
                model.MenuStatusPaint.setColor(Color.rgb(224, 144, 45));
                surfacecanvas.drawLine(x2 - model.GenericInterSpace * 3, y1, x2 - model.GenericInterSpace * 3, y2, model.MenuStatusPaint);
              }
              surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

              mabase += model.GenericCaseH + model.GenericInterSpace;
              if (mabase > mysurf.myheight)
                return;
            }
          }

          for (int j = 0; j < appinfo.servicel; j++) {
            boolean prefdis = false;
            String command = packagename + " " + appinfo.servicename[j];
            if (model.preferences.contains("servicedisabled" + command))
              prefdis = true;
            if (appinfo.servicestatuscurrent[j] == ShellExecuter.isDISABLED
                || (prefdis && appinfo.servicestatuscurrent[j] != ShellExecuter.isENABLED)
            ) {
              ecritoption = appinfo.servicename[j] + "    ";
              y1 = mabase;
              y2 = mabase + model.GenericCaseH;
              taillex = model.Menu1TextePaint.measureText(ecritoption);
              x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
              x2 = mysurf.ScreenWidth - model.GenericInterSpace;
              if (x1 > mysurf.SettingsXmin) {
                x1 = mysurf.SettingsXmin;
              }
              ecritx = x2 - model.GenericInterSpace;
              ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
              if (clickquicknavmenu) {
                if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
                  if (appinfo.servicestatuscurrent[j] == ShellExecuter.isDISABLED || prefdis) {
                    try {
                      model.commandethreaddatabase.put(new String[]{"scanapps", String.valueOf(currid), "enable.sh", command, "refreshappsthumbs"});
                      model.commandethreaddatabase.put(new String[]{"deleteservicedisabled", command});
                    } catch (InterruptedException e) {
                      e.printStackTrace();
                    }
                  } else {
                    try {
                      model.commandethreaddatabase.put(new String[]{"scanapps", String.valueOf(currid), "disable.sh", command, "refreshappsthumbs"});
                      model.commandethreaddatabase.put(new String[]{"addservicedisabled", command});
                    } catch (InterruptedException e) {
                      e.printStackTrace();
                    }
                  }
                  needanotherupdate = true;
                  clickquicknavmenu = false;
                }
              }
              recti = new RectF(x1, y1, x2, y2);
              surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
              recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
              surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
              if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
                RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
                surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
              }
              model.MenuStatusPaint.setColor(Color.rgb(224, 45, 45));
              surfacecanvas.drawLine(x2 - model.GenericInterSpace * 4, y1, x2 - model.GenericInterSpace * 4, y2, model.MenuStatusPaint);
              if (prefdis) {
                model.MenuStatusPaint.setColor(Color.rgb(224, 144, 45));
                surfacecanvas.drawLine(x2 - model.GenericInterSpace * 3, y1, x2 - model.GenericInterSpace * 3, y2, model.MenuStatusPaint);
              }
              surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

              mabase += model.GenericCaseH + model.GenericInterSpace;
              if (mabase > mysurf.myheight)
                return;
            }
          }

          ecritoption = "services " + appinfo.servicel;
          y1 = mabase;
          y2 = mabase + model.GenericCaseH;
          taillex = model.Menu1TextePaint.measureText(ecritoption);
          x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
          x2 = mysurf.ScreenWidth - model.GenericInterSpace;
          if (x1 > mysurf.SettingsXmin) {
            x1 = mysurf.SettingsXmin;
          }
          ecritx = x2 - model.GenericInterSpace;
          ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
          if (clickquicknavmenu) {
            if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
              model.optionshowappsdisplays = !model.optionshowappsdisplays;
              needanotherupdate = true;
              clickquicknavmenu = false;
            }
          }
          recti = new RectF(x1, y1, x2, y2);
          surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
          recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
          surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
          if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
            RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
            surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
          }
          surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

          mabase += model.GenericCaseH + model.GenericInterSpace;
          if (mabase > mysurf.myheight)
            return;

          if (model.optionshowappsdisplays) {
            for (int j = 0; j < appinfo.servicel; j++) {
              boolean prefdis = false;
              String command = packagename + " " + appinfo.servicename[j];
              if (model.preferences.contains("servicedisabled" + command))
                prefdis = true;
              if (appinfo.servicestatuscurrent[j] == ShellExecuter.isENABLED
                  || appinfo.servicestatuscurrent[j] == ShellExecuter.isDISABLED
                  || prefdis
              ) {
              } else {
                ecritoption = appinfo.servicename[j] + "    ";
                y1 = mabase;
                y2 = mabase + model.GenericCaseH;
                taillex = model.Menu1TextePaint.measureText(ecritoption);
                x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
                x2 = mysurf.ScreenWidth - model.GenericInterSpace;
                if (x1 > mysurf.SettingsXmin) {
                  x1 = mysurf.SettingsXmin;
                }
                ecritx = x2 - model.GenericInterSpace;
                ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
                if (clickquicknavmenu) {
                  if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
                    if (appinfo.servicestatuscurrent[j] == ShellExecuter.isDISABLED || prefdis) {
                      try {
                        model.commandethreaddatabase.put(new String[]{"scanapps", String.valueOf(currid), "enable.sh", command, "refreshappsthumbs"});
                        model.commandethreaddatabase.put(new String[]{"deleteservicedisabled", command});
                      } catch (InterruptedException e) {
                        e.printStackTrace();
                      }
                    } else {
                      try {
                        model.commandethreaddatabase.put(new String[]{"scanapps", String.valueOf(currid), "disable.sh", command, "refreshappsthumbs"});
                        model.commandethreaddatabase.put(new String[]{"addservicedisabled", command});
                      } catch (InterruptedException e) {
                        e.printStackTrace();
                      }
                    }
                    needanotherupdate = true;
                    clickquicknavmenu = false;
                  }
                }
                recti = new RectF(x1, y1, x2, y2);
                surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
                recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
                surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
                if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
                  RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
                  surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
                }
                surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

                mabase += model.GenericCaseH + model.GenericInterSpace;
                if (mabase > mysurf.myheight)
                  return;
              }
            }
          }

          ecritoption = "activities " + appinfo.activityl;
          y1 = mabase;
          y2 = mabase + model.GenericCaseH;
          taillex = model.Menu1TextePaint.measureText(ecritoption);
          x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
          x2 = mysurf.ScreenWidth - model.GenericInterSpace;
          if (x1 > mysurf.SettingsXmin) {
            x1 = mysurf.SettingsXmin;
          }
          ecritx = x2 - model.GenericInterSpace;
          ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
          if (clickquicknavmenu) {
            if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
              model.optionshowappsdisplaya = !model.optionshowappsdisplaya;
              needanotherupdate = true;
              clickquicknavmenu = false;
            }
          }
          recti = new RectF(x1, y1, x2, y2);
          surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
          recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
          surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
          if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
            RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
            surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
          }
          surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

          mabase += model.GenericCaseH + model.GenericInterSpace;
          if (mabase > mysurf.myheight)
            return;

          if (model.optionshowappsdisplaya) {
            for (int j = 0; j < appinfo.activityl; j++) {
              String command = packagename + " " + appinfo.activityname[j];
              ecritoption = appinfo.activityname[j] + "    ";
              y1 = mabase;
              y2 = mabase + model.GenericCaseH;
              taillex = model.Menu1TextePaint.measureText(ecritoption);
              x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
              x2 = mysurf.ScreenWidth - model.GenericInterSpace;
              if (x1 > mysurf.SettingsXmin) {
                x1 = mysurf.SettingsXmin;
              }
              ecritx = x2 - model.GenericInterSpace;
              ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
              if (clickquicknavmenu) {
                if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
                  try {
                    model.commandethreaddatabase.put(new String[]{"scanapps", String.valueOf(currid), "disable.sh", command, "refreshappsthumbs"});
                  } catch (InterruptedException e) {
                    e.printStackTrace();
                  }
                  needanotherupdate = true;
                  clickquicknavmenu = false;
                }
              }
              recti = new RectF(x1, y1, x2, y2);
              surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
              recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
              surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
              if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
                RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
                surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
              }
              surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

              mabase += model.GenericCaseH + model.GenericInterSpace;
              if (mabase > mysurf.myheight)
                return;
            }
          }

          ecritoption = "permissions " + appinfo.permissionl;
          y1 = mabase;
          y2 = mabase + model.GenericCaseH;
          taillex = model.Menu1TextePaint.measureText(ecritoption);
          x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
          x2 = mysurf.ScreenWidth - model.GenericInterSpace;
          if (x1 > mysurf.SettingsXmin) {
            x1 = mysurf.SettingsXmin;
          }
          ecritx = x2 - model.GenericInterSpace;
          ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
          if (clickquicknavmenu) {
            if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
              model.optionshowappsdisplayp = !model.optionshowappsdisplayp;
              needanotherupdate = true;
              clickquicknavmenu = false;
            }
          }
          recti = new RectF(x1, y1, x2, y2);
          surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
          recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
          surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
          if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
            RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
            surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
          }
          surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

          mabase += model.GenericCaseH + model.GenericInterSpace;
          if (mabase > mysurf.myheight)
            return;

          if (model.optionshowappsdisplayp) {
            for (int j = 0; j < appinfo.permissionl; j++) {
              String command = packagename + " permission " + appinfo.permissionname[j];
              ecritoption = appinfo.permissionname[j] + "    ";
              y1 = mabase;
              y2 = mabase + model.GenericCaseH;
              taillex = model.Menu1TextePaint.measureText(ecritoption);
              x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
              x2 = mysurf.ScreenWidth - model.GenericInterSpace;
              if (x1 > mysurf.SettingsXmin) {
                x1 = mysurf.SettingsXmin;
              }
              ecritx = x2 - model.GenericInterSpace;
              ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
              if (clickquicknavmenu) {
                if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
                  try {
                    model.commandethreaddatabase.put(new String[]{"scanapps", String.valueOf(currid), "disable.sh", command, "refreshappsthumbs"});
                  } catch (InterruptedException e) {
                    e.printStackTrace();
                  }
                  needanotherupdate = true;
                  clickquicknavmenu = false;
                }
              }
              recti = new RectF(x1, y1, x2, y2);
              surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
              recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
              surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
              if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
                RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
                surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
              }
              surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

              mabase += model.GenericCaseH + model.GenericInterSpace;
              if (mabase > mysurf.myheight)
                return;
            }
          }


        }

      }

    }

  }

  private void menurootcmd(Canvas surfacecanvas, int foldercount) {
    y1 = mabase;
    y2 = mabase + model.GenericCaseH;
    ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;

    ecritoption = "...";
    taillex = model.Menu1TextePaint.measureText(ecritoption);
    x1 = (mysurf.ScreenWidth - model.GenericInterSpace + mysurf.SettingsXmin) * 0.5f + model.GenericInterSpace * 0.5f;
    x2 = mysurf.ScreenWidth - model.GenericInterSpace;
    ecritx = x2;
    if (clickquicknavmenu) {
      if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
        Intent intent = new Intent();
        intent.setAction(Gallery.broadcastname);
        intent.putExtra("goal", "rootcommand");
        intent.putExtra("id", currid);
        intent.putExtra("default", model.runcmd);
        LocalBroadcastManager.getInstance(model.activitycontext).sendBroadcast(intent);
        needanotherupdate = true;
        clickquicknavmenu = false;
      }
    }
    recti = new RectF(x1, y1, x2, y2);
    //surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu1BgPainta);
    //recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
    surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu1BgPainto);
    if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
      RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
      surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
    }
    surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

    ecritoption = "run cmd";
    taillex = model.Menu1TextePaint.measureText(ecritoption);
    x2 = x1 - model.GenericInterSpace;
    x1 = x2 - model.GenericInterSpace * 2.0f - taillex;
    if (x1 > mysurf.SettingsXmin)
      x1 = mysurf.SettingsXmin;
    ecritx = x2 - model.GenericInterSpace;
    if (clickquicknavmenu) {
      if (mysurf.SettingsXmin < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
        model.message("executes a shell command" +
                "\n" + model.runcmd);
        try {
          model.commandethreaddatabase.put(new String[]{"scanapps", String.valueOf(currid), "runcmd", model.runcmd, "null"});
        } catch (InterruptedException e) {
          e.printStackTrace();
        }
        needanotherupdate = true;
        clickquicknavmenu = false;
      }
    }
    recti = new RectF(x1, y1, x2, y2);
    surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu1BgPainta);
    recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
    surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu1BgPainto);
    if ((model.isandroidtv || model.keypadnavactive) && mysurf.SettingsXmin < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
      RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
      surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
    }
    surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

    mabase += model.GenericCaseH + model.GenericInterSpace;
    if (mabase > mysurf.myheight)
      return;
  }

  private void menurecord(Canvas surfacecanvas, int foldercount) {
    ecritoption = "screenshot";
    y1 = mabase;
    y2 = mabase + model.GenericCaseH;
    taillex = model.Menu1TextePaint.measureText(ecritoption);
    x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
    x2 = mysurf.ScreenWidth - model.GenericInterSpace;
    if (x1 > mysurf.SettingsXmin) {
      x1 = mysurf.SettingsXmin;
    }
    ecritx = x2 - model.GenericInterSpace;
    ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
    if (clickquicknavmenu) {
      if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
        mysurf.optionshowrecord = !mysurf.optionshowrecord;
        if (mysurf.optionshowrecord)
          model.message("Only for rooted devices.\nExecutes the screencap shell command.");
        needanotherupdate = true;
        clickquicknavmenu = false;
      }
    }
    recti = new RectF(x1, y1, x2, y2);
    surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu1BgPainta);
    recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
    surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu1BgPainto);
    if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
      RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
      surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
    }
    surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

    mabase += model.GenericCaseH + model.GenericInterSpace;
    if (mabase > mysurf.myheight)
      return;

    if (mysurf.optionshowrecord) {
      y1 = mabase;
      y2 = mabase + model.GenericCaseH;
      ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;

      ecritoption = "+";
      taillex = model.Menu1TextePaint.measureText(ecritoption);
      x1 = (mysurf.ScreenWidth - model.GenericInterSpace + mysurf.SettingsXmin) * 0.5f + model.GenericInterSpace * 0.5f;
      x2 = mysurf.ScreenWidth - model.GenericInterSpace;
      ecritx = x2;
      if (clickquicknavmenu) {
        if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
          model.screenshotdelay += 1;
          needanotherupdate = true;
          clickquicknavmenu = false;
        }
      }
      recti = new RectF(x1, y1, x2, y2);
      //surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
      //recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
      if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
        RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
        surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
      }
      surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

      ecritoption = "delay " + model.screenshotdelay + "s -";
      taillex = model.Menu1TextePaint.measureText(ecritoption);
      x2 = x1 - model.GenericInterSpace;
      x1 = x2 - model.GenericInterSpace * 2.0f - taillex;
      if (x1 > mysurf.SettingsXmin)
        x1 = mysurf.SettingsXmin;
      ecritx = x2 - model.GenericInterSpace;
      if (clickquicknavmenu) {
        if (mysurf.SettingsXmin < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
          if (model.screenshotdelay > 0)
            model.screenshotdelay -= 1;
          needanotherupdate = true;
          clickquicknavmenu = false;
        }
      }
      recti = new RectF(x1, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
      recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
      if ((model.isandroidtv || model.keypadnavactive) && mysurf.SettingsXmin < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
        RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
        surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
      }
      surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

      mabase += model.GenericCaseH + model.GenericInterSpace;
      if (mabase > mysurf.myheight)
        return;

      ecritoption = "screenshot";
      y1 = mabase;
      y2 = mabase + model.GenericCaseH;
      taillex = model.Menu1TextePaint.measureText(ecritoption);
      x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
      x2 = mysurf.ScreenWidth - model.GenericInterSpace;
      if (x1 > mysurf.SettingsXmin) {
        x1 = mysurf.SettingsXmin;
      }
      ecritx = x2 - model.GenericInterSpace;
      ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
      if (clickquicknavmenu) {
        if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
          model.message("screenshot in 10s...");
          shellexecute(true);
          needanotherupdate = true;
          clickquicknavmenu = false;
        }
      }
      recti = new RectF(x1, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
      recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
      if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
        RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
        surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
      }
      surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

      mabase += model.GenericCaseH + model.GenericInterSpace;
      if (mabase > mysurf.myheight)
        return;

      y1 = mabase;
      y2 = mabase + model.GenericCaseH;
      ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;

      ecritoption = "+";
      taillex = model.Menu1TextePaint.measureText(ecritoption);
      x1 = (mysurf.ScreenWidth - model.GenericInterSpace + mysurf.SettingsXmin) * 0.5f + model.GenericInterSpace * 0.5f;
      x2 = mysurf.ScreenWidth - model.GenericInterSpace;
      ecritx = x2;
      if (clickquicknavmenu) {
        if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
          model.screenrecordlength += model.screenrecordlengthu;
          needanotherupdate = true;
          clickquicknavmenu = false;
        }
      }
      recti = new RectF(x1, y1, x2, y2);
      //surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
      //recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
      if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
        RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
        surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
      }
      surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

      if (model.screenrecordlength <= 0)
        ecritoption = "length " + model.screenrecordlength + "s (180s) -";
      else
        ecritoption = "length " + model.screenrecordlength + "s -";
      taillex = model.Menu1TextePaint.measureText(ecritoption);
      x2 = x1 - model.GenericInterSpace;
      x1 = x2 - model.GenericInterSpace * 2.0f - taillex;
      if (x1 > mysurf.SettingsXmin)
        x1 = mysurf.SettingsXmin;
      ecritx = x2 - model.GenericInterSpace;
      if (clickquicknavmenu) {
        if (mysurf.SettingsXmin < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
          model.screenrecordlength -= model.screenrecordlengthu;
          if (model.screenrecordlength < 0)
            model.screenrecordlength = 0;
          needanotherupdate = true;
          clickquicknavmenu = false;
        }
      }
      recti = new RectF(x1, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
      recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
      if ((model.isandroidtv || model.keypadnavactive) && mysurf.SettingsXmin < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
        RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
        surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
      }
      surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

      mabase += model.GenericCaseH + model.GenericInterSpace;
      if (mabase > mysurf.myheight)
        return;

      ecritoption = "+/- " + model.screenrecordlengthu + "s";
      y1 = mabase;
      y2 = mabase + model.GenericCaseH;
      taillex = model.Menu1TextePaint.measureText(ecritoption);
      x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
      x2 = mysurf.ScreenWidth - model.GenericInterSpace;
      if (x1 > mysurf.SettingsXmin) {
        x1 = mysurf.SettingsXmin;
      }
      ecritx = x2 - model.GenericInterSpace;
      ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
      if (clickquicknavmenu) {
        if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
          if (model.screenrecordlengthu == 1)
            model.screenrecordlengthu = 10;
          else if (model.screenrecordlengthu == 10)
            model.screenrecordlengthu = 60;
          else
            model.screenrecordlengthu = 1;
          needanotherupdate = true;
          clickquicknavmenu = false;
        }
      }
      recti = new RectF(x1, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
      recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
      if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
        RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
        surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
      }
      surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

      mabase += model.GenericCaseH + model.GenericInterSpace;
      if (mabase > mysurf.myheight)
        return;

      ecritoption = "screenrecord";
      y1 = mabase;
      y2 = mabase + model.GenericCaseH;
      taillex = model.Menu1TextePaint.measureText(ecritoption);
      x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
      x2 = mysurf.ScreenWidth - model.GenericInterSpace;
      if (x1 > mysurf.SettingsXmin) {
        x1 = mysurf.SettingsXmin;
      }
      ecritx = x2 - model.GenericInterSpace;
      ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
      if (clickquicknavmenu) {
        if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
          model.message("screenrecord in 10s...");
          shellexecute(false);
          needanotherupdate = true;
          clickquicknavmenu = false;
        }
      }
      recti = new RectF(x1, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
      recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
      if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
        RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
        surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
      }
      surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

      mabase += model.GenericCaseH + model.GenericInterSpace;
      if (mabase > mysurf.myheight)
        return;

    }

  }

  /*private void menuatvzoombtn(Canvas surfacecanvas, int foldercount) {
    if (model.atvzoommode == myViewModel.AtvZoomButtonVolFfwd) {
      ecritoption = "vol ffwd zoom";
    } else if (model.atvzoommode == myViewModel.AtvZoomButtonFfwd) {
      ecritoption = "ffwd zoom";
    } else if (model.atvzoommode == myViewModel.AtvZoomButtonVol) {
      ecritoption = "vol zoom";
    }
    y1 = mabase;
    y2 = mabase + model.GenericCaseH;
    taillex = model.Menu1TextePaint.measureText(ecritoption);
    x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
    x2 = mysurf.ScreenWidth - model.GenericInterSpace;
    if (x1 > mysurf.SettingsXmin) {
      x1 = mysurf.SettingsXmin;
    }
    ecritx = x2 - model.GenericInterSpace;
    ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
    if (clickquicknavmenu) {
      if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
        if (model.atvzoommode == myViewModel.AtvZoomButtonVolFfwd) {
          model.atvzoommode = myViewModel.AtvZoomButtonFfwd;
          model.message("zoom button :\nfast forward");
        } else if (model.atvzoommode == myViewModel.AtvZoomButtonFfwd) {
          model.atvzoommode = myViewModel.AtvZoomButtonVol;
          model.message("zoom button :\nvolume up");
        } else if (model.atvzoommode == myViewModel.AtvZoomButtonVol) {
          model.atvzoommode = myViewModel.AtvZoomButtonVolFfwd;
          model.message("zoom button :\nfast forward\nand volume up");
        }
        model.preferences.edit().putInt("atvzoombtn", model.atvzoommode).apply();
        needanotherupdate = true;
        clickquicknavmenu = false;
      }
    }
    recti = new RectF(x1, y1, x2, y2);
    surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu1BgPainta);
    recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
    surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu1BgPainto);
    if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
      RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
      surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
    }
    surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

    mabase += model.GenericCaseH + model.GenericInterSpace;

  }*/

  private void menuflagsecure(Canvas surfacecanvas, int foldercount) {
    if (model.flagsecure) {
      ecritoption = "secure";
    } else {
      ecritoption = "no secure";
    }
    y1 = mabase;
    y2 = mabase + model.GenericCaseH;
    taillex = model.Menu1TextePaint.measureText(ecritoption);
    x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
    x2 = mysurf.ScreenWidth - model.GenericInterSpace;
    if (x1 > mysurf.SettingsXmin) {
      x1 = mysurf.SettingsXmin;
    }
    ecritx = x2 - model.GenericInterSpace;
    ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
    if (clickquicknavmenu) {
      if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
        model.flagsecure = !model.flagsecure;
        if (model.flagsecure) {
          model.message("secure flag\nhides content from task switcher");
          Intent intent = new Intent();
          intent.setAction(Gallery.broadcastname);
          intent.putExtra("goal", "flagsecure");
          intent.putExtra("secure", model.flagsecure);
          LocalBroadcastManager.getInstance(model.activitycontext).sendBroadcast(intent);
        } else {
          model.message();
          Intent intent = new Intent();
          intent.setAction(Gallery.broadcastname);
          intent.putExtra("goal", "flagsecure");
          intent.putExtra("secure", model.flagsecure);
          LocalBroadcastManager.getInstance(model.activitycontext).sendBroadcast(intent);
        }
        model.preferences.edit().putBoolean("flagsecure", model.flagsecure).apply();
        needanotherupdate = true;
        clickquicknavmenu = false;
      }
    }
    recti = new RectF(x1, y1, x2, y2);
    surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu1BgPainta);
    recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
    surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu1BgPainto);
    if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
      RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
      surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
    }
    surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

    mabase += model.GenericCaseH + model.GenericInterSpace;

  }



  private void menuswcopy(Canvas surfacecanvas) {
    switch(model.videohardwaremode) {
      case Gallery.videohwaamodesw:
        ecritoption = "sw decode";
        break;
      case Gallery.videohwaamodehw:
        ecritoption = "hw copy to ram";
        break;
      case Gallery.videohwaamodehwp:
        ecritoption = "hw+";
        break;
      case Gallery.videohwaamodehwpp:
        ecritoption = "hw direct rendering";
        break;
      case Gallery.videohwaamodensw:
        ecritoption = "gpu-next sw decode";
        break;
      case Gallery.videohwaamodenhw:
        ecritoption = "gpu-next hw copy to ram";
        break;
      case Gallery.videohwaamodenhwp:
        ecritoption = "gpu-next hw+";
        break;
    }
    ecritoption = "mpv " + ecritoption;
    y1 = mabase;
    y2 = mabase + model.GenericCaseH;
    taillex = model.Menu1TextePaint.measureText(ecritoption);
    x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
    x2 = mysurf.ScreenWidth - model.GenericInterSpace;
    if (x1 > mysurf.SettingsXmin) {
      x1 = mysurf.SettingsXmin;
    }
    ecritx = x2 - model.GenericInterSpace;
    ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
    if (clickquicknavmenu) {
      if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
        model.videohardwaremode = (model.videohardwaremode + 1) % Gallery.videohwaamodecount;
        switch(model.videohardwaremode){
          case Gallery.videohwaamodensw:
          case Gallery.videohwaamodesw:
            model.message("software decoding\n" +
                    "always works");
            model.videoaaspectresize = false;
            break;
          case Gallery.videohwaamodenhw:
          case Gallery.videohwaamodehw:
            model.message("hardware copy to ram\n" +
                    "should perform better\n" +
                    "(mediacodec-copy)");
            model.videoaaspectresize = false;
            break;
          case Gallery.videohwaamodehwp:
          case Gallery.videohwaamodenhwp:
            model.message("hardware\n" +
                    "should perform even better\n" +
                    "(mediacodec)");
            model.videoaaspectresize = false;
            break;
          case Gallery.videohwaamodehwpp:
            model.message("hardware direct rendering\n" +
                    "default\n" +
                    "should play smoothly on all devices\n" +
                    "subtitles are rendered on an overlay\n" +
                    "(mediacodec-embed)\n" +
                    "Used with tweaks -> resize to video.");
            model.videoaaspectresize = true;
            break;
          default:
            model.message();
            model.videoaaspectresize = false;
            break;
        }
        model.preferences.edit().putInt("videohardwaremode", model.videohardwaremode).apply();
        model.preferences.edit().putBoolean("videoaaspectresize", model.videoaaspectresize).commit();
        needanotherupdate = true;
        clickquicknavmenu = false;
      }
    }
    recti = new RectF(x1, y1, x2, y2);
    surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu1BgPainta);
    recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
    surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu1BgPainto);
    if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
      RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
      surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
    }
    surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

    mabase += model.GenericCaseH + model.GenericInterSpace;
  }

  private void menusort(Canvas surfacecanvas, int foldercount) {
    if (model.sortmode == model.SortAZ) {
      ecritoption = "sort : a-z";
    } else if (model.sortmode == model.SortDate) {
      ecritoption = "sort : date"; // fit window each time for slideshow
    }
    y1 = mabase;
    y2 = mabase + model.GenericCaseH;
    taillex = model.Menu1TextePaint.measureText(ecritoption);
    x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
    x2 = mysurf.ScreenWidth - model.GenericInterSpace;
    if (x1 > mysurf.SettingsXmin) {
      x1 = mysurf.SettingsXmin;
    }
    ecritx = x2 - model.GenericInterSpace;
    ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
    if (clickquicknavmenu) {
      if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
        model.message("only works on apache servers\nuseless otherwise for now.");
        if (model.sortmode == model.SortAZ) {
          model.sortmode = model.SortDate;
        } else if (model.sortmode == model.SortDate) {
          model.sortmode = model.SortAZ;
        }
        needanotherupdate = true;
        clickquicknavmenu = false;
      }
    }
    recti = new RectF(x1, y1, x2, y2);
    surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu1BgPainta);
    recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
    surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu1BgPainto);
    if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
      RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
      surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
    }
    surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

    mabase += model.GenericCaseH + model.GenericInterSpace;

  }

  private void menucleanup(Canvas surfacecanvas, int foldercount) {
    ecritoption = "cleanup";
    y1 = mabase;
    y2 = mabase + model.GenericCaseH;
    taillex = model.Menu1TextePaint.measureText(ecritoption);
    x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
    x2 = mysurf.ScreenWidth - model.GenericInterSpace;
    if (x1 > mysurf.SettingsXmin) {
      x1 = mysurf.SettingsXmin;
    }
    ecritx = x2 - model.GenericInterSpace;
    ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
    if (clickquicknavmenu) {
      if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
        /*
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
          model.cookieManager.removeAllCookies(null);
          model.cookieManager.flush();
        }
        */
        try {
          model.commandethreadminiature.put(new String[]{"supercleanup"});
        } catch (InterruptedException e) {
          e.printStackTrace();
        }
        needanotherupdate = true;
        clickquicknavmenu = false;
      }
    }
    recti = new RectF(x1, y1, x2, y2);
    surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu1BgPainta);
    recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
    surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu1BgPainto);
    if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
      RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
      surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
    }
    surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

    mabase += model.GenericCaseH + model.GenericInterSpace;

  }

  private void menuhwaccel(Canvas surfacecanvas, int foldercount) {
    if (model.optionhardwareaccelerationb)
      ecritoption = "browser hw";
    else
      ecritoption = "browser sw";
    y1 = mabase;
    y2 = mabase + model.GenericCaseH;
    taillex = model.Menu1TextePaint.measureText(ecritoption);
    x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
    x2 = mysurf.ScreenWidth - model.GenericInterSpace;
    if (x1 > mysurf.SettingsXmin) {
      x1 = mysurf.SettingsXmin;
    }
    ecritx = x2 - model.GenericInterSpace;
    ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
    if (clickquicknavmenu) {
      if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
        if (!model.optionhardwareaccelerationb)
          model.message("Browser canvas is hardware accelerated" +
                  "\ndefault" +
                  "\nPlease restart app to apply.");
        else
          model.message("Browser canvas is not hardware accelerated" +
                  "\nuse if browser hw lags" +
                  "\nPlease restart app to apply.");
        model.preferences.edit().putBoolean("optionhardwareaccelerationbrowser", !model.optionhardwareaccelerationb).commit();
        /*try {
          Thread.sleep(10000);
        } catch (InterruptedException e) {
        }
        model.optionhardwareaccelerationb = !model.optionhardwareaccelerationb;*/
        needanotherupdate = true;
        clickquicknavmenu = false;
      }
    }
    recti = new RectF(x1, y1, x2, y2);
    surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu1BgPainta);
    recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
    surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu1BgPainto);
    if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
      RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
      surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
    }
    surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

    mabase += model.GenericCaseH + model.GenericInterSpace;
    if (mabase > mysurf.myheight)
      return;

    /*if (model.optionhardwareaccelerationv)
      ecritoption = "video hw";
    else
      ecritoption = "video sw";
    y1 = mabase;
    y2 = mabase + model.GenericCaseH;
    taillex = model.Menu1TextePaint.measureText(ecritoption);
    x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
    x2 = mysurf.ScreenWidth - model.GenericInterSpace;
    if (x1 > mysurf.SettingsXmin) {
      x1 = mysurf.SettingsXmin;
    }
    ecritx = x2 - model.GenericInterSpace;
    ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
    if (clickquicknavmenu) {
      if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
        model.optionhardwareaccelerationv = !model.optionhardwareaccelerationv;
        model.preferences.edit().putBoolean("optionhardwareaccelerationv", model.optionhardwareaccelerationv).commit();
        if (model.optionhardwareaccelerationv)
          model.message("Video canvas is hardware accelerated" +
                  "\nnormally not needed" +
                  "\nPlease refresh app to apply.");
        else
          model.message("Video canvas is not hardware accelerated" +
                  "\ndefault" +
                  "\nPlease refresh app to apply.");
        needanotherupdate = true;
        clickquicknavmenu = false;
      }
    }
    recti = new RectF(x1, y1, x2, y2);
    surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu1BgPainta);
    recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
    surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu1BgPainto);
    if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
      RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
      surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
    }
    surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

    mabase += model.GenericCaseH + model.GenericInterSpace;
    if (mabase > mysurf.myheight)
      return;*/

  }

  private void menuinfo(Canvas surfacecanvas, int foldercount) {
    ecritoption = "media info";
    y1 = mabase;
    y2 = mabase + model.GenericCaseH;
    taillex = model.Menu1TextePaint.measureText(ecritoption);
    x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
    x2 = mysurf.ScreenWidth - model.GenericInterSpace;
    if (x1 > mysurf.SettingsXmin) {
      x1 = mysurf.SettingsXmin;
    }
    ecritx = x2 - model.GenericInterSpace;
    ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
    if (clickquicknavmenu) {
      if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
        try {
          model.commandethreadbrowser.put(new String[]{String.valueOf(currid), "displayinfo"});
        } catch (InterruptedException e) {
        }
        needanotherupdate = true;
        clickquicknavmenu = false;
      }
    }
    recti = new RectF(x1, y1, x2, y2);
    surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu1BgPainta);
    recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
    surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu1BgPainto);
    if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
      RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
      surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
    }
    surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

    mabase += model.GenericCaseH + model.GenericInterSpace;

  }

  private void menuautostartboot(Canvas surfacecanvas, int foldercount) {
    if (model.optionautostartboot)
      ecritoption = "autostart";
    else
      ecritoption = "no autostart";
    y1 = mabase;
    y2 = mabase + model.GenericCaseH;
    taillex = model.Menu1TextePaint.measureText(ecritoption);
    x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
    x2 = mysurf.ScreenWidth - model.GenericInterSpace;
    if (x1 > mysurf.SettingsXmin) {
      x1 = mysurf.SettingsXmin;
    }
    ecritx = x2 - model.GenericInterSpace;
    ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
    if (clickquicknavmenu) {
      if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
        model.optionautostartboot = !model.optionautostartboot;
        if (model.optionautostartboot)
          model.message("App will autostart at boot.");
        else
          model.message("App will not autostart at boot.");
        model.preferences.edit().putBoolean("optionautostartboot", model.optionautostartboot).commit();
        needanotherupdate = true;
        clickquicknavmenu = false;
      }
    }
    recti = new RectF(x1, y1, x2, y2);
    surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu1BgPainta);
    recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
    surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu1BgPainto);
    if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
      RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
      surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
    }
    surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

    mabase += model.GenericCaseH + model.GenericInterSpace;

  }

  private void menumonitor(Canvas surfacecanvas, int foldercount) {
    ecritoption = "monitor";
    y1 = mabase;
    y2 = mabase + model.GenericCaseH;
    taillex = model.Menu1TextePaint.measureText(ecritoption);
    x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
    x2 = mysurf.ScreenWidth - model.GenericInterSpace;
    if (x1 > mysurf.SettingsXmin) {
      x1 = mysurf.SettingsXmin;
    }
    ecritx = x2 - model.GenericInterSpace;
    ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
    if (clickquicknavmenu) {
      if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
        Intent intent = new Intent();
        intent.setAction(Gallery.broadcastname);
        intent.putExtra("goal", "lockscreenwidgetconf");
        intent.putExtra("id", currid);
        intent.putExtra("type", Gallery.screenmonitorwidget);
        LocalBroadcastManager.getInstance(model.activitycontext).sendBroadcast(intent);
        needanotherupdate = true;
        clickquicknavmenu = false;
      }
    }
    recti = new RectF(x1, y1, x2, y2);
    surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu1BgPainta);
    recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
    surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu1BgPainto);
    if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
      RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
      surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
    }
    surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

    mabase += model.GenericCaseH + model.GenericInterSpace;
    if (mabase > mysurf.myheight)
      return;


    y1 = mabase;
    y2 = mabase + model.GenericCaseH;
    ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;

    ecritoption = "stop";
    taillex = model.Menu1TextePaint.measureText(ecritoption);
    x1 = (mysurf.ScreenWidth - model.GenericInterSpace + mysurf.SettingsXmin) * 0.5f + model.GenericInterSpace * 0.5f;
    x2 = mysurf.ScreenWidth - model.GenericInterSpace;
    ecritx = x2;
    if (clickquicknavmenu) {
      if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
        try {
          model.commandethreaddatabase.put(new String[]{"scanapps", String.valueOf(currid), "temp.sh", "0", "null"});
        } catch (InterruptedException e) {
          e.printStackTrace();
        }
        needanotherupdate = true;
        clickquicknavmenu = false;
      }
    }
    recti = new RectF(x1, y1, x2, y2);
    //surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
    //recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
    surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu1BgPainto);
    if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
      RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
      surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
    }
    surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

    ecritoption = "monitor start";
    taillex = model.Menu1TextePaint.measureText(ecritoption);
    x2 = x1 - model.GenericInterSpace;
    x1 = x2 - model.GenericInterSpace * 2.0f - taillex;
      if (x1 > mysurf.SettingsXmin)
        x1 = mysurf.SettingsXmin;
    ecritx = x2 - model.GenericInterSpace;
    if (clickquicknavmenu) {
      if (mysurf.SettingsXmin < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
        try {
          model.commandethreaddatabase.put(new String[]{"scanapps", String.valueOf(currid), "temp.sh", "1 " + Gallery.monitordeltatime, "null"});
        } catch (InterruptedException e) {
          e.printStackTrace();
        }
        needanotherupdate = true;
        clickquicknavmenu = false;
      }
    }
    recti = new RectF(x1, y1, x2, y2);
    surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu1BgPainta);
    recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
    surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu1BgPainto);
    if ((model.isandroidtv || model.keypadnavactive) && mysurf.SettingsXmin < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
      RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
      surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
    }
    surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

    mabase += model.GenericCaseH + model.GenericInterSpace;
    if (mabase > mysurf.myheight)
      return;

  }

  private void menulog(Canvas surfacecanvas, int foldercount) {
    if (Gallery.debugi == 2)
      ecritoption = "ultra verbose";
    else if (Gallery.debugi == 1)
      ecritoption = "log debug";
    else
      ecritoption = "no log";
    y1 = mabase;
    y2 = mabase + model.GenericCaseH;
    taillex = model.Menu1TextePaint.measureText(ecritoption);
    x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
    x2 = mysurf.ScreenWidth - model.GenericInterSpace;
    if (x1 > mysurf.SettingsXmin) {
      x1 = mysurf.SettingsXmin;
    }
    ecritx = x2 - model.GenericInterSpace;
    ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
    if (clickquicknavmenu) {
      if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
        Gallery.debugi = (Gallery.debugi + 1) % 3;
        model.preferences.edit().putInt("debugi", Gallery.debugi).commit();
        needanotherupdate = true;
        clickquicknavmenu = false;
      }
    }
    recti = new RectF(x1, y1, x2, y2);
    surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu1BgPainta);
    recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
    surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu1BgPainto);
    if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
      RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
      surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
    }
    surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

    mabase += model.GenericCaseH + model.GenericInterSpace;

  }

  private void menukill(Canvas surfacecanvas, int foldercount) {
    if (model.killonexit)
      ecritoption = "kill on exit";
    else
      ecritoption = "no kill";
    y1 = mabase;
    y2 = mabase + model.GenericCaseH;
    taillex = model.Menu1TextePaint.measureText(ecritoption);
    x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
    x2 = mysurf.ScreenWidth - model.GenericInterSpace;
    if (x1 > mysurf.SettingsXmin) {
      x1 = mysurf.SettingsXmin;
    }
    ecritx = x2 - model.GenericInterSpace;
    ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
    if (clickquicknavmenu) {
      if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
        model.killonexit = !model.killonexit;
        needanotherupdate = true;
        clickquicknavmenu = false;
      }
    }
    recti = new RectF(x1, y1, x2, y2);
    surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu1BgPainta);
    recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
    surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu1BgPainto);
    if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
      RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
      surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
    }
    surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

    mabase += model.GenericCaseH + model.GenericInterSpace;

  }

  private void menudecodelibextractor(Canvas surfacecanvas, int foldercount) {
    if (model.optiondecodelibextractor)
      ecritoption = "libextractor decoder";
    else
      ecritoption = "android decoder";
    y1 = mabase;
    y2 = mabase + model.GenericCaseH;
    taillex = model.Menu1TextePaint.measureText(ecritoption);
    x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
    x2 = mysurf.ScreenWidth - model.GenericInterSpace;
    if (x1 > mysurf.SettingsXmin) {
      x1 = mysurf.SettingsXmin;
    }
    ecritx = x2 - model.GenericInterSpace;
    ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
    if (clickquicknavmenu) {
      if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
        model.optiondecodelibextractor = !model.optiondecodelibextractor;
        model.preferences.edit().putBoolean("optiondecodelibextractor", model.optiondecodelibextractor).commit();
        needanotherupdate = true;
        clickquicknavmenu = false;
      }
    }
    recti = new RectF(x1, y1, x2, y2);
    surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu1BgPainta);
    recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
    surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu1BgPainto);
    if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
      RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
      surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
    }
    surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

    mabase += model.GenericCaseH + model.GenericInterSpace;

  }

  private void menuhidesurface(Canvas surfacecanvas, int foldercount) {
    if (!model.optionhidesurface)
      ecritoption = "overlay default";
    else
      ecritoption = "overlay hidden";
    y1 = mabase;
    y2 = mabase + model.GenericCaseH;
    taillex = model.Menu1TextePaint.measureText(ecritoption);
    x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
    x2 = mysurf.ScreenWidth - model.GenericInterSpace;
    if (x1 > mysurf.SettingsXmin) {
      x1 = mysurf.SettingsXmin;
    }
    ecritx = x2 - model.GenericInterSpace;
    ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
    if (clickquicknavmenu) {
      if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
        model.optionhidesurface = !model.optionhidesurface;
        if (model.optionhidesurface)
          model.message("hide orverlay");
        else
          model.message("default");
        /*Intent intent = new Intent();
        intent.setAction(Gallery.broadcastname);
        intent.putExtra("goal", "showoverlay");
        LocalBroadcastManager.getInstance(model.activitycontext).sendBroadcast(intent);*/
        model.preferences.edit().putBoolean("optionhidesurface", model.optionhidesurface).commit();
        needanotherupdate = true;
        clickquicknavmenu = false;
      }
    }
    recti = new RectF(x1, y1, x2, y2);
    surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu1BgPainta);
    recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
    surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu1BgPainto);
    if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
      RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
      surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
    }
    surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

    mabase += model.GenericCaseH + model.GenericInterSpace;

  }

  private void menushowmenuprevnext(Canvas surfacecanvas, int foldercount) {
    if (model.showmenuprevnextbutton)
      ecritoption = "buttons";
    else
      ecritoption = "no button";
    y1 = mabase;
    y2 = mabase + model.GenericCaseH;
    taillex = model.Menu1TextePaint.measureText(ecritoption);
    x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
    x2 = mysurf.ScreenWidth - model.GenericInterSpace;
    if (x1 > mysurf.SettingsXmin) {
      x1 = mysurf.SettingsXmin;
    }
    ecritx = x2 - model.GenericInterSpace;
    ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
    if (clickquicknavmenu) {
      if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
        model.showmenuprevnextbutton = !model.showmenuprevnextbutton;
        if (model.showmenuprevnextbutton)
          model.message("corner buttons\nmenu previous next\nshown");
        else
          model.message("no corner button\nmenu previous next");
        model.preferences.edit().putBoolean("showmenuprevnextbutton", model.showmenuprevnextbutton).commit();
        needanotherupdate = true;
        clickquicknavmenu = false;
      }
    }
    recti = new RectF(x1, y1, x2, y2);
    surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu1BgPainta);
    recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
    surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu1BgPainto);
    if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
      RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
      surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
    }
    surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

    mabase += model.GenericCaseH + model.GenericInterSpace;

  }

  private void menuhowto(Canvas surfacecanvas, int foldercount) {
    ecritoption = "how to";
    y1 = mabase;
    y2 = mabase + model.GenericCaseH;
    taillex = model.Menu1TextePaint.measureText(ecritoption);
    x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
    x2 = mysurf.ScreenWidth - model.GenericInterSpace;
    if (x1 > mysurf.SettingsXmin) {
      x1 = mysurf.SettingsXmin;
    }
    ecritx = x2 - model.GenericInterSpace;
    ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
    if (clickquicknavmenu) {
      if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
        if (model.isandroidtv)
          model.message(Gallery.howtomessagetv);
        else if (model.iswatch)
          model.message(Gallery.howtomessagewatch);
        else
          model.message(Gallery.howtomessage);
        needanotherupdate = true;
        clickquicknavmenu = false;
      }
    }
    recti = new RectF(x1, y1, x2, y2);
    surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu1BgPainta);
    recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
    surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu1BgPainto);
    if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
      RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
      surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
    }
    surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

    mabase += model.GenericCaseH + model.GenericInterSpace;

  }

  private void menureadme(Canvas surfacecanvas, int foldercount) {
    ecritoption = "readme";
    y1 = mabase;
    y2 = mabase + model.GenericCaseH;
    taillex = model.Menu1TextePaint.measureText(ecritoption);
    x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
    x2 = mysurf.ScreenWidth - model.GenericInterSpace;
    if (x1 > mysurf.SettingsXmin) {
      x1 = mysurf.SettingsXmin;
    }
    ecritx = x2 - model.GenericInterSpace;
    ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
    if (clickquicknavmenu) {
      if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
        Intent intent = new Intent();
        intent.setAction(Gallery.broadcastname);
        intent.putExtra("goal", "loadwebsite");
        intent.putExtra("address", "https://codeberg.org/LaDaube/PhotoChiotte/src/branch/master/README.md");
        LocalBroadcastManager.getInstance(model.activitycontext).sendBroadcast(intent);
        needanotherupdate = true;
        clickquicknavmenu = false;
      }
    }
    recti = new RectF(x1, y1, x2, y2);
    surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu1BgPainta);
    recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
    surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu1BgPainto);
    if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
      RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
      surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
    }
    surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

    mabase += model.GenericCaseH + model.GenericInterSpace;

  }

  private String thereisanupdate = null;
  private String releasenamecode = BuildConfig.VERSION_NAME + " (" + BuildConfig.VERSION_CODE + ")";
  private void menucheckupdates(Canvas surfacecanvas, int foldercount) {
    if (thereisanupdate == null) {
      ecritoption = "check updates";
    } else {
      ecritoption = "update " + releasenamecode + " available";
    }
    y1 = mabase;
    y2 = mabase + model.GenericCaseH;
    taillex = model.Menu1TextePaint.measureText(ecritoption);
    x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
    x2 = mysurf.ScreenWidth - model.GenericInterSpace;
    if (x1 > mysurf.SettingsXmin) {
      x1 = mysurf.SettingsXmin;
    }
    ecritx = x2 - model.GenericInterSpace;
    ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
    if (clickquicknavmenu) {
      if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
        String releasename = "";
        int releasecode = -1;
        String gradlename = "";
        int gradlecode = -1;
        //       <a href="https://f-droid.org/repo/la.daube.photochiotte_21.apk">
        // href="https://codeberg.org/LaDaube/PhotoChiotte/releases/download/v1.47/la.daube.photochiotte_48.apk"
        //String html = InternetSession.gethtml("https://f-droid.org/packages/la.daube.photochiotte/", "https://f-droid.org/packages/la.daube.photochiotte/");
        String html = InternetSession.gethtml(model, "https://codeberg.org/LaDaube/PhotoChiotte/releases/", "https://codeberg.org/LaDaube/PhotoChiotte/releases/");
        if (html == null) {
          model.message("Internet error.\nCould not communicate with Codeberg.org.");
        } else {
          Matcher found = Pattern.compile("href=.https://codeberg.org/LaDaube/PhotoChiotte/releases/download/([^/]+)/la.daube.photochiotte_(\\d+).apk", Pattern.CASE_INSENSITIVE).matcher(html);
          if (found.find()) {
            releasename = found.group(1);
            releasecode = Integer.parseInt(found.group(2));
          }
          //         versionName "1.21"          versionCode 22
          html = InternetSession.gethtml(model, "https://codeberg.org/LaDaube/PhotoChiotte/raw/branch/master/app/build.gradle", "https://codeberg.org/LaDaube/PhotoChiotte/src/branch/master/app/build.gradle");
          if (html == null) {
            model.message("Internet error.\nCould not communicate with Codeberg.org.");
          } else {
            found = Pattern.compile("versionName .([^\"]+)", Pattern.CASE_INSENSITIVE).matcher(html);
            if (found.find())
              gradlename = found.group(1);
            found = Pattern.compile("versionCode (\\d+)", Pattern.CASE_INSENSITIVE).matcher(html);
            if (found.find())
              gradlecode = Integer.parseInt(found.group(1));
            // 1.21B 22
            int yourversion = BuildConfig.VERSION_CODE;
            if (yourversion < releasecode) {
              releasenamecode = releasename + " (" + releasecode + ")";
              thereisanupdate = "https://codeberg.org/LaDaube/PhotoChiotte/releases/download/" + releasename + "/la.daube.photochiotte_" + releasecode + ".apk";
              model.message("A new version " + releasecode + ">" + yourversion + " is available in F-Droid."
                      + "\nPlease open F-Droid to update it"
                      + "\nor download it by clicking again.");
            } else if (releasecode < gradlecode) {
              model.message("A new version is available on Codeberg.org"
                      + "\nbut not yet published by F-Droid."
                      + "\nIt will be available within the week.\n" + yourversion + "<=" + releasecode + "(F-Droid)<" + gradlecode + "(Codeberg)"
              );
              thereisanupdate = null;
            } else {
              model.message("This is the latest version.\n" + yourversion + "=" + releasecode + "(F-Droid)=" + gradlecode + "(Codeberg)\nv" + gradlename);
              thereisanupdate = null;
            }
            llog.d(TAG, "release " + releasename + " " + releasecode + " / gradle " + gradlename + " " + gradlecode + " : " + "https://codeberg.org/LaDaube/PhotoChiotte/releases/download/" + thereisanupdate);
            // TODO : DUMMY remove
            //thereisanupdate = fdroidlatest + "";
          }
        }
        needanotherupdate = true;
        clickquicknavmenu = false;
      }
    }
    recti = new RectF(x1, y1, x2, y2);
    surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu1BgPainta);
    recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
    surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu1BgPainto);
    if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
      RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
      surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
    }
    surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

    mabase += model.GenericCaseH + model.GenericInterSpace;
    if (mabase > mysurf.myheight)
      return;

    if (thereisanupdate != null) {
      ecritoption = "download update " + releasenamecode;
      y1 = mabase;
      y2 = mabase + model.GenericCaseH;
      taillex = model.Menu1TextePaint.measureText(ecritoption);
      x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
      x2 = mysurf.ScreenWidth - model.GenericInterSpace;
      if (x1 > mysurf.SettingsXmin) {
        x1 = mysurf.SettingsXmin;
      }
      ecritx = x2 - model.GenericInterSpace;
      ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
      if (clickquicknavmenu) {
        if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
          // String downloadurl = "https://codeberg.org/LaDaube/PhotoChiotte/releases/download/v1.47/la.daube.photochiotte_" + thereisanupdate + ".apk";
          /*
          llog.d(TAG, "download " + downloadurl);
          Intent intent = new Intent(Intent.ACTION_VIEW);
          intent.setDataAndType(Uri.parse(downloadurl), "text/plain");
          intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
          intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
          model.activitycontext.startActivity(Intent.createChooser(intent, "dialogTitle"));
          */

          //if (!model.iswatch) {
          Intent intent = new Intent();
          intent.setAction(Gallery.broadcastname);
          intent.putExtra("goal", "downloadfile");
          intent.putExtra("address", thereisanupdate);
          LocalBroadcastManager.getInstance(model.activitycontext).sendBroadcast(intent);

        /*} else {
          String downloadtarget = model.dossierminiature + "/update.apk";
          llog.d(TAG, "download " + downloadurl + " to " + downloadtarget);
          File outputFile = new File(downloadtarget);
          if (outputFile.exists()) {
            outputFile.delete();
          }
          try {
            llog.d(TAG, "download " + downloadurl + " to " + downloadtarget);
            URL urlurl = new URL(downloadurl);
            HttpURLConnection c = (HttpURLConnection) urlurl.openConnection();
            c.setRequestProperty("User-Agent", "Mozilla/5.0");
            c.setConnectTimeout(6000);
            c.connect();
            int contentlength = -1;
            for (Map.Entry<String, List<String>> entries : c.getHeaderFields().entrySet()) {
              String mykey = entries.getKey();
              if (mykey != null) {
                String premierevalue = null;
                String values = "";
                for (String value : entries.getValue()) {
                  if (premierevalue == null) {
                    premierevalue = value;
                  }
                  values += value + " ";
                }
                llog.d(TAG, mykey + " = " + values);
                if (mykey.equals("Content-Length")) {
                  llog.d(TAG, "content-length " + premierevalue);
                  if (contentlength == -1)
                    contentlength = Integer.parseInt(premierevalue);
                } else if (mykey.equals("Content-Type")) {
                  if (!premierevalue.equals("application/vnd.android.package-archive")) {
                    llog.d(TAG, "error content-type");
                    contentlength = 0;
                  }
                }
              }
            }
            if (contentlength > 0) {
              FileOutputStream fos = new FileOutputStream(outputFile);
              InputStream is = c.getInputStream();
              byte[] buffer = new byte[8192];
              int fullsize = 0;
              int len1 = 0;
              while ((len1 = is.read(buffer)) != -1) {
                //Log.d(TAG, len1+">>>"+new String(buffer, Charset.forName("UTF-8")));
                // Log.d(TAG, ">>> writing "+len1+" bytes");
                fullsize += len1;
                fos.write(buffer, 0, len1);
              }
              fos.close();
              is.close();
              llog.d(TAG, " -> " + fullsize);
              Intent intent = new Intent(Intent.ACTION_VIEW);
              intent.setDataAndType(Uri.fromFile(outputFile), "application/vnd.android.package-archive");
              intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
              intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
              // without this flag android returned a intent error!
              model.activitycontext.startActivity(Intent.createChooser(intent, "dialogTitle"));
              //getActivity().setResult(Activity.RESULT_OK);
              //context.startActivity(intent);
              //getActivity().finish();
            }
          } catch (FileNotFoundException | MalformedURLException e) {
            e.printStackTrace();
          } catch (IOException e) {
            e.printStackTrace();
          }
        }*/

          needanotherupdate = true;
          clickquicknavmenu = false;
        }
      }
      recti = new RectF(x1, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu1BgPainta);
      recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu1BgPainto);
      if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
        RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
        surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
      }
      surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

      mabase += model.GenericCaseH + model.GenericInterSpace;
      if (mabase > mysurf.myheight)
        return;
    }
  }

  private void menurootupdate(Canvas surfacecanvas, int foldercount) {
    String downloadurl;
    if (thereisanupdate != null) {
      downloadurl = thereisanupdate;
    } else {
      downloadurl = model.runcmddownloadurl;
    }

    y1 = mabase;
    y2 = mabase + model.GenericCaseH;
    ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;

    ecritoption = "...";
    taillex = model.Menu1TextePaint.measureText(ecritoption);
    x1 = (mysurf.ScreenWidth - model.GenericInterSpace + mysurf.SettingsXmin) * 0.5f + model.GenericInterSpace * 0.5f;
    x2 = mysurf.ScreenWidth - model.GenericInterSpace;
    ecritx = x2;
    if (clickquicknavmenu) {
      if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
        Intent intent = new Intent();
        intent.setAction(Gallery.broadcastname);
        intent.putExtra("goal", "rootinstall");
        intent.putExtra("id", currid);
        intent.putExtra("default", downloadurl);
        LocalBroadcastManager.getInstance(model.activitycontext).sendBroadcast(intent);
        needanotherupdate = true;
        clickquicknavmenu = false;
      }
    }
    recti = new RectF(x1, y1, x2, y2);
    //surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu1BgPainta);
    //recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
    surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu1BgPainto);
    if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
      RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
      surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
    }
    surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

    if (thereisanupdate != null) {
      ecritoption = "root update " + releasenamecode;
    } else {
      ecritoption = "root install";
    }
    taillex = model.Menu1TextePaint.measureText(ecritoption);
    x2 = x1 - model.GenericInterSpace;
    x1 = x2 - model.GenericInterSpace * 2.0f - taillex;
    if (x1 > mysurf.SettingsXmin)
      x1 = mysurf.SettingsXmin;
    ecritx = x2 - model.GenericInterSpace;
    if (clickquicknavmenu) {
      if (mysurf.SettingsXmin < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
        model.message("su && curl && pm install " + downloadurl);
        String command = downloadurl;
        try {
          model.commandethreaddatabase.put(new String[]{"scanapps", String.valueOf(currid), "install.sh", command, "null"});
        } catch (InterruptedException e) {
          e.printStackTrace();
        }
        needanotherupdate = true;
        clickquicknavmenu = false;
      }
    }
    recti = new RectF(x1, y1, x2, y2);
    surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu1BgPainta);
    recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
    surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu1BgPainto);
    if ((model.isandroidtv || model.keypadnavactive) && mysurf.SettingsXmin < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
      RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
      surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
    }
    surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

    mabase += model.GenericCaseH + model.GenericInterSpace;
    if (mabase > mysurf.myheight)
      return;

  }

  private void menufdroid(Canvas surfacecanvas, int foldercount) {
    ecritoption = "view in fdroid";
    y1 = mabase;
    y2 = mabase + model.GenericCaseH;
    taillex = model.Menu1TextePaint.measureText(ecritoption);
    x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
    x2 = mysurf.ScreenWidth - model.GenericInterSpace;
    if (x1 > mysurf.SettingsXmin) {
      x1 = mysurf.SettingsXmin;
    }
    ecritx = x2 - model.GenericInterSpace;
    ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
    if (clickquicknavmenu) {
      if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
        Intent intent = new Intent();
        intent.setAction(Gallery.broadcastname);
        intent.putExtra("goal", "loadwebsite");
        intent.putExtra("address", "https://f-droid.org/packages/la.daube.photochiotte/");
        LocalBroadcastManager.getInstance(model.activitycontext).sendBroadcast(intent);
        needanotherupdate = true;
        clickquicknavmenu = false;
      }
    }
    recti = new RectF(x1, y1, x2, y2);
    surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu1BgPainta);
    recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
    surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu1BgPainto);
    if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
      RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
      surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
    }
    surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

    mabase += model.GenericCaseH + model.GenericInterSpace;

  }

  private void menudiscover(Canvas surfacecanvas, int foldercount) {
    if (!model.optiondiscoverserviceactive)
      ecritoption = "discover off";
    else {
      ecritoption = "discover on";
      if (mysurf.optionshowdiscover) {
        if (Gallery.backgroundService != null) {
          if (Gallery.backgroundService.threadDiscover != null) {
            ecritoption = backgroundService.torpubkey;
            ecritoption += " " + backgroundService.STATUS_CLIENTSL + "nodes/" + backgroundService.STATUS_RANGESL;
            ecritoption += " " + backgroundService.STATUS_FIND_OUR_POSITION_SUCCESS + "sit|" + backgroundService.STATUS_FIND_OUR_POSITION_FAILED;
            ecritoption += " " + backgroundService.STATUS_HASHESL + "files";
            ecritoption += " " + backgroundService.STATUS_ANNOUNCE_PEER_SUCCESS + "publish|" + backgroundService.STATUS_ANNOUNCE_PEER_FAILED;
            ecritoption += " " + backgroundService.STATUS_GET_PEERS_SUCCESS + "peers|" + backgroundService.STATUS_GET_PEERS_FAILED;
            ecritoption += " " + backgroundService.STATUS_SEARCH_KEYWORDS_FOUND + "dl";
            ecritoption += " " + backgroundService.STATUS_DOWNLOAD_SUCCESS + "dl|" + backgroundService.STATUS_DOWNLOAD_FAILED;
          }
        }
      }
    }
    y1 = mabase;
    y2 = mabase + model.GenericCaseH;
    taillex = model.Menu1TextePaint.measureText(ecritoption);
    x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
    x2 = mysurf.ScreenWidth - model.GenericInterSpace;
    if (x1 > mysurf.SettingsXmin) {
      x1 = mysurf.SettingsXmin;
    }
    ecritx = x2 - model.GenericInterSpace;
    ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
    if (clickquicknavmenu) {
      if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
        mysurf.optionshowdiscover = !mysurf.optionshowdiscover;
        if (mysurf.optionshowdiscover)
          model.message("discover service\nwill be released in v2.0");
        needanotherupdate = true;
        clickquicknavmenu = false;
      }
    }
    recti = new RectF(x1, y1, x2, y2);
    surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu1BgPainta);
    recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
    surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu1BgPainto);
    if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
      RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
      surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
    }
    surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

    mabase += model.GenericCaseH + model.GenericInterSpace;
    if (mabase > mysurf.myheight)
      return;

    if (mysurf.optionshowdiscover) {

      if (model.optiondiscoverserviceactive)
        ecritoption = "stop";
      else
        ecritoption = "start";
      y1 = mabase;
      y2 = mabase + model.GenericCaseH;
      taillex = model.Menu1TextePaint.measureText(ecritoption);
      x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
      x2 = mysurf.ScreenWidth - model.GenericInterSpace;
      if (x1 > mysurf.SettingsXmin) {
        x1 = mysurf.SettingsXmin;
      }
      ecritx = x2 - model.GenericInterSpace;
      ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
      if (clickquicknavmenu) {
        if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
          model.optiondiscoverserviceactive = !model.optiondiscoverserviceactive;
          model.preferences.edit().putBoolean("ThreadDiscoverActive", model.optiondiscoverserviceactive).apply();
          if (!model.optiondiscoverserviceactive) {
            if (Gallery.isMyServiceRunning(backgroundService.class, model.activitycontext)) {
              model.message("discover service stopped\nwill be released in v2.0");
              Intent intent = new Intent(backgroundService.REQUEST + backgroundService.REQUEST_STOPTOR);
              intent.setClass(model.activitycontext, backgroundService.class);
              if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
                model.activitycontext.startForegroundService(intent);
              else
                model.activitycontext.startService(intent);
            }
          } else {
            if (!Gallery.isMyServiceRunning(backgroundService.class, model.activitycontext)) {
              model.message("discover service started\nwill be released in v2.0");
              Intent intent = new Intent(model.activitycontext, backgroundService.class);
              if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
                model.activitycontext.startForegroundService(intent);
              else
                model.activitycontext.startService(intent);
            } else {
              model.message("discover service restarted\nwill be released in v2.0");
              Intent intent = new Intent(backgroundService.REQUEST + backgroundService.REQUEST_STARTTOR);
              intent.setClass(model.activitycontext, backgroundService.class);
              if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
                model.activitycontext.startForegroundService(intent);
              else
                model.activitycontext.startService(intent);
            }
          }
          recheckThreadDiscoverRunning = 10;
          needanotherupdate = true;
          clickquicknavmenu = false;
        }
      }
      recti = new RectF(x1, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
      recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
      surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
      if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
        RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
        surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
      }
      surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

      mabase += model.GenericCaseH + model.GenericInterSpace;
      if (mabase > mysurf.myheight)
        return;

      if (model.optiondiscoverserviceactive) {
        ecritoption = "search";
        y1 = mabase;
        y2 = mabase + model.GenericCaseH;
        taillex = model.Menu1TextePaint.measureText(ecritoption);
        x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
        x2 = mysurf.ScreenWidth - model.GenericInterSpace;
        if (x1 > mysurf.SettingsXmin) {
          x1 = mysurf.SettingsXmin;
        }
        ecritx = x2 - model.GenericInterSpace;
        ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
        if (clickquicknavmenu) {
          if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
            Intent intent = new Intent();
            intent.setAction(Gallery.broadcastname);
            intent.putExtra("goal", "bootstraptor");
            intent.putExtra("id", currid);
            intent.putExtra("lastsearch", "crap shit");
            LocalBroadcastManager.getInstance(model.activitycontext).sendBroadcast(intent);
            needanotherupdate = true;
            clickquicknavmenu = false;
          }
        }
        recti = new RectF(x1, y1, x2, y2);
        surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
        recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
        surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
        if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
          RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
          surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
        }
        surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

        mabase += model.GenericCaseH + model.GenericInterSpace;
        if (mabase > mysurf.myheight)
          return;

      } else {
        ecritoption = "bootstraps";
        y1 = mabase;
        y2 = mabase + model.GenericCaseH;
        taillex = model.Menu1TextePaint.measureText(ecritoption);
        x1 = mysurf.ScreenWidth - model.GenericInterSpace * 3.0f - taillex;
        x2 = mysurf.ScreenWidth - model.GenericInterSpace;
        if (x1 > mysurf.SettingsXmin) {
          x1 = mysurf.SettingsXmin;
        }
        ecritx = x2 - model.GenericInterSpace;
        ecrity = mabase + (model.GenericCaseH + model.GenericTextH) * 0.5f;
        if (clickquicknavmenu) {
          if (x1 < clicktohandlex && clicktohandlex < x2 && y1 < clicktohandley && clicktohandley < y2) {
            Intent intent = new Intent();
            intent.setAction(Gallery.broadcastname);
            intent.putExtra("goal", "bootstraptor");
            intent.putExtra("id", currid);
            intent.putExtra("default", "https://codeberg.org/LaDaube/PhotoChiotte/raw/branch/master/torbootstrap.txt");
            LocalBroadcastManager.getInstance(model.activitycontext).sendBroadcast(intent);
            needanotherupdate = true;
            clickquicknavmenu = false;
          }
        }
        recti = new RectF(x1, y1, x2, y2);
        surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainta);
        recti = new RectF(mysurf.SettingsXmin, y1, x2, y2);
        surfacecanvas.drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu2BgPainto);
        if ((model.isandroidtv || model.keypadnavactive) && x1 < mysurf.cursorx && mysurf.cursorx < x2 && y1 < mysurf.cursory && mysurf.cursory < y2) {
          RectF rectisel = new RectF(ecritx, ecrity + 1.0f * quatredixiememillimetre, ecritx - taillex, ecrity + 2.0f * quatredixiememillimetre);
          surfacecanvas.drawRoundRect(rectisel, quatredixiememillimetre, quatredixiememillimetre, model.KeyboardFocus);
        }
        surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.Menu1TextePaint);

        mabase += model.GenericCaseH + model.GenericInterSpace;
        if (mabase > mysurf.myheight)
          return;

      }

    }

  }

  private volatile int deleterecursivecounter = 0;
  private volatile long deleterecursivetimer = System.currentTimeMillis();
  void deletefiles(File fileOrDirectory) {
    final long newtime = System.currentTimeMillis();
    if (newtime - deleterecursivetimer > 1000) {
      deleterecursivetimer = newtime;
      model.message(deleterecursivecounter + " thumbs removed");
    }
    if (fileOrDirectory.isDirectory()) {
      File[] children = fileOrDirectory.listFiles();
      if (children != null)
        for (File child : children)
          if (child != null)
            deletefiles(child);
    } else {
      if (fileOrDirectory.delete())
        deleterecursivecounter += 1;
    }
  }

  private void shellexecute(boolean picture){
    new Thread(new Runnable() {
      @Override
      public void run() {
        int delayseconds = model.screenshotdelay;
        int timelimit = model.screenrecordlength;
        Runtime runtime = Runtime.getRuntime();
        Process proc = null;
        OutputStreamWriter osw = null;
        String sbstdOut = "";
        String sbstdErr = "";
        int exitvalue = -128;
        String filename = "ghfgh";
        String command = "";

        try { // Run Script
            proc = runtime.exec("su");
            //proc = runtime.exec("pwd");

            for (int i = delayseconds ; i > 0 ; i--) {
              try {
                Thread.sleep(1000);
              } catch (InterruptedException e) {
                e.printStackTrace();
              }
              model.message(i + "s");
            }

            long starttime = System.currentTimeMillis();
            osw = new OutputStreamWriter(proc.getOutputStream());
            if (picture) {
              filename = model.dossierscreenshot + "su" + starttime + ".png";
              command = "screencap -p " + filename;
              osw.write(command + " ;");
            } else {
              int temps = 0;
              while (temps < timelimit || (timelimit == 0 && temps == 0)) {
                command = "screenrecord";
                if (timelimit > 0 && timelimit - temps < 180) {
                  int diff = timelimit - temps;
                  command += " --time-limit " + diff;
                  temps += diff;
                } else {
                  temps += 180;
                }
                filename = model.dossierscreenshot + "su" + starttime + ".mp4";
                command += " " + filename;
                osw.write(command + " ;");
                starttime += 180000;
              }
            }
            osw.flush();
            osw.close();
            llog.d(TAG, "wrote " + filename);
        } catch (IOException ex) {
            ex.printStackTrace();
            model.message("screenshot failed");
        } finally {
            if (osw != null) {
                try {
                    osw.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        try {
            if (proc != null) {
                proc.waitFor();
            } else {
                return;
            }
        } catch (InterruptedException e) {
            llog.d(TAG, "InterruptedException e");
            e.printStackTrace();
        }

        try {
            InputStreamReader is = new InputStreamReader(proc.getInputStream());
            BufferedReader reader = new BufferedReader(is);
            StringBuilder stdout = new StringBuilder();
            String line;
            try {
                while ((line = reader.readLine()) != null) {
                    stdout.append(" > " + line + "\n");
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            sbstdOut = stdout.toString();
            is.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            InputStreamReader is = new InputStreamReader(proc.getErrorStream());
            BufferedReader reader = new BufferedReader(is);
            StringBuilder stdout = new StringBuilder();
            String line;
            try {
                while ((line = reader.readLine()) != null) {
                    stdout.append("E> " + line + "\n");
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            sbstdErr = stdout.toString();
            is.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        exitvalue = proc.exitValue();

        if (exitvalue == -128) {
            llog.d(TAG, "Device is not rooted.");
        } else {
          llog.d(TAG, "Return code = " + exitvalue + "\n\n" + sbstdOut + "\n\n" + sbstdErr);
        }

        llog.d(TAG, "scren captured " + filename);
        try {
          model.commandethreaddatabase.put(new String[]{"chercheFichierDansDatabaseSinonRescanSonDossier", model.dossierscreenshot, filename, "-1"});
        } catch (InterruptedException e) {
          e.printStackTrace();
        }

      }
    }).start();
  }

}
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
















