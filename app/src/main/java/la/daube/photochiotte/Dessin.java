package la.daube.photochiotte;

import android.graphics.Bitmap;
import android.graphics.Color;

import java.io.Serializable;
import java.util.ArrayList;

class Dessin {
  
  public int couche;
  public int drawerasefillcontour;
  
  public int nbdepoints;
  public float[] x;
  public float[] y;
  public float[] p;
  public float[] d;
  public float[] t;
  
  public int couleur;
  public float couleura;
  public float couleurh;
  public float couleurs;
  public float couleurv;
  
  public float pressuremin;
  public float pressuremax;
  public float sizemin;
  public float sizemax;
  
  public float bezierlength;
  
  public ArrayList<float[]> colormatrix;
  
  public void generecouleur() {
    this.couleur = Color.HSVToColor((int) couleura, new float[]{couleurh, couleurs, couleurv});
  }
  
}
































