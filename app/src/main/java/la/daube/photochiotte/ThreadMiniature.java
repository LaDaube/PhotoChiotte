package la.daube.photochiotte;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.locks.ReentrantLock;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import android.graphics.Matrix;
import android.graphics.drawable.Drawable;
import android.media.ExifInterface;
import android.util.Base64;

import kotlin.text.Charsets;

public class ThreadMiniature implements Runnable {
  private static final String TAG = "YYYtmin";

  private Gallery model;

  private volatile boolean iscleaningup = false;
  private final ReentrantLock lockb = new ReentrantLock();
  private volatile ArrayList<String> loadingb = new ArrayList();
  private boolean isloadingb(String df){
    boolean isloading = false;
    lockb.lock();
    int loadings = loadingb.size();
    for (int i = 0 ; i < loadings ; i++){
      if (df.equals(loadingb.get(i))){
        isloading = true;
        break;
      }
    }
    lockb.unlock();
    return isloading;
  }
  private boolean setloadingb(String df){
    boolean adeja = false;
    lockb.lock();
    int loadings = loadingb.size();
    for (int i = 0 ; i < loadings ; i++){
      if (df.equals(loadingb.get(i))){
        adeja = true;
        break;
      }
    }
    if (!adeja)
      loadingb.add(df);
    lockb.unlock();
    return !adeja;
  }
  private void unsetloadingb(String df){
    lockb.lock();
    int found = -1;
    int loadings = loadingb.size();
    for (int i = 0 ; i < loadings ; i++){
      if (df.equals(loadingb.get(i))){
        found = i;
        break;
      }
    }
    if (found != -1)
      loadingb.remove(found);
    else
      llog.d(TAG, "Error shouldn't happen unsetloading " + df);
    lockb.unlock();
  }
  private final ReentrantLock lock = new ReentrantLock();
  private volatile ArrayList<String> loading = new ArrayList();
  private boolean isloading(String df){
    boolean isloading = false;
    lock.lock();
    int loadings = loading.size();
    for (int i = 0 ; i < loadings ; i++){
      if (df.equals(loading.get(i))){
        isloading = true;
        break;
      }
    }
    lock.unlock();
    return isloading;
  }
  private boolean setloading(String df){
    boolean adeja = false;
    lock.lock();
    int loadings = loading.size();
    for (int i = 0 ; i < loadings ; i++){
      if (df.equals(loading.get(i))){
        adeja = true;
        break;
      }
    }
    if (!adeja)
      loading.add(df);
    lock.unlock();
    return !adeja;
  }
  private void unsetloading(String df){
    lock.lock();
    int found = -1;
    int loadings = loading.size();
    for (int i = 0 ; i < loadings ; i++){
      if (df.equals(loading.get(i))){
        found = i;
        break;
      }
    }
    if (found != -1)
      loading.remove(found);
    else
      llog.d(TAG, "Error shouldn't happen unsetloading " + df);
    lock.unlock();
  }
  private volatile long lastloading = 0;
  private final static long lastloadingminhelper = 4000L;
  public void creeminiaturethread(){
    lastloading = System.currentTimeMillis();
    for (int i = 0 ; i < Gallery.miniaturethreadl ; i++) {
      if (model.miniaturethreadrunning[i])
        continue;
      final int iis = i;
      new Thread(new Runnable() {
        @Override
        public void run() {
          model.miniaturethreadrunning[iis] = true;
          llog.d(TAG, "++++++++++++++++++++++ miniaturethread " + iis);
          String[] newcommande = null;
          while (true) {
            try {
              newcommande = model.commandeminiaturethreadqueue.take();
            } catch (InterruptedException e) {
            }
            if (newcommande == null)
              break;
            if (newcommande.length == 0)
              break;
            if (!model.threadminiaturerunning || model.databasecurrentlyremovingfiles || iscleaningup)
              continue;
            boolean updateminiature = false;
            if (newcommande[0].equals("updateminiature"))
              updateminiature = true;
            int mysurfid = Integer.parseInt(newcommande[1]);
            if (mysurfid >= model.surf.size())
              continue;
            int thumbcurrentlydisplayed = Integer.parseInt(newcommande[4]);
            if (thumbcurrentlydisplayed != model.surf.get(mysurfid).thumbcurrentlydisplayed && !updateminiature)
              continue; // modifié après la commande drawbitmap/updateminiature
            if (iis > 0.666f * Gallery.miniaturethreadl) {
              if (System.currentTimeMillis() - lastloading < lastloadingminhelper) {
                try {
                  model.commandeminiaturethreadqueue.put(newcommande);
                } catch (InterruptedException e) {}
                try {
                  Thread.sleep(100);
                } catch (InterruptedException e) {}
                continue;
              }
            }
            String df = newcommande[2] + "+" + newcommande[3];
            if (!setloading(df))
              continue;
            int cedossier = Integer.parseInt(newcommande[2]);
            int cefichier = Integer.parseInt(newcommande[3]);
            if (model.getFileBitmapInMemory(cedossier, cefichier) && !updateminiature) {
              unsetloading(df);
              continue; // on dessine l'ancien tant qu'on n'a pas le nouveau
            }
            Media media = model.copymedia(cedossier, cefichier);
            if (media == null) {
              unsetloading(df);
              continue;
            }
            if (iis <= 0.666f * Gallery.miniaturethreadl)
              lastloading = System.currentTimeMillis();
            media.ordnerIndex = cedossier;
            media.index = cefichier;
            if (!getthumbnail(media, mysurfid)) {
              //llog.d(TAG, String.format(iis + " .      %4d %4d ERROR", cedossier, cefichier) + " " + media.address);
              if (media.isOnline == Media.online_no) {
                try {
                  model.commandethreaddatabase.put(new String[]{"deletefilefromdatabase", media.ordnerAddress, media.address});
                } catch (InterruptedException e) {}
              }
            } else {
              //llog.d(TAG, String.format(iis + " .      f%4d %4d", cedossier, cefichier) + " " + media.address);
              try {
                model.commandethreadbrowser.put(new String[]{String.valueOf(mysurfid), "update"});
              } catch (InterruptedException e) {}
            }
            unsetloading(df);
          }
          llog.d(TAG, "------------------------ miniaturethread " + iis);
          model.miniaturethreadrunning[iis] = false;
        }
      }).start();
    }
  }

  private volatile long lastloadingb = 0;
  private final static long lastloadingbminhelper = 4000L;
  public void creebigimagethread(){
    lastloadingb = System.currentTimeMillis();
    for (int i = 0 ; i < Gallery.bigimagethreadl ; i++) {
      if (model.bigimagethreadrunning[i])
        continue;
      final int iis = i;
      new Thread(new Runnable() {
        @Override
        public void run() {
          model.bigimagethreadrunning[iis] = true;
          llog.d(TAG, "++++++++++++++++++++++ bigimagethread " + iis);
          while (true) {
            String[] newcommande = null;
            try {
              newcommande = model.commandebigimagethreadqueue.take();
            } catch (InterruptedException e) {}
            if (newcommande == null)
              break;
            if (newcommande.length == 0)
              break;
            if (!model.threadminiaturerunning || iscleaningup)
              continue;
            boolean updatebigpicture = false;
            if (newcommande[0].equals("updatebigpicture"))
              updatebigpicture = true;
            int mysurfid = Integer.parseInt(newcommande[1]);
            if (mysurfid >= model.surf.size())
              continue;
            Surf mysurf = model.surf.get(mysurfid);
            int cedossier = Integer.parseInt(newcommande[2]);
            int cefichier = Integer.parseInt(newcommande[3]);
            int bigimagecurrentlydisplayed = Integer.parseInt(newcommande[4]);
            boolean forcerecharge = Boolean.parseBoolean(newcommande[5]);
            int cefichiersuivant;
            if (mysurf.cachefromlefttoright)
              cefichiersuivant = cefichier + 1;
            else
              cefichiersuivant = cefichier - 1;
            if (bigimagecurrentlydisplayed != mysurf.bigimagecurrentlydisplayed && !updatebigpicture)
              continue;

            if (iis > 0.666f * Gallery.bigimagethreadl) {
              if (System.currentTimeMillis() - lastloadingb < lastloadingbminhelper) {
                try {
                  model.commandebigimagethreadqueue.put(newcommande);
                } catch (InterruptedException e) {}
                try {
                  Thread.sleep(100);
                } catch (InterruptedException e) {}
                continue;
              }
            }
            String df = cedossier + "+" + cefichier;
            if (!setloadingb(df))
              continue;
            //llog.d(TAG, String.format(iis + "  +     %4d %4d", cedossier, cefichier) + " setloading");
            Media media = model.copymedia(cedossier, cefichier);
            if (media == null) {
              //llog.d(TAG, String.format(iis + "  +     %4d %4d", cedossier, cefichier) + " media null");
              unsetloadingb(df);
              continue;
            }
            media.ordnerIndex = cedossier;
            media.index = cefichier;
            if (forcerecharge) {
              //llog.d(TAG, String.format(iis + "  +     %4d %4d", cedossier, cefichier) + " forcerecharge removekeysfromcache");
              mysurf.removekeysfromcache();
              mysurf.removefromcache();
              for (Surf thissurf : model.surf)
                thissurf.putbigpictureinmemory = true;
            }
            lastloadingb = System.currentTimeMillis();
            // si on n'a pas chargé la miniature avant (retour vers l'appli)
            // copymedia -> media.bitmapcount ne sera pas enregistré pas getthebigpicture
            if (updatebigpicture && newcommande.length > 7)
              media.filmstripCount = Integer.parseInt(newcommande[7]);
            if (!mysurf.isincache(media.address) || updatebigpicture || media.isALinkThatCreatesAnOrdner) {
              if (cefichier == mysurf.mediaIndex && cedossier == mysurf.ordnerIndex) {
                for (String encache : mysurf.getcachekeyset()) {
                  if (!encache.equals(media.address)) {
                    mysurf.removekeyfromcache(encache);
                    //llog.d(TAG, String.format(iis + "  +     %4d %4d", cedossier, cefichier) + " removekeyfromcache " + encache);
                  }
                }
                mysurf.removefromcache();
                boolean mediasuccess = getbigpicture(media, mysurfid, model);
                if (mediasuccess) {
                  //llog.d(TAG, String.format(iis + "  +     %4d %4d", cedossier, cefichier) + " " + media.address);
                  if (bigimagecurrentlydisplayed == mysurf.bigimagecurrentlydisplayed || updatebigpicture) {
                    try {
                      model.commandethreadbrowser.put(new String[]{String.valueOf(mysurfid), "dontmissupdate"});
                    } catch (InterruptedException e) {
                    }
                  } else {
                    try {
                      model.commandethreadbrowser.put(new String[]{String.valueOf(mysurfid), "update"});
                    } catch (InterruptedException e) {
                    }
                  }
                } else if (media.isOnline == Media.online_no) {
                  //llog.d(TAG, String.format(iis + "  +     %4d %4d ERROR", cedossier, cefichier) + " " + media.address);
                  try {
                    model.commandethreaddatabase.put(new String[]{"deletefilefromdatabase", media.ordnerAddress, media.address});
                  } catch (InterruptedException e) {
                  }
                } else {
                  //llog.d(TAG, String.format(iis + "  +     %4d %4d ERROR2", cedossier, cefichier) + " " + media.address);
                }
              }
            } else {
              //llog.d(TAG, String.format(iis + "  +     %4d %4d", cedossier, cefichier) + " isincache");
            }
            unsetloadingb(df);

            if (cefichier == mysurf.mediaIndex && cedossier == mysurf.ordnerIndex) {
              df = cedossier + "+" + cefichiersuivant;
              if (!setloadingb(df))
                continue;
              //llog.d(TAG, String.format(iis + "    ++  %4d %4d", cedossier, cefichiersuivant) + " setloading");
              Media mediasuivant = model.copymedia(cedossier, cefichiersuivant);
              if (mediasuivant == null) {
                //llog.d(TAG, String.format(iis + "    ++  %4d %4d", cedossier, cefichiersuivant) + " null");
                unsetloadingb(df);
                continue;
              }
              mediasuivant.ordnerIndex = cedossier;
              mediasuivant.index = cefichiersuivant;
              if (!mysurf.isincache(mediasuivant.address)) {
                for (String encache : mysurf.getcachekeyset()) {
                  if (!encache.equals(media.address) && !encache.equals(mediasuivant.address)) {
                    mysurf.removekeyfromcache(encache);
                    //llog.d(TAG, String.format(iis + "  +     %4d %4d", cedossier, cefichier) + " removekeyfromcache " + encache);
                  }
                }
                mysurf.removefromcache();
                boolean mediasuccess = getbigpicture(mediasuivant, mysurfid, model);
                if (mediasuccess) {
                  //llog.d(TAG, String.format(iis + "    ++  %4d %4d", cedossier, cefichiersuivant) + " " + mediasuivant.address);
                  try {
                    model.commandethreadbrowser.put(new String[]{String.valueOf(mysurfid), "update"});
                  } catch (InterruptedException e) {
                  }
                } else if (mediasuivant.isOnline == Media.online_no) {
                  //llog.d(TAG, String.format(iis + "    ++  %4d %4d ERROR", cedossier, cefichiersuivant) + " " + mediasuivant.address);
                  try {
                    model.commandethreaddatabase.put(new String[]{"deletefilefromdatabase", mediasuivant.ordnerAddress, mediasuivant.address});
                  } catch (InterruptedException e) {
                  }
                } else {
                  //llog.d(TAG, String.format(iis + "    ++  %4d %4d ERROR2", cedossier, cefichiersuivant) + " " + mediasuivant.address);
                }
              } else {
                //llog.d(TAG, String.format(iis + "    ++  %4d %4d", cedossier, cefichiersuivant) + " isincache");
              }
              unsetloadingb(df);
            } else {
              //llog.d(TAG, String.format(iis + "    ++  %4d %4d", cedossier, cefichiersuivant) + " mediaIndex");
            }
          }
          llog.d(TAG, "--------------------- bigimagethread " + iis);
          model.bigimagethreadrunning[iis] = false;
        }
      }).start();
    }
  }

  protected ThreadMiniature(Gallery mmodel){
    model = mmodel;
  }

  @Override
  public void run(){
    model.threadminiaturerunning = true;
    llog.d(TAG, "+++++++++++++++++++++ ThreadMiniature");

    while (model.dossierminiature == null) {
      try {
        Thread.sleep(10);
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
    }

    creeminiaturethread();
    creebigimagethread();

    String []newcommande = new String[]{"hello"};
    while (true) {

      boolean encore = true;
      while (encore) {
  
        try {
          newcommande = model.commandethreadminiature.take();
        } catch (InterruptedException e) {
          e.printStackTrace();
        }
  
        if (newcommande[0].equals("quit")) {
          if (model.threadminiatureon) {
            llog.d(TAG, "////////////////////// thread miniature keep running");
          } else {
            //llog.d(TAG, "quit cleanup demandé : on nettoie tous les thumbnails en cache "+model.numberthumsincache);
            //model.getFileFreeAllBitmapsFromFolders(false);
            //model.clearbitmaps();
            //llog.d(TAG, "quit cleanup demandé : on a nettoyé tous les thumbnails en cache reste "+model.numberthumsincache);
            model.getFileFreeAllBitmapsFromFolders(true);
            model.threadminiaturerunning = false;
            llog.d(TAG, "----------------------- ThreadMiniature");
            return;
          }
        /*
              locking method
        } else if (wearelocked) {
  
          if (newcommande[0].equals("unlock")) {
            wearelocked = false;
            try {
              model.commandethreadbrowser.put(new String[]{"-1", "update"});
            } catch (InterruptedException e) {
              e.printStackTrace();
            }
          } else {
            // on remet à la fin les autres ordres le temps de la pause
            if (model.commandethreadminiature.isEmpty()) {
              try {
                Thread.sleep(30);
              } catch (InterruptedException e) {
                e.printStackTrace();
              }
            }
            if (!newcommande[0].equals("loadminiature")) {
              try {
                model.commandethreadminiature.put(newcommande);
              } catch (InterruptedException e) {
                e.printStackTrace();
              }
            } // sinon on vide simplement la liste car sinon ça crashe à cause de dossierprincipal = Integer.parseInt(newcommande[1]);
          }

        } else if (newcommande[0].equals("reloadmtouslesdossiers")) {
          boolean attendthread = true;
          while (attendthread) {
            attendthread = false;
            for (int i = 0; i < bigimagethreadl; i++) {
              if (bigimagethreadrunning[i]) {
                attendthread = true;
                break;
              }
            }
            for (int i = 0; i < miniaturethreadl; i++) {
              if (miniaturethreadrunning[i]) {
                attendthread = true;
                break;
              }
            }
            if (attendthread) {
              try {
                Thread.sleep(50);
              } catch (InterruptedException e) {
                e.printStackTrace();
              }
            }
          }
          try {
            model.commandethreaddatabase.put(new String[]{"reloadmtouslesdossiers"});
          } catch (InterruptedException e) {
            e.printStackTrace();
          }
          wearelocked = true;*/

        } else if (newcommande[0].equals("supercleanup")) {
          iscleaningup = true;
          final boolean silent;
          if (newcommande.length > 1)
            silent = true;
          else
            silent = false;
          if (!silent) {
            for (int i = 0; i < 3; i++) {
              model.message("clean up " + model.dossierminiature + " in " + (3 - i) + "s");
              try {
                Thread.sleep(1000);
              } catch (InterruptedException e) {
              }
            }
          }
          deleterecursivecounter = 0;
          final File[] children = new File(model.dossierminiature).listFiles();
          //new Thread(new Runnable() {
          //  @Override
          //  public void run() {
          deleteminiatures(children, silent);
          //  }
          //}).start();
          llog.d(TAG, deleterecursivecounter + " thumbnails cleaned up in " + model.dossierminiature);
          if (!silent)
            model.message(deleterecursivecounter + " thumbnails cleaned up in\n" + model.dossierminiature);
          iscleaningup = false;
          if (!new File(model.dossierminiature).exists())
            llog.d(TAG, "------------------- ERRRORRRR deleterecursive cleanup -----------------------------------------------");
          try {
            model.commandethreadminiature.put(new String[]{"cleanup", "force"});
          } catch (InterruptedException e) {
            e.printStackTrace();
          }
          
        } else if (newcommande[0].equals("cleanup")) {
          llog.d(TAG, "cleanup demandé : on nettoie tous les thumbnails en cache il y en a " + model.numberthumsincache + " " + newcommande[1]);
          if (newcommande[1].equals("force"))
            model.getFileFreeAllBitmapsFromFolders(true);
          else
            model.getFileFreeAllBitmapsFromFolders(false);
          if (newcommande.length == 2) {
            try {
              model.commandethreadbrowser.put(new String[]{"-1", "update"});
            } catch (InterruptedException e) {
              e.printStackTrace();
            }
          }
          llog.d(TAG, "on a nettoyé tous les thumbnails en cache il en reste  " + model.numberthumsincache);

  
        } else if (model.commandethreadminiature.isEmpty()) {
          encore = false;
        }
  
      }
      
    }
  }

  private final static Pattern timestamp = Pattern.compile("(.*?)(\\d{10}|\\d{13})(.+)");
  public static String epochToDate(String name){
    String epochtodate = name.replaceAll(".*/", "");
    Matcher matcher = timestamp.matcher(epochtodate);
    if (matcher.find()) {
      String m = matcher.group(2);
      if (m != null) {
        try {
          long t = (long) Long.parseLong(m);
          long currt;
          if (m.length() == 13)
            currt = (long) (System.currentTimeMillis());
          else
            currt = (long) (System.currentTimeMillis() / 1000L);
          if (t < currt) {
            if (currt - 24 * 3600 * 60 < t) {
              SimpleDateFormat formatter = new SimpleDateFormat(" dd EEE HH:mm ", Locale.getDefault());
              epochtodate = String.valueOf(matcher.group(1))
                      + formatter.format(new Date(t * 1000L))
                      + String.valueOf(matcher.group(3));
              return epochtodate;
            } else {
              llog.d(TAG, "date too old " + t + " << " + currt);
            }
          } else {
            llog.d(TAG, "date posterior " + t + " > " + currt);
          }
        } catch (Exception e) {
          llog.d(TAG, e.toString());
        }
      } else {
        llog.d(TAG, "date matcher null");
      }
    } else {
      llog.d(TAG, "date matcher did not find timestamp");
    }
    return null;
  }
  
  private static void prettyprint(Media media, Canvas canv, float width, float height, boolean wantminiature, Gallery model){

    if (!model.allowfakebitmaps)
      return;

    String ligne = "";
    Rect bounds = new Rect();
    int pos;
    float posx = model.GenericInterSpace;
    float posy = model.GenericInterSpace;
    int lignelmax;
    int lignel;
    float taillex;
    width *= 0.97f;
    int reflmax = 45;
    Paint montextepaint;
    float taillei;
    int nbcar;
    int lignef;
    int refl;
    int textel;
    String ref;

    String texte;
    if (media.isonlineparentlink != null) { // variable inutile virable
      texte = media.isonlineparentlink;
    } else if (media.printName != null) {
      texte = media.printName;
    } else if (media.addressescaped != null) {
      texte = media.addressescaped;
    } else if (media.address != null) {
      texte = media.address;
    } else {
      texte = "dummy";
    }
    if (media.printName == null) {
      textel = texte.length();
      if (texte.endsWith("/")) {
        texte = texte.substring(0, textel - 1);
        textel = texte.length();
      }
      int dernierslash = texte.lastIndexOf("/");
      if (dernierslash != -1 && dernierslash < textel - 1)
        texte = texte.substring(dernierslash + 1);
    }
    if (!wantminiature)
      if (media.metadatacreationtime != null)
        texte += " " + media.metadatacreationtime;

    lignelmax = 0;
    refl = reflmax;
    textel = texte.length();
    if (textel < refl)
      refl = textel;
    ref = texte.substring(0, refl);
    refl = ref.length();
    if (wantminiature)
      montextepaint = model.PictureLabelThumb;
    else
      montextepaint = model.PictureLabel;
    montextepaint.getTextBounds(ref, 0, refl, bounds);
    taillei = bounds.width() / (float) refl;
    nbcar = (int) ((float) width / taillei);
    if (nbcar < 5)
      nbcar = 5;
    pos = 0;
    while (pos < textel) {

      lignef = pos + nbcar;
      if (lignef > textel)
        lignef = textel;
      ligne = texte.substring(pos, lignef);

      lignel = ligne.length();
      montextepaint.getTextBounds(ligne, 0, lignel, bounds);
      taillex = bounds.width();

      if (taillex > width) {
        float reduction = ((float) width) / taillex;
        nbcar = (int) (0.95f * reduction * ((float) nbcar));
        if (nbcar < 5)
          nbcar = 5;
        if (nbcar < lignel) {
          ligne = ligne.substring(0, nbcar);

          lignel = ligne.length();
          montextepaint.getTextBounds(ligne, 0, lignel, bounds);
          taillex = bounds.width();
        }
      }

      if (pos + lignel >= textel && taillex < width) {
      } else if (taillex > width * 0.5f) {
        if (lignel > lignelmax)
          lignelmax = lignel;
        int jmin = (int) (lignel - lignelmax / 2.0f);
        char nocar;
        for (int j = lignel - 1 ; j > jmin && j >= 0 ; j--) {
          nocar = ligne.charAt(j);
          if (nocar == ' ' || nocar == '.' || nocar == '-' || nocar == '(' || nocar == '[' || nocar == ']' || nocar == ')' || nocar == '\n') {
            ligne = ligne.substring(0, j + 1);
            lignel = ligne.length();
            montextepaint.getTextBounds(ligne, 0, lignel, bounds);
            break;
          }
        }
      }

      pos += lignel;

      posy += bounds.height() + model.GenericInterSpace * 0.60f;
      canv.drawRect(posx, posy - bounds.height(), posx + bounds.left + bounds.width(), posy, model.MiniatureBgPaint);
      canv.drawText(ligne, posx, posy - bounds.bottom, montextepaint);

      if (wantminiature)
        montextepaint = model.PictureLabelSmallThumb;
      else
        montextepaint = model.PictureLabelSmall;

      if (posy > height * 0.40f)
        break;
    }

    if (!wantminiature && media.printDetails != null) {
      texte = media.printDetails;

      lignelmax = 0;
      refl = reflmax;
      textel = texte.length();
      if (textel < refl)
        refl = textel;
      ref = texte.substring(0, refl);
      refl = ref.length();
      montextepaint.getTextBounds(ref, 0, refl, bounds);
      taillei = bounds.width() / (float) refl;
      nbcar = (int) ((float) width / taillei);
      if (nbcar < 5)
        nbcar = 5;
      pos = 0;
      while (pos < textel) {

        lignef = pos + nbcar;
        if (lignef > textel)
          lignef = textel;
        ligne = texte.substring(pos, lignef);

        lignel = ligne.length();
        montextepaint.getTextBounds(ligne, 0, lignel, bounds);
        taillex = bounds.width();

        if (taillex > width) {
          float reduction = ((float) width) / taillex;
          nbcar = (int) (0.95f * reduction * ((float) nbcar));
          if (nbcar < 5)
            nbcar = 5;
          if (nbcar < lignel) {
            ligne = ligne.substring(0, nbcar);

            lignel = ligne.length();
            montextepaint.getTextBounds(ligne, 0, lignel, bounds);
            taillex = bounds.width();
          }
        }

        if (pos + lignel >= textel && taillex < width) {
        } else if (taillex > width * 0.5f) {
          if (lignel > lignelmax)
            lignelmax = lignel;
          int jmin = (int) (lignel - lignelmax / 2.0f);
          char nocar;
          for (int j = lignel - 1 ; j > jmin && j >= 0 ; j--) {
            nocar = ligne.charAt(j);
            if (nocar == ' ' || nocar == '.' || nocar == '-' || nocar == '(' || nocar == '[' || nocar == ']' || nocar == ')' || nocar == '\n') {
              ligne = ligne.substring(0, j + 1);
              lignel = ligne.length();
              montextepaint.getTextBounds(ligne, 0, lignel, bounds);
              break;
            }
          }
        }

        pos += lignel;

        posy += bounds.height() + model.GenericInterSpace * 0.60f;
        canv.drawRect(posx, posy - bounds.height(), posx + bounds.left + bounds.width(), posy, model.MiniatureBgPaint);
        canv.drawText(ligne, posx, posy - bounds.bottom, montextepaint);

        if (posy > height * 0.40f)
          break;
      }

    }

    if (media.isonlinelinktonextpagetot > 1) {
      posy = height;
      ligne = "("+media.isonlinelinktonextpagei+"/"+media.isonlinelinktonextpagetot+")";
      lignel = ligne.length();
      montextepaint.getTextBounds(ligne, 0, lignel, bounds);
      canv.drawRect(posx, posy - bounds.height(), posx + bounds.left + bounds.width(), posy, model.MiniatureBgPaint);
      canv.drawText(ligne, posx, posy - bounds.bottom, montextepaint);

    } else if (!wantminiature && media.printFooter != null) {
      posy = height;
      /*int subsl= media.printFooter.length();
      int subsmin = subsl;
      int subs = media.printFooter.indexOf(",external_subtitle=");
      if (subs >= 0 && subs < subsmin)
        subsmin = subs;
      subs = media.printFooter.indexOf(",external_subtitleEncode=");
      if (subs >= 0 && subs < subsmin)
        subsmin = subs;
      subs = media.printFooter.indexOf(",external_subtitleencode=");
      if (subs >= 0 && subs < subsmin)
        subsmin = subs;
      subs = media.printFooter.indexOf(",position=");
      if (subs >= 0 && subs < subsmin)
        subsmin = subs;
      subs = media.printFooter.indexOf(",sequence=true");
      if (subs >= 0 && subs < subsmin)
        subsmin = subs;
      if (subsmin < subsl)
        ligne = media.printFooter.substring(0, subsmin);
      else
        ligne = media.printFooter;*/
      ligne = media.printFooter;
      lignel = ligne.length();
      if (lignel > 0) {
        montextepaint.getTextBounds(ligne, 0, lignel, bounds);
        canv.drawRect(posx, posy - bounds.height(), posx + bounds.left + bounds.width(), posy, model.MiniatureBgPaint);
        canv.drawText(ligne, posx, posy - bounds.bottom, montextepaint);
      }
    }

  }

  public static String getMediaMetadataForLibextractor(Media file) {
    String metadata = "";
    if (file.printName != null)
      metadata += "printName=" + file.printName + "\n";
    if (file.printDetails != null)
      metadata += "printDetails=" + file.printDetails + "\n";
    if (file.printFooter != null)
      metadata += "printFooter=" + file.printFooter + "\n";
    if (file.playInSequence)
      metadata += "playInSequence=true\n";
    if (file.playStartAtPosition != 0)
      metadata += "playStartAtPosition=" + file.playStartAtPosition + "\n";
    if (file.subtitleAddress != null) {
      int subtitleAddressl = file.subtitleAddress.size();
      for (int i = 0 ; i < subtitleAddressl ; i++) {
        metadata += "subtitleAddress=" + file.subtitleAddress.get(i) + "\n";
      }
    }
    if (metadata.length() == 0)
      return null;
    return metadata;
  }

  private boolean getthumbnail(Media media, int mysurfid){
    //llog.w(TAG, "getthumbnail " + media.address + " " + media.addressToGetThumbnail);
    int thumbsize = model.thumbsize;
    int filmstripcurrent = media.filmstripcurrent;
    Bitmap cachedbitmap = null;
    boolean createfakebitmap = false;
    boolean getbitmapfrombuffer = false;
    boolean isAllowedToResizeThumbnail = true;
    byte[] buffer = null;
    if (media.isALinkThatCreatesAnOrdner && media.addressToGetThumbnail == null) {
      // c'est juste un lien on ne cherche pas à lui télécharger une image
      createfakebitmap = true;
      media.filmstripCount = Media.filmstripCreateFakeBitmap;
    } else if (media.isOnline != Media.online_no) {

      int handleThumbnailAddress = -1;
      String addressToGetThumbnail = media.addressToGetThumbnail;
      if (media.isOnline == Media.online_apache) {
        if (addressToGetThumbnail == null) {
          handleThumbnailAddress = 1;
          addressToGetThumbnail = media.address + ".thmb_0";
        } else if (addressToGetThumbnail.equals(media.address)) {
          handleThumbnailAddress = 1;
          addressToGetThumbnail = media.address + ".thmb_0";
        } else if (addressToGetThumbnail.endsWith(".thmb_0")) {
          handleThumbnailAddress = 2;
        } else if (addressToGetThumbnail.endsWith(".thmb")) {
          handleThumbnailAddress = 3;
          addressToGetThumbnail += "_0";
        } // else there is no .thmb
        if (handleThumbnailAddress > 0) {
          if (media.filmstripCount == Media.filmstripNotCheckedYet) {
            buffer = InternetSession.getPicture(model, addressToGetThumbnail, media, false);
            getFilmstripCount(media, buffer, model);
            if (media.filmstripCount > 0)
              filmstripcurrent = (int) ((model.filmstripreftime / model.filmstripanimatetime) % (media.filmstripCount));
            model.setMediaFilmstripCurrentAndCount(media, media.filmstripCount, filmstripcurrent); //sera fait après
          } else if (media.filmstripCount > 0) { // ici on ne re-télécharge pas une autre
            filmstripcurrent = (int) ((model.filmstripreftime / model.filmstripanimatetime) % (media.filmstripCount));
            int addressToGetThumbnaill = addressToGetThumbnail.length();
            addressToGetThumbnail = addressToGetThumbnail.substring(0, addressToGetThumbnaill - 1) + filmstripcurrent;
            buffer = InternetSession.getPicture(model, addressToGetThumbnail, media, false);
          }
        }
      }
      if (buffer == null) {
        if (media.addressToGetThumbnail != null)
          buffer = InternetSession.getPicture(model, media.addressToGetThumbnail, media, false);
        if (buffer == null && media.type == Media.media_picture) { // créé miniature depuis la grande
          buffer = InternetSession.getPicture(model, media.address, media, false);
        }
      }

      if (media.type != Media.media_picture || media.isALinkThatCreatesAnOrdner) {
        createfakebitmap = true; // on affiche en plus le texte par-dessus
      }
      if (buffer == null) {
        createfakebitmap = true;
      } else {
        getbitmapfrombuffer = true;
      }
      //llog.d(TAG, media.address + " fake " + createfakebitmap + " buffer " + getbitmapfrombuffer + " count " + media.filmstripCount);
    } else {
      if (media.isInsideAnArchive) {
        if (!new File(media.ordnerAddress).exists()) {
          llog.d(TAG, "   getthumbnail archive file does not exist don't create thumb remove from database " + media.ordnerAddress);
          return false;
        }
        if (media.filmstripCount == Media.filmstripCreateFakeBitmap) {
          createfakebitmap = true;
        } else {
          buffer = unzip(media);
          if (buffer == null) {
            llog.d(TAG, "   getthumbnail failed to unzip " + media.address);
            createfakebitmap = true;
            media.filmstripCount = Media.filmstripCreateFakeBitmap;
          } else {
            getbitmapfrombuffer = true;
            media.filmstripCount = Media.filmstripNoDirectBitmap;
          }
        }
      } else if (media.type == Media.media_app) {
        int imax = model.allappinfo.size();
        AppInfo appinfo = null;
        for (int i = 0 ; i < imax ; i++) {
          appinfo = model.allappinfo.get(i);
          if (media.address.equals(appinfo.packagename)) {
            break;
          } else {
            appinfo = null;
          }
        }
        float ts = model.thumbsize;
        cachedbitmap = Bitmap.createBitmap((int) ts, (int) ts, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(cachedbitmap);
        if (appinfo != null) {
          if (appinfo.statuscurrent == ShellExecuter.isUNINSTALLED) {
            canvas.drawRect(0, 0, ts, ts, model.AppinfoUninstalled);
          } else if (appinfo.statuscurrent == ShellExecuter.isDISABLED) {
            canvas.drawRect(0, 0, ts, ts, model.AppinfoDisabled);
          }
          if (appinfo.servicerunningl > 0) {
            canvas.drawRect(0, 0, ts, ts, model.AppinfoActive);
          }
        }
        try {
          Drawable drawable = model.packageManager.getApplicationIcon(media.address);
          drawable.setBounds((int) (ts * 0.10f),
              (int) (ts * 0.10f),
              (int) (ts * 0.90f),
              (int) (ts * 0.90f));
          drawable.draw(canvas);
        } catch (Exception e) {
          llog.d(TAG, "could not load app icon");
        }
        if (appinfo != null) {
          if (appinfo.statuscurrent == ShellExecuter.isDISABLED) {
            canvas.drawText("X", ts * 0.99f, ts * 0.99f, model.AppinfoTextPaint);
          }
          if (appinfo.servicerunningl > 0) {
            canvas.drawText("" + appinfo.servicerunningl, ts * 0.99f, ts * 0.99f, model.AppinfoTextPaint);
          }
        }
        createfakebitmap = true;
      } else {
        if (media.address != null && media.type != Media.media_button) {
          if (!new File(media.address).exists()) {
            llog.d(TAG, "   getthumbnail original file does not exist don't create thumb remove from database " + media.address);
            return false;
          }
        }
        if (media.filmstripCount == Media.filmstripCreateFakeBitmap) {
          llog.d(TAG, "   getthumbnail filmstripcountCreateFakeBitmap " + media.address);
          createfakebitmap = true;
        } else {
          String checkifexists;
          if (media.addressToGetThumbnail != null) {
            checkifexists = media.addressToGetThumbnail;
          } else if (media.addressToGetLibextractorsThumbnail >= 0) {
            checkifexists = model.dossierminiature + media.addressToGetLibextractorsThumbnail;
          } else if (media.addressToGetPreviewFullSize != null) {
            checkifexists = media.addressToGetPreviewFullSize;
          } else {
            checkifexists = media.address;
          }
          if (media.type == Media.media_button) {
            if (checkifexists.endsWith(".thmb_0"))
              checkifexists = checkifexists.substring(0, checkifexists.length() - 2);
            else if (!checkifexists.endsWith(".thmb"))
              checkifexists += ".thmb";
          }
          if (media.type != Media.media_button) {
            if (media.filmstripCount == Media.filmstripNotCheckedYet) {
              File miniature = new File(checkifexists + "_0");
              if (miniature.exists())
                getFilmstripCount(media, miniature, model); // if (media.metadatamaxwidth != thumbsize) { llog.d(TAG, "   getthumbnail wrong thumbnail dimensions, force resize" + media.address);
              if (!miniature.exists() || media.metadatamaxwidth != thumbsize) {
                String additionalmetadata = getMediaMetadataForLibextractor(media);
                int result = Libextractor.extract(media.address, model.dossierminiature + media.addressToGetLibextractorsThumbnail, thumbsize, thumbsize, media.type == Media.media_picture, additionalmetadata);
                if (result != 0) {
                  llog.d(TAG, "   getthumbnail Libextractor failed " + result + " " + media.address);
                  createfakebitmap = true;
                  media.filmstripCount = Media.filmstripCreateFakeBitmap;
                }
              }
            }
          }
          if (media.filmstripCount == Media.filmstripCreateFakeBitmap) {
            llog.d(TAG, "   getthumbnail 2 filmstripcountCreateFakeBitmap " + media.address);
            createfakebitmap = true;
          } else {
            if (media.filmstripCount == Media.filmstripNotCheckedYet) {
              File miniature = new File(checkifexists + "_0");
              if (miniature.exists())
                getFilmstripCount(media, miniature, model);
            }
            if (media.filmstripCount == 0) { // no thumb could be created by libextractor
              media.filmstripCount = Media.filmstripCreateFakeBitmap;
              createfakebitmap = true;
            } else {
              if (media.filmstripCount > 0) { // update filmstrip
                filmstripcurrent = (int) ((model.filmstripreftime / model.filmstripanimatetime) % (media.filmstripCount));
                checkifexists += "_" + filmstripcurrent;
              } else
                checkifexists += "_0";
              if (!new File(checkifexists).exists()) { // happens on drawable medias
                createfakebitmap = true;
              } else {
                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inPreferredConfig = Bitmap.Config.ARGB_8888;
                cachedbitmap = BitmapFactory.decodeFile(checkifexists, options);
                if (cachedbitmap == null) {
                  llog.d(TAG, "   getthumbnail wantminiature n'a pas pu être décodé cachedbitmap null " + media.address + " " + checkifexists);
                  createfakebitmap = true;
                } else {
                  isAllowedToResizeThumbnail = false;
                }
              }
            }
          }
        }
      }
    }

    int originalwidth = 0;
    int originalheight = 0;
    if (cachedbitmap != null) {
      originalwidth = cachedbitmap.getWidth();
      originalheight = cachedbitmap.getHeight();
    }

    if (cachedbitmap == null || getbitmapfrombuffer) {
      /**
       *    on créé un thumbnail à partir d'une image quelconque online
       *    ou à partir du grand fichier mais là on utilise plutôt libextractor
       */
      if (media.isOnline == Media.online_no && !media.isInsideAnArchive && !getbitmapfrombuffer && media.type != Media.media_button) {
        if (!new File(media.address).exists()) {
          llog.d(TAG, "   getthumbnail doesn't exist don't create thumb from big delete file from database " + media.address);
          return false;
        }
      }
      BitmapFactory.Options options = new BitmapFactory.Options();
      options.inJustDecodeBounds = true;
      if (getbitmapfrombuffer) {
        int bufferl = buffer.length;
        BitmapFactory.decodeByteArray(buffer, 0, bufferl, options);
      } else {
        BitmapFactory.decodeFile(media.address, options);
      }
      originalwidth = options.outWidth;
      originalheight = options.outHeight;
      //llog.d(TAG, "miniature size " + media.minipic + " : " + originalwidth + "x" + originalheight);
      if (originalwidth <= 0 || originalheight <= 0) {
        //llog.d(TAG, "   getthumbnail originalwidth <=0 createfakebitmap " + createfakebitmap + " getbitmapfrombuffer " + getbitmapfrombuffer  + " on supprime " + media.fichier); //+ " mais on ne delete pas (que si on ouvre la grande et que la grande merde)");
        createfakebitmap = true;
      } else {
        options.inSampleSize = calculateInSampleSize(originalwidth, originalheight, thumbsize, thumbsize);
        options.inPreferredConfig = Bitmap.Config.ARGB_8888;
        options.inJustDecodeBounds = false;
        if (createfakebitmap) {
          options.inMutable = true;
        }
        if (getbitmapfrombuffer) {
          int bufferl = buffer.length;
          try {
            cachedbitmap = BitmapFactory.decodeByteArray(buffer, 0, bufferl, options);
            //llog.d(TAG, "miniature decodeByteArray " + media.minipic + " : " + originalwidth + "x" + originalheight + " : " + cachedbitmap.getWidth() + "x" + cachedbitmap.getHeight());
          } catch(java.lang.OutOfMemoryError e) {
            llog.d(TAG, "   getthumbnail n'a pas pu être décodé buffer " + media.address);
            e.printStackTrace();
            createfakebitmap = true;
          }
        } else {
          cachedbitmap = BitmapFactory.decodeFile(media.address, options);
        }
        if (cachedbitmap == null) {
          if (media.isOnline != Media.online_no) {
            createfakebitmap = true;
          } else {
            llog.d(TAG, "   getthumbnail n'a pas pu être décodé " + media.address);
            createfakebitmap = true;
          }
        }
      }
    }

    if (cachedbitmap != null) {
      originalwidth = cachedbitmap.getWidth();
      originalheight = cachedbitmap.getHeight();
    }
    
    if (originalwidth <= 0 || originalheight <= 0) {
      originalwidth = thumbsize;
      originalheight = originalwidth;
      createfakebitmap = true;
    }

    /**
     *   rotations et filtres
     */
    /*if (mysurfid < model.surf.size()) {
      Surf mysurf = model.surf.get(mysurfid);
      int cachedbitmapwidth = originalwidth;
      int cachedbitmapheight = originalheight;
      if (mysurf.rotate == 270 || mysurf.rotate == 90) {
        int ooriginalwidth = originalwidth;
        originalwidth = originalheight;
        originalheight = ooriginalwidth;
      }
      if (cachedbitmap != null) {
        if (mysurf.rotate != 0 || mysurf.mirror == -1) {
          Matrix matrix = new Matrix();
          if (mysurf.mirror == -1)
            matrix.preScale(-1, 1);
          if (mysurf.rotate != 0)
            matrix.postRotate(mysurf.rotate);
          cachedbitmap = Bitmap.createBitmap(cachedbitmap, 0, 0, cachedbitmapwidth, cachedbitmapheight, matrix, false);
        }
        if (mysurf.brightness != 0 || mysurf.contrast != 0 || mysurf.saturation != 0 || mysurf.hue != 0) {
          cachedbitmap = ColorFilterGenerator.changeBitmapColor(cachedbitmap,
                  ColorFilterGenerator.adjustColor(mysurf.brightness, mysurf.contrast, mysurf.saturation, mysurf.hue));
        }
      }
    }*/

    if (isAllowedToResizeThumbnail && !createfakebitmap) {
      /**
       *    si c'est une miniature on la redimensionne sinon on ne modifie pas
       */
      if (cachedbitmap == null) {
        llog.d(TAG, "   getthumbnailgettheminitaurek cachedbitmap is null : " + media.address);
        createfakebitmap = true;
      } else if (cachedbitmap.isRecycled()) {
        llog.d(TAG, "   getthumbnailgettheminitaurek cachedbitmap is recycled : " + media.address);
        createfakebitmap = true;
      } else {
        float taillelimite = thumbsize;
        float ratio = ((float) originalheight) / ((float) originalwidth);
        if (ratio <= 1.0f) {
          originalwidth = (int) taillelimite;
          originalheight = (int) (taillelimite * ratio);
        } else {
          originalheight = (int) taillelimite;
          originalwidth = (int) (taillelimite / ratio);
        }
        if (originalwidth <= 0)
          originalwidth = 10;
        if (originalheight <= 0)
          originalheight = 10;
        try {
          cachedbitmap = Bitmap.createScaledBitmap(cachedbitmap, originalwidth, originalheight, false);
        } catch (java.lang.OutOfMemoryError e) {
          llog.d(TAG, "   getthumbnail ERROR OutOfMemoryError " + media.address);
          e.printStackTrace();
          createfakebitmap = true;
        }
      }
    }

    if (createfakebitmap) {
      if (cachedbitmap == null) {
        cachedbitmap = Bitmap.createBitmap(originalwidth, originalheight, Bitmap.Config.ARGB_8888);
        Canvas canv = new Canvas(cachedbitmap);
        RectF recti = new RectF(0, 0, originalwidth, originalheight);
        canv.drawRoundRect(recti, model.GenericTextH * 0.4f, model.GenericTextH * 0.4f, model.MiniatureBgPaint);
        prettyprint(media, canv, originalwidth, originalheight, true, model);
      } else if (cachedbitmap.isRecycled()) {
        cachedbitmap = Bitmap.createBitmap(originalwidth, originalheight, Bitmap.Config.ARGB_8888);
        Canvas canv = new Canvas(cachedbitmap);
        RectF recti = new RectF(0, 0, originalwidth, originalheight);
        canv.drawRoundRect(recti, model.GenericTextH * 0.4f, model.GenericTextH * 0.4f, model.MiniatureBgPaint);
        prettyprint(media, canv, originalwidth, originalheight, true, model);
      } else if (isAllowedToResizeThumbnail) {
        /**
         *    on a déjà une image, on normalise sa taille (taillelimite x taillelimite) avant d'écrire dessus
         */
        float taillelimitew = thumbsize;
        float taillelimiteh = thumbsize;
        float taillelimiteratio = taillelimiteh / taillelimitew;
        float ratio = ((float) originalheight) / ((float) originalwidth);
        if (ratio <= taillelimiteratio) {
          originalwidth = (int) taillelimitew;
          originalheight = (int) (taillelimitew * ratio);
        } else {
          originalheight = (int) taillelimiteh;
          originalwidth = (int) (taillelimiteh / ratio);
        }
        if (originalwidth <= 10)
          originalwidth = 100;
        if (originalheight <= 10)
          originalheight = 100;
        Bitmap tmpcachedbitmap = Bitmap.createScaledBitmap(cachedbitmap, originalwidth, originalheight, false);
        // on descend l'image on écrira en-haut
        float decalx = (taillelimitew - originalwidth) / 2.0f;
        float decaly = (taillelimiteh - originalheight);
        originalwidth = (int) taillelimitew;
        originalheight = (int) taillelimiteh;
        if (originalwidth <= 10)
          originalwidth = 100;
        if (originalheight <= 10)
          originalheight = 100;
        cachedbitmap = Bitmap.createBitmap(originalwidth, originalheight, Bitmap.Config.ARGB_8888);
        Canvas canv = new Canvas(cachedbitmap);
        if (tmpcachedbitmap != null) {
          if (!tmpcachedbitmap.isRecycled())
            canv.drawBitmap(tmpcachedbitmap, (int) decalx, (int) decaly, null);
          if (!tmpcachedbitmap.isRecycled())
            tmpcachedbitmap.recycle();
        }
        prettyprint(media, canv, originalwidth, originalheight, true, model);
      } else {
        Bitmap tmpcachedbitmap = cachedbitmap.copy(Bitmap.Config.ARGB_8888,true);
        if (tmpcachedbitmap != null) {
          if (!cachedbitmap.isRecycled())
            cachedbitmap.recycle();
          cachedbitmap = tmpcachedbitmap;
          Canvas canv = new Canvas(cachedbitmap);
          prettyprint(media, canv, originalwidth, originalheight, true, model);
        }
      }
    }
    
    if (cachedbitmap == null) {
      llog.d(TAG, "   getthumbnail ERROR cachedbitmap is null : " + media.address);
      return true;
    } else if (cachedbitmap.isRecycled()) {
      llog.d(TAG, "   getthumbnail ERROR cachedbitmap is recycled : " + media.address);
      return true;
    }

    //Bitmap mybitmap = cachedbitmap.copy(Bitmap.Config.ARGB_8888, false);
    Bitmap[] bitmaplist = new Bitmap[]{cachedbitmap};
    if (media.isOnline == Media.online_no && isAllowedToResizeThumbnail) {
      FileOutputStream fos;
      try {
        fos = new FileOutputStream(model.dossierminiature + media.addressToGetLibextractorsThumbnail + "_0");
        cachedbitmap.compress(Bitmap.CompressFormat.JPEG, 93, fos); // 100 ignored for png
        fos.write("filmstripcount=1\n".getBytes(Charsets.UTF_8));
        fos.close();
      } catch (IOException e) {
        e.printStackTrace();
      }
    }
    int success = model.setFileBitmapinmemory(media, bitmaplist, media.filmstripCount, filmstripcurrent);
    //llog.d(TAG, "   getthumbnail setfilebitmapinmemory " + success + " " + media.filmstripCount + " " + media.address + " " + filmstripcurrent);
    if (success == 0)
      return true;
    else if (success < 0) // NODBRMMOD return false;
      return true;
    else {
      if (!isAllowedToResizeThumbnail) {
        String addressToGetThumbnail;
        File mini;
        for (int i = 0; i < media.filmstripCount; i++) {
          addressToGetThumbnail = model.dossierminiature + media.addressToGetLibextractorsThumbnail + "_" + i;
          mini = new File(addressToGetThumbnail);
          if (mini.exists())
            mini.delete();
          addressToGetThumbnail = model.dossierminiature + media.addressToGetLibextractorsThumbnail + "." + i;
          mini = new File(addressToGetThumbnail);
          if (mini.exists())
            mini.delete();
        }
        llog.d(TAG, "recreate thumbnail on disk wrong size " + media.address + " " + model.dossierminiature + media.addressToGetLibextractorsThumbnail + "_0 / " + media.filmstripCount);
        media.filmstripCount = Media.filmstripNotCheckedYet;
        model.setMediaFilmstripCurrentAndCount(media, media.filmstripCount, -1);
        //return getthumbnail(media, mysurfid);
      }
      return true;
    }
    // thumbnail on disk doesn't have thumbsize dimensions
    // create new thumbnail with thumbsize dimensions
    // ici on laisse tomber il rechargera de toute façon à la bonne taille plus tard s'il n'est pas en mémoire
  }

  public static boolean getbigpicture(Media media, int mysurfid, Gallery model){
    //llog.w(TAG, "  getbigpicture " + media.address + " : " + media.printFooter);
    int thumbsize = model.thumbsize;
    Bitmap cachedbitmap = null;
    int filmstripcurrent = media.filmstripcurrent;
    boolean createfakebitmap = false;
    boolean getbitmapfrombuffer = false;
    boolean onlinepicturefaileddonotoverwriteifkeyexists = false;
    CachedBitmap cb = null;
    byte[] buffer = null;
    String filter = null;
    if (mysurfid < model.surf.size()) {
      Surf mysurf = model.surf.get(mysurfid);
      filter = mysurf.filter;
    }
    if (media.isALinkThatCreatesAnOrdner && media.addressToGetPreviewFullSize == null) {
      // c'est juste un lien on ne cherche pas à lui télécharger une image
      createfakebitmap = true;
      media.filmstripCount = Media.filmstripCreateFakeBitmap;
    } else if (media.isOnline != Media.online_no) {

      int handleThumbnailAddress = -1;
      String addressToGetThumbnail = media.addressToGetThumbnail;
      if (media.isOnline == Media.online_apache) {
        if (addressToGetThumbnail == null) { // allowed to be different
          handleThumbnailAddress = 1;
          addressToGetThumbnail = media.address + ".thmb_0";
        } else if (addressToGetThumbnail.equals(media.address)) {
          handleThumbnailAddress = 1;
          addressToGetThumbnail = media.address + ".thmb_0";
        } else if (addressToGetThumbnail.endsWith(".thmb_0")) {
          handleThumbnailAddress = 2;
        } else if (addressToGetThumbnail.endsWith(".thmb")) {
          handleThumbnailAddress = 3;
          addressToGetThumbnail += "_0";
        } // else there is no .thmb
        if (handleThumbnailAddress > 0) {
          if (media.filmstripCount == Media.filmstripNotCheckedYet) {
            buffer = InternetSession.getPicture(model, addressToGetThumbnail, media, false);
            getFilmstripCount(media, buffer, model);
            buffer = null; // on n'utilisera plus la miniature
            if (media.filmstripCount > 0)
              filmstripcurrent = (int) ((model.filmstripreftime / model.filmstripanimatetime) % (media.filmstripCount));
            model.setMediaFilmstripCurrentAndCount(media, media.filmstripCount, filmstripcurrent);
          }
          if (media.filmstripCount > 0 && media.type != Media.media_picture) { // ici on chope la grande on laisse tomber la miniature
            filmstripcurrent = (int) ((model.filmstripreftime / model.filmstripanimatetime) % (media.filmstripCount));
            int addressToGetThumbnaill = addressToGetThumbnail.length();
            addressToGetThumbnail = addressToGetThumbnail.substring(0, addressToGetThumbnaill - 2) + "." + filmstripcurrent;
            buffer = InternetSession.getPicture(model, addressToGetThumbnail, media, false);
            if (buffer == null) {
              if (media.type == Media.media_button) {
                String[] buttonaction = model.getMediaButtonAction(media);
                if (buttonaction != null) {
                  cachedbitmap = ldpcdrawdraw(buttonaction, model);
                } else {
                  onlinepicturefaileddonotoverwriteifkeyexists = true;
                }
              } else {
                onlinepicturefaileddonotoverwriteifkeyexists = true;
              }
            } else {
              if (media.type == Media.media_button) {
                String[] buttonaction = ldpccheckbuffer(buffer);
                if (buttonaction != null) {
                  model.setMediaButtonAction(media, buttonaction);
                  cachedbitmap = ldpcdrawdraw(buttonaction, model);
                }
              }
            }
          }
        }
      }
      //llog.d(TAG, " getbigpicture " + addressToGetThumbnail + " " + media.filmstripCount);
      if (cachedbitmap == null) {
        if (buffer == null) {
          if (media.type == Media.media_picture) {
            if (media.getDirectPictureAddressBypassAds) {
              String directfile = InternetSession.getDirectPictureAddressBypassAds(model, media.address, media.addressToGetThumbnail);
              buffer = InternetSession.getPicture(model, directfile, media, true);
            } else if (media.addressToGetPreviewFullSize != null) {
              buffer = InternetSession.getPicture(model, media.addressToGetPreviewFullSize, media, false);
            } else {
              buffer = InternetSession.getPicture(model, media.address, media, false);
            }
            if (buffer == null) {
              if (media.addressToGetThumbnail != null) { // on utilise la miniature si elle existe
                buffer = InternetSession.getPicture(model, media.addressToGetThumbnail, media, false);
              }
            }
          } else {
            if (media.addressToGetPreviewFullSize != null) {
              buffer = InternetSession.getPicture(model, media.addressToGetPreviewFullSize, media, false);
            } else if (media.addressToGetThumbnail != null) { // on utilise la miniature si elle existe
              buffer = InternetSession.getPicture(model, media.addressToGetThumbnail, media, false);
            }
          }
        }
        if (media.type != Media.media_picture || media.isALinkThatCreatesAnOrdner) {
          createfakebitmap = true; // on affiche en plus le texte par-dessus
        }
        if (buffer == null) {
          createfakebitmap = true;
        } else {
          getbitmapfrombuffer = true;
        }
      }
    } else {
      if (media.isInsideAnArchive) {
         if (!new File(media.ordnerAddress).exists()) {
           llog.d(TAG, "   getbigpicture archive file does not exist don't create thumb remove from database " + media.ordnerAddress);
           return false;
         }
         if (media.filmstripCount == Media.filmstripCreateFakeBitmap) {
           createfakebitmap = true;
         } else {
           buffer = unzip(media);
           if (buffer == null) {
             llog.d(TAG, "   getbigpicture failed to unzip remove from database " + media.address);
             return false;
           } else {
             getbitmapfrombuffer = true;
             media.filmstripCount = Media.filmstripNoDirectBitmap;
           }
         }
      } else if (media.type == Media.media_app) {
        int imax = model.allappinfo.size();
        AppInfo appinfo = null;
        for (int i = 0 ; i < imax ; i++) {
          appinfo = model.allappinfo.get(i);
          if (media.address.equals(appinfo.packagename)) {
            break;
          } else {
            appinfo = null;
          }
        }
        float ts = model.thumbsize * 3.0f;
        cachedbitmap = Bitmap.createBitmap((int) ts, (int) ts, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(cachedbitmap);
        if (appinfo != null) {
          if (appinfo.statuscurrent == ShellExecuter.isUNINSTALLED) {
            canvas.drawRect(0, 0, ts, ts, model.AppinfoUninstalled);
          } else if (appinfo.statuscurrent == ShellExecuter.isDISABLED) {
            canvas.drawRect(0, 0, ts, ts, model.AppinfoDisabled);
          }
          if (appinfo.servicerunningl > 0) {
            canvas.drawRect(0, 0, ts, ts, model.AppinfoActive);
          }
        }
        try {
          Drawable drawable = model.packageManager.getApplicationIcon(media.address);
          drawable.setBounds((int) (ts * 0.33f),
                  (int) (ts * 0.33f),
                  (int) (ts * 0.66f),
                  (int) (ts * 0.66f));
          drawable.draw(canvas);
        } catch (Exception e) {
          llog.d(TAG, "could not load app icon");
        }
        if (appinfo != null) {
          if (appinfo.statuscurrent == ShellExecuter.isDISABLED) {
            canvas.drawText("X", ts * 0.99f, ts * 0.99f, model.AppinfoTextPaint);
          }
          if (appinfo.servicerunningl > 0) {
            canvas.drawText("" + appinfo.servicerunningl, ts * 0.99f, ts * 0.99f, model.AppinfoTextPaint);
          }
        }
        createfakebitmap = true;
      } else if (media.type != Media.media_picture) {
        if (media.address != null && media.type != Media.media_button) {
          if (!new File(media.address).exists()) {
            llog.d(TAG, "   getbigpicture video file does not exist don't create thumb remove from database " + media.address);
            return false;
          }
        }
        if (media.filmstripCount == Media.filmstripCreateFakeBitmap) {
          llog.d(TAG, "   getbigpicture filmstripcountCreateFakeBitmap " + media.address);
          createfakebitmap = true;
        } else {
          String checkifexists;
          if (media.addressToGetThumbnail != null) {
            checkifexists = media.addressToGetThumbnail;
          } else if (media.addressToGetLibextractorsThumbnail >= 0) {
            checkifexists = model.dossierminiature + media.addressToGetLibextractorsThumbnail;
          } else if (media.addressToGetPreviewFullSize != null) {
            checkifexists = media.addressToGetPreviewFullSize;
          } else {
            checkifexists = media.address;
          }
          if (media.type == Media.media_button) {
            if (checkifexists.endsWith(".thmb_0"))
              checkifexists = checkifexists.substring(0, checkifexists.length() - 2);
            else if (!checkifexists.endsWith(".thmb"))
              checkifexists += ".thmb";
          }
          if (media.type != Media.media_button) {
            if (media.filmstripCount == Media.filmstripNotCheckedYet) {
              File miniature = new File(checkifexists + ".0");
              if (!miniature.exists()) {
                String additionalmetadata = getMediaMetadataForLibextractor(media);
                int result = Libextractor.extract(media.address, model.dossierminiature + media.addressToGetLibextractorsThumbnail, thumbsize, thumbsize, media.type == Media.media_picture, additionalmetadata);
                if (result != 0) {
                  llog.d(TAG, "   getbigpicture Libextractor failed " + result + " " + media.address);
                  createfakebitmap = true;
                  media.filmstripCount = Media.filmstripCreateFakeBitmap;
                }
              }
            }
          }
          if (media.filmstripCount == Media.filmstripCreateFakeBitmap) {
            llog.d(TAG, "   getbigpicture 2 filmstripcountCreateFakeBitmap " + media.address);
            createfakebitmap = true;
          } else {
            if (media.filmstripCount == Media.filmstripNotCheckedYet) {
              File miniature = new File(checkifexists + "_0");
              if (miniature.exists())
                getFilmstripCount(media, miniature, model);
            }
            if (media.filmstripCount == 0) { // no thumb could be created by libextractor
              media.filmstripCount = Media.filmstripCreateFakeBitmap;
              createfakebitmap = true;
            } else {
              if (media.filmstripCount > 0) { // update filmstrip
                filmstripcurrent = (int) ((model.filmstripreftime / model.filmstripanimatetime) % (media.filmstripCount));
                checkifexists += "." + filmstripcurrent;
              }
              boolean fileexists = new File(checkifexists).exists();
              if (!fileexists && media.type != Media.media_button) { // happens on drawable medias
                createfakebitmap = true;
              } else {
                if (media.type == Media.media_button) {
                  String[] buttonaction = model.getMediaButtonAction(media);
                  if (buttonaction != null) {
                    cachedbitmap = ldpcdrawdraw(buttonaction, model);
                  } else {
                    if (fileexists) {
                      buttonaction = ldpccheckbuffer(checkifexists);
                      if (buttonaction != null) {
                        model.setMediaButtonAction(media, buttonaction);
                        cachedbitmap = ldpcdrawdraw(buttonaction, model);
                      }
                    }
                  }
                } else if (model.optiondecodelibextractor || filter != null) {
                  Libextractor lib = new Libextractor();
                  if (filter != null) {
                    cachedbitmap = lib.filterrgb(checkifexists, null, 0, filter);
                  } else {
                    cachedbitmap = lib.decodergb(checkifexists, null, 0);
                  }
                } else {
                  BitmapFactory.Options options = new BitmapFactory.Options();
                  options.inPreferredConfig = Bitmap.Config.ARGB_8888;
                  cachedbitmap = BitmapFactory.decodeFile(checkifexists, options);
                }
                if (cachedbitmap == null) {
                  llog.d(TAG, "   getbigpicture " + media.address + " dont want miniature is video cachedbitmap null " + checkifexists);
                  media.filmstripCount = Media.filmstripCreateFakeBitmap;
                  createfakebitmap = true;
                }
              }
            }
          }
        }
      } else {
        if (media.filmstripCount == Media.filmstripNotCheckedYet) {
          String checkifexists = model.dossierminiature + media.addressToGetLibextractorsThumbnail;
          File miniature = new File(checkifexists + "_0");
          if (miniature.exists()) { // else wait for the thumbnail view, do not create it here
            getFilmstripCount(media, miniature, model);
            if (media.filmstripCount <= 0 && media.filmstripCount != Media.filmstripNoDirectBitmap) {
              media.filmstripCount = Media.filmstripCreateFakeBitmap;
            }
            model.setMediaFilmstripCurrentAndCount(media, media.filmstripCount, filmstripcurrent);
          }
        }
        if (!new File(media.address).exists()) {
          llog.d(TAG, "   getbigpicture other file does not exist don't create thumb remove from database " + media.address);
          return false;
        }
      }
    }

    int originalwidth = 0;
    int originalheight = 0;
    if (cachedbitmap != null) {
      originalwidth = cachedbitmap.getWidth();
      originalheight = cachedbitmap.getHeight();
    }
    int rotation = 0;
    String date = null;

    if (cachedbitmap == null || getbitmapfrombuffer) {
      /**
       *    on essaye de décoder une image qui existe
       */
      if (media.isOnline == Media.online_no && !media.isInsideAnArchive && !getbitmapfrombuffer && media.type != Media.media_button) {
        if (!new File(media.address).exists()) {
          llog.d(TAG, "   getbigpicture file does not exist remove from database " + media.address);
          return false;
        }
      }

      if (model.optiondecodelibextractor || filter != null) {
        try {
          Libextractor lib = new Libextractor();
          int bufferl = 0;
          if (getbitmapfrombuffer)
            bufferl = buffer.length;
          if (filter != null) {
            cachedbitmap = lib.filterrgb(media.address, buffer, bufferl, filter);
          } else {
            cachedbitmap = lib.decodergb(media.address, buffer, bufferl);
          }
          if (cachedbitmap != null) {
            if (!cachedbitmap.isRecycled()) {
              originalwidth = cachedbitmap.getWidth();
              originalheight = cachedbitmap.getHeight();
            }
          }
        } catch (java.lang.OutOfMemoryError e) {
          llog.d(TAG, "   getbigpicture n'a pas pu être décodé OutOfMemoryError libextractor " + media.address);
          e.printStackTrace();
          createfakebitmap = true;
        }
      } else {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inPreferredConfig = Bitmap.Config.ARGB_8888;
        if (createfakebitmap) {
          options.inMutable = true;
        }
        int bufferl = 0;
        try {
          if (getbitmapfrombuffer) {
            bufferl = buffer.length;
            cachedbitmap = BitmapFactory.decodeByteArray(buffer, 0, bufferl, options);
          } else {
            cachedbitmap = BitmapFactory.decodeFile(media.address, options);
          }
        } catch (java.lang.OutOfMemoryError e) {
          llog.d(TAG, "   getbigpicture n'a pas pu être décodé OutOfMemoryError buffer " + media.address);
          e.printStackTrace();
          createfakebitmap = true;
        }
        originalwidth = options.outWidth;
        originalheight = options.outHeight;
        if (originalwidth <= 0 || originalheight <= 0) {
          llog.d(TAG, "   getbigpicture bufferl " + bufferl + " originalwidth " + originalwidth + " " + originalheight + " createfakebitmap " + createfakebitmap + " getbitmapfrombuffer " + getbitmapfrombuffer + " on supprime " + media.address); //+ " mais on ne delete pas (que si on ouvre la grande et que la grande merde)");
          createfakebitmap = true;
        }
      }
      if (cachedbitmap == null) {
        llog.d(TAG, "   getbigpicture n'a pas pu être décodé " + media.address);
        createfakebitmap = true;
      } else {
        boolean foundexif = false;
        try {
          if (getbitmapfrombuffer) {
            int bufferl = buffer.length;
            for (int i = 0 ; i < 20  && i < bufferl - 4 ; i++) {
              if (buffer[i] == (byte) 'E') {
                if (buffer[i+1] == (byte) 'x') {
                  if (buffer[i+2] == (byte) 'i') {
                    if (buffer[i+3] == (byte) 'f') {
                      foundexif = true;
                      break;
                    }
                  }
                }
              }
            }
            if (foundexif) {
              ExifInterface exif = new ExifInterface(new ByteArrayInputStream(buffer));
              rotation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);
              date = exif.getAttribute(ExifInterface.TAG_DATETIME);
            }
          } else {
            FileInputStream f = new FileInputStream(media.address);
            byte[] data = new byte[20];
            int readl = f.read(data, 0, 20);
            f.close();
            for (int i = 0 ; i < 20 && i < readl - 4 ; i++) {
              if (data[i] == (byte) 'E') {
                if (data[i+1] == (byte) 'x') {
                  if (data[i+2] == (byte) 'i') {
                    if (data[i+3] == (byte) 'f') {
                      foundexif = true;
                      break;
                    }
                  }
                }
              }
            }
            if (foundexif) {
              ExifInterface exif = new ExifInterface(media.address);
              rotation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);
              date = exif.getAttribute(ExifInterface.TAG_DATETIME);
            }
          }
          if (foundexif) {
            if (rotation == ExifInterface.ORIENTATION_ROTATE_90)
              rotation = 90;
            else if (rotation == ExifInterface.ORIENTATION_ROTATE_180)
              rotation = 180;
            else if (rotation == ExifInterface.ORIENTATION_ROTATE_270)
              rotation = 270;
            else
              rotation = 0;
            if (date != null || rotation != 0) {
              media.metadatacreationtime = date;
              media.metadatawidth = originalwidth;
              media.metadataheight = originalheight;
              media.metadatarotation = rotation;
              model.setMediaMetadata(media);
            }
          }
          //llog.d(TAG, "exifrotation : " + rotation + " date " + date);
        } catch (Exception e) {
          llog.d(TAG, "   getbigpicture n'a pas pu être décodé rotation");
        }
      }
    }

    if (cachedbitmap != null) {
      originalwidth = cachedbitmap.getWidth();
      originalheight = cachedbitmap.getHeight();
    }

    if (originalwidth <= 0 || originalheight <= 0) {
      if (model.bigScreenHeight < model.bigScreenWidth) {
        originalheight = (int) (model.zoomconfortratio * (float) model.bigScreenHeight);
        originalwidth = (int) (originalheight * 4.0f / 3.0f);
      } else {
        originalheight = (int) (model.zoomconfortratio * (float) model.bigScreenWidth);
        originalwidth = (int) (originalheight * 3.0f / 4.0f);
      }
      if (originalwidth <= 0 || originalheight <= 0) {
        originalwidth = thumbsize;
        originalheight = originalwidth;
      }
      createfakebitmap = true;
    }

    /**
     *   rotations et filtres
     */
    if (rotation != 0 && cachedbitmap != null) {
      int cachedbitmapwidth = originalwidth;
      int cachedbitmapheight = originalheight;
      if (rotation == 270 || rotation == 90) {
        int ooriginalwidth = originalwidth;
        originalwidth = originalheight;
        originalheight = ooriginalwidth;
      }
      Matrix matrix = new Matrix();
      matrix.preRotate(rotation);
      cachedbitmap = Bitmap.createBitmap(cachedbitmap, 0, 0, cachedbitmapwidth, cachedbitmapheight, matrix, false);
    }

    if (createfakebitmap) {
      if (cachedbitmap == null) {
        cachedbitmap = Bitmap.createBitmap(originalwidth, originalheight, Bitmap.Config.ARGB_8888);
        Canvas canv = new Canvas(cachedbitmap);
        RectF recti = new RectF(0, 0, originalwidth, originalheight);
        canv.drawRoundRect(recti, model.GenericTextH * 0.4f, model.GenericTextH * 0.4f, model.MiniatureBgPaint);
        prettyprint(media, canv, originalwidth, originalheight, false, model);
      } else if (cachedbitmap.isRecycled()) {
        cachedbitmap = Bitmap.createBitmap(originalwidth, originalheight, Bitmap.Config.ARGB_8888);
        Canvas canv = new Canvas(cachedbitmap);
        RectF recti = new RectF(0, 0, originalwidth, originalheight);
        canv.drawRoundRect(recti, model.GenericTextH * 0.4f, model.GenericTextH * 0.4f, model.MiniatureBgPaint);
        prettyprint(media, canv, originalwidth, originalheight, false, model);
      } else {
        /**
         *    on a déjà une image, on normalise sa taille (taillelimite x taillelimite) avant d'écrire dessus
         */
        if (model.isandroidtv) {
          float taillelimitew;
          float taillelimiteh;
          float ratio = ((float) originalheight) / ((float) originalwidth);
          if (model.bigScreenWidth > model.bigScreenHeight) {
            taillelimiteh = (model.zoomconfortratio * (float) model.bigScreenHeight);
            taillelimitew = taillelimiteh / ratio;
          } else {
            taillelimitew = (model.zoomconfortratio * (float) model.bigScreenWidth);
            taillelimiteh = taillelimitew * ratio;
          }
          originalwidth = (int) taillelimitew;
          originalheight = (int) taillelimiteh;
          if (originalwidth <= 10)
            originalwidth = 100;
          if (originalheight <= 10)
            originalheight = 100;

          float decalx = 0.0f;
          float decaly = 0.0f;
          float ratiomin;
          float ratiomax;
          if (model.bigScreenWidth > model.bigScreenHeight) {
            ratiomin = (model.zoomconfortratio * model.bigScreenHeight) / (0.66f * model.bigScreenWidth);
            if (ratio < ratiomin) {
              decaly = originalheight * ratiomin / ratio - originalheight;
            }
          } else {
            ratiomax = (model.zoomconfortratio * model.bigScreenWidth) / (0.20f * model.bigScreenHeight);
            if (ratio > ratiomax) {
              decalx = originalwidth * ratio / ratiomax - originalwidth;
            }
          }
          if (media.printDetails != null) {
            decaly = originalheight * 0.37f;
          }

          Bitmap tmpcachedbitmap = Bitmap.createScaledBitmap(cachedbitmap, originalwidth, originalheight, false);
          if (!cachedbitmap.isRecycled())
            cachedbitmap.recycle();
          originalwidth += decalx;
          originalheight += decaly;
          cachedbitmap = Bitmap.createBitmap(originalwidth, originalheight, Bitmap.Config.ARGB_8888);
          Canvas canv = new Canvas(cachedbitmap);
          if (tmpcachedbitmap != null) {
            if (!tmpcachedbitmap.isRecycled())
              canv.drawBitmap(tmpcachedbitmap, decalx * 0.5f, decaly, null);
            if (!tmpcachedbitmap.isRecycled())
              tmpcachedbitmap.recycle();
          }
          //long t = System.currentTimeMillis();
          prettyprint(media, canv, originalwidth, originalheight, false, model);
          //llog.d(TAG, " deltat " + (System.currentTimeMillis() - t));
        } else {
          Bitmap tmpcachedbitmap = cachedbitmap.copy(Bitmap.Config.ARGB_8888,true);
          if (tmpcachedbitmap != null) {
            if (!cachedbitmap.isRecycled())
              cachedbitmap.recycle();
            cachedbitmap = tmpcachedbitmap;
            Canvas canv = new Canvas(cachedbitmap);
            prettyprint(media, canv, originalwidth, originalheight, false, model);
          }
        }
      }
    }

    if (cachedbitmap == null) {
      llog.d(TAG, "   getbigpicture ERROR cachedbitmap is null : " + media.address);
      return true;
    } else if (cachedbitmap.isRecycled()) {
      llog.d(TAG, "   getbigpicture ERROR cachedbitmap is recycled : " + media.address);
      return true;
    }

    if (mysurfid < model.surf.size())
      model.surf.get(mysurfid).addtocache(media.address, originalwidth, originalheight, filmstripcurrent, media.filmstripCount, cachedbitmap, onlinepicturefaileddonotoverwriteifkeyexists);
    //llog.d(TAG, "   getbigpicture " + media.address  + " : " + media.printFooter + " end " + filmstripcurrent + "/" + media.filmstripCount);
    return true;
  }
  
  private void checkthumbnail(Media media){
    File file = new File(media.address);
    int filel = (int)file.length();
    llog.d(TAG, "file length = "+filel);
    if (filel > 70000) {
      filel = 70000;
    }
    byte[] fileData = new byte[filel];
    DataInputStream dis;
    try {
      dis = new DataInputStream(new FileInputStream(file));
      dis.readFully(fileData);
      dis.close();
    } catch (IOException e) {
      e.printStackTrace();
    }
    for (int i = 0 ; i < filel-1 ; i++) {
      if (fileData[i] == (byte)0xff) {
        if (fileData[i+1] == (byte)0xd8) {
          llog.d(TAG, " <- ffd8 start en "+i);
        } else if (fileData[i+1] == (byte)0xd9) {
          llog.d(TAG, " -> ffd9 end en "+i);
        } else if (fileData[i+1] == (byte)0xdb) {
          llog.d(TAG, "ffdb en "+i);
        } else if (fileData[i+1] == (byte)0xee) {
          llog.d(TAG, "ffee en "+i);
        } else if (fileData[i+1] == (byte)0xe0) {
          llog.d(TAG, "ffe0 en "+i);
        } else if (fileData[i+1] == (byte)0xe1) {
          llog.d(TAG, "ffe1 en "+i);
        }
      } else if (fileData[i] == (byte)0x4a) {
        if (fileData[i + 1] == (byte) 0x46) {
          if (fileData[i + 1] == (byte) 0x49) {
            if (fileData[i + 1] == (byte) 0x46) {
              llog.d(TAG, "JFIF en " + i);
            }
          }
        }
      } else if (fileData[i] == (byte)0x45) {
        if (fileData[i + 1] == (byte) 0x78) {
          if (fileData[i + 1] == (byte) 0x69) {
            if (fileData[i + 1] == (byte) 0x66) {
              llog.d(TAG, "Exif en " + i);
            }
          }
        }
      }
    }
  }
  
  public static byte[] unzip(Media media) {
    byte[] buffer = null;
    ZipInputStream zis = null;
    try {
      zis = new ZipInputStream(new BufferedInputStream(new FileInputStream(media.ordnerAddress)));
    } catch (FileNotFoundException e) {
      e.printStackTrace();
    }
    if (zis == null) {
      llog.d(TAG, "zis == null "+media.address);
      return buffer;
    }
    int cherchenompos = media.ordnerAddress.length()+1;
    String cherchecenom = media.address.substring(cherchenompos);
    ZipEntry ze = null;
    while (true) {
      try {
        ze = zis.getNextEntry();
      } catch (IOException e) {
        e.printStackTrace();
      }
      if (ze == null) {
        llog.d(TAG, "ze == null "+media.address);
        break;
      }
      if (ze.isDirectory()) {
        continue;
      }
      if (cherchecenom.equals(ze.getName())) {
        int bufferl = (int) ze.getSize();
        //llog.d(TAG, " size "+bufferl);
  
        ByteArrayOutputStream babuffer = new ByteArrayOutputStream();
        int nRead;
        int datal = 1024;
        byte[] data = new byte[datal];
        try {
          while ((nRead = zis.read(data, 0, datal)) != -1) {
            babuffer.write(data, 0, nRead);
          }
          babuffer.flush();
          buffer = babuffer.toByteArray();
        } catch (IOException e) {
          e.printStackTrace();
        }
        /* if time should be restored as well
        long time = ze.getTime();
        if (time > 0)
            file.setLastModified(time);
        */
        break;
      }
    }
    try {
      zis.close();
    } catch (IOException e) {
      e.printStackTrace();
    }
    return buffer;
  }

  private static int calculateInSampleSize(int srcWidth, int srcHeight, int reqWidth, int reqHeight) {
    int inSampleSize = 1;
    if (srcHeight > reqHeight || srcWidth > reqWidth) {
      final int halfHeight = srcHeight / 2;
      final int halfWidth = srcWidth / 2;
      // Calculate the largest inSampleSize value that is a power of 2 and keeps both
      // ScreenHeight and ScreenWidth larger than the requested ScreenHeight and ScreenWidth.
      while ((halfHeight / inSampleSize) >= reqHeight && (halfWidth / inSampleSize) >= reqWidth) {
        inSampleSize *= 2;
      }
    }
    return inSampleSize;
  }

  private volatile int deleterecursivecounter = 0;
  private volatile long deleterecursivetimer = System.currentTimeMillis();
  void deleteminiatures(File[] children, boolean silent) {
    if (children != null) {
      long newtime = System.currentTimeMillis();
      for (File child : children) {
        if (child != null) {
          if (child.delete())
            deleterecursivecounter += 1;
          if (!silent) {
            newtime = System.currentTimeMillis();
            if (newtime - deleterecursivetimer > 1000) {
              deleterecursivetimer = newtime;
              model.message(deleterecursivecounter + " thumbs removed");
            }
          }
        }
      }
    }
  }

  private static boolean characcepted(byte b) {
    char c = (char) (b & 0xff);
    if (c == '\n' || ('a' <= c  && c <= 'z') || ('A' <= c  && c <= 'Z') || c == '=')
      return true;
    else
      return false;
  }

  private static Media getFilmstripCount(Media media, byte[] buffer, Gallery model){
    media.filmstripCount = Media.filmstripCreateFakeBitmap;
    if (buffer != null) {

      int bufferl = buffer.length;
      if (bufferl < 1) {
        llog.d(TAG, "getFilmstripCount " + media.address + " too small " + bufferl);
        return media;
      }
      if (bufferl > 200 * 1024000) {
        llog.d(TAG, "getFilmstripCount " + media.address + " too big " + bufferl);
        return media;
      }

      int found = 0; // commence à 0 si thmb sans aucun jpg
      for (int i = bufferl - 1 ; i > 0 ; i--) { // ff d9
        if ((buffer[i - 1] & 0xff) == 0xff && (buffer[i] & 0xff) == 0xd9) {
          found = i + 1;
          break;
        }
      }
      //if (found > 0)
      //  media.filmstripCount = Media.filmstripThumbnailFound; // old format can be ignored
      if (found >= bufferl - 6)
        return media;

      int i = found;
      if (       !characcepted(buffer[i])
              || !characcepted(buffer[i + 1])
              || !characcepted(buffer[i + 2])
              || !characcepted(buffer[i + 3])
              || !characcepted(buffer[i + 4])
              || !characcepted(buffer[i + 5])
              || !characcepted(buffer[i + 6])
      ) {
        return media;
      }
      //llog.d(TAG, "getFilmstripCount " + media.address + " found at pos " + found + " / " + bufferl);


      try {
        char c;
        StringBuilder k = new StringBuilder();
        byte[] buf = null;
        String v = null;
        int vi = 0;
        int vf = 0;
        boolean iskey = true;
        while (i < bufferl) {
          c = (char) (buffer[i] & 0xff); // nécessaire sinon bar est signé
          if (c == '\n' || i == bufferl - 1) {
            if (c != '\n') {
              //v.append(c);
              vf = i + 1;
            } else {
              vf = i;
            }
            String kk = k.toString().toLowerCase();
            if (kk.length() > 1) {
              int vl = vf - vi;
              buf = new byte[vl];
              for (int j = 0; j < vl; j++)
                buf[j] = buffer[vi + j];
              v = new String(buf, StandardCharsets.UTF_8);
              //llog.d(TAG, "<" + kk + ">=<" + new String(buf, StandardCharsets.UTF_8) + ">");
              switch (kk) {
                case "filmstripcount":
                  media.filmstripCount = Integer.parseInt(v.toString());
                  break;
                case "width":
                  media.metadatawidth = Integer.parseInt(v.toString());
                  break;
                case "height":
                  media.metadataheight = Integer.parseInt(v.toString());
                  break;
                case "rotation": // -90 -180 90
                  media.metadatarotation = Integer.parseInt(v.toString());
                  break;
                case "audio": // not set if == 1
                  media.metadataaudio = Integer.parseInt(v.toString());
                  break;
                case "subtitle": // not set if == 0
                  media.metadatasubtitle = Integer.parseInt(v.toString());
                  break;
                case "creationtime": // 2023-11-02T21:24:07.000000Z ou - ou /
                  media.metadatacreationtime = v.toString();
                  break;
                case "maxwidth":
                  media.metadatamaxwidth = Integer.parseInt(v.toString());
                  break;
                case "maxheight":
                  media.metadatamaxheight = Integer.parseInt(v.toString());
                  break;
                case "duration":
                  media.metadataduration = Long.parseLong(v.toString());
                  break;

                case "playinsequence":
                  media.playInSequence = Boolean.parseBoolean(v.toString());
                  break;
                case "playstartatposition":
                  media.playStartAtPosition = Integer.parseInt(v.toString());
                  break;
                case "subtitleaddress": {
                  if (media.subtitleAddress == null)
                    media.subtitleAddress = new ArrayList<>();
                  media.subtitleAddress.add(v.toString());
                }
                break;
                case "subtitleaddressencode": {
                  if (media.subtitleAddress == null)
                    media.subtitleAddress = new ArrayList<>();
                  media.subtitleAddress.add(Gallery.urlEncode(v.toString()));
                }
                break;
                case "printname":
                  media.printName =  v.toString();
                  break;
                case "printdetails":
                  media.printDetails = v.toString();
                  break;
                case "printfooter":
                  media.printFooter = v.toString();
                  break;

                default:
                  llog.d(TAG, "unknown key <" + k + ">=<" + v + "> " + media.address);
                  break;
              }
            }
            //llog.d(TAG, "key " + k + "=" + v + " " + media.address);
            k.delete(0, k.length());
            iskey = true;
          } else if (c == '=') {
            if (iskey) {
              iskey = false;
              vi = i + 1;
              vf = i + 1;
            }
          } else {
            if (iskey) {
              if (k.length() < 128)
                k.append(c);
              else
                break;
            }
          }
          i++;
        }
      } catch (Exception e) {
        llog.d(TAG, "getFilmstripCount " + media.address + " err " + e);
      }
      if (media.filmstripCount > 0)
        model.setMediaMetadata(media);
    }
    return media;
  }

  private static Media getFilmstripCount(Media media, File file, Gallery model){
    media.filmstripCount = Media.filmstripCreateFakeBitmap;
    if (file.exists()) {
      final long filesize = file.length();
      if (filesize < 1) {
        llog.d(TAG, "getFilmstripCounta too small " + media.address + " " + file);
        return media;
      }
      if (filesize > 200 * 1024000) {
        llog.d(TAG, "getFilmstripCounta too big " + media.address + " " + file);
        return media;
      }
      try {
        FileInputStream fr = new FileInputStream(file);
        int fsize = (int) filesize;
        byte[] buffer = new byte[fsize];
        int read = fr.read(buffer);
        if (read != fsize)
          llog.d(TAG, "getFilmstripCounta error read only " + read + " / " + fsize + " " + media.address);
        fr.close();
        return getFilmstripCount(media, buffer, model);
      } catch (Exception e) {
        llog.d(TAG, "getFilmstripCounta " + media.address + " err " + e);
        return media;
      }
    }
    return media;
  }

  private static String[] ldpccheckbuffer(String filename) {
    if (filename != null) {
      try {
        FileReader fr = new FileReader(filename);
        BufferedReader in = new BufferedReader(fr);
        String line;
        ArrayList<String> buttonaction = new ArrayList<>();
        boolean start = false;
        while ((line = in.readLine()) != null) {
          if (line.length() > 0) {
            if (line.contains("LDPCDRAW")) {
              start = true;
            } else if (start) {
              buttonaction.add(line);
            }
          }
        }
        in.close();
        fr.close();
        int buttonactionl = buttonaction.size();
        if (buttonactionl > 0) {
          return buttonaction.toArray(new String[buttonactionl]);
        }
      } catch (Exception e) {
        llog.d(TAG, "error reading file " + filename);
      }
    }
    return null;
  }

  private static String[] ldpccheckbuffer(byte[] buffer) {
    if (buffer != null) {
      if (buffer.length > 8) {
        if (
            (char) (buffer[0] & 0xff) == 'L'
                && (char) (buffer[1] & 0xff) == 'D'
                && (char) (buffer[2] & 0xff) == 'P'
                && (char) (buffer[3] & 0xff) == 'C'
                && (char) (buffer[4] & 0xff) == 'D'
                && (char) (buffer[5] & 0xff) == 'R'
                && (char) (buffer[6] & 0xff) == 'A'
                && (char) (buffer[7] & 0xff) == 'W'
        ) {
          String str;
          try {
            str = new String(buffer, StandardCharsets.UTF_8);
            String[] line = str.split("\n");
            return line;
          } catch(Exception e){
            llog.d(TAG, "ldpcdraw error " + e);
          }
        }
      }
    }
    return null;
  }

  private static Bitmap ldpcdrawdraw(String[] line, Gallery model) {
    Bitmap bitmap = null;
    if (line != null) {
      int linel = line.length;
      float w = 300f;
      float h = 300f;
      float x = w;
      if (h > x)
        x = h;
      Canvas canvas = null;
      Pattern findpattern;
      Matcher matcher;
      Paint paint = new Paint();
      paint.setAntiAlias(true);
      Date date = new Date();
      Calendar calendar = Calendar.getInstance();
      calendar.setTime(date);
      for (int i = 0; i < linel; i++) {
        if (line[i].contains("wh=")) {
          findpattern = Pattern.compile("^\\w*wh= w (\\d+) h (\\d+)");
          matcher = findpattern.matcher(line[i]);
          if (matcher.find()) {
            w = Integer.parseInt(matcher.group(1));
            h = Integer.parseInt(matcher.group(2));
            x = w;
            if (h > x)
              x = h;
            bitmap = Bitmap.createBitmap((int) w, (int) h, Bitmap.Config.ARGB_8888);
            canvas = new Canvas(bitmap);
          }
        } else if (canvas != null) {
          if (line[i].contains("drawcircle=")) {
            findpattern = Pattern.compile("^\\w*drawcircle= r ([\\d.]+) g ([\\d.]+) b ([\\d.]+) s ([\\d.]+) x ([\\d.]+) y ([\\d.]+) r ([\\d.]+)");
            matcher = findpattern.matcher(line[i]);
            if (matcher.find()) {
              paint.setColor(Color.rgb(
                  (int) (Float.parseFloat(matcher.group(1)) * 255f),
                  (int) (Float.parseFloat(matcher.group(2)) * 255f),
                  (int) (Float.parseFloat(matcher.group(3)) * 255f)
              ));
              float swidth = h * Float.parseFloat(matcher.group(4));
              if (swidth > 0) {
                paint.setStyle(Paint.Style.STROKE);
                paint.setStrokeWidth(swidth);
              } else {
                paint.setStyle(Paint.Style.FILL);
                paint.setStrokeWidth(1.0f);
              }
              canvas.drawCircle(w * Float.parseFloat(matcher.group(5)),
                  h * Float.parseFloat(matcher.group(6)),
                  h * Float.parseFloat(matcher.group(7)),
                  paint);
            }
          } else if (line[i].contains("drawpicture=")) {
            findpattern = Pattern.compile("^\\w*drawpicture= xi ([\\d.]+) xf ([\\d.]+) yi ([\\d.]+) yf ([\\d.]+) b (.+)");
            matcher = findpattern.matcher(line[i]);
            if (matcher.find()) {
              String b64 = matcher.group(5);
              byte[] decodedString = Base64.decode(b64, Base64.DEFAULT);
              Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
              float xi = w * Float.parseFloat(matcher.group(1));
              float xf = w * Float.parseFloat(matcher.group(2));
              float yi = h * Float.parseFloat(matcher.group(3));
              float yf = h * Float.parseFloat(matcher.group(4));
              Bitmap decodedByteS = Bitmap.createScaledBitmap(decodedByte, (int) (xf - xi), (int) (yf - yi), false);
              canvas.drawBitmap(decodedByteS, xi, yi, paint);
              decodedByteS.recycle();
              decodedByteS = null;
              decodedByte.recycle();
              decodedByte = null;
              decodedString = null;
            }
          } else if (line[i].contains("drawrect=")) {
            findpattern = Pattern.compile("^\\w*drawrect= r ([\\d.]+) g ([\\d.]+) b ([\\d.]+) s ([\\d.]+) xi ([\\d.]+) xf ([\\d.]+) yi ([\\d.]+) yf ([\\d.]+)");
            matcher = findpattern.matcher(line[i]);
            if (matcher.find()) {
              paint.setColor(Color.rgb(
                  (int) (Float.parseFloat(matcher.group(1)) * 255f),
                  (int) (Float.parseFloat(matcher.group(2)) * 255f),
                  (int) (Float.parseFloat(matcher.group(3)) * 255f)
              ));
              float swidth = h * Float.parseFloat(matcher.group(4));
              if (swidth > 0) {
                paint.setStyle(Paint.Style.STROKE);
                paint.setStrokeWidth(swidth);
              } else {
                paint.setStyle(Paint.Style.FILL);
                paint.setStrokeWidth(1.0f);
              }
              canvas.drawRect(w * Float.parseFloat(matcher.group(5)),
                  h * Float.parseFloat(matcher.group(7)),
                  w * Float.parseFloat(matcher.group(6)),
                  h * Float.parseFloat(matcher.group(8)),
                  paint);
            }
          } else if (line[i].contains("drawarc=")) {
            findpattern = Pattern.compile("^\\w*drawarc= r ([\\d.]+) g ([\\d.]+) b ([\\d.]+) s ([\\d.]+) xi ([\\d.]+) xf ([\\d.]+) yi ([\\d.]+) yf ([\\d.]+) ai ([\\d.]+) af ([\\d.]+)");
            matcher = findpattern.matcher(line[i]);
            if (matcher.find()) {
              paint.setColor(Color.rgb(
                  (int) (Float.parseFloat(matcher.group(1)) * 255f),
                  (int) (Float.parseFloat(matcher.group(2)) * 255f),
                  (int) (Float.parseFloat(matcher.group(3)) * 255f)
              ));
              float swidth = h * Float.parseFloat(matcher.group(4));
              if (swidth > 0) {
                paint.setStyle(Paint.Style.STROKE);
                paint.setStrokeWidth(swidth);
              } else {
                paint.setStyle(Paint.Style.FILL);
                paint.setStrokeWidth(1.0f);
              }
              canvas.drawArc(w * Float.parseFloat(matcher.group(5)),
                  h * Float.parseFloat(matcher.group(7)),
                  w * Float.parseFloat(matcher.group(6)),
                  h * Float.parseFloat(matcher.group(8)),
                  Float.parseFloat(matcher.group(9)),
                  Float.parseFloat(matcher.group(10)),
                  false,
                  paint);
            }
          } else if (line[i].contains("drawtarc=")) {
            findpattern = Pattern.compile("^\\w*drawtarc= r ([\\d.]+) g ([\\d.]+) b ([\\d.]+) s ([\\d.]+) xi ([\\d.]+) xf ([\\d.]+) yi ([\\d.]+) yf ([\\d.]+) a ([\\d.]+) t (\\d+) c ([yn])");
            matcher = findpattern.matcher(line[i]);
            if (matcher.find()) {
              paint.setColor(Color.rgb(
                  (int) (Float.parseFloat(matcher.group(1)) * 255f),
                  (int) (Float.parseFloat(matcher.group(2)) * 255f),
                  (int) (Float.parseFloat(matcher.group(3)) * 255f)
              ));
              float delta = Float.parseFloat(matcher.group(9)) * 0.5f;
              int type = Integer.parseInt(matcher.group(10));
              float sec = 0.0f;
              float sweep2 = 0.0f;
              float sweep = delta;
              float dmax = 0.0f;
              float tmax = 0.0f;
              int dw;
              switch (type) {
                case Calendar.SECOND:
                  sec = (calendar.get(Calendar.SECOND) * 360.0f) / 60.0f - 90.0f - delta;
                  sweep = delta * 2.0f;
                  break;
                case Calendar.MINUTE:
                  sec = ((calendar.get(Calendar.MINUTE) + calendar.get(Calendar.SECOND) / 60.0f) * 360.0f) / 60.0f - 90.0f - delta;
                  sweep = delta * 2.0f;
                  break;
                case Calendar.HOUR:
                  sec = ((calendar.get(Calendar.HOUR) + calendar.get(Calendar.MINUTE) / 60.0f + calendar.get(Calendar.SECOND) / 3600.0f) * 360.0f) / 12.0f - 90.0f - delta;
                  sweep = delta * 2.0f;
                  break;
                case Calendar.DAY_OF_WEEK:
                  dw = calendar.get(Calendar.DAY_OF_WEEK);
                  dw = (dw + 7 - 2) % 7;
                  sec = (dw * 360.0f) / 7.0f - 90.0f;
                  tmax = (360.0f) / 7.0f;
                  sweep = ((calendar.get(Calendar.HOUR_OF_DAY) + calendar.get(Calendar.MINUTE) / 60.0f) * tmax) / 24.0f;
                  dw = calendar.get(Calendar.DAY_OF_WEEK);
                  dw = (dw + 7 - 2) % 7 + 1;
                  sweep2 = (dw * 360.0f) / 7.0f - 90.0f - sec;
                  break;
                case Calendar.DAY_OF_MONTH:
                  dmax = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
                  sec = ((calendar.get(Calendar.DAY_OF_MONTH) - 1) * 360.0f) / dmax - 90.0f;
                  tmax = (360.0f) / dmax;
                  //sweep = ((calendar.get(Calendar.HOUR_OF_DAY) + calendar.get(Calendar.MINUTE) / 60.0f) * tmax) / 24.0f;
                  sweep = ((calendar.get(Calendar.DAY_OF_MONTH)) * 360.0f) / dmax - 90.0f - sec;
                  break;
                case Calendar.MONTH:
                  dmax = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
                  sec = (calendar.get(Calendar.MONTH) * 360.0f) / 12.0f - 90.0f;
                  tmax = (360.0f) / 12.0f;
                  sweep = (calendar.get(Calendar.DAY_OF_MONTH) * tmax) / dmax;
                  sweep2 = ((calendar.get(Calendar.MONTH) + 1.0f) * 360.0f) / 12.0f - 90.0f - sec;
                  break;
              }
              if (sweep < delta)
                sweep = delta;
              boolean usecenter = false;
              if (matcher.group(11).equals("y"))
                usecenter = true;
              paint.setStyle(Paint.Style.STROKE);
              if (type == Calendar.DAY_OF_WEEK || type == Calendar.MONTH) {
                //paint.setStrokeWidth(h * Float.parseFloat(matcher.group(4)) * 0.33f);
                paint.setAlpha((int) (255.0f * 0.50f));
                canvas.drawArc(w * Float.parseFloat(matcher.group(5)),
                        h * Float.parseFloat(matcher.group(7)),
                        w * Float.parseFloat(matcher.group(6)),
                        h * Float.parseFloat(matcher.group(8)),
                        sec,
                        sweep2,
                        usecenter,
                        paint);
                paint.setAlpha(255);
              }
              paint.setStrokeWidth(h * Float.parseFloat(matcher.group(4)));
              canvas.drawArc(w * Float.parseFloat(matcher.group(5)),
                  h * Float.parseFloat(matcher.group(7)),
                  w * Float.parseFloat(matcher.group(6)),
                  h * Float.parseFloat(matcher.group(8)),
                  sec,
                  sweep,
                  usecenter,
                  paint);
            }
          } else if (line[i].contains("drawtext=")) {
            findpattern = Pattern.compile("^\\w*drawtext= r ([\\d.]+) g ([\\d.]+) b ([\\d.]+) s ([\\d.]+) ([lrc]) x ([\\d.]+) y ([\\d.]+) txt (.+)");
            matcher = findpattern.matcher(line[i]);
            if (matcher.find()) {
              paint.setColor(Color.rgb(
                  (int) (Float.parseFloat(matcher.group(1)) * 255f),
                  (int) (Float.parseFloat(matcher.group(2)) * 255f),
                  (int) (Float.parseFloat(matcher.group(3)) * 255f)
              ));
              paint.setStyle(Paint.Style.FILL);
              paint.setTextSize(h * Float.parseFloat(matcher.group(4)));
              if (matcher.group(5).equals("l"))
                paint.setTextAlign(Paint.Align.LEFT);
              else if (matcher.group(5).equals("r"))
                paint.setTextAlign(Paint.Align.RIGHT);
              else if (matcher.group(5).equals("c"))
                paint.setTextAlign(Paint.Align.CENTER);
              canvas.drawText(matcher.group(8), w * Float.parseFloat(matcher.group(6)), h * Float.parseFloat(matcher.group(7)), paint);
            }
          } else if (line[i].contains("dateformat=")) {
            findpattern = Pattern.compile("^\\w*dateformat= r ([\\d.]+) g ([\\d.]+) b ([\\d.]+) s ([\\d.]+) ([lrc]) x ([\\d.]+) y ([\\d.]+) txt (.+)");
            matcher = findpattern.matcher(line[i]);
            if (matcher.find()) {
              paint.setColor(Color.rgb(
                  (int) (Float.parseFloat(matcher.group(1)) * 255f),
                  (int) (Float.parseFloat(matcher.group(2)) * 255f),
                  (int) (Float.parseFloat(matcher.group(3)) * 255f)
              ));
              paint.setStyle(Paint.Style.FILL);
              paint.setTextSize(h * Float.parseFloat(matcher.group(4)));
              if (matcher.group(5).equals("l"))
                paint.setTextAlign(Paint.Align.LEFT);
              else if (matcher.group(5).equals("r"))
                paint.setTextAlign(Paint.Align.RIGHT);
              else if (matcher.group(5).equals("c"))
                paint.setTextAlign(Paint.Align.CENTER);
              SimpleDateFormat simp = new SimpleDateFormat(matcher.group(8), Locale.getDefault());
              canvas.drawText(simp.format(date), w * Float.parseFloat(matcher.group(6)), h * Float.parseFloat(matcher.group(7)), paint);
            }
          } else if (line[i].contains("monitor=")) {
            if (model != null) {
              findpattern = Pattern.compile("^\\w*monitor= t (\\d+)");
              matcher = findpattern.matcher(line[i]);
              if (matcher.find()) {
                int maxtime = Integer.parseInt(matcher.group(1));
                bitmap = WidgetMonitor.drawgraph(model.internalStorageDir, w, h, maxtime, -1);
              }
            }
          } else if (line[i].contains("countdown=")) {
            if (model != null) {
              findpattern = Pattern.compile("^\\w*countdown= j (\\d+) m (\\d+) a (\\d+) t (\\d+) txt (.+)");
              matcher = findpattern.matcher(line[i]);
              if (matcher.find()) {
                int j = Integer.parseInt(matcher.group(1));
                int m = Integer.parseInt(matcher.group(2));
                int a = Integer.parseInt(matcher.group(3));
                int ttt = Integer.parseInt(matcher.group(4));
                bitmap = WidgetCalendarCountdown.drawCountdown(model.unmillimetre, j, m, a, matcher.group(5), ttt, (int) w, (int) h);
              }
            }
          } else if (line[i].contains("calendar=")) {
            if (model != null) {
              findpattern = Pattern.compile("^\\w*calendar= t (\\d+) cd (.*)");
              matcher = findpattern.matcher(line[i]);
              if (matcher.find()) {
                int maxtime = Integer.parseInt(matcher.group(1));
                String cds = matcher.group(2);
                String[] datez = null;
                if (cds.length() >= 4)
                  datez = cds.split("\\|");
                bitmap = WidgetCalendarCountdown.drawCalendar(maxtime, datez, (int) w, (int) h);
              }
            }
          } else if (line[i].contains("runcmd=")) {
            findpattern = Pattern.compile("^\\w*runcmd= cmd (.+?) r ([\\d.]+) g ([\\d.]+) b ([\\d.]+) s ([\\d.]+) ([lrc]) x ([\\d.]+) y ([\\d.]+)");
            matcher = findpattern.matcher(line[i]);
            if (matcher.find()) {
              String cmd = matcher.group(1);
              ShellExecuter exe = new ShellExecuter(model);
              ArrayList<String> command = new ArrayList<>();
              command.add(cmd);
              //llog.d(TAG, "execute cmd " + cmd);
              String[] result = exe.execute(command, false, true);
              //llog.d(TAG, "cmd executed " + cmd);
              if (result != null) {
                int rl = result.length;
                if (rl > 0) {
                  int ri = 0;
                  findpattern = Pattern.compile(" r ([\\d.]+) g ([\\d.]+) b ([\\d.]+) s ([\\d.]+) ([lrc]) x ([\\d.]+) y ([\\d.]+)");
                  matcher = findpattern.matcher(line[i]);
                  while (matcher.find() && ri < rl) {
                    paint.setColor(Color.rgb(
                        (int) (Float.parseFloat(matcher.group(1)) * 255f),
                        (int) (Float.parseFloat(matcher.group(2)) * 255f),
                        (int) (Float.parseFloat(matcher.group(3)) * 255f)
                    ));
                    paint.setStyle(Paint.Style.FILL);
                    paint.setTextSize(h * Float.parseFloat(matcher.group(4)));
                    if (matcher.group(5).equals("l"))
                      paint.setTextAlign(Paint.Align.LEFT);
                    else if (matcher.group(5).equals("r"))
                      paint.setTextAlign(Paint.Align.RIGHT);
                    else if (matcher.group(5).equals("c"))
                      paint.setTextAlign(Paint.Align.CENTER);
                    canvas.drawText(result[ri], w * Float.parseFloat(matcher.group(6)), h * Float.parseFloat(matcher.group(7)), paint);
                    ri += 1;
                  }
                }
              }
            }
          }
        }
      }
    }
    return bitmap;
  }

  public static boolean buttonaction(String[] line, Gallery model, float x, float y){
    boolean executed = false;
    int linel = line.length;
    Pattern findpattern;
    Matcher matcher;
    for (int i = 0; i < linel; i++) {
      if (line[i].contains("buttonruncmd=")) {
        findpattern = Pattern.compile("^\\w*buttonruncmd= xi ([\\d.]+) xf ([\\d.]+) yi ([\\d.]+) yf ([\\d.]+) msg ([yn]) cmd (.+)");
        matcher = findpattern.matcher(line[i]);
        if (matcher.find()) {
          float xi = Float.parseFloat(matcher.group(1));
          float xf = Float.parseFloat(matcher.group(2));
          float yi = Float.parseFloat(matcher.group(3));
          float yf = Float.parseFloat(matcher.group(4));
          if (xi < x && x <= xf && yi < y && y < yf) {
            boolean showresults = false;
            if (matcher.group(5).equals("y"))
              showresults = true;
            String cmd = matcher.group(6);
            ShellExecuter exe = new ShellExecuter(model);
            ArrayList<String> command = new ArrayList<>();
            command.add(cmd);
            llog.d(TAG, "execute cmd " + cmd);
            model.messagelocknext = true;
            String[] result = exe.execute(command, showresults, false);
            llog.d(TAG, "cmd executed " + cmd);
            executed = true;
          }
        }
      } else if (line[i].contains("buttongethtml=")) {
        findpattern = Pattern.compile("^\\w*buttongethtml= xi ([\\d.]+) xf ([\\d.]+) yi ([\\d.]+) yf ([\\d.]+) url (.+)");
        matcher = findpattern.matcher(line[i]);
        if (matcher.find()) {
          float xi = Float.parseFloat(matcher.group(1));
          float xf = Float.parseFloat(matcher.group(2));
          float yi = Float.parseFloat(matcher.group(3));
          float yf = Float.parseFloat(matcher.group(4));
          if (xi < x && x <= xf && yi < y && y < yf) {
            String cmd = matcher.group(5);
            String html = InternetSession.gethtml(model, cmd, cmd);
            llog.d(TAG, "url retrieved " + cmd + " " + html);
            if (html != null) {
              if (html.length() > 1) {
                if (html.length() > 1000)
                  model.message(html.substring(0, 1000));
                else
                  model.message(html);
              }
            }
            executed = true;
          }
        }
      }
    }
    return executed;
  }

}





































