package la.daube.photochiotte;

import android.annotation.SuppressLint;
import android.app.Instrumentation;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.PixelFormat;
import android.net.Uri;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link FragmentBrowser.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link FragmentBrowser#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FragmentBrowser extends Fragment {
  private static final String TAG = "YYYfbv";

  private Gallery model;
  public int myid = -1;
  private Surf mysurf = null;

  public FragmentBrowser() {
  }

  public static FragmentBrowser newInstance(String param1, String param2) {
    llog.d(TAG, "newInstance()");
    FragmentBrowser fragment = new FragmentBrowser();
    Bundle args = new Bundle();
    args.putString("param1", param1);
    args.putString("param2", param2);
    fragment.setArguments(args);
    return fragment;
  }

  @Override
  public void onSaveInstanceState(Bundle savedInstanceState) {
    llog.d(TAG, "onSaveInstanceState()");
    super.onSaveInstanceState(savedInstanceState);
  }

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    Bundle args = getArguments();
    if (args != null) {
      myid = args.getInt("myid", -1);
    }
    llog.d(TAG, myid+" onCreate()");
    model = new ViewModelProvider(requireActivity()).get(Gallery.class);
    if (model.surfzappernumber == 0) // si on zappe on veut le contrôle sur zappercommander
      model.currentselectedfragment = myid;
  }

  @Override
  public void onResume() {
    super.onResume();
    llog.d(TAG, myid+" onResume()");
  }

  @Override
  public void onStart() {
    super.onStart();
    llog.d(TAG, myid+" onStart()");
  }

  @Override
  public void onPause() {
    super.onPause();
    llog.d(TAG, myid+" onPause()");
    SharedPreferences.Editor prefEdit = model.preferences.edit();
    prefEdit.putInt("thumbsize", model.thumbsize);
    prefEdit.putInt("miniaturevideo", model.miniaturevideo);
    prefEdit.commit();
  }

  @Override
  public void onDestroy() {
    super.onDestroy();
    llog.d(TAG, myid+" onDestroy()");
    if (mysurf != null) {
      if (mysurf.mpvSurfaceHolder != null) {
        Surface surface = mysurf.mpvSurfaceHolder.getSurface();
        if (surface != null) {
          if (surface.isValid()) {
            surface.release();
          }
        }
        if (mycallback != null) {
          mysurf.mpvSurfaceHolder.removeCallback(mycallbackmpv);
        }
        mysurf.mpvSurfaceHolder = null;
      }
      if (mysurf.browserSurfaceHolder != null) {
        Surface surface = mysurf.browserSurfaceHolder.getSurface();
        if (surface != null) {
          if (surface.isValid()) {
            surface.release();
          }
        }
        if (mycallback != null) {
          mysurf.browserSurfaceHolder.removeCallback(mycallback);
        }
        mysurf.browserSurfaceHolder = null;
      }
      //if (mysurf.mpvlib != null) {
        // mpvlib set to null only if user shuts it down : videoaskclose = true
        llog.d(TAG, myid+" onDestroy() do not set mpvlib=null in case we're rotating screen");
        //mysurf.mpvlib = null;
      //}
    }

  }

  private final View.OnTouchListener ontouchlistener = new View.OnTouchListener() {
    float pos0ix = 0.0f;
    float pos0iy = 0.0f;
    float pos0fx = 0.0f;
    float pos0fy = 0.0f;
    float pos1ix = 0.0f;
    float pos1iy = 0.0f;
    float pos1fx = 0.0f;
    float pos1fy = 0.0f;
    float delta10ix = 0.0f;
    float delta10iy = 0.0f;
    float delta10isqr = 0.0f;
    float bpxi = 0.0f;
    float bpyi = 0.0f;
    float bpxmax = 0.0f;
    float bpymax = 0.0f;
    float bscalei = 0.0f;
    float bpany = 0.0f;
    float bpanx = 0.0f;
    float thumbsizei = 0.0f;
    float bscalecx = 0.0f;
    float bscalecy = 0.0f;
    float posmix = 0.0f;
    float posmiy = 0.0f;
    long timetouchdown = System.currentTimeMillis();
    long lasttime = System.currentTimeMillis();
    int maxpointerdown=0;
    boolean thumbsizechanged = false;

    boolean wemoved = false;
    boolean wetouchedbigimage = false;
    boolean westartedtouchingnavigationbars = false;
    boolean wetouchednavigationbars = false;
    boolean wetouchedmenuascenseur = false;
    boolean wetouchedresizesurf = false;
    boolean wepinched = false;
    final long minmovingtime = 250;
    float optionsbase = 0.0f;
    final int nbremaxptr = 20;
    final int[] pointerids = new int[nbremaxptr];
    float deltagrabsplit;

    int cechoixdepossibilite = -1;

    @Override
    public boolean onTouch(View v, MotionEvent ev) {
      model.currentselectedfragment = myid;
      if(mysurf.ScreenWidth <4 || mysurf.ScreenHeight <4){
        return true;
      }
      int action = ev.getAction() & MotionEvent.ACTION_MASK;
      int pointerindex = ev.getActionIndex();
      int pointerid = ev.getPointerId(pointerindex);
      int pointercount = ev.getPointerCount();
      int pointerindex0, pointerindex1;
      if (pointercount > maxpointerdown){
        maxpointerdown = pointercount;
      }

      if (action == MotionEvent.ACTION_DOWN){
        cechoixdepossibilite =  2;
      } else if (cechoixdepossibilite != 2){
        llog.d(TAG, "no good----------");
        return false;
      }

      switch (action) {
        case MotionEvent.ACTION_DOWN:
          //llog.d(TAG, "touch down");
          if (mysurf.fragmenttype == Surf.FRAGMENT_TYPE_BROWSER) {
            if (mysurf.optiondiaporamaactive && mysurf.diaporamalastchange > 0)
              mysurf.diaporamalastchange = System.currentTimeMillis();
          }
          for (int i=0; i<nbremaxptr; i++){
            pointerids[i] = -99;
          }
          pointerids[0] = pointerid;
          pointerindex0 = ev.findPointerIndex(pointerids[0]);
          pos0ix = ev.getX(pointerindex0);
          pos0iy = ev.getY(pointerindex0);
          bpxi = mysurf.bpx;
          bpyi = mysurf.bpy;
          bpxmax = mysurf.bpxmax;
          bpymax = mysurf.bpymax;
          thumbsizei = model.thumbsize;
          float dx = bpxmax - bpxi;
          if (dx < thumbsizei) {
            bpxmax += thumbsizei - dx;
          }
          float dy = bpymax - bpyi;
          if (dy < thumbsizei) {
            bpymax += thumbsizei - dy;
          }
          optionsbase = mysurf.SettingsYmin;
          wemoved = false;
          wepinched = false;
          wetouchedmenuascenseur = false;
          wetouchedresizesurf = false;
          westartedtouchingnavigationbars = false;
          wetouchednavigationbars = false;
          wetouchedbigimage = false;
          boolean menushown = ((mysurf.OptionMenuShown && mysurf.fragmenttype == Surf.FRAGMENT_TYPE_BROWSER)
                  || (mysurf.videoShowOptionMenu && mysurf.fragmenttype == Surf.FRAGMENT_TYPE_VIDEO));
          if (!menushown
                  && (mysurf.myy + pos0iy < mysurf.myscreenheight * Surf.PERCENTOFSCREENIGNORENAVIGATIONBARTOP
                  || mysurf.myy + pos0iy > Surf.PERCENTOFSCREENIGNORENAVIGATIONBARBOTTOM * mysurf.myscreenheight
                  || mysurf.myx + pos0ix > Surf.PERCENTOFSCREENIGNORENAVIGATIONBARRIGHTBACK * mysurf.myscreenwidth)
          ) { //llog.d(TAG, "we touched navigation bar, bail out move");
            westartedtouchingnavigationbars = true;
          }
          deltagrabsplit = 3.0f * model.GenericInterSpace;
          if (menushown && pos0ix > mysurf.SettingsXmin && mysurf.SettingsYmin < pos0iy && pos0iy < mysurf.SettingsYmax) {
            wetouchedmenuascenseur = true;
          } else if ((mysurf.splitleft && pos0ix < deltagrabsplit)
                  || (mysurf.splittop && pos0iy < deltagrabsplit)
                  || (mysurf.splitright && pos0ix > mysurf.mywidth - deltagrabsplit)
                  || (mysurf.splitbottom && pos0iy > mysurf.myheight - deltagrabsplit)) {
            wetouchedresizesurf = true;
          } else if (mysurf.fragmenttype == Surf.FRAGMENT_TYPE_BROWSER
               && bpxi <= pos0ix && pos0ix < bpxmax
               && bpyi <= pos0iy && pos0iy < bpymax) {
            wetouchedbigimage = true;
          }
          if (mysurf.fragmenttype == Surf.FRAGMENT_TYPE_VIDEO) {
            wetouchedbigimage = true;
          }
          maxpointerdown = 0;
          lasttime = System.currentTimeMillis();
          timetouchdown = System.currentTimeMillis();
          break;
        case MotionEvent.ACTION_POINTER_DOWN:
          //llog.d(TAG, "pointer down " + maxpointerdown);
          if (maxpointerdown > 1) {
            westartedtouchingnavigationbars = false;
            wetouchednavigationbars = false;
          }
          if (maxpointerdown == 2) {
            if(pointerids[1] == -99) {
              pointerids[1] = pointerid;
              pointerindex1 = ev.findPointerIndex(pointerids[1]);
              pos1ix = ev.getX(pointerindex1);
              pos1iy = ev.getY(pointerindex1);
              if (mysurf.fragmenttype == Surf.FRAGMENT_TYPE_BROWSER
                 && ( (bpxi <= pos1ix && pos1ix < bpxmax && bpyi <= pos1iy && pos1iy < bpymax)
                   || (bpxi <= pos0ix && pos1ix < bpxmax && bpyi <= pos0iy && pos1iy < bpymax)
                   || (bpxi <= pos1ix && pos0ix < bpxmax && bpyi <= pos1iy && pos0iy < bpymax)
                   || (bpxi <= pos1ix && pos0ix < bpxmax && bpyi <= pos0iy && pos1iy < bpymax)
                   || (bpxi <= pos0ix && pos1ix < bpxmax && bpyi <= pos1iy && pos0iy < bpymax)
                )
              ) {
                wetouchedbigimage = true;
              }
              delta10ix = pos1ix - pos0ix;
              delta10iy = pos1iy - pos0iy;
              delta10isqr = (float) Math.sqrt(delta10ix * delta10ix + delta10iy * delta10iy);
              if (mysurf.fragmenttype == Surf.FRAGMENT_TYPE_BROWSER) {
                bscalei = mysurf.bscale;
              } else if (mysurf.fragmenttype == Surf.FRAGMENT_TYPE_VIDEO) {
                if (mysurf.mpvlib != null) {
                  bscalei = (float) mysurf.mpvlib.videozoom;
                  bpanx = (float) mysurf.mpvlib.videopanx;
                  bpany = (float) mysurf.mpvlib.videopany;
                }
              }
              thumbsizei = model.thumbsize;
              bscalecx = (pos0ix + pos1ix) / 2.0f;
              bscalecy = (pos0iy + pos1iy) / 2.0f;
              posmix = (pos0ix + pos1ix) / 2.0f;
              posmiy = (pos0iy + pos1iy) / 2.0f;
            }
          }
          wemoved = false;
          wepinched = false;
          break;
        case MotionEvent.ACTION_MOVE:
          if (westartedtouchingnavigationbars) {
            wetouchednavigationbars = true;
            return false;
          }
          long temps = System.currentTimeMillis();
          if (temps - lasttime > 30 && !wetouchedresizesurf) {

            if (mysurf.fragmenttype == Surf.FRAGMENT_TYPE_BROWSER) {
              if (mysurf.optiondiaporamaactive && mysurf.diaporamalastchange > 0)
                mysurf.diaporamalastchange = temps;
              if (model.filmstripanimatetime > 1000)
                mysurf.filmstriplastchange = (long) (temps - model.filmstripanimatetime * 0.5);
              else
                mysurf.filmstriplastchange = temps;
            }

            if(pointerid == pointerids[0]) {
              pointerindex0 = ev.findPointerIndex(pointerids[0]);
              pos0fx = ev.getX(pointerindex0);
              pos0fy = ev.getY(pointerindex0);
            }
            float delta0fix = pos0fx - pos0ix;
            float delta0fiy = pos0fy - pos0iy;
            float delta0fisqr = delta0fix * delta0fix + delta0fiy * delta0fiy;
            if (!wemoved) {
              if (delta0fisqr > model.deltaminmove2 || temps - timetouchdown > minmovingtime) {
                wemoved = true;
              }
            }
            if (pointercount == 1 && maxpointerdown == 1) {
              float ratio;
              if (wetouchedmenuascenseur) {
                ratio = (float) Math.sqrt((delta0fisqr * 0.125f) / model.deltapowermoveunit2);
                if (ratio < 1.0f) ratio = 1.0f;
                mysurf.SettingsYmin = optionsbase + ratio * delta0fiy;
                sendcommandtothread(new String[]{String.valueOf(myid), "update"});
              } else if (mysurf.fragmenttype == Surf.FRAGMENT_TYPE_VIDEO && mysurf.videoShowLog) {
                if (mysurf.mpvlib != null) {
                  if (delta0fiy > 0) {
                    if (mysurf.videoshowlogfrom - 1 >= 0) {
                      mysurf.videoshowlogfrom -= 1;
                    } else {
                      mysurf.videoshowlogfrom = 0;
                    }
                  } else {
                    if (mysurf.videoshowlogfrom + 1 < mysurf.mpvlib.log.size()) {
                      mysurf.videoshowlogfrom += 1;
                    }
                  }
                }
              } else {
                float speedboost;
                if (wetouchedbigimage) speedboost = 0.125f;
                else if (temps - timetouchdown > 5000) speedboost = 10.0f;
                else speedboost = 5.0f;
                ratio = (float) Math.sqrt((delta0fisqr * speedboost) / model.deltapowermoveunit2);
                if (ratio < 1.0f) ratio = 1.0f;
                mysurf.bpx = bpxi + ratio * delta0fix;
                mysurf.bpy = bpyi + ratio * delta0fiy;
                sendcommandtothread(new String[]{String.valueOf(myid), "move"});
              }
            } else if (pointercount == 2 && maxpointerdown == 2) {
              pointerindex1 = ev.findPointerIndex(pointerids[1]);
              pos1fx = ev.getX(pointerindex1);
              pos1fy = ev.getY(pointerindex1);
              float delta10fx = pos1fx - pos0fx;
              float delta10fy = pos1fy - pos0fy;
              float delta10fsqr = (float) Math.sqrt(delta10fx * delta10fx + delta10fy * delta10fy);
              float ratio = delta10fsqr / delta10isqr;
              /**
               si on est sur la grande image on rescale
               sinon on augmente/diminue la taille des miniatures
               */
              if (mysurf.fragmenttype == Surf.FRAGMENT_TYPE_BROWSER) {
                if (wetouchedbigimage) {
                  mysurf.bscale = bscalei * ratio;
                  float deltamfix = (pos0fx + pos1fx) / 2.0f - posmix;
                  float deltamfiy = (pos0fy + pos1fy) / 2.0f - posmiy;
                  mysurf.bpx = bpxi - (bscalecx - bpxi) * (ratio - 1.0f) + deltamfix;
                  mysurf.bpy = bpyi - (bscalecy - bpyi) * (ratio - 1.0f) + deltamfiy;
                } else {
                  thumbsizechanged = true;
                  model.thumbsize = (int) (thumbsizei * ratio);
                  if (model.thumbsize < 2) {
                    model.thumbsize = 2;
                  }
                  model.maxnumberthumsincache = (int) Math.floor((model.bigScreenWidth * model.bigScreenHeight * Gallery.nombredecranenram) / (model.thumbsize * model.thumbsize));
                }
              } else if (mysurf.fragmenttype == Surf.FRAGMENT_TYPE_VIDEO) {
                if (mysurf.mpvlib != null) {
                  if (ratio > 3.0f)
                    ratio = 3.0f;
                  if (ratio >= 1.0f)
                    mysurf.mpvlib.videozoom = bscalei + (ratio - 1.0f);
                  else
                    mysurf.mpvlib.videozoom = bscalei - (1.0f - ratio);
                  float deltamfix = (pos0fx + pos1fx) / 2.0f - posmix;
                  float deltamfiy = (pos0fy + pos1fy) / 2.0f - posmiy;
                  mysurf.mpvlib.videopanx = bpanx + deltamfix / mysurf.myscreenwidth;
                  mysurf.mpvlib.videopany = bpany + deltamfiy / mysurf.myscreenheight;
                }
              }
              wepinched = true;
              sendcommandtothread(new String[]{String.valueOf(myid), "rescale"});
            }
            lasttime = temps;
          }
          break;
        case MotionEvent.ACTION_POINTER_UP:
          break;
        case MotionEvent.ACTION_UP:
          //llog.d(TAG, "touch up " + maxpointerdown + " moved " + wemoved);
          if (mysurf.fragmenttype == Surf.FRAGMENT_TYPE_BROWSER) {
            if (mysurf.optiondiaporamaactive && mysurf.diaporamalastchange > 0)
              mysurf.diaporamalastchange = System.currentTimeMillis();
          }
          if (wetouchednavigationbars) {
            return false;
          }
          if(pointerid == pointerids[0]) {
            pointerindex0 = ev.findPointerIndex(pointerids[0]);
            pos0fx = ev.getX(pointerindex0);
            pos0fy = ev.getY(pointerindex0);
          }
          if (System.currentTimeMillis() - timetouchdown > minmovingtime) {
            wemoved = true;
          }
          if (wetouchedresizesurf) {
            int splitdistance = 0;
            int splitcote = 1;
            if (mysurf.splitleft && pos0ix < deltagrabsplit) {
              splitcote = 1;
              splitdistance = (int) pos0fx;
            }
            if (mysurf.splittop && pos0iy < deltagrabsplit) {
              splitcote = 2;
              splitdistance = (int) pos0fy;
            }
            if (mysurf.splitright && pos0ix > mysurf.mywidth - deltagrabsplit) {
              splitcote = 3;
              splitdistance = (int) (pos0fx - mysurf.mywidth);
            }
            if (mysurf.splitbottom && pos0iy > mysurf.myheight - deltagrabsplit) {
              splitcote = 4;
              splitdistance = (int) (pos0fy - mysurf.myheight);
            }
            //llog.d(TAG, "modify cut "+splitcote+" "+splitdistance);
            Intent intent = new Intent();
            intent.setAction(Gallery.broadcastname);
            intent.putExtra("goal", "expandsplitscreen");
            intent.putExtra("splitsurf", mysurf.myid);
            intent.putExtra("splitdistance", splitdistance);
            intent.putExtra("splitcote", splitcote);
            LocalBroadcastManager.getInstance(model.activitycontext).sendBroadcast(intent);

          } else if (maxpointerdown == 1 && !wemoved) {
            sendcommandtothread(new String[]{String.valueOf(myid), "next", String.valueOf(pos0fx), String.valueOf(pos0fy)});

          } else if (maxpointerdown <=2 && wemoved) {
            if (thumbsizechanged) { // only set on browser anyway
              thumbsizechanged = false;
              try {
                model.commandethreadminiature.put(new String[]{"cleanup", "force"}); // do not force not needed here
              } catch (InterruptedException e) {
                e.printStackTrace();
              }
            }

          } else if (maxpointerdown == 2 && !wemoved && model.iswatch) {
            if (   (pos0ix < model.bigScreenWidth * 0.15f && pos1iy < model.bigScreenHeight * 0.15f)
                || (pos1ix < model.bigScreenWidth * 0.15f && pos0iy < model.bigScreenHeight * 0.15f)
                || (pos0ix > model.bigScreenWidth * 0.85f && pos1iy < model.bigScreenHeight * 0.15f)
                || (pos1ix > model.bigScreenWidth * 0.85f && pos0iy < model.bigScreenHeight * 0.15f)
            ) {
              llog.d(TAG, "watch back asked");
              Intent intent = new Intent();
              intent.setAction(Gallery.broadcastname);
              intent.putExtra("goal", "backpressed");
              intent.putExtra("id", model.currentselectedfragment);
              LocalBroadcastManager.getInstance(model.activitycontext).sendBroadcast(intent);
            } else {
              llog.d(TAG, "watch menu asked");
              sendcommandtothread(new String[]{String.valueOf(model.currentselectedfragment), "next tv", "menu"});
            }

          } else if (maxpointerdown == 3) {
            if (mysurf.fragmenttype == Surf.FRAGMENT_TYPE_BROWSER) {
              mysurf.OptionMenuShown = !mysurf.OptionMenuShown;
            } else if (mysurf.fragmenttype == Surf.FRAGMENT_TYPE_VIDEO) {
              mysurf.videoShowOptionMenu = !mysurf.videoShowOptionMenu;
              if (mysurf.videoShowOverlay != mysurf.videoShowOptionMenu
                      && !mysurf.videoShowProgressBar
                      && !mysurf.videoShowSub) {
                mysurf.videoShowOverlay = mysurf.videoShowOptionMenu;
                if (!mysurf.videoShowOverlay)
                  model.startVideoClearBrowserSurfaceTransparent = true;
              }
            }
            sendcommandtothread(new String[]{String.valueOf(myid), "update"});

          } else if (maxpointerdown == 4) {
            sendcommandtothread(new String[]{String.valueOf(myid), "displayinfo"});

          } else if (maxpointerdown == 5) {
            if (mysurf.fragmenttype == Surf.FRAGMENT_TYPE_BROWSER) {
              mysurf.showthumbnails = !mysurf.showthumbnails;
              try {
                model.commandethreadbrowser.put(new String[]{String.valueOf(myid), "update"});
              } catch (InterruptedException e) {
                e.printStackTrace();
              }
              if (mysurf.showthumbnails) {
                try {
                  model.commandethreadminiature.put(new String[]{"cleanup", "force"}); // do not force not needed here
                } catch (InterruptedException e) {
                  e.printStackTrace();
                }
              }
            }

          }
          break;
        default:
          llog.d(TAG, "on ordonne autre");
          break;
      }
      return true;
    }
  };

  private final SurfaceHolder.Callback mycallbackmpv = new SurfaceHolder.Callback() {
    @Override
    public void surfaceCreated(SurfaceHolder surfaceHolder) {
      llog.d(TAG, myid + " mpv surface surfaceCreated() " + mysurf.mywidth + "x" + mysurf.myheight);
      mysurf.mpvSurfaceHolder = surfaceHolder;
      mysurf.mpvSurfaceHolder.setSizeFromLayout();
      mysurf.foldoptions();
      if (mysurf.mpvlib != null) {
        mysurf.mpvlib.attachSurface(mysurf.mpvSurfaceHolder.getSurface());
        mysurf.mpvlib.framewidth = mysurf.mywidth;
        mysurf.mpvlib.frameheight = mysurf.myheight;
        mysurf.mpvlib.aspectCheckIfVideoOrScreen();
        mysurf.mpvlib.pause(false);
        mysurf.mpvlib.setvid(1);
        if (!model.videomute)
          mysurf.mpvlib.setaiddefault();
        mysurf.mpvlib.setsid(1);
      }
    }
    @Override
    public void surfaceChanged(SurfaceHolder surfaceHolder, int i, int i1, int i2) {
      llog.d(TAG, myid + " mpv surface surfaceChanged() " + i1 + "x" + i2 + " / mysurf.mywidth " + mysurf.mywidth + "x" + mysurf.myheight);
      if (mysurf.mpvlib != null) {
        mysurf.mpvlib.setProperty("android-surface-size", i1 + "x" + i2);
        mysurf.mpvlib.framewidth = i1; //on garde les dimensions de mysurf si on réduit volontairement le champ de la video
        mysurf.mpvlib.frameheight = i2;
        boolean success = mysurf.mpvlib.aspectCheckIfVideoOrScreen();
        if (model.videoaaspectresize && success && mysurf.mpvlib != null) {
          if (0 == mysurf.mpvlib.eventFatalNoVideo) {
            llog.d(TAG, myid + " mpv surface asked videoaaspectresize");
            sendcommandtothread(new String[]{String.valueOf(myid), "videoaaspectresize"});
          }
        }
      }
    }
    @Override
    public void surfaceDestroyed(SurfaceHolder surfaceHolder) {
      if (mysurf.mpvlib == null) {
        llog.d(TAG, myid + " mpv surface surfaceDestroyed() mysurf.mpvlib=null");
      } else {
        llog.d(TAG, myid + " mpv surface surfaceDestroyed() videoaskclose=" + mysurf.mpvlib.videoaskclose);
        if (!mysurf.mpvlib.videoaskclose) {
          mysurf.mpvlib.pause(true);
          mysurf.mpvlib.setvid(0);
          mysurf.mpvlib.setaid(0);
          mysurf.mpvlib.setsid(0);
          mysurf.mpvlib.detachSurface();
        }
      }
    }
  };

  private final SurfaceHolder.Callback mycallback = new SurfaceHolder.Callback() {
    @Override
    public void surfaceCreated(SurfaceHolder surfaceHolder) {
      llog.d(TAG, myid+" surface surfaceCreated()");
      mysurf.browserSurfaceHolder = surfaceHolder;
      if (mysurf.browserSurfaceHolder != null)
        mysurf.browserSurfaceHolder.setSizeFromLayout();
      mysurf.foldoptions();
    }

    @Override
    public void surfaceChanged(SurfaceHolder surfaceHolder, int i, int i1, int i2) {
      llog.d(TAG, myid+" surface surfacechanged() : "+i1 +"x"+i2 + " mywidth " + mysurf.mywidth + "x" + mysurf.myheight + " ScreenWidth " + mysurf.ScreenWidth + "x" + mysurf.ScreenHeight + " bigScreenWidth " + model.bigScreenWidth + "x" + model.bigScreenHeight);
      if (i1 != mysurf.mywidth || i2 != mysurf.myheight) {
        llog.d(TAG, myid+" surfacechanged() : different sizes need to modify surface !");
      }
      if (mysurf.mywidth != mysurf.ScreenWidth || mysurf.myheight != mysurf.ScreenHeight) {
        llog.d(TAG, myid+" surfacechanged() different size from beforee " + i1 + " " + i2);
        mysurf.ScreenWidth = mysurf.mywidth;
        mysurf.ScreenHeight = mysurf.myheight;
      }
      setallthepaints();
      mysurf.mysurfacestopdrawing = false;
      mysurf.mysurfaceisdestroyed = false;
      sendcommandtothread(new String[]{String.valueOf(myid), "surfacechanged"});
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder surfaceHolder) {
      llog.d(TAG, myid+" surface surfaceDestroyed()");
      mysurf.mysurfacestopdrawing = true;
      while (mysurf.surfaceIsCurrentlyDrawing) {
        try {
          Thread.sleep(10);
        } catch (InterruptedException e) {
          e.printStackTrace();
        }
      }
      mysurf.mysurfaceisdestroyed = true;
    }
  };

  @SuppressLint("ClickableViewAccessibility")
  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
    llog.d(TAG, myid+" onCreateView()");
    if (myid >= model.surf.size() || myid < 0) {
      llog.d(TAG, "error myid " + myid + " surfsize " + model.surf.size());
      return null;
    }
    mysurf = model.surf.get(myid);
    mysurf.fragmentView = inflater.inflate(R.layout.fragment_browser, container, false);

    mysurf.fragmentView.setX(model.surf.get(myid).myx);
    mysurf.fragmentView.setY(model.surf.get(myid).myy);
    ViewGroup.LayoutParams parms = mysurf.fragmentView.getLayoutParams();
    parms.width = model.surf.get(myid).mywidth;
    parms.height = model.surf.get(myid).myheight;
    mysurf.fragmentView.setLayoutParams(parms);

    mysurf.mpvSurfaceView = mysurf.fragmentView.findViewById(R.id.mpvSurface);
    mysurf.mpvSurfaceHolder = mysurf.mpvSurfaceView.getHolder();
    mysurf.mpvSurfaceHolder.addCallback(mycallbackmpv);

    mysurf.browserSurfaceView = mysurf.fragmentView.findViewById(R.id.browserSurface);
    if (!model.optionhidesurface)
      mysurf.browserSurfaceView.setZOrderMediaOverlay(true);
    mysurf.browserSurfaceHolder = mysurf.browserSurfaceView.getHolder();
    mysurf.browserSurfaceHolder.setFormat(PixelFormat.TRANSLUCENT);
    mysurf.browserSurfaceHolder.addCallback(mycallback);

    mysurf.fragmentView.setOnTouchListener(ontouchlistener);

    return mysurf.fragmentView;
  }

  public void showoverlayinfo(final boolean force, final int delay) {
    llog.d(TAG, "showoverlayinfo " + force + " " + delay);
    if (delay == 0) {
      showoverlayinfosub2(force);
    } else {
      mysurf.fragmentView.postDelayed(new Runnable() {
        @Override
        public void run() {
          showoverlayinfosub2(force);
        }
      }, delay);
    }
  }

  private void showoverlayinfosub2(boolean force) {
    llog.d(TAG, "showoverlayinfosub2");
    if (mysurf == null)
      return;
    if (mysurf.browserSurfaceView == null)
      return;
    if (mysurf.browserSurfaceView.getVisibility() == View.GONE) {
      model.message("showoverlayinfosub2 videoShowOverlay GONE -> VISIBLE");
      mysurf.browserSurfaceView.setVisibility(View.VISIBLE); // appelle surfaceCreated
    } else {
      model.message("showoverlayinfosub2 videoShowOverlay VISIBLE -> GONE");
      mysurf.browserSurfaceView.setVisibility(View.GONE); // appelle surfaceDestroyed
    }
  }

  private void showoverlayinfosub(boolean force) {
    if (mysurf == null)
      return;
    if (mysurf.browserSurfaceView == null)
      return;
    if (mysurf.fragmenttype == Surf.FRAGMENT_TYPE_VIDEO) {
      if (mysurf.videoShowOverlay) {
        if (mysurf.browserSurfaceView.getVisibility() == View.GONE) {
          llog.d(TAG, "showoverlayinfosub FRAGMENT_TYPE_VIDEO videoShowOverlay GONE -> VISIBLE");
          mysurf.browserSurfaceView.setVisibility(View.VISIBLE); // appelle surfaceCreated
        } else if (force) {
          llog.d(TAG, "showoverlayinfosub FRAGMENT_TYPE_VIDEO videoShowOverlay VISIBLE -> GONE force");
          mysurf.browserSurfaceView.setVisibility(View.GONE); // appelle surfaceDestroyed
        }
      } else {
        if (mysurf.browserSurfaceView.getVisibility() != View.GONE) {
          llog.d(TAG, "showoverlayinfosub FRAGMENT_TYPE_VIDEO !videoShowOverlay !GONE -> GONE");
          mysurf.browserSurfaceView.setVisibility(View.GONE); // appelle surfaceDestroyed
        }
      }
    } else {
      if (mysurf.browserSurfaceView.getVisibility() == View.GONE) {
        llog.d(TAG, "showoverlayinfosub !FRAGMENT_TYPE_VIDEO GONE -> VISIBLE");
        mysurf.browserSurfaceView.setVisibility(View.VISIBLE); // appelle surfaceCreated
      }
    }
  }

  public void resizevideoframe(boolean maxout){

    if (mysurf == null)
      return;
    if (mysurf.mywidth < 4 || mysurf.myheight < 4)
      return;

    int newwidth, decalx, decaly, newheight;
    if (maxout) {
      decalx = 0;
      decaly = 0;
      newwidth = mysurf.mywidth;
      newheight = mysurf.myheight;
    } else {
      if (mysurf.fragmenttype != Surf.FRAGMENT_TYPE_VIDEO)
        return;
      if (mysurf.mpvlib == null)
        return;
      double videoratio = mysurf.mpvlib.videoaspect;
      double surfratio = (double) mysurf.mywidth / (double) mysurf.myheight;
      double zoom = Math.pow(2, mysurf.mpvlib.videozoom);
      if (videoratio < surfratio) {
        newwidth = (int) Math.round(videoratio * (double) mysurf.myheight * zoom);
        newheight = (int) Math.round((double) mysurf.myheight * zoom);
        llog.d(TAG, "+++++++++++++++++++++++++++++++++++++++resizevideoframe " + videoratio + " " + surfratio + " " + zoom + " " + newwidth + "x" + newheight);
      } else {
        newwidth = (int) Math.round((double) mysurf.mywidth * zoom);
        newheight = ((int) Math.round(((double) mysurf.mywidth * zoom) / videoratio));
        llog.d(TAG, "+++++++++++++++++++++++++++++++++++++++resizevideoframe " + videoratio + " " + surfratio + " " + zoom + " " + newwidth + "x" + newheight);
      }
      if (newwidth > mysurf.mywidth)
        newwidth = mysurf.mywidth;
      if (newheight > mysurf.myheight)
        newheight = mysurf.myheight;
      if (newwidth < 4)
        newwidth = 4;
      if (newheight < 4)
        newheight = 4;
      decalx = (int) Math.round((mysurf.mywidth - newwidth) / 2.0f);
      decaly = (int) Math.round((mysurf.myheight - newheight) / 2.0f);
    }

    if (mysurf.mpvSurfaceView == null)
      return;
    ViewGroup.LayoutParams parms = mysurf.mpvSurfaceView.getLayoutParams();
    int newwhidthi = newwidth;
    int newheighti = newheight;
    parms.width = newwhidthi;
    parms.height = newheighti;
    mysurf.mpvSurfaceView.setLayoutParams(parms);
    mysurf.mpvSurfaceView.setX(decalx);
    mysurf.mpvSurfaceView.setY(decaly);
    mysurf.mpvSurfaceView.requestLayout();
    if (mysurf.browserSurfaceView == null)
      return;
    mysurf.browserSurfaceView.requestLayout();

    //if (mysurf.mpvlib == null)
    //  return;
    /*mysurf.mpvlib.setProperty("android-surface-size", newwhidthi + "x" + newheighti);
    mysurf.mpvlib.framewidth = newwhidthi;
    mysurf.mpvlib.frameheight = newheighti;
    mysurf.mpvlib.aspectCheckIfVideoOrScreen();*/
    /*
    mysurf.mpvlib.videozoom = 1.0f;
    mysurf.mpvlib.videopanx = 0.0f;
    mysurf.mpvlib.videopany = 0.0f;
    mysurf.mpvlib.setzoom(mysurf.mpvlib.videozoom);
    mysurf.mpvlib.setvideopanx(mysurf.mpvlib.videopanx);
    mysurf.mpvlib.setvideopany(mysurf.mpvlib.videopany);*/

    llog.d(TAG, "++++++++++++++++++++++++++++++++++++++++++++++++resizevideoframe success " + maxout + " " +decalx + ","  + decaly + " " + newwhidthi + "x" + newheighti);

  }

  private void setallthepaints(){

    mysurf.GraphWidth = model.GraphWidth;
    if (mysurf.GraphWidth > Gallery.graphwidthmax * mysurf.mywidth && !model.iswatch) {
      mysurf.GraphWidth = (int)(Gallery.graphwidthmax * mysurf.mywidth);
    }
    mysurf.SettingsWidth = model.SettingsWidth;
    if (mysurf.SettingsWidth > Gallery.settingswidthmax * mysurf.mywidth && !model.iswatch) {
      mysurf.SettingsWidth = (int)(Gallery.settingswidthmax * mysurf.mywidth);
    }

    mysurf.SettingsXmin = mysurf.mywidth - mysurf.SettingsWidth;
    mysurf.SettingsYmax = model.GenericCaseH;

    mysurf.CaseInvisibleW = (int) (model.GenericCaseH * 1.50f);
    mysurf.CaseInvisiblePrecXmax = mysurf.CaseInvisibleW;
    mysurf.CaseInvisibleSuivXmin = mysurf.mywidth - mysurf.CaseInvisibleW;
    mysurf.CaseInvisiblePrecSuivYmin = mysurf.myheight - mysurf.CaseInvisibleW;
    mysurf.CaseInvisibleOptionXmax = mysurf.CaseInvisibleW;
    mysurf.CaseInvisibleOptionYmax = mysurf.CaseInvisibleW;
  }

  private void sendcommandtothread(String[] message) {
    if (mysurf.fragmenttype == Surf.FRAGMENT_TYPE_BROWSER) {
      try {
        model.commandethreadbrowser.put(message);
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
    } else if (mysurf.fragmenttype == Surf.FRAGMENT_TYPE_VIDEO) {
      try {
        model.commandethreadvideo.put(message);
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
    } else {
      llog.d(TAG, "error surf is neither browser nor video " + mysurf.fragmenttype);
    }
  }

  public OnFragmentInteractionListener fragmentlistener;
  
  public interface OnFragmentInteractionListener {
    // TODO: Update argument type and name
    Integer onFragmentInteraction(Uri uri);
  }
  
  @Override
  public void onAttach(Context context) {
    super.onAttach(context);
    if (context instanceof OnFragmentInteractionListener) {
      fragmentlistener = (OnFragmentInteractionListener) context;
    } else {
      llog.d(TAG, context.toString() + " must implement OnFragmentInteractionListener");
    }
  }
  
  
}






















