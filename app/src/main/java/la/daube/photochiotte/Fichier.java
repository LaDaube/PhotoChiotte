package la.daube.photochiotte;


import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity
public class Fichier {

  @PrimaryKey(autoGenerate = true)
  public int uid;

  @ColumnInfo(name = "fichier")
  public String fichier;

  @ColumnInfo(name = "dossier")
  public String dossier;

}
