package la.daube.photochiotte;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

public class WebViewFragment extends Fragment {

  private static final String NEWS_LINK_KEY = "NEWS_LINK";
  private String newsLink;
  private Gallery model;

  public static WebViewFragment newInstance(String uri) {
    Bundle args = new Bundle();
    args.putString(NEWS_LINK_KEY, uri);
    WebViewFragment webViewFragment = new WebViewFragment();
    webViewFragment.setArguments(args);
    return webViewFragment;
  }

  @Override
  public void onDestroy() {
    super.onDestroy();
  }

  @Override
  public void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    //model = ViewModelProviders.of(getActivity()).get(myViewModel.class);
    model = new ViewModelProvider(requireActivity()).get(Gallery.class);
    newsLink = this.getArguments().getString(NEWS_LINK_KEY);
  }

  @Nullable
  @Override
  public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
    return inflater.inflate(R.layout.webview, container, false);
  }

  @Override
  public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);
    WebView webView = (WebView) view.findViewById(R.id.web_view);

    // Enable Javascript
    WebSettings webSettings = webView.getSettings();
    webSettings.setJavaScriptEnabled(true);

    // Force links and redirects to open in the WebView instead of in a browser
    webView.setWebViewClient(new WebViewClient());

    webView.loadUrl(newsLink);
  }
}


