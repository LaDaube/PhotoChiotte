package la.daube.photochiotte;

// Wrapper for native library

import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import android.graphics.SurfaceTexture;
import android.view.Surface;


@SuppressWarnings("unused")
public class MPVLib {
    private static final String TAG = "YYYvlib";
    public final static int videoaspectother = 0;
    public final static int videoaspectscreen = 1;
    public final static int videoaspectvideo = 2;
    public final static int videoaspectboth = 3;

    // do not reset these
    public volatile int id = -1;
    public volatile boolean isinitialized = false;

    // reset these
    public volatile int propertyi = 0;
    public volatile ArrayList<String> log = new ArrayList<>();
    public volatile List<String[]> playlistdetail = new ArrayList();
    public volatile Map<Integer, Map<String, String>> vida = new HashMap<>();
    public volatile Map<Integer, Map<String, String>> aida = new HashMap<>();
    public volatile Map<Integer, Map<String, String>> sida = new HashMap<>();
    public volatile Map<String, String> mapmetadata = new HashMap<>();
    public volatile Map<String, String> mapdemuxercachestate = new HashMap<>();
    public volatile Map<String, String> mapvideoparam = new HashMap<>();
    public volatile Map<String, Integer> propertysi = new HashMap<>();
    public volatile String[] subtitles = new String[0];
    public volatile String[] subtitlespprint = new String[0];
    public volatile int statsluamode = 1;
    public volatile boolean hwdec = false;
    public volatile float audiodelay = 0.0f;
    public volatile float subtitledelay = 0.0f;
    public volatile int subfontsize = 55;
    public volatile int vid = 0;
    public volatile int aid = 0;
    public volatile int sid = 0;
    public volatile double volume = 100.0;
    public volatile double speed = 1.00;
    public volatile int abloop = 0;
    public volatile boolean loop = false;
    public volatile boolean maxout = false;
    public volatile double videozoom = 0.00;
    public volatile double videopanx = 0.00;
    public volatile double videopany = 0.00;
    public volatile double videorotate = 0.00;
    public volatile long lastdemuxercachestate = System.currentTimeMillis();
    public volatile float rawinputrate = 0;
    public volatile boolean idle = false;
    public volatile boolean underrun = false;
    public volatile int videowidth = 1;
    public volatile int videoheight = 1;
    public volatile int videoaspecttype = videoaspectother;
    public volatile double videoaspect = 1.0f;
    public volatile String streampath = "-";
    public volatile int lastplayedposition = 0;
    public volatile int extsubtitle = 0;
    public volatile String lastfileadded = null;
    public volatile String lastfolderadded = null;
    public volatile boolean playInSequence = false;
    public volatile int trackposition = 0;
    public volatile int framewidth = 1;
    public volatile int frameheight = 1;
    public volatile int demuxwidth = 1;
    public volatile int demuxheight = 1;
    public volatile boolean isshutdown = false;
    public volatile float timepos = -1;
    public volatile float duration = -1;
    public volatile String subtext = "";
    public volatile boolean ispaused = true;
    public volatile boolean ismutedfakevolume = false;
    public volatile String mediatitle = "-";
    public volatile boolean videoaskclose = false;
    public volatile int playlistnext = 0;
    public volatile int startfile = 0;
    public volatile int endfile = 0;
    public volatile int lastendfile = 0;
    public volatile int eventFinishedFailed = 0;
    public volatile int eventFatalNoVideo = 0;
    public volatile boolean videoformatautonomanualoverride = true;
    public volatile boolean jumptoposifbigaudiotrack = false;

    public void resetvars(){
        propertyi = 0;
        log.clear();
        playlistdetail.clear();
        vida.clear();
        aida.clear();
        sida.clear();
        mapmetadata.clear();
        mapdemuxercachestate.clear();
        mapvideoparam.clear();
        propertysi.clear();
        subtitles = new String[0];
        subtitlespprint = new String[0];
        statsluamode = 1;
        hwdec = false;
        audiodelay = 0.0f;
        subtitledelay = 0.0f;
        subfontsize = 55;
        vid = 0;
        aid = 0;
        sid = 0;
        volume = 100.0;
        speed = 1.00;
        abloop = 0;
        loop = false;
        maxout = false;
        videozoom = 0.00;
        videopanx = 0.00;
        videopany = 0.00;
        videorotate = 0.00;
        lastdemuxercachestate = System.currentTimeMillis();
        rawinputrate = 0;
        idle = false;
        underrun = false;
        videowidth = 1;
        videoheight = 1;
        videoaspecttype = videoaspectother;
        videoaspect = 1.0f;
        streampath = "-";
        lastplayedposition = 0;
        extsubtitle = 0;
        jumptoposifbigaudiotrack = false;
        lastfileadded = null;
        lastfolderadded = null;
        playInSequence = false;
        trackposition = 0;
        framewidth = 1;
        frameheight = 1;
        timepos = -1;
        duration = -1;
        subtext = "";
        ispaused = true;
        ismutedfakevolume = false;
        mediatitle = "-";
        playlistnext = 0;
        startfile = 0;
        endfile = 0;
        lastendfile = 0;
        eventFinishedFailed = 0;
        eventFatalNoVideo = 0;
        videoformatautonomanualoverride = true;
    }

    static {
        System.loadLibrary("player");
    }

    public MPVLib(int mclassindex) {
        id = mclassindex;
        create(id);
    }

    public int init(){
        isshutdown = false;
        videoaskclose = false;
        observeproperties();
        if (!isinitialized) {
            init(id);
            isinitialized = true;
        }
        return 0;
    }
    public int stop(){
        isshutdown = false;
        videoaskclose = true;
        setaid(0); // necessary otherwise locked mutex crash
        setvid(0);
        unobserveProperties();
        detachSurface();
        //command(new String[]{"quit"}, id);
        command(new String[]{"stop"}, id);
        resetvars();
        isshutdown = true;
        videoaskclose = true;
        return 0;
    }
    public int destroy(){ // cannot use, mpv leaves a dangling thread but otherwise crashes
        return destroy(id);
    }

    public void cyclepause() {
        ispaused = !ispaused;
        command(new String[]{"cycle", "pause"});
    }

    public void pause(boolean pause) {
        if (pause) {
            //setProperty("mute", "yes");
            setProperty("pause", "yes");
            ispaused = true;
        } else {
            //if (aid == 0)
            //    setaid(1);
            //setProperty("mute", "no");
            setProperty("pause", "no");
            ispaused = false;
        }
    }
    public final static int[] modeseek = new int[] {0, 1, 2, 5, 20, 60};
    public boolean seek(int currentmode , boolean right) {
        if (currentmode == 0) {
            if (!ispaused) {
                pause(true);
                //if (aid != 0)
                //    setaid(0);
            }
            if (right) {
                command(new String[]{"frame-step"});
            } else {
                command(new String[]{"frame-back-step"});
            }
        } else {
            if (right) {
                command(new String[]{"seek", String.format("+%d", modeseek[currentmode])});
            } else {
                if (playInSequence
                    && playlistcount > 1 && playlistpos > 0
                    && timepos >= 0 && duration > 0
                    && timepos - modeseek[currentmode] < 4 && timepos < 4
                ) {
                    llog.d(TAG, "load playlist file at pos 0");
                    setProperty("playlist-pos", "0");
                    return true;
                } else {
                    command(new String[]{"seek", String.format("-%d", modeseek[currentmode])});
                }
            }
        }
        return false;
    }

    public void gethwdec(){
        String get = getProperty("hwdec-current");
        if (get != null) {
            if (!get.equals("no")) {
                hwdec = true;
            }
        }
    }
    public void sethwdec(boolean set){
        if (set)
            setProperty("hwdec", "mediacodec-copy");
        else
            setProperty("hwdec", "no");
    }

    public void setaudiodelay(float set){
        setProperty("audio-delay", String.format("%.3f", set));
    }
    public void setsubtitledelay(float set){
        setProperty("sub-delay", String.format("%.3f", set));
    }
    public void setsubfontsize(int set){
        setProperty("sub-font-size", String.format("%d", set));
    }

    private static final Pattern patterndictfromlist = Pattern.compile("(\\{.*?\\})");
    public void parsetracklist(String get) {
        Map<String, String> trackinfo1 = new HashMap<>();
        trackinfo1.put("pprint", "no video");
        vida.put(0, trackinfo1);
        Map<String, String> trackinfo2 = new HashMap<>();
        trackinfo2.put("pprint", "no audio");
        aida.put(0, trackinfo2);
        Map<String, String> trackinfo3 = new HashMap<>();
        trackinfo3.put("pprint", "no subtitle");
        sida.put(0, trackinfo3);
        Matcher m1 = patterndictfromlist.matcher(get);
        while (m1.find()) {
            //llog.d(TAG, m1.group(0));
            Matcher m = patternkeyvalue.matcher(m1.group(0));
            Map<String, String> trackinfo = new HashMap<>();
            while (m.find()) {
                if (m.group(3) == null) {
                    //llog.d(TAG, m.group(1) + " : " + m.group(2));
                    trackinfo.put(m.group(1), m.group(2));
                } else {
                    //llog.d(TAG, m.group(1) + " : " + m.group(3));
                    trackinfo.put(m.group(1), m.group(3));
                }
            }
            if (trackinfo.containsKey("id") && trackinfo.containsKey("type")) {
                int id = Integer.parseInt(trackinfo.get("id"));
                String type = trackinfo.get("type");
                if (type.equals("video")) {
                    String info = "";
                    if (trackinfo.containsKey("demux-w")) {
                      int dw = Integer.parseInt(trackinfo.get("demux-w"));
                      if (dw > demuxwidth)
                        demuxwidth = dw;
                      info += " " + dw;
                    }
                    if (trackinfo.containsKey("demux-h")) {
                      int dw = Integer.parseInt(trackinfo.get("demux-h"));
                      if (dw > demuxheight)
                        demuxheight = dw;
                      info += "x" + dw;
                    }
                    if (trackinfo.containsKey("demux-fps")) {
                        String getlang = trackinfo.get("demux-fps");
                        int fp = getlang.indexOf(".");
                        if (fp != -1) {
                            info += ":" + getlang.substring(0, fp);
                        } else {
                            info += ":" + getlang;
                        }
                    }
                    if (trackinfo.containsKey("codec")) {
                        info += " " + trackinfo.get("codec");
                    }
                    trackinfo.put("pprint", info);
                    //llog.d(TAG, "   vtrack : " + info);
                    vida.put(id, trackinfo);

                } else if (type.equals("audio")) {
                    String info = "";
                    if (trackinfo.containsKey("lang")) {
                        info += " " + trackinfo.get("lang");
                    }
                    if (trackinfo.containsKey("codec")) {
                        info += " " + trackinfo.get("codec");
                    }
                    if (trackinfo.containsKey("demux-samplerate")) {
                        info += " " + trackinfo.get("demux-samplerate");
                    }
                    if (trackinfo.containsKey("hearing-impaired")) {
                        if (trackinfo.get("hearing-impaired").equals("true")) {
                            info += " h/";
                        }
                    }
                    if (trackinfo.containsKey("visual-impaired")) {
                        if (trackinfo.get("visual-impaired").equals("true")) {
                            info += " v/";
                        }
                    }
                    trackinfo.put("pprint", info);
                    //llog.d(TAG, "   atrack : " + info);
                    aida.put(id, trackinfo);
                } else if (type.equals("sub")) {
                    String info = "";
                    if (trackinfo.containsKey("lang")) {
                        info += " " + trackinfo.get("lang");
                    }
                    trackinfo.put("pprint", info);
                    //llog.d(TAG, "   strack : " + info);
                    sida.put(id, trackinfo);
                } else {
                    llog.d(TAG, "unknown type " + m1.group(0));
                }
            }
        }
    }

    public void getvid(){
        String get = getProperty("vid");
        if (get != null) {
            if (get.equals("no")) {
                vid = 0;
            } else if (get.equals("auto")) {
                if (vida.size() > 1)
                    vid = 1;
                else
                    vid = 0;
            } else {
                try {
                    vid = Integer.parseInt(get);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }
    public void setvid(int set){
        int vidal = vida.size();
        if (set > 0 && set < vidal)
            setProperty("vid", String.valueOf(set));
        else
            setProperty("vid", "no");
        getvid();
    }

    public void getaid(){
        String get = getProperty("aid");
        if (get != null) {
            if (get.equals("no")) {
                aid = 0;
            } else if (get.equals("auto")) {
                if (aida.size() > 1)
                    aid = 1;
                else
                    aid = 0;
            } else {
                try {
                    aid = Integer.parseInt(get);
                } catch (Exception e) {
                    llog.d(TAG, "aid not parsed " + get);
                }
            }
        }
    }
    public void setaiddefault(){
        if (aid == 1 || playInSequence) {
            int sel = 1;
            int aidal = aida.size();
            for (int i = 1; i < aidal; i++) {
                String pprint = aida.get(i).get("pprint");
                if (!pprint.endsWith(" v/") && !pprint.endsWith(" h/")) {
                    sel = i;
                    break;
                }
            }
            setaid(sel);
        }
    }
    public void setaid(int set){
        int aidal = aida.size();
        if (set > 0 && set < aidal)
            setProperty("aid", String.valueOf(set));
        else
            setProperty("aid", "no");
        getaid();
    }

    public void getsid(){
        String get = getProperty("sid");
        if (get != null) {
            if (get.equals("no")) {
                sid = 0;
            } else if (get.equals("auto")) {
                if (sida.size() > 1)
                    sid = 1;
                else
                    sid = 0;
            } else {
                try {
                    sid = Integer.parseInt(get);
                } catch (Exception e) {
                    llog.d(TAG, "sid not parsed " + get);
                }
            }
        }
    }
    public void setsid(int set){
        int sidal = sida.size();
        if (set > 0 && set < sidal)
            setProperty("sid", String.valueOf(set));
        else
            setProperty("sid", "no");
        getsid();
    }

    public void getvolume(){
        if (!ismutedfakevolume) {
            String get = getProperty("volume");
            if (get != null) {
                try {
                    volume = Double.parseDouble(get);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                //llog.d(TAG, id + " volume switched to " + volume + " ismutedfakevolume " + ismutedfakevolume);
            }
        }
    }
    public void setvolume(double set){
        setProperty("volume", String.format("%d", (int) set));
    }

    public void setmutereal(boolean mute){
        if (mute) {
            setProperty("aid", "no");
            //setProperty("mute", "yes");
        } else {
            setProperty("aid", "1");
            //setProperty("mute", "no");
        }
    }

    public void setmutefakevolume(boolean mute){
        //llog.d(TAG, id + " setmutefakevolume " + mute + " : " + volume);
        if (mute) {
            ismutedfakevolume = true;
            setProperty("volume", "0");
        } else {
            ismutedfakevolume = false;
            setProperty("volume", String.format("%d", (int) volume));
        }
    }

    public void getspeed(){
        String get = getProperty("speed");
        if (get != null) {
            try {
                speed = Double.parseDouble(get);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
    public void setspeed(double set){
        setProperty("speed", String.valueOf(set));
    }

    public void setabloop(){
        if (abloop == 0) { // il faut clear les deux sinon il revient plus tard b -> a
            setProperty("ab-loop-a", "no");
            setProperty("ab-loop-b", "no");
        } else
            command(new String[]{"ab-loop"});
    }

    public void setloop(boolean set) {
        if (set) {
            setProperty("loop-file", "inf");
            setProperty("eof", "yes");
        } else {
            setProperty("loop-file", "no");
        }
    }

    public void getzoom(){
        String get = getProperty("video-zoom");
        if (get != null) {
            try {
                videozoom = Double.parseDouble(get);
                //llog.d(TAG, "video-zoom " + videozoom);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
    public void setzoom(double set){
        setProperty("video-zoom", String.valueOf(set));
    }

    public void getpanx(){
        String get = getProperty("video-pan-x");
        if (get != null) {
            try {
                videopanx = Double.parseDouble(get);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
    public void setvideopanx(double set){
        setProperty("video-pan-x", String.valueOf(set));
    }

    public void getpany(){
        String get = getProperty("video-pan-y");
        if (get != null) {
            try {
                videopany = Double.parseDouble(get);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
    public void setvideopany(double set){
        setProperty("video-pan-y", String.valueOf(set));
    }

    public void getrotate(){
        String get = getProperty("video-rotate");
        if (get != null) {
            try {
                videorotate = Double.parseDouble(get);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
    public void setrotate(double set){
        setProperty("video-rotate", String.valueOf((int) set));
    }
    public String currfilter = null;
    public void setfilter(String filter){
        if (filter == null)
            setProperty("vf", "");
        else
            setProperty("vf", filter);
        currfilter = filter;
    }

    private static final Pattern patternkeyvalue = Pattern.compile("\"([^\"]+)\":(\"([^\"]+)\"|([^\",\\[\\]{}]+))");
    public void getmetadata(){
        String get = getProperty("metadata");
        if (get == null) {
            return;
        }
        parsemetadata(get);
    }
    public volatile String metadataToPrettyName = "";
    public void parsemetadata(String get){
        //llog.d(TAG, "========================================================================================");
        //llog.d(TAG, get);
        //mapmetadata.clear();
        //String get = getProperty("metadata");
        // {"MAJOR_BRAND":"dash","MINOR_VERSION":"0","COMPATIBLE_BRANDS":"iso6avc1mp41","ENCODER":"Lavf57.56.100"}
        Matcher m = patternkeyvalue.matcher(get);
        String key, val, name = "", artist = "", title = "";
        metadataToPrettyName = "";
        while (m.find()) {
            key = m.group(1).toLowerCase();
            if (m.group(3) == null)
                val = m.group(2);
            else
                val = m.group(3);
            if (key.contains("name"))
                name = val;
            if (key.contains("artist"))
                artist = val;
            if (key.contains("title"))
                title = val;
            llog.d(TAG, key + " : " + val);
            mapmetadata.put(key, val);
        }
        if (name.contains(artist)) {
            if (name.length() > 0)
                metadataToPrettyName += "/" + name;
        } else if (artist.contains(name)) {
            if (artist.length() > 0)
                metadataToPrettyName += "/" + artist;
        } else {
            if (name.length() > 0)
                metadataToPrettyName += "/" + name;
            if (artist.length() > 0)
                metadataToPrettyName += "/" + artist;
        }
        if (title.length() > 0)
            metadataToPrettyName += "/" + title;
    }

    public void getdemuxercachestate(){
        long newtime = System.currentTimeMillis();
        if (newtime - lastdemuxercachestate < 1000) {
            return;
        }
        String get = getProperty("demuxer-cache-state");
        if (get == null) {
            return;
        }
        /*
             {
                "cache-end":22.609000,
                "reader-pts":13.397000,
                "cache-duration":9.212000,
                "eof":false,                True if the reader thread has hit the end of the file.
                "underrun":false,           True if the reader thread could not satisfy a decoder's request for a new packet.
                "idle":false,               True if the thread is currently not reading.
                "total-bytes":13257480,     Sum of packet bytes (plus some overhead estimation) of the entire packet queue, including cached seekable ranges.
                "fw-bytes":5447768,         Sum of packet bytes (plus some overhead estimation) of the readahead packet queue
                                                       (packets between current decoder reader positions and demuxer position).
                "raw-input-rate":482418,
                "debug-low-level-seeks":0,
                "debug-byte-level-seeks":0,
                "debug-ts-last":82619.301000,
                "bof-cached":true,
                "eof-cached":false,
                "seekable-ranges":[
                    {
                        "start":0.000000,
                        "end":21.609000
                    }
                ]
            }
        */
        int i = 0;
        Matcher m = patternkeyvalue.matcher(get);
        while (m.find()) {
            if (m.group(3) == null) {
                //llog.d(TAG, m.group(1) + " : " + m.group(2));
                if (m.group(1).equals("start")) {
                    mapdemuxercachestate.put("start" + i, m.group(2));
                } else if (m.group(1).equals("end")) {
                    mapdemuxercachestate.put("end" + i, m.group(2));
                    i += 1;
                } else {
                    mapdemuxercachestate.put(m.group(1), m.group(2));
                }
            } else {
                //llog.d(TAG, m.group(1) + " : " + m.group(3));
                mapdemuxercachestate.put(m.group(1), m.group(3));
            }
        }
        if (mapdemuxercachestate.containsKey("raw-input-rate")) {
            float newrawinputrate = ((Float.parseFloat(mapdemuxercachestate.get("raw-input-rate")))) / 1000.0f;
            if (newrawinputrate == rawinputrate) {
                idle = true;
            }
            rawinputrate = newrawinputrate;
        } else {
            rawinputrate = 0.0f;
        }
        if (mapdemuxercachestate.containsKey("idle")) {
            if (mapdemuxercachestate.get("idle").equals("true")) {
                idle = true;
                rawinputrate = 0.0f;
            } else {
                idle = false;
            }
        }
        if (mapdemuxercachestate.containsKey("underrun")) {
            if (mapdemuxercachestate.get("underrun").equals("true")) {
                underrun = true;
            } else {
                underrun = false;
            }
        }
    }
    public void getvideoparams(){
        String get = getProperty("video-params");
        if (get == null) {
            return;
        }
        parsevideoparam(get);
    }
    public boolean parsevideoparam(String get){
        mapvideoparam.clear();
        Matcher m = patternkeyvalue.matcher(get);
        while (m.find()) {
            if (m.group(3) == null) {
                mapvideoparam.put(m.group(1), m.group(2));
            } else {
                mapvideoparam.put(m.group(1), m.group(3));
            }
        }
        if (mapvideoparam.containsKey("aspect")) {
          // pas dans certaines videos hq
          videoaspect = Double.parseDouble(mapvideoparam.get("aspect"));
        }
        if (mapvideoparam.containsKey("w")) {
            videowidth = Integer.parseInt(mapvideoparam.get("w"));
        }
        if (mapvideoparam.containsKey("h")) {
            videoheight = Integer.parseInt(mapvideoparam.get("h"));
        }
        return true;
    }

    public boolean aspectCheckIfVideoOrScreen(){
        if (videowidth == 1 && videoheight == 1) {
            llog.d("TAG", "calculatevideoaspect wait for video-params/aspect");
            return false;
        } else {
            int vatype = 0;
            if (frameheight > 0) {
                if (Math.abs(videoaspect - ((float) framewidth / (float) frameheight)) < 0.01f) {
                    videoaspecttype = videoaspectscreen;
                    vatype += 1;
                }
            }
            if (videoheight > 0) {
                if (Math.abs(videoaspect - ((float) videowidth / (float) videoheight)) < 0.01f) {
                    videoaspecttype = videoaspectvideo;
                    vatype += 1;
                }
            }
            if (vatype == 2) {
                videoaspecttype = videoaspectboth;
            } else if (vatype == 0) {
                videoaspecttype = videoaspectother;
            } else if (videoaspecttype == videoaspectvideo) {
            } else if (videoaspecttype == videoaspectscreen) {
            }
            return true;
        }
    }
    public void setvideoaspect(double mvideoaspect){
        setProperty("video-aspect-override", String.valueOf(mvideoaspect));
    }
    public boolean setvideoaspectscreen(){
        if (frameheight > 0) {
            double newaspect = (double) framewidth / (double) frameheight;
            if (Math.abs(videoaspect - newaspect) < 0.01f)
                llog.d(TAG, " ------------ setvideoaspectscreen but video aspect remains the same " + newaspect);
            setvideoaspect(newaspect);
        }
        return false;
    }
    public boolean setvideoaspectvideo(){
        if (videoheight > 0) {
            double newaspect = (double) videowidth / (double) videoheight;
            if (Math.abs(videoaspect - newaspect) < 0.01f)
                llog.d(TAG, " ------------ setvideoaspectvideo but video aspect remains the same " + newaspect);
            setvideoaspect(newaspect);
        }
        return false;
    }

    public void getstreampath(){
        String get = getProperty("stream-path");
        if (get != null)
            streampath = get;
        else
            streampath = "-";
    }

    public int playlistcount = 0;
    public int playlistpos = -1;

    public String getplaylist(){
        try {
            playlistpos = Integer.parseInt(getProperty("playlist-pos"));
            playlistcount = Integer.parseInt(getProperty("playlist-count"));
        } catch (Exception e) {
            llog.d(TAG, "could not parse");
        }
        return playlistpos + "/" + playlistcount;
    }
    public void getduration(){
        String get = getProperty("duration");
        llog.d(TAG, "get duration ==================== " + get);
        try {
            duration = Float.parseFloat(get);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void addsubtitle(int subtitlenumber) {
        command(new String[]{"sub-remove"});
        command(new String[]{"sub-add", subtitles[subtitlenumber]});
    }

    public void clearplaylist(){
        command(new String[]{"playlist-clear"});
    }

    public void loadfilesetat0(String filename){
        command(new String[]{"loadfile", filename, "insert-at", "0"});
    }
    public void loadfile(String filename, boolean append){
        if (!append) {
            command(new String[]{"loadfile", filename, "replace"});
            //command(new String[]{"loadfile", filename, "replace", "sub-file=/storage/emulated/0/bb.srt"});
        } else {
            command(new String[]{"loadfile", filename, "append"});
        }
    }


    public void settimepos(int set){
        setProperty("time-pos", String.valueOf(set));
    }

    public int setOptionString(String name, String value){
      //llog.d(TAG, "setOptionString " + name + "<" + value + ">");
      return setOptionString(name, value, id);
    }
    public String getProperty(String property){
        return getProperty(property, id);
    }
    public int setProperty(String property, String value){
      //llog.d(TAG, "setProperty " + property + "<" + value + ">");
      return setProperty(property, value, id);
    }

    public int observeProperty(String property, int format){
        propertyi += 1;
        propertysi.put(property, propertyi);
        return observeProperty(property, propertyi, format, id);
    }
    public int unobserveProperties(){
        for (String property : propertysi.keySet()) {
            if (propertysi.containsKey(property)) {
                unobserveProperty(propertysi.get(property), id);
            } else {
                llog.d(TAG, "unobserveProperty property not found : " + property);
            }
        }
        return 0;
    }
    public int unobserveProperty(String property){
        if (propertysi.containsKey(property)) {
            return unobserveProperty(propertysi.get(property), id);
        } else {
            llog.d(TAG, "unobserveProperty property not found : " + property);
        }
        return -2;
    }
    public void observeproperties() {
        observeProperty("time-pos", MPVLib.mpvFormat.MPV_FORMAT_INT64);
        observeProperty("duration", MPVLib.mpvFormat.MPV_FORMAT_INT64);
        observeProperty("pause", MPVLib.mpvFormat.MPV_FORMAT_FLAG);
        observeProperty("track-list", MPVLib.mpvFormat.MPV_FORMAT_STRING);
        observeProperty("video-params", MPVLib.mpvFormat.MPV_FORMAT_STRING);
        observeProperty("video-format", MPVLib.mpvFormat.MPV_FORMAT_STRING);
        observeProperty("playlist-pos", MPVLib.mpvFormat.MPV_FORMAT_STRING);
        observeProperty("playlist-count", MPVLib.mpvFormat.MPV_FORMAT_STRING);
        observeProperty("hwdec-current", MPVLib.mpvFormat.MPV_FORMAT_STRING);
        observeProperty("volume", MPVLib.mpvFormat.MPV_FORMAT_STRING);
        observeProperty("metadata", MPVLib.mpvFormat.MPV_FORMAT_STRING);
        observeProperty("stream-path", MPVLib.mpvFormat.MPV_FORMAT_STRING);
        //observeProperty("demuxer-cache-state", MPVLib.mpvFormat.MPV_FORMAT_STRING); trop souvent
        //if (model.videohwaamode == 0)
        observeProperty("sub-text", MPVLib.mpvFormat.MPV_FORMAT_STRING);
        //observeProperty("vo-configured", MPVLib.mpvFormat.MPV_FORMAT_STRING);
    }

    public void unobserverproperties() {
        unobserveProperty("time-pos");
        unobserveProperty("duration");
        unobserveProperty("pause");
        unobserveProperty("track-list");
        unobserveProperty("video-params");
        unobserveProperty("video-format");
        unobserveProperty("playlist-pos");
        unobserveProperty("playlist-count");
        unobserveProperty("hwdec-current");
        unobserveProperty("volume");
        unobserveProperty("metadata");
        unobserveProperty("stream-path");
        //unobserveProperty("demuxer-cache-state"); trop souvent
        unobserveProperty("sub-text");
        //unobserveProperty("vo-configured");
    }

    public int attachSurface(SurfaceTexture surfacetexture){
        Surface surface = new Surface(surfacetexture);
        return attachSurface(surface, id);
    }
    public int attachSurface(Surface surface){
        return attachSurface(surface, id);
    }
    public int detachSurface(){
        return detachSurface(id);
    }
    public int command(String[] cmd){
        return command(cmd, id);
    }
    public int streamcbAddRo(String protocol, ByteBuffer data){
        return streamcbAddRo(protocol, data, id);
    }
    public int sendBufferRegion(int end){
        return sendBufferRegion(end, id);
    }
    public int grabThumbnail(ByteBuffer data){
        return -1;//grabThumbnail(id, data);
    }
    final static int maxlen = 1000000; // no string bigger than this
    public ArrayList<Integer> getEvents(ByteBuffer buffer) {
        ArrayList<Integer> rc = new ArrayList<>();
        int pf = getEvents(id, buffer);
        if (pf < 1)
          return rc;

        buffer.rewind();
        int id = buffer.get();
        //llog.d(TAG, "mpvlib id = " + id);
        while (buffer.position() < pf) {
            int eventid = buffer.get();
            //llog.d(TAG, "eventid " + eventid + " : buffer pos " + buffer.position() + " < " + pf);

            switch (eventid) {
                case MPVLib.mpvEventId.MPV_EVENT_NONE:
                    break;
                case MPVLib.mpvEventId.MPV_EVENT_LOG_MESSAGE: {

                    int loglevel = buffer.get();
                    StringBuilder sprefix = new StringBuilder();
                    for (int i = 0 ; i < maxlen ; i++){
                        byte car = buffer.get();
                        if (0x20 <= car && car <= 0x7e || car == 0xa)
                            sprefix.append((char) car);
                        else if (car == 0x0)
                            break;
                    }
                    String prefix = sprefix.toString();
                    StringBuilder sdata = new StringBuilder();
                    for (int i = 0 ; i < maxlen ; i++){
                        byte car = buffer.get();
                        if (0x20 <= car && car <= 0x7e || car == 0xa)
                            sdata.append((char) car);
                        else if (car == 0x0)
                            break;
                    }
                    String data = sdata.toString();

                    //if (loglevel != 50)
                        llog.d(TAG, loglevel + " : " + prefix + " : " + data);

                    if (loglevel == mpvLogLevel.MPV_LOG_LEVEL_FATAL) {
                        if (data.equals("Could not initialize video chain.\n")) {
                            eventFatalNoVideo++;
                            eventFinishedFailed++;
                        } else if (data.equals("Error opening/initializing the selected video_out (--vo) device.\n")) {
                            eventFatalNoVideo++;
                            eventFinishedFailed++;
                        }
                    }
                    if (data.equals("EOF reached.\n")) {
                        llog.d(TAG, id + "                               eof reached");
                    }

                    // TODO : not in build
                    int logsize = log.size();
                    if (logsize < 600) {
                        //llog.d(TAG, "MPV_EVENT_LOG_MESSAGE " + data);
                        int premcar = 0;
                        int datal = data.length();
                        while (premcar < datal) {
                            if (data.charAt(0) == ' ')
                                premcar += 1;
                            else
                                break;
                        }
                        if (premcar > 0) {
                            data = data.substring(premcar);
                        }
                        if (!data.isEmpty()) {
                            if (logsize > 0) {
                                if (!data.equals(log.get(logsize - 1))) {
                                    log.add(data);
                                }
                            } else {
                                log.add(data);
                            }
                        }
                    }
                    if (
                            (data.contains("finished playback, loading failed (reason ")
                                    || data.contains("finished playback, unrecognized file format (reason ")
                                    || data.contains("finished playback, no audio or video data played (reason ")
                            )
                    ) {
                        eventFinishedFailed++;
                        llog.d(TAG, data);
                    }
                    /*if (data.startsWith("delaying audio start ") && delayingaudiostart != -1) {
                        delayingaudiostart += 1;
                    }*/
                    break;
                }
                case MPVLib.mpvEventId.MPV_EVENT_PROPERTY_CHANGE: {

                    int format = buffer.get();
                    StringBuilder sname = new StringBuilder();
                    for (int i = 0 ; i < maxlen ; i++){
                        byte car = buffer.get();
                        if (0x20 <= car && car <= 0x7e)
                            sname.append((char) car);
                        else if (car == 0x0)
                            break;
                    }
                    String name = sname.toString();
                    String data = null;
                    int o, p;
                    switch (format) {
                        case MPVLib.mpvFormat.MPV_FORMAT_NONE:
                            break;
                        case MPVLib.mpvFormat.MPV_FORMAT_FLAG:
                            data = String.valueOf(buffer.getInt());
                            break;
                        case MPVLib.mpvFormat.MPV_FORMAT_INT64:{
                            data = String.valueOf(buffer.getLong());
                            break;
                        }
                        case MPVLib.mpvFormat.MPV_FORMAT_DOUBLE:
                            data = String.valueOf(buffer.getDouble());
                            break;
                        case MPVLib.mpvFormat.MPV_FORMAT_STRING:
                        case MPVLib.mpvFormat.MPV_FORMAT_OSD_STRING: {
                            /*
                            StringBuilder sdata = new StringBuilder();
                            for (int i = 0; i < maxlen; i++) {
                                byte bar = buffer.get();
                                char car = (char) (bar & 0xff); // nécessaire sinon bar est signé
                                if ((0x0 < car && car < 0x20) || car == 0x7f) {
                                    //llog.d(TAG, String.format("new char : 0x%02x", (int) car));
                                    sdata.append(' ');
                                } else if (0x20 <= car && car <= 0x7e)
                                    sdata.append(car);
                                else if (car == 0x0)
                                    break;
                                else if (car <= 0xdf) {
                                    byte[] utf = new byte[]{
                                            bar,
                                            buffer.get()
                                    };
                                    String spe = new String(utf, StandardCharsets.UTF_8);
                                    sdata.append(spe);
                                    //llog.d(TAG, "new char : car <= (byte) 0xdf " + spe);
                                } else if (car <= 0xef) {
                                    byte[] utf = new byte[]{
                                            bar,
                                            buffer.get(),
                                            buffer.get()
                                    };
                                    String spe = new String(utf, StandardCharsets.UTF_8);
                                    sdata.append(spe);
                                    //llog.d(TAG, "new char : car <= (byte) 0xef " + spe);
                                } else {
                                    byte[] utf = new byte[]{
                                            bar,
                                            buffer.get(),
                                            buffer.get(),
                                            buffer.get()
                                    };
                                    String spe = new String(utf, StandardCharsets.UTF_8);
                                    sdata.append(spe);
                                    //llog.d(TAG, "new char : car <= (byte) 0xff " + spe);
                                }
                            }
                            data = sdata.toString();
                            */
                            ByteBuffer buf = ByteBuffer.allocateDirect(65536);
                            int i = 0;
                            while (i < 65536) {
                                byte bar = buffer.get();
                                char car = (char) (bar & 0xff); // nécessaire sinon bar est signé
                                if (car == 0x0) // ne pas mettre le 0 car inclus dans string et pas lu par log
                                    break;
                                buf.put(bar);
                                i++;
                            }
                            buf.flip();
                            data = StandardCharsets.UTF_8.decode(buf).toString();
                            buf.clear();
                            break;
                        }
                        case MPVLib.mpvFormat.MPV_FORMAT_NODE:
                        case MPVLib.mpvFormat.MPV_FORMAT_NODE_ARRAY:
                        case MPVLib.mpvFormat.MPV_FORMAT_NODE_MAP:
                        case MPVLib.mpvFormat.MPV_FORMAT_BYTE_ARRAY:
                        default:
                            llog.d(TAG, "========================= NEW FORMAT TO HANDLE");
                            break;
                    }
                    // TODO : not in build
                    //llog.d(TAG, "----------------------------------" + format + " : " + name + " : " + data);
                    // TODO : not in build
                    if (data != null) {
                        switch (name) {
                            case "pause":
                                llog.d(TAG, "    <" + name + ":" + data + ">" + format);
                                if (format == MPVLib.mpvFormat.MPV_FORMAT_FLAG) {
                                    if (data.equals("0"))
                                        ispaused = false;
                                    else if (data.equals("1"))
                                        ispaused = true;
                                }
                                break;
                            case "duration":
                                llog.d(TAG, "    <" + name + ":" + data + ">" + format);
                                if (format == MPVLib.mpvFormat.MPV_FORMAT_INT64) {
                                    duration = (int) Long.parseLong(data);
                                    rc.add(EVENT_LOOK_LAST_PLAYED_POSITION);
                                    /*if (jumptoposifbigaudiotrack) { too early here
                                        if (18 * 60 < duration && duration < 3 * 60 * 60) {
                                            int rjum = Gallery.rand.nextInt((int) duration);
                                            llog.d(TAG, " been asked to jump to track pos " + rjum + " / " + duration);
                                            settimepos(rjum);
                                            jumptoposifbigaudiotrack = false;
                                        }
                                    }*/
                                }
                                break;
                            case "time-pos":
                                if (format == MPVLib.mpvFormat.MPV_FORMAT_INT64) {
                                    timepos = (int) Long.parseLong(data);
                                    if (timepos == 1 || timepos == 5) {
                                      rc.add(EVENT_LOOK_LAST_PLAYED_POSITION);
                                    }
                                }
                                break;
                            case "sub-text":
                                if (format == MPVLib.mpvFormat.MPV_FORMAT_STRING) {
                                    subtext = data;
                                    rc.add(EVENT_DISPLAY_SUB);
                                }
                                break;

                            case "hwdec-current":
                                llog.d(TAG, "    <" + name + ":" + data + ">" + format);
                                if (format == MPVLib.mpvFormat.MPV_FORMAT_STRING) {
                                    if (data.equals("no"))
                                        hwdec = false;
                                    else
                                        hwdec = true;
                                }
                                break;
                            case "volume":
                                llog.d(TAG, "    <" + name + ":" + data + ">" + format);
                                if (!ismutedfakevolume) {
                                    if (format == MPVLib.mpvFormat.MPV_FORMAT_STRING) {
                                        try {
                                            volume = Double.parseDouble(data);
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                    }
                                }
                                break;
                            case "metadata":
                                llog.d(TAG, "    <" + name + ":" + data + ">" + format);
                                if (format == MPVLib.mpvFormat.MPV_FORMAT_STRING) {
                                    parsemetadata(data);
                                }
                                break;
                            case "video-params":
                                llog.d(TAG, "    <" + name + ":" + data + ">" + format + "  videoformatautonomanualoverride " + videoformatautonomanualoverride);
                                if (format == MPVLib.mpvFormat.MPV_FORMAT_STRING) {
                                    boolean success = parsevideoparam(data);
                                    if (success)
                                      if (videoformatautonomanualoverride)
                                          rc.add(EVENT_GOT_VIDEO_FORMAT_CAN_CORRECT);
                                }
                                break;
                            case "track-list":
                                llog.d(TAG, "    <" + name + ":" + data + ">" + format);
                                if (format == MPVLib.mpvFormat.MPV_FORMAT_STRING) {
                                    parsetracklist(data);
                                    getvid();
                                    getaid();
                                    getsid();
                                }
                                break;
                            case "stream-path":
                                llog.d(TAG, "    <" + name + ":" + data + ">" + format);
                                if (format == MPVLib.mpvFormat.MPV_FORMAT_STRING) {
                                    streampath = data;
                                    rc.add(EVENT_LOOK_LAST_PLAYED_POSITION);
                                }
                                break;
                            case "video-aspect":
                            case "video-format":
                            case "media-title":
                            default:
                                llog.d(TAG, "    <" + name + ":" + data + ">" + format);
                                break;
                        }
                    }
                    break;
                }
                case MPVLib.mpvEventId.MPV_EVENT_PLAYBACK_RESTART: {
                    getvid();
                    getaid();
                    getsid();
                    llog.d(TAG, id + " ===============================       MPV_EVENT_PLAYBACK_RESTART");
                    break;
                }
                case MPVLib.mpvEventId.MPV_EVENT_START_FILE:
                    startfile++;
                    llog.d(TAG, id + " ===============================       MPV_EVENT_START_FILE lastendfile " + lastendfile + " endfile " + endfile + " startfile " + startfile + " eventFinishedFailed " + eventFinishedFailed);
                    rc.add(EVENT_START_FILE_APPEND_NEXT_SEQUENCE);
                    break;
                case mpvEventId.MPV_EVENT_SEEK:
                    llog.d(TAG, id + " ===============================       MPV_EVENT_SEEK");
                    break;
                case MPVLib.mpvEventId.MPV_EVENT_SHUTDOWN:
                    llog.d(TAG, id + " ===============================       MPV_EVENT_SHUTDOWN");
                    break;
                //case MPVLib.mpvEventId.MPV_EVENT_IDLE:
                //    llog.d(TAG, id + " ===============================       MPV_EVENT_IDLE");
                //    break;
                case MPVLib.mpvEventId.MPV_EVENT_END_FILE:
                    endfile++;
                    llog.d(TAG, id + " ===============================       MPV_EVENT_END_FILE lastendfile " + lastendfile + " endfile " + endfile + " startfile " + startfile);
                    break;
                case MPVLib.mpvEventId.MPV_EVENT_FILE_LOADED:
                    llog.d(TAG, id + " ===============================       MPV_EVENT_FILE_LOADED");
                    break;
                case MPVLib.mpvEventId.MPV_EVENT_AUDIO_RECONFIG:
                    llog.d(TAG, id + " ===============================       MPV_EVENT_AUDIO_RECONFIG");
                    break;
                case MPVLib.mpvEventId.MPV_EVENT_VIDEO_RECONFIG:
                    llog.d(TAG, id + " ===============================       MPV_EVENT_VIDEO_RECONFIG");
                    break;
                default :
                    llog.d(TAG, id + " ===============================       OTHER_EVENT " + eventid);
                    break;
            }
        }

        if (eventFinishedFailed > 0) {
            llog.d(TAG, id + " eventFinishedFailed lastendfile " + lastendfile + " endfile " + endfile + " startfile " + startfile);
            rc.add(EVENT_END_FILE);
            lastendfile = endfile;
            videoformatautonomanualoverride = true;
        } else if (endfile == startfile && endfile > lastendfile) { // if (startfile > endfile) media playing
            llog.d(TAG, id + " endfile == startfile lastendfile " + lastendfile + " endfile " + endfile + " startfile " + startfile);
            rc.add(EVENT_END_FILE);
            lastendfile = endfile;
            videoformatautonomanualoverride = true;
        }

        return rc;
    }

    public static final int EVENT_LOOK_LAST_PLAYED_POSITION = 2;
    public static final int EVENT_GOT_VIDEO_FORMAT_CAN_CORRECT = 3;
    public static final int EVENT_END_FILE = 5;
    public static final int EVENT_START_FILE_APPEND_NEXT_SEQUENCE = 6;
    public static final int EVENT_DISPLAY_SUB = 7;

    public static native int create(int id);
    public static native int init(int id);
    public static native int destroy(int id);
    public static native int setOptionString(String name, String value, int id);
    public static native int setProperty(String property, String value, int id);
    public static native String getProperty(String property, int id);
    public static native int observeProperty(String property, int propertyid, int format, int id);
    public static native int unobserveProperty(int propertyid, int id);
    public static native int attachSurface(Surface surface, int id);
    public static native int detachSurface(int id);
    public static native int command(String[] cmd, int id);
    public static native int streamcbAddRo(String protocol, ByteBuffer data, int id);
    public static native int sendBufferRegion(int end, int id);
    public static native int getEvents(int id, ByteBuffer data);

    public static class mpvFormat {
        public static final int MPV_FORMAT_NONE=0;
        public static final int MPV_FORMAT_STRING=1;
        public static final int MPV_FORMAT_OSD_STRING=2;
        public static final int MPV_FORMAT_FLAG=3;
        public static final int MPV_FORMAT_INT64=4;
        public static final int MPV_FORMAT_DOUBLE=5;
        public static final int MPV_FORMAT_NODE=6;
        public static final int MPV_FORMAT_NODE_ARRAY=7;
        public static final int MPV_FORMAT_NODE_MAP=8;
        public static final int MPV_FORMAT_BYTE_ARRAY=9;
    }

    public static class mpvEventId {
        public static final int MPV_EVENT_NONE=0;
        public static final int MPV_EVENT_SHUTDOWN=1;
        public static final int MPV_EVENT_LOG_MESSAGE=2;
        public static final int MPV_EVENT_GET_PROPERTY_REPLY=3;
        public static final int MPV_EVENT_SET_PROPERTY_REPLY=4;
        public static final int MPV_EVENT_COMMAND_REPLY=5;
        public static final int MPV_EVENT_START_FILE=6;
        public static final int MPV_EVENT_END_FILE=7;
        public static final int MPV_EVENT_FILE_LOADED=8;
        public static final @Deprecated int MPV_EVENT_TRACKS_CHANGED=9;
        public static final @Deprecated int MPV_EVENT_TRACK_SWITCHED=10;
        public static final @Deprecated int MPV_EVENT_IDLE=11;
        public static final @Deprecated int MPV_EVENT_PAUSE=12;
        public static final @Deprecated int MPV_EVENT_UNPAUSE=13;
        public static final int MPV_EVENT_TICK=14;
        public static final @Deprecated int MPV_EVENT_SCRIPT_INPUT_DISPATCH=15;
        public static final int MPV_EVENT_CLIENT_MESSAGE=16;
        public static final int MPV_EVENT_VIDEO_RECONFIG=17;
        public static final int MPV_EVENT_AUDIO_RECONFIG=18;
        public static final @Deprecated int MPV_EVENT_METADATA_UPDATE=19;
        public static final int MPV_EVENT_SEEK=20;
        public static final int MPV_EVENT_PLAYBACK_RESTART=21;
        public static final int MPV_EVENT_PROPERTY_CHANGE=22;
        public static final @Deprecated int MPV_EVENT_CHAPTER_CHANGE=23;
        public static final int MPV_EVENT_QUEUE_OVERFLOW=24;
        public static final int MPV_EVENT_HOOK=25;
    }

    public static class mpvLogLevel {
        public static final int MPV_LOG_LEVEL_NONE=0;
        public static final int MPV_LOG_LEVEL_FATAL=10;
        public static final int MPV_LOG_LEVEL_ERROR=20;
        public static final int MPV_LOG_LEVEL_WARN=30;
        public static final int MPV_LOG_LEVEL_INFO=40;
        public static final int MPV_LOG_LEVEL_V=50;
        public static final int MPV_LOG_LEVEL_DEBUG=60;
        public static final int MPV_LOG_LEVEL_TRACE=70;
    }
}
