package la.daube.photochiotte;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity
public class Dossier {
  @PrimaryKey(autoGenerate = true)
  public long uid;

  @ColumnInfo(name = "dossier")
  public String dossier;
  
  @ColumnInfo(name = "fichierpardefaut")
  public int fichierpardefaut;

}
