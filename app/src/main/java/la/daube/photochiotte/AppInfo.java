package la.daube.photochiotte;

import android.graphics.Bitmap;

import java.util.ArrayList;

public class AppInfo {
  public String appname = "";
  public String packagename = "";
  public String versionname = "";
  public String targetsdkversion = "";
  public String sourcedir = "";
  public long firstinstall = 0;
  public long lastupdate = 0;
  public String comment = "";
  public int reenabled = 0;
  public int statuswanted = 0;
  public int statuscurrent = 0;

  public int permissionl = 0;
  public String[] permissionname = new String[0];
  public int[] permissionstatuswanted = new int[0];
  public int[] permissionstatuscurrent = new int[0];

  public int servicerunningl = 0;
  public int servicel = 0;
  public String[] servicename = new String[0];
  public int[] servicestatuswanted = new int[0];
  public int[] servicestatuscurrent = new int[0];

  public int activityl = 0;
  public String[] activityname = new String[0];
  public int[] activitystatuswanted = new int[0];
  public int[] activitystatuscurrent = new int[0];
}
