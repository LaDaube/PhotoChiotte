package la.daube.photochiotte;

import java.util.List;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;
import androidx.room.Delete;

@Dao
public interface DBDossier {
  @Query("SELECT * FROM dossier ORDER BY dossier")
  List<Dossier> getAll();

  @Query("SELECT dossier FROM dossier WHERE dossier LIKE :first LIMIT 1")
  String dossierDejaDansDB(String first);

  @Query("SELECT * FROM dossier WHERE uid IN (:userIds)")
  List<Dossier> loadAllByIds(int[] userIds);

  @Query("SELECT * FROM dossier WHERE dossier LIKE :first LIMIT 1")
  Dossier findByName(String first);

  @Query("DELETE FROM dossier WHERE dossier LIKE :first || '%'")
  void deleteFolderOrZipStartswith(String first);

  @Query("DELETE FROM dossier WHERE dossier LIKE :first")
  void deleteFolderOrZip(String first);

  @Query("UPDATE dossier SET fichierpardefaut = :indice WHERE dossier LIKE :first")
  void updateFichierParDefaut(String first, int indice);

  @Insert
  long[] insertAll(List<Dossier> dossiers);

  @Update
  void update(Dossier dossier);

  @Insert
  void insert(Dossier dossier);

  @Delete
  void delete(Dossier dossier);
}
