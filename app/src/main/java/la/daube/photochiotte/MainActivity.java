package la.daube.photochiotte;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.UiModeManager;
import android.app.WallpaperManager;
import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.AssetManager;
import android.content.res.Configuration;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Rect;
import android.graphics.RectF;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.core.view.InputDeviceCompat;
import androidx.core.view.MotionEventCompat;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowCompat;
import androidx.core.view.WindowInsetsCompat;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.DefaultLifecycleObserver;
import androidx.lifecycle.LifecycleObserver;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.OnLifecycleEvent;
import androidx.preference.PreferenceManager;

import android.os.PowerManager;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.os.Bundle;
import android.provider.OpenableColumns;
import android.provider.Settings;
import android.util.DisplayMetrics;

import android.view.Display;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.WindowMetrics;
import android.webkit.JavascriptInterface;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

//import androidx.core.content.FileProvider;
import androidx.documentfile.provider.DocumentFile;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.ViewModelProvider;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.room.Room;
import androidx.fragment.app.Fragment;
//import androidx.wear.ambient.AmbientLifecycleObserver;
//import androidx.wear.ambient.AmbientLifecycleObserverKt;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

//import la.daube.photochiotte.ghost.*;

import static android.provider.MediaStore.MediaColumns.RELATIVE_PATH;

public class MainActivity extends FragmentActivity
        implements
        FragmentDrawing.OnFragmentInteractionListener,
        FragmentBrowser.OnFragmentInteractionListener,
        FragmentGamePuzzle.OnFragmentInteractionListener,
        FragmentGameBounceCurve.OnFragmentInteractionListener,
        FragmentGameBounceHit.OnFragmentInteractionListener,
        FragmentGameTilt.OnFragmentInteractionListener,
        FragmentGameTiltAvoid.OnFragmentInteractionListener,
        FragmentGameSwap.OnFragmentInteractionListener,
    DefaultLifecycleObserver {

  private static final String TAG = "YYYa";
  private final static int MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE = 88;
  private final static int MY_PERMISSIONS_REQUEST_POST_NOTIFICATIONS = 89;
  private static final int READ_REQUEST_CODE = 42;
  private static final int DELETEFILES = 47;
  private static final int MOVEFILES = 48;
  private static final int COPYFILES = 49;
  private final static int layoutid = 12345;

  private LocalBroadcastManager localbroad;
  private RelativeLayout mainlayout = null;
  private ImageView status = null;
  private EditText input = null;
  private Button inputvalidate = null;
  private WebView webview = null;
  private WebView webviewhidden = null;

  private Gallery model = null;

  @Override
  public void onAttachedToWindow() {
    super.onAttachedToWindow();
    //mAmbientLifecycleObserver = AmbientLifecycleObserverKt.AmbientLifecycleObserver(this, mAmbientCallback);
    //getLifecycle().addObserver(mAmbientLifecycleObserver);
    llog.d(TAG, "onAttachedToWindow");
  }
  /*private AmbientLifecycleObserver mAmbientLifecycleObserver;
  final AmbientLifecycleObserver.AmbientLifecycleCallback mAmbientCallback = new AmbientLifecycleObserver.AmbientLifecycleCallback() {
    @Override
    public void onEnterAmbient(AmbientLifecycleObserver.AmbientDetails ambientDetails) {
      llog.d(TAG, " ... Called when moving from interactive mode into ambient mode.");
    }
    @Override
    public void onUpdateAmbient() {
      llog.d(TAG, " ... Called when leaving ambient mode, back into interactive mode.");
    }
    @Override
    public void onExitAmbient() {
      llog.d(TAG, " ... Called by the system in order to allow the app to periodically " +
          "update the display while in ambient mode. Typically the system will call this every 60 seconds.");
    }
  };*/

  @Override
  public void onWindowFocusChanged(boolean hasFocus) {
    super.onWindowFocusChanged(hasFocus);
    llog.d(TAG, "onWindowFocusChanged Focus " + hasFocus);
    //if (model != null)
    //  model.appinfocus = hasFocus;
    /*if (hasFocus) {
      hideSystemUI();
    } else {
      llog.d(TAG, "onWindowFocusChanged has lost Focus");
    }*/
  }

  void deletefiles(File fileOrDirectory) {
    final long newtime = System.currentTimeMillis();
    if (fileOrDirectory.isDirectory()) {
      File[] children = fileOrDirectory.listFiles();
      if (children != null)
        for (File child : children)
          if (child != null)
            deletefiles(child);
    } else {
      fileOrDirectory.delete();
    }
  }

  private void hideSystemUI() {
    try {
      // Enables regular immersive mode.
      // For "lean back" mode, remove SYSTEM_UI_FLAG_IMMERSIVE.
      // Or for "sticky immersive," replace it with SYSTEM_UI_FLAG_IMMERSIVE_STICKY
      View decorView = getWindow().getDecorView();
      decorView.setSystemUiVisibility(
          View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
              //View.SYSTEM_UI_FLAG_IMMERSIVE
              // Set the content to appear under the system bars so that the
              // content doesn't resize when the system bars hide and show.
              | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
              | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
              | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
              // Hide the nav bar and status bar
              | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
              | View.SYSTEM_UI_FLAG_FULLSCREEN);
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  // Shows the system bars by removing all the flags
  // except for the ones that make the content appear under the system bars.
  private void showSystemUI() {
    View decorView = getWindow().getDecorView();
    decorView.setSystemUiVisibility(
            View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                    | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
  }

  @Override
  public void onLowMemory() {
    super.onLowMemory();
    llog.d(TAG, "============ Low Memory Warning ====================");
    try {
      model.commandethreadminiature.put(new String[]{"cleanup", "force"});
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
  }

  private boolean activityfirstcreated = false;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    //llog.d(TAG, "MainActivity onCreate()");
    if (model == null) {
      //llog.d(TAG, "---------- no existing viewmodel");
      model = new ViewModelProvider(this).get(Gallery.class);
    }
    model.iswatch = getPackageManager().hasSystemFeature(PackageManager.FEATURE_WATCH);
    model.isauto = getPackageManager().hasSystemFeature(PackageManager.FEATURE_AUTOMOTIVE);

    llog.d(TAG, "MainActivity onCreate() iswatch " + model.iswatch + " isauto " + model.isauto);


    /*
    if (searchmusic) {
      if (Gallery.isMyServiceRunning(backgroundService.class, this)) {
        if (Gallery.backgroundService != null) {
          if (Gallery.backgroundService.threadmusic != null) {
            //llog.d(TAG, "++++++++++ recover existing viewmodel");
            //model = Gallery.backgroundService.threadmusic.model;
            model.searchmusic = true;
          }
        }
      }
    }*/

    model.activitycontext = this.getApplicationContext();
    model.preferences = PreferenceManager.getDefaultSharedPreferences(this);
    //Gallery.packagename = this.getPackageName();
    //myViewModel.broadcastname = Gallery.packagename + ".broadcast";



    hideSystemUI();



    //if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
      WindowCompat.setDecorFitsSystemWindows(getWindow(), false);
      //WindowInsetsControllerCompat windowInsetsController = WindowCompat.getInsetsController(getWindow(), getWindow().getDecorView());
      //if (windowInsetsController != null) // Configure the behavior of the hidden system bars.
      //  windowInsetsController.setSystemBarsBehavior(WindowInsetsControllerCompat.BEHAVIOR_SHOW_TRANSIENT_BARS_BY_SWIPE);
    //}

    /*
    getWindow().getDecorView().setOnSystemUiVisibilityChangeListener(new View.OnSystemUiVisibilityChangeListener() {
      @Override
      public void onSystemUiVisibilityChange(int i) {
        llog.d(TAG, i + " onSystemUiVisibilityChange"); // 0 absent 6 present
        if (i != 0)
          hideSystemUI();
      }
    });
    */
    /*{ visibility ->
        // Note that system bars will only be "visible" if none of the
        // LOW_PROFILE, HIDE_NAVIGATION, or FULLSCREEN flags are set.
        if (visibility and View.SYSTEM_UI_FLAG_FULLSCREEN == 0) {
            hideSystemUI()
            // adjustments to your UI, such as showing the action bar or
            // other navigational controls.
        }
    */



    UiModeManager uiModeManager = (UiModeManager) getSystemService(UI_MODE_SERVICE);
    if (uiModeManager.getCurrentModeType() == Configuration.UI_MODE_TYPE_TELEVISION) {
      model.isandroidtv = true;
      model.optionhardwareaccelerationb = false;
      model.videohardwaremode = Gallery.videohwaamodehwpp;
      model.videoaaspectresize = true;
      llog.d(TAG, "Android TV, disable browser HW Acceleration by default, set full hw accel for videos");
    } else {
      model.isandroidtv = false;
      model.optionhardwareaccelerationb = true;
      model.videohardwaremode = Gallery.videohwaamodenhwp;
      model.videoaaspectresize = false;
    }
    if (!model.isandroidtv) {
      ViewCompat.setOnApplyWindowInsetsListener(getWindow().getDecorView(), (v, insets) -> {

        /*int type;
        boolean imeVisiblen, imeVisibles, imeVisibley;
        Insets navigationBars, statusBars, systemBars;

        type = WindowInsetsCompat.Type.navigationBars();
        imeVisiblen = insets.isVisible(type);
        navigationBars =  insets.getInsets(type);
        llog.d(TAG, "                navigationBars " + imeVisiblen + " : " + navigationBars.top + "," + navigationBars.left + " " + navigationBars.bottom + "," + navigationBars.right);

        type = WindowInsetsCompat.Type.statusBars();
        imeVisibles = insets.isVisible(type);
        statusBars =  insets.getInsets(type);
        llog.d(TAG, "                statusBars " + imeVisibles + " : " + statusBars.top + "," + statusBars.left + " " + statusBars.bottom + "," + statusBars.right);

        type = WindowInsetsCompat.Type.systemBars();
        imeVisibley = insets.isVisible(type);
        systemBars =  insets.getInsets(type);
        llog.d(TAG, "                systemBars " + imeVisibley + " : " + systemBars.top + "," + systemBars.left + " " + systemBars.bottom + "," + systemBars.right);

        type = WindowInsetsCompat.Type.ime();
        imeVisibley = insets.isVisible(type);
        systemBars =  insets.getInsets(type);
        llog.d(TAG, "                keyboard " + imeVisibley + " : " + systemBars.top + "," + systemBars.left + " " + systemBars.bottom + "," + systemBars.right);
        */

        if (!insets.isVisible(WindowInsetsCompat.Type.navigationBars()) && !insets.isVisible(WindowInsetsCompat.Type.ime())) {
          correctallfragmentlayouts(0, 0, 0, 0);
        }

        return insets;
      });
    }
    model.forceatvmode = model.preferences.getBoolean("forceatvmode", false);
    if (model.forceatvmode) {
      model.isandroidtv = true;
      llog.d(TAG, "force mode : Android TV");
    }



    if (savedInstanceState == null) {
      llog.d(TAG, "MainActivity bundle is null : the activity is first created");
      activityfirstcreated = true;

    } else {
      llog.d(TAG, "MainActivity recreated : bundle is not null");
      activityfirstcreated = false;
      //mUser = savedInstanceState.getString(STATE_USER);
    }

    boolean launchmainfragment = true;
    boolean usesaf = model.preferences.getBoolean("usesaf", false);

    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R && Environment.isExternalStorageManager()) {
      if (usesaf)
        model.preferences.edit().putBoolean("usesaf", false).apply();
    } else {
      if ((model.iswatch || model.isauto) && Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
        model.preferences.edit().putBoolean("usesaf", true).apply();
        if (checkSelfPermission(Manifest.permission.READ_MEDIA_IMAGES) != PackageManager.PERMISSION_GRANTED) {
          requestPermissions(new String[]{Manifest.permission.READ_MEDIA_IMAGES, Manifest.permission.READ_MEDIA_VIDEO, Manifest.permission.READ_MEDIA_AUDIO}, MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE);
          launchmainfragment = false;
          for (int i = 0; i < 100; i++) {
            if (checkSelfPermission(Manifest.permission.READ_MEDIA_IMAGES) == PackageManager.PERMISSION_GRANTED) {
              launchmainfragment = true;
              break;
            }
            try {
              Thread.sleep(1000);
            } catch (InterruptedException e) {
              e.printStackTrace();
            }
            llog.d(TAG, "wait for checkSelfPermission(Manifest.permission.READ_MEDIA_IMAGES) == PackageManager.PERMISSION_GRANTED");
          }
        } else if (checkSelfPermission(Manifest.permission.READ_MEDIA_VIDEO) != PackageManager.PERMISSION_GRANTED) {
          requestPermissions(new String[]{Manifest.permission.READ_MEDIA_VIDEO, Manifest.permission.READ_MEDIA_AUDIO}, MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE);
          launchmainfragment = false;
          for (int i = 0; i < 100; i++) {
            if (checkSelfPermission(Manifest.permission.READ_MEDIA_VIDEO) == PackageManager.PERMISSION_GRANTED) {
              launchmainfragment = true;
              break;
            }
            try {
              Thread.sleep(1000);
            } catch (InterruptedException e) {
              e.printStackTrace();
            }
            llog.d(TAG, "wait for checkSelfPermission(Manifest.permission.READ_MEDIA_VIDEO) == PackageManager.PERMISSION_GRANTED");
          }
        } else if (checkSelfPermission(Manifest.permission.READ_MEDIA_AUDIO) != PackageManager.PERMISSION_GRANTED) {
          requestPermissions(new String[]{Manifest.permission.READ_MEDIA_AUDIO}, MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE);
          launchmainfragment = false;
          for (int i = 0; i < 100; i++) {
            if (checkSelfPermission(Manifest.permission.READ_MEDIA_AUDIO) == PackageManager.PERMISSION_GRANTED) {
              launchmainfragment = true;
              break;
            }
            try {
              Thread.sleep(1000);
            } catch (InterruptedException e) {
              e.printStackTrace();
            }
            llog.d(TAG, "wait for checkSelfPermission(Manifest.permission.READ_MEDIA_AUDIO) == PackageManager.PERMISSION_GRANTED");
          }
        }
      } else if (!usesaf && Build.VERSION.SDK_INT >= Build.VERSION_CODES.R && !model.iswatch) {      // 30        Android 11
        model.preferences.edit().putBoolean("usesaf", false).apply();
        if (!Environment.isExternalStorageManager()) {
          boolean failed = false;
          try {
            startActivity(new Intent(Settings.ACTION_MANAGE_APP_ALL_FILES_ACCESS_PERMISSION, Uri.parse("package:" + Gallery.packagename)));
          } catch (Exception ex) {
            try {
              startActivity(new Intent(Settings.ACTION_MANAGE_ALL_FILES_ACCESS_PERMISSION));
            } catch (Exception exc) {
              llog.d(TAG, "could not find storage");
              failed = true;
            }
          }
          if (!failed) {
            launchmainfragment = false;
            for (int i = 0; i < 100; i++) {
              if (Environment.isExternalStorageManager()) {
                launchmainfragment = true;
                break;
              }
              try {
                Thread.sleep(1000);
              } catch (InterruptedException e) {
                e.printStackTrace();
              }
              llog.d(TAG, "wait for Environment.isExternalStorageManager()");
            }
          }
        }
      } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {          // 23       Android 6
        model.preferences.edit().putBoolean("usesaf", true).apply();
        if (checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
          requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE);
          launchmainfragment = false;
          for (int i = 0; i < 100; i++) {
            if (checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
              launchmainfragment = true;
              break;
            }
            try {
              Thread.sleep(1000);
            } catch (InterruptedException e) {
              e.printStackTrace();
            }
            llog.d(TAG, "wait for checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED");
          }
        }
      } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
        model.preferences.edit().putBoolean("usesaf", true).apply();
      }
    }

    if (launchmainfragment) {
      lauchmainfragment(activityfirstcreated);
    }

  }

  private void asknotificationpermission() {
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
      if (checkSelfPermission(Manifest.permission.POST_NOTIFICATIONS) == PackageManager.PERMISSION_GRANTED) {
        llog.d(TAG, " -------------- > Manifest.permission.POST_NOTIFICATIONS == PackageManager.PERMISSION_GRANTED");
      } else {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
          llog.d(TAG, " -------------- > requestPermissions Manifest.permission.POST_NOTIFICATIONS MY_PERMISSIONS_REQUEST_POST_NOTIFICATIONS");
          requestPermissions(new String[] {Manifest.permission.POST_NOTIFICATIONS}, MY_PERMISSIONS_REQUEST_POST_NOTIFICATIONS);
        } else {
          llog.d(TAG, " -------------- > requestPermissions ok");
        }
      }
    }
  }

  @Override
  public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
    super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    llog.d(TAG, "onRequestPermissionsResult " + requestCode);
    switch (requestCode) {
      /*case MY_PERMISSIONS_REQUEST_MANAGE_EXTERNAL_STORAGE: {
        // If request is cancelled, the result arrays are empty.
        if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
          lauchmainfragment(activityfirstcreated);
        } else {
          //finish();
          llog.d(TAG, "onRequestPermissionsResult MY_PERMISSIONS_REQUEST_MANAGE_EXTERNAL_STORAGE not granted");
          lauchmainfragment(activityfirstcreated);
        }
        break;
      }*/
      case MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE: {
        if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
          lauchmainfragment(activityfirstcreated);
        } else {
          llog.d(TAG, "onRequestPermissionsResult MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE not granted");
          lauchmainfragment(activityfirstcreated);
        }
        break;
      }
      case MY_PERMISSIONS_REQUEST_POST_NOTIFICATIONS: {
        if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
          llog.d(TAG, "onRequestPermissionsResult MY_PERMISSIONS_REQUEST_POST_NOTIFICATIONS granted");
        } else {
          llog.d(TAG, "onRequestPermissionsResult MY_PERMISSIONS_REQUEST_POST_NOTIFICATIONS not granted");
        }
        break;
      }
      default:
        llog.d(TAG, "onRequestPermissionsResult unhandled");
        break;
    }
  }

  @Override
  public void onDestroy() {
    super.onDestroy();
    if (isFinishing()) {
      //getLifecycle().removeObserver(mAmbientLifecycleObserver);
      llog.d(TAG, "onDestroy() isfinishing...");
      if (model != null) {
        model.closethreads(true);
      }
      System.runFinalization();
      System.gc();
      llog.d(TAG, "onDestroy() isfinishing finished");
    } else {
      llog.d(TAG, "onDestroy() isnotfinishing");
    }
  }

  @Override
  public void onSaveInstanceState(Bundle savedInstanceState) {
    //savedInstanceState.putString(STATE_USER, mUser);
    // Always call the superclass so it can save the view hierarchy state
    llog.d(TAG, "onSaveInstanceState()");
    super.onSaveInstanceState(savedInstanceState);
  }

  private void lauchmainfragment(boolean activityfirstcreated) {
    llog.d(TAG, "lauchmainfragment getPackageName " + Gallery.packagename);
    model.usesaf = model.preferences.getBoolean("usesaf", false);

    //model.atvzoommode = model.preferences.getInt("atvzoombtn", myViewModel.AtvZoomButtonVolFfwd);

    model.flagsecure = model.preferences.getBoolean("flagsecure", false);
    if (model.flagsecure) {
      getWindow().addFlags(WindowManager.LayoutParams.FLAG_SECURE);
    } else {
      getWindow().clearFlags(WindowManager.LayoutParams.FLAG_SECURE);
    }

    List<Fragment> fragments = getSupportFragmentManager().getFragments();
    int fragmentsl = fragments.size();
    if (model.surf.size() <= 0 && fragmentsl > 0) {
      llog.d(TAG, "...app has been destroyed in the background...");
      FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
      for (int i = 0; i < fragmentsl; i++) {
        llog.d(TAG, "destroy fragment " + fragments.get(i).getTag());
        transaction.remove(fragments.get(i));
      }
      transaction.commitNowAllowingStateLoss();
      try {
        Thread.sleep(500);
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
      activityfirstcreated = true;
      mainlayout = null;
    }

    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
      model.refreshrate = getWindowManager().getDefaultDisplay().getMode().getRefreshRate();
    }
    DisplayMetrics displayMetrics = new DisplayMetrics();
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
      getWindowManager().getDefaultDisplay().getRealMetrics(displayMetrics);
    } else {
      getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
    }
    Rect rectangle = new Rect();
    getWindow().getDecorView().getWindowVisibleDisplayFrame(rectangle);
    int dmw = displayMetrics.widthPixels;
    int dmh = displayMetrics.heightPixels;
    int rw = rectangle.width();
    int rh = rectangle.height();
    llog.d(TAG, dmw + "x" + dmh
            + " " + rw + "x" + rh
            + " " + displayMetrics.xdpi + "x" + displayMetrics.ydpi);
    if (dmw < 4 || dmh < 4) {
      llog.d(TAG, "screen too small try again in a bit");
      try {
        Thread.sleep(1000);
      } catch (InterruptedException e) {
      }
      if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
        getWindowManager().getDefaultDisplay().getRealMetrics(displayMetrics);
      } else {
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
      }
      getWindow().getDecorView().getWindowVisibleDisplayFrame(rectangle);
      dmw = displayMetrics.widthPixels;
      dmh = displayMetrics.heightPixels;
      rw = rectangle.width();
      rh = rectangle.height();
    }
    int screenmax = model.preferences.getInt("screenmax", 0);
    int screenmin = model.preferences.getInt("screenmin", 0);
    if (dmw > dmh) {
      if (dmw > screenmax)
        model.preferences.edit().putInt("screenmax", dmw).commit();
      if (dmh > screenmin)
        model.preferences.edit().putInt("screenmin", dmh).commit();
    } else {
      if (dmh > screenmax)
        model.preferences.edit().putInt("screenmax", dmh).commit();
      if (dmw > screenmin)
        model.preferences.edit().putInt("screenmin", dmw).commit();
    }
    if (rw > rh) {
      if (rw > screenmax)
        model.preferences.edit().putInt("screenmax", rw).commit();
      if (rh > screenmin)
        model.preferences.edit().putInt("screenmin", rh).commit();
    } else {
      if (rh > screenmax)
        model.preferences.edit().putInt("screenmax", rh).commit();
      if (rw > screenmin)
        model.preferences.edit().putInt("screenmin", rw).commit();
    }

    model.bigScreenWidth = displayMetrics.widthPixels;
    model.bigScreenHeight = displayMetrics.heightPixels;
    model.mXDpi = displayMetrics.xdpi;
    model.mYDpi = displayMetrics.ydpi;
    llog.d(TAG, model.bigScreenWidth + "x" + model.bigScreenHeight + " xdpi " + model.mXDpi);
    model.unmillimetre = model.mXDpi / 25.4f;
    model.buttontextsize = model.preferences.getFloat("buttontextsize", 1.0f);
    model.thumbsize = model.preferences.getInt("thumbsize", model.thumbsize);
    if (model.thumbsize > model.bigScreenWidth * 3) {
      llog.d(TAG, "thumbsize too big force reset " + model.thumbsize);
      model.thumbsize = 100;
    }
    model.miniaturevideo = model.preferences.getInt("miniaturevideo", model.miniaturevideo);

    if (model.buttontextsize == 1.0f) {
      Button button = new Button(this);
      model.buttontextsize = button.getTextSize();
      model.preferences.edit().putFloat("buttontextsize", model.buttontextsize).apply();
    }

    model.setallthepaints();

    model.optionhidesurface = model.preferences.getBoolean("optionhidesurface", model.optionhidesurface);
    model.nombreonline = model.preferences.getInt("nombreonline", model.nombreonline);
    model.nombredossierbookmarked = model.preferences.getInt("nombredossierbookmarked", model.nombredossierbookmarked);
    model.nombredestinationsaved = model.preferences.getInt("nombredestinationsaved", model.nombredestinationsaved);
    model.optiondestinationmemory = model.preferences.getInt("optiondestinationmemory", model.optiondestinationmemory);
    model.videoaudiovolume = model.preferences.getInt("audiovolume", model.videoaudiovolume);
    model.videoaudioweightroot1 = model.preferences.getBoolean("videoaudioweightroot1", model.videoaudioweightroot1);
    model.videoaudioweightlevel1 = model.preferences.getBoolean("videoaudioweightlevel1", model.videoaudioweightlevel1);
    model.optionhardwareaccelerationb = model.preferences.getBoolean("optionhardwareaccelerationbrowser", model.optionhardwareaccelerationb);
    model.optionautostartboot = model.preferences.getBoolean("optionautostartboot", model.optionautostartboot);
    model.optiondecodelibextractor = model.preferences.getBoolean("optiondecodelibextractor", model.optiondecodelibextractor);
    Gallery.debugi = model.preferences.getInt("debugi", BuildConfig.DEBUG ? 1 : 0);
    model.gamecolumns = model.preferences.getInt("gamecolumns", model.gamecolumns);
    model.gamelines = model.preferences.getInt("gamelines", model.gamelines);
    model.optionshowbookmarkedlist = model.preferences.getBoolean("optionshowbookmarkedlist", model.optionshowbookmarkedlist);
    model.filmstripanimatetime = model.preferences.getLong("filmstripanimatetime", model.filmstripanimatetime);
    model.musiclastsearch = model.preferences.getString("musiclastsearch", null);
    model.videohardwaremode = model.preferences.getInt("videohardwaremode", model.videohardwaremode);
    model.videoaaspectresize = model.preferences.getBoolean("videoaaspectresize", model.videoaaspectresize);
    model.autoloadlastonlinemediaviewed = model.preferences.getBoolean("autoloadlastonlinemediaviewed", model.autoloadlastonlinemediaviewed);
    model.runcmddownloadurl = model.preferences.getString("runcmddownloadurl", model.runcmddownloadurl);
    model.runcmd = model.preferences.getString("runcmd", model.runcmd);
    model.bootstraptor = model.preferences.getString("bootstraptor", model.bootstraptor);
    model.musicrandminreplay = model.preferences.getInt("musicrandminreplay", model.musicrandminreplay);
    model.showmenuprevnextbutton = model.preferences.getBoolean("showmenuprevnextbutton", model.showmenuprevnextbutton);

    if (activityfirstcreated) {

      File[] myfiles = null;
      if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
        myfiles = this.getExternalCacheDirs();
      } else {
        llog.d(TAG, "getExternalCacheDir() file null");
        File myfolde = this.getExternalCacheDir();
        if (myfolde != null) {
          String myfolder = myfolde.toString();
          if (new File("/storage/emulated/legacy/Android/data/" + Gallery.packagename).exists())
            myfolder = "/storage/emulated/legacy/Android/data/" + Gallery.packagename;
          myfiles = new File[]{
              new File(myfolder)
          };
        }
      }
      if (myfiles == null) {
        myfiles = new File[]{
            new File("/storage/emulated/0/")
        };
      }
      model.dossiercache = null;
      for (File file : myfiles) {
        if (file == null) {
          llog.d(TAG, "file null");
          continue;
        }
        String thisfolder = file.getAbsolutePath();
        Pattern basepattern = Pattern.compile("(/.+)(/[^/]+)/Android/data/la.daube.photochiotte[^/]*");
        Matcher matcher = basepattern.matcher(thisfolder);
        if (matcher.find()) {
          model.dossiercache = matcher.group(0);
          String basefolder = matcher.group(1) + matcher.group(2);
          model.basefolder.add(basefolder);
          for (String survey : new String[]{
              "/Documents",
              "/Download",
              "/Pictures",
              "/Movies",
              "/DCIM",
          }) {
            if (new File(basefolder + survey).exists()) {
              model.surveyfolder.add(basefolder + survey);
              llog.d(TAG, "folder : " + basefolder + survey);
            }
          }
        }
      }

      String dossiercache = model.preferences.getString("dossiercache", null);
      if (dossiercache != null) {
        if (new File(dossiercache).exists()) {
          if (model.dossiercache == null) {
            llog.d(TAG, "Error " + model.dossiercache + " null");
            model.dossiercache = dossiercache;
          } else {
            while (dossiercache.endsWith("/") && dossiercache.length() > 1)
              dossiercache = dossiercache.substring(0, dossiercache.length() - 1);
            if (!dossiercache.equals(model.dossiercache)) {
              llog.d(TAG, "Error " + model.dossiercache + " different from old " + dossiercache + " keep using " + dossiercache);
              model.dossiercache = dossiercache;
            }
          }
        } else {
          llog.d(TAG, "Error " + dossiercache + " not found anymore");
        }
      }
      llog.d(TAG, "dossiercache            : " + model.dossiercache);
      model.preferences.edit().putString("dossiercache", model.dossiercache).apply();

      model.internalStorageDir = model.activitycontext.getFilesDir().getAbsolutePath() + "/";
      model.preferences.edit().putString("internalStorageDir", model.internalStorageDir).apply();
      llog.d(TAG, "internalstoragedir      : " + model.internalStorageDir);

      File file;
      //model.surveyfolder.add(model.dossiercachefiles);
      model.dossierscreenshot = model.dossiercache + Gallery.staticdossierscreenshot;
      model.dossierdessin = model.dossiercache + Gallery.staticdossierdessin;
      model.dossiercollections = model.dossiercache + Gallery.staticdossiercollections;
      model.dossierconfig = model.dossiercache + Gallery.staticdossierconfig;
      model.dossierbitmaptemp = model.dossiercache + Gallery.staticdossierbitmaptemp;
      model.dossierminiature = model.dossiercache + Gallery.staticdossierminiature;
      file = new File(model.dossierscreenshot);
      if (!file.exists())
        file.mkdirs();
      file = new File(model.dossierdessin);
      if (!file.exists())
        file.mkdirs();
      file = new File(model.dossiercollections);
      if (!file.exists())
        file.mkdirs();
      file = new File(model.dossierconfig);
      if (!file.exists())
        file.mkdirs();
      file = new File(model.dossierbitmaptemp);
      if (!file.exists())
        file.mkdirs();
      file = new File(model.dossierminiature);
      if (!file.exists())
        file.mkdirs();
    }


    Intent intent = getIntent();
    Uri data = intent.getData();
    model.searchMusicAskedFromWidget = intent.getBooleanExtra("SearchMusic", false);
    model.searchMusicAskedFromWidgetR = intent.getStringExtra("SearchMusicR");
    if (model.searchMusicAskedFromWidgetR == null)
      llog.d(TAG, "searchMusicAskedFromWidgetR = null");
    else
      llog.d(TAG, "searchMusicAskedFromWidgetR something");
    if (model.searchMusicAskedFromWidget)
      llog.d(TAG, "searchMusicAskedFromWidget = true");
    else
      llog.d(TAG, "searchMusicAskedFromWidget = false");
    if (activityfirstcreated)
      llog.d(TAG, "activityfirstcreated = true");
    else
      llog.d(TAG, "activityfirstcreated = false");
    if (data != null)
      llog.d(TAG, "data != null");
    else
      llog.d(TAG, "data == null");
    if (model.searchMusicAskedFromWidgetR != null) {
    } else if (model.searchMusicAskedFromWidget) {
    } else {
      if (data != null) {
      } else if (activityfirstcreated) {
        String showthisfile = model.preferences.getString("showthisfile", model.showthisfile);
        if (showthisfile != null) {
          File fichierz = new File(showthisfile);
          llog.d(TAG, "dernier fichier vu : " + showthisfile + " existe toujours : " + fichierz.exists());
          model.showthisfile = showthisfile;
        }
        String showthisfolder = model.preferences.getString("showthisfolder", model.showthisfolder);
        if (showthisfolder != null) {
          model.showthisfolder = showthisfolder;
          File fichierz = new File(showthisfolder);
          llog.d(TAG, "dernier dossier vu : " + showthisfolder + " existe toujours : " + fichierz.exists());
        }
        try {
          model.commandethreaddatabase.put(new String[]{"chercheFichierDansDatabaseSinonRescanSonDossier", model.showthisfolder, model.showthisfile, "0"});
        } catch (InterruptedException e) {
          e.printStackTrace();
        }
      }

      if (activityfirstcreated) {
        try {
          model.commandethreaddatabase.put(new String[]{"loadAutoStartOnlineMedia"});
        } catch (InterruptedException e) {
          e.printStackTrace();
        }
        model.autorescan.clear();
        int nombredossierbookmarked = model.preferences.getInt("nombredossierautorescan", 0);
        for (int i = 0; i < nombredossierbookmarked; i++) {
          String monoption = model.preferences.getString("dossierautorescan" + i, null);
          if (new File(monoption).exists()) {
            model.autorescan.add(monoption);
          }
        }
      }
    }



    layoutfragments(0, 0, 0.50f, true, false, true);

    if (activityfirstcreated) {
      final String lastVersionSaved = model.preferences.getString("ApplVersion", "sfgjdjgh");
      String versionName1;
      try {
        versionName1 = this.getPackageManager().getPackageInfo(Gallery.packagename, 0).versionName;
      } catch (PackageManager.NameNotFoundException e) {
        e.printStackTrace();
        versionName1 = "fghdjhjk";
      }
      final String versionname = versionName1;
      final boolean newversiondetected;
      if (!lastVersionSaved.equals(versionname))
        newversiondetected = true;
      else
        newversiondetected = false;
      //llog.d(TAG, "----*******             *********-------" + lastVersionSaved + " " + versionname);
      new Thread(new Runnable() {
        @Override
        public void run() {
          try {
            Thread.sleep(1500);
          } catch (InterruptedException e) {
            e.printStackTrace();
          }


          if (newversiondetected) {
            /**
             * only when updated
             **/

            String fullmessage;
            if (model.isandroidtv)
              fullmessage = Gallery.howtomessagetv + "\n";
            else if (model.iswatch)
              fullmessage = Gallery.howtomessagewatch + "\n";
            else
              fullmessage = Gallery.howtomessage + "\n";

            if (lastVersionSaved.equals("sfgjdjgh")) {
              llog.d(TAG, "----******* first install *********-------" + lastVersionSaved + " " + versionname);
              fullmessage += "Please wait while scanning folders for the first time...\n v" + versionname;
            } else {
              float lastversionnumber = 1.50f;
              Pattern lvn = Pattern.compile("([0-9.]+)");
              Matcher m = lvn.matcher(lastVersionSaved);
              String floats = lastVersionSaved;
              if (m.find())
                floats = m.group(1);
              try {
                lastversionnumber = Float.parseFloat(floats);
              } catch (Exception e) {
              }

              llog.d(TAG, "----*******  new version  *********-------" + lastVersionSaved + " " + lastversionnumber + " vs " + versionname);

              if (lastversionnumber < 1.50) { // pour v1.50
                fullmessage += "\nThe thumbnail creator has been improved," +
                    "\nplease upgrade your servers" +
                    "\nto support comments.";
                if (!model.isandroidtv) {
                  model.thumbsize += 5;
                } else {
                  try {
                    model.commandethreadminiature.put(new String[]{"supercleanup", "silent"});
                  } catch (InterruptedException e) {
                    e.printStackTrace();
                  }
                }
              }

              if (lastversionnumber < 1.51) { // pour v1.51
                if (model.videohardwaremode == Gallery.videohwaamodehwpp)
                  model.videoaaspectresize = true;
                else
                  model.videoaaspectresize = false;
                model.preferences.edit().putBoolean("videoaaspectresize", model.videoaaspectresize).commit();
              }

              if (lastversionnumber < 1.54) { // pour v1.54
                int nombredossierbookmarked = model.preferences.getInt("nombredossierbookmarked", 0);
                SharedPreferences.Editor editor = model.preferences.edit();
                for (int i = 0; i < nombredossierbookmarked + 10; i++) {
                  if (model.preferences.contains("pprintbookmarked" + i))
                    editor.remove("pprintbookmarked" + i);
                }
                editor.apply();
              }

              fullmessage += "\n     v" + versionname;

            }

            model.message(fullmessage);

            model.preferences.edit().putString("ApplVersion", versionname).apply();

          }

          try {
            if (newversiondetected)
              model.commandethreaddatabase.put(new String[]{"copyassets", "newversion"});
            else
              model.commandethreaddatabase.put(new String[]{"copyassets", "sameoversion"});
          } catch (InterruptedException e) {
            e.printStackTrace();
          }

        }
      }).start();
    }

    setContentView(mainlayout);
  }

  private void findfoldertoupdate() {
    llog.d(TAG, "findfoldertoupdate()");
    String lastvideo = model.preferences.getString("lastvideo", "");
    findlatestvideo(-1, lastvideo, -1);
    String lastimage = model.preferences.getString("lastimage", "");
    findlatestpicture(-1, lastimage, -1);
  }

  public void findlatestvideo(int maxcount, String lastvideo, int shownext) {
    if (maxcount == -1)
      maxcount = Gallery.mediastorelookupmaxcount;
    List<String> folders = new ArrayList<>();
    Uri collection;
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q)
      collection = MediaStore.Video.Media.getContentUri(MediaStore.VOLUME_EXTERNAL);
    else
      collection = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
    String[] projection = new String[]{MediaStore.Video.Media._ID, MediaStore.Video.Media.DISPLAY_NAME,
            MediaStore.Video.Media.DURATION, MediaStore.Video.Media.SIZE, MediaStore.Video.Media.DATE_ADDED};
    //String selection = MediaStore.Video.Media.DURATION + " >= ?";
    //String[] selectionArgs = new String[] {
    //        String.valueOf(TimeUnit.MILLISECONDS.convert(5, TimeUnit.MINUTES)) };
    String selection = null;
    String[] selectionArgs = null;
    //String sortOrder = MediaStore.Video.Media.DISPLAY_NAME + " ASC";
    String sortOrder = MediaStore.Video.Media.DATE_ADDED + " DESC";
    int count = 0;
    try (Cursor cursor = getApplicationContext().getContentResolver().query(collection,
            projection, selection, selectionArgs, sortOrder)) { // Cache column indices.
      int idColumn = cursor.getColumnIndexOrThrow(MediaStore.Video.Media._ID);
      int nameColumn = cursor.getColumnIndexOrThrow(MediaStore.Video.Media.DISPLAY_NAME);
      int durationColumn = cursor.getColumnIndexOrThrow(MediaStore.Video.Media.DURATION);
      int sizeColumn = cursor.getColumnIndexOrThrow(MediaStore.Video.Media.SIZE);
      int dateAddedColumn = cursor.getColumnIndexOrThrow(MediaStore.Video.Media.DATE_ADDED);
      while (cursor.moveToNext()) { // Get values of columns for a given video.
        long id = cursor.getLong(idColumn);
        String name = cursor.getString(nameColumn);
        int duration = cursor.getInt(durationColumn);
        int size = cursor.getInt(sizeColumn);
        int dateadded = cursor.getInt(dateAddedColumn);
        Uri contentUri = ContentUris.withAppendedId(MediaStore.Video.Media.EXTERNAL_CONTENT_URI, id);
        String path = getPathFromUri(this, contentUri, false);
        llog.d(TAG, idColumn + " name " + name + " duration " + duration + " size " + size + " path " + path + " dateadded " + dateadded);
        count += 1;
        if (path != null) {
          if (path.equals(lastvideo))
            break;
          if (count == 1)
            model.preferences.edit().putString("lastvideo", path).apply();
          String folder = path.replaceFirst("(.+)/[^/]+$", "$1");
          if (shownext == -1 || count == maxcount) {
            try {
              model.commandethreaddatabase.put(new String[]{"chercheFichierDansDatabaseSinonRescanSonDossier", folder, path, String.valueOf(shownext)});
            } catch (InterruptedException e) {
              e.printStackTrace();
            }
          }
        }
        if (count >= maxcount)
          break;
      }
      if (cursor != null)
        cursor.close();
    } catch (Exception e) {
      llog.d(TAG, "error findlatestvideo " + e.toString());
    }
  }

  public void findlatestpicture(int maxcount, String lastimage, int shownext) {
    Uri collection;
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q)
      collection = MediaStore.Images.Media.getContentUri(MediaStore.VOLUME_EXTERNAL);
    else
      collection = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
    String[] projection = new String[]{MediaStore.Images.Media._ID, MediaStore.Images.Media.DISPLAY_NAME,
            MediaStore.Images.Media.SIZE, MediaStore.Images.Media.DATE_ADDED};
    String selection = null;
    String[] selectionArgs = null;
    //String sortOrder = MediaStore.Video.Media.DISPLAY_NAME + " ASC";
    String sortOrder = MediaStore.Images.Media.DATE_ADDED + " DESC";
    int count = 0;
    try (Cursor cursor = getApplicationContext().getContentResolver().query(collection,
            projection, selection, selectionArgs, sortOrder)) { // Cache column indices.
      int idColumn = cursor.getColumnIndexOrThrow(MediaStore.Images.Media._ID);
      int nameColumn = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DISPLAY_NAME);
      int sizeColumn = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.SIZE);
      int dateAddedColumn = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATE_ADDED);
      while (cursor.moveToNext()) { // Get values of columns for a given video.
        long id = cursor.getLong(idColumn);
        String name = cursor.getString(nameColumn);
        int size = cursor.getInt(sizeColumn);
        int dateadded = cursor.getInt(dateAddedColumn);
        Uri contentUri = ContentUris.withAppendedId(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, id);
        String path = getPathFromUri(this, contentUri, false);
        llog.d(TAG, idColumn + " name " + name + " size " + size + " path " + path + " dateadded " + dateadded);
        count += 1;
        if (path != null) {
          if (path.equals(lastimage))
            break;
          if (count == 1)
            model.preferences.edit().putString("lastimage", path).apply();
          String folder = path.replaceFirst("(.+)/[^/]+$", "$1");
          if (shownext == -1 || count == maxcount) {
            try {
              model.commandethreaddatabase.put(new String[]{"chercheFichierDansDatabaseSinonRescanSonDossier", folder, path, String.valueOf(shownext)});
            } catch (InterruptedException e) {
              e.printStackTrace();
            }
          }
        }
        if (count >= maxcount)
          break;
      }
      if (cursor != null)
        cursor.close();
    } catch (Exception e) {
      llog.d(TAG, "error findlatestvideo " + e.toString());
    }
  }

  private int layoutfragments(int targetnumber, int thissurf, float thisratio, boolean vertical, boolean fragmentvideo, boolean requestlayout) {
    int targetcreated = -1;

    if (mainlayout == null) {
      llog.d(TAG, "==================================================================================================mainlayout");
      mainlayout = new RelativeLayout(this);
      mainlayout.setX(0);
      mainlayout.setY(0);
      mainlayout.setBackgroundColor(Color.BLACK);
      mainlayout.setId(layoutid);
    }

    List<Fragment> fragments = getSupportFragmentManager().getFragments();
    int fragmentsl = fragments.size();
    if (model.surf.size() <= 0 && fragmentsl > 0) {
      llog.d(TAG, "...app has been destroyed in background...");
      fragmentsl = 0;
    }
    llog.d(TAG, targetnumber + " vs " + fragmentsl);

    if (fragmentsl == 0) {
      llog.d(TAG, "cree fragment 0");
      targetcreated = addsurf(0, 0, model.bigScreenWidth, model.bigScreenHeight);
      FragmentBrowser fragment1 = new FragmentBrowser();
      Bundle bundle1 = new Bundle();
      bundle1.putInt("myid", targetcreated);
      fragment1.setArguments(bundle1);
      getSupportFragmentManager().beginTransaction().add(layoutid, fragment1, "f" + model.surf.get(targetcreated).surftagi).commitNowAllowingStateLoss();
      try {
        model.commandethreadbrowser.put(new String[]{String.valueOf(0), "start"});
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
      mainlayout.postDelayed(
              new Runnable() {
                @Override
                public void run() {
                  try {
                    model.commandethreaddatabase.put(new String[]{"update"});
                  } catch (InterruptedException e) {
                    e.printStackTrace();
                  }
                }
              }
              , 1500);

    } else if (targetnumber == 2) {
      llog.d(TAG, "split in two");
      targetcreated = splitintwo(thissurf, thisratio, vertical, fragmentvideo, requestlayout);
      try {
        model.commandethreadbrowser.put(new String[]{String.valueOf(targetcreated), "start"});
      } catch (InterruptedException e) {
        e.printStackTrace();
      }

    } else if (targetnumber == 1) {

      if (fragmentsl == 1) {

        llog.d(TAG, "cannot remove more splits");
        //correctFragmentLayout(0, 0, 0, model.bigScreenWidth, model.bigScreenHeight);

      } else {
        /*for (int i = surfsize - 1; i >= 0; i--) {
          Surf sur = model.surf.get(i);
          if (i != thissurf) {
            if (i > thissurf) {
              int j = i - 1;
              sur.myid = j;
              llog.d(TAG, "fragment " + i + " -> " + j);
              int surftagi = model.surf.get(i).surftagi;
              Fragment fragment3 = getSupportFragmentManager().findFragmentByTag("f" + surftagi);
              if (fragment3 != null) {
                if (sur.fragmenttype == Surf.FRAGMENT_TYPE_BROWSER) {
                  ((FragmentBrowser) fragment3).myid = j;
                } else if (sur.fragmenttype == Surf.FRAGMENT_TYPE_VIDEO) {
                  ((FragmentVideo) fragment3).myid = j;
                } else if (sur.fragmenttype == Surf.FRAGMENT_TYPE_DRAWING) {
                  ((FragmentDrawing) fragment3).myid = j;
                } else if (sur.fragmenttype == Surf.FRAGMENT_TYPE_PUZZLE) {
                  ((FragmentGamePuzzle) fragment3).myid = j;
                }
              }
            }
          }
        }*/
        int surfsize = model.surf.size();
        Surf surdel = model.surf.get(thissurf);
        final float dm = 40.0f;
        for (int i = 0; i < surfsize; i++) {
          Surf sur = model.surf.get(i);
          if (i != thissurf && !sur.idle) {
            llog.d(TAG, "fragment " + i + "/" + thissurf + " : " + sur.myx + "," + sur.myy + " " + sur.mywidth + "x" + sur.myheight);
            if (Math.abs(sur.mywidth - surdel.mywidth) < dm) {
              if (Math.abs((sur.myy + sur.myheight) - surdel.myy) < dm) {
                llog.d(TAG, "on augmente fragment a " + i + " " + sur.myheight + " -> " + sur.myheight + " + " + surdel.myheight);
                correctFragmentLayout(i, sur.myx, sur.myy, sur.mywidth, (sur.myheight + surdel.myheight), requestlayout);
                break;
              } else if (Math.abs(sur.myy - (surdel.myy + surdel.myheight)) < dm) {
                llog.d(TAG, "on augmente fragment b " + i + " " + sur.myy + " -> " + surdel.myy);
                correctFragmentLayout(i, sur.myx, surdel.myy, sur.mywidth, (sur.myheight + surdel.myheight), requestlayout);
                break;
              }
            }
            if (Math.abs(sur.myheight - surdel.myheight) < dm) {
              if (Math.abs((sur.myx + sur.mywidth) - surdel.myx) < dm) {
                llog.d(TAG, "on augmente fragment c " + i + " " + sur.mywidth + " -> " + sur.mywidth + " + " + surdel.mywidth);
                correctFragmentLayout(i, sur.myx, sur.myy, (sur.mywidth + surdel.mywidth), sur.myheight, requestlayout);
                break;
              } else if (Math.abs(sur.myx - (surdel.myx + surdel.mywidth)) < dm) {
                llog.d(TAG, "on augmente fragment d " + i + " " + sur.myx + " -> " + surdel.myx);
                correctFragmentLayout(i, surdel.myx, sur.myy, (sur.mywidth + surdel.mywidth), sur.myheight, requestlayout);
                break;
              }
            }
            if (Math.abs(surdel.myheight - sur.myheight) > -dm) {
              if (Math.abs(sur.myx - (surdel.myx + surdel.mywidth)) < dm
                      && surdel.myy - sur.myy < +dm
                      && (surdel.myy + surdel.myheight) - (sur.myy + sur.myheight) > -dm
              ) {
                llog.d(TAG, "on augmente fragment e " + i + " " + sur.myx + " -> " + surdel.myx);
                correctFragmentLayout(i, surdel.myx, sur.myy, (sur.mywidth + surdel.mywidth), sur.myheight, requestlayout);
              }
            }
            if (Math.abs(surdel.myheight - sur.myheight) > -dm) {
              if (Math.abs(surdel.myx - (sur.myx + sur.mywidth)) < dm
                      && surdel.myy - sur.myy < +dm
                      && (surdel.myy + surdel.myheight) - (sur.myy + sur.myheight) > -dm
              ) {
                llog.d(TAG, "on augmente fragment f " + i + " " + sur.myx + " -> " + surdel.myx);
                correctFragmentLayout(i, sur.myx, sur.myy, (sur.mywidth + surdel.mywidth), sur.myheight, requestlayout);
              }
            }
            if (Math.abs(surdel.mywidth - sur.mywidth) > -dm) {
              if (Math.abs(sur.myy - (surdel.myy + surdel.myheight)) < dm
                      && surdel.myx - sur.myx < +dm
                      && (surdel.myx + surdel.mywidth) - (sur.myx + sur.mywidth) > -dm
              ) {
                llog.d(TAG, "on augmente fragment g " + i + " " + sur.myy + " -> " + surdel.myy);
                correctFragmentLayout(i, sur.myx, surdel.myy, sur.mywidth, surdel.myheight + sur.myheight, requestlayout);
              }
            }
            if (Math.abs(surdel.myheight - sur.myheight) > -dm) {
              if (Math.abs(surdel.myy - (sur.myy + sur.myheight)) < dm
                      && surdel.myx - sur.myx < +dm
                      && (surdel.myx + surdel.mywidth) - (sur.myx + sur.mywidth) > -dm
              ) {
                llog.d(TAG, "on augmente fragment h " + i + " " + sur.myx + " -> " + surdel.myx);
                correctFragmentLayout(i, sur.myx, sur.myy, sur.mywidth, surdel.myheight + sur.myheight, requestlayout);
              }
            }
          }
        }

        llog.d(TAG, "on supprime le fragment " + thissurf);
        model.removesurf(thissurf, true);
        int surftagii = model.surf.get(thissurf).surftagi;

        FragmentManager fragmentManager = getSupportFragmentManager();
        Fragment fragment3i = fragmentManager.findFragmentByTag("f" + surftagii);
        if (fragment3i != null) {
          FragmentTransaction transaction = fragmentManager.beginTransaction();
          transaction.setReorderingAllowed(true);
          transaction.remove(fragment3i);
          transaction.commitNow();
        }
      }

    } else {
      int surfsize = model.surf.size();
      for (int i = 0; i < surfsize; i++) {
        llog.d(TAG, "corrige fragment " + i);
        Surf modsurf = model.surf.get(i);
        correctFragmentLayout(i, modsurf.myx, modsurf.myy, modsurf.mywidth, modsurf.myheight, requestlayout);
      }
      try {
        model.commandethreaddatabase.put(new String[]{"update"});
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
    }

    return targetcreated;
  }

  private int lastdmw = 0;
  private int lastdmh = 0;
  private int lastrw = 0;
  private int lastrh = 0;
  public void correctallfragmentlayouts(int wi, int wf, int hi, int hf) {

    int dmw;
    int dmh;
    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.R) {
      Display activityDisplay = getDisplay();
      WindowMetrics windowMetrics = getWindowManager().getCurrentWindowMetrics();
      WindowMetrics maximumWindowMetrics = getWindowManager().getMaximumWindowMetrics();
      Rect bounds = windowMetrics.getBounds();
      dmw = bounds.width();
      dmh = bounds.height();
    } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
      DisplayMetrics displayMetrics = new DisplayMetrics();
      getWindowManager().getDefaultDisplay().getRealMetrics(displayMetrics);
      dmw = displayMetrics.widthPixels;
      dmh = displayMetrics.heightPixels;
    } else {
      DisplayMetrics displayMetrics = new DisplayMetrics();
      getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
      dmw = displayMetrics.widthPixels;
      dmh = displayMetrics.heightPixels;
    }
    if (dmw < 4 || dmh < 4) {
      llog.d(TAG, "ignore screen too small " + dmw + " " + dmh);
      return;
    }

    Rect rectangle = new Rect();
    getWindow().getDecorView().getWindowVisibleDisplayFrame(rectangle);
    int rw = rectangle.width();
    int rh = rectangle.height();
    if (rw < 4 || rh < 4) {
      llog.d(TAG, "replace screen too small " + rw + " " + rh + " -> " + dmw + " " + dmh);
      rw = dmw;
      rh = dmh;
    }

    //int w = getWindow().getDecorView().getMeasuredWidth();
    //int h = getWindow().getDecorView().getMeasuredHeight();
    /*float diffx = Math.abs(rectangle.width() - model.rectangleScreenWidth);
    float diffy = Math.abs(rectangle.height() - model.rectangleScreenHeight);
    float minx = 0.15f * displayMetrics.widthPixels;
    float miny = 0.15f * displayMetrics.heightPixels;
    boolean correct = false;*/

    /*if (diffx > minx) {
      llog.d(TAG, "correctallfragmentlayouts x"
              + " rect " + model.rectangleScreenWidth + "x" + model.rectangleScreenHeight
              + " -> " + rectangle.left + "," + rectangle.top
              + " " + rectangle.width() + "x" + rectangle.height()
              + " displayMetrics " + displayMetrics.widthPixels + "x" + displayMetrics.heightPixels
              + " wh " + wi + "x" + hi + " -> " + wf + "x" + hf
              + " diffx " + diffx + " mindiffx " + minx
              + " diffy " + diffy + " mindiffy " + miny
      );
      model.rectangleScreenWidth = rectangle.width();
      model.bigScreenWidth = rectangle.width();
      correct = true;
    } else if (diffy > miny) {
      llog.d(TAG, "correctallfragmentlayouts y"
              + " rect " + model.rectangleScreenWidth + "x" + model.rectangleScreenHeight
              + " -> " + rectangle.left + "," + rectangle.top
              + " " + rectangle.width() + "x" + rectangle.height()
              + " displayMetrics " + displayMetrics.widthPixels + "x" + displayMetrics.heightPixels
              + " wh " + wi + "x" + hi + " -> " + wf + "x" + hf
              + " diffx " + diffx + " mindiffx " + minx
              + " diffy " + diffy + " mindiffy " + miny
      );
      model.rectangleScreenHeight = rectangle.height();
      model.bigScreenHeight = rectangle.height();
      correct = true;
    } else {
      llog.d(TAG, "correctallfragmentlayouts ignored");
    }*/

    float diffx = Math.abs((float) dmw - (float) rw) / ((float) dmw);
    float diffy = Math.abs((float) dmh - (float) rh) / ((float) dmh);


    llog.d(TAG, " LAST " + model.bigScreenWidth + "x" + model.bigScreenHeight
            + " NEW "  + rw + "x" + rh
            + " displaymetrics " + dmw + "x" + dmh
            + " diffx " + diffx + "x" + diffy
    );

    if (diffx > 0.10f) {
      model.bigScreenWidth = rw;
    } else {
      model.bigScreenWidth = dmw;
    }

    if (diffy > 0.10f) {
      model.bigScreenHeight = rh;
    } else {
      model.bigScreenHeight = dmh;
    }

    //correct = true;

    if (rw != lastrw || rh != lastrh || dmw != lastdmw || dmh != lastdmh) {
      //   rx      rw    dw
      //    0     866  2560
      //    0    1272  2560
      //    0    1677  2560
      //  1289   1271  2560
      lastrw = rw;
      lastrh = rh;
      lastdmw = dmw;
      lastdmh = dmh;
      int surfsize = model.surf.size();

      /*for (int i = 0; i < surfsize; i++) {
        Surf modsurf = model.surf.get(i);
        float mx = ((float) modsurf.myx * (float) model.bigScreenWidth) / (float) modsurf.myscreenwidth;
        float mxm = ((float) (modsurf.myx + modsurf.mywidth) * (float) model.bigScreenWidth) / (float) modsurf.myscreenwidth;
        llog.d(TAG, ((float) model.bigScreenWidth / (float) modsurf.myscreenwidth)
                + " : " + i + " : " + modsurf.myx + " -> " + (modsurf.myx + modsurf.mywidth) + " will be " + mx +" -> " + mxm
        );
      }*/

      for (int i = 0; i < surfsize; i++) {
        Surf modsurf = model.surf.get(i);
        //llog.d(TAG, "initial " + i + " " + modsurf.myx + " " + modsurf.myy + " " + modsurf.mywidth + " " + modsurf.myheight);
        correctFragmentLayout(i,
                modsurf.myx, modsurf.myy,
                modsurf.mywidth, modsurf.myheight,
                false);
        //llog.d(TAG, "final   " + i + " " + modsurf.myx + " " + modsurf.myy + " " + modsurf.mywidth + " " + modsurf.myheight);
      }
      model.setallthepaints();

      try {
        model.commandethreadbrowser.put(new String[]{"-1", "update"});
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
    }
  }

  public int addsurf(int myx, int myy, int mywidth, int myheight) {
    int myid = -1;
    Surf mysurf = null;
    int surfl = model.surf.size();
    for (int i = 0; i < surfl; i++) {
      Surf thissurf = model.surf.get(i);
      if (thissurf.idle) {
        myid = i;
        mysurf = thissurf;
      }
    }
    if (myid == -1) {
      mysurf = new Surf();
    }
    mysurf.foldoptions();
    model.surftagi += 1;
    mysurf.surftagi = model.surftagi;
    mysurf.myx = myx;
    mysurf.myy = myy;
    mysurf.mywidth = mywidth;
    mysurf.myheight = myheight;
    mysurf.myscreenwidth = model.bigScreenWidth;
    mysurf.myscreenheight = model.bigScreenHeight;
    mysurf.splitleft = false;
    mysurf.splittop = false;
    mysurf.splitright = false;
    mysurf.splitbottom = false;
    if (mysurf.myx > 0)
      mysurf.splitleft = true;
    if (mysurf.myy > 0)
      mysurf.splittop = true;
    if (mysurf.myx + mysurf.mywidth < model.bigScreenWidth)
      mysurf.splitright = true;
    if (mysurf.myy + mysurf.myheight < model.bigScreenHeight)
      mysurf.splitbottom = true;
    //mysurf.OptionMenuShown = false;
    //mysurf.showfoldernames = false;
    mysurf.SettingsXmin = mysurf.mywidth - model.SettingsWidth;
    mysurf.SettingsYmin = model.settingsYmin;
    mysurf.cursorx = mysurf.mywidth * 0.5f;
    mysurf.cursory = mysurf.myheight * 0.5f;
    mysurf.paintwantsdither = model.preferences.getBoolean("paintwantsdither", mysurf.paintwantsdither);
    mysurf.paintdither.setDither(mysurf.paintwantsdither);
    if (myid == -1) {
      model.surf.add(mysurf);
      myid = surfl;
    }
    mysurf.myid = myid;
    mysurf.idle = false;
    llog.d(TAG, "using surf " + mysurf.myid + " / " + surfl);
    return myid;
  }

  public void correctFragmentLayout(int myid, int x, int y, int width, int height, boolean requestlayout) {
    Surf mysurf = model.surf.get(myid);
    if (mysurf.fragmentView == null) {
      llog.d(TAG, myid + " correctFragmentLayout() null");
      return;
    }
    /*llog.d(TAG, myid + " START : " + x + "," + y + "  " + width + "x" + height
            + " mysurf " + mysurf.myx + "," + mysurf.myy + "  " + mysurf.mywidth + "x" + mysurf.myheight
            + " myswh " + mysurf.myscreenwidth + "x" + mysurf.myscreenheight
            + " bswh " + model.bigScreenWidth + "x" + model.bigScreenHeight
            + " l" + mysurf.splitleft + " r" + mysurf.splitright + " t" + mysurf.splittop + " b" + mysurf.splitbottom);*/
    // on décale les photos
    if (mysurf.myscreenwidth > 0 && model.bigScreenWidth > 0
            && mysurf.myscreenheight > 0 && model.bigScreenHeight > 0
            && mysurf.mywidth > 4 && mysurf.myheight > 4) {
      float decalx = (mysurf.mywidth - width) * 0.5f;
      float decaly = (mysurf.myheight - height) * 0.5f;
      mysurf.bpx -= decalx;
      mysurf.bpxmax -= decalx;
      mysurf.bpy -= decaly;
      mysurf.bpymax -= decaly;
    }
    mysurf.myx = x;
    mysurf.myy = y;
    mysurf.mywidth = width;
    mysurf.myheight = height;

    if (mysurf.myscreenwidth > 0 && model.bigScreenWidth > 0 && mysurf.myscreenheight > 0 && model.bigScreenHeight > 0
            && (mysurf.myscreenwidth != model.bigScreenWidth || mysurf.myscreenheight != model.bigScreenHeight)) {
      // on réajuste si on a tourné l'écran
      float mx = ((float) x * (float) model.bigScreenWidth) / (float) mysurf.myscreenwidth;
      float my = ((float) y * (float) model.bigScreenHeight) / (float) mysurf.myscreenheight;
      float mxm = ((float) (x + width) * (float) model.bigScreenWidth) / (float) mysurf.myscreenwidth;
      float mym = ((float) (y + height) * (float) model.bigScreenHeight) / (float) mysurf.myscreenheight;
      mysurf.myx = (int) (mx);
      mysurf.myy = (int) (my);
      mysurf.mywidth = (int) (mxm) - (int) (mx); // achtung ici il faut arrondir avant de soustraire sinon on a l'erreur des deux arrondis : 2 * 0.5 >= 1.0
      mysurf.myheight = (int) (mym) - (int) (my);

      float decalx = (mysurf.mywidth - width) * 0.5f;
      float decaly = (mysurf.myheight - height) * 0.5f;
      mysurf.bpx += decalx;
      mysurf.bpxmax += decalx;
      mysurf.bpy += decaly;
      mysurf.bpymax += decaly;

      mysurf.myscreenwidth = model.bigScreenWidth;
      mysurf.myscreenheight = model.bigScreenHeight;
      llog.d(TAG, myid + " screen rotated : " + mysurf.myx + "," + mysurf.myy + "  " + mysurf.mywidth + "x" + mysurf.myheight + "       on a corrigé les valeurs");
    }

    /*
    float correcterror = 0.10f;
    if (0 < mysurf.myx && mysurf.myx < correcterror * (float) mysurf.myscreenwidth) {
      llog.d(TAG, myid + "       decalx " + mysurf.myx + "," + mysurf.myy + "  " + mysurf.mywidth + "x" + mysurf.myheight);
      //mysurf.mywidth += mysurf.myx;
      //mysurf.myx = 0;
      llog.d(TAG, myid + "       decalx " + mysurf.myx + "," + mysurf.myy + "  " + mysurf.mywidth + "x" + mysurf.myheight);
    }
    if (mysurf.myx + mysurf.mywidth < mysurf.myscreenwidth && mysurf.myx + mysurf.mywidth > (1.0f - correcterror) * (float) mysurf.myscreenwidth) {
      llog.d(TAG, myid + "       decalw " + mysurf.myx + "," + mysurf.myy + "  " + mysurf.mywidth + "x" + mysurf.myheight);
      //mysurf.mywidth = mysurf.myscreenwidth - mysurf.myx;
      llog.d(TAG, myid + "       decalw " + mysurf.myx + "," + mysurf.myy + "  " + mysurf.mywidth + "x" + mysurf.myheight);
    }
    if (0 < mysurf.myy && mysurf.myy < correcterror * (float) mysurf.myscreenheight) {
      llog.d(TAG, myid + "       decaly " + mysurf.myx + "," + mysurf.myy + "  " + mysurf.mywidth + "x" + mysurf.myheight);
      //mysurf.myheight += mysurf.myy;
      //mysurf.myy = 0;
      llog.d(TAG, myid + "       decaly " + mysurf.myx + "," + mysurf.myy + "  " + mysurf.mywidth + "x" + mysurf.myheight);
    }
    if (mysurf.myy + mysurf.myheight < mysurf.myscreenheight && mysurf.myy + mysurf.myheight > (1.0f - correcterror) * (float) mysurf.myscreenheight) {
      llog.d(TAG, myid + "       decalh " + mysurf.myx + "," + mysurf.myy + "  " + mysurf.mywidth + "x" + mysurf.myheight);
      //mysurf.myheight = mysurf.myscreenheight - mysurf.myy;
      llog.d(TAG, myid + "       decalh " + mysurf.myx + "," + mysurf.myy + "  " + mysurf.mywidth + "x" + mysurf.myheight);
    }
    */

    mysurf.SettingsXmin = mysurf.mywidth - model.SettingsWidth;
    mysurf.cursorx = mysurf.SettingsXmin + mysurf.SettingsWidth * 0.20f;
    mysurf.cursory = model.settingsYmin + model.GenericCaseH * 0.5f;
    mysurf.splitleft = false;
    mysurf.splittop = false;
    mysurf.splitright = false;
    mysurf.splitbottom = false;
    if (mysurf.myx > 0) {
      mysurf.splitleft = true;
    }
    if (mysurf.myy > 0) {
      mysurf.splittop = true;
    }
    if (mysurf.myx + mysurf.mywidth < model.bigScreenWidth) {
      mysurf.splitright = true;
    }
    if (mysurf.myy + mysurf.myheight < model.bigScreenHeight) {
      mysurf.splitbottom = true;
    }
    //PGBZmysurf.ScreenHeight = -1;
    //PGBZmysurf.ScreenWidth = -1;
    //llog.d(TAG, myid + " correctFragmentLayout()");
    mysurf.fragmentView.setX(mysurf.myx);
    mysurf.fragmentView.setY(mysurf.myy);
    ViewGroup.LayoutParams parms = mysurf.fragmentView.getLayoutParams();
    parms.width = mysurf.mywidth;
    parms.height = mysurf.myheight;
    mysurf.fragmentView.setLayoutParams(parms);
    if (requestlayout)
      mysurf.fragmentView.requestLayout();
    if (mysurf.fragmenttype == Surf.FRAGMENT_TYPE_VIDEO && mysurf.mpvSurfaceView != null) {
      //mysurf.surfacempv.setX(mysurf.myx); // en fait 0 par rapport à pictureview
      //mysurf.surfacempv.setY(mysurf.myy);
      parms = mysurf.mpvSurfaceView.getLayoutParams();
      parms.width = mysurf.mywidth;
      parms.height = mysurf.myheight;
      mysurf.mpvSurfaceView.setLayoutParams(parms);
      if (requestlayout)
        mysurf.mpvSurfaceView.requestLayout();
    }
    // ça va envoyer juste un surfacechanged rien d'autre

    //if (mysurf.fragmenttype == Surf.FRAGMENT_TYPE_VIDEO) {
    //if (mysurf.fragmenttype == Surf.FRAGMENT_TYPE_VIDEO) {
    //  llog.d(TAG, "FRAGMENT_TYPE_VIDEO need to request layout");
    // DUMMYLEQUI
    //mysurf.surfacempv.requestLayout();
    //if (requestlayout)
    //  mainlayout.requestLayout();
    //}
    llog.d(TAG, myid + " END : " + x + "," + y + "  " + width + "x" + height
            + " mysurf " + mysurf.myx + "," + mysurf.myy + "  " + mysurf.mywidth + "x" + mysurf.myheight
            + " myswh " + mysurf.myscreenwidth + "x" + mysurf.myscreenheight
            + " bswh " + model.bigScreenWidth + "x" + model.bigScreenHeight
            + " l" + mysurf.splitleft + " r" + mysurf.splitright + " t" + mysurf.splittop + " b" + mysurf.splitbottom);

  }

  public void correctFragmentLayoutMove(int myid, int distance, int cote) {
    //llog.d(TAG, myid+" correctFragmentLayoutMove()");
    Surf mysurf = model.surf.get(myid);
    if (mysurf.fragmentView == null) {
      llog.d(TAG, myid + " correctFragmentLayoutMove() null");
      return;
    }
    //llog.d(TAG, myid+" correctFragmentLayout() "+distance);
    llog.d(TAG, myid + " : " + mysurf.myx + "," + mysurf.myy + " " + mysurf.mywidth + "x" + mysurf.myheight + " demande correctFragmentLayoutMove()");
    // on force la recalcul des distances et le redessin
    int onsupprimeid = -1;
    int taillemini = 75;
    int corrigex = -1;
    int corrigey = -1;
    if (mysurf.splitleft && cote == 1) {
      if (mysurf.mywidth - distance < taillemini) {
        distance = mysurf.mywidth;
        onsupprimeid = myid;
      }
      corrigex = mysurf.myx;
      mysurf.myx += distance;
      mysurf.mywidth -= distance;
      llog.d(TAG, myid + " : left");
    }
    if (mysurf.splittop && cote == 2) {
      if (mysurf.myheight - distance < taillemini) {
        distance = mysurf.myheight;
        onsupprimeid = myid;
      }
      corrigey = mysurf.myy;
      mysurf.myy += distance;
      mysurf.myheight -= distance;
      llog.d(TAG, myid + " : top");
    }
    if (mysurf.splitright && cote == 3) {
      if (mysurf.mywidth + distance < taillemini) {
        distance = -mysurf.mywidth;
        onsupprimeid = myid;
      }
      corrigex = mysurf.myx + mysurf.mywidth;
      mysurf.mywidth += distance;
      llog.d(TAG, myid + " : right");
    }
    if (mysurf.splitbottom && cote == 4) {
      if (mysurf.myheight + distance < taillemini) {
        distance = -mysurf.myheight;
        onsupprimeid = myid;
      }
      corrigey = mysurf.myy + mysurf.myheight;
      mysurf.myheight += distance;
      llog.d(TAG, myid + " : bottom");
    }
    //llog.d(TAG, myid + " : " + mysurf.myx + "," + mysurf.myy + "  " + mysurf.mywidth + "x" + mysurf.myheight + " après correction");
    int surfl = model.surf.size();
    float errorpc = 10.0f;
    for (int i = 0; i < surfl; i++) {
      mysurf = model.surf.get(i);
      boolean modified = false;
      if (i != myid) {
        //if (mysurf.splitleft && mysurf.myx == corrigex) {
        if (mysurf.splitleft && corrigex - errorpc < mysurf.myx && mysurf.myx < corrigex + errorpc) {
          mysurf.myx += distance;
          mysurf.mywidth -= distance;
          llog.d(TAG, i + " : left");
          modified = true;
        }
        //if (mysurf.splittop && mysurf.myy == corrigey) {
        if (mysurf.splittop && corrigey - errorpc < mysurf.myy && mysurf.myy < corrigey + errorpc) {
          mysurf.myy += distance;
          mysurf.myheight -= distance;
          llog.d(TAG, i + " : top");
          modified = true;
        }
        //if (mysurf.splitright && mysurf.myx + mysurf.mywidth == corrigex) {
        if (mysurf.splitright && corrigex - errorpc < mysurf.myx + mysurf.mywidth && mysurf.myx + mysurf.mywidth < corrigex + errorpc) {
          mysurf.mywidth += distance;
          llog.d(TAG, i + " : right");
          modified = true;
        }
        //if (mysurf.splitbottom && mysurf.myy + mysurf.myheight == corrigey) {
        if (mysurf.splitbottom && corrigey - errorpc < mysurf.myy + mysurf.myheight && mysurf.myy + mysurf.myheight < corrigey + errorpc) {
          mysurf.myheight += distance;
          llog.d(TAG, i + " : bottom");
          modified = true;
        }
      }
      if (modified || (i == myid && onsupprimeid == -1)) {
        //PGBZmysurf.ScreenHeight = -1;
        //PGBZmysurf.ScreenWidth = -1;
        mysurf.fragmentView.setX(mysurf.myx);
        mysurf.fragmentView.setY(mysurf.myy);
        ViewGroup.LayoutParams parms = mysurf.fragmentView.getLayoutParams();
        parms.width = mysurf.mywidth;
        parms.height = mysurf.myheight;
        mysurf.fragmentView.setLayoutParams(parms);
        mysurf.fragmentView.requestLayout();
        if (mysurf.fragmenttype == Surf.FRAGMENT_TYPE_VIDEO && mysurf.mpvSurfaceView != null) {
          //mysurf.surfacempv.setX(mysurf.myx); // en fait 0 par rapport à pictureview
          //mysurf.surfacempv.setY(mysurf.myy);
          parms = mysurf.mpvSurfaceView.getLayoutParams();
          parms.width = mysurf.mywidth;
          parms.height = mysurf.myheight;
          mysurf.mpvSurfaceView.setLayoutParams(parms);
          mysurf.mpvSurfaceView.requestLayout();
          llog.d(TAG, i + " : " + mysurf.myx + "," + mysurf.myy + "  " + mysurf.mywidth + "x" + mysurf.myheight + " après correctFragmentLayoutMove() FRAGMENT_TYPE_VIDEO");
        } else
          llog.d(TAG, i + " : " + mysurf.myx + "," + mysurf.myy + "  " + mysurf.mywidth + "x" + mysurf.myheight + " après correctFragmentLayoutMove()");

        mysurf.SettingsXmin = mysurf.mywidth - model.SettingsWidth;
        mysurf.cursorx = mysurf.mywidth * 0.5f;
        mysurf.cursory = mysurf.myheight * 0.5f;
        // ça va envoyer juste un surfacechanged rien d'autre
      }
    }
    if (onsupprimeid != -1) {
      llog.d(TAG, onsupprimeid + " : correctFragmentLayoutMove() on supprime car trop petit ");
      model.removesurf(onsupprimeid, true);
      int surftagi = model.surf.get(onsupprimeid).surftagi;
      // model.surf.remove(onsupprimeid); on ne peut pas faire ça pour le moment
      Fragment fragment3 = getSupportFragmentManager().findFragmentByTag("f" + surftagi);
      if (fragment3 != null) {
        getSupportFragmentManager().beginTransaction().remove(fragment3).commitNowAllowingStateLoss();
      }
    }

    mainlayout.requestLayout();

  }

  public int splitintwo(int thissurf, float thisratio, boolean vertical, boolean fragmentvideo, boolean requestlayout) {
    Surf original = model.surf.get(thissurf);
    int originalx = original.myx;
    int originaly = original.myy;
    int originalw = original.mywidth;
    int originalh = original.myheight;

    int targeti = -1;
    int f0x, f0y, f0w, f0h, f1x, f1y, f1w, f1h;
    if (vertical) {
      f1w = (int) (thisratio * originalw);
      f0w = originalw - f1w;
      f0h = originalh;
      f1h = originalh;
      f0x = originalx;
      f0y = originaly;
      f1x = originalx + f0w;
      f1y = originaly;
      correctFragmentLayout(thissurf, f0x, f0y, f0w, f0h, requestlayout);
      targeti = addsurf(f1x, f1y, f1w, f1h);
    } else {
      f0w = originalw;
      f1w = originalw;
      f1h = (int) (thisratio * originalh);
      f0h = originalh - f1h;
      f0x = originalx;
      f0y = originaly + f1h;
      f1x = originalx;
      f1y = originaly;
      correctFragmentLayout(thissurf, f1x, f1y, f1w, f1h, requestlayout);
      targeti = addsurf(f0x, f0y, f0w, f0h);
    }

    llog.d(TAG, "------- split in two : " + String.format("%d : (%d,%d) %dx%d   ->   %d : (%d,%d) %dx%d", thissurf, f0x, f0y, f0w, f0h, targeti, f1x, f1y, f1w, f1h));
    Surf surft = model.surf.get(targeti);
    surft.ordnerIndex = original.ordnerIndex;
    surft.mediaIndex = original.mediaIndex;
    surft.ordnerIndexAddress = original.ordnerIndexAddress;
    surft.mediaIndexAddress = original.mediaIndexAddress;
    surft.putbigpictureinmemory = true;

    FragmentBrowser fragment1 = new FragmentBrowser();
    Bundle bundle2 = new Bundle();
    bundle2.putInt("myid", targeti);
    fragment1.setArguments(bundle2);
    getSupportFragmentManager().beginTransaction().add(layoutid, fragment1, "f" + model.surf.get(targeti).surftagi).commitNowAllowingStateLoss();
    return targeti;
  }

  private String sourcehtml = null;
  class MyJavaScriptInterface {
    @JavascriptInterface
    public void processHTML(final String html) {
      //llog.d(TAG,"processed html" + html);
      //sourcehtml = Html.fromHtml(html).toString();
      sourcehtml = html;
      FileWriter fWriter;
      try{
        fWriter = new FileWriter(model.dossiercache + "/page.html", true);
        fWriter.write(html);
        fWriter.flush();
        fWriter.close();
      } catch(Exception e) {
        e.printStackTrace();
      }
      //llog.d(TAG, sourcehtml);
      llog.d(TAG, model.dossiercache + "/page.html");
    }
  }

  private void showwebview(String lienhref, String lienminiature) {
    /*
    retrievecloudflarecookies(intent.getStringExtra("url"));
    getSupportFragmentManager().beginTransaction()
      .add(android.R.id.content, WebViewFragment.newInstance(intent.getStringExtra("url")))
      .addToBackStack(null)
      .commit();
     */
    if (webview == null) {
      webview = new WebView(this);
      //webview.setX(0);
      //webview.setY(0);
      //mainlayout.addView(webview);
      //ViewGroup.LayoutParams parms = webview.getLayoutParams();
      //parms.width = model.surf.get(0).ScreenWidth;
      //parms.height = model.surf.get(0).ScreenHeight;
      //webview.setLayoutParams(parms);
      //mainlayout.requestLayout();
      WebSettings settings = webview.getSettings();
      settings.setDomStorageEnabled(true);
      settings.setJavaScriptEnabled(true);
      webview.addJavascriptInterface(new MyJavaScriptInterface(), "HTMLOUT");
      settings.setUserAgentString(InternetSession.USER_AGENT);
      webview.setWebViewClient(new WebViewClient() {
        boolean timeout = true;
        @Override
        public void onPageStarted(final WebView view, final String url, Bitmap favicon) {
          llog.d(TAG, "onPageStarted " + url);
        }
        @Override
        public void onPageFinished(WebView view, String url) {
          llog.d(TAG, "onPageFinished " + url);
          view.loadUrl("javascript:window.HTMLOUT.processHTML('<html>'+document.getElementsByTagName('html')[0].innerHTML+'</html>');");
          /*if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            view.evaluateJavascript(
              "function() { return ('<html>'+document.getElementsByTagName('html')[0].innerHTML+'</html>'); })();",
              new ValueCallback<String>() {
                @Override
                public void onReceiveValue(String html) {
                  llog.d(TAG, html);
                  // code here
                }
              });
          }*/
          timeout = false;
        }
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
          // redirections
          llog.d(TAG, "shouldOverrideUrlLoading " + url);
          String cookies = model.getcookie(url);
          if (cookies != null) {
            llog.d(TAG, "got cookies for " + url + " : " + cookies);
            //mainlayout.removeView(webview);
            webview = null;
            //mainlayout.requestLayout();
              /*
              new Thread(new Runnable() {
                @Override
                public void run() {
                  InternetSession.posthtml(url, url, "");
                  InternetSession.getthepicturelink(lienhref, lienminiature);
                }
              }).start();
              */
            // http://lifeofcoding.com/2015/04/05/Execute-JavaScript-in-Android-without-WebView/
            return true;  // if true the host application handles the url
          } else {
            view.loadUrl(url);
            return false; // if false the current WebView handles the url
          }
        }
      });
    }
    webview.loadUrl(lienhref);
  }

  private final BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
    @Override
    public void onReceive(Context context, Intent intent) {

      String goal = intent.getStringExtra("goal");

      if (goal.equals("toast")) {
        Toast.makeText(getApplicationContext(), intent.getStringExtra("toast"), Toast.LENGTH_SHORT).show();

      } else if (goal.equals("status")) {
        if (intent.hasExtra("hide")) {
          if (model.deactivateactivitykeydown) {
            //llog.d(TAG, "----------------------------------------------do not hide keep for intent");
            model.lastmessagetime = System.currentTimeMillis();
          } else {
            //llog.d(TAG, "----------------------------------------------hide");
            model.lastmessagetime = 0;
            model.lastmessage = "";
            if (status != null) {
              mainlayout.removeView(status);
              status = null;
              mainlayout.requestLayout();
            }
          }
        } else if (intent.hasExtra("status")) {
          String displaythis = intent.getStringExtra("status");
          if (displaythis == null)
            displaythis = "";
          long currtime = System.currentTimeMillis();
          if (model.lastmessagetime != 0) {
            if (currtime - model.lastmessagetime < Gallery.messagetimestay) {
              if (displaythis.toLowerCase().contains("exception"))
                displaythis = model.lastmessage + "E";
            }
          }
          model.lastmessage = displaythis;
          model.lastmessagetime = currtime;
          if (displaythis.equals("recaptcha")) {
            //displaythislienhref = intent.getStringExtra("lienhref");
            //displaythislienminiature = intent.getStringExtra("lienminiature");
          }
          String[] displaythat = displaythis.split("\n+");
          int displaythati = 0;
          int displaythatl = displaythat.length;
          float largeur = 0.0f;
          float hauteur = 0.0f;
          Rect bounds = new Rect();

          model.MessageTextPaint.getTextBounds("XyPqp,;!|$§?", 0, "XyPqp,;!|$§?".length(), bounds);
          hauteur = bounds.height();
          int maxlines = (int) ((model.bigScreenHeight - model.GenericInterSpace) / hauteur) - 1;
          if (displaythatl - displaythati > maxlines)
            displaythati = displaythatl - maxlines;

          int longest = 0;
          int longestl = 0;
          for (int i = displaythati; i < displaythatl; i++) {
            if (displaythat[i].length() > longestl) {
              longest = i;
              longestl = displaythat[i].length();
            }
          }
          model.MessageTextPaint.getTextBounds("." + displaythat[longest] + ".", 0, longestl + 2, bounds);
          largeur = (longestl * bounds.width()) / (longestl + 1.0f);
          float largmax = model.bigScreenWidth * 1.75f;
          if (largeur > largmax) {
            longestl = (int) ((longestl * largmax) / largeur);
            largeur = largmax;
          }
          float boitelargeur = (int) (largeur + model.GenericInterSpace * 2);
          float boitehauteur = (int) (hauteur * (displaythatl - displaythati) + model.GenericInterSpace * 2);
          float posx = 0.0f;
          float posy = 0.0f;
          if (intent.hasExtra("positionx")) {
            posx = intent.getFloatExtra("positionx", 0.0f);
            posy = intent.getFloatExtra("positiony", 0.0f);
            if (intent.getStringExtra("align").equals("right"))
              posx -= boitelargeur;
          }
          if (model.iswatch)
            posy = model.bigScreenHeight * 0.50f - hauteur * (displaythatl - displaythati) * 0.50f - model.GenericInterSpace;
          else
            posy += model.bigScreenHeight * 0.10f;

          if (status != null) {
            status.setX(posx);
            status.setY(posy);
          } else {
            final float bboitelargeur = boitelargeur;
            status = new ImageView(context);
            status.setX(posx);
            status.setY(posy);
            mainlayout.addView(status);
            status.setOnTouchListener(new View.OnTouchListener() {
              @Override
              public boolean onTouch(View view, MotionEvent motionEvent) {
                /*
                if (displaythishasdata != null) {
                  if (motionEvent.getX() < (float) (view.getWidth()) / 2.0f) {
                    if (displaythishasdata.equals("recaptcha")) {
                      showwebview(displaythislienhref, displaythislienminiature);
                    }
                  }
                }
                */
                /*mainlayout.removeView(status);
                status = null;
                mainlayout.requestLayout();*/
                if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                  if (view != null) {
                    if (motionEvent.getX() < bboitelargeur * 0.25f) { // (float) (view.getWidth()) * 0.25f) {
                      model.deactivateactivitykeydown = true;
                    } else {
                      model.deactivateactivitykeydown = false;
                      model.message();
                    }
                  } else {
                    model.deactivateactivitykeydown = false;
                    model.message();
                  }
                }
                return true; // true ne renvoie pas le clic à ce qui est en-dessous
              }
            });
          }
          ViewGroup.LayoutParams parms = status.getLayoutParams();
          parms.width = (int) boitelargeur;
          parms.height = (int) boitehauteur;
          status.setLayoutParams(parms);

          Bitmap displaystatus = Bitmap.createBitmap(parms.width, parms.height, Bitmap.Config.ARGB_8888);
          Canvas canv = new Canvas(displaystatus);
          RectF rect = new RectF(0, 0, parms.width, parms.height);
          canv.drawRoundRect(rect, model.GenericInterSpace * 3.0f, model.GenericInterSpace * 3.0f, model.MessageBgPaint);
          canv.drawRoundRect(rect, model.GenericInterSpace * 3.0f, model.GenericInterSpace * 3.0f, model.MessageContourPaint);
          float inposy = 0.0f; //model.GenericInterSpace - rectinit.height() - rectinit.top;
          for (int i = displaythati; i < displaythatl; i++) {
            inposy += hauteur;
            String showthat;
            int dl = displaythat[i].length();
            if (dl > longestl) {
              int longestl2 = longestl / 2 - 1;
              showthat = displaythat[i].substring(0, longestl2) + "[]" + displaythat[i].substring(dl - longestl2);
            } else {
              showthat = displaythat[i];
            }
            canv.drawText(showthat, model.GenericInterSpace, inposy, model.MessageTextPaint);
          }

          status.setImageBitmap(displaystatus);
          mainlayout.requestLayout();
          // nécessaire si on modifie le fragment où s'affiche status
          status.bringToFront();
          status.postDelayed(new Runnable() {
            @Override
            public void run() {
              if (status != null) {
                try {
                  status.bringToFront();
                } catch (Exception e) {
                  llog.d(TAG, "status " + e.toString());
                }
              }
            }
          }, 500);

        }

      } else if (goal.equals("showoverlay")) {
        llog.d(TAG, "broadcast showoverlay");
        int thisid = intent.getIntExtra("id", 0);
        int delay = intent.getIntExtra("delay", 0);
        boolean force = intent.getBooleanExtra("force", false);
        if (thisid < model.surf.size()) {
          Fragment fragment1 = getSupportFragmentManager().findFragmentByTag("f" + model.surf.get(thisid).surftagi);
          if (fragment1 != null) {
            ((FragmentBrowser) fragment1).showoverlayinfo(force, 10000);
            ((FragmentBrowser) fragment1).showoverlayinfo(force, 20000);
          }
        }

      } else if (goal.equals("resizevideoframe")) {
        llog.d(TAG, "broadcast resizevideoframe");
        int thisid = intent.getIntExtra("id", 0);
        boolean maxout = intent.getBooleanExtra("maxout", true);
        if (thisid < model.surf.size()) {
          Fragment fragment1 = getSupportFragmentManager().findFragmentByTag("f" + model.surf.get(thisid).surftagi);
          if (fragment1 != null)
            if (model.surf.get(thisid).fragmenttype == Surf.FRAGMENT_TYPE_VIDEO)
              ((FragmentBrowser) fragment1).resizevideoframe(maxout);
        }

      } else if (goal.equals("downloadfile")) {
        String address = intent.getStringExtra("address");
        llog.d(TAG, "downloadfile " + address);
        /*DownloadManager manager = (DownloadManager) getSystemService(Context.DOWNLOAD_SERVICE);
        Uri uri = Uri.parse(address);
        DownloadManager.Request request = new DownloadManager.Request(uri);
        request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE);
        long reference = manager.enqueue(request);*/
        Uri intenturi = Uri.parse(address);
        Intent playintent = new Intent();
        playintent.setAction(Intent.ACTION_VIEW);
        playintent.setDataAndType(intenturi, "text/plain");
        playintent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        playintent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        startActivity(playintent);
       /* try {
          startActivity(playintent);
        } catch (ActivityNotFoundException e) {*/
        // text/html demande un browser qui demande ensuite de télécharger
        //  playintent.setDataAndType(intenturi,"application/vnd.android.package-archive");//"text/plain");
        //  startActivity(Intent.createChooser(playintent, "dialogTitle"));
        //  e.printStackTrace();
        //}

      } else if (goal.equals("checknewfilesinmediastore")) {
        llog.d(TAG, "checknewfilesinmediastore");
        findfoldertoupdate();

      } else if (goal.equals("shownextfileinmediastore")) {
        llog.d(TAG, "shownextfileinmediastore");
        int thisid = intent.getIntExtra("id", 0);
        int maxcount = intent.getIntExtra("numberlast", 0);
        boolean picture = intent.getBooleanExtra("wantpicture", true);
        if (picture)
          findlatestpicture(maxcount, "=_=__=_=", thisid);
        else
          findlatestvideo(maxcount, "=_=__=_=", thisid);

      } else if (goal.equals("autorescan")) {
        input = new EditText(context);
        inputvalidate = new Button(context);
        new IntentRescan(context, intent, model, mainlayout, input, inputvalidate);

      } else if (goal.equals("lockscreenwidgetconf")) {
        input = new EditText(context);
        inputvalidate = new Button(context);
        new IntentLockscreenWidgetConf(context, intent, model, mainlayout, input, inputvalidate);

      } else if (goal.equals("search")) {
        input = new EditText(context);
        inputvalidate = new Button(context);
        new IntentSearch(context, intent, model, mainlayout, input, inputvalidate);

      } else if (goal.equals("musicsearch")) {
        input = new EditText(context);
        inputvalidate = new Button(context);
        new IntentSearchMusic(context, intent, model, mainlayout, input, inputvalidate);

      } else if (goal.equals("filter")) {
        input = new EditText(context);
        inputvalidate = new Button(context);
        new IntentCustomFilter(context, intent, model, mainlayout, input, inputvalidate);

      } else if (goal.equals("createfolder")) {
        int thisid = intent.getIntExtra("id", 0);
        input = new EditText(context);
        inputvalidate = new Button(context);
        new IntentCreateFolder(context, intent, model, mainlayout, input, inputvalidate);

      } else if (goal.equals("bootstraptor")) {
        int thisid = intent.getIntExtra("id", 0);
        input = new EditText(context);
        inputvalidate = new Button(context);
        new IntentBootstrapTor(context, intent, model, mainlayout, input, inputvalidate);

      } else if (goal.equals("searchtor")) {
        int thisid = intent.getIntExtra("id", 0);
        input = new EditText(context);
        inputvalidate = new Button(context);
        new IntentSearchTor(context, intent, model, mainlayout, input, inputvalidate);

      } else if (goal.equals("rootinstall")) {
        int thisid = intent.getIntExtra("id", 0);
        input = new EditText(context);
        inputvalidate = new Button(context);
        new IntentRootInstall(context, intent, model, mainlayout, input, inputvalidate);

      } else if (goal.equals("rootcommand")) {
        int thisid = intent.getIntExtra("id", 0);
        input = new EditText(context);
        inputvalidate = new Button(context);
        new IntentRootCommand(context, intent, model, mainlayout, input, inputvalidate);

      } else if (goal.equals("commentmedia")) {
        int thisid = intent.getIntExtra("id", 0);
        input = new EditText(context);
        inputvalidate = new Button(context);
        new IntentCommentMedia(context, intent, model, mainlayout, input, inputvalidate);

      } else if (goal.equals("destinationfolder")) {
        int thisid = intent.getIntExtra("id", 0);
        input = new EditText(context);
        inputvalidate = new Button(context);
        new IntentDestinationFolder(context, intent, model, mainlayout, input, inputvalidate);

      } else if (goal.equals("renameprefix")) {
        int thisid = intent.getIntExtra("id", 0);
        input = new EditText(context);
        inputvalidate = new Button(context);
        new IntentRenamePrefix(context, intent, model, mainlayout, input, inputvalidate);

      } else if (goal.equals("CreateCollectionOnDisk")) {
        input = new EditText(context);
        inputvalidate = new Button(context);
        new IntentCreateCollection(context, intent, model, mainlayout, input, inputvalidate);

      } else if (goal.equals("addonline")) {
        input = new EditText(context);
        inputvalidate = new Button(context);
        new IntentOnlineSite(context, intent, model, mainlayout, input, inputvalidate);

      } else if (goal.equals("asknotificationpermission")) {
        llog.d(TAG, "we need to ask notification permission");
        asknotificationpermission();

      } else if (goal.equals("backpressed")) {
        int thisid = intent.getIntExtra("id", 0);
        llog.d(TAG, "backpressed asked " + thisid);
        keybackpressed();

      } else if (goal.equals("quit")) {
        int thisid = intent.getIntExtra("id", 0);
        llog.d(TAG, "global quit asked " + thisid);
        finish();

      } else if (goal.equals("startbrowser")) {

        int thisid = intent.getIntExtra("id", 0);
        llog.d(TAG, "startbrowser asked " + thisid);

        int drawingfragmentcount = 0;
        for (int i = 0 ; i < model.surf.size() ; i++) {
          if (model.surf.get(i).fragmenttype == Surf.FRAGMENT_TYPE_DRAWING) {
            drawingfragmentcount += 1;
          }
        }

        // on force la recalcul des distances et le redessin
        Surf mysurf = model.surf.get(thisid);
        mysurf.drawblackscreennext = true;
        mysurf.OptionMenuShown = false;
        // mysurf.foldoptions(); achtung empêche videoaskclose et la sauvegarde des params par le bouton "quit"
        // sera de toute façon appelé par onCreate fragment
        mysurf.SettingsYmin = model.settingsYmin;
        mysurf.cursorx = mysurf.SettingsXmin + mysurf.SettingsWidth * 0.20f;
        mysurf.cursory = model.settingsYmin + model.GenericCaseH * 0.5f;
        mysurf.SettingsYmin = model.settingsYmin;

        if (Gallery.backgroundService != null) {
          boolean videoclosedreplay = true;
          int surfl = model.surf.size();
          for (int i = 0; i < surfl; i++) {
            Surf thissurf = model.surf.get(i);
            if (thissurf.fragmenttype == Surf.FRAGMENT_TYPE_VIDEO && i != thisid) {
              videoclosedreplay = false;
            }
          }
          if (videoclosedreplay) {
            llog.d(TAG, "optionaudioplayeractive && videoclosedreplay");
            Gallery.backgroundService.musicnext();
          } else {
            llog.d(TAG, "optionaudioplayeractive && videoclosedreplay but ther eis still a video running");
          }
        }

        if (mysurf.fragmenttype == Surf.FRAGMENT_TYPE_DRAWING) {
          llog.d(TAG, "----------------------------------------------------------------------------startbrowser closedrawing");
          try {
            model.commandethreaddrawing.put(new String[]{String.valueOf(thisid), "closedrawing", "save"});
          } catch (InterruptedException e) {
            e.printStackTrace();
          }
        }

        if (mysurf.fragmenttype == Surf.FRAGMENT_TYPE_VIDEO) {
          try {
            model.commandethreadbrowser.put(new String[]{String.valueOf(thisid), "start"});
          } catch (InterruptedException e) {
            e.printStackTrace();
          }
        } else {
          FragmentManager fragmentManager = getSupportFragmentManager();
          FragmentTransaction transaction;
          Fragment fragment1 = getSupportFragmentManager().findFragmentByTag("f" + model.surf.get(thisid).surftagi);
          if (fragment1 != null) {
            transaction = fragmentManager.beginTransaction();
            transaction.setReorderingAllowed(true);
            transaction.remove(fragment1);
            transaction.commitNow();
            FragmentBrowser fragment = new FragmentBrowser();
            Bundle bundle = new Bundle();
            bundle.putInt("myid", thisid);
            fragment.setArguments(bundle);
            try {
              model.commandethreadbrowser.put(new String[]{String.valueOf(thisid), "start"});
            } catch (InterruptedException e) {
              e.printStackTrace();
            }
            transaction = fragmentManager.beginTransaction();
            transaction.setReorderingAllowed(true);
            transaction.add(layoutid, fragment, "f" + model.surf.get(thisid).surftagi);
            transaction.commit();
          }
        }

        /*if (drawingfragmentcount == 1) {
          llog.d(TAG, "----------------------------------------------------------------------------startbrowser quit drawing thread");
          try {
            model.commandethreaddrawing.put(new String[]{"-1", "quit", "threaddrawingoff"});
          } catch (InterruptedException e) {
            e.printStackTrace();
          }
        }*/

      } else if (goal.equals("startdrawing")) {
        if (model.threaddrawingstart()) {
          int thisid = intent.getIntExtra("id", 0);
          llog.d(TAG, "startdrawing asked by " + thisid);

          Fragment fragment1 = getSupportFragmentManager().findFragmentByTag("f" + model.surf.get(thisid).surftagi);
          if (fragment1 != null) {
            getSupportFragmentManager().beginTransaction().remove(fragment1).commitNowAllowingStateLoss();
          }
          // on force la recalcul des distances et le redessin
          Surf mysurf = model.surf.get(thisid);
          mysurf.drawblackscreennext = true;
          mysurf.OptionMenuShown = false;
          mysurf.foldoptions();
          mysurf.SettingsYmin = model.settingsYmin;
          mysurf.cursorx = mysurf.SettingsXmin + mysurf.SettingsWidth * 0.20f;
          mysurf.cursory = model.settingsYmin + model.GenericCaseH * 0.5f;
          mysurf.SettingsYmin = model.settingsYmin;
          FragmentDrawing fragment = new FragmentDrawing();
          Bundle bundle = new Bundle();
          bundle.putInt("myid", thisid);
          bundle.putString("cefichier", intent.getStringExtra("cefichier"));
          fragment.setArguments(bundle);
          getSupportFragmentManager().beginTransaction().add(layoutid, fragment, "f" + model.surf.get(thisid).surftagi).commitNowAllowingStateLoss();

          llog.d(TAG, "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++startdrawing initialize " + thisid);
          try {
            model.commandethreaddrawing.put(new String[]{String.valueOf(thisid), "initialize", mysurf.mediaIndexAddress});
          } catch (InterruptedException e) {
            e.printStackTrace();
          }

        }
      } else if (goal.equals("startplaying")) {
        //model.message();
        int thisid = intent.getIntExtra("id", 0);
        String thisgame = intent.getStringExtra("game");
        llog.d(TAG, "startplaying asked by " + thisid);
        Surf mysurf = model.surf.get(thisid);
        mysurf.drawblackscreennext = true;
        mysurf.OptionMenuShown = false;
        mysurf.foldoptions();
        mysurf.SettingsYmin = model.settingsYmin;
        mysurf.cursorx = mysurf.SettingsXmin + mysurf.SettingsWidth * 0.20f;
        mysurf.cursory = model.settingsYmin + model.GenericCaseH * 0.5f;
        mysurf.SettingsYmin = model.settingsYmin;
        Fragment fragment1 = getSupportFragmentManager().findFragmentByTag("f" + model.surf.get(thisid).surftagi);
        if (fragment1 != null) {
          getSupportFragmentManager().beginTransaction().remove(fragment1).commitNowAllowingStateLoss();
        }
        if (thisgame.equals("puzzle")) {
          FragmentGamePuzzle fragment = new FragmentGamePuzzle();
          Bundle bundle = new Bundle();
          bundle.putInt("myid", thisid);
          bundle.putString("cefichier", intent.getStringExtra("cefichier"));
          fragment.setArguments(bundle);
          getSupportFragmentManager().beginTransaction().add(layoutid, fragment, "f" + model.surf.get(thisid).surftagi).commitNowAllowingStateLoss();
        } else if (thisgame.equals("swap")) {
          FragmentGameSwap fragment = new FragmentGameSwap();
          Bundle bundle = new Bundle();
          bundle.putInt("myid", thisid);
          bundle.putString("cefichier", intent.getStringExtra("cefichier"));
          fragment.setArguments(bundle);
          getSupportFragmentManager().beginTransaction().add(layoutid, fragment, "f" + model.surf.get(thisid).surftagi).commitNowAllowingStateLoss();
        } else if (thisgame.equals("hit")) {
          FragmentGameBounceHit fragment = new FragmentGameBounceHit();
          Bundle bundle = new Bundle();
          bundle.putInt("myid", thisid);
          bundle.putString("cefichier", intent.getStringExtra("cefichier"));
          fragment.setArguments(bundle);
          getSupportFragmentManager().beginTransaction().add(layoutid, fragment, "f" + model.surf.get(thisid).surftagi).commitNowAllowingStateLoss();
        } else if (thisgame.equals("curve")) {
          FragmentGameBounceCurve fragment = new FragmentGameBounceCurve();
          Bundle bundle = new Bundle();
          bundle.putInt("myid", thisid);
          bundle.putString("cefichier", intent.getStringExtra("cefichier"));
          fragment.setArguments(bundle);
          getSupportFragmentManager().beginTransaction().add(layoutid, fragment, "f" + model.surf.get(thisid).surftagi).commitNowAllowingStateLoss();
        } else if (thisgame.equals("tilt")) {
          FragmentGameTilt fragment = new FragmentGameTilt();
          Bundle bundle = new Bundle();
          bundle.putInt("myid", thisid);
          bundle.putString("cefichier", intent.getStringExtra("cefichier"));
          fragment.setArguments(bundle);
          getSupportFragmentManager().beginTransaction().add(layoutid, fragment, "f" + model.surf.get(thisid).surftagi).commitNowAllowingStateLoss();
        } else if (thisgame.equals("avoid")) {
          FragmentGameTiltAvoid fragment = new FragmentGameTiltAvoid();
          Bundle bundle = new Bundle();
          bundle.putInt("myid", thisid);
          bundle.putString("cefichier", intent.getStringExtra("cefichier"));
          fragment.setArguments(bundle);
          getSupportFragmentManager().beginTransaction().add(layoutid, fragment, "f" + model.surf.get(thisid).surftagi).commitNowAllowingStateLoss();
        }

      } else if (goal.equals("playvid")) {
        if (model.threadvideostart()) {
          if (Gallery.backgroundService != null)
            Gallery.backgroundService.musicstop();
          int thisid = intent.getIntExtra("id", 0);
          int dossierprincipal = intent.getIntExtra("dossierprincipal", -1);
          int fichierprincipal = intent.getIntExtra("fichierprincipal", -1);
          String thisurl = "not specified";
          String folderaddress = "unspecified";
          boolean zapperplayer = false;
          boolean zapperoriginal = false;
          if (model.surfzappernumber > 0 && dossierprincipal == -1 && fichierprincipal == -1) {
            zapperplayer = intent.getBooleanExtra("zapperplayer", false);
            zapperoriginal = intent.getBooleanExtra("zapperoriginal", false);
          } else {
            thisurl = model.getMediaAddress(dossierprincipal, fichierprincipal);
            folderaddress = model.getOrnderAddress(dossierprincipal);
            model.addvideotemps(thisurl, dossierprincipal, fichierprincipal);
          }

          if (model.surfzappernumber > 0) {
            llog.d(TAG, "                                             zapping asked by " + thisid
                + "\ncommander " + model.surfzappercommander + ":" + thisurl
                + "\norginal   " + model.surfzapperoriginal + ":" + model.surfzapperoriginalvideo
                + "\nplayer    " + model.surfzapperplayer + ":" + model.surfzapperplayervideo);
            if (thisurl.equals(model.surfzapperoriginalvideo)) {
              llog.d(TAG, "                                             replace original same video as original");
              zapperoriginal = true;
              zapperplayer = false;
            } else if (thisurl.equals(model.surfzapperplayervideo)) {
              llog.d(TAG, "                                             replace player same video as player");
              zapperoriginal = false;
              zapperplayer = true;
            }
            if (zapperplayer || zapperoriginal) {
              // on supprime celui qui n'est pas le même
              model.surfzappernumber = 0;
              Surf surfcommand = model.surf.get(model.surfzappercommander);
              if (zapperoriginal) {
                model.surfzapperplayervideo = "surfzapperplayervideo reset equals";
                model.changeBigPicture(model.surfzapperoriginal, surfcommand.ordnerIndex, 0, surfcommand.mediaIndex, 0, true, false);
                try {
                  model.commandethreadvideo.put(new String[]{String.valueOf(model.surfzapperplayer), "startbrowser"});
                } catch (InterruptedException e) {
                }
                Surf surfremet = model.surf.get(model.surfzapperoriginal);
                if (surfremet.mpvlib != null)
                  surfremet.mpvlib.setmutefakevolume(false);
                layoutfragments(1, model.surfzapperplayer, 0.50f, false, false, false);
                layoutfragments(1, model.surfzappercommander, 0.50f, false, false, true);
                model.currentselectedfragment = model.surfzapperoriginal;
              } else {
                model.surfzapperoriginalvideo = "surfzapperoriginalvideo reset equals";
                model.changeBigPicture(model.surfzapperplayer, surfcommand.ordnerIndex, 0, surfcommand.mediaIndex, 0, true, false);
                try {
                  model.commandethreadvideo.put(new String[]{String.valueOf(model.surfzapperoriginal), "startbrowser"});
                } catch (InterruptedException e) {
                }
                Surf surfremet = model.surf.get(model.surfzapperplayer);
                if (surfremet.mpvlib != null)
                  surfremet.mpvlib.setmutefakevolume(false);
                layoutfragments(1, model.surfzapperoriginal, 0.50f, false, false, false);
                layoutfragments(1, model.surfzappercommander, 0.50f, false, false, true);
                model.currentselectedfragment = model.surfzapperplayer;
              }
            } else {
              llog.d(TAG, "                                             play in player " + model.surfzapperplayer);
              // on joue dans le player
              model.surfzapperplayervideo = thisurl;
              Surf surfcommand = model.surf.get(model.surfzappercommander);
              model.changeBigPicture(model.surfzapperplayer, surfcommand.ordnerIndex, 0, surfcommand.mediaIndex, 0, true, false);
              Surf surfremet = model.surf.get(model.surfzapperoriginal);
              if (surfremet.mpvlib != null)
                if (surfremet.mpvlib.ismutedfakevolume)
                  surfremet.mpvlib.setmutefakevolume(false);

              Fragment fragment1 = getSupportFragmentManager().findFragmentByTag("f" + model.surf.get(thisid).surftagi);
              if (fragment1 != null)
                ((FragmentBrowser) fragment1).resizevideoframe(true);

              try {
                model.commandethreadvideo.put(new String[]{String.valueOf(model.surfzapperplayer), "startplaying",
                    thisurl, folderaddress, String.valueOf(surfcommand.mediaIndex)});
              } catch (InterruptedException e) {
                e.printStackTrace();
              }
            }
          } else {
            llog.d(TAG, thisid + " asked playvid " + thisurl);
            // pas de zapper
            model.surfzapperoriginalvideo = thisurl;
            model.surfzapperplayervideo = "surfzapperplayervideo reset no zapper";
            //model.message();
            //llog.d(TAG, "playvid asked by "+thisid);
            Surf mysurf = model.surf.get(thisid);
            mysurf.drawblackscreennext = true;
            mysurf.OptionMenuShown = false;
            mysurf.foldoptions();
            mysurf.SettingsYmin = model.settingsYmin;
            mysurf.cursorx = mysurf.SettingsXmin + mysurf.SettingsWidth * 0.20f;
            mysurf.cursory = model.settingsYmin + model.GenericCaseH * 0.5f;
            mysurf.SettingsYmin = model.settingsYmin;

            Uri intenturi;
            if (thisurl.matches("^(ftp|https?)://.*")) {
              String query = thisurl;
              intenturi = Uri.parse(query);
            } else {
              File file = new File(thisurl);
              intenturi = androidx.core.content.FileProvider.getUriForFile(context, Gallery.packagename + ".provider", file);
            }
            //llog.d(TAG, "playvid : asked " + thisid + " : " + thisurl);

            /*boolean asmusic = intent.getBooleanExtra("asmusic", false);
            if (asmusic) {

              boolean append = intent.getBooleanExtra("append", false);
              llog.d(TAG, "play audio");
              try {
                if (append)
                  model.commandethreadvideo.put(new String[]{String.valueOf(thisid), "startplaying", thisurl, "asmusic", "append"});
                else
                  model.commandethreadvideo.put(new String[]{String.valueOf(thisid), "startplaying", thisurl, "asmusic"});
              } catch (InterruptedException e) {
                e.printStackTrace();
              }

            } else */
            if (thisurl.matches("^drm://.*")) {
              thisurl = thisurl.substring(6);
              Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse(thisurl));
              startActivity(i);

              //} else if (myViewModel.audiopattern.matcher(thisurl).find()) {

            } else if (Gallery.externalviewerpattern.matcher(thisurl).find() || model.miniaturevideo == 0) {
              Intent playintent = new Intent();
              playintent.setAction(Intent.ACTION_VIEW);
              if (Gallery.externalviewerpattern.matcher(thisurl).find()) { // application/pdf
                playintent.setDataAndType(intenturi, "application/*");
              } else {
                playintent.setDataAndType(intenturi, "video/*");
              }
              playintent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
              playintent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
              try {
                startActivity(playintent);
              } catch (ActivityNotFoundException e) {
                // text/html demande un browser qui demande ensuite de télécharger
                playintent.setDataAndType(intenturi, "text/plain");
                startActivity(Intent.createChooser(playintent, "Choose"));
                e.printStackTrace();
                llog.d(TAG, "could not find activity to handle : " + thisurl);
              }
            } else if (model.miniaturevideo >= 1) {

              Fragment fragment1 = getSupportFragmentManager().findFragmentByTag("f" + model.surf.get(thisid).surftagi);
              if (fragment1 != null)
                ((FragmentBrowser) fragment1).resizevideoframe(true);

              try {
                model.commandethreadvideo.put(new String[]{String.valueOf(thisid), "startplaying",
                    thisurl, folderaddress, String.valueOf(fichierprincipal)});
              } catch (InterruptedException e) {
                e.printStackTrace();
              }

            }
          }
        }

      } else if (goal.equals("setclipboard")) {

      } else if (goal.equals("correctallfragmentlayouts2")) {
        llog.d(TAG, "correctallfragmentlayouts2");

        /*int i = intent.getIntExtra("myid", 0);
        int x = intent.getIntExtra("x", 0);
        int y = intent.getIntExtra("y", 0);
        int w = intent.getIntExtra("w", 50);
        int h = intent.getIntExtra("h", 50);

        correctFragmentLayout(i, x, y, w, h, true);
        correctallfragmentlayouts(0, 0, 0, 0);

        mainlayout.invalidate();
        mainlayout.setX(0);
        mainlayout.setY(0);
        mainlayout.requestLayout();

        showSystemUI();

        mainlayout.invalidate();
        mainlayout.setX(0);
        mainlayout.setY(0);
        mainlayout.requestLayout();

        hideSystemUI();

        mainlayout.invalidate();
        mainlayout.setX(0);
        mainlayout.setY(0);
        mainlayout.requestLayout();*/

        //Toast.makeText(getApplicationContext(), " ", Toast.LENGTH_SHORT).show();

        /*int surftagi = model.surf.get(i).surftagi;
        Fragment fragment = getSupportFragmentManager().findFragmentByTag("f" + surftagi);
        if (fragment != null) {
          getSupportFragmentManager().beginTransaction().remove(fragment).commitNowAllowingStateLoss();
          //getSupportFragmentManager().beginTransaction().add(layoutid, fragment, "f" + surftagi).commitNowAllowingStateLoss();
        }*/


      } else if (goal.equals("splitscreenzapping")) {
        int splitnumber = intent.getIntExtra("splitnumber", 0);
        int thissurf = intent.getIntExtra("splitsurf", 0);
        llog.d(TAG, "splitscreenzapping surfzapperoriginal " + thissurf);
        if (model.surfzappernumber == 0) {
          model.surfzappernumber = 1;
          model.videotempslongestlevel = 1;
          model.surfzapperoriginal = thissurf;
          llog.d(TAG, "splitscreenzapping asked from surf " + thissurf);
          int newsurf = layoutfragments(splitnumber, thissurf, 0.50f, false, false, false);
          llog.d(TAG, "splitscreenzapping surfzappercommander " + newsurf);
          model.surfzappercommander = newsurf;
          newsurf = layoutfragments(splitnumber, thissurf, 0.50f, true, true, true);
          llog.d(TAG, "splitscreenzapping surfzapperplayer " + newsurf);
          model.surfzapperplayer = newsurf;
          model.currentselectedfragment = model.surfzappercommander;
        }

      } else if (goal.equals("splitscreen")) {
        int splitnumber = intent.getIntExtra("splitnumber", 0);
        int thissurf = intent.getIntExtra("splitsurf", 0);
        float thisratio = intent.getFloatExtra("splitratio", 0.50f);
        boolean vertical = intent.getBooleanExtra("splitvertical", true);
        llog.d(TAG, "splitscreen asked " + splitnumber + " from surf " + thissurf);
        int newsurf = layoutfragments(splitnumber, thissurf, thisratio, vertical, false, true);
        model.currentselectedfragment = newsurf;
        int surfl = model.surf.size();
        for (int i = 0; i < surfl; i++) {
          Surf thissurfs = model.surf.get(i);
          if (thissurfs.fragmenttype == Surf.FRAGMENT_TYPE_VIDEO) {
            thissurfs.videoShowOptionMenu = false;
            thissurfs.foldoptions();
          } else if (thissurfs.fragmenttype == Surf.FRAGMENT_TYPE_BROWSER) {
            thissurfs.OptionMenuShown = false;
            thissurfs.foldoptions();
          }
        }

      } else if (goal.equals("expandsplitscreen")) {
        int myid = intent.getIntExtra("splitsurf", 0);
        int distance = intent.getIntExtra("splitdistance", 0);
        int cote = intent.getIntExtra("splitcote", 1);
        llog.d(TAG, "expandsplitscreen asked ");
        correctFragmentLayoutMove(myid, distance, cote);

      } else if (goal.equals("deletefiles")) {
        try {
          Intent intent2 = new Intent(Intent.ACTION_OPEN_DOCUMENT_TREE);
          Gallery.actionopendocumentreeasked = DELETEFILES;
          mStartForResult.launch(intent2);
        } catch (android.content.ActivityNotFoundException e) {
          llog.d(TAG, "ActivityNotFoundException " + e.toString());
        }

      } else if (goal.equals("movefiles")) {
        try {
          Intent intent2 = new Intent(Intent.ACTION_OPEN_DOCUMENT_TREE);
          Gallery.actionopendocumentreeasked = MOVEFILES;
          mStartForResult.launch(intent2);
        } catch (android.content.ActivityNotFoundException e) {
          llog.d(TAG, "ActivityNotFoundException " + e.toString());
        }

      } else if (goal.equals("sharefiles")) {
        ArrayList<Uri> imageUris = new ArrayList<>();
        int tempgalleryl = model.deletefiles.size();
        for (int i = 0 ; i < tempgalleryl ; i++) {
          File thisfile = new File(model.deletefiles.get(i));
          Uri thisuri = androidx.core.content.FileProvider.getUriForFile(context,
              Gallery.packagename + ".provider", thisfile);
          imageUris.add(thisuri);
        }
        if (imageUris.size() > 0) {
          Intent shareIntent = new Intent();
          shareIntent.setAction(Intent.ACTION_SEND_MULTIPLE);
          shareIntent.putParcelableArrayListExtra(Intent.EXTRA_STREAM, imageUris);
          shareIntent.setType("image/*");
          startActivity(Intent.createChooser(shareIntent, "Share medias to.."));
        }

      } else if (goal.equals("loadwebsite")) {
        String url = intent.getStringExtra("address");
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse(url));
        i.addCategory(Intent.CATEGORY_BROWSABLE);
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(i);
        llog.d(TAG, "goto " + url);

      } else if (goal.equals("loadsourcehtml")) {
        String url = intent.getStringExtra("address");
        showwebview(url, null);

      } else if (goal.equals("launchwallpaper")) {
        lanceIntentWallpaper(false);

      } else if (goal.equals("launchlockscreen")) {
        lanceIntentWallpaper(true);

      } else if (goal.equals("flagsecure")) {
        if (model.flagsecure) {
          getWindow().addFlags(WindowManager.LayoutParams.FLAG_SECURE);
        } else {
          getWindow().clearFlags(WindowManager.LayoutParams.FLAG_SECURE);
        }

      } else {
        llog.d(TAG, "goal not handled");
        //Toast.makeText(getApplicationContext(), "goal not handled", Toast.LENGTH_SHORT).show();

      }
    }
  };

  private void lanceIntentWallpaper(boolean lockscreen) {
    String prefixe;
    if (lockscreen) {
      prefixe = Wallpaper.lockscreenprefixe;
    } else {
      prefixe = Wallpaper.wallpaperprefixe;
    }
    SharedPreferences.Editor prefEdit = model.preferences.edit();
    prefEdit.putBoolean(prefixe, true);
    prefEdit.commit();

    WallpaperManager wallpaperManager = WallpaperManager.getInstance(this);
    try {
      if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
        if (lockscreen) {
          wallpaperManager.clear(WallpaperManager.FLAG_LOCK);
        } else {
          wallpaperManager.clear(WallpaperManager.FLAG_SYSTEM);
        }
      } else {
        wallpaperManager.clear();
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
      Intent intent = new Intent(WallpaperManager.ACTION_CHANGE_LIVE_WALLPAPER);
      intent.putExtra(WallpaperManager.EXTRA_LIVE_WALLPAPER_COMPONENT, new ComponentName(model.activitycontext, Wallpaper.class));
      //intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
      startActivity(intent);
    }
  }

  public static boolean copyFile(File sourceFile, File destFile) {
    if (destFile == null)
      return false;
    File parent = destFile.getParentFile();
    if (parent == null)
      return false;
    if (!parent.exists()) {
      if (!parent.mkdirs()) {
        llog.d(TAG, "copyFile could not create parent");
        return false;
      }
    }
    if (destFile.exists()) {
      llog.d(TAG, "copyFile destfile exists");
    } else {
      try {
        if (!destFile.createNewFile()) {
          llog.d(TAG, "copyFile could not create file");
        }
      } catch (IOException e) {
        llog.d(TAG, "copyFile error could not create file " + e);
        return false;
      }
    }
    FileChannel source;
    FileChannel destination;
    try {
      source = new FileInputStream(sourceFile).getChannel();
      destination = new FileOutputStream(destFile).getChannel();
      destination.transferFrom(source, 0, source.size());
      destination.close();
      source.close();
    } catch (IOException e) {
      llog.d(TAG, "copyFile error " + e);
    }
    return true;
  }

  public int deletethemallstd() {
    // on élimine les /Android/data/la.daube d'abord
    int fn = model.deletefiles.size();
    int dltd = 0;
    for (int i = 0; i < fn; i++) {
      String thisfile = model.deletefiles.get(i);
      File file = new File(thisfile);
      if (file.exists()) {
        boolean deleted = file.delete();
        if (deleted) {
          dltd += 1;
          try {
            model.commandethreaddatabase.put(new String[]{"deletefilefromdatabase", thisfile.replaceFirst("(.+)/[^/]+$", "$1"), thisfile});
          } catch (InterruptedException e) {
            e.printStackTrace();
          }
          //model.message("deleted " + thisfile);
        } else {
          llog.d(TAG, deleted + "\t: delete file " + file.getName() + " with size " + file.length());
          model.message("could not delete " + thisfile);
        }
      }
    }
    return dltd;
  }

  public void deletethemall(String basedir, DocumentFile pickedDir) {
    for (DocumentFile file : pickedDir.listFiles()) {
      if (file.isDirectory()) {
        deletethemall(basedir + "/" + file.getName(), file);
      } else {
        String currentfile = basedir + "/" + file.getName();
        int fn = model.deletefiles.size();
        for (int i = 0; i < fn; i++) {
          String thisfile = model.deletefiles.get(i);
          if (thisfile.endsWith(currentfile)) {
            boolean deleted = file.delete();
            if (deleted) {
              try {
                model.commandethreaddatabase.put(new String[]{"deletefilefromdatabase", thisfile.replaceFirst("(.+)/[^/]+$", "$1"), thisfile});
              } catch (InterruptedException e) {
                e.printStackTrace();
              }
              model.message("deleted " + currentfile);
            } else {
              llog.d(TAG, deleted + "\t: delete file " + file.getName() + " with size " + file.length());
              model.message("could not delete " + currentfile);
            }
          }
        }
      }
    }
  }

  ActivityResultLauncher<Intent> mStartForResult = registerForActivityResult(
    new ActivityResultContracts.StartActivityForResult(), new ActivityResultCallback<>() {
        @Override
        public void onActivityResult(ActivityResult result) {
          int resultCode = result.getResultCode();
          Intent resultData = result.getData();
          if (resultData == null) {
            llog.d(TAG, "intent null");
            return;
          }
          int requestCode = Gallery.actionopendocumentreeasked;
          llog.d(TAG, "onActivityResult " + requestCode + " resultCode " + resultCode);
          if (/*resultCode != RESULT_OK || */requestCode == -1) {
            llog.d(TAG, "resultcode not ok requestCode " + requestCode);
            return;
          }
          final Uri treeUri = resultData.getData();
          if (treeUri == null) {
            llog.d(TAG, "treeUri null");
            return;
          }
          final DocumentFile pickedDir = DocumentFile.fromTreeUri(getApplicationContext(), treeUri);
          if (pickedDir == null) {
            llog.d(TAG, "pickedDir null");
            return;
          }
          final String pickedname = pickedDir.getName();
          final String safaccepted = getPathFromUri(getApplicationContext(), treeUri, false);
          llog.d(TAG, "-> Storage Access Framework accepted " + safaccepted);

          if (requestCode == MOVEFILES) {
            llog.d(TAG, "MOVEFILES");
            new Thread(new Runnable() {
              @Override
              public void run() {
                try {
                  Thread.sleep(1000);
                } catch (InterruptedException e) {
                  e.printStackTrace();
                }
                int fn = model.deletefiles.size();
                String dstname = safaccepted + "/fichouille.pol";
                for (int i = 0; i < fn; i++) {
                  String thisfile = model.deletefiles.get(i);
                  File file = new File(thisfile);
                  if (!file.exists())
                    llog.d(TAG, "error file " + thisfile + "doesn't exist");
                  File dst = new File(safaccepted, file.getName());
                  dstname = dst.getAbsolutePath();
                  /*if (dst.exists()) {
                    llog.d(TAG, "there is already a file named\n" + dstname);
                    model.message("there is already a file named\n" + dstname);
                  } else {*/
                    boolean deleted = copyFile(file, dst);
                    if (dst.exists()) {
                      llog.d(TAG, "copied " + thisfile + "\nto " + dstname);
                      //model.message("copied " + thisfile + "\nto " + dstname);
                    } else {
                      llog.d(TAG, "could not copy " + thisfile + "\nto " + dstname);
                      model.message("could not copy " + thisfile + "\nto " + dstname);
                    }
                  //}
                  llog.d(TAG, "finished looking for files in directory : " + safaccepted);
                }
                try {
                  model.commandethreaddatabase.put(new String[]{"chercheFichierDansDatabaseSinonRescanSonDossier", safaccepted, dstname, "-1"});
                } catch (InterruptedException e) {
                  e.printStackTrace();
                }
              }
            }).start();
          } else if (requestCode == DELETEFILES) {
            llog.d(TAG, "DELETEFILES");
            new Thread(new Runnable() {
              @Override
              public void run() {
                try {
                  Thread.sleep(1000);
                } catch (InterruptedException e) {
                  e.printStackTrace();
                }
                deletethemallstd();
                if (pickedname != null) {
                  model.message("permission granted\nsearching for files to delete in\n" + pickedname);
                  //llog.d(TAG, "start looking for files in directory : " + pickedname);
                  deletethemall(pickedname, pickedDir);
                  //llog.d(TAG, "finished looking for files in directory : " + pickedname);
                }
              }
            }).start();
          } else if (requestCode == COPYFILES) {
            llog.d(TAG, "COPYFILES");

          } else if (requestCode == READ_REQUEST_CODE) {
            llog.d(TAG, "READ_REQUEST_CODE");
            // List all existing files inside picked directory
            for (DocumentFile file : model.pickedDir.listFiles()) {
              if (file.isDirectory()) {
                for (DocumentFile file2 : file.listFiles()) {
                  if (file2.isDirectory()) {
                    //llog.d(TAG, "Found dir " + file2.getName());
                  } else {
                    //llog.d(TAG, "Found file " + file2.getParentFile().getName() + "/" + file2.getName() + " with size " + file2.length());
                  }
                }
              } else {
                llog.d(TAG, "Found file " + file.getName() + " with size " + file.length());
                boolean delfile = file.delete();
                llog.d(TAG, delfile + " Delete file " + file.getName() + " with size " + file.length());
              }
            }
          }

        }
      }
  );


  public void replaceFragment(View view) {
    //BlankFragment2 blankFragment2 = new BlankFragment2();
    //getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,blankFragment2).addToBackStack(null).commit();
  }


  public String getPathFromUri(final Context context, final Uri uri, final boolean createnew) {
    final String authority = uri.getAuthority();

    if (createnew) {
      llog.d(TAG, "getScheme = " + "\ngetEncodedPath               " + uri.getEncodedPath() + "\ngetLastPathSegment           " + uri.getLastPathSegment() + "\ngetAuthority                 " + uri.getAuthority() + "\ngetEncodedAuthority          " + uri.getEncodedAuthority() + "\ngetEncodedFragment           " + uri.getEncodedFragment() + "\ngetEncodedQuery              " + uri.getEncodedQuery() + "\ngetEncodedSchemeSpecificPart " + uri.getEncodedSchemeSpecificPart() + "\ngetEncodedUserInfo           " + uri.getEncodedUserInfo() + "\ngetFragment                  " + uri.getFragment() + "\ngetHost                      " + uri.getHost() + "\ngetPath                      " + uri.getPath() + "\ngetQuery                     " + uri.getQuery() + "\ngetScheme                    " + uri.getScheme() + "\ngetSchemeSpecificPart        " + uri.getSchemeSpecificPart() + "\ngetUserInfo                  " + uri.getUserInfo());
      for (int i = 0; i < uri.getPathSegments().size(); i++) {
        String path = uri.getPathSegments().get(i);
        llog.d(TAG, "getPathSegments().get(" + i + ") = " + path);
      }
    }

    /*
    ContentResolver resolver = context.getContentResolver();
    ContentProviderClient providerClient = resolver.acquireContentProviderClient(uri);
    try {
      ParcelFileDescriptor descriptor = providerClient.openFile(uri, "r");
      llog.d(TAG, "" + descriptor.toString());
      descriptor.close();
    } catch (IOException | RemoteException e) {
      e.printStackTrace();
    }
    providerClient.close();
    */

    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT && DocumentsContract.isDocumentUri(context, uri)) { // DocumentProvider

      if (authority.equals("com.android.externalstorage.documents")) { // ExternalStorageProvider
        String docId = DocumentsContract.getDocumentId(uri);
        String[] split = docId.split(":");
        String type = split[0];
        String fichier;
        if ("primary".equalsIgnoreCase(type)) {
          if (split.length > 1)
            fichier = Environment.getExternalStorageDirectory() + "/" + split[1];
          else
            fichier = Environment.getExternalStorageDirectory() + "/";
        } else {
          if (new File("storage" + "/" + docId.replace(":", "/")).exists())
            return "/storage/" + docId.replace(":", "/");
          String root = "";
          for (String s : model.basefolder) {
            if (split[1].startsWith("/"))
              root = s + split[1];
            else
              root = s + "/" + split[1];
          }
          if (root.contains(type))
            fichier = "storage" + "/" + docId.replace(":", "/");
          else {
            if (root.startsWith("/storage/") || root.startsWith("storage/"))
              fichier = root;
            else if (root.startsWith("/"))
              fichier = "/storage" + root;
            else
              fichier = "/storage/" + root;
          }
        }
        if (new File(fichier).exists())
          return fichier;
        else
          llog.d(TAG, "ERROR failed to get path from -- isDocumentUri -- com.android.externalstorage.documents --");

      } else if (String.valueOf(uri).contains("com.android.providers.downloads.documents/document/raw")) {
        String fileName = getFilePath(context, uri);
        String subFolderName = getSubFolders(uri);
        String fichier;
        if (fileName != null)
          fichier = Environment.getExternalStorageDirectory().toString() + "/Download/" + subFolderName + fileName;
        else {
          String id = DocumentsContract.getDocumentId(uri);
          final Uri contentUri = ContentUris.withAppendedId(Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));
          fichier = getDataColumn(context, contentUri, null, null);
        }
        if (fichier == null)
          llog.d(TAG, "ERROR failed to get path from -- isDocumentUri -- com.android.providers.downloads.documents/document/raw --");
        else if (new File(fichier).exists())
          return fichier;
        else
          llog.d(TAG, "ERROR failed to get path from -- isDocumentUri -- com.android.providers.downloads.documents/document/raw --");

      } else if (authority.equals("com.android.providers.downloads.documents")) {
        String fileName = getFilePath(context, uri);
        String fichier;
        if (fileName != null)
          fichier = Environment.getExternalStorageDirectory().toString() + "/Download/" + fileName;
        else {
          String id = DocumentsContract.getDocumentId(uri);
          if (id.startsWith("raw:")) {
            id = id.replaceFirst("raw:", "");
            File file = new File(id);
            if (file.exists())
              return id;
          }
          if (id.startsWith("raw%3A%2F")) {
            id = id.replaceFirst("raw%3A%2F", "");
            File file = new File(id);
            if (file.exists())
              return id;
          }
          final Uri contentUri = ContentUris.withAppendedId(Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));
          fichier = getDataColumn(context, contentUri, null, null);
        }
        if (fichier == null)
          llog.d(TAG, "ERROR failed to get path from -- isDocumentUri -- com.android.providers.downloads.documents --");
        else if (new File(fichier).exists())
          return fichier;
        else
          llog.d(TAG, "ERROR failed to get path from -- isDocumentUri -- com.android.providers.downloads.documents --");

      } else if (authority.equals("com.android.providers.media.documents")) {
        final String docId = DocumentsContract.getDocumentId(uri);
        final String[] split = docId.split(":");
        final String type = split[0];

        Uri contentUri = null;
        if ("image".equals(type))
          contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
        else if ("video".equals(type))
          contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
        else if ("audio".equals(type))
          contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;

        final String selection = "_id=?";
        final String[] selectionArgs = new String[]{split[1]};

        String fichier = getDataColumn(context, contentUri, selection, selectionArgs);
        if (fichier == null)
          llog.d(TAG, "ERROR failed to get path from -- isDocumentUri -- com.android.providers.media.documents --");
        else if (new File(fichier).exists())
          return fichier;
        else
          llog.d(TAG, "ERROR failed to get path from -- isDocumentUri -- com.android.providers.media.documents --");

      } else {
        llog.d(TAG, "ERROR -- isDocumentUri -- not handled");

      }
    } else if ("content".equalsIgnoreCase(uri.getScheme())) { // MediaStore (and general)
      if (authority.equals("com.google.android.apps.photos.content")) { // Return the remote address
        return uri.getLastPathSegment();
      } else if (authority.equals("com.android.externalstorage.documents")) {
        String fichier;
        String docId = uri.getLastPathSegment();
        String[] split = docId.split(":");
        String type = split[0];
        if ("primary".equalsIgnoreCase(type)) {
          if (split.length > 1)
            fichier = Environment.getExternalStorageDirectory() + "/" + split[1];
          else
            fichier = Environment.getExternalStorageDirectory() + "/";
        } else {
          if (new File("/storage/" + docId.replace(":", "/")).exists())
            return "/storage/" + docId.replace(":", "/");
          String root = "";
          for (String s : model.basefolder) {
            if (split[1].startsWith("/"))
              root = s + split[1];
            else
              root = s + "/" + split[1];
          }
          if (root.contains(type))
            fichier = "storage" + "/" + docId.replace(":", "/");
          else {
            if (root.startsWith("/storage/") || root.startsWith("storage/"))
              fichier = root;
            else if (root.startsWith("/"))
              fichier = "/storage" + root;
            else
              fichier = "/storage/" + root;
          }
        }
        if (new File(fichier).exists())
          return fichier;
        else
          llog.d(TAG, "ERROR failed to get path from -- isDocumentUri -- content Scheme --");

      }
      String datacolumn = getDataColumn(context, uri, null, null);
      if (datacolumn != null)
        return datacolumn;

    } else if ("file".equalsIgnoreCase(uri.getScheme())) {
      String fichier = uri.getPath();
      if (fichier != null) {
        if (new File(fichier).exists()) {
          return fichier;
        }
      }

    } else {
      llog.d(TAG, "getScheme not handled : " + uri.getScheme());
    }


    String docId = uri.getLastPathSegment();
    String[] split = docId.split(":");
    if (split.length > 1) {
      for (String stdpath : model.surveyfolder) {
        String fich = stdpath + "/" + split[1];
        if (new File(fich).exists())
          return fich;
      }
    }

    String fichier = getNameFromContentUri(context, uri);
    if (fichier != null) {
      for (String stdpath : model.surveyfolder) {
        fichier = stdpath + "/" + fichier;
        if (new File(fichier).exists()) {
          return fichier;
        }
      }
    }

    // else return create new file
    if (createnew)
      return storefiletotmp(context, uri);
    else
      return null;
  }

  private static String getSubFolders(Uri uri) {
    String decodedPath = Uri.decode(String.valueOf(uri));
    if (decodedPath == null)
      return "";
    int beginIndex = decodedPath.indexOf("Download/");
    if (beginIndex == -1)
      return "";
    beginIndex += "Download/".length();
    int endIndex = decodedPath.lastIndexOf("/");
    if (endIndex == -1)
      return "";
    endIndex += 1;
    try {
      return decodedPath.substring(beginIndex, endIndex);
    } catch (IndexOutOfBoundsException e) {
      return "";
    }
  }

  public static String getFilePath(Context context, Uri uri) {
    Cursor cursor = null;
    final String[] projection = {MediaStore.Files.FileColumns.DISPLAY_NAME};
    try {
      cursor = context.getContentResolver().query(uri, projection, null, null,
              null);
      if (cursor != null && cursor.moveToFirst()) {
        final int index = cursor.getColumnIndexOrThrow(MediaStore.Files.FileColumns.DISPLAY_NAME);
        return cursor.getString(index);
      }
    } catch (Exception e) {
      llog.d(TAG, "error" + e.getMessage());
    } finally {
      if (cursor != null)
        cursor.close();
    }
    return null;
  }

  private static String getDataColumn(Context context, Uri uri, String selection, String[] selectionArgs) {
    Cursor cursor = null;
    final String column = "_data";
    final String[] projection = {column};
    try {
      cursor = context.getContentResolver().query(uri, projection, selection, selectionArgs, null);
      if (cursor != null && cursor.moveToFirst()) {
        final int index = cursor.getColumnIndexOrThrow(column);
        return cursor.getString(index);
      }
    } catch (Exception e) {
      llog.d(TAG, "error" + e.getMessage());
    } finally {
      if (cursor != null)
        cursor.close();
    }
    return null;
  }

  public String getNameFromContentUri(Context context, Uri contentUri) {
    String returnstring = null;
    Cursor cursor = null;
    try {
      cursor = context.getContentResolver().query(contentUri, null, null, null, null);
      if (cursor != null && cursor.moveToFirst()) {
        int index = cursor.getColumnIndexOrThrow(OpenableColumns.DISPLAY_NAME);
        String renvoi = cursor.getString(index);
        if (renvoi != null)
          returnstring = renvoi;
        llog.d(TAG, " -> " + index + " DISPLAY_NAME : " + returnstring);
      }
      cursor = context.getContentResolver().query(contentUri, null, null, null, null);
      if (cursor != null && cursor.moveToFirst() && android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.Q) {
        // Error : java.lang.IllegalArgumentException: column 'relative_path' does not exist.
        // Available columns:
        // [document_id, mime_type, _display_name, summary, last_modified, flags, _size]
        int index = cursor.getColumnIndexOrThrow(RELATIVE_PATH);
        String renvoi = cursor.getString(index);
        if (renvoi != null)
          returnstring = renvoi;
        llog.d(TAG, " -> " + index + " RELATIVE_PATH : " + returnstring);
      }
    } catch (Exception e) {
      llog.d(TAG, " -> Error : " + e.toString());
    } finally {
      if (cursor != null)
        cursor.close();
    }
    return returnstring;
  }

  private String storefiletotmp(Context context, Uri uri) {
    String type = context.getContentResolver().getType(uri);
    String outFile = model.dossierbitmaptemp;
    int indexe = 1;
    String extension = "jpg";
    if (type.equals("image/jpeg")) {
      extension = ".jpg";
    } else if (type.startsWith("image")) {        // image/jpeg
      extension = ".jpg";
    } else if (type.startsWith("video")) { // video/
      extension = ".avi";
    } else {
      extension = ".jpg";
    }
    while (new File(outFile + indexe + extension).exists()) {
      indexe += 1;
    }
    outFile += indexe + extension;
    llog.d(TAG, "getType " + type);
    InputStream ins;
    OutputStream out;
    try {
      ins = context.getContentResolver().openInputStream(uri);
      out = new FileOutputStream(outFile);
      int read;
      //int totalsize = 0;
      byte[] bytes = new byte[8192];
      while ((read = ins.read(bytes)) != -1) {
        out.write(bytes, 0, read);
        //totalsize += read;
        //llog.d(TAG, " write " + totalsize);
      }
      ins.close();
      out.close();
      llog.d(TAG, "Copied input stream to " + outFile);
      return outFile;
    } catch (IOException e) {
      llog.d(TAG, "Failed to copy input stream file " + e);
    }
    return null;
  }

  private String storestreamtotmp(Context context, InputStream ins, String type) {
    String outFile = model.dossierbitmaptemp;
    int indexe = 1;
    String extension = "jpg";
    if (type.equals("image/jpeg")) {
      extension = ".jpg";
    } else if (type.startsWith("image")) {        // image/jpeg
      extension = ".jpg";
    } else if (type.startsWith("video")) { // video/
      extension = ".avi";
    } else {
      extension = ".jpg";
    }
    while (new File(outFile + indexe + extension).exists()) {
      indexe += 1;
    }
    outFile += indexe + extension;
    llog.d(TAG, "getType " + type);
    OutputStream out;
    try {
      out = new FileOutputStream(outFile);
      int read;
      //int totalsize = 0;
      byte[] bytes = new byte[8192];
      while ((read = ins.read(bytes)) != -1) {
        out.write(bytes, 0, read);
        //totalsize += read;
        //llog.d(TAG, " write " + totalsize);
      }
      ins.close();
      out.close();
      llog.d(TAG, "Copied input stream to " + outFile);
      return outFile;
    } catch (IOException e) {
      llog.d(TAG, "Failed to copy input stream file " + e);
    }
    return null;
  }

  @Override
  public void onPause() {
    super.onPause();
    llog.d(TAG, "onPause()");
  }

  @Override
  public void onResume() {
    super.onResume();
    Intent intent = getIntent();
    Uri data = intent.getData();
    model.searchMusicAskedFromWidget = intent.getBooleanExtra("SearchMusic", false);
    model.searchMusicAskedFromWidgetR = intent.getStringExtra("SearchMusicR");
    if (model.searchMusicAskedFromWidgetR == null)
      llog.d(TAG, "onResume() searchMusicAskedFromWidgetR = null");
    else
      llog.d(TAG, "onResume() searchMusicAskedFromWidgetR something");
    if (model.searchMusicAskedFromWidget)
      llog.d(TAG, "onResume() searchMusicAskedFromWidget = true");
    else
      llog.d(TAG, "onResume() searchMusicAskedFromWidget = false");
    if (activityfirstcreated)
      llog.d(TAG, "onResume() activityfirstcreated = true");
    else
      llog.d(TAG, "onResume() activityfirstcreated = false");
    if (data != null)
      llog.d(TAG, "onResume() data != null");
    else
      llog.d(TAG, "onResume() data == null");
    if (model.searchMusicAskedFromWidgetR != null) {
      if (Gallery.isMyServiceRunning(backgroundService.class, this)) {
        if (Gallery.backgroundService != null) {
          if (Gallery.backgroundService.threadmusic != null) {
            if (Gallery.backgroundService.threadmusic.model != null) {
              if (model.folderCount <= 0) {
                model.gallery = Gallery.backgroundService.threadmusic.model.gallery;
                model.folderCount = Gallery.backgroundService.threadmusic.model.folderCount;
                llog.d(TAG, "onResume() searchMusicAskedFromWidgetR load music gallery foldercount " + model.folderCount);
              } else {
                llog.d(TAG, "onResume() searchMusicAskedFromWidgetR keep current gallery foldercount " + model.folderCount);
              }
              model.musiclastsearch = model.searchMusicAskedFromWidgetR;
              model.preferences.edit().putString("musiclastsearch", model.searchMusicAskedFromWidgetR).apply();
              Gallery.backgroundService.musicsearch(model.searchMusicAskedFromWidgetR);
              model.searchMusicAskedFromWidgetR = null;
            }
          }
        }
      }
      try {
        model.commandethreadbrowser.put(new String[]{"-1", "musicsearch", "donotaskwhat"});
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
    } else if (model.searchMusicAskedFromWidget) {
      if (Gallery.isMyServiceRunning(backgroundService.class, this)) {
        if (Gallery.backgroundService != null) {
          if (Gallery.backgroundService.threadmusic != null) {
            if (Gallery.backgroundService.threadmusic.model != null) {
              if (model.folderCount <= 0) {
                model.gallery = Gallery.backgroundService.threadmusic.model.gallery;
                model.folderCount = Gallery.backgroundService.threadmusic.model.folderCount;
                llog.d(TAG, "onResume() searchMusicAskedFromWidget load music gallery foldercount " + model.folderCount);
              } else {
                llog.d(TAG, "onResume() searchMusicAskedFromWidget keep current gallery foldercount " + model.folderCount);
              }
            }
          }
        }
      }
      try {
        model.commandethreadbrowser.put(new String[]{"-1", "musicsearch", "askwhat"});
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
    } else if (data != null) {
      boolean failed = false;
      String cefichier = getPathFromUri(this, data, true);
      if (cefichier != null) {
        //File file = new File(thisurl);
        //Uri intentUri = FileProvider.getUriForFile(context, getPackageName() + ".provider", file);
        File fichierz = new File(cefichier);
        llog.d(TAG, "onResume() data != null fichier demandé : " + cefichier + "\n\texiste : " + fichierz.exists());
        if (fichierz.exists()) {
          model.showthisfolder = fichierz.getParent();
          model.showthisfile = cefichier;
          try {
            model.commandethreaddatabase.put(new String[]{"chercheFichierDansDatabaseSinonRescanSonDossier", model.showthisfolder, model.showthisfile, "0"});
          } catch (InterruptedException e) {
            e.printStackTrace();
          }
        } else
          failed = true;
      } else
        failed = true;
      if (failed)
        llog.d(TAG, "fichier demandé : failed, check with ghost commander");
    }
  }

  @Override
  public void onStart() {
    super.onStart();
    llog.d(TAG, "onStart() " + Gallery.broadcastname);
    model.activitycontext = this.getApplicationContext();
    localbroad = LocalBroadcastManager.getInstance(this);
    localbroad.registerReceiver(mMessageReceiver, new IntentFilter(Gallery.broadcastname));
    model.threadbrowserstart();
    model.threadminiaturestart();
    model.threaddatabasestart();
    for (int i = 0; i < model.surf.size(); i++) {
      Surf surf = model.surf.get(i);
      if (surf.SettingsYmin > surf.myy + surf.myheight) {
        surf.SettingsYmin = model.settingsYmin;
        surf.SettingsYmax = surf.myy + surf.myheight;
      }
      if (surf.SettingsYmax < surf.myy) {
        surf.SettingsYmin = model.settingsYmin;
        surf.SettingsYmax = surf.myy + surf.myheight;
      }
      if (surf.fragmenttype == Surf.FRAGMENT_TYPE_BROWSER) {
      } else if (surf.fragmenttype == Surf.FRAGMENT_TYPE_VIDEO) {
        model.threadvideostart();
      } else if (surf.fragmenttype == Surf.FRAGMENT_TYPE_DRAWING) {
        model.threaddrawingstart();
        llog.d(TAG, "onStart() FRAGMENT_TYPE_DRAWING " + i);
      } else if (surf.fragmenttype == Surf.FRAGMENT_TYPE_PUZZLE) {
        llog.d(TAG, "onStart() FRAGMENT_TYPE_PUZZLE " + i);
      } else if (surf.fragmenttype == Surf.FRAGMENT_TYPE_NONE) {
        llog.d(TAG, "onStart() FRAGMENT_TYPE_NONE " + i);
      }
    }

    model.optionaudioplayeractive = model.preferences.getBoolean("playmusic", false);
    model.optiondiscoverserviceactive = model.preferences.getBoolean("ThreadDiscoverActive", false);
    if (model.optionaudioplayeractive || model.optiondiscoverserviceactive) {
      if (!Gallery.isMyServiceRunning(backgroundService.class, model.activitycontext)) {
        llog.d(TAG, "launch myService +++++++++++++++++++++++++++" + (Gallery.backgroundService == null));
        Intent intent = new Intent(model.activitycontext, backgroundService.class);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
          model.activitycontext.startForegroundService(intent);
        else
          model.activitycontext.startService(intent);
      }
    }

    if (model.dossierminiature != null && model.folderCount > 0)
      findfoldertoupdate();
    for (int i = 0; i < model.surf.size(); i++) {
      try {
        model.commandethreadbrowser.put(new String[]{String.valueOf(i), "dontmissupdate"});
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
    }

  }

  public void cleanupMemoryIfOnlyVideo() {
    if (model == null)
      return;
    int mysurfl = model.surf.size();
    if (mysurfl != 1)
      return;
    //if (model.surf.get(0).fragmenttype != Surf.FRAGMENT_TYPE_VIDEO)
    //  return;
    try { // free some memory to avoid being killed by android
      model.commandethreadminiature.put(new String[]{"cleanup", "force", "donotupdateafterwards"});
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
    Surf mysurf = model.surf.get(0);
    String[] cachekeyset = mysurf.getcachekeyset();
    for (String encache : cachekeyset)
      mysurf.removekeyfromcache(encache);
    mysurf.removefromcache();
  }

  @Override
  public void onStop() {
    super.onStop();
    llog.d(TAG, "onStop() stopping...");
    if (model != null) {
      int mysurfl = model.surf.size();
      llog.d(TAG, "freeing  memory to avoid being killed by android number of surf " + mysurfl);
      for (int i = 0; i < mysurfl; i++) {
        Surf mysurf = model.surf.get(i);
        String keepincache = null;
        if (Media.online_no != model.getMediaIsOnline(mysurf.ordnerIndex, mysurf.mediaIndex)) {
          llog.d(TAG, i + " keepincache " + mysurf.mediaIndexAddress);
          keepincache = model.getMediaAddress(mysurf.ordnerIndex, mysurf.mediaIndex);
        }
        String[] cachekeyset = mysurf.getcachekeyset();
        for (String encache : cachekeyset) {
          if (keepincache != null) {
            if (!encache.equals(keepincache)) {
              //llog.d(TAG, i + " remove from cache removal2 " + encache);
              mysurf.removekeyfromcache(encache);
            } else {
              //llog.d(TAG, i + " keepincache2 " + encache);
            }
          } else {
            //llog.d(TAG, i + " remove from cache removal3 " + encache);
            mysurf.removekeyfromcache(encache);
          }
        }
        mysurf.removefromcache();
      }
      if (model != null)
        model.closethreads(false);
      /*model.isMyServiceRunning(ThreadDiscover.class);
      if (model.ThreadDiscoverActive) {
        llog.d(TAG, "onStart ThreadDiscoverActive reBound++++++++++++++++++++++++++++");
      }*/
      /*
      for (Map.Entry<Integer, IntentVideoPlayer> entry : playerlist.entrySet()) {
        entry.getValue().quit();
      }
      */
    }
    localbroad.unregisterReceiver(mMessageReceiver);
    llog.d(TAG, "onStop() stopped");
  }

  
  void deleteRecursive(File fileOrDirectory) {
    /*if (fileOrDirectory != null) {
      if (fileOrDirectory.isDirectory()) {
        for (File child : fileOrDirectory.listFiles()) {
          deleteRecursive(child);
        }
      }
      fileOrDirectory.delete();
    }*/
  }
  
  public void retrievecloudflarecookies(final String curl){
    final WebView webView = new WebView(this.getApplicationContext());
    webView.getSettings().setJavaScriptEnabled(true);
    WebSettings settings = webView.getSettings();
    settings.setDomStorageEnabled(true);
    settings.setUserAgentString("Mozilla/5.0");
    webView.setWebViewClient(new WebViewClient() {
      boolean timeout = true;
      @Override
      public void onPageStarted(final WebView view, final String url, Bitmap favicon) {
        new Thread(new Runnable() {
          @Override
          public void run() {
            for(int i=0; i<10; i++) {
              try {
                Thread.sleep(1000);
              } catch (InterruptedException e) {
                e.printStackTrace();
              }
              String cookies = model.getcookie(url);
              llog.d("YYY", "waiting for cookie");
              if(cookies != null){
                if(cookies.contains("cf_clearance=")){
                  //llog.d(model.TAG, "cf_clearance cookies obtained : " + cookies);
                  //model.secondarysite = true;
                  return;
                }
              }
            }
            llog.d("YYY", "failed to retrieve cookies after 10s");
            view.post(new Runnable() {
              @Override
              public void run() {
                view.loadUrl("url");
              }
            });
          }
        }).start();
      }
      @Override
      public void onPageFinished(WebView view, String url) {
        timeout = false;
      }
      @Override
      public boolean shouldOverrideUrlLoading(WebView view, String url) {
        view.loadUrl(url);
        return false;
      }
    });
    llog.d("YYY", "retrieving cookie");
    webView.loadUrl(curl);
  }

  private void envoiekey(int fragtype, String[] comm) {

    if (fragtype == Surf.FRAGMENT_TYPE_VIDEO) {
      try {
        model.commandethreadvideo.put(comm);
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
    } else if (fragtype == Surf.FRAGMENT_TYPE_BROWSER) {
      try {
        model.commandethreadbrowser.put(comm);
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
    }
  }

  private volatile long lastkeydown = System.currentTimeMillis();
  private volatile int keydowncount = 0;
  private final static float zoomstep = 0.10f;

  private int keydown = -1;

  /*@Override
  public void onBackPressed(){
    llog.w(TAG, "back pressed");
  }*/



  //private long lastkeyprdown = System.currentTimeMillis();
  @SuppressLint("RestrictedApi")
  @Override
  public boolean dispatchKeyEvent(KeyEvent event) {
    int keyCode = event.getKeyCode();
    //llog.w(TAG, "dispatchkeyevent " + keyCode);
    int action = event.getAction();
    if (keydown != -1                   // a key is down
            && keyCode != keydown       // but it is another one
            && keyCode != KeyEvent.KEYCODE_SHIFT_LEFT
            && keyCode != KeyEvent.KEYCODE_SHIFT_RIGHT
            && keyCode != KeyEvent.KEYCODE_ALT_LEFT
            && keyCode != KeyEvent.KEYCODE_ALT_RIGHT
            && keyCode != KeyEvent.KEYCODE_CTRL_LEFT
            && keyCode != KeyEvent.KEYCODE_CTRL_RIGHT
    ) {
      if (System.currentTimeMillis() - lastkeydown > 5000) {
         keydown = -1;                   // our key down is released
      } else {
        llog.d(TAG, "another key is down key refused " + keyCode + " != should be " + keydown + " action " + action);
        return true;
      }
    }
    if (action == KeyEvent.ACTION_DOWN
            && keyCode != KeyEvent.KEYCODE_SHIFT_LEFT  // impossible de faire : = shift+; au clavier sinon
            && keyCode != KeyEvent.KEYCODE_SHIFT_RIGHT
            && keyCode != KeyEvent.KEYCODE_ALT_LEFT
            && keyCode != KeyEvent.KEYCODE_ALT_RIGHT
            && keyCode != KeyEvent.KEYCODE_CTRL_LEFT
            && keyCode != KeyEvent.KEYCODE_CTRL_RIGHT
    ) {
      keydown = keyCode;                // record the new key down
      lastkeydown = System.currentTimeMillis();
      //llog.d(TAG, "record keydown " + keyCode + " action " + action);
    }
    if (action == KeyEvent.ACTION_UP
            && keyCode != KeyEvent.KEYCODE_SHIFT_LEFT
            && keyCode != KeyEvent.KEYCODE_SHIFT_RIGHT
            && keyCode != KeyEvent.KEYCODE_ALT_LEFT
            && keyCode != KeyEvent.KEYCODE_ALT_RIGHT
            && keyCode != KeyEvent.KEYCODE_CTRL_LEFT
            && keyCode != KeyEvent.KEYCODE_CTRL_RIGHT
    ) {
      if (keydown == -1) {              // we didn't handle its key down event
        llog.d(TAG, "key refused " + keyCode + " action " + action);
        return true;
      } else {
        //llog.d(TAG, "key released " + keydown);
        keydown = -1;                   // our key down is released
      }
    }

    if (!model.deactivateactivitykeydown
            //&& model.isandroidtv
            && action == KeyEvent.ACTION_DOWN) {

      if (model.optionaudioplayeractive) {
        if ((keyCode == KeyEvent.KEYCODE_MEDIA_REWIND
                || keyCode == KeyEvent.KEYCODE_MEDIA_PREVIOUS
                || keyCode == KeyEvent.KEYCODE_4)
        ) {
          if (Gallery.backgroundService != null) {
            model.message("music previous");
            Gallery.backgroundService.musicprevious();
            return true;
          }
        } else if ((keyCode == KeyEvent.KEYCODE_MEDIA_FAST_FORWARD
                || keyCode == KeyEvent.KEYCODE_MEDIA_NEXT
                || keyCode == KeyEvent.KEYCODE_5)
        ) {
          if (Gallery.backgroundService != null) {
            model.message("music next");
            Gallery.backgroundService.musicnext();
            return true;
          }
        } else if ((keyCode == KeyEvent.KEYCODE_MEDIA_PLAY
                || keyCode == KeyEvent.KEYCODE_MEDIA_PLAY_PAUSE
                || keyCode == KeyEvent.KEYCODE_6)
        ) {
          if (Gallery.backgroundService != null) {
            model.message("music play/pause");
            Gallery.backgroundService.musicplaypause();
            return true;
          }
        } else if ((keyCode == KeyEvent.KEYCODE_MEDIA_STOP
                || keyCode == KeyEvent.KEYCODE_MEDIA_PAUSE
                || keyCode == KeyEvent.KEYCODE_7)
        ) {
          if (Gallery.backgroundService != null) {
            model.message("music stop");
            Gallery.backgroundService.musicpause();
            return true;
          }
        } else if ((keyCode == KeyEvent.KEYCODE_MEDIA_EJECT
                || keyCode == KeyEvent.KEYCODE_MEDIA_CLOSE
                || keyCode == KeyEvent.KEYCODE_8)
        ) {
          if (Gallery.backgroundService != null) {
            model.message("music random");
            Gallery.backgroundService.musicrandomforce();
            return true;
          }
        } else if ((keyCode == KeyEvent.KEYCODE_MEDIA_RECORD
                || keyCode == KeyEvent.KEYCODE_9)
        ) {
          if (Gallery.backgroundService != null) {
            model.message("music linear");
            Gallery.backgroundService.musiclinearforce();
            return true;
          }
        }
      }

      if ((keyCode == KeyEvent.KEYCODE_0 ||
              keyCode == KeyEvent.KEYCODE_1 || keyCode == KeyEvent.KEYCODE_ZOOM_IN
              || keyCode == KeyEvent.KEYCODE_2 || keyCode == KeyEvent.KEYCODE_ZOOM_OUT
              || keyCode == KeyEvent.KEYCODE_3 || keyCode == KeyEvent.KEYCODE_TV_ZOOM_MODE)
      ) {
        if (0 <= model.currentselectedfragment && model.currentselectedfragment < model.surf.size()) {
          Surf mysurf = model.surf.get(model.currentselectedfragment);
          if (mysurf.fragmenttype == Surf.FRAGMENT_TYPE_BROWSER) {
            float centerscreenx = mysurf.myx + mysurf.mywidth * 0.5f;
            float centerscreeny = mysurf.myy + mysurf.myheight * 0.5f;
            float ratio;
            switch (keyCode) {
              case KeyEvent.KEYCODE_0:
                mysurf.centeronscreen = true;
                model.changeBigPicture(model.currentselectedfragment, -1, 0, -1, 1, true, true);
                try {
                  model.commandethreadbrowser.put(new String[]{"-1", "update"});
                } catch (InterruptedException e) {
                  e.printStackTrace();
                }
                return true;
              case KeyEvent.KEYCODE_1:
              case KeyEvent.KEYCODE_ZOOM_IN:
                ratio = 1.0f + zoomstep;
                mysurf.bpx = centerscreenx - ratio * (centerscreenx - mysurf.bpx);
                mysurf.bpy = centerscreeny - ratio * (centerscreeny - mysurf.bpy);
                mysurf.bscale *= ratio;
                try {
                  model.commandethreadbrowser.put(new String[]{"-1", "update"});
                } catch (InterruptedException e) {
                  e.printStackTrace();
                }
                return true;
              case KeyEvent.KEYCODE_2:
              case KeyEvent.KEYCODE_ZOOM_OUT:
                ratio = 1.0f - zoomstep;
                mysurf.bpx = centerscreenx - ratio * (centerscreenx - mysurf.bpx);
                mysurf.bpy = centerscreeny - ratio * (centerscreeny - mysurf.bpy);
                mysurf.bscale *= ratio;
                try {
                  model.commandethreadbrowser.put(new String[]{"-1", "update"});
                } catch (InterruptedException e) {
                  e.printStackTrace();
                }
                return true;
              case KeyEvent.KEYCODE_3:
              case KeyEvent.KEYCODE_TV_ZOOM_MODE:
                if (mysurf.zoommode == Gallery.ZoomKeep) {
                  mysurf.zoommode = Gallery.ZoomFitToWindow;
                  model.message("fit screen");
                  String fichiername = mysurf.mediaIndexAddress;
                  if (mysurf.isincache(fichiername)) {
                    float[] cachedbitmapnfo = mysurf.getcachedbitmapnfo(fichiername);
                    float scalew = (((float) mysurf.ScreenWidth) / ((float) cachedbitmapnfo[0]));
                    float scaleh = (((float) mysurf.ScreenHeight) / ((float) cachedbitmapnfo[1]));
                    if (scalew < scaleh)
                      mysurf.bscale = scalew;
                    else
                      mysurf.bscale = scaleh;
                    mysurf.bpx = 0.0f;
                    mysurf.bpy = 0.0f;
                    mysurf.centeronscreen = true;
                  }
                } else if (mysurf.zoommode == Gallery.ZoomFitToWindow) {
                  mysurf.zoommode = Gallery.ZoomComic;
                  model.message("keep zoom\nstart top left");
                  String fichiername = mysurf.mediaIndexAddress;
                  if (mysurf.isincache(fichiername)) {
                    float[] cachedbitmapnfo = mysurf.getcachedbitmapnfo(fichiername);
                    float scalew = (((float) mysurf.ScreenWidth) / ((float) cachedbitmapnfo[0]));
                    //float scaleh = (((float) mysurf.ScreenHeight) / ((float) cachedbitmapnfo[1]));
                    mysurf.bscale = scalew;
                    mysurf.bpx = 0.0f;
                    mysurf.bpy = 0.0f;
                  }
                } else if (mysurf.zoommode == Gallery.ZoomComic) {
                  mysurf.zoommode = Gallery.ZoomKeep;
                  model.message("keep frame size");
                }
                try {
                  model.commandethreadbrowser.put(new String[]{"-1", "update"});
                } catch (InterruptedException e) {
                  e.printStackTrace();
                }
                return true;
            }
          }
        }
      }
    }
    return super.dispatchKeyEvent(event);
  }

  private volatile long backup1t = System.currentTimeMillis();
  private volatile boolean backup1 = false;
  private volatile boolean backup2 = false;
  private volatile boolean backup3 = false;

  private long tempslast = System.currentTimeMillis();
  private final static float padstep = 0.075f;
  @Override
  public boolean onKeyDown(int keyCode, KeyEvent event) {
    /**   newkeydown-lastkeydown ~  1er 34570       2e 500ms     puis 50ms      longpress
     *                           ~ 50000ms si l'on effectue un nouveau clic 50s plus tard
     *    newkeydown-lastkeyup   augmente depuis newkeydown-lastkeyup longpress
     */
    long tempsnew = System.currentTimeMillis();
    //llog.w(TAG, String.format("onKeyDown %d", keyCode) + " deltatime=" + (tempsnew-tempslast));
    if (keyCode == KeyEvent.KEYCODE_UNKNOWN
            || (tempsnew-tempslast < 50)
    ) {
      return true;
    }
    tempslast = tempsnew;
    if (keyCode != KeyEvent.KEYCODE_BACK
                  && keyCode != KeyEvent.KEYCODE_DEL
                  && keyCode != KeyEvent.KEYCODE_ESCAPE
                  && keyCode != KeyEvent.KEYCODE_STEM_PRIMARY
                  && status != null) {
      //llog.d(TAG, "----------------------------------------------------onkeydown removeView status");
      model.message();
      /*mainlayout.removeView(status);
      status = null;
      mainlayout.requestLayout();*/
    }
    if (model.currentselectedfragment < 0 || model.currentselectedfragment >= model.surf.size()) {
      llog.d(TAG, "ERREUR onKeyDown model.currentselectedfragment >= model.surf.size()");
      model.currentselectedfragment = 0;
    }
    //llog.d(TAG, "currentselectedfragment " + model.currentselectedfragment);
    if (!model.deactivateactivitykeydown) {
      Surf mysurf = model.surf.get(model.currentselectedfragment);
      if (keyCode == KeyEvent.KEYCODE_DPAD_CENTER || keyCode == KeyEvent.KEYCODE_ENTER || keyCode == KeyEvent.KEYCODE_NUMPAD_ENTER || keyCode == KeyEvent.KEYCODE_MEDIA_PLAY_PAUSE) { // ok au milieu du pad directions
        model.keypadnavactive = true;
        envoiekey(mysurf.fragmenttype, new String[]{String.valueOf(model.currentselectedfragment), "next tv", "ok"});
      } else if (keyCode == KeyEvent.KEYCODE_DPAD_UP) {
        model.keypadnavactive = true;
        keydowncount += 1;
        if (mysurf.fragmenttype == Surf.FRAGMENT_TYPE_BROWSER && !mysurf.OptionMenuShown // keep button down to move around the main picture, don't navigate in the media list
         && ((mysurf.bpymax > mysurf.myheight && mysurf.bpy < mysurf.myheight * 0.95f) || mysurf.bpy < 0.00f || keydowncount > 1)) {
          mysurf.bpy += padstep * mysurf.myheight;
          try {
            model.commandethreadbrowser.put(new String[]{"-1", "update"});
          } catch (InterruptedException e) {
            e.printStackTrace();
          }
        } else {
          keydowncount = 0;
          envoiekey(mysurf.fragmenttype, new String[]{String.valueOf(model.currentselectedfragment), "next tv", "haut"});
        }
      } else if (keyCode == KeyEvent.KEYCODE_DPAD_DOWN) {
        model.keypadnavactive = true;
        keydowncount += 1;
        //  && mysurf.imageheight > mysurf.myheight
        if (mysurf.fragmenttype == Surf.FRAGMENT_TYPE_BROWSER && !mysurf.OptionMenuShown
         && ((mysurf.bpy < 0.0f && mysurf.bpymax > mysurf.myheight * 0.05f) || mysurf.bpymax > mysurf.myheight || keydowncount > 1)) {
          mysurf.bpy -= padstep * mysurf.myheight;
          try {
            model.commandethreadbrowser.put(new String[]{"-1", "update"});
          } catch (InterruptedException e) {
            e.printStackTrace();
          }
        } else {
          keydowncount = 0;
          envoiekey(mysurf.fragmenttype, new String[]{String.valueOf(model.currentselectedfragment), "next tv", "bas"});
        }
      } else if ((keyCode == KeyEvent.KEYCODE_DPAD_LEFT)) {
        model.keypadnavactive = true;
        keydowncount += 1;
        //  && mysurf.imageheight > mysurf.myheight
        if (mysurf.fragmenttype == Surf.FRAGMENT_TYPE_BROWSER && !mysurf.OptionMenuShown
         && ((mysurf.bpxmax > mysurf.mywidth && mysurf.bpx < mysurf.mywidth * 0.25f) || mysurf.bpx < 0.00f || keydowncount > 1)) {
          mysurf.bpx += padstep * mysurf.myheight;
          try {
            model.commandethreadbrowser.put(new String[]{"-1", "update"});
          } catch (InterruptedException e) {
            e.printStackTrace();
          }
        } else {
          keydowncount = 0;
          envoiekey(mysurf.fragmenttype, new String[]{String.valueOf(model.currentselectedfragment), "next tv", "gauche"});
        }
        model.keypadnavactive = true;
      } else if ((keyCode == KeyEvent.KEYCODE_DPAD_RIGHT)) {
        model.keypadnavactive = true;
        keydowncount += 1;
        //  && mysurf.imageheight > mysurf.myheight
        if (mysurf.fragmenttype == Surf.FRAGMENT_TYPE_BROWSER && !mysurf.OptionMenuShown
         && ((mysurf.bpx < 0.0f && mysurf.bpxmax > mysurf.mywidth * 0.75f) || mysurf.bpxmax > mysurf.mywidth || keydowncount > 1)) {
          mysurf.bpx -= padstep * mysurf.myheight;
          try {
            model.commandethreadbrowser.put(new String[]{"-1", "update"});
          } catch (InterruptedException e) {
            e.printStackTrace();
          }
        } else {
          keydowncount = 0;
          envoiekey(mysurf.fragmenttype, new String[]{String.valueOf(model.currentselectedfragment), "next tv", "droite"});
        }
        model.keypadnavactive = true;
      } else if ((keyCode == KeyEvent.KEYCODE_GUIDE
              || keyCode == KeyEvent.KEYCODE_TAB
              || keyCode == KeyEvent.KEYCODE_SETTINGS
              || keyCode == KeyEvent.KEYCODE_MENU
              || keyCode == KeyEvent.KEYCODE_PAGE_UP
              || keyCode == KeyEvent.KEYCODE_PAGE_DOWN
      )) {// bouton menu en fait guide des programmes
        //mysurf.bailouthidemenu = true;
        model.keypadnavactive = true;
        envoiekey(mysurf.fragmenttype, new String[]{String.valueOf(model.currentselectedfragment), "next tv", "menu"});
      }
    }
    return super.onKeyDown(keyCode, event);
  }

  @Override
  public boolean onKeyUp(int keyCode, KeyEvent event) {
    //llog.w(TAG, "onKeyUp " + keyCode);
    if (!model.deactivateactivitykeydown &&
            (keyCode == KeyEvent.KEYCODE_BACK
            || keyCode == KeyEvent.KEYCODE_DEL
            || keyCode == KeyEvent.KEYCODE_ESCAPE
            || keyCode == KeyEvent.KEYCODE_STEM_PRIMARY)
      ) {
      return keybackpressed();
    }
    /*lastkeyup = System.currentTimeMillis();
    if (keydowncount == 1) {
      llog.d(TAG, "onKeyUp");
      if (keyCode == KeyEvent.KEYCODE_DPAD_UP) {
        Surf mysurf = model.surf.get(model.currentselectedfragment);
        mysurf.keypressdown = 0;
        envoiekey(mysurf.fragmenttype, new String[]{String.valueOf(model.currentselectedfragment), "next tv", "haut"});
      } else if (keyCode == KeyEvent.KEYCODE_DPAD_DOWN) {
        Surf mysurf = model.surf.get(model.currentselectedfragment);
        mysurf.keypressdown = 0;
        envoiekey(mysurf.fragmenttype, new String[]{String.valueOf(model.currentselectedfragment), "next tv", "bas"});
      } else if (keyCode == KeyEvent.KEYCODE_DPAD_LEFT) {
        Surf mysurf = model.surf.get(model.currentselectedfragment);
        mysurf.keypressdown = 0;
        envoiekey(mysurf.fragmenttype, new String[]{String.valueOf(model.currentselectedfragment), "next tv", "gauche"});
      } else if (keyCode == KeyEvent.KEYCODE_DPAD_RIGHT) {
        Surf mysurf = model.surf.get(model.currentselectedfragment);
        mysurf.keypressdown = 0;
        envoiekey(mysurf.fragmenttype, new String[]{String.valueOf(model.currentselectedfragment), "next tv", "droite"});
      }
    }*/
    keydowncount = 0;
    return super.onKeyUp(keyCode, event);
  }

  private boolean keybackpressed() {
    if (!model.deactivateactivitykeydown) {
      Surf surfnotabrowser = null;
      for (Surf surf : model.surf) {
        if (!surf.idle && surf.fragmenttype != Surf.FRAGMENT_TYPE_NONE && surf.fragmenttype != Surf.FRAGMENT_TYPE_BROWSER) {
          surfnotabrowser = surf;
          break;
        }
      }
      if (input != null || inputvalidate != null || webview != null || webviewhidden != null) {
        if (input != null) {
          llog.d(TAG, "removeView input");
          mainlayout.removeView(input);
          input = null;
        }
        if (inputvalidate != null) {
          llog.d(TAG, "removeView inputvalidate");
          mainlayout.removeView(inputvalidate);
          inputvalidate = null;
        }
        if (webview != null) {
          llog.d(TAG, "removeView webview");
          mainlayout.removeView(webview);
          webview = null;
        }
        if (webviewhidden != null) {
          llog.d(TAG, "removeView webviewhidden");
          mainlayout.removeView(webviewhidden);
          webviewhidden = null;
        }
        mainlayout.requestLayout();

        return true;
      } else if (model.surfzappernumber > 0) {
        Intent intent = new Intent();
        intent.setAction(Gallery.broadcastname);
        intent.putExtra("goal", "playvid");
        intent.putExtra("zapperoriginal", true);
        LocalBroadcastManager.getInstance(model.activitycontext).sendBroadcast(intent);

        return true;
      } else if (surfnotabrowser != null) {
        if (surfnotabrowser.optiondiaporamaactive) {
          surfnotabrowser.optiondiaporamaactive = false;
        }
        if (surfnotabrowser.fragmenttype == Surf.FRAGMENT_TYPE_VIDEO) {
          try {
            model.commandethreadvideo.put(new String[]{String.valueOf(surfnotabrowser.myid), "startbrowser"});
          } catch (InterruptedException e) {
          }
        } else {
          Intent intent = new Intent();
          intent.setAction(Gallery.broadcastname);
          intent.putExtra("goal", "startbrowser");
          intent.putExtra("id", surfnotabrowser.myid);
          LocalBroadcastManager.getInstance(model.activitycontext).sendBroadcast(intent);
        }

        model.currentselectedfragment = surfnotabrowser.myid;

        return true;
      } else {
        llog.d(TAG, "        back down----");
        long newtime = System.currentTimeMillis();
        if (!backup1 || (newtime - backup1t) > 3000) {
          llog.d(TAG, "        !backup1");
          backup1 = true;
          backup1t = newtime;
          if (model.iswatch || model.isauto) {
            backup2 = true;
            backup3 = false;
          } else {
            backup2 = false;
            backup3 = false;
          }
        } else if (backup1 && !backup2) {
          llog.d(TAG, "        backup1 && !backup2");
          backup2 = true;
        } else if (backup2 && !backup3) {
          llog.d(TAG, "        backup2 && !backup3");
          backup3 = true;
          finish();
        }
        return true;
      }
    }
    return true;
  }

  @Override
  public Integer onFragmentInteraction(Uri uri) {
    llog.d(TAG, "new interaction from the fragment calling the activity");
    //performFileSearch();
    return 35;
  }

  private void setClipboard(String text) {
    android.content.ClipboardManager clipboard = (android.content.ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
    android.content.ClipData clip = android.content.ClipData.newPlainText("Copied Text", text);
    clipboard.setPrimaryClip(clip);
  }











}















