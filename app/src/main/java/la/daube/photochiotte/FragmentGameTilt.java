package la.daube.photochiotte;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import java.util.Random;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link FragmentGameTilt.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link FragmentGameTilt#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FragmentGameTilt extends Fragment {
  private static final String ARG_PARAM1 = "param1";
  private static final String ARG_PARAM2 = "param2";
  private static final String TAG = "YYYfg5";

  private Gallery model;

  public int myid = -1;
  private Surf mysurf = null;

  public FragmentGameTilt() {
    // Required empty public constructor
  }

  public static FragmentGameTilt newInstance(String param1, String param2) {
    FragmentGameTilt fragment = new FragmentGameTilt();
    Bundle args = new Bundle();
    args.putString(ARG_PARAM1, param1);
    args.putString(ARG_PARAM2, param2);
    fragment.setArguments(args);
    return fragment;
  }

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    if (getArguments() != null) {
      myid = getArguments().getInt("myid", -1);
      cefichier = getArguments().getString("cefichier", "");
    }
    //model = ViewModelProviders.of(getActivity()).get(myViewModel.class);
    model = new ViewModelProvider(requireActivity()).get(Gallery.class);
    model.currentselectedfragment = myid;
    llog.d(TAG, myid+" onCreate() " + myid + " -> " + cefichier);
  }

  @Override
  public void onResume() {
    super.onResume();
    llog.d(TAG, myid+" onResume()");
  }

  @Override
  public void onStart() {
    super.onStart();
    llog.d(TAG, myid+" onStart()");
  }

  @Override
  public void onPause() {
    super.onPause();
    llog.d(TAG, myid+" onPause()");
  }

  @Override
  public void onDestroy() {
    super.onDestroy();
    llog.d(TAG, myid+" onDestroy()");
    isrunning = false;
    isreallyrunning = false;
    mWood.recycle();
    //mamaow.recycle();
    /*for(int uu=0;uu<parterre.length;uu++)
      parterre[uu].recycle();
    for(int uu=0;uu<pasterre.length;uu++)
      pasterre[uu].recycle();*/
    //if(affiche==true)
    //  zzz.start();
  }

  private SensorManager mSensorManager;

  //ceci sera dans l'ordre dans les préférences !!!
  private int tempsaffiche;
  private int tempscalcul;
  private double limaccel;	//0.1f
  private double decalache;	//0.01f niveau de difficulté 1 = le plus dur
  private double attir;	//champ d'attraction 0.0005f
  private double multiple;	//champ de répulsion 0.0010f
  private double glurp;	//durée de deviation 0.005f=200fois
  private double mulipli;	//durée de deviation 0.005f=200fois
  private double sBallDiameter = 0.002f;
  private double sBallDiameter2;
  private double ballibump;//toucher bump
  private double bums;
  private double field=0.025f;//=0.02f;
  private double incertitude=0.06f;

  private double[][] pose=null;
  private String wavou;
  static private Uri hurry;
  private Thread zzz;
  private String cefichier=null;
  private Bitmap mamaow = null;
  private Bitmap[] parterre = null;
  private Bitmap[] pasterre = null;
  private Bitmap[] debutbits = null;
  private Bitmap mBitmap = null;
  private Bitmap mWood = null;
  private volatile int largeur=480;
  private volatile int nwaow=0;
  private volatile boolean tourne=false;
  private volatile int hauteur=800;
  private volatile double bbx=0.0f;
  private volatile double bby=0.0f;
  private volatile boolean isrunning=false;
  private volatile boolean isreallyrunning=false;
  private volatile double[] touche={-1.0f,-1.0f};
  private volatile boolean bloquelikethis=false;
  private double msx=0.0f;
  private double msy=0.0f;
  private double valeurmemo=0.0f;
  private int supernw=4;
  private int supernh=5;//>16 !!!!
  private int prefalpha=80;
  private int prefcouleur=Color.YELLOW;
  private final double sFriction = 0.10f;//0.10f
  private int nombre;//baballes et troutrous
  private int nombrem;//portails
  private int nombrep;//croix
  private volatile double mSensorX;
  private volatile double mSensorY;
  private final int[][] coord=new int[][]{
          {1,0},  //0=E
          {0,-1}, //1=N
          {-1,0}, //2=W
          {0,1},  //3=S
  };
  private Bitmap trou = null;
  private Bitmap paf = null;
  private Bitmap paf2 = null;
  private Bitmap up = null;
  private Bitmap left = null;
  private Random r5;
  private int coco;//11max
  private int lili;
  private double mXDpi;
  private double mYDpi;
  private double mMetersToPixelsX;
  private double mMetersToPixelsY;
  private double mXOrigin;
  private double mYOrigin;
  private double mHorizontalBound;
  private double mVerticalBound;
  private int[] pointxi;
  private int[] pointyi;
  private double[] pointx;
  private double[] pointy;
  private int[] murxi;
  private int[] muryi;
  private int[] murpxi;
  private int[] murpyi;
  private double[] murpx;
  private double[] murpy;
  private double[] murx;
  private double[] mury;
  private boolean[][] scatterize;//emplacements privilégiés des pièges
  private Canvas canv;
  private Paint paint;
  private int colonnes;
  private int lignes;
  private int[] avamur;
  private int[] avatrou;
  private int[] avapif;
  private int[] conversion;
  private double[][] hvmur;
  private boolean[] validecoord=new boolean[4];
  private int[][] chemin;
  private int scorr=0;
  private int scorr2=0;
  private double mHoriz2,mVert2;
  private Paint ppa;
  private int dif1=0;
  private int maximur=0;
  private int maxiconvers=0;
  private int[][] decoupage;
  private volatile boolean[] libre;
  private Particle[] mBalls;
  private volatile boolean evenement=false;
  private volatile boolean[][] evene;
  private volatile double[] blocktimexy={0.0f,0.0f};
  private volatile double demardiv=64.0f;

  public double gauss(double x){
    double z=x-0.5;
    double zz=z*z*z;
    z=4.0*zz+0.5;
    return z;
  }

  public double randum(double y){
    double u=Math.random();
    u=gauss(u);
    double v=(double)(u);
    v=y*v;
    return v;
  }

  @SuppressLint("ClickableViewAccessibility")
  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
    mysurf = model.surf.get(myid);
    mysurf.fragmenttype = Surf.FRAGMENT_TYPE_PUZZLE;
    mysurf.fragmentView = inflater.inflate(R.layout.fragment_game_hit, container, false);

    mysurf.fragmentView.setX(model.surf.get(myid).myx);
    mysurf.fragmentView.setY(model.surf.get(myid).myy);
    ViewGroup.LayoutParams parms = mysurf.fragmentView.getLayoutParams();
    parms.width = model.surf.get(myid).mywidth;
    parms.height = model.surf.get(myid).myheight;
    mysurf.fragmentView.setLayoutParams(parms);
    RelativeLayout relativelayout = mysurf.fragmentView.findViewById(R.id.relativelayoutHit);
    relativelayout.setLayoutParams(parms);

    llog.d(TAG, myid+" onCreateView()");
    prefcouleur=model.preferences.getInt("grid_color",Color.YELLOW);
    prefalpha=model.preferences.getInt("grid_intensity",80);
    supernw=model.preferences.getInt("cut_col",6);
    supernh=model.preferences.getInt("cut_lig",10);
    supernw = model.gamecolumns;
    supernh = model.gamelines;

    final double[] defval=ballz.defaut[5];
    tempsaffiche=(int)model.preferences.getFloat("5param0",(float) defval[0]);
    tempscalcul=(int)model.preferences.getFloat("5param1",(float) defval[1]);
    limaccel=(model.preferences.getFloat("5param2",(float) defval[2]));	//hole friction
    decalache=(model.preferences.getFloat("5param3",(float) defval[3]));	//0.01f facilité
    glurp=10000.0f*(model.preferences.getFloat("5param6",(float) defval[6]));	//durée de déviation en 10ms
    mulipli=(model.preferences.getFloat("5param7",(float) defval[7]));	//importance de déviation
    incertitude=(model.preferences.getFloat("5param8",(float) defval[8]));	//importance de déviation
    sBallDiameter=(model.preferences.getFloat("5param10",(float) defval[10])); //taille de la bille
    attir=sBallDiameter*(model.preferences.getFloat("5param4",(float) defval[4]));	//champ d'attraction 0.0005f
    multiple=sBallDiameter*(model.preferences.getFloat("5param5",(float) defval[5]));	//champ de répulsion 0.0010f
    bums = (model.preferences.getFloat("5param17", (float) defval[17]));  //bumheight intensity kinkyness

    sBallDiameter2 = sBallDiameter * sBallDiameter;
    glurp=glurp/((double)(tempscalcul+1));
    nombre=(int)(randum(15.0f))+1;//baballes et troutrous
    nombrem=(int)(randum(16.0f))+4;//portails
    nombrep=(int)(randum(4.0f))+1;//croix
    r5= Gallery.rand;
    coco=8+r5.nextInt(4);//11max
    lili=coco*2;
    coco = model.gamecolumns;
    lili = model.gamelines;
    ppa=new Paint();

    mBalls= new Particle[nombre];
    for (int i = 0; i < nombre; i++) {
      mBalls[i] = new Particle();
    }
    evene=new boolean[nombre][2];
    for (int i = 0; i < nombre; i++) {
      evene[i][0]=false;
      evene[i][1]=false;
    }

    // Restore preferences
    valeurmemo = model.preferences.getFloat("inCLINAIS", 0.0f);
    bloquelikethis=model.preferences.getBoolean("blokoupas",false);
    bbx=valeurmemo;
    bby=-valeurmemo;
    // Get an instance of the SensorManager
    mSensorManager = (SensorManager) getActivity().getApplicationContext().getSystemService(Context.SENSOR_SERVICE);

    largeur= model.surf.get(myid).mywidth;
    hauteur= model.surf.get(myid).myheight;
    nwaow = 1;

    if (mysurf.isincache(cefichier)) {
      mamaow = mysurf.getcachedbitmap(cefichier);
    }
    llog.d(TAG, cefichier + " : " + mamaow.getWidth() + " xxxxx " + mamaow.getHeight());

    demarrage();

    //final SimulationView imageviewpicture = mysurf.pictureview.findViewById(R.id.viewImageHit);
    SimulationView imageviewpicture = new SimulationView(getActivity().getApplicationContext());
    relativelayout.addView(imageviewpicture);

    mysurf.fragmentView.setOnTouchListener(new View.OnTouchListener() {
      public boolean onTouch(View arg0, MotionEvent arg1) {
        model.currentselectedfragment = myid;
        ajusteoriente();
				/*
				if(arg1.getAction()==MotionEvent.ACTION_DOWN && isreallyrunning){
					touche[0]=arg1.getX();
					touche[1]=arg1.getY();
				}
				*/
        return false;
      }
    });

    // Inflate the layout for this fragment
    return mysurf.fragmentView;
  }


  public void ajusteoriente() {
    boolean nerienfaire = false;
    if (tourne == true && (bbx != valeurmemo || bby != 0.0f)) {
      nerienfaire = true;
      bbx = valeurmemo;
      bby = 0.0f;
    }
    if (tourne == false && (bby != -valeurmemo || bbx != 0.0f)) {
      nerienfaire = true;
      bby = -valeurmemo;
      bbx = 0.0f;
    }
    if (nerienfaire == false) {
      if (tourne == true) {
        valeurmemo = Math.abs(msx);
        if (valeurmemo < 0.0f)
          valeurmemo = 0.0f;
        bbx = valeurmemo;
      } else {
        valeurmemo = msy;
        if (valeurmemo < 0.0f)
          valeurmemo = 0.0f;
        bby = -valeurmemo;
      }
    }
  }

  public void executecetruc() {
    final Uri hurryup = hurry;
    zzz = new Thread(new Runnable() {
      public void run() {
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_VIEW);
        intent.setDataAndType(hurryup, "image/jpeg");
        startActivity(intent);

      }
    });
  }

  public Bitmap ShrinkBitmap(String file) {
    int height=1000;
    int width=height;
    BitmapFactory.Options bmpFactoryOptions = new BitmapFactory.Options();
    bmpFactoryOptions.inJustDecodeBounds = true;
    Bitmap bitmap = BitmapFactory.decodeFile(file, bmpFactoryOptions);
    double oh=(double)(bmpFactoryOptions.outHeight);
    double ow=(double)(bmpFactoryOptions.outWidth);
    int heightRatio = (int)Math.ceil(oh/(double)height);//ceil
    int widthRatio = (int)Math.ceil(ow/(double)width);

    if (heightRatio > 1 || widthRatio > 1)
    {
      if (heightRatio > widthRatio)
      {
        bmpFactoryOptions.inSampleSize = heightRatio;
      } else {
        bmpFactoryOptions.inSampleSize = widthRatio;
      }
    }

    bmpFactoryOptions.inJustDecodeBounds = false;
    bmpFactoryOptions.inPreferQualityOverSpeed=true;
    bitmap = BitmapFactory.decodeFile(file, bmpFactoryOptions);
    if (bitmap==null)
      nwaow=0;
    return bitmap;
  }

  public void choiximage() {
      /*File imagi = cefichier;
      wavou = imagi.getAbsolutePath();
      hurry = Uri.fromFile(imagi);
      executecetruc();
      mamaow = ShrinkBitmap(wavou);
      int www = mamaow.getWidth();
      int wwh = mamaow.getHeight();
      if (www > wwh) {
          Bitmap mama = Bitmap.createBitmap(wwh, www, Config.ARGB_8888);
          for (int v = 0; v < wwh; v++)
              for (int u = 0; u < www; u++)
                  mama.setPixel((wwh-1-v), u, mamaow.getPixel(u, v));
          mamaow = Bitmap.createBitmap(mama);
          mama.recycle();
          tourne = true;
      }*/
  }

  public Bitmap rotimage(Bitmap droitt) {
    int www2 = droitt.getWidth();
    int wwh2 = droitt.getHeight();
    Bitmap mamie = Bitmap.createBitmap(wwh2, www2, Bitmap.Config.ARGB_8888);
    for (int v = 0; v < wwh2; v++)
      for (int u = 0; u < www2; u++)
        mamie.setPixel(v, (www2 - 1 - u), droitt.getPixel(u, v));
    Bitmap mamy = Bitmap.createBitmap(mamie);
    mamie.recycle();
    return mamy;
  }

    /*
	@Override
	public boolean onKeyUp(int keyCode, KeyEvent event) {
		switch (keyCode) {
		case KeyEvent.KEYCODE_BACK:
			isrunning = false;
			isreallyrunning = false;
			return true;
		case KeyEvent.KEYCODE_MENU:
			bloquelikethis = !bloquelikethis;
			return true;
		case KeyEvent.KEYCODE_SEARCH:
			ajusteoriente();
			return true;
		default:
			return super.onKeyUp(keyCode, event);
		}
	}
	*/



  public void demarrage() {
    //DisplayMetrics metrics = new DisplayMetrics();
    //getWindowManager().getDefaultDisplay().getMetrics(metrics);
    mXDpi = model.mXDpi;
    mYDpi = model.mYDpi;
    mMetersToPixelsX = mXDpi / 0.0254f;
    mMetersToPixelsY = mYDpi / 0.0254f;
    int taratatax=(int)(((double)largeur)/22.0f);
    int taratatay=(int)(((double)largeur*mXDpi/mYDpi)/22.0f);
    // rescale the ball so it's about 0.5 cm on screen
    Bitmap ball = BitmapFactory.decodeResource(getResources(), R.drawable.ball);
    // taille réelle de la balle
    final int dstWidth = (int) (sBallDiameter * mMetersToPixelsX);
    final int dstHeight = (int) (sBallDiameter * mMetersToPixelsY);
    // trou croix fleche
    final int dstWidth2 = taratatax;
    final int dstHeight2 = taratatay;
    // mouche vache
    int dstWidth5 = taratatax * 8;
    int dstHeight5 = taratatay * 8;
    mBitmap = Bitmap.createScaledBitmap(ball, dstWidth, dstHeight, true);
    Bitmap boing = BitmapFactory.decodeResource(getResources(),
            R.drawable.boing);
    Bitmap boing2 = BitmapFactory.decodeResource(getResources(),
            R.drawable.boing2);
    Bitmap upp = BitmapFactory.decodeResource(getResources(),
            R.drawable.oup);
    Bitmap hole = BitmapFactory.decodeResource(getResources(),
            R.drawable.hole);
    debutbits = new Bitmap[6];
    debutbits[2] = BitmapFactory.decodeResource(getResources(),
            R.drawable.flup);
    debutbits[3] = BitmapFactory.decodeResource(getResources(),
            R.drawable.flup2);
    debutbits[0] = Bitmap.createScaledBitmap(debutbits[2], dstWidth5,
            dstHeight5, true);
    debutbits[1] = Bitmap.createScaledBitmap(debutbits[3], dstWidth5,
            dstHeight5, true);
    debutbits[2].recycle();
    debutbits[3].recycle();
    debutbits[4] = BitmapFactory.decodeResource(getResources(),
            R.drawable.xinvert);
    debutbits[2] = Bitmap.createScaledBitmap(debutbits[4], dstWidth5,
            dstHeight5, true);
    debutbits[4].recycle();
    trou = Bitmap.createScaledBitmap(hole, dstWidth2, dstHeight2, true);
    paf2 = Bitmap.createScaledBitmap(boing, dstWidth2, dstHeight2, true);
    paf = Bitmap.createScaledBitmap(boing2, dstWidth2, dstHeight2, true);
    up = Bitmap.createScaledBitmap(upp, dstWidth2, dstHeight2, true);
    Bitmap leftt = rotimage(upp);
    left = Bitmap.createScaledBitmap(leftt, dstWidth2, dstHeight2, true);
    upp.recycle();
    leftt.recycle();
    boing.recycle();
    boing2.recycle();
    hole.recycle();

    BitmapFactory.Options opts = new BitmapFactory.Options();
    opts.inPreferredConfig = Bitmap.Config.RGB_565;
    Bitmap fondfond = BitmapFactory.decodeResource(getResources(),
            R.drawable.pat2, opts);
    int patw = fondfond.getWidth();
    int path = fondfond.getHeight();
    int ww = largeur;
    int wh = hauteur;
    Bitmap fond = Bitmap.createBitmap(ww, wh, Bitmap.Config.RGB_565);
    Canvas fdcanv = new Canvas(fond);
    for (int fnd = 0; fnd < ww; fnd += patw)
      for (int fndh = 0; fndh < wh; fndh += path)
        fdcanv.drawBitmap(fondfond, fnd, fndh, new Paint());
    fondfond.recycle();

    ColorMatrix couleurs;
    ColorMatrixColorFilter coul;
    Bitmap fond2 = Bitmap.createBitmap(ww, wh, Bitmap.Config.RGB_565);
    Canvas c = new Canvas(fond2);
    Paint paint = new Paint();
    couleurs = modifyback();
    coul = new ColorMatrixColorFilter(couleurs);
    paint.setColorFilter(coul);
    c.drawBitmap(fond, 0.0f, 0.0f, paint);

    fond.recycle();
    mWood = Bitmap.createScaledBitmap(fond2, ww, wh, true);
    fond2.recycle();
    canv = new Canvas(mWood);
    paint = new Paint();

    onsizechanged(ww, wh);
  }

  public void traitementimage(int ww, int wh) {
    Bitmap mamaoww;
    int w2=mamaow.getWidth();
    int h2=mamaow.getHeight();
    if(tourne==true){
      bby=0.0f;
      canv.drawBitmap(left,(float)(ww)/2.0f,(float)(wh)/2.0f,paint);
      for(int uu=0;uu<3;uu++){
        debutbits[uu]=rotimage(debutbits[uu]);
      }
    }
    else {
      bbx=0.0f;
      canv.drawBitmap(up,(float)(ww)/2.0f,(float)(wh)/2.0f,paint);
    }
    up.recycle();
    left.recycle();
    double ratiof=((double)(ww))/((double)(wh));
    double ratio2=((double)(w2))/((double)(h2));
    if(ratio2>ratiof){
      h2=(int)(Math.floor((double)(ww)/ratio2));
      w2=ww;
    }
    else {
      h2=wh;
      w2=(int)(Math.floor((double)(wh)*ratio2));
    }
    if(h2>wh)
      h2=wh;
    if(w2>ww)
      w2=ww;
    mamaoww = Bitmap.createScaledBitmap(mamaow,w2, h2, true);
    //mamaow.recycle();
    int decalh=(int)(Math.floor(Math.abs((double)(h2-wh)/2.0f)));
    int decalw=(int)(Math.floor(Math.abs((double)(w2-ww)/2.0f)));

    Bitmap fondimage= Bitmap.createBitmap(mWood);
    Canvas cvfondimage= new Canvas(fondimage);
    cvfondimage.drawBitmap(mamaoww, decalw, decalh, paint);
    //ppa.setAlpha(100);
    ppa.setAntiAlias(true);
    //ppa.setShadowLayer(1.0f, 0.001f, 0.001f, Color.BLACK);
    int troutaille;
    int troutailleh;
    int trouw=trou.getWidth()/2;
    int trouh=trou.getHeight()/2;
    int paftaille;
    int paftailleh;
    int pafw=paf.getWidth()/2;
    int pafh=paf.getHeight()/2;
    int piftaille;
    int piftailleh;
    int pifw=paf2.getWidth()/2;
    int pifh=paf2.getHeight()/2;
    for(int jk=0;jk<nombre;jk++){
      troutaille=pointxi[jk]-trouw;
      troutailleh=pointyi[jk]-trouh;
      cvfondimage.drawBitmap(trou, troutaille, troutailleh, ppa);
    }
    for(int jk=0;jk<nombrem;jk++){
      paftaille=murxi[jk]-pafw;
      paftailleh=muryi[jk]-pafh;
      cvfondimage.drawBitmap(paf, paftaille, paftailleh, ppa);
    }
    for(int jk=0;jk<nombrep;jk++){
      piftaille=murpxi[jk]-pifw;
      piftailleh=murpyi[jk]-pifh;
      cvfondimage.drawBitmap(paf2, piftaille, piftailleh, ppa);
    }
    double xxx1, yyy1, xxx2, yyy2, xxx3, yyy3;
    Paint pa = new Paint();
    pa.setColor(prefcouleur);
    pa.setAntiAlias(true);
    Paint ppp = new Paint();
    ppp.setAntiAlias(true);
    ppp.setColor(prefcouleur);
    ppp.setAlpha(prefalpha);
    double whm=mMetersToPixelsY;
    double wwm=mMetersToPixelsX;
    double wh2 = ((double) (h2)) / 2.0f +decalh;
    double ww2 = ((double) (w2)) / 2.0f +decalw;
    int jkjkjk=lili*coco;
    for (int jk = 0; jk < jkjkjk; jk++) {
      if (hvmur[jk][0] != 5000.0f) {
        xxx1 = hvmur[jk][0] * wwm +ww/2.0f;
        yyy1 = -hvmur[jk][1] * whm +wh/2.0f;
        xxx2 = hvmur[jk][2] * wwm +ww/2.0f;
        yyy2 = -hvmur[jk][3] * whm +wh/2.0f;
        canv.drawLine((float) xxx2, (float) yyy2,(float)  xxx1, (float) yyy1, pa);
        xxx1 = hvmur[jk][0] * wwm +ww2;
        yyy1 = -hvmur[jk][1] * whm +wh2;
        xxx2 = hvmur[jk][2] * wwm +ww2;
        yyy2 = -hvmur[jk][3] * whm +wh2;
        cvfondimage.drawLine((float) xxx2, (float) yyy2, (float) xxx1, (float) yyy1, ppp);
      }
      if (hvmur[jk][4] != 5000.0f) {
        xxx2 = hvmur[jk][2] * wwm +ww/2.0f;
        yyy2 = -hvmur[jk][3] * whm +wh/2.0f;
        xxx3 = hvmur[jk][4] * wwm +ww/2.0f;
        yyy3 = -hvmur[jk][5] * whm +wh/2.0f;
        canv.drawLine((float) xxx2, (float) yyy2, (float) xxx3, (float) yyy3, pa);
        xxx2 = hvmur[jk][2] * wwm +ww2;
        yyy2 = -hvmur[jk][3] * whm +wh2;
        xxx3 = hvmur[jk][4] * wwm +ww2;
        yyy3 = -hvmur[jk][5] * whm +wh2;
        cvfondimage.drawLine((float) xxx2, (float) yyy2, (float) xxx3, (float) yyy3, ppp);
      }
    }
    //mamaoww.recycle();
    mamaoww=Bitmap.createBitmap(fondimage, decalw, decalh, w2, h2);
    fondimage.recycle();

    int nw=supernw;
    int nh=supernh;
    int parta=nw*nh;
    int wt=(int)(Math.floor(((double)w2)/((double)(nw))));
    int ht=(int)(Math.floor(((double)h2)/((double)(nh))));
    //on créé les différentes parties découpées en paires
    parterre = new Bitmap[parta];
    pasterre = new Bitmap[parta];
    pose=new double[parta][2];

    //gaffe aux arrondis on dépasse pas les bornes
    while(nw*wt>w2 || nw*wt+decalw>ww)
      wt--;
    while(nh*ht>h2 || nh*ht+decalh>wh)
      ht--;

    int z1,z2,z3;
    for (int xn=0;xn<nw;xn++){
      for(int yn=0;yn<nh;yn++){
        z1=yn+xn*nh;
        z2=xn*wt;
        z3=yn*ht;
        parterre[z1]=Bitmap.createBitmap(mamaoww, z2, z3, wt, ht);
        z2+=decalw;
        z3+=decalh;
        pasterre[z1]=Bitmap.createBitmap(mWood, z2, z3, wt, ht);
        pose[z1][0]=(double)(z2);
        pose[z1][1]=(double)(z3);
      }
    }
    mamaoww.recycle();
    //nbre max de cases affichées par balle :
    int parts=(int)Math.floor(((double)parta)/((double)nombre));
    boolean[] estpris=new boolean[parta];
    for(int g=0;g<parta;g++)
      estpris[g]=false;
    int compteurpaspris=parta;
    int nbreaajouter=parta-nombre*parts;

    decoupage=new int[nombre][];

    for(int g=0;g<nombre-1;g++){
      int partss=parts;
      if(nbreaajouter>0){
        nbreaajouter--;
        partss++;
      }
      decoupage[g]=new int[partss];
      int j=(int)Math.floor(Math.random()*(double)parta);
      for(int h=0;h<partss;h++){
        while(estpris[j]==true){
          j++;
          if(j==parta)
            j=0;
        }
        estpris[j]=true;
        decoupage[g][h]=j;
        compteurpaspris--;
      }
    }
    decoupage[nombre-1]=new int[compteurpaspris];
    int j=(int)Math.floor(Math.random()*(double)parta);
    for(int h=0;h<compteurpaspris;h++){
      while(estpris[j]==true){
        j++;
        if(j==parta)
          j=0;
      }
      estpris[j]=true;
      decoupage[nombre-1][h]=j;
    }

    //canv.drawBitmap(parterre[nombre], pose[nombre][0], pose[nombre][1], paint);
  }

  public ColorMatrix modifyback() {
    ColorMatrix couleurs=new ColorMatrix();
    float[] colors= new float[]{
            1,0,0,0,0,//g0
            0,1,0,0,0,//g1
            0,0,1,0,0,//g2
            0,0,0,1,0 //g3
    };
    for (int u=0;u<3;u++){
      for (int g=0;g<3;g++){
        if(u==g)
          colors[u+g*5]=(float)(Math.random());
        else if (u==4)
          colors[u+g*5]=(float)(Math.random()*512.0-256.0);
      }
    }
    couleurs.set(colors);
    return couleurs;
  }

  public void onsizechanged(int w, int h) {
    // compute the origin of the screen relative to the origin of
    // the bitmap

    mXOrigin = (w - mBitmap.getWidth()) * 0.5f;
    mYOrigin = (h - mBitmap.getHeight()) * 0.5f;
    double mHoriz=w / mMetersToPixelsX;
    double mVert=h / mMetersToPixelsY;
    mHoriz2=mHoriz;
    mVert2=mVert;
    mHorizontalBound = ((mHoriz - sBallDiameter) * 0.5f);
    mVerticalBound = ((mVert - sBallDiameter) * 0.5f);

    colonnes=coco*2;
    lignes=lili*2;/*
        if(colonnes*lignes<nombre+nombrem+2){
        	colonnes=8;
        	lignes=16;
        }*/

    conversion = new int[lignes*colonnes];
    maxiconvers=lignes*colonnes;
    for(int jj=0;jj<maxiconvers;jj++){
      conversion[jj]=-1;
    }
    scatterize=new boolean[colonnes][lignes];//coordonnées occupées
    for (int ui=0;ui<colonnes;ui++)
      for(int io=0;io<lignes;io++)
        scatterize[ui][io]=false;//(ui+1)+carre*io;
    //coordonnées : x/10./2.
    //coordonnées : y/10./2.
    int maax=colonnes*lignes;
    double a,b;
    int limitem,limitec;
    double[] tempo=new double[2];
    murx=new double[maax];
    mury=new double[maax];
    murxi=new int[nombrem];
    muryi=new int[nombrem];
    avamur=new int[nombrem];
    int paftaille=paf.getWidth()/2;
    int paftailleh=paf.getHeight()/2;
    int troutaille=trou.getWidth()/2;
    int troutailleh=trou.getHeight()/2;
    limitem=1;//2
    limitec=1;//1
    int c,d,poos;

    for(int jk=0;jk<maax;jk++){
      murx[jk]=5000.0f;
      mury[jk]=5000.0f;
    }
    for(int jk=0;jk<nombrem;jk++){
      tempo=changecoordonnees2(lignes,colonnes,limitem,limitec);
      a=tempo[0];
      b=tempo[1];
      murxi[jk]=(int)(Math.floor(a*(double)(w)));
      muryi[jk]=(int)(Math.floor(b*(double)(h)));
      c=(int)Math.floor(a*(double)(colonnes));
      d=(int)Math.floor(b*(double)(lignes));
      poos=c+d*colonnes;
      avamur[jk]=poos;
      conversion[poos]=0;
      murx[poos]=((double)(a)-0.5f)*mHoriz;
      mury[poos]=-((double)(b)-0.5f)*mVert;
      canv.drawBitmap(paf, murxi[jk]-paftaille, muryi[jk]-paftailleh, paint);
    }

    murpx=new double[maax];
    murpy=new double[maax];
    murpxi=new int[nombrep];
    murpyi=new int[nombrep];
    avapif=new int[nombrep];
    int piftaille=paf2.getWidth()/2;
    int piftailleh=paf2.getHeight()/2;
    for(int jk=0;jk<maax;jk++){
      murpx[jk]=5000.0f;
      murpy[jk]=5000.0f;
    }
    for(int jk=0;jk<nombrep;jk++){
      tempo=changecoordonnees2(lignes,colonnes,limitem,limitec);
      a=tempo[0];
      b=tempo[1];
      murpxi[jk]=(int)(Math.floor(a*(double)(w)));
      murpyi[jk]=(int)(Math.floor(b*(double)(h)));
      c=(int)Math.floor(a*(double)(colonnes));
      d=(int)Math.floor(b*(double)(lignes));
      poos=c+d*colonnes;
      avapif[jk]=poos;
      conversion[poos]=2;
      murpx[poos]=((double)(a)-0.5f)*mHoriz;
      murpy[poos]=-((double)(b)-0.5f)*mVert;
      canv.drawBitmap(paf2, murpxi[jk]-piftaille, murpyi[jk]-piftailleh, paint);
    }

    pointx=new double[maax];
    pointy=new double[maax];
    for(int jk=0;jk<maax;jk++){
      pointx[jk]=5000.0f;
      pointy[jk]=5000.0f;
    }
    pointxi=new int[nombre];
    pointyi=new int[nombre];
    avatrou=new int[nombre];
    limitem=1;//2
    limitec=1;//1
    for(int jk=0;jk<nombre;jk++){
      tempo=changecoordonnees2(lignes,colonnes,limitem,limitec);
      a=tempo[0];
      b=tempo[1];
      pointxi[jk]=(int)(Math.floor(a*(double)w));
      pointyi[jk]=(int)(Math.floor(b*(double)h));
      c=(int)Math.floor(a*(double)(colonnes));
      d=(int)Math.floor(b*(double)(lignes));
      poos=c+d*colonnes;
      avatrou[jk]=poos;
      conversion[poos]=1;
      pointx[poos]=((double)(a)-0.5f)*mHoriz;
      pointy[poos]=-((double)(b)-0.5f)*mVert;
      canv.drawBitmap(trou, pointxi[jk]-troutaille, pointyi[jk]-troutailleh, paint);
    }

    int maximum=lili*coco;
    libre=new boolean[maximum];
    for(int j=0;j<maximum;j++)
      libre[j]=true;

    dessain();

    dessingrille2(lili,coco,w,h,mHoriz,mVert);

    if(nwaow>0){
      traitementimage(w,h);
    }
    else {
      bbx=0.0f;
    }

    paf.recycle();
    trou.recycle();
    paf2.recycle();

  }

  public double[] changecoordonnees2(int lign, int colonn, int limitem,
                                     int limitec) {
    double[] sortie=new double[2];
    //limitem = int depuis le bord
    //limitec = int depuis le centre
    double ligg1=Math.floor((double)(lign)/2.0-(double)(limitec));
    double coll1=Math.floor((double)(colonn)/2.0-(double)(limitec));
    double ligg2=Math.ceil((double)(lign)/2.0+(double)(limitec));
    double coll2=Math.ceil((double)(colonn)/2.0+(double)(limitec));
    int liggg1=(int)(ligg1);
    int liggg2=(int)(ligg2);
    int colll1=(int)(coll1);
    int colll2=(int)(coll2);
    boolean[][] scatto=new boolean[colonn][lign];
    for(int jkk=0;jkk<colonn;jkk++){
      for(int jkl=0;jkl<lign;jkl++){
        scatto[jkk][jkl]=scatterize[jkk][jkl];
      }
    }
    for(int jkk=0;jkk<colonn;jkk++){
      for(int jkl=0;jkl<lign;jkl++){
        if(jkl<limitem
                || jkl>lign-1-limitem
                || jkk<limitem
                || jkk>colonn-1-limitem)
          scatto[jkk][jkl]=true;
        else if((liggg1<=jkl && jkl<=liggg2)
                && (colll1<=jkk && jkk<=colll2))
          scatto[jkk][jkl]=true;
      }
    }
    int hj,hk;
    hj=(int)(Math.floor(Math.random()*(double)(colonn)));
    hk=(int)(Math.floor(Math.random()*(double)(lign)));
    while(scatto[hj][hk]==true){
      hj=(int)(Math.floor(Math.random()*(double)(colonn)));
      hk=(int)(Math.floor(Math.random()*(double)(lign)));
    }
    //colonnes ; lignes
    scatterize[hj][hk]=true;
    sortie[0]=(1.0/((double)(colonn)))*((double)(hj)+0.5);
    sortie[1]=(1.0/((double)(lign)))*((double)(hk)+0.5);
    return sortie;
  }

  public void dessingrille2(int li, int co, int w, int h, double mHoriz,
                            double mVert) {
    double lignes = (double) (li);
    double colonnes = (double) (co);
    double width = (double) (w);
    double height = (double) (h);
    double wdemi = mHoriz / 2.0f;
    double hdemi = mVert / 2.0f;
    double lx = (width / (colonnes));
    double ly = (height / (lignes));
    double lx0 = (mHoriz / (colonnes));
    double ly0 = (mVert / (lignes));
    int maximum = li * co;
    maximur = maximum;
    // indice = x+y*xmax

    double blok = 5000.0f;
    hvmur = new double[maximum][6];
    for (int u = 0; u < maximum; u++) {
      for (int v = 0; v < 6; v++)
        hvmur[u][v] = blok;
    }
    int xi, yi, xf, yf;
    double xid, yid, xfd, yfd;
    double xi0, yi0, xf0, yf0;
    int i, f;
    int mi=chemin.length - 1;
    Random rr= Gallery.rand;
    double bougex,bougey;
    for (int u = 0; u < mi; u++) {
      xi = chemin[u][0];
      yi = chemin[u][1];
      xf = chemin[u + 1][0];
      yf = chemin[u + 1][1];
      bougex=rr.nextFloat()*incertitude-incertitude/2.0f+1.0f;
      bougey=rr.nextFloat()*incertitude-incertitude/2.0f+1.0f;
      xi0 = ((double) (xi) + 0.5f) * lx0 - wdemi;
      yi0 = -((double) (yi) + 0.5f) * ly0 + hdemi;
      xf0 = ((double) (xf) + 0.5f) * lx0 - wdemi;
      yf0 = -((double) (yf) + 0.5f) * ly0 + hdemi;
      i = xi + yi * co;
      f = xf + yf * co;
      if (Math.abs(xf - xi) <= 1 && Math.abs(yf - yi) <= 1) {
        hvmur[i][2]=xi0*bougex;
        hvmur[i][3]=yi0*bougey;
        hvmur[i][4]=(xi0 + xf0) / 2.0f;
        hvmur[i][5]=(yi0 + yf0) / 2.0f;

        hvmur[f][0]=(xi0 + xf0) / 2.0f;
        hvmur[f][1]=(yi0 + yf0) / 2.0f;
        hvmur[f][2]=xf0;
        hvmur[f][3]=yf0;
      }
    }

  }

  public int[][] dessinchemin(int ligns, int colonns, int startx, int starty) {
    int maximum=ligns*colonns;
    int[][] chemint= new int[maximum][2];
    chemint[0][0]=startx;
    chemint[0][1]=starty;
    libre[startx+starty*colonns]=false;
    int stopstopval=-1;
    int x,y,nvalide,randomme,lax,lay;
    for(int u=0;u<maximum-1 && stopstopval==-1;u++){
      x=chemint[u][0];
      y=chemint[u][1];
      nvalide=scan(x,y,ligns,colonns);
      if(nvalide>0)
        randomme=r5.nextInt(nvalide);
      else {
        randomme=0;
        stopstopval=u+2;
      }
      int k=-1;
      boolean fini=false;
      for(int l=0;l<4;l++){
        if(validecoord[l]==true)
          k++;
        if(k==randomme && fini==false){
          lax=x+coord[l][0];
          lay=y+coord[l][1];
          chemint[u+1][0]=lax;
          chemint[u+1][1]=lay;
          libre[lax+lay*colonns]=false;
          fini=true;
        }
      }
    }
    int[][] chemintt= new int[stopstopval][2];
    for(int u=0;u<stopstopval;u++){
      chemintt[u][0]=chemint[u][0];
      chemintt[u][1]=chemint[u][1];
    }

    return chemintt;
  }

  public void dessain() {
    int[][] chemint;
    int cococo=coco;
    int lilili=lili;
    int mxm=cococo*lilili;
    int[][] chemintt=new int[mxm*2][2];
    boolean pasfini=true;
    int taillealafin=0;
    int mxmlim=(mxm*9)/10;
    int startx=0;
    int starty=0;
    while(pasfini==true){
      boolean librecase=false;
      while(librecase==false){
        startx=(int)(Math.floor(Math.random()*(double)(cococo)));
        starty=(int)(Math.floor(Math.random()*(double)(lilili)));
        int resuldessain=startx+starty*cococo;
        librecase=libre[resuldessain];
      }
      chemint = dessinchemin(lilili,cococo,startx,starty);
      int taille=chemint.length;
      for(int u=0;u<taille;u++){
        int v=taillealafin+u;
        if(v<mxm){
          chemintt[v][0]=chemint[u][0];
          chemintt[v][1]=chemint[u][1];
        }
      }

      taillealafin+=taille;

      if(taillealafin>=mxmlim)
        pasfini=false;
    }

    //llog.d("node","mxmlim="+mxmlim+" < taillealafin="+taillealafin+" < mxm=lili*coco="+mxm);

    chemin=new int[taillealafin][2];
    for(int u=0;u<taillealafin;u++){
      chemin[u][0]=chemintt[u][0];
      chemin[u][1]=chemintt[u][1];
    }

  }

  public int scan(int x, int y, int ligns, int colonns) {
    int nvalide=0;
    int[] pondere=new int[]{0,0,0,0};
    boolean[] libreapres=new boolean[]{false,false,false,false};
    validecoord=new boolean[]{false,false,false,false};
    int nblibres=-1;
    int min=5;
    int max=-5;
    for(int l=0;l<4;l++){
      int lax=x+coord[l][0];
      int lay=y+coord[l][1];
      if((0<=lax && lax<colonns)
              &&(0<=lay && lay<ligns)){
        if(libre[lax+lay*colonns]==true){
          nblibres=nscan(lax,lay,ligns,colonns);
          pondere[l]=nblibres;
          if(nblibres==0)
            validecoord[l]=true;
          else {
            if(nblibres<min)
              min=nblibres;
            if(nblibres>max)
              max=nblibres;
          }
        }
      }
      int lax2=lax+coord[l][0];
      int lay2=lay+coord[l][1];
      int lax3=lax+coord[l][0]+coord[l][0];
      int lay3=lay+coord[l][1]+coord[l][1];
      if((0<=lax2 && lax2<colonns)
              &&(0<=lay2 && lay2<ligns)
              &&(0<=lax3 && lax3<colonns)
              &&(0<=lay3 && lay3<ligns)){
        if(libre[lax2+lay2*colonns]==true
                && libre[lax3+lay3*colonns]==true){
          libreapres[l]=true;
        }
      }
    }
    if(nblibres>=0){
      for(int l=0;l<4;l++){
        if(pondere[l]==min){
          validecoord[l]=libreapres[l];
          if(validecoord[l]==true)
            nvalide++;
        }
      }
      if(nvalide==0){
        for(int l=0;l<4;l++){
          if(pondere[l]==min){
            validecoord[l]=true;
            nvalide++;
          }
        }
        if(nvalide==0){
          for(int l=0;l<4;l++){
            if(pondere[l]>min){
              validecoord[l]=libreapres[l];
              if(validecoord[l]==true)
                nvalide++;
            }
          }
          if(nvalide==0){
            for(int l=0;l<4;l++){
              if(pondere[l]>min){
                validecoord[l]=true;
                nvalide++;
              }
            }
          }
        }
      }
    }
    return nvalide;
  }

  public int nscan(int x, int y, int ligns, int colonns) {
    int nvalide=0;
    for(int l=0;l<4;l++){
      int lax=x+coord[l][0];
      int lay=y+coord[l][1];
      if((0<=lax && lax<colonns)
              && (0<=lay && lay<ligns)){
        if(libre[lax+lay*colonns]==true){
          nvalide++;
        }
      }
    }
    return nvalide;
  }

  class SimulationView extends View {
    private ParticleSystem mParticleSystem;
    private Particle courante;

    final double xcp = largeur / 2;
    final double ycp = hauteur / 2;
    final double xc = mXOrigin;
    final double yc = mYOrigin;
    final double xs = mMetersToPixelsX;
    final double ys = mMetersToPixelsY;
    final Bitmap[] bitmap;
    private Paint nopainttext;
    private final int taille=25;

    double x,y;

    public SimulationView(Context context) {
      super(context);
      mParticleSystem = new ParticleSystem();
      int olwi=mBitmap.getWidth();
      int olhe=mBitmap.getHeight();
      bitmap=new Bitmap[nombre];
      for(int o=0;o<nombre;o++){
        Particle curr=mBalls[o];
        bitmap[o]=Bitmap.createBitmap(olwi,olhe, Bitmap.Config.ARGB_8888);
        Canvas savna=new Canvas(bitmap[o]);
        savna.drawBitmap(mBitmap, 0.0f, 0.0f, curr.couleuvre);
      }
      mBitmap.recycle();
      nopainttext=new Paint();
      nopainttext.setTextAlign(Paint.Align.CENTER);
      nopainttext.setColor(prefcouleur);
      nopainttext.setTextSize(taille);
      nopainttext.setShadowLayer(4.0f, 1.0f, 1.0f, Color.BLACK);
      nopainttext.setTypeface(Typeface.MONOSPACE);
      nopainttext.setAntiAlias(true);


      isrunning=true;

      new Thread(new Runnable(){
        public void run(){
          Bitmap mWood3 = Bitmap.createBitmap(mWood);
          double ecart = (double) (debutbits[0].getWidth()) * 0.5f;
          double recul = ycp / 2.0f;
          double xdd = xcp - ecart;
          double ydd = ycp - ecart;
          int tempsp = 6;
          int tempsd = 0;

          while(tempsd<tempsp && isrunning){
            try {
              Thread.sleep(600);
            } catch (InterruptedException e) {
              // TODO Auto-generated catch block
              e.printStackTrace();
            }
            switch(tempsd){
              case 0 :
                canv.drawBitmap(debutbits[0], (float) xdd, (float) (ydd+recul), null);
                break;
              case 1 :
                canv.drawBitmap(debutbits[1], (float) xdd, (float) (ydd+recul), null);
                break;
              case 2 :
                canv.drawBitmap(debutbits[0],(float)  xdd, (float) ydd, null);
                break;
              case 3 :
                canv.drawBitmap(debutbits[0],(float)  xdd, (float) (ydd+recul), null);
                canv.drawBitmap(debutbits[1],(float)  xdd, (float) ydd, null);
                break;
              case 4 :
                canv.drawBitmap(debutbits[0], (float) xdd, (float) (ydd-recul), null);
                break;
              case 5 :
                canv.drawBitmap(debutbits[0],(float)  xdd, (float) ydd, null);
                canv.drawBitmap(debutbits[1], (float) xdd, (float) (ydd-recul), null);
                break;
              default :
                break;
            }
            tempsd++;
            demardiv/=2.0f;
          }
          canv.drawBitmap(mWood3, 0.0f, 0.0f, null);
          mWood3.recycle();
          if(isrunning==true)
            isreallyrunning=true;
        }
      }).start();

      surfaceCreated();
    }

    public void drawblack(Canvas c){
      c.drawColor(Color.BLACK);
    }

    public void drawinitial(Canvas c){
      c.drawBitmap(mWood,0.0f,0.0f,null);
      for(int o=0;o<nombre;o++){
        courante=mBalls[o];//
        x = xc + courante.mPosX * xs;
        y = yc - courante.mPosY * ys;
        c.drawBitmap(bitmap[o], (float) x, (float) y, null);
      }
      if(tourne)
        c.drawText("Please turn screen ---->",largeur/2.0f,taille,nopainttext);
      c.drawText("Menu : Block balls",largeur/2.0f,2.0f*taille,nopainttext);
      c.drawText("Search : Set inclination",largeur/2.0f,3.0f*taille,nopainttext);
      c.drawText("Touch screen to bump ball",largeur/2.0f,4.0f*taille,nopainttext);
    }

    public void drawupdate(Canvas c){
      c.drawBitmap(mWood,0.0f,0.0f,null);
      for(int o=0;o<nombre;o++){
        courante=mBalls[o];//
        x = xc + courante.mPosX * xs;
        y = yc - courante.mPosY * ys;
        c.drawBitmap(bitmap[o], (float) x, (float) y, null);
      }
      if(evenement)
        c.drawBitmap(debutbits[2], (float) blocktimexy[0], (float) blocktimexy[1], null);
    }

    public void surfaceChanged(SurfaceHolder arg0, int arg1, int arg2,
                               int arg3) {

    }

    public void surfaceCreated() {
      mParticleSystem.setRunning(true);
      mParticleSystem.start();
    }

    public void surfaceDestroyed() {
      boolean retry1 = true;
      mParticleSystem.setRunning(false);
      while (retry1) {
        try  {
          mParticleSystem.join();
          retry1 = false;
        } catch (InterruptedException e) {}
      }
      for(int uu=0;uu<bitmap.length;uu++)
        bitmap[uu].recycle();
      mWood.recycle();
      debutbits[0].recycle();
      debutbits[1].recycle();
      debutbits[2].recycle();
      if(nwaow>0) {
        for (int kl=0;kl<parterre.length;kl++)
          parterre[kl].recycle();
        for (int kl=0;kl<pasterre.length;kl++)
          pasterre[kl].recycle();
      }
    }


    @Override
    public void onDraw(Canvas c){
      if(isrunning && !isreallyrunning){
        drawinitial(c);
      } else if (isrunning && isreallyrunning) {
        if (nwaow > 0) {
          for (int ev = 0; ev < nombre; ev++) {
            int tnat=decoupage[ev].length;
            if (evene[ev][1] == true) {
              for (int t = 0; t < tnat; t++) {
                int fafa = decoupage[ev][t];
                canv.drawBitmap(parterre[fafa], (float) pose[fafa][0],
                        (float) pose[fafa][1], null);
              }
              evene[ev][1] = false;
            }
            if (evene[ev][0] == true) {
              for (int t = 0; t < tnat; t++) {
                int fafa = decoupage[ev][t];
                canv.drawBitmap(pasterre[fafa], (float) pose[fafa][0],
                        (float) pose[fafa][1], null);
              }
              evene[ev][0] = false;
            }
          }
        }
        drawupdate(c);/*
				nbtemps++;
				long tempsla=System.nanoTime();
				if(nbtemps>1)
					tempsici+=tempsla-tempssauve;
				tempssauve=System.nanoTime();*/
      }else if (!isrunning && !isreallyrunning) {
        drawblack(c);/*
				double moyenne=((double)(nbtemps-1))/(((double)tempsici)/1000000000.0f);
				llog.d("node",""+moyenne);*/
        surfaceDestroyed();
      }
      invalidate();
    }
  }

  class ParticleSystem extends Thread implements SensorEventListener {
    private boolean _run = false;
    private Sensor mAccelerometer;
    double ecart;
    double xdd;
    double ydd;
    double increment;
    int cadureunpeu;
    double calala;
    double calala2;
    double xc;
    double yc;
    double xs;
    double ys;
    int ctdwn=0;

    public ParticleSystem() {
      mAccelerometer = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
      llog.d(TAG, "ParticleSystem getDefaultSensor");
    }

    public void startSimulation() {
      /*
       * It is not necessary to get accelerometer events at a very high
       * rate, by using a slower rate (SENSOR_DELAY_UI), we get an
       * automatic low-pass filter, which "extracts" the gravity component
       * of the acceleration. As an added benefit, we use less power and
       * CPU resources.
       */
      mSensorManager.registerListener(this, mAccelerometer, SensorManager.SENSOR_DELAY_UI);
      llog.d(TAG, "startSimulation registerListener");

    }

    public void stopSimulation() {
      mSensorManager.unregisterListener(this);
      llog.d(TAG, "stopSimulation unregisterListener");
    }

    public void setRunning(boolean run) {
      _run = run;
    }

    public void placedepart(){
      double sballdia=sBallDiameter*1.01f;
      Particle ball=mBalls[0];
      ball.mPosX=0.0f;
      ball.mPosY=0.0f;
      double[] repre={0.0f,0.0f};
      int N=nombre;
      int Ne=1;
      while (N>0){
        N=N-Ne;
        Ne++;
      }
      Ne--;
      repre[1]+=sballdia* (double) Math.sqrt(3.0f)/2.0f;
      repre[0]-=sballdia/2.0f;
      //Ne = nombre d'étages !
      int etage=1;
      int nbdsetagemax=etage+1;
      int nbdsetage=0;
      for(int o=1;o<nombre;o++){
        ball=mBalls[o];
        nbdsetage++;
        ball.mPosX=repre[0]+(nbdsetage-1)*sballdia;
        ball.mPosY=repre[1];

        if(nbdsetage==nbdsetagemax){
          etage++;
          nbdsetagemax=etage+1;
          nbdsetage=0;
          repre[1]+=sballdia* (double) Math.sqrt(3.0f)/2.0f;
          repre[0]-=sballdia/2.0f;
        }
      }
    }

    public void multiArrayCopy(double[][] source,double[][] destination){
      for (int a=0;a<source.length;a++){
        System.arraycopy(source[a],0,destination[a],0,source[a].length);
      }
    }

    @Override
    public void run(){

      while(!isrunning){
        try {
          Thread.sleep(400);
        } catch (InterruptedException e) {
          // TODO Auto-generated catch block
          e.printStackTrace();
        }
      }

      startSimulation();

      placedepart();

      while(isrunning && !isreallyrunning){
        try {
          Thread.sleep(tempscalcul);
        } catch (InterruptedException e) {
          // TODO Auto-generated catch block
          e.printStackTrace();
        }

        //calculs physiques
        updatePositions2();
        //chocs interbilles et bords
        update();

      }

      xc = mXOrigin;
      yc = mYOrigin;
      xs = mMetersToPixelsX;
      ys = mMetersToPixelsY;
      ecart=(double)(debutbits[2].getWidth())*0.5f;
      xdd= mXOrigin-ecart;
      ydd= mYOrigin-ecart;
      increment=xdd/glurp;
      blocktimexy[0]=xdd;
      blocktimexy[1]=ydd;
      cadureunpeu=0;
      calala=0.0f;
      calala2=0.0f;


      while(isreallyrunning){
        try {
          Thread.sleep(tempscalcul);
        } catch (InterruptedException e) {
          // TODO Auto-generated catch block
          e.printStackTrace();
        }

        //calculs physiques
        updatePositions2();
        //chocs interbilles et bords
        update();
        //chocs avec les murs
        verifmurs2();
        //chocs avec les pièges
        pieges();
        //on ejecte une balle
        //ejection();
      }//while toutes les 15ms


      stopSimulation();
      onStop();
    }

    public void onAccuracyChanged(Sensor sensor, int accuracy) {
      // TODO Auto-generated method stub
    }

    public void onSensorChanged(SensorEvent event) {
      if (event.sensor.getType() != Sensor.TYPE_ACCELEROMETER)
        return;
      /*
       * record the accelerometer data, the event's timestamp as well as
       * the current time. The latter is needed so we can calculate the
       * "present" time during rendering. In this application, we need to
       * take into account how the screen is rotated with respect to the
       * sensors (which always return data in a coordinate space aligned
       * to with the screen in its native orientation).
       */
		            /*
		            switch (mDisplay.getRotation()) {
		                case Surface.ROTATION_0:
		                    //mSensorX = event.values[0]*ratioevent+bbx;//event.values[0]*a;
		                    //mSensorY = event.values[1]*ratioevent+bby;//event.values[1]*a;
		                    break;
		                case Surface.ROTATION_90:
		                    llog.d("node","rot "+90);
		                    //mSensorX = -event.values[1]*ratioevent;//-event.values[1]*a;
		                    //mSensorY = event.values[0]*ratioevent;//event.values[0]*a;
		                    break;
		                case Surface.ROTATION_180:
		                    llog.d("node","rot "+180);
		                    //mSensorX = -event.values[0]*ratioevent;//-event.values[0]*a;
		                   // mSensorY = -event.values[1]*ratioevent;//-event.values[1]*a;
		                    break;
		                case Surface.ROTATION_270:
		                    llog.d("node","rot "+270);
		                   // mSensorX = event.values[1]*ratioevent;//event.values[1]*a;
		                   // mSensorY = -event.values[0]*ratioevent;//-event.values[0]*a;
		                    break;
		            }*/


      msx= event.values[0];
      msy= event.values[1];
      if(bloquelikethis==false){
        mSensorX = msx+bbx;
        mSensorY = msy+bby;
      }
    }

    private void updatePositions2(){
      for(int o=0;o<nombre;o++){
        final double sx = mSensorX;
        final double sy = mSensorY;
        Particle mball = mBalls[o];
        mball.computePhysics2(sx, sy);
      }
    }

    private void pieges(){

      int nbreplaces=nombre;
      for (int i = 0; i<nombre  && isreallyrunning; i++) {

        Particle courante=mBalls[i];
        double xf=courante.mPosX;
        double yf=courante.mPosY;
        boolean plassee=false;
        final double accx=courante.mAccelX;
        final double accy=courante.mAccelY;
        double vivx=courante.mVitX;
        double vivy=courante.mVitY;


        int laxi,layi,jk;
        double laxf,layf;
        laxf=((xf+mHoriz2*0.5f)*((double)colonnes))/mHoriz2;
        laxi=(int)( (double) Math.floor(laxf));
        layf=((yf+mVert2*0.5f)*((double)lignes))/mVert2;
        layi=lignes-1-(int)( (double) Math.floor(layf));
        jk=laxi+layi*colonnes;

        if(0<=jk && jk<maxiconvers){
          double mur = attir;
          double o,p;
          int converse=conversion[jk];
          if(converse==1){//1 : endroit où placer bille
            o=pointx[jk];
            if(o!=5000.0f){
              p=pointy[jk];
              final double erx=o-xf;
              final double ery=p-yf;
              double erx2=erx*erx;
              double ery2=ery*ery;
              double er2=erx2+ery2;
              double er= (double) Math.sqrt(er2);
              double prealpha=courante.mAng;
              double alpha=0.0f;
              if(er<mur){
                courante.moneminus=limaccel;
                double dvex,dvey;
                if(er>0.0001f){
                  alpha=ballz.atanderivgauss(-1.0f,er,mur);
                  double alphagood=(alpha+prealpha)/2.0f;
                  double cste=bums* (double) Math.sin(alphagood);
                  dvex=(erx*cste)/er;
                  dvey=(ery*cste)/er;/*
						  					double vitreelle=FloatMath.sqrt(accx*accx+accy*accy);
						  					double dvitreelle=FloatMath.sqrt(dvex*dvex+dvey*dvey);
						  					String ajoutons=xf+"\t"+yf+"\t"+accx+"\t"+accy+"\t185AA826\t\t"+
						  					courante.mLastT+"\t"+vitreelle+"\t"+dvitreelle+"\r\n";
						  					eralpha.add(ajoutons);*/
                  courante.mdVitX += dvex;
                  courante.mdVitY += dvey;
                }
                nbreplaces--;
                plassee = true;
				  			  			/*
										if (Math.abs(accx) < 0.3f
												&& Math.abs(accy) < 0.3f) {
											nbreplaces--;
											plassee = true;
											xf = o;
											yf = p;
											vivx = 0.0f;
											vivy = 0.0f;
										}*/
              }
              courante.mOlAng=prealpha;
              courante.mAng=alpha;
		  			  				/*
          			                if(Math.abs(xf-o)<mur && Math.abs(yf-p)<mur){
          			                	if(Math.abs(accx)<limaccel && Math.abs(accy)<limaccel){
          			                		nbreplaces--;
          			                		plassee=true;
          					                xf=o;
          					                yf=p;
	              		                	vivx=0.0f;
	          					            vivy=0.0f;
          			                	}*/
          			                	/*
          			                	else {
          		          	            	double[] vectf=replassagecoinapres(xf,yf,o,p,vivx,vivy);
          		      	                	xf=vectf[0];
          		      	                	yf=vectf[1];
          			                	}*/

            }
          }
          else if(converse==0){//0 : teleportage
            int cluila=-1;
            mur=multiple;
            o=murx[jk];
            if(o!=5000.0f){
              p=mury[jk];
              if(Math.abs(xf-o)<mur && Math.abs(yf-p)<mur){
                double macx=Math.signum(vivx);
                double macy=Math.signum(vivy);
                cluila = (int)(Math.floor(Math.random()*(double)(nombrem)));
                xf=murx[avamur[cluila]]+macx*mur;
                yf=mury[avamur[cluila]]+macy*mur;
              }
            }
          }
          else if(converse==2){//2 : croix
            int cluila=-1;
            mur=multiple;
            o=murpx[jk];
            if(o!=5000.0f){
              p=murpy[jk];
              if(Math.abs(xf-o)<mur && Math.abs(yf-p)<mur){
                cadureunpeu++;
                if(cadureunpeu==1){
                  blocktimexy[0]=xdd;
                  blocktimexy[1]=ydd;
                  double aupif=(Math.floor(Math.random()*3.0)/10.0)-0.1;
                  calala=(double)(aupif)*mulipli;
                  aupif=(Math.floor(Math.random()*3.0)/10.0)-0.1;
                  calala2=(double)(aupif)*mulipli;
                  calala=calala/glurp;
                  calala2=calala2/glurp;
                  double macx=Math.signum(vivx);
                  double macy=Math.signum(vivy);
                  cluila = (int)(Math.floor(Math.random()*(double)(nombrem)));
                  xf=murx[avamur[cluila]]+macx*mur;
                  yf=mury[avamur[cluila]]+macy*mur;
                  scorr2++;
                }
              }
            }
          }
        }
        courante.mPosX=xf;
        courante.mPosY=yf;
        courante.mVitX=vivx;
        courante.mVitY=vivy;
        if(nwaow>0){
          if(courante.plassee!=plassee){
            if (plassee){
              evene[i][1]=true;
            }
            else{
              evene[i][0]=true;
              scorr++;
            }
          }
        }
        courante.plassee=plassee;
        if(!plassee){
          ctdwn=0;
        }
      }//for boucle sur chaque particule

      if(cadureunpeu>0){
        if(cadureunpeu<(int)glurp){
          bbx+=calala;
          bby+=calala2;
          blocktimexy[0]-=increment*Math.signum(calala);
          blocktimexy[1]+=increment*Math.signum(calala2);
          evenement=true;
          cadureunpeu++;
        }
        else {
          cadureunpeu=0;
          evenement=false;
        }
      }

      if(nbreplaces==0){
        ctdwn++;
        if(ctdwn==500){
          ctdwn=0;
          cestfini();
        }
      }
    }

    private void ejection(){
      if(touche[0]>=0.0f && isreallyrunning){
        double[] shoot={999.0f,-1.0f};
        for (int i = 0; i<nombre  && isreallyrunning; i++) {
          Particle courante=mBalls[i];
          boolean plassee=courante.plassee;
          if(plassee==false){
            double xf=courante.mPosX;
            double yf=courante.mPosY;

            double dx = (touche[0]-xc)/xs - xf;
            double dy = (yc-touche[1])/ys - yf;
            double dd = dx * dx + dy * dy;
            if(dd<shoot[0]){
              shoot[0]=dd;
              shoot[1]=(double)i;
            }
          }
        }
        Particle courante=mBalls[(int)shoot[1]];
        touche[0]=-1.0f;
        touche[1]=-1.0f;
        courante.mAccelX*=10.0f;
        courante.mAccelY*=10.0f;
      }
    }

    public void update() {
      final int count = nombre;
      for (int i = 0; i < count; i++) {
        Particle curr = mBalls[i];
        for (int j = i+1 ; j < count; j++) {
          Particle ball = mBalls[j];
          double x1 = ball.mPosX;
          double y1 = ball.mPosY;
          double x2 = curr.mPosX;
          double y2 = curr.mPosY;
          double alpha1 = ball.mAng;
          double alpha2 = curr.mAng;
          double vudehaut= (double) Math.cos((alpha1+alpha2)/2.0f);
          vudehaut*=vudehaut;
          double dx = x1-x2;
          double dy = y1-y2;
          double d2 = dx*dx+dy*dy;
          if (d2 < vudehaut*sBallDiameter2) {
            double v1x = ball.mVitX;
            double v1y = ball.mVitY;
            double v2x = curr.mVitX;
            double v2y = curr.mVitY;
            double a1x = ball.mAccelX;
            double a1y = ball.mAccelY;
            double a2x = curr.mAccelX;
            double a2y = curr.mAccelY;
            double f1 = ball.mLastoneminus*10.0f*demardiv;
            double f2 = curr.mLastoneminus*10.0f*demardiv;
            double[] vectp = ballz.collision(sBallDiameter,
                    false, alpha1, f1, x1, y1, v1x, v1y, a1x, a1y,
                    false, alpha2, f2, x2, y2, v2x, v2y, a2x, a2y);
            if(vectp!=null){
              ball.mPosX = vectp[0];
              ball.mPosY = vectp[1];
              curr.mPosX = vectp[2];
              curr.mPosY = vectp[3];
              ball.mVitX = vectp[4];
              ball.mVitY = vectp[5];
              curr.mVitX = vectp[6];
              curr.mVitY = vectp[7];
            }
		    						/*
		                            double v1x = ball.mVitX;
		                            double v1y = ball.mVitY;
		                            double v2x = curr.mVitX;
		                            double v2y = curr.mVitY;
		                            double[] vectp=replassage(x1,y1,x2,y2,v1x,v1y,v2x,v2y);
			            				ball.mPosX=vectp[0];
			            				ball.mPosY=vectp[1];
			            				curr.mPosX=vectp[2];
			            				curr.mPosY=vectp[3];
		            				double[] vect=collision(x1,y1,x2,y2,v1x,v1y,v2x,v2y);
		    						boolean autrechose=true;
		    						if(ball.plassee){
		    							if(Math.abs(vect[0])<limaccel && Math.abs(vect[1])<limaccel){
		    								vect=collisioncoin(x2,y2,x1,y1,v2x,v2y);
		    								curr.mVitX = vect[0];
		    								curr.mVitY = vect[1];
		    								autrechose=false;
		    							}
		    						}else if(curr.plassee){
		    							if(Math.abs(vect[2])<limaccel && Math.abs(vect[3])<limaccel){
		    								vect=collisioncoin(x1,y1,x2,y2,v1x,v1y);
		    								ball.mVitX = vect[0];
		    								ball.mVitY = vect[1];
		    								autrechose=false;
		    							}
		    						}
		    						if(autrechose){
		    							ball.mVitX = vect[0];
		    							ball.mVitY = vect[1];
		    							curr.mVitX = vect[2];
		    							curr.mVitY = vect[3];
		    						}*/
          }
        }
        curr.resolveCollisionWithBounds();
      }

    }

    public double[] ecartement(double x1, double y1, double x2, double y2,boolean moitie){
      double[] vect=new double[4];
      double dx=x1-x2;
      double dy=y1-y2;
      double dx2=dx*dx;
      double dy2=dy*dy;
      double racine= (double) Math.sqrt(dx2+dy2);
      double deltaa;
      if(moitie)
        deltaa=sBallDiameter/2.0f-racine;
      else
        deltaa=sBallDiameter-racine;
      double deltax,deltay;
      if(Math.abs(dx)<0.000000001f){
        deltax=0.0f;
        deltay=deltaa;
      }else{
        double penta=dy/dx;
        double deltax2=(deltaa*deltaa)/(1.0f+penta*penta);
        deltax= (double) Math.sqrt(deltax2);
        deltay=Math.abs(penta*deltax);
        deltax+=0.000000002f;
      }
      deltax/=2.0f;
      deltay/=2.0f;
      deltay+=0.000000001f;
      if(x1>x2){
        x1+=deltax;
        x2-=deltax;
      }else{
        x1-=deltax;
        x2+=deltax;
      }
      if(y1>y2){
        y1+=deltay;
        y2-=deltay;
      }else{
        y1-=deltay;
        y2+=deltay;
      }
      vect[0]=x1;
      vect[1]=y1;
      vect[2]=x2;
      vect[3]=y2;
      return vect;
    }

    public double[] replassage(double x1, double y1, double x2, double y2,
                              double v1x, double v1y, double v2x, double v2y){
      double[] vect=new double[4];
      double dx=x1-x2;
      double dy=y1-y2;
      double dvx=v1x-v2x;
      double dvy=v1y-v2y;
      double dx2=dx*dx;
      double dy2=dy*dy;
      double dvx2=dvx*dvx;
      double dvy2=dvy*dvy;

      double dt1=0.0f;
      double dt2=0.0f;
      double a=dvx2+dvy2;

      if(a<0.0001f){
        return ecartement(x1,y1,x2,y2,false);
      }

      double b=2.0f*(dvx*dx+dvy*dy);
      double c=dx2+dy2-(sBallDiameter2+0.00000001f);
      double deltaz=b*b-4.0f*a*c;
      if(deltaz==0.0f){
        dt1=-b/(2.0f*a);
        dt2=dt1;
      }else if (deltaz>0.0f){
        dt1=(-b- (double) Math.sqrt(deltaz))/(2.0f*a);
        dt2=(-b+ (double) Math.sqrt(deltaz))/(2.0f*a);
      }
      double dt=0.0f;
      if(dt1<0.0f){
        dt=dt1;
      }else if(dt2<0.0f){
        dt=dt2;
      }
      x1+=v1x*dt;
      y1+=v1y*dt;
      x2+=v2x*dt;
      y2+=v2y*dt;
      vect[0]=x1;
      vect[1]=y1;
      vect[2]=x2;
      vect[3]=y2;

      return vect;
    }

    public double[] collision(double x1, double y1, double x2, double y2,
                             double vx1, double vy1, double vx2, double vy2)     {

      double dvx2,a,x21,y21,vx21,vy21,fy21,sign;

      x21=x2-x1;
      y21=y2-y1;
      vx21=vx2-vx1;
      vy21=vy2-vy1;

      if(Math.abs(x21)<0.00000001f){
        if(x21>=0)
          x21=0.00000001f;
        else
          x21=-0.00000001f;
      }

      a=y21/x21;
      dvx2= -(vx21 +a*vy21)/(1.0f+a*a) ;
      double[] helpme=new double[4];
      helpme[0]=vx1-dvx2;
      helpme[1]=vy1-a*dvx2;
      helpme[2]=vx2+dvx2;
      helpme[3]=vy2+a*dvx2;

      return helpme;
    }

    public void verifmurs2() {
      final int count = nombre;
      final double demil = sBallDiameter / 2.0f;
      for (int i = 0; i < count; i++) {
        Particle courante = mBalls[i];
        double xf = courante.mPosX;
        double yf = courante.mPosY;
        double laxf = ((xf + mHoriz2 * 0.5f) * ((double) coco)) / mHoriz2;
        int laxi = (int) ( (double) Math.floor(laxf));
        double layf = ((yf + mVert2 * 0.5f) * ((double) lili)) / mVert2;
        int layi = lili - 1 - (int) ( (double) Math.floor(layf));
        int jk = laxi + layi * coco;
        if (0 <= jk && jk < maximur) {
          double ox1 = hvmur[jk][0];
          double oy1 = hvmur[jk][1];
          double ox2 = hvmur[jk][2];
          double oy2 = hvmur[jk][3];
          double ox3 = hvmur[jk][4];
          double oy3 = hvmur[jk][5];
          double v1x = courante.mVitX;
          double v1y = courante.mVitY;
          boolean done=false;
          if (ox1 != 5000.0f && ox2!=5000.0f) {
            double[] p2=ballz.hifisinrange(ox1,oy1,ox2,oy2,xf,yf,demil);
            if(p2!=null){
              double[] vectp = ballz.replassage(demil,
                      false, xf, yf, v1x, v1y,
                      true, p2[0], p2[1], 0.0, 0.0);
              if(vectp!=null){
                courante.mPosX = (double)vectp[0];
                courante.mPosY = (double)vectp[1];
                courante.mVitX = (double)vectp[4];
                courante.mVitY = (double)vectp[5];
                done=true;
              }
            }
          }
          if (ox2 != 5000.0f && ox3 != 5000.0f && !done) {
            double[] p2=ballz.hifisinrange(ox2,oy2,ox3,oy3,xf,yf,demil);
            if(p2!=null){
              double[] vectp = ballz.replassage(demil,
                      false, xf, yf, v1x, v1y,
                      true, p2[0], p2[1], 0.0, 0.0);
              if(vectp!=null){
                courante.mPosX = (double)vectp[0];
                courante.mPosY = (double)vectp[1];
                courante.mVitX = (double)vectp[4];
                courante.mVitY = (double)vectp[5];
                done=true;
              }
            }
          }
          if(ox1!=5000.0f && !done){
            double px1=xf-ox1;
            double py1=yf-oy1;
            double p12=px1*px1+py1*py1;
            double demil2=demil*demil;
            if(p12<=demil2){
              double[] vectp = ballz.replassage(demil,
                      false, xf, yf, v1x, v1y,
                      true, ox1, oy1, 0.0, 0.0);
              if(vectp!=null){
                courante.mPosX = (double)vectp[0];
                courante.mPosY = (double)vectp[1];
                courante.mVitX = (double)vectp[4];
                courante.mVitY = (double)vectp[5];
                done=true;
              }
            }
          }
          if(ox2!=5000.0f && !done){
            double px1=xf-ox2;
            double py1=yf-oy2;
            double p12=px1*px1+py1*py1;
            double demil2=demil*demil;
            if(p12<=demil2){
              double[] vectp = ballz.replassage(demil,
                      false, xf, yf, v1x, v1y,
                      true, ox2, oy2, 0.0, 0.0);
              if(vectp!=null){
                courante.mPosX = (double)vectp[0];
                courante.mPosY = (double)vectp[1];
                courante.mVitX = (double)vectp[4];
                courante.mVitY = (double)vectp[5];
                done=true;
              }
            }
          }
          if(ox3!=5000.0f && !done){
            double px1=xf-ox3;
            double py1=yf-oy3;
            double p12=px1*px1+py1*py1;
            double demil2=demil*demil;
            if(p12<=demil2){
              double[] vectp = ballz.replassage(demil,
                      false, xf, yf, v1x, v1y,
                      true, ox3, oy3, 0.0, 0.0);
              if(vectp!=null){
                courante.mPosX = (double)vectp[0];
                courante.mPosY = (double)vectp[1];
                courante.mVitX = (double)vectp[4];
                courante.mVitY = (double)vectp[5];
                done=true;
              }
            }
          }
        }
      }
    }
		        /*
		        public void verifmurs(){
		            final int count = nombre;
		            //final double espc=0.00001f;
		            final double demil=sBallDiameter/2.0f;//+espc;
		            final double demil2=demil*demil*0.9f;
		                for (int i = 0; i < count; i++) {
		                	Particle courante=mBalls[i];
		        			double xf=courante.mPosX;
		        			double yf=courante.mPosY;
		                    double laxf=((xf+mHoriz2*0.5f)*((double)coco))/mHoriz2;
		                    int laxi=(int)(FloatMath.floor(laxf));
		                    double layf=((yf+mVert2*0.5f)*((double)lili))/mVert2;
		                    int layi=lili-1-(int)(FloatMath.floor(layf));
		                    int jk=laxi+layi*coco;
		                    if(0<=jk && jk<maximur){
		          	            double ox1=hmur[jk][0];
		          	            double oy=hmur[jk][2];
		      	        		double dy=yf-oy;
		          	            double oy1=vmur[jk][0];
		          	          	double ox=vmur[jk][2];
		      	        		double dx=xf-ox;
		  	        			double vx1=courante.mVitX;
		      	            	double vy1=courante.mVitY;

			          	            if(ox1!=5000.0f){
			          	            	double ox2=hmur[jk][1];
			    	      	        	if((ox1<=xf && xf<=ox2)){
			    	      	                if (Math.abs(dy) <= demil) {
			    	      	                	courante.correction(false, oy+Math.signum(dy)*demil);
			    	      	                }
			    	      	        	}else{
			    	      	        		double dx0=xf-ox1;
			    	      	        		double dc20=dx0*dx0+dy*dy;
			    	      	        		if(dc20<demil2){
					          	            	double[] vectf=replassagecoin(xf,yf,ox1,oy,vx1,vy1);
					      	                	courante.mPosX=vectf[0];
					      	                	courante.mPosY=vectf[1];
					      	                	double[] vectvf=collisioncoin(xf,yf,ox1,oy,vx1,vy1);
					      	                	courante.mVitX=vectvf[0];
					      	                	courante.mVitY=vectvf[1];
			    	      	        		}else{
				    	      	        		double dx1=xf-ox2;
				    	      	        		double dc21=dx1*dx1+dy*dy;
				    	      	        		if(dc21<demil2){
						          	            	double[] vectf=replassagecoin(xf,yf,ox2,oy,vx1,vy1);
						      	                	courante.mPosX=vectf[0];
						      	                	courante.mPosY=vectf[1];
						      	                	double[] vectvf=collisioncoin(xf,yf,ox2,oy,vx1,vy1);
						      	                	courante.mVitX=vectvf[0];
						      	                	courante.mVitY=vectvf[1];
				    	      	        		}
			    	      	        		}
			    	      	        	}
			          	            }
			          	            if(oy1!=5000.0f){
			          	            	double oy2=vmur[jk][1];
			    	      	        	if((oy1<=yf && yf<=oy2)){
			    	      	                if (Math.abs(dx) <= demil) {
			    	      	                	courante.correction(true, ox+Math.signum(dx)*demil);
			    	      	                }
			    	      	        	}else{
			    	      	        		double dy0=yf-oy1;
			    	      	        		double dc20=dx*dx+dy0*dy0;
			    	      	        		if(dc20<demil2){
					          	            	double[] vectf=replassagecoin(xf,yf,ox,oy1,vx1,vy1);
					      	                	courante.mPosX=vectf[0];
					      	                	courante.mPosY=vectf[1];
					      	                	double[] vectvf=collisioncoin(xf,yf,ox,oy1,vx1,vy1);
					      	                	courante.mVitX=vectvf[0];
					      	                	courante.mVitY=vectvf[1];
			    	      	        		}else{
				    	      	        		double dy1=yf-oy2;
				    	      	        		double dc21=dx*dx+dy1*dy1;
				    	      	        		if(dc21<demil2){
						          	            	double[] vectf=replassagecoin(xf,yf,ox,oy2,vx1,vy1);
						      	                	courante.mPosX=vectf[0];
						      	                	courante.mPosY=vectf[1];
						      	                	double[] vectvf=collisioncoin(xf,yf,ox,oy2,vx1,vy1);
						      	                	courante.mVitX=vectvf[0];
						      	                	courante.mVitY=vectvf[1];
				    	      	        		}
			    	      	        		}
				          	            }
			          	            }
		                    }
		            }
		        }*/

    public double[] replassagecoinapres(double x1, double y1, double x2, double y2,
                                       double v1x, double v1y){
      double[] vect=new double[2];
      double dx=x1-x2;
      double dy=y1-y2;
      double dvx=v1x;
      double dvy=v1y;
      double dx2=dx*dx;
      double dy2=dy*dy;
      double dvx2=dvx*dvx;
      double dvy2=dvy*dvy;

      double dt1=0.0f;
      double dt2=0.0f;
      double a=dvx2+dvy2;

      if(a<0.0001f){
        return ecartement(x1,y1,x2,y2,true);
      }

      double b=2.0f*(dvx*dx+dvy*dy);
      double c=dx2+dy2-(sBallDiameter2/4.0f+0.00000001f);
      double deltaz=b*b-4.0f*a*c;
      if(a!=0.0f){
        if(deltaz==0.0f){
          dt1=-b/(2.0f*a);
          dt2=dt1;
        }else if (deltaz>0.0f){
          dt1=(-b- (double) Math.sqrt(deltaz))/(2.0f*a);
          dt2=(-b+ (double) Math.sqrt(deltaz))/(2.0f*a);
        }
      }
      double dt=0.0f;
      if(dt1>0.0f){
        dt=dt1;
      }else if(dt2>0.0f){
        dt=dt2;
      }
      if(dt!=0.0f){
        x1+=v1x*dt;
        y1+=v1y*dt;
      }
      vect[0]=x1;
      vect[1]=y1;
      return vect;
    }

    public double[] replassagecoin(double x1, double y1, double x2, double y2,
                                  double v1x, double v1y){
      double[] vect=new double[2];
      double dx=x1-x2;
      double dy=y1-y2;
      double dvx=v1x;
      double dvy=v1y;
      double dx2=dx*dx;
      double dy2=dy*dy;
      double dvx2=dvx*dvx;
      double dvy2=dvy*dvy;

      double dt1=0.0f;
      double dt2=0.0f;
      double a=dvx2+dvy2;

      if(a<0.0001f){
        return ecartement(x1,y1,x2,y2,true);
      }

      double b=2.0f*(dvx*dx+dvy*dy);
      double c=dx2+dy2-(sBallDiameter2/4.0f+0.00000001f);
      double deltaz=b*b-4.0f*a*c;
      if(a!=0.0f){
        if(deltaz==0.0f){
          dt1=-b/(2.0f*a);
          dt2=dt1;
        }else if (deltaz>0.0f){
          dt1=(-b- (double) Math.sqrt(deltaz))/(2.0f*a);
          dt2=(-b+ (double) Math.sqrt(deltaz))/(2.0f*a);
        }
      }
      double dt=0.0f;
      if(dt1<0.0f){
        dt=dt1;
      }else if(dt2<0.0f){
        dt=dt2;
      }
      if(dt!=0.0f){
        x1+=v1x*dt;
        y1+=v1y*dt;
      }
      vect[0]=x1;
      vect[1]=y1;
      return vect;
    }

    public double[] collisioncoin(double x1, double y1, double x2, double y2,
                                 double vx1, double vy1)     {

      double dvx2,a,x21,y21,vx21,vy21,fy21,sign;

      x21=x2-x1;
      y21=y2-y1;
      vx21=-vx1;
      vy21=-vy1;

      if(Math.abs(x21)<0.00000001f){
        if(x21>=0)
          x21=0.00000001f;
        else
          x21=-0.00000001f;
      }

      a=y21/x21;
      dvx2= -(vx21 +a*vy21)/(1.0f+a*a) ;
      double[] helpme=new double[2];
      helpme[0]=vx1-2.0f*dvx2;
      helpme[1]=vy1-2.0f*a*dvx2;

      return helpme;
    }

    public void cestfini(){
      isrunning=false;
      isreallyrunning=false;
      if(nwaow>0){
        nwaow=0;
        //zzz.start();
      }
      nwaow=0;
      int resss=nombre*19+coco*16+nombrep*15+dif1;
      int resultaa=resss-scorr*18-scorr2*26;
      if(resultaa<0)
        resultaa=3;
      String resulta=
              "Score : "+resultaa
                      +"\n="+resss
                      +" - "+scorr+" lost balls"
                      +" - "+scorr2+" (x)";
      /*Uri outUri=Uri.parse(resulta);
      Intent outData=new Intent();
      outData.setData(outUri);
      setResult(Activity.RESULT_OK,outData);*/

      Intent intent = new Intent();
      intent.setAction(Gallery.broadcastname);
      intent.putExtra("goal", "startbrowser");
      intent.putExtra("id", myid);
      LocalBroadcastManager.getInstance(model.activitycontext).sendBroadcast(intent);
    }
  }

  class Particle {
    private volatile long dT=1;
    private volatile double mOlAng=0.0f;
    private volatile double mAng=0.0f;
    private volatile double mPosX=0.0f;
    private volatile double mPosY=0.0f;
    private volatile double mVitX=0.0f;
    private volatile double mVitY=0.0f;
    private volatile double mdVitX=0.0f;
    private volatile double mdVitY=0.0f;
    private volatile double mAccelX=0.0f;
    private volatile double mAccelY=0.0f;
    private volatile double moneminus=0.0f;
    private volatile double mLastoneminus=0.0f;
    private final double mOneMinusFriction;
    private final Paint couleuvre;
    private volatile boolean plassee=false;

    Particle() {
      // make each particle a bit different by randomizing its
      // coefficient of friction
      //final double r = ((double) Math.random() - 0.5f) * 0.2f;
      //  =  (1 -0.1) +  {-0.1;+0.1} soit    0.8<mOMF<1.0
      final double r = randum(sFriction);

      mOneMinusFriction = (decalache-0.01f) + r;//- (r * (0.1f-decalache-0.01f)/0.1f + decalache-0.01f);
      //[0.90001;1.0] r:[0;0.1]
      moneminus=mOneMinusFriction;

      if(r<0.042f)
        dif1+=6;
      if(r<0.02f)
        dif1+=17;
      if(r<0.005f)
        dif1+=27;

      couleuvre=new Paint();
      ColorMatrix filtre= new ColorMatrix();
      float[] matrice=filtre.getArray();
      //double re=1.0f-r*10.0f;//[0;1]
      //double g=255.0f-r*10.0f;//[0;1]
      //double b=1.0f-r*10.0f;//[0;1]
      float n=((float) r*10.0f)*256.0f-123.0f;//[0;255]
      //matrice[0]=re;
      //matrice[6]=g;
      //matrice[12]=b;
      //matrice[4]=n;
      //matrice[9]=n;
      matrice[14]=n;
      ColorMatrixColorFilter mat=new ColorMatrixColorFilter(matrice);
      couleuvre.setColorFilter(mat);
      couleuvre.setAntiAlias(true);
      dT=System.nanoTime();
    }

    public void computePhysics2(double sx, double sy) {
      limitercela();
      final long dpT=System.nanoTime();
      final double deltaT=(double)(dpT-dT)*0.000000001f;

      mLastoneminus=moneminus;
      mAccelX = mdVitX-sx;
      mAccelY = mdVitY-sy;

      //double mangmoy=(mAng+mOlAng)/2.0f;
      final double orien = (double) Math.cos(mAng);
      //frottements agissent sur la vitesse seulement
      final double frottx=mVitX*10.0f*moneminus*deltaT*demardiv;
      final double frotty=mVitY*10.0f*moneminus*deltaT*demardiv;
      //verlet vitesse
      final double vxt112 = mVitX - frottx + 0.5f * mAccelX * deltaT ;
      final double vyt112 = mVitY - frotty + 0.5f * mAccelY * deltaT ;

      mPosX = mPosX + orien * vxt112 * deltaT ;
      mPosY = mPosY + orien * vyt112 * deltaT ;
      mVitX = vxt112 ;
      mVitY = vyt112 ;
      mdVitX=0.0f;
      mdVitY=0.0f;
      moneminus=mOneMinusFriction;

      dT=dpT;
    }

    public void limitercela(){
      final double xmax = 0.1f;//20cm/s = 7Km/h
      final double ymax = 0.1f;
      final double xf = mVitX;
      final double yf = mVitY;

      if (xf > xmax) {
        mVitX=xmax;
      } else if (xf < -xmax) {
        mVitX=-xmax;
      }
      if (yf > ymax) {
        mVitY=ymax;
      } else if (yf < -ymax) {
        mVitY=-ymax;
      }
    }

    public void correction(boolean vertical, double z0){
      if(vertical==true){
        mPosX=z0;
        mVitX=-mVitX;
      }else{
        mPosY=z0;
        mVitY=-mVitY;
      }
      //computePhysics();
    }

    public void resolveCollisionWithBounds() {
      final double xmax = mHorizontalBound+(sBallDiameter*3.0f)/4.0f;
      final double ymax = mVerticalBound+(sBallDiameter*3.0f)/4.0f;
      final double xf = mPosX;
      final double yf = mPosY;

      if (xf > xmax) {
        correction(true,xmax);
      } else if (xf < -xmax) {
        correction(true,-xmax);
      }
      if (yf > ymax) {
        correction(false,ymax);
      } else if (yf < -ymax) {
        correction(false,-ymax);
      }
    }

  }
  
  /**
   * This interface must be implemented by activities that contain this
   * fragment to allow an interaction in this fragment to be communicated
   * to the activity and potentially other fragments contained in that
   * activity.
   * <p>
   * See the Android Training lesson <a href=
   * "http://developer.android.com/training/basics/fragments/communicating.html"
   * >Communicating with Other Fragments</a> for more information.
   */
  
  public OnFragmentInteractionListener fragmentlistener;
  
  public interface OnFragmentInteractionListener {
    // TODO: Update argument type and name
    Integer onFragmentInteraction(Uri uri);
  }
  
  @Override
  public void onAttach(Context context) {
    super.onAttach(context);
    if (context instanceof OnFragmentInteractionListener) {
      fragmentlistener = (OnFragmentInteractionListener) context;
    } else {
      llog.d(TAG, context.toString() + " must implement OnFragmentInteractionListener");
    }
  }
  
  
}






















