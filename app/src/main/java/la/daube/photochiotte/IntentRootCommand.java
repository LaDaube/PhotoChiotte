package la.daube.photochiotte;

import android.content.Context;
import android.content.Intent;
import android.text.InputType;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class IntentRootCommand {
  private final static String TAG = "YYYis";

  public IntentRootCommand(Context context, Intent intent, Gallery model, RelativeLayout mainlayout, EditText input, Button inputvalidate){
    model.deactivateactivitykeydown = true;
    final int id = intent.getIntExtra("id", -1);
    String defaultfolder = intent.getStringExtra("default");

    input.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_WEB_EDIT_TEXT);
    input.setX(0);
    input.setY(100);
    input.setBackgroundColor(Gallery.CouleurTresSombre);
    //input.setHint("regular expressions, case insensitive (e.g. this.*named == ThisFolderIsNamed)");
    input.setHintTextColor(Gallery.CouleurClaire);
    input.setTextColor(Gallery.CouleurTresClaire);
    input.setFocusableInTouchMode(true);
    input.setImeOptions(EditorInfo.IME_ACTION_DONE);
    if (defaultfolder != null)
      input.setText(defaultfolder);

    //int buttonwidth = (int) (model.bigScreenWidth * 0.075f);

    mainlayout.addView(input);
    ViewGroup.LayoutParams parms = input.getLayoutParams();
    parms.width = model.bigScreenWidth;
    input.setLayoutParams(parms);

    //inputvalidate.setX(model.bigScreenWidth - buttonwidth);
    //inputvalidate.setY(0);
    /*ViewGroup.LayoutParams parms2 = inputvalidate.getLayoutParams();
    parms2.width = buttonwidth;
    inputvalidate.setLayoutParams(parms2);*/

    mainlayout.requestLayout();

    model.message(  "run a command as root" +
        "\nwm density 240" +
        "\nwm density reset" +
        "\n...", 0, 100, "left"
    );
  
    input.setOnFocusChangeListener(new View.OnFocusChangeListener() {
      @Override
      public void onFocusChange(View view, boolean focused) {
        InputMethodManager keyboard = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        if (focused)
          keyboard.showSoftInput(input, 0);
        else
          keyboard.hideSoftInputFromWindow(input.getWindowToken(), 0);
      }
    });
  
    input.setOnEditorActionListener(new EditText.OnEditorActionListener() {
      @Override
      public boolean onEditorAction(TextView textView, int actionId, KeyEvent keyEvent) {
        if (actionId == EditorInfo.IME_ACTION_DONE || actionId == EditorInfo.IME_ACTION_UNSPECIFIED) {
          llog.d(TAG, "actionId " + actionId);
          String searchthis = input.getText().toString();
          int searchthisl = searchthis.length();
          if (searchthisl >= 1) {
            if (model.surf.size() > id && id >= 0) {
              model.runcmd = searchthis;
              model.preferences.edit().putString("runcmd", model.runcmd).apply();
            }
          }
          input.clearFocus();
          mainlayout.removeView(input);
          mainlayout.requestLayout();
          model.deactivateactivitykeydown = false;
          return true;
        } else {
          llog.d(TAG, "unused actionId " + actionId);
        }
        return false;
      }
    });
    
    input.requestFocus();
  }
}
