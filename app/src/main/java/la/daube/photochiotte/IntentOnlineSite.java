package la.daube.photochiotte;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Rect;
import android.graphics.RectF;
import android.text.InputType;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class IntentOnlineSite {
  private final static String TAG = "YYYfww";
  private String siteaddress = null;
  private float mabase = 0.0f;
  
  public IntentOnlineSite(Context context, Intent intent, Gallery model, RelativeLayout mainlayout, EditText input, Button inputvalidate){
    model.deactivateactivitykeydown = true;
    final int memory = intent.getIntExtra("memory", -1);
    String monoption = null;
    if (memory >= 0)
      monoption = model.preferences.getString("mediaonline" + memory, null);

    // Specify the type of input expected; this, for example, sets the input as a password, and will mask the text
    input.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_WEB_EDIT_TEXT);
    input.setX(0);
    input.setY(100);
    input.setBackgroundColor(Gallery.CouleurTresSombre);
    if (monoption == null)
      input.setHint("http://192.168.1.1/pictures/ ... http://192.168.1.1/pictures/folderlist.txt ...");
    else
      input.setText(monoption);
    input.setHintTextColor(Gallery.CouleurClaire);
    input.setTextColor(Gallery.CouleurTresClaire);
    input.setFocusableInTouchMode(true);
    input.setImeOptions(EditorInfo.IME_ACTION_DONE);

    mainlayout.addView(input);
    ViewGroup.LayoutParams parms = input.getLayoutParams();
    parms.width = model.bigScreenWidth;
    input.setLayoutParams(parms);
    mainlayout.requestLayout();

    model.message(  "url to your file server"
                  + "\nto get a dynamic browsable view type in"
                  + "\n   your base directory"
                  + "\n   e.g. http://192.168.1.25/public/comics/"
                  + "\nto get an expanded view (preferred) set it"
                  + "\n   to a .txt file containing a list of your folders"
                  + "\n   e.g. http://192.168.1.25/public/comics/folderlist.txt", 0, 100, "left"
    );

  
    input.setOnFocusChangeListener(new View.OnFocusChangeListener() {
      @Override
      public void onFocusChange(View view, boolean focused) {
        InputMethodManager keyboard = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        if (focused)
          keyboard.showSoftInput(input, 0);
        else
          keyboard.hideSoftInputFromWindow(input.getWindowToken(), 0);
      }
    });
    
    input.setOnEditorActionListener(new EditText.OnEditorActionListener() {
      @Override
      public boolean onEditorAction(TextView textView, int actionId, KeyEvent keyEvent) {
        if (actionId == EditorInfo.IME_ACTION_DONE || actionId == EditorInfo.IME_ACTION_UNSPECIFIED) {
          llog.d(TAG, "actionId : " + actionId + " : " + input.getText().toString());
          siteaddress = input.getText().toString();
          if (siteaddress.length() > 3) {
            //askautoload(context, intent, model, mainlayout);
            if (memory == -1) {
              try {
                model.commandethreaddatabase.put(new String[]{"addonline", siteaddress, "noautoload"});
              } catch (InterruptedException e) {
                e.printStackTrace();
              }
            } else {
              model.preferences.edit().putString("mediaonline" + memory, siteaddress).commit();
            }
          } else if (memory >= 0) {
            try {
              model.commandethreaddatabase.put(new String[]{"deleteonline", String.valueOf(memory)});
            } catch (InterruptedException e) {
              e.printStackTrace();
            }
          }
          input.clearFocus();
          mainlayout.removeView(input);
          mainlayout.requestLayout();
          model.deactivateactivitykeydown = false;
          return true;
        } else {
          llog.d(TAG, "actionId " + actionId);
        }
        return false;
      }
    });
  
    input.requestFocus();
  }
  
  public void askautoload(Context context, Intent intent, Gallery model, RelativeLayout mainlayout) {
    float quatredixiememillimetre = model.GenericCaseH * 0.12f;
    float roundedRectRatio = model.GenericCaseH * 0.2f;
    Rect bounds = new Rect();
    float y1, y2, x1, x2;
    ViewGroup.LayoutParams parms;
    
    mabase = 0.0f;
    final int largeur = model.bigScreenWidth;
    final int hauteur = (int) model.GenericCaseH;
    final RectF recti = new RectF(0, 0, largeur, hauteur);
    final RectF rectisel = new RectF(quatredixiememillimetre, quatredixiememillimetre, largeur - quatredixiememillimetre, hauteur - quatredixiememillimetre);
    
    final Bitmap display[] = new Bitmap[2];
    final String ecritoption[] = new String[2];
    final float ecritx[] = new float[2];
    final float ecrity[] = new float[2];
    final ImageView status[] = {new ImageView(context), new ImageView(context)};
    final Canvas canv[] = new Canvas[2];
    final int id[] = {26516, 516848};
    
    ecritoption[0] = "click it in the menu to load it";
    y1 = mabase;
    y2 = mabase + model.GenericCaseH;
    x1 = model.GenericInterSpace * 2;
    x2 = model.bigScreenWidth - model.GenericInterSpace * 2;
    
    status[0].setX(x1);
    status[0].setY(y1);
    status[0].setFocusableInTouchMode(true);
    status[0].setId(id[0]);
    mainlayout.addView(status[0]);
    parms = status[0].getLayoutParams();
    parms.width = largeur;
    parms.height = hauteur;
    status[0].setLayoutParams(parms);
    display[0] = Bitmap.createBitmap(largeur, hauteur, Bitmap.Config.ARGB_8888);
    canv[0] = new Canvas(display[0]);
    
    model.Menu1TextePaint.getTextBounds(ecritoption[0], 0, ecritoption[0].length(), bounds);
    ecritx[0] = x2 - model.GenericInterSpace;
    ecrity[0] = model.GenericCaseH * 0.5f + bounds.height() * 0.5f - bounds.bottom;
    canv[0].drawColor(Color.BLACK);
    canv[0].drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu1BgPainto);
    canv[0].drawText(ecritoption[0], ecritx[0], ecrity[0], model.Menu1TextePaint);
    status[0].setImageBitmap(display[0]);
    if (model.isandroidtv) {
      status[0].setOnFocusChangeListener(new View.OnFocusChangeListener() {
        @Override
        public void onFocusChange(View view, boolean hasFocus) {
          llog.d(TAG, "1 onFocusChange " + hasFocus);
          if (hasFocus && model.isandroidtv) {
            canv[0].drawColor(Color.BLACK);
            canv[0].drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu1BgPainto);
            canv[0].drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu1BgPainto);
            canv[0].drawRoundRect(rectisel, roundedRectRatio, roundedRectRatio, model.SettingsSelPaint);
            canv[0].drawText(ecritoption[0], ecritx[0], ecrity[0], model.Menu1TextePaint);
          } else {
            canv[0].drawColor(Color.BLACK);
            canv[0].drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu1BgPainto);
            canv[0].drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu1BgPainto);
            canv[0].drawText(ecritoption[0], ecritx[0], ecrity[0], model.Menu1TextePaint);
          }
        }
      });
    }
    status[0].setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        llog.d(TAG, "1 onClick ");
        mainlayout.removeView(status[0]);
        mainlayout.removeView(status[1]);
        mainlayout.requestLayout();
        try {
          model.commandethreaddatabase.put(new String[]{"addonline", siteaddress, "noautoload"});
        } catch (InterruptedException e) {
          e.printStackTrace();
        }
        model.deactivateactivitykeydown = false;
      }
    });
  
    mabase += model.GenericCaseH + model.GenericInterSpace;
    
    ecritoption[1] = "automatic load at startup, if available";
    y1 = mabase;
    y2 = mabase + model.GenericCaseH;
    x1 = model.GenericInterSpace;
    x2 = model.bigScreenWidth - model.GenericInterSpace;
    
    status[1].setX(x1);
    status[1].setY(y1);
    status[1].setFocusableInTouchMode(true);
    status[1].setId(id[1]);
    mainlayout.addView(status[1]);
    parms = status[1].getLayoutParams();
    parms.width = largeur;
    parms.height = hauteur;
    status[1].setLayoutParams(parms);
    display[1] = Bitmap.createBitmap(largeur, hauteur, Bitmap.Config.ARGB_8888);
    canv[1] = new Canvas(display[1]);
    
    model.Menu1TextePaint.getTextBounds(ecritoption[1], 0, ecritoption[1].length(), bounds);
    ecritx[1] = x2 - model.GenericInterSpace;
    ecrity[1] = model.GenericCaseH * 0.5f + bounds.height() * 0.5f - bounds.bottom;
    canv[1].drawColor(Color.BLACK);
    canv[1].drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu1BgPainto);
    canv[1].drawText(ecritoption[1], ecritx[1], ecrity[1], model.Menu1TextePaint);
    status[1].setImageBitmap(display[1]);
    if (model.isandroidtv) {
      status[1].setOnFocusChangeListener(new View.OnFocusChangeListener() {
        @Override
        public void onFocusChange(View view, boolean hasFocus) {
          llog.d(TAG, "2 onFocusChange " + hasFocus);
          if (hasFocus && model.isandroidtv) {
            canv[1].drawColor(Color.BLACK);
            canv[1].drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu1BgPainto);
            canv[1].drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu1BgPainto);
            canv[1].drawRoundRect(rectisel, roundedRectRatio, roundedRectRatio, model.SettingsSelPaint);
            canv[1].drawText(ecritoption[1], ecritx[1], ecrity[1], model.Menu1TextePaint);
          } else {
            canv[1].drawColor(Color.BLACK);
            canv[1].drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu1BgPainto);
            canv[1].drawRoundRect(recti, roundedRectRatio, roundedRectRatio, model.Menu1BgPainto);
            canv[1].drawText(ecritoption[1], ecritx[1], ecrity[1], model.Menu1TextePaint);
          }
        }
      });
    }
    status[1].setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        llog.d(TAG, "2 onClick ");
        mainlayout.removeView(status[0]);
        mainlayout.removeView(status[1]);
        mainlayout.requestLayout();
        try {
          model.commandethreaddatabase.put(new String[]{"addonline", siteaddress, "autoload"});
        } catch (InterruptedException e) {
          e.printStackTrace();
        }
        model.deactivateactivitykeydown = false;
      }
    });
    
    mabase += model.GenericCaseH + model.GenericInterSpace;
  
    if (model.isandroidtv) {
      status[0].setNextFocusForwardId(id[1]);
      status[0].setNextFocusDownId(id[1]);
      status[0].setNextFocusUpId(id[1]);
      status[1].setNextFocusForwardId(id[0]);
      status[1].setNextFocusDownId(id[0]);
      status[1].setNextFocusUpId(id[0]);
    }
  
    mainlayout.requestLayout();
    if (model.isandroidtv) {
      status[0].requestFocus();
    }
    
  }
  
  
}
