package la.daube.photochiotte;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import java.util.Random;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link FragmentGameBounceCurve.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link FragmentGameBounceCurve#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FragmentGameBounceCurve extends Fragment {
  private static final String ARG_PARAM1 = "param1";
  private static final String ARG_PARAM2 = "param2";
  private static final String TAG = "YYYfg1";

  private Gallery model;

  public int myid = -1;
  private Surf mysurf = null;

  public FragmentGameBounceCurve() {
    // Required empty public constructor
  }

  public static FragmentGameBounceCurve newInstance(String param1, String param2) {
    FragmentGameBounceCurve fragment = new FragmentGameBounceCurve();
    Bundle args = new Bundle();
    args.putString(ARG_PARAM1, param1);
    args.putString(ARG_PARAM2, param2);
    fragment.setArguments(args);
    return fragment;
  }

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    if (getArguments() != null) {
      myid = getArguments().getInt("myid", -1);
      cefichier = getArguments().getString("cefichier", "");
    }
    //model = ViewModelProviders.of(getActivity()).get(myViewModel.class);
    model = new ViewModelProvider(requireActivity()).get(Gallery.class);
    model.currentselectedfragment = myid;
    llog.d(TAG, myid+" onCreate() " + myid + " -> " + cefichier);
  }

  @Override
  public void onResume() {
    super.onResume();
    llog.d(TAG, myid+" onResume()");
  }

  @Override
  public void onStart() {
    super.onStart();
    llog.d(TAG, myid+" onStart()");
  }

  @Override
  public void onPause() {
    super.onPause();
    llog.d(TAG, myid+" onPause()");
  }

  @Override
  public void onDestroy() {
    super.onDestroy();
    llog.d(TAG, myid+" onDestroy()");
    isrunning = false;
    isreallyrunning = false;
    mWood.recycle();
    //mamaow.recycle();
    /*for(int uu=0;uu<parterre.length;uu++)
      parterre[uu].recycle();
    for(int uu=0;uu<pasterre.length;uu++)
      pasterre[uu].recycle();*/
    //if(affiche==true)
    //  zzz.start();
  }

  // ceci sera dans l'ordre dans les préférences !!!
  private int tempsaffiche;
  private int tempscalcul;
  private double limaccel; // 0.1f
  private double decalache; // 0.01f niveau de difficulté 1 = le plus dur
  private double attir; // champ d'attraction 0.0005f
  private double multiple; // champ de répulsion 0.0010f
  private double glurp; // durée de deviation 0.005f=200fois
  private double mulipli; // durée de deviation 0.005f=200fois
  private double sBallDiameter = 0.002f;
  private double sBallDiameter2;
  private double ballibump;// toucher bump
  private double completeness;
  private int nbums;
  private double bums;
  private double bumsh;
  private double aide=1.0f;
  private double field=0.025f;//=0.02f;
  private double incertitude=0.06f;
  private double cinqdixmillieme=0.0005f;
  private double quatredixieme=0.0001f;
  private double troisdixieme=0.001f;

  private final double pi = 3.141592653589793;
  private double[][] pose = null;
  private String wavou;
  static private Uri hurry;
  private Thread zzz;
  private String cefichier = null;
  private Bitmap mamaow = null;
  private Bitmap[] parterre = null;
  private Bitmap[] pasterre = null;
  private Bitmap[] debutbits = null;
  private Bitmap mBitmap = null;
  private Bitmap mWood = null;
  private volatile int largeur = 480;
  private volatile int nwaow = 0;
  private volatile boolean tourne = false;
  private volatile int hauteur = 800;
  private volatile double bbx = 0.2f;
  private volatile double bby = -0.2f;
  private volatile boolean isrunning = false;
  private volatile boolean isreallyrunning = false;
  private volatile double[] touche = { -1.0f, -1.0f };
  private volatile double[] touche2 = { -1.0f, -1.0f };
  private volatile boolean bloquelikethis = true;
  private double msx = 0.0f;
  private double msy = 0.0f;
  private double valeurmemo = 0.0f;
  private int supernw = 4;
  private int supernh = 5;// >16 !!!!
  private int prefalpha = 80;
  private int prefcouleur = Color.YELLOW;
  private double sFriction = 0.02f;// 0.10f
  private int nombre;// baballes et troutrous
  // private int nombrem;//portails
  private int nombrep;// croix
  private volatile long mSensorTimeStamp;
  private volatile long mCpuTimeStamp;
  private volatile double mSensorX;
  private volatile double mSensorY;
  private final int[][] coord = new int[][] { { 1, 0 }, // 0=E
          { 0, -1 }, // 1=N
          { -1, 0 }, // 2=W
          { 0, 1 }, // 3=S
  };
  private volatile double areduire=0.0f;
  private Bitmap trou = null;
  private Bitmap paf = null;
  private Bitmap paf2 = null;
  private Bitmap up = null;
  private Bitmap left = null;
  private Random r5;
  private int coco;// 11max
  private int lili;
  private double mXDpi;
  private double mYDpi;
  //private double mMetersToPixelsX;
  //private double mMetersToPixelsY;
  private int[] pointxi;
  private int[] pointyi;
  private double[] pointx;
  private double[] pointy;
  private int[] murxi;
  private int[] muryi;
  private int[] murpxi;
  private int[] murpyi;
  private double[] murpx;
  private double[] murpy;
  private double[] murx;
  private double[] mury;
  private boolean[][] scatterize;// emplacements privilégiés des pièges
  private Canvas canv;
  private Paint paint;
  private int colonnes;
  private int lignes;
  private int[] avamur;
  private int[] avatrou;
  private int[] avapif;
  private int[] conversion;
  private double[][] hvmur;
  private boolean[] validecoord = new boolean[4];
  private int[][] chemin;
  private int scorr = 0;
  private int scorr2 = 0;
  private double mHoriz2, mVert2;
  private Paint ppa;
  private int dif1 = 0;
  private int maximur = 0;
  private int maxiconvers = 0;
  private int[][] decoupage;
  private volatile boolean[] libre;
  private Particle[] mBalls;
  private volatile boolean evenement = false;
  private volatile boolean[][] evene;
  private volatile double[] blocktimexy = { 0.0f, 0.0f };
  private volatile int scaure = 0;
  private int maxicola;
  private volatile int cellecila = 0;
  private volatile double[] drawschlang = { -1.0f, -1.0f, -1.0f, -1.0f, -1.0f,
          -1.0f, -1.0f, -1.0f, -1.0f, -1.0f };
  private volatile double[][] electrique = {
          {-1.0f, -1.0f, -1.0f}
  };
  //private volatile ArrayList<String> eralpha=new ArrayList();
  //private String eralphastr="";

  public double gauss(double x) {
    double z = x - 0.5;
    double zz = z * z * z;
    z = 4.0 * zz + 0.5;
    return z;
  }

  public double randum(double y) {
    double u = Math.random();
    u = gauss(u);
    double v = (double) (u);
    v = y * v;
    return v;
  }

  @SuppressLint("ClickableViewAccessibility")
  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
    mysurf = model.surf.get(myid);
    mysurf.fragmenttype = Surf.FRAGMENT_TYPE_PUZZLE;
    mysurf.fragmentView = inflater.inflate(R.layout.fragment_game_hit, container, false);

    mysurf.fragmentView.setX(model.surf.get(myid).myx);
    mysurf.fragmentView.setY(model.surf.get(myid).myy);
    ViewGroup.LayoutParams parms = mysurf.fragmentView.getLayoutParams();
    parms.width = model.surf.get(myid).mywidth;
    parms.height = model.surf.get(myid).myheight;
    mysurf.fragmentView.setLayoutParams(parms);
    RelativeLayout relativelayout = mysurf.fragmentView.findViewById(R.id.relativelayoutHit);
    relativelayout.setLayoutParams(parms);

    llog.d(TAG, myid+" onCreateView()");
    prefcouleur = model.preferences.getInt("grid_color", Color.YELLOW);
    prefalpha = model.preferences.getInt("grid_intensity", 80);
    supernw = model.gamecolumns;
    supernh = model.gamelines;

    r5 = Gallery.rand;
    final double[] defval = ballz.defaut[4];
    nbums = r5.nextInt(((int) Math.floor(model.preferences.getFloat("4param0", (float) defval[0]))));
    tempscalcul = (int) Math.floor(model.preferences.getFloat("4param1", (float) defval[1]));
    bumsh=(model.preferences.getFloat("4param2", (float) defval[2]));
    decalache = (model.preferences.getFloat("4param3", (float) defval[3])); // 0.01f
    glurp = 10000.0f * (model.preferences.getFloat("4param6", (float) defval[6])); // durée
    mulipli = (model.preferences.getFloat("4param7", (float) defval[7])); // importance
    aide = (model.preferences.getFloat("4param8", (float) defval[8])); // importance
    incertitude = (model.preferences.getFloat("4param9", (float) defval[9])); // importance
    ballibump = (model.preferences.getFloat("4param11", (float) defval[11])); // taille
    ballibump=1.0f/(2000.0f*ballibump);
    sFriction = (model.preferences.getFloat("4param12", (float) defval[12])); // 0.01f
    completeness = (model.preferences.getFloat("4param13", (float) defval[13])); // 0.01f
    sBallDiameter = (model.preferences.getFloat("4param14", (float) defval[14])); // taille
    attir = (model.preferences.getFloat("4param4", (float) defval[4])); // champ d'attraction
    multiple = (model.preferences.getFloat("4param5", (float) defval[5])); //0.0010f
    limaccel = (model.preferences.getFloat("4param15", (float) defval[15])); // 0.1f
    field = (model.preferences.getFloat("4param16", (float) defval[16])); //bum field size
    bums = (model.preferences.getFloat("4param17", (float) defval[17]));  //gravité des trous attir

    //corrections
    double correction=160.210525f/0.0254f;
    sBallDiameter*=correction;
    attir*=sBallDiameter;
    multiple*=sBallDiameter;
    ballibump*=correction;
    bums*=correction;
    bumsh*=correction;
    cinqdixmillieme*=correction;
    quatredixieme*=correction;
    troisdixieme*=correction;
    field*=correction;

    sBallDiameter2 = sBallDiameter * sBallDiameter;
    glurp = glurp / ((double) (tempscalcul + 1));
    nombre = (int) (randum(10.0f)) + 1;// baballes et troutrous 15
    // nombrem=(int)(randum(16.0f))+4;//portails
    nombrep = (int) (randum(4.0f)) + 1;// croix
    int var=r5.nextInt(3);
    coco = model.preferences.getInt("cut_col6", 4) + var;// 8-11
    lili = model.preferences.getInt("cut_lig6", 7) + var;// 16-22
    coco = model.gamecolumns;
    lili = model.gamelines;
    ppa = new Paint();
    electrique=new double[nbums][3];

    int difficulty = (int)Math.ceil(((double)(coco * lili)) * completeness);
    String cola = nombre + "bou"+nbums+"nce" + difficulty;
    int maximaxicola=nombre*(4+2*nbums);
    maxicola = model.preferences.getInt(cola, maximaxicola);

    mBalls = new Particle[nombre];
    for (int i = 0; i < nombre; i++) {
      mBalls[i] = new Particle();
    }
    mBalls[cellecila].changecouleur();

    evene = new boolean[nombre][2];
    for (int i = 0; i < nombre; i++) {
      evene[i][0] = false;
      evene[i][1] = false;
    }

    // Restore preferences
    valeurmemo = model.preferences.getFloat("inCLINAIS", 0.0f);
    bbx = valeurmemo;
    bby = -valeurmemo;

    largeur= model.surf.get(myid).mywidth;
    hauteur= model.surf.get(myid).myheight;
    nwaow = 1;

    if (mysurf.isincache(cefichier)) {
      mamaow = mysurf.getcachedbitmap(cefichier);
    }
    llog.d(TAG, cefichier + " : " + mamaow.getWidth() + " xxxxx " + mamaow.getHeight());

    demarrage();

    //final SimulationView imageviewpicture = mysurf.pictureview.findViewById(R.id.viewImageHit);
    SimulationView imageviewpicture = new SimulationView(getActivity().getApplicationContext());
    relativelayout.addView(imageviewpicture);

    mysurf.fragmentView.setOnTouchListener(new View.OnTouchListener() {
      public boolean onTouch(View arg0, MotionEvent arg1) {
        model.currentselectedfragment = myid;

        if (arg1.getAction() == MotionEvent.ACTION_DOWN
                && isreallyrunning) {
          touche[0] = arg1.getX();
          touche[1] = arg1.getY();
          drawschlang[0] = touche[0];
          drawschlang[1] = touche[1];
          touche2[0] = -1.0f;
          return true;
        }
        if (arg1.getAction() == MotionEvent.ACTION_MOVE) {
          drawschlang[2] = 3.0f * arg1.getX() - 2.0f * touche[0];
          drawschlang[3] = 3.0f * arg1.getY() - 2.0f * touche[1];
          return true;
        }
        if (arg1.getAction() == MotionEvent.ACTION_UP) {
          touche2[0] = arg1.getX();
          touche2[1] = arg1.getY();
        }
        return false;
      }
    });
    isrunning=true;

    // Inflate the layout for this fragment
    return mysurf.fragmentView;
  }


  public void ajusteoriente() {
    boolean nerienfaire = false;
    if (tourne == true && (bbx != valeurmemo || bby != 0.0f)) {
      nerienfaire = true;
      bbx = valeurmemo;
      bby = 0.0f;
    }
    if (tourne == false && (bby != -valeurmemo || bbx != 0.0f)) {
      nerienfaire = true;
      bby = -valeurmemo;
      bbx = 0.0f;
    }
    if (nerienfaire == false) {
      if (tourne == true) {
        valeurmemo = Math.abs(msx);
        if (valeurmemo < 0.0f)
          valeurmemo = 0.0f;
        bbx = valeurmemo;
      } else {
        valeurmemo = msy;
        if (valeurmemo < 0.0f)
          valeurmemo = 0.0f;
        bby = -valeurmemo;
      }
    }
  }

  public void executecetruc() {
    final Uri hurryup = hurry;
    zzz = new Thread(new Runnable() {
      public void run() {
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_VIEW);
        intent.setDataAndType(hurryup, "image/jpeg");
        startActivity(intent);

      }
    });
  }

  public Bitmap ShrinkBitmap(String file) {
    int height = 1000;
    int width = height;
    BitmapFactory.Options bmpFactoryOptions = new BitmapFactory.Options();
    bmpFactoryOptions.inJustDecodeBounds = true;
    Bitmap bitmap = BitmapFactory.decodeFile(file, bmpFactoryOptions);
    double oh = (double) (bmpFactoryOptions.outHeight);
    double ow = (double) (bmpFactoryOptions.outWidth);
    int heightRatio = (int) Math.ceil(oh / (double) height);// ceil
    int widthRatio = (int) Math.ceil(ow / (double) width);

    if (heightRatio > 1 || widthRatio > 1) {
      if (heightRatio > widthRatio) {
        bmpFactoryOptions.inSampleSize = heightRatio;
      } else {
        bmpFactoryOptions.inSampleSize = widthRatio;
      }
    }

    bmpFactoryOptions.inJustDecodeBounds = false;
    bmpFactoryOptions.inPreferQualityOverSpeed=true;
    bitmap = BitmapFactory.decodeFile(file, bmpFactoryOptions);
    if (bitmap == null)
      nwaow = 0;
    return bitmap;
  }

  public void choiximage() {
      /*File imagi = cefichier;
      wavou = imagi.getAbsolutePath();
      hurry = Uri.fromFile(imagi);
      executecetruc();
      mamaow = ShrinkBitmap(wavou);
      int www = mamaow.getWidth();
      int wwh = mamaow.getHeight();
      if (www > wwh) {
          Bitmap mama = Bitmap.createBitmap(wwh, www, Config.ARGB_8888);
          for (int v = 0; v < wwh; v++)
              for (int u = 0; u < www; u++)
                  mama.setPixel((wwh-1-v), u, mamaow.getPixel(u, v));
          mamaow = Bitmap.createBitmap(mama);
          mama.recycle();
          tourne = true;
      }*/
  }

  public Bitmap rotimage(Bitmap droitt) {
    int www2 = droitt.getWidth();
    int wwh2 = droitt.getHeight();
    Bitmap mamie = Bitmap.createBitmap(wwh2, www2, Bitmap.Config.ARGB_8888);
    for (int v = 0; v < wwh2; v++)
      for (int u = 0; u < www2; u++)
        mamie.setPixel(v, (www2 - 1 - u), droitt.getPixel(u, v));
    Bitmap mamy = Bitmap.createBitmap(mamie);
    mamie.recycle();
    return mamy;
  }

    /*
	@Override
	public boolean onKeyUp(int keyCode, KeyEvent event) {
		switch (keyCode) {
		case KeyEvent.KEYCODE_BACK:
			isrunning = false;
			isreallyrunning = false;
			return true;
		case KeyEvent.KEYCODE_MENU:
			bloquelikethis = !bloquelikethis;
			return true;
		case KeyEvent.KEYCODE_SEARCH:
			ajusteoriente();
			return true;
		default:
			return super.onKeyUp(keyCode, event);
		}
	}
	*/



  public void demarrage() {
    //DisplayMetrics metrics = new DisplayMetrics();
    //getWindowManager().getDefaultDisplay().getMetrics(metrics);
    mXDpi = model.mXDpi;
    mYDpi = model.mYDpi;
    //double mMetersToPixelsX = mXDpi / 0.0254f;
    //double mMetersToPixelsY = mYDpi / 0.0254f;
    int taratatax=(int)(((double)largeur)/22.0f);
    int taratatay=(int)(((double)largeur*mXDpi/mYDpi)/22.0f);
    // rescale the ball so it's about 0.5 cm on screen
    Bitmap ball = BitmapFactory.decodeResource(getResources(), R.drawable.ball);
    // taille réelle de la balle
    final int dstWidth = (int) (sBallDiameter );//* mMetersToPixelsX);
    final int dstHeight = (int) (sBallDiameter );//* mMetersToPixelsY);
    // trou croix fleche
    final int dstWidth2 = taratatax;
    final int dstHeight2 = taratatay;
    // mouche vache
    int dstWidth5 = taratatax * 8;
    int dstHeight5 = taratatay * 8;
    mBitmap = Bitmap.createScaledBitmap(ball, dstWidth, dstHeight, true);
    Bitmap boing = BitmapFactory.decodeResource(getResources(),
            R.drawable.boing);
    Bitmap boing2 = BitmapFactory.decodeResource(getResources(),
            R.drawable.boing2);
    Bitmap upp = BitmapFactory.decodeResource(getResources(),
            R.drawable.oup);
    Bitmap hole = BitmapFactory.decodeResource(getResources(),
            R.drawable.hole);
    debutbits = new Bitmap[6];
    debutbits[2] = BitmapFactory.decodeResource(getResources(),
            R.drawable.flup);
    debutbits[3] = BitmapFactory.decodeResource(getResources(),
            R.drawable.flup2);
    debutbits[0] = Bitmap.createScaledBitmap(debutbits[2], dstWidth5,
            dstHeight5, true);
    debutbits[1] = Bitmap.createScaledBitmap(debutbits[3], dstWidth5,
            dstHeight5, true);
    debutbits[2].recycle();
    debutbits[3].recycle();
    debutbits[4] = BitmapFactory.decodeResource(getResources(),
            R.drawable.xinvert);
    debutbits[2] = Bitmap.createScaledBitmap(debutbits[4], dstWidth5,
            dstHeight5, true);
    debutbits[4].recycle();
    trou = Bitmap.createScaledBitmap(hole, dstWidth2, dstHeight2, true);
    paf2 = Bitmap.createScaledBitmap(boing, dstWidth2, dstHeight2, true);
    paf = Bitmap.createScaledBitmap(boing2, dstWidth2, dstHeight2, true);
    up = Bitmap.createScaledBitmap(upp, dstWidth2, dstHeight2, true);
    Bitmap leftt = rotimage(upp);
    left = Bitmap.createScaledBitmap(leftt, dstWidth2, dstHeight2, true);
    upp.recycle();
    leftt.recycle();
    boing.recycle();
    boing2.recycle();
    hole.recycle();

    BitmapFactory.Options opts = new BitmapFactory.Options();
    opts.inPreferredConfig = Bitmap.Config.RGB_565;
    Bitmap fondfond = BitmapFactory.decodeResource(getResources(),
            R.drawable.pat2, opts);
    int patw = fondfond.getWidth();
    int path = fondfond.getHeight();
    int ww = largeur;
    int wh = hauteur;
    Bitmap fond = Bitmap.createBitmap(ww, wh, Bitmap.Config.RGB_565);
    Canvas fdcanv = new Canvas(fond);
    for (int fnd = 0; fnd < ww; fnd += patw)
      for (int fndh = 0; fndh < wh; fndh += path)
        fdcanv.drawBitmap(fondfond, fnd, fndh, new Paint());
    fondfond.recycle();

    ColorMatrix couleurs;
    ColorMatrixColorFilter coul;
    Bitmap fond2 = Bitmap.createBitmap(ww, wh, Bitmap.Config.RGB_565);
    Canvas c = new Canvas(fond2);
    Paint paint = new Paint();
    couleurs = modifyback();
    coul = new ColorMatrixColorFilter(couleurs);
    paint.setColorFilter(coul);
    c.drawBitmap(fond, 0.0f, 0.0f, paint);

    fond.recycle();
    mWood = Bitmap.createScaledBitmap(fond2, ww, wh, true);
    fond2.recycle();
    canv = new Canvas(mWood);
    paint = new Paint();

    onsizechanged(ww, wh);
  }

  public void traitementimage(int ww, int wh) {
    Bitmap mamaoww;
    int w2 = mamaow.getWidth();
    int h2 = mamaow.getHeight();
    if (tourne == true) {
      bby = 0.0f;
      canv.drawBitmap(left, (float) (ww) / 2.0f, (float) (wh) / 2.0f,
              paint);
      for (int uu = 0; uu < 3; uu++) {
        debutbits[uu] = rotimage(debutbits[uu]);
      }
    } else {
      bbx = 0.0f;
      canv.drawBitmap(up, (float) (ww) / 2.0f, (float) (wh) / 2.0f, paint);
    }
    up.recycle();
    left.recycle();
    double ratiof = ((double) (ww)) / ((double) (wh));
    double ratio2 = ((double) (w2)) / ((double) (h2));
    if (ratio2 > ratiof) {
      h2 = (int) ((double) Math.floor((double) (ww) / ratio2));
      w2 = ww;
    } else {
      h2 = wh;
      w2 = (int) ((double) Math.floor((double) (wh) * ratio2));
    }
    if (h2 > wh)
      h2 = wh;
    if (w2 > ww)
      w2 = ww;
    mamaoww = Bitmap.createScaledBitmap(mamaow, w2, h2, true);
    //mamaow.recycle();
    int decalh = (int) ((double) Math.floor(Math.abs((double) (h2 - wh) / 2.0f)));
    int decalw = (int) ((double) Math.floor(Math.abs((double) (w2 - ww) / 2.0f)));

    double[] electroc=new double[2];
    Paint nopainttext6 = new Paint();
    nopainttext6.setColor(prefcouleur);//bosses
    nopainttext6.setAntiAlias(true);
    nopainttext6.setAlpha(20);
    nopainttext6.setStyle(Paint.Style.STROKE);
    int maxprout=12;
    double []rayon=ballz.ygauss(maxprout,-1.0f);
    Random rr= Gallery.rand;
    for(int bv=0;bv<nbums;bv++){
      electroc[0]=rr.nextFloat()*(double)(largeur);
      electroc[1]=rr.nextFloat()*(double)(hauteur);

      electrique[bv][0] = electroc[0];
      electrique[bv][1] = electroc[1];

      electrique[bv][2]=rr.nextFloat()*(field*0.25f)+field*0.75f;

      double olray=0.0f;
      for(int prout=1;prout<maxprout;prout++){
        double ray=electrique[bv][2]*rayon[prout];
        nopainttext6.setStrokeWidth((float)Math.abs(olray-ray));
        canv.drawCircle((float) electroc[0],(float) electroc[1],(float) ray,nopainttext6);
      }
    }

    Bitmap fondimage = Bitmap.createBitmap(mWood);
    Canvas cvfondimage = new Canvas(fondimage);
    int[] colors=new int[w2*h2];
    mamaoww.getPixels(colors, 0, w2, 0, 0, w2, h2);
    int[] newcolors=ballz.transformation(colors, w2, h2, electrique, nbums);
    mamaoww.setPixels(newcolors, 0, w2, 0, 0, w2, h2);
    cvfondimage.drawBitmap(mamaoww, decalw, decalh, paint);

    // ppa.setAlpha(100);
    ppa.setAntiAlias(true);
    // ppa.setShadowLayer(1.0f, 0.001f, 0.001f, Color.BLACK);
    int troutaille;
    int troutailleh;
    int trouw = trou.getWidth() / 2;
    int trouh = trou.getHeight() / 2;
    int paftaille;
    int paftailleh;
    int pafw = paf.getWidth() / 2;
    int pafh = paf.getHeight() / 2;
    int piftaille;
    int piftailleh;
    int pifw = paf2.getWidth() / 2;
    int pifh = paf2.getHeight() / 2;
    for (int jk = 0; jk < nombre; jk++) {
      troutaille = pointxi[jk] - trouw;
      troutailleh = pointyi[jk] - trouh;
      cvfondimage.drawBitmap(trou, troutaille, troutailleh, ppa);
    }/*
     * for(int jk=0;jk<nombrem;jk++){ paftaille=murxi[jk]-pafw;
     * paftailleh=muryi[jk]-pafh; cvfondimage.drawBitmap(paf, paftaille,
     * paftailleh, ppa); }
     */
    for (int jk = 0; jk < nombrep; jk++) {
      piftaille = murpxi[jk] - pifw;
      piftailleh = murpyi[jk] - pifh;
      cvfondimage.drawBitmap(paf2, piftaille, piftailleh, ppa);
    }
    double xxx1, yyy1, xxx2, yyy2, xxx3, yyy3;
    Paint pa = new Paint();
    pa.setColor(prefcouleur);
    pa.setAntiAlias(true);
    Paint ppp = new Paint();
    ppp.setAntiAlias(true);
    ppp.setColor(prefcouleur);
    ppp.setAlpha(prefalpha);
    int jkjkjk=lili*coco;
    for (int jk = 0; jk < jkjkjk; jk++) {
      if (hvmur[jk][0] != 5000.0f) {
        xxx1 = hvmur[jk][0];
        yyy1 = hvmur[jk][1];
        xxx2 = hvmur[jk][2];
        yyy2 = hvmur[jk][3];
        canv.drawLine((float) xxx2, (float) yyy2, (float) xxx1, (float) yyy1, pa);
        xxx1 = hvmur[jk][0];
        yyy1 = hvmur[jk][1];
        xxx2 = hvmur[jk][2];
        yyy2 = hvmur[jk][3];
        cvfondimage.drawLine((float) xxx2, (float) yyy2, (float) xxx1, (float) yyy1, ppp);
      }
      if (hvmur[jk][4] != 5000.0f) {
        xxx2 = hvmur[jk][2];
        yyy2 = hvmur[jk][3];
        xxx3 = hvmur[jk][4];
        yyy3 = hvmur[jk][5];
        canv.drawLine((float) xxx2, (float) yyy2, (float) xxx3, (float) yyy3, pa);
        xxx2 = hvmur[jk][2];
        yyy2 = hvmur[jk][3];
        xxx3 = hvmur[jk][4];
        yyy3 = hvmur[jk][5];
        cvfondimage.drawLine((float) xxx2, (float) yyy2, (float) xxx3, (float) yyy3, ppp);
      }
    }
    mamaoww.recycle();
    mamaoww = Bitmap.createBitmap(fondimage, decalw, decalh, w2, h2);
    fondimage.recycle();

    int nw = supernw;
    int nh = supernh;
    int parta = nw * nh;
    int wt = (int) ((double) Math.floor(((double) w2) / ((double) (nw))));
    int ht = (int) ((double) Math.floor(((double) h2) / ((double) (nh))));
    // on créé les différentes parties découpées en paires
    parterre = new Bitmap[parta];
    pasterre = new Bitmap[parta];
    pose = new double[parta][2];

    // gaffe aux arrondis on dépasse pas les bornes
    while (nw * wt > w2 || nw * wt + decalw > ww)
      wt--;
    while (nh * ht > h2 || nh * ht + decalh > wh)
      ht--;

    int z1, z2, z3;
    for (int xn = 0; xn < nw; xn++) {
      for (int yn = 0; yn < nh; yn++) {
        z1 = yn + xn * nh;
        z2 = xn * wt;
        z3 = yn * ht;
        parterre[z1] = Bitmap.createBitmap(mamaoww, z2, z3, wt, ht);
        z2 += decalw;
        z3 += decalh;
        pasterre[z1] = Bitmap.createBitmap(mWood, z2, z3, wt, ht);
        pose[z1][0] = (double) (z2);
        pose[z1][1] = (double) (z3);
      }
    }
    mamaoww.recycle();
    // nbre max de cases affichées par balle :
    int parts = (int) (double) Math.floor(((double) parta) / ((double) nombre));
    boolean[] estpris = new boolean[parta];
    for (int g = 0; g < parta; g++)
      estpris[g] = false;
    int compteurpaspris = parta;
    int nbreaajouter = parta - nombre * parts;

    decoupage = new int[nombre][];

    for (int g = 0; g < nombre - 1; g++) {
      int partss = parts;
      if (nbreaajouter > 0) {
        nbreaajouter--;
        partss++;
      }
      decoupage[g] = new int[partss];
      int j = (int) Math.floor(Math.random() * (double) parta);
      for (int h = 0; h < partss; h++) {
        while (estpris[j] == true) {
          j++;
          if (j == parta)
            j = 0;
        }
        estpris[j] = true;
        decoupage[g][h] = j;
        compteurpaspris--;
      }
    }
    decoupage[nombre - 1] = new int[compteurpaspris];
    int j = (int) Math.floor(Math.random() * (double) parta);
    for (int h = 0; h < compteurpaspris; h++) {
      while (estpris[j] == true) {
        j++;
        if (j == parta)
          j = 0;
      }
      estpris[j] = true;
      decoupage[nombre - 1][h] = j;
    }

    // canv.drawBitmap(parterre[nombre], pose[nombre][0], pose[nombre][1],
    // paint);
  }

  public ColorMatrix modifyback() {
    ColorMatrix couleurs = new ColorMatrix();
    float[] colors = new float[] { 1, 0, 0, 0, 0,// g0
            0, 1, 0, 0, 0,// g1
            0, 0, 1, 0, 0,// g2
            0, 0, 0, 1, 0 // g3
    };
    for (int u = 0; u < 3; u++) {
      for (int g = 0; g < 3; g++) {
        if (u == g)
          colors[u + g * 5] = (float) (Math.random());
        else if (u == 4)
          colors[u + g * 5] = (float) (Math.random() * 512.0 - 256.0);
      }
    }
    couleurs.set(colors);
    return couleurs;
  }

  public void onsizechanged(int w, int h) {
    // compute the origin of the screen relative to the origin of
    // the bitmap

    double mHoriz = (double)w;
    double mVert = (double)h ;
    mHoriz2 = mHoriz;
    mVert2 = mVert;

    colonnes = coco * 2;
    lignes = lili * 2;/*
     * if(colonnes*lignes<nombre+nombrem+2){ colonnes=8;
     * lignes=16; }
     */

    conversion = new int[lignes * colonnes];
    maxiconvers = lignes * colonnes;
    for (int jj = 0; jj < maxiconvers; jj++) {
      conversion[jj] = -1;
    }

    scatterize = new boolean[colonnes][lignes];// coordonnées occupées
    for (int ui = 0; ui < colonnes; ui++)
      for (int io = 0; io < lignes; io++)
        scatterize[ui][io] = false;// (ui+1)+carre*io;
    // coordonnées : x/10./2.
    // coordonnées : y/10./2.
    int maax = colonnes * lignes;
    double a, b;
    int limitem, limitec;
    double[] tempo = new double[2];
    murx = new double[maax];
    mury = new double[maax];
    // murxi=new int[nombrem];
    // muryi=new int[nombrem];
    // avamur=new int[nombrem];
    int paftaille = paf.getWidth() / 2;
    int paftailleh = paf.getHeight() / 2;
    int troutaille = trou.getWidth() / 2;
    int troutailleh = trou.getHeight() / 2;
    limitem = 0;// 1
    limitec = 0;// 1
    int c, d, poos;

    for (int jk = 0; jk < maax; jk++) {
      murx[jk] = 5000.0f;
      mury[jk] = 5000.0f;
    }/*
     * for(int jk=0;jk<nombrem;jk++){
     * tempo=changecoordonnees2(lignes,colonnes,limitem,limitec);
     * a=tempo[0]; b=tempo[1]; murxi[jk]=(int)(Math.floor(a*(double)(w)));
     * muryi[jk]=(int)(Math.floor(b*(double)(h)));
     * c=(int)Math.floor(a*(double)(colonnes));
     * d=(int)Math.floor(b*(double)(lignes)); poos=c+d*colonnes;
     * avamur[jk]=poos; conversion[poos]=0;
     * murx[poos]=((double)(a)-0.5f)*mHoriz;
     * mury[poos]=-((double)(b)-0.5f)*mVert; canv.drawBitmap(paf,
     * murxi[jk]-paftaille, muryi[jk]-paftailleh, paint); }
     */

    murpx = new double[maax];
    murpy = new double[maax];
    murpxi = new int[nombrep];
    murpyi = new int[nombrep];
    avapif = new int[nombrep];
    int piftaille = paf2.getWidth() / 2;
    int piftailleh = paf2.getHeight() / 2;
    for (int jk = 0; jk < maax; jk++) {
      murpx[jk] = 5000.0f;
      murpy[jk] = 5000.0f;
    }

    Paint pappaint = new Paint();
    pappaint.setColor(Color.BLACK);
    pappaint.setAlpha(80);
    pappaint.setAntiAlias(true);
    double attirpix = attir;
    double multiplepix = multiple;

    for (int jk = 0; jk < nombrep; jk++) {
      tempo = changecoordonnees2(lignes, colonnes, limitem, limitec);
      if (tempo != null) {
        a = tempo[0];
        b = tempo[1];
        murpxi[jk] = (int) (Math.floor(a * (double) (w)));
        murpyi[jk] = (int) (Math.floor(b * (double) (h)));
        c = (int) Math.floor(a * (double) (colonnes));
        d = (int) Math.floor(b * (double) (lignes));
        poos = c + d * colonnes;
        avapif[jk] = poos;
        conversion[poos] = 2;
        murpx[poos] = ((double) (a)) * mHoriz;
        murpy[poos] = ((double) (b)) * mVert;
        canv.drawCircle(murpxi[jk], murpyi[jk], (float) multiplepix, pappaint);
        canv.drawBitmap(paf2, murpxi[jk] - piftaille, murpyi[jk]
                - piftailleh, paint);
      }
    }

    pointx = new double[maax];
    pointy = new double[maax];
    for (int jk = 0; jk < maax; jk++) {
      pointx[jk] = 5000.0f;
      pointy[jk] = 5000.0f;
    }
    pointxi = new int[nombre];
    pointyi = new int[nombre];
    avatrou = new int[nombre];
    limitem = 0;// 1
    limitec = 0;// 1

    for (int jk = 0; jk < nombre; jk++) {
      tempo = changecoordonnees2(lignes, colonnes, limitem, limitec);
      if (tempo != null) {
        a = tempo[0];
        b = tempo[1];
        pointxi[jk] = (int) (Math.floor(a * (double) w));
        pointyi[jk] = (int) (Math.floor(b * (double) h));
        c = (int) Math.floor(a * (double) (colonnes));
        d = (int) Math.floor(b * (double) (lignes));
        poos = c + d * colonnes;
        avatrou[jk] = poos;
        conversion[poos] = 1;
        pointx[poos] = ((double) (a)) * mHoriz;
        pointy[poos] = ((double) (b)) * mVert;
        canv.drawCircle(pointxi[jk], pointyi[jk], (float) attirpix, pappaint);
        canv.drawBitmap(trou, pointxi[jk] - troutaille, pointyi[jk]
                - troutailleh, paint);
      }
    }
    int maximum = lili * coco;
    libre = new boolean[maximum];
    for (int j = 0; j < maximum; j++)
      libre[j] = true;

    dessain();

    dessingrille2(lili, coco, w, h, mHoriz, mVert);

    if (nwaow > 0) {
      traitementimage(w, h);
    } else {
      bbx = 0.0f;
    }

    paf.recycle();
    trou.recycle();
    paf2.recycle();

  }

  public double[] changecoordonnees2(int lign, int colonn, int limitem,
                                     int limitec) {
    double[] sortie = new double[2];
    // limitem = int depuis le bord
    // limitec = int depuis le centre
    double ligg1 = Math.floor((double) (lign) / 2.0 - (double) (limitec));
    double coll1 = Math.floor((double) (colonn) / 2.0 - (double) (limitec));
    double ligg2 = Math.ceil((double) (lign) / 2.0 + (double) (limitec));
    double coll2 = Math.ceil((double) (colonn) / 2.0 + (double) (limitec));
    int liggg1 = (int) (ligg1);
    int liggg2 = (int) (ligg2);
    int colll1 = (int) (coll1);
    int colll2 = (int) (coll2);
    boolean[][] scatto = new boolean[colonn][lign];
    for (int jkk = 0; jkk < colonn; jkk++) {
      for (int jkl = 0; jkl < lign; jkl++) {
        scatto[jkk][jkl] = scatterize[jkk][jkl];
      }
    }
    // problo!
    for (int jkk = 0; jkk < colonn; jkk++) {
      for (int jkl = 0; jkl < lign; jkl++) {
        if (jkl < limitem || jkl > lign - 1 - limitem || jkk < limitem
                || jkk > colonn - 1 - limitem) {
          scatto[jkk][jkl] = true;
        } else if ((liggg1 <= jkl && jkl <= liggg2)
                && (colll1 <= jkk && jkk <= colll2)) {
          scatto[jkk][jkl] = true;
        }
      }
    }
    boolean yenaplus = true;
    for (int jkk = 0; jkk < colonn; jkk++) {
      for (int jkl = 0; jkl < lign; jkl++) {
        if (scatto[jkk][jkl] == false) {
          yenaplus = false;
        }
      }
    }
    if (yenaplus == true) {
      for (int jkk = 0; jkk < colonn; jkk++) {
        for (int jkl = 0; jkl < lign; jkl++) {
          scatto[jkk][jkl] = scatterize[jkk][jkl];
        }
      }
    }
    yenaplus = true;
    for (int jkk = 0; jkk < colonn; jkk++) {
      for (int jkl = 0; jkl < lign; jkl++) {
        if (scatto[jkk][jkl] == false) {
          yenaplus = false;
        }
      }
    }
    if (yenaplus == true) {
      return null;
    }

    int hj, hk;
    hj = (int) (Math.floor(Math.random() * (double) (colonn)));
    hk = (int) (Math.floor(Math.random() * (double) (lign)));
    while (scatto[hj][hk] == true) {
      hj = (int) (Math.floor(Math.random() * (double) (colonn)));
      hk = (int) (Math.floor(Math.random() * (double) (lign)));
    }

    // colonnes ; lignes
    scatterize[hj][hk] = true;
    sortie[0] = (1.0 / ((double) (colonn))) * ((double) (hj) + 0.5);
    sortie[1] = (1.0 / ((double) (lign))) * ((double) (hk) + 0.5);
    return sortie;
  }

  public void dessingrille2(int li, int co, int w, int h, double mHoriz,
                            double mVert) {
    double lignes = (double) (li);
    double colonnes = (double) (co);
    double width = (double) (w);
    double height = (double) (h);
    double lx = (width / (colonnes));
    double ly = (height / (lignes));
    double lx0 = (mHoriz / (colonnes));
    double ly0 = (mVert / (lignes));
    int maximum = li * co;
    maximur = maximum;
    // indice = x+y*xmax

    double blok = 5000.0f;
    hvmur = new double[maximum][6];
    for (int u = 0; u < maximum; u++) {
      for (int v = 0; v < 6; v++)
        hvmur[u][v] = blok;
    }
    int xi, yi, xf, yf;
    double xi0, yi0, xf0, yf0;
    int i, f;
    int mi=chemin.length - 1;
    Random rr= Gallery.rand;
    double bougex,bougey;
    for (int u = 0; u < mi; u++) {
      xi = chemin[u][0];
      yi = chemin[u][1];
      xf = chemin[u + 1][0];
      yf = chemin[u + 1][1];
      bougex=(rr.nextFloat()-1.0f/2.0f)*incertitude*lx0;
      bougey=(rr.nextFloat()-1.0f/2.0f)*incertitude*ly0;
      xi0 = ((double) (xi) + 0.5f) * lx0;
      yi0 = ((double) (yi) + 0.5f) * ly0;
      xf0 = ((double) (xf) + 0.5f) * lx0;
      yf0 = ((double) (yf) + 0.5f) * ly0;
      i = xi + yi * co;
      f = xf + yf * co;
      if (Math.abs(xf - xi) <= 1 && Math.abs(yf - yi) <= 1) {
        if ((double) Math.random() < completeness) {
          hvmur[i][2]=xi0+bougex;
          hvmur[i][3]=yi0+bougey;
          hvmur[i][4]=(xi0 + xf0) / 2.0f;
          hvmur[i][5]=(yi0 + yf0) / 2.0f;

          hvmur[f][0]=(xi0 + xf0) / 2.0f;
          hvmur[f][1]=(yi0 + yf0) / 2.0f;
          hvmur[f][2]=xf0;
          hvmur[f][3]=yf0;
        }
      }
    }

  }

  public int[][] dessinchemin(int ligns, int colonns, int startx, int starty) {
    int maximum = ligns * colonns;
    int[][] chemint = new int[maximum][2];
    chemint[0][0] = startx;
    chemint[0][1] = starty;
    libre[startx + starty * colonns] = false;
    int stopstopval = -1;
    int x, y, nvalide, randomme, lax, lay;
    for (int u = 0; u < maximum - 1 && stopstopval == -1; u++) {
      x = chemint[u][0];
      y = chemint[u][1];
      nvalide = scan(x, y, ligns, colonns);
      if (nvalide > 0)
        randomme = r5.nextInt(nvalide);
      else {
        randomme = 0;
        stopstopval = u + 2;
      }
      int k = -1;
      boolean fini = false;
      for (int l = 0; l < 4; l++) {
        if (validecoord[l] == true)
          k++;
        if (k == randomme && fini == false) {
          lax = x + coord[l][0];
          lay = y + coord[l][1];
          chemint[u + 1][0] = lax;
          chemint[u + 1][1] = lay;
          libre[lax + lay * colonns] = false;
          fini = true;
        }
      }
    }
    int[][] chemintt = new int[stopstopval][2];
    for (int u = 0; u < stopstopval; u++) {
      chemintt[u][0] = chemint[u][0];
      chemintt[u][1] = chemint[u][1];
    }

    return chemintt;
  }

  public void dessain() {
    int[][] chemint;
    int cococo = coco;
    int lilili = lili;
    int mxm = cococo * lilili;
    int[][] chemintt = new int[mxm * 2][2];
    boolean pasfini = true;
    int taillealafin = 0;
    int mxmlim = (mxm * 9) / 10;
    int startx = 0;
    int starty = 0;
    while (pasfini == true) {
      boolean librecase = false;
      while (librecase == false) {
        startx = (int) (Math.floor(Math.random() * (double) (cococo)));
        starty = (int) (Math.floor(Math.random() * (double) (lilili)));
        int resuldessain = startx + starty * cococo;
        librecase = libre[resuldessain];
      }
      chemint = dessinchemin(lilili, cococo, startx, starty);
      int taille = chemint.length;
      for (int u = 0; u < taille; u++) {
        int v = taillealafin + u;
        if (v < mxm) {
          chemintt[v][0] = chemint[u][0];
          chemintt[v][1] = chemint[u][1];
        }
      }

      taillealafin += taille;

      if (taillealafin >= mxmlim)
        pasfini = false;
    }

    // llog.d("node","mxmlim="+mxmlim+" < taillealafin="+taillealafin+" < mxm=lili*coco="+mxm);

    chemin = new int[taillealafin][2];
    for (int u = 0; u < taillealafin; u++) {
      chemin[u][0] = chemintt[u][0];
      chemin[u][1] = chemintt[u][1];
    }

  }

  public int scan(int x, int y, int ligns, int colonns) {
    int nvalide = 0;
    int[] pondere = new int[] { 0, 0, 0, 0 };
    boolean[] libreapres = new boolean[] { false, false, false, false };
    validecoord = new boolean[] { false, false, false, false };
    int nblibres = -1;
    int min = 5;
    int max = -5;
    for (int l = 0; l < 4; l++) {
      int lax = x + coord[l][0];
      int lay = y + coord[l][1];
      if ((0 <= lax && lax < colonns) && (0 <= lay && lay < ligns)) {
        if (libre[lax + lay * colonns] == true) {
          nblibres = nscan(lax, lay, ligns, colonns);
          pondere[l] = nblibres;
          if (nblibres == 0)
            validecoord[l] = true;
          else {
            if (nblibres < min)
              min = nblibres;
            if (nblibres > max)
              max = nblibres;
          }
        }
      }
      int lax2 = lax + coord[l][0];
      int lay2 = lay + coord[l][1];
      int lax3 = lax + coord[l][0] + coord[l][0];
      int lay3 = lay + coord[l][1] + coord[l][1];
      if ((0 <= lax2 && lax2 < colonns) && (0 <= lay2 && lay2 < ligns)
              && (0 <= lax3 && lax3 < colonns)
              && (0 <= lay3 && lay3 < ligns)) {
        if (libre[lax2 + lay2 * colonns] == true
                && libre[lax3 + lay3 * colonns] == true) {
          libreapres[l] = true;
        }
      }
    }
    if (nblibres >= 0) {
      for (int l = 0; l < 4; l++) {
        if (pondere[l] == min) {
          validecoord[l] = libreapres[l];
          if (validecoord[l] == true)
            nvalide++;
        }
      }
      if (nvalide == 0) {
        for (int l = 0; l < 4; l++) {
          if (pondere[l] == min) {
            validecoord[l] = true;
            nvalide++;
          }
        }
        if (nvalide == 0) {
          for (int l = 0; l < 4; l++) {
            if (pondere[l] > min) {
              validecoord[l] = libreapres[l];
              if (validecoord[l] == true)
                nvalide++;
            }
          }
          if (nvalide == 0) {
            for (int l = 0; l < 4; l++) {
              if (pondere[l] > min) {
                validecoord[l] = true;
                nvalide++;
              }
            }
          }
        }
      }
    }
    return nvalide;
  }

  public int nscan(int x, int y, int ligns, int colonns) {
    int nvalide = 0;
    for (int l = 0; l < 4; l++) {
      int lax = x + coord[l][0];
      int lay = y + coord[l][1];
      if ((0 <= lax && lax < colonns) && (0 <= lay && lay < ligns)) {
        if (libre[lax + lay * colonns] == true) {
          nvalide++;
        }
      }
    }
    return nvalide;
  }

  class SimulationView extends View {
    private ParticleSystem mParticleSystem;
    private Particle courante;
		/*
		private long tempsici=0,tempssauve;
		private int nbtemps=0;*/

    final double sbb=sBallDiameter/2.0f;
    final double xcp = ((double)largeur) / 2.0f;
    final double ycp = ((double)hauteur) / 2.0f;
    final Bitmap bitmap, bitmap1;
    private Paint nopainttext, nopainttext2, nopainttext3;
    private final int taille = 25;
    private final double diam=sBallDiameter*aide;

    double x, y;

    public SimulationView(Context context) {
      super(context);


      mParticleSystem = new ParticleSystem();
      bitmap = Bitmap.createBitmap(mBitmap);
      bitmap1 = Bitmap.createBitmap(mBitmap, 0, 0, mBitmap.getWidth(),
              mBitmap.getHeight());
      Paint couleuvre = new Paint();
      ColorMatrix filtre = new ColorMatrix();
      float[] matrice = filtre.getArray();
      float n = 256.0f - 123.0f;
      matrice[14] = n;
      ColorMatrixColorFilter mat = new ColorMatrixColorFilter(matrice);
      couleuvre.setColorFilter(mat);
      couleuvre.setAntiAlias(true);
      Canvas calvi = new Canvas(bitmap1);
      calvi.drawBitmap(mBitmap, 0.0f, 0.0f, couleuvre);
      mBitmap.recycle();
      nopainttext = new Paint();
      nopainttext.setTextAlign(Paint.Align.CENTER);
      nopainttext.setColor(prefcouleur);
      nopainttext.setTextSize(taille);
      nopainttext.setShadowLayer(4.0f, 1.0f, 1.0f, Color.BLACK);
      nopainttext.setTypeface(Typeface.MONOSPACE);
      nopainttext.setAntiAlias(true);
      nopainttext.setStyle(Paint.Style.STROKE);
      nopainttext2 = new Paint();
      nopainttext2.setStrokeWidth(2.0f);
      nopainttext2.setColor(Color.CYAN);
      nopainttext2.setStyle(Paint.Style.STROKE);
      nopainttext3 = new Paint();
      nopainttext3.setStrokeWidth(3.0f);
      nopainttext3.setColor(Color.BLUE);
      nopainttext3.setStyle(Paint.Style.STROKE);
      isrunning=true;

      new Thread(new Runnable(){
        public void run(){
          Bitmap mWood3 = Bitmap.createBitmap(mWood);
          double ecart = (double) (debutbits[0].getWidth()) * 0.5f;
          double recul = ycp / 2.0f;
          double xdd = xcp - ecart;
          double ydd = ycp - ecart;
          /*int tempsp = 6;
          int tempsd = 0;
          while (tempsd < tempsp && isrunning) {
            try {
              Thread.sleep(200);
            } catch (InterruptedException e) {
            }
            switch (tempsd) {
              case 0:
                canv.drawBitmap(debutbits[0], xdd, ydd + recul, null);
                break;
              case 1:
                canv.drawBitmap(debutbits[1], xdd, ydd + recul, null);
                break;
              case 2:
                canv.drawBitmap(debutbits[0], xdd, ydd, null);
                break;
              case 3:
                canv.drawBitmap(debutbits[0], xdd, ydd + recul, null);
                canv.drawBitmap(debutbits[1], xdd, ydd, null);
                break;
              case 4:
                canv.drawBitmap(debutbits[0], xdd, ydd - recul, null);
                break;
              case 5:
                canv.drawBitmap(debutbits[0], xdd, ydd, null);
                canv.drawBitmap(debutbits[1], xdd, ydd - recul, null);
                break;
              default:
                break;
            }
            tempsd++;
          }*/

          canv.drawBitmap(mWood3, 0.0f, 0.0f, null);
          mWood3.recycle();
          if (isrunning == true)
            isreallyrunning = true;
        }
      }).start();


      surfaceCreated();
    }

    public void drawblack(Canvas c) {
      c.drawColor(Color.BLACK);
    }

    public void drawinitial(Canvas c) {
      c.drawBitmap(mWood, 0.0f, 0.0f, null);
      for (int o = 0; o < nombre; o++) {
        courante = mBalls[o];//
        x = courante.mPosX-sbb;
        y = courante.mPosY-sbb;
        c.drawBitmap(bitmap, (float) x, (float) y, null);
      }
      if (tourne)
        c.drawText("Please turn screen ---->", largeur / 2.0f, taille,
                nopainttext);
      c.drawText("Menu : Block balls", largeur / 2.0f, 2.0f * taille,
              nopainttext);
      c.drawText("Search : Set inclination", largeur / 2.0f,
              3.0f * taille, nopainttext);
      c.drawText("Touch screen to bump ball", largeur / 2.0f,
              4.0f * taille, nopainttext);
    }

    public void drawupdate(Canvas c) {
      c.drawBitmap(mWood, 0.0f, 0.0f, null);
      if (drawschlang[2] != -1.0f) {
        if (touche2[0] == -1.0f) {
          c.drawLine((float) drawschlang[0], (float) drawschlang[1], (float) drawschlang[2],
                  (float) drawschlang[3], nopainttext3);
          c.drawLine((float) drawschlang[0], (float) drawschlang[1], (float) drawschlang[2],
                  (float) drawschlang[3], nopainttext2);

          int i = cellecila;
          courante = mBalls[i];
          x = courante.mPosX ;
          y = courante.mPosY ;
          c.drawCircle((float) x, (float) y, (float) diam, nopainttext3);
        }
      }
      for (int o = 0; o < nombre; o++) {
        courante = mBalls[o];//
        x = courante.mPosX-sbb;
        y = courante.mPosY-sbb;
        if (courante.couleuris == 0)
          c.drawBitmap(bitmap, (float) x, (float) y, null);
        else{
          c.drawBitmap(bitmap1, (float) x, (float) y, null);
        }
      }
      if (evenement)
        c.drawBitmap(debutbits[2], (float) blocktimexy[0], (float) blocktimexy[1], null);
      int allezvite=maxicola-scaure;
      String allez;
      if(allezvite<0){
        allezvite=-allezvite;
        allez="+"+allezvite;
      }
      else if (allezvite>1){
        allez="";
        int dedix=(int)Math.floor(((double)allezvite)/10.0f);
        int reste=allezvite-dedix*10;
        for(int plok=0;plok<dedix;plok++)
          allez+="*";//"encore "+allezvite+" coups";
        for(int plok=0;plok<reste;plok++)
          allez+="o";//"encore "+allezvite+" coups";
      }
      else if (allezvite==1)
        allez="plus qu'un coup !";
      else
        allez="c'est le meilleur score";
      c.drawText(allez, (float) xcp, 30.0f, nopainttext);
    }

    public void surfaceChanged(SurfaceHolder arg0, int arg1, int arg2,
                               int arg3) {

    }

    public void surfaceCreated() {
      mParticleSystem.setRunning(true);
      mParticleSystem.start();
    }

    public void surfaceDestroyed() {
      boolean retry1 = true;
      mParticleSystem.setRunning(false);
      while (retry1) {
        try {
          mParticleSystem.join();
          retry1 = false;
        } catch (InterruptedException e) {
        }
      }
      bitmap.recycle();
      bitmap1.recycle();
      mWood.recycle();
      debutbits[0].recycle();
      debutbits[1].recycle();
      debutbits[2].recycle();
      if (nwaow > 0) {
        for (int kl = 0; kl < parterre.length; kl++)
          parterre[kl].recycle();
        for (int kl = 0; kl < pasterre.length; kl++)
          pasterre[kl].recycle();
      }
    }

    @Override
    public void onDraw(Canvas c){
      if(isrunning && !isreallyrunning){
        drawinitial(c);
      } else if (isrunning && isreallyrunning) {
        if (nwaow > 0) {
          for (int ev = 0; ev < nombre; ev++) {
            int tnat=decoupage[ev].length;
            if (evene[ev][1] == true) {
              for (int t = 0; t < tnat; t++) {
                int fafa = decoupage[ev][t];
                canv.drawBitmap(parterre[fafa], (float) pose[fafa][0],
                        (float) pose[fafa][1], null);
              }
              evene[ev][1] = false;
            }
            if (evene[ev][0] == true) {
              for (int t = 0; t < tnat; t++) {
                int fafa = decoupage[ev][t];
                canv.drawBitmap(pasterre[fafa], (float) pose[fafa][0],
                        (float) pose[fafa][1], null);
              }
              evene[ev][0] = false;
            }
          }
        }
        drawupdate(c);/*
				nbtemps++;
				long tempsla=System.nanoTime();
				if(nbtemps>1)
					tempsici+=tempsla-tempssauve;
				tempssauve=System.nanoTime();*/
      }else if (!isrunning && !isreallyrunning) {
        drawblack(c);/*
				double moyenne=((double)(nbtemps-1))/(((double)tempsici)/1000000000.0f);
				llog.d("node",""+moyenne);*/
        surfaceDestroyed();
      }
      invalidate();
    }
  }

  class ParticleSystem extends Thread {// implements SensorEventListener{
    private boolean _run = false;
    double ecart;
    double xdd;
    double ydd;
    double increment;
    int cadureunpeu;
    double calala;
    double calala2;
    int cptenbreplaces=0;

    public void setRunning(boolean run) {
      _run = run;
    }

    public void placedepart() {
      double sballdia = sBallDiameter * 1.01f;
      Particle ball = mBalls[0];
      double[] repre = { mHoriz2/2.0f, sballdia * 3.0f };
      ball.mPosX = repre[0];
      ball.mPosY = repre[1];
      int N = nombre;
      int Ne = 1;
      while (N > 0) {
        N = N - Ne;
        Ne++;
      }
      Ne--;
      repre[1] += sballdia * Math.sqrt(3.0f) / 2.0f;
      repre[0] -= sballdia / 2.0f;
      // Ne = nombre d'étages !
      int etage = 1;
      int nbdsetagemax = etage + 1;
      int nbdsetage = 0;
      for (int o = 1; o < nombre; o++) {
        ball = mBalls[o];
        nbdsetage++;
        ball.mPosX = repre[0] + (nbdsetage - 1) * sballdia;
        ball.mPosY = repre[1];

        if (nbdsetage == nbdsetagemax) {
          etage++;
          nbdsetagemax = etage + 1;
          nbdsetage = 0;
          repre[1] += sballdia * Math.sqrt(3.0f) / 2.0f;
          repre[0] -= sballdia / 2.0f;
        }
      }
    }

    public void multiArrayCopy(double[][] source, double[][] destination) {
      for (int a = 0; a < source.length; a++) {
        System.arraycopy(source[a], 0, destination[a], 0,
                source[a].length);
      }
    }

    @Override
    public void run() {

      while (!isrunning) {
        try {
          Thread.sleep(400);
        } catch (InterruptedException e) {
          // TODO Auto-generated catch block
          e.printStackTrace();
        }
      }

      placedepart();

      while (isrunning && !isreallyrunning) {
        try {
          Thread.sleep(tempscalcul);
        } catch (InterruptedException e) {
          // TODO Auto-generated catch block
          e.printStackTrace();
        }
        // chocs interbilles et bords
        update();
        // calculs physiques
        // updatepositions();
      }
      ecart = (double) (debutbits[2].getWidth()) * 0.5f;
      xdd = largeur/2.0f - ecart;
      ydd = hauteur/2.0f - ecart;
      increment = xdd / glurp;
      blocktimexy[0] = xdd;
      blocktimexy[1] = ydd;
      cadureunpeu = 0;
      calala = 0.0f;
      calala2 = 0.0f;

      while (isreallyrunning) {
        try {
          Thread.sleep(tempscalcul);
        } catch (InterruptedException e) {
          // TODO Auto-generated catch block
          e.printStackTrace();
        }
        // calculs physiques
        updatepositions();
        // chocs interbilles et bords
        update();
        // chocs avec les murs
        verifmurs2();
        // chocs avec les pièges
        pieges();
        // on ejecte une balle
        ejection();
      }// while toutes les 15ms

      onStop();
    }

    private void updatepositions() {
      for (int i = 0; i < nombre && isreallyrunning; i++) {
        updatePositions(i);
      }
    }

    private void pieges() {

      int nbreplaces = nombre;
      for (int i = 0; i < nombre && isreallyrunning; i++) {

        Particle courante = mBalls[i];
        double xf = courante.mPosX;
        double yf = courante.mPosY;
        boolean plassee = false;
        double accx = courante.mVitX;
        double accy = courante.mVitY;

        int laxi, layi, jk;
        double laxf, layf;
        laxf = ((xf ) * ((double) colonnes)) / mHoriz2;
        laxi = (int) (Math.floor(laxf));
        layf = ((yf ) * ((double) lignes)) / mVert2;
        layi = (int) (Math.floor(layf));
        jk = laxi + layi * colonnes;

        if (0 <= jk && jk < maxiconvers) {
          double mur = attir;
          double o, p;
          int converse = conversion[jk];
          if (converse == 1) {// 1 : endroit où placer bille
            o = pointx[jk];
            if (o != 5000.0f) {
              p = pointy[jk];
              if(o!=5000.0f){
                p=pointy[jk];

                final double erx=o-xf;
                final double ery=p-yf;
                double erx2=erx*erx;
                double ery2=ery*ery;
                double er2=erx2+ery2;
                double er=(double) Math.sqrt(er2);
                double prealpha=courante.mAng;
                double alpha=0.0f;
                if(er<mur){
                  courante.moneminus=limaccel;
                  double dvex,dvey;
                  if(er>quatredixieme){
                    alpha=ballz.atanderivgauss(-1.0f,er,mur);
                    double alphagood=(alpha+prealpha)/2.0f;
                    double cste=bums*(double) Math.sin(alphagood);
                    dvex=(erx*cste)/er;
                    dvey=(ery*cste)/er;/*
					  					double vitreelle=Math.sqrt(accx*accx+accy*accy);
					  					double dvitreelle=Math.sqrt(dvex*dvex+dvey*dvey);
					  					String ajoutons=xf+"\t"+yf+"\t"+accx+"\t"+accy+"\t185AA826\t\t"+
					  					courante.mLastT+"\t"+vitreelle+"\t"+dvitreelle+"\r\n";
					  					eralpha.add(ajoutons);*/
                    courante.mdVitX += dvex;
                    courante.mdVitY += dvey;
                  }else{
                    if (Math.abs(accx) < troisdixieme
                            && Math.abs(accy) < troisdixieme) {
                      nbreplaces--;
                      plassee = true;
                      xf = o;
                      yf = p;
                      accx = 0.0f;
                      accy = 0.0f;
                      mBalls[cellecila].defautcouleur();
                      for (int pop = 0; pop < nombre; pop++) {
                        if (mBalls[pop].plassee == false) {
                          cellecila = pop;
                          mBalls[cellecila].changecouleur();
                          break;
                        }
                      }
                    }
                  }
                }
                courante.mOlAng=prealpha;
                courante.mAng=alpha;
  			  					/*
								if (Math.abs(accx) < limaccel
										&& Math.abs(accy) < limaccel) {
									nbreplaces--;
									plassee = true;
									xf = o;
									yf = p;
									accx = 0.0f;
									accy = 0.0f;
									mBalls[cellecila].defautcouleur();
									for (int pop = 0; pop < nombre; pop++) {
										if (mBalls[pop].plassee == false) {
											cellecila = pop;
											mBalls[cellecila].changecouleur();
											break;
										}
									}
								}
								else {
									double[] vectf = replassagecoinapres(xf, yf, o, p, accx, accy);
									xf = vectf[0];
									yf = vectf[1];
									}*/

              }
            }
          }/*
           * else if(converse==0){//0 : teleportage int cluila=-1;
           * mur=multiple; o=murx[jk]; if(o!=5000.0f){ p=mury[jk];
           * if(Math.abs(xf-o)<mur && Math.abs(yf-p)<mur){ cluila =
           * (int)(Math.floor(Math.random()*(double)(nombrem)));
           * xf=murx[avamur[cluila]]+macx*mur;
           * yf=mury[avamur[cluila]]+macy*mur; xi=xf-acx; yi=yf-acy; }
           * } }
           */
          else if (converse == 2) {// 2 : croix
            mur = multiple;
            o = murpx[jk];
            if (o != 5000.0f) {
              p = murpy[jk];
              if (Math.abs(xf - o) < mur
                      && Math.abs(yf - p) < mur) {
                cadureunpeu++;
                if (cadureunpeu == 1) {

                  if (accx == 0.0f && accy == 0.0f)
                    accx = 0.004f;
                  double ratt=2.0f;
                  areduire+=(Math.abs(accx)+Math.abs(accy))*ratt;
                  accx *= ratt;
                  accy *= ratt;

                  blocktimexy[0] = xdd;
                  blocktimexy[1] = ydd;
                  double aupif = (Math.floor(Math.random() * 3.0) / 10.0) - 0.1;
                  calala = (double) (aupif) * mulipli;
                  aupif = (Math.floor(Math.random() * 3.0) / 10.0) - 0.1;
                  calala2 = (double) (aupif) * mulipli;
                  calala = calala / glurp;
                  calala2 = calala2 / glurp;
                  //scorr2++;
                  //scaure++;
                }
              }
            }
          }
        }
        courante.mPosX = xf;
        courante.mPosY = yf;
        courante.mVitX = accx;
        courante.mVitY = accy;
        // courante.computePhysics();
        if (nwaow > 0) {
          if (courante.plassee != plassee) {
            if (plassee) {
              evene[i][1] = true;
            } else {
              evene[i][0] = true;
              scorr++;
            }
          }
        }
        courante.plassee = plassee;

      }// for boucle sur chaque particule

      if (cadureunpeu > 0) {
        if (cadureunpeu < (int) glurp) {
          bbx += calala;
          bby += calala2;
          blocktimexy[0] -= increment * Math.signum(calala);
          blocktimexy[1] += increment * Math.signum(calala2);
          evenement = true;
          cadureunpeu++;
        } else {
          cadureunpeu = 0;
          evenement = false;
        }
      }

      if (nbreplaces == 0) {
        cptenbreplaces++;
        if(cptenbreplaces==150)
          cestfini();
      }else{
        cptenbreplaces=0;
      }
    }

    private void ejection() {
      if (touche2[0] != -1.0f && isreallyrunning) {
        drawschlang[0] = touche[0];
        drawschlang[1] = touche[1];
        drawschlang[2] = touche2[0];
        drawschlang[3] = touche2[1];

        final double touche0 = touche[0];
        final double touche1 = touche[1];
        final double touche20 = touche2[0];
        final double touche21 = touche2[1];

        double vectxa = touche20 - touche0;
        double vectxb = touche21 - touche1;
        double vectx = vectxa * ballibump;
        double vecty = vectxb * ballibump;

        int i = cellecila;
        Particle courante = mBalls[i];
        // oulala yen a pas comprendu le chosien là !
        double xf = courante.mPosX ;
        double yf = courante.mPosY ;/*
				drawschlang[4] = xc + xf * xs;
				drawschlang[5] = yc - yf * ys;*/

        //double dminx=touche20-xf;
        //double dminy=touche21-yf;
        //double dminx0=touche0-xf;
        //double dminy0=touche1-yf;
        //double signx0=Math.signum(dminx0);
        //double signy0=Math.signum(dminy0);
        //double signx=Math.signum(dminx);
        //double signy=Math.signum(dminy);
        //double dmin=dminx*dminx+dminy*dminy;
        //dmin=Math.sqrt(dmin);
        //if(dmin>0.01f && signx0==signx && signy0==signy){
        double[] secc = secante(xf, yf, touche0, touche1, vectx, vecty);
        if (secc != null) {
          double[] vectvf = spheresecante(xf, yf, secc[0], secc[1],
                  vectx, vecty);
          if (vectvf != null) {
							/*
							double ocx=secc[0]-xf;
							double ocy=secc[1]-yf;
							drawschlang[6] = xc + secc[0] * xs;
							drawschlang[7] = yc - secc[1] * ys;
							drawschlang[8] = xc + vectvf[0] * xs;
							drawschlang[9] = yc - vectvf[1] * ys;
							double omega=(ocx*vay-ocy*vax)/(sBallDiameter2/4.0f);
							double inertia=2.0f*sBallDiameter2/(5.0f*4.0f);
							double ecin=inertia * omega * omega;
							if(Math.abs(vax)<0.000001f){
								vay=Math.sqrt(vay-ecin);
							}else{
								double alph=vay/vax;
								ecin=ecin/(1.0f*alph*alph);
								vax=Math.sqrt(vax*vax-ecin);
								vay=alph*vax;
							}
							courante.mAng += omega;*/
            double vax = vectvf[0] - secc[0];
            double vay = vectvf[1] - secc[1];
            courante.mVitX += vax;
            courante.mVitY += vay;
            scaure++;
          } else {
            //double vax = secc[0] - touche0;
            //double vay = secc[1] - touche1;
            courante.mVitX += vectx;
            courante.mVitY += vecty;
            scaure++;
            //llog.d(TAG, "vectvf");
          }
        }
        //}
        touche2[0] = -1.0f;
        drawschlang[2] = -1.0f;
      }
    }

    private void updatePositions(int baballe) {
      Particle ball = mBalls[baballe];
      ball.computePhysics();
    }

    private double[] secante(double x0, double y0, double xi, double yi,
                            double vxi, double vyi) {
      double x = 0.0f;
      double y = 0.0f;
      double a, b, c, alpha, beta, delta;
      if (vxi == 0.0f) {
        x = xi;
        a = 1.0f;
        b = -2.0f * y0;
        c = x * x + y0 * y0 + x0 * x0 - 2.0f * x * x0 - sBallDiameter2 *aide*aide;///4 !!!!
        delta = b * b - 4.0f * a * c;
        if (delta < 0.0f) {
          return null;
        } else if (delta == 0.0f) {
          y = -b / (2.0f * a);
        } else {
          double y1, y2;
          delta = (double) Math.sqrt(delta);
          y1 = (-b - delta) / (2.0f * a);
          y2 = (-b + delta) / (2.0f * a);
          double dee1 = (yi - y1) * (yi - y1);
          double dee2 = (yi - y2) * (yi - y2);
          if (dee1 < dee2) {
            y = y1;
          } else {
            y = y2;
          }
        }
      } else {
        alpha = vyi / vxi;
        beta = yi - alpha * xi;
        a = alpha * alpha + 1.0f;
        b = 2.0f * alpha * beta - 2.0f * alpha * y0 - 2.0f * x0;
        c = x0 * x0 + y0 * y0 - sBallDiameter2 * aide *aide + beta * beta
                - 2.0f * y0 * beta;///4!!!!!
        delta = b * b - 4.0f * a * c;
        if (delta < 0.0f) {
          return null;
        } else if (delta == 0.0f) {
          x = -b / (2.0f * a);
          y = alpha * x + beta;
        } else {
          double x1, x2, y1, y2;
          delta = (double) Math.sqrt(delta);
          x1 = (-b - delta) / (2.0f * a);
          x2 = (-b + delta) / (2.0f * a);
          y1 = alpha * x1 + beta;
          y2 = alpha * x2 + beta;
          double dee1 = (xi - x1) * (xi - x1) + (yi - y1) * (yi - y1);
          double dee2 = (xi - x2) * (xi - x2) + (yi - y2) * (yi - y2);
          if (dee1 < dee2) {
            x = x1;
            y = y1;
          } else {
            x = x2;
            y = y2;
          }
        }
      }
      double[] sec = { x, y };
      if (x != 0.0f && y != 0.0f)
        return sec;
      else
        return null;
    }

    private double[] spheresecante(double x0, double y0, double xa, double ya,
                                  double vxi, double vyi) {
      final double toutpetit = 0.001f;
      double xb = xa + vxi;
      double yb = ya + vyi;
      double x = 0.0f;
      double y = 0.0f;
      double a, b, c, alpha, beta, delta;
      if (xa == x0) {
        x = xa;
        a = 1.0f;
        b = -2.0f * ya;
        c = ya * ya + (xa - xb) * (xa - xb) - vxi * vxi - vyi * vyi;
        delta = b * b - 4.0f * a * c;
        if (delta < 0.0f) {
          return null;
        } else if (delta == 0.0f) {
          y = -b / (2.0f * a);
        } else {
          double y1, y2;
          delta = (double) Math.sqrt(delta);
          y1 = (-b - delta) / (2.0f * a);
          y2 = (-b + delta) / (2.0f * a);
          if (Math.abs(y2 - ya) < toutpetit) {
            y = y1;
          } else if (Math.abs(y1 - ya) < toutpetit) {
            y = y2;
          } else {
            return null;
          }
        }
      } else {
        alpha = (ya - y0) / (xa - x0);
        beta = ya - alpha * xa;
        a = 2.0f * alpha * alpha + 2.0f;
        b = 2.0f * (2.0f * alpha * beta - alpha * ya - alpha * yb - xa - xb);
        c = (ya - beta) * (ya - beta) + (yb - beta) * (yb - beta) + xa
                * xa + xb * xb - vxi * vxi - vyi * vyi;
        delta = b * b - 4.0f * a * c;
        if (delta < 0.0f) {
          return null;
        } else if (delta == 0.0f) {
          x = -b / (2.0f * a);
          y = alpha * x + beta;
        } else {
          double x1, x2, y1, y2;
          delta = (double) Math.sqrt(delta);
          x1 = (-b - delta) / (2.0f * a);
          x2 = (-b + delta) / (2.0f * a);
          y1 = alpha * x1 + beta;
          y2 = alpha * x2 + beta;
          if (Math.abs(x2 - xa) < toutpetit) {
            x = x1;
            y = y1;
          } else if (Math.abs(x1 - xa) < toutpetit) {
            x = x2;
            y = y2;
          } else {
            return null;
          }
        }
      }
      double[] sec = { x, y };
      return sec;
    }

    public void update() {
      final int count = nombre;
      for (int i = 0; i < count; i++) {
        Particle curr = mBalls[i];
        for (int j = i + 1; j < count; j++) {
          Particle ball = mBalls[j];
          double x1 = ball.mPosX;
          double y1 = ball.mPosY;
          double x2 = curr.mPosX;
          double y2 = curr.mPosY;
          double alpha1 = ball.mAng;
          double alpha2 = curr.mAng;
          double vudehaut=(double) Math.cos((alpha1+alpha2)/2.0f);
          vudehaut*=vudehaut;
          double dx = x1-x2;
          double dy = y1-y2;
          double d2 = dx*dx+dy*dy;
          if (d2 < vudehaut*sBallDiameter2) {
            double v1x = ball.mVitX;
            double v1y = ball.mVitY;
            double v2x = curr.mVitX;
            double v2y = curr.mVitY;
            double a1x = ball.mAccelX;
            double a1y = ball.mAccelY;
            double a2x = curr.mAccelX;
            double a2y = curr.mAccelY;
            double f1 = ball.mLastoneminus*2.0f;
            double f2 = curr.mLastoneminus*2.0f;
            double[] vectp = ballz.collision(sBallDiameter,
                    false, alpha1, f1, x1, y1, v1x, v1y, a1x, a1y,
                    false, alpha2, f2, x2, y2, v2x, v2y, a2x, a2y);
            if(vectp!=null){
              ball.mPosX = vectp[0];
              ball.mPosY = vectp[1];
              curr.mPosX = vectp[2];
              curr.mPosY = vectp[3];
              ball.mVitX = vectp[4];
              ball.mVitY = vectp[5];
              curr.mVitX = vectp[6];
              curr.mVitY = vectp[7];
            }
          }
        }
        curr.resolveCollisionWithBounds();
      }

    }

    public void verifmurs2() {
      final int count = nombre;
      final double demil = sBallDiameter / 2.0f;
      for (int i = 0; i < count; i++) {
        Particle courante = mBalls[i];
        double xf = courante.mPosX;
        double yf = courante.mPosY;
        double laxf = ((xf ) * ((double) coco)) / mHoriz2;
        int laxi = (int) (Math.floor(laxf));
        double layf = ((yf ) * ((double) lili)) / mVert2;
        int layi = (int) (Math.floor(layf));
        int jk = laxi + layi * coco;
        if (0 <= jk && jk < maximur) {
          double ox1 = hvmur[jk][0];
          double oy1 = hvmur[jk][1];
          double ox2 = hvmur[jk][2];
          double oy2 = hvmur[jk][3];
          double ox3 = hvmur[jk][4];
          double oy3 = hvmur[jk][5];
          double v1x = courante.mVitX;
          double v1y = courante.mVitY;
          boolean done=false;
          if (ox1 != 5000.0f && ox2!=5000.0f) {
            double[] p2=ballz.hifisinrange(ox1,oy1,ox2,oy2,xf,yf,demil);
            if(p2!=null){
              double[] vectp = ballz.replassage(demil,
                      false, xf, yf, v1x, v1y,
                      true, p2[0], p2[1], 0.0, 0.0);
              if(vectp!=null){
                courante.mPosX = (double)vectp[0];
                courante.mPosY = (double)vectp[1];
                courante.mVitX = (double)vectp[4];
                courante.mVitY = (double)vectp[5];
                done=true;
              }
            }
          }
          if (ox2 != 5000.0f && ox3 != 5000.0f && !done) {
            double[] p2=ballz.hifisinrange(ox2,oy2,ox3,oy3,xf,yf,demil);
            if(p2!=null){
              double[] vectp = ballz.replassage(demil,
                      false, xf, yf, v1x, v1y,
                      true, p2[0], p2[1], 0.0, 0.0);
              if(vectp!=null){
                courante.mPosX = (double)vectp[0];
                courante.mPosY = (double)vectp[1];
                courante.mVitX = (double)vectp[4];
                courante.mVitY = (double)vectp[5];
                done=true;
              }
            }
          }
          if(ox1!=5000.0f && !done){
            double px1=xf-ox1;
            double py1=yf-oy1;
            double p12=px1*px1+py1*py1;
            double demil2=demil*demil;
            if(p12<=demil2){
              double[] vectp = ballz.replassage(demil,
                      false, xf, yf, v1x, v1y,
                      true, ox1, oy1, 0.0, 0.0);
              if(vectp!=null){
                courante.mPosX = (double)vectp[0];
                courante.mPosY = (double)vectp[1];
                courante.mVitX = (double)vectp[4];
                courante.mVitY = (double)vectp[5];
                done=true;
              }
            }
          }
          if(ox2!=5000.0f && !done){
            double px1=xf-ox2;
            double py1=yf-oy2;
            double p12=px1*px1+py1*py1;
            double demil2=demil*demil;
            if(p12<=demil2){
              double[] vectp = ballz.replassage(demil,
                      false, xf, yf, v1x, v1y,
                      true, ox2, oy2, 0.0, 0.0);
              if(vectp!=null){
                courante.mPosX = (double)vectp[0];
                courante.mPosY = (double)vectp[1];
                courante.mVitX = (double)vectp[4];
                courante.mVitY = (double)vectp[5];
                done=true;
              }
            }
          }
          if(ox3!=5000.0f && !done){
            double px1=xf-ox3;
            double py1=yf-oy3;
            double p12=px1*px1+py1*py1;
            double demil2=demil*demil;
            if(p12<=demil2){
              double[] vectp = ballz.replassage(demil,
                      false, xf, yf, v1x, v1y,
                      true, ox3, oy3, 0.0, 0.0);
              if(vectp!=null){
                courante.mPosX = (double)vectp[0];
                courante.mPosY = (double)vectp[1];
                courante.mVitX = (double)vectp[4];
                courante.mVitY = (double)vectp[5];
                done=true;
              }
            }
          }
        }
      }
    }

    public void cestfini() {
      isrunning = false;
      isreallyrunning = false;
      if (nwaow > 0) {
        nwaow = 0;
        //zzz.start();
      }
      nwaow = 0;/*
       * int resss=nombre*19+coco*16+nombrep*15+dif1; int
       * resultaa=resss-scorr*18-scorr2*26; if(resultaa<0)
       * resultaa=3; String resulta= "Score : "+resultaa
       * +"\n="+resss +" - "+scorr+" lost balls"
       * +" - "+scorr2+" (x)"; Uri outUri=Uri.parse(resulta);
       * Intent outData=new Intent(); outData.setData(outUri);
       * setResult(Activity.RESULT_OK,outData);complet
       */

      int difficulty = (int)(((double)(coco * lili)) * completeness);
      int[] resulta = { nombre, scaure, difficulty, nbums };
      // Uri outUri=Uri.parse(resulta);
      /*Intent outData = new Intent();
      outData.putExtra("resultate", resulta);
      setResult(Activity.RESULT_OK, outData);*/

      Intent intent = new Intent();
      intent.setAction(Gallery.broadcastname);
      intent.putExtra("goal", "startbrowser");
      intent.putExtra("id", myid);
      LocalBroadcastManager.getInstance(model.activitycontext).sendBroadcast(intent);

    }
  }

  class Particle {
    private volatile long mLastT;
    private double olang=0.0f;
    private double ang=0.0f;
    private volatile double mOlAng=0.0f;
    private volatile double mAng=0.0f;
    private volatile double mPosX;
    private volatile double mPosY;
    private volatile double mVitX=0.0f;
    private volatile double mVitY=0.0f;
    private volatile double mAccelX=0.0f;
    private volatile double mAccelY=0.0f;
    private volatile double mdVitX=0.0f;
    private volatile double mdVitY=0.0f;
    private volatile int couleuris = 0;
    private volatile double moneminus = sFriction;
    private volatile double mLastoneminus = sFriction;
    private volatile boolean plassee = false;

    Particle() {
      mLastT = System.nanoTime();
      //mAng=0.0f;
      couleuris = 0;
    }

    public void defautcouleur() {
      couleuris = 0;
    }

    public void changecouleur() {
      couleuris = 1;
    }

    public void computePhysics() {
      //defaut
      limitercela();

      final long tnow = System.nanoTime();
      final double dt = (double) (tnow - mLastT) * 0.000000001f;
      mLastT = tnow;

      olang=ang;
      ang=0.0f;
      for(int bv=0;bv<nbums;bv++){
        final double erx=electrique[bv][0]-mPosX;
        final double ery=electrique[bv][1]-mPosY;
        double erx2=erx*erx;
        double ery2=ery*ery;
        double er2=erx2+ery2;
        double er=(double) Math.sqrt(er2);
        final double dvex,dvey;
        if(er<electrique[bv][2] && er>cinqdixmillieme){
          ang=ballz.atanderivgauss(1.0f,er,electrique[bv][2]);
          //double angmoy=(ang+olang)/2.0f;
          double cste=bumsh*(double) Math.sin(ang);
          dvex=(erx*cste)/er;
          dvey=(ery*cste)/er;
          mdVitX += dvex;
          mdVitY += dvey;
        }
      }

      mLastoneminus=moneminus;
      mAccelX = mdVitX;
      mAccelY = mdVitY;

      final double frottx= mVitX * 2.0f * moneminus * dt;
      final double frotty= mVitY * 2.0f * moneminus * dt;

      final double vxt112 = mVitX - frottx + 0.5f * mAccelX * dt ;
      final double vyt112 = mVitY - frotty + 0.5f * mAccelY * dt ;

      //double angmoy=(ang+olang)/2.0f;
      //double mangmoy=(mAng+mOlAng)/2.0f;
      final double orien = (double) Math.cos(ang) * (double) Math.cos(mAng);

      mPosX = mPosX + orien * vxt112 * dt ;
      mPosY = mPosY + orien * vyt112 * dt ;
      mVitX = vxt112 ;
      mVitY = vyt112 ;

      mdVitX=0.0f;
      mdVitY=0.0f;

      moneminus=sFriction;

			/*
			//avec la vitesse angulaire
			double dx = mPosX2 - mPosX;
			double dy = mPosY2 - mPosY;
			double angleadevier = (double) Math.atan2(dy, dx);
			angleadevier += mAng * dt;
			double rayon = (double) Math.sqrt(dx * dx + dy * dy);
			mPosX += rayon * (double) Math.cos(angleadevier);
			mPosY += rayon * (double) Math.sin(angleadevier);
			*/
    }

    public void limitercela() {
      if(areduire>0.0f){
        final double ondiv=2000.0f;
        final double xf = mVitX/ondiv;
        final double yf = mVitY/ondiv;
        areduire -= (Math.abs(xf)+Math.abs(yf));
        mVitX -= xf;
        mVitY -= yf;
      }
    }

    public void limitercela2() {
      final double xmax = 2.0f*limaccel;// 20cm/s = 7Km/h
      final double ymax = 2.0f*limaccel;
      final double xf = mVitX;
      final double yf = mVitY;
      if(areduire>0.0f){
        double extra;
        if (xf > xmax) {
          extra=Math.abs(mVitX-xmax);
          mVitX = xmax;
          areduire-=extra;
        } else if (xf < -xmax) {
          extra=Math.abs(mVitX-xmax);
          mVitX = -xmax;
          areduire-=extra;
        }
        if (yf > ymax) {
          extra=Math.abs(mVitY-xmax);
          mVitY = ymax;
          areduire-=extra;
        } else if (yf < -ymax) {
          extra=Math.abs(mVitY-xmax);
          mVitY = -ymax;
          areduire-=extra;
        }
      }
    }

    public void correction(boolean vertical, double z0) {
      if (vertical == true) {
        mPosX = z0;
        mVitX = -mVitX;
      } else {
        mPosY = z0;
        mVitY = -mVitY;
      }
      // computePhysics();
    }

    public void resolveCollisionWithBounds() {
      final double deb=(sBallDiameter * 1.0f) / 2.0f;
      final double xmax = mHoriz2 + deb;
      final double ymax = mVert2 + deb;
      final double xf = mPosX;
      final double yf = mPosY;

      if (xf > xmax) {
        correction(true, xmax);
      } else if (xf < -deb) {
        correction(true, -deb);
      }
      if (yf > ymax) {
        correction(false, ymax);
      } else if (yf < -deb) {
        correction(false, -deb);
      }
    }

  }
  
  /**
   * This interface must be implemented by activities that contain this
   * fragment to allow an interaction in this fragment to be communicated
   * to the activity and potentially other fragments contained in that
   * activity.
   * <p>
   * See the Android Training lesson <a href=
   * "http://developer.android.com/training/basics/fragments/communicating.html"
   * >Communicating with Other Fragments</a> for more information.
   */
  
  public OnFragmentInteractionListener fragmentlistener;
  
  public interface OnFragmentInteractionListener {
    // TODO: Update argument type and name
    Integer onFragmentInteraction(Uri uri);
  }
  
  @Override
  public void onAttach(Context context) {
    super.onAttach(context);
    if (context instanceof OnFragmentInteractionListener) {
      fragmentlistener = (OnFragmentInteractionListener) context;
    } else {
      llog.d(TAG, context.toString() + " must implement OnFragmentInteractionListener");
    }
  }
  
  
}






















