package la.daube.photochiotte;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class WidgetMonitor {
  public static final String TAG = "YYYmon";
  public static float mtmin = 20;
  public static float mtmax = 60;
  public static float mnmax = 10;
  public static long maxtimeo = -1;

  private final static Pattern keyval = Pattern.compile("(\\d+) (\\d+) (\\d+) (\\d+) (\\d+) (\\d+) (\\d+) (\\d+) (\\d+)");

  public static Bitmap drawgraph(String lognamebase, float w, float h, long maxtime, float GenericTextH) {
    Bitmap graphbmp = null;
    Canvas graphcanv = null;

    if (GenericTextH < 0)
      GenericTextH = h / 25.0f;

    float dpi = 0.66f;
    double dymin = 1.0;
    double dypixmin = 6;
    float cwratio = 6.0f;
    float cw = w / cwratio;
    float vl = cw * 0.80f;

    double theta = Math.PI / 6.0;
    float sint = (float) Math.sin(theta);
    float cost = (float) Math.cos(theta);
    float vx = vl * cost;
    float vy = vl * sint;

    float xmin = 0.0f;
    float xmax = w - vx;
    cw = (w - vx) / cwratio;
    float ymin = 0.0f;
    float ymax = h - vy;

    float ttmin = 9999999999.0f;
    float ttmax = -5;
    float ccmin = ttmin;
    float ccmax = ttmax;
    float nnmin = ttmin;
    float nnmax = ttmax;
    float ppmin = ttmin;
    float ppmax = ttmax;

    float mttmin = ttmin;
    float mttmax = ttmax;
    float mccmin = ttmin;
    float mccmax = ttmax;
    float mnnmin = ttmin;
    float mnnmax = ttmax;
    float mppmin = ttmin;
    float mppmax = ttmax;

    float color = 0;

    float tcpu = 0, tgpu = 0, tbat = 0, cbat = 0, chg = 0, netd = 0, netu = 0, ccpu = 0;
    float mtcpu = 0, mtgpu = 0, mtbat = 0, mcbat = 0, mchg = 0, mnetd = 0, mnetu = 0, mccpu = 0;
    float count = 0;
    float x1, y1, x2, y2, vpx, vpy;
    double ax = 0, bx = 0, datemin = 0, date = 0, odate = 0;

    float tax = vx / (mtmax - mtmin);
    float tbx = vx - tax * mtmax;
    float tay = vy / (mtmax - mtmin);
    float tby = vy - tay * mtmax;

    float nax = vx / (mnmax - 0.0f);
    float nbx = vx - nax * mnmax;
    float nay = vy / (mnmax - 0.0f);
    float nby = vy - nay * mnmax;

    float cax = vx / (100.0f - 0.0f);
    float cbx = vx - cax * 100.0f;
    float cay = vy / (100.0f - 0.0f);
    float cby = vy - cay * 100.0f;

    float pi;
    Path path = new Path();

    Paint paintpath = new Paint();
    paintpath.setColor(Color.rgb(255, 255, 0));
    paintpath.setAntiAlias(true);
    paintpath.setStrokeWidth(1.0f);
    Paint pheure = new Paint();
    pheure.setColor(Color.rgb(255, 255, 255));
    pheure.setAntiAlias(true);
    pheure.setTextAlign(Paint.Align.RIGHT);
    pheure.setStrokeWidth(3.0f);
    pheure.setShadowLayer(5.0f, 0.0f, 0.0f, Color.BLACK);
    pheure.setTextSize(GenericTextH * 0.9f);

    double currtime = System.currentTimeMillis() / 1000.0;
    float dt = 1.0f;

    for (int logi = 10 ;  logi >= 0 ; logi--) {
      String logname = String.format("%stemp/temp%d.log", lognamebase, logi);
      File log = new File(logname);
      if (log.exists()) {
        if (graphbmp == null) {
          graphbmp = Bitmap.createBitmap((int) (w), (int) (h), Bitmap.Config.ARGB_8888);
          graphcanv = new Canvas(graphbmp);
          //graphcanv.drawColor(Color.DKGRAY);
        }
        try {
          FileReader fr = new FileReader(log);
          BufferedReader in = new BufferedReader(fr);
          String line;
          Matcher m;
          while ((line = in.readLine()) != null) {
            if (line.length() > 0) {
              m = keyval.matcher(line);
              if (m.find()) {
                date = Long.parseLong(m.group(1));
                if (maxtime > 0) {
                  if (date > 0 && date < currtime - maxtime) {
                    continue;
                  }
                }
                tcpu = (float) Integer.parseInt(m.group(2)) / 10.0f;
                tgpu = (float) Integer.parseInt(m.group(3)) / 10.0f;
                tbat = (float) Integer.parseInt(m.group(4)) / 10.0f;
                cbat = Integer.parseInt(m.group(5));
                chg = Integer.parseInt(m.group(6));
                netd = (float) Integer.parseInt(m.group(7)) / 1000.0f;
                netu = (float) Integer.parseInt(m.group(8)) / 1000.0f;
                ccpu = (float) Integer.parseInt(m.group(9)) / 10.0f;
                if (tcpu < ttmin) ttmin = tcpu;
                if (tcpu > ttmax) ttmax = tcpu;
                if (cbat < ccmin) ccmin = cbat;
                if (cbat > ccmax) ccmax = cbat;
                if (ccpu < ppmin) ppmin = ccpu;
                if (ccpu > ppmax) ppmax = ccpu;
                if (netd < nnmin) nnmin = netd;
                if (netd > nnmax) nnmax = netd;
                if (netu < nnmin) nnmin = netu;
                if (netu > nnmax) nnmax = netu;
                if (date > 0 && datemin == 0) {
                  datemin = date;
                  ax = -(ymax - ymin) / (currtime - datemin);
                  bx = -ymin - ax * datemin + (ymax - ymin);
                  dymin = ((currtime - datemin) * dypixmin) / (ymax - ymin);
                  if (dymin < 5.0)
                    dymin = 5.0;
                  double tps = datemin - (datemin % (24.0 * 3600.0));
                  if (currtime - datemin < 1.0 * 3600.0)
                    dt = 10.0f * 60.0f;
                  else if (currtime - datemin < 12.0 * 3600.0)
                    dt = 1.0f * 3600.0f;
                  else if (currtime - datemin < 72.0 * 3600.0)
                    dt = 12.0f * 3600.0f;
                  else
                    dt = 24.0f * 3600.0f;
                  while (tps < currtime) {
                    if (tps > datemin) {
                      y1 = (float) (tps * ax + bx);
                      y2 = y1;
                      x1 = 0;
                      x2 = w;
                      graphcanv.drawLine(x1, y1, x2, y2, pheure);
                    }
                    tps += dt;
                  }
                }
                //llog.d(TAG, " cbat " + cbat);
                if (date > 0 && odate > 0 && date - odate > dymin && ttmax >= 0 && nnmax >= 0 && count > 0) {

                  y1 = (float) (odate * ax + bx);
                  if (date - odate < dymin * 2)
                    y2 = (float) (date * ax + bx);
                  else
                    y2 = (float) ((odate + dymin) * ax + bx);

                  pi = 4.0f;
                  vpx = (cax * mcbat) / count + cbx;
                  vpy = (cay * mcbat) / count + cby;
                  color = (mcbat / count - 0) / (100.0f - 0);
                  drawpath(graphcanv, path, paintpath, color, cw, pi, dpi, vpx, vpy, y1, y2, w, h);

                  pi = 3.0f;
                  vpx = (tax * mtcpu) / count + tbx;
                  vpy = (tay * mtcpu) / count + tby;
                  color = (mtcpu / count - mtmin) / (mtmax - mtmin);
                  drawpath(graphcanv, path, paintpath, color, cw, pi, dpi, vpx, vpy, y1, y2, w, h);

                  pi = 2.0f;
                  vpx = (cax * mccpu) / count + cbx;
                  vpy = (cay * mccpu) / count + cby;
                  color = (mccpu / count - 0) / (100.0f - 0);
                  drawpath(graphcanv, path, paintpath, color, cw, pi, dpi, vpx, vpy, y1, y2, w, h);

                  if (mnetu > 0) {
                    pi = 1.0f;
                    vpx = (nax * mnetu) / count + nbx;
                    vpy = (nay * mnetu) / count + nby;
                    color = (mnetu / count - 0) / (mnmax - 0);
                    drawpath(graphcanv, path, paintpath, color, cw, pi, dpi, vpx, vpy, y1, y2, w, h);
                  }

                  if (mnetd > 0) {
                    pi = 0.0f;
                    vpx = (nax * mnetd) / count + nbx;
                    vpy = (nay * mnetd) / count + nby;
                    color = (mnetd / count - 0) / (mnmax - 0);
                    drawpath(graphcanv, path, paintpath, color, cw, pi, dpi, vpx, vpy, y1, y2, w, h);
                  }

                  if (mtmin > mtcpu / count)
                    mtmin = mtcpu / count;
                  if (mtmax < mtcpu / count)
                    mtmax = mtcpu / count;
                  if (mnmax < mnetu / count)
                    mnmax = mnetu / count;
                  if (mnmax < mnetd / count)
                    mnmax = mnetd / count;

                  if (mttmin > mtcpu / count)
                    mttmin = mtcpu / count;
                  if (mttmax < mtcpu / count)
                    mttmax = mtcpu / count;
                  if (mnnmin > mnetu / count)
                    mnnmin = mnetu / count;
                  if (mnnmax < mnetu / count)
                    mnnmax = mnetu / count;
                  if (mnnmin > mnetd / count)
                    mnnmin = mnetd / count;
                  if (mnnmax < mnetd / count)
                    mnnmax = mnetd / count;
                  if (mccmin > mcbat / count)
                    mccmin = mcbat / count;
                  if (mccmax < mcbat / count)
                    mccmax = mcbat / count;
                  if (mppmin > mccpu / count)
                    mppmin = mccpu / count;
                  if (mppmax < mccpu / count)
                    mppmax = mccpu / count;

                  count = 0;
                  mtcpu = 0;
                  mtgpu = 0;
                  mtbat = 0;
                  mcbat = 0;
                  mchg = 0;
                  mnetd = 0;
                  mnetu = 0;
                  mccpu = 0;

                }
                count += 1;
                mtcpu += tcpu;
                mtgpu += tgpu;
                mtbat += tbat;
                mcbat += cbat;
                mchg += chg;
                mnetd += netd;
                mnetu += netu;
                mccpu += ccpu;
                if (date - odate > dymin || odate < 1) {
                  odate = date;
                }
              }
            }
          }
          in.close();
          fr.close();
        } catch (Exception e) {
          llog.d(TAG, "IOException " + e.toString());
        }
      }
    }

    if (graphbmp != null) {
      float i = 1;
      String display;

      display = String.format("%.0f%%-%.0f%% bat", mccmax, ccmin);
      graphcanv.drawText(display, w, 0 + GenericTextH * i, pheure);
      i += 1;
      display = String.format("%.1f°C-%.1f°C", ttmax, ttmin);
      graphcanv.drawText(display, w, 0 + GenericTextH * i, pheure);
      i += 1;
      display = String.format("%.1f%%", ppmax);
      graphcanv.drawText(display, w, 0 + GenericTextH * i, pheure);
      i += 1;
      display = String.format("%.1fkB/s", nnmax);
      graphcanv.drawText(display, w, 0 + GenericTextH * i, pheure);
      i += 1.5f;

      if (dt >= 3600)
        display = String.format("%.0fs/%.0fh", dymin, dt / 3600.0f);
      else if (dt >= 60)
        display = String.format("%.0fs/%.0fmin", dymin, dt / 60.0f);
      else
        display = String.format("%.0fs/%.0fs", dymin, dt);
      graphcanv.drawText(display, w, 0 + GenericTextH * i, pheure);
      i += 1;
      display = String.format("%.1f°C-%.1f°C", mttmax, mttmin);
      graphcanv.drawText(display, w, 0 + GenericTextH * i, pheure);
      i += 1;
      display = String.format("%.1f%%-%.1f%%", mppmax, mppmin);
      graphcanv.drawText(display, w, 0 + GenericTextH * i, pheure);
      i += 1;
      display = String.format("%.1fkB/s", mnnmax);
      graphcanv.drawText(display, w, 0 + GenericTextH * i, pheure);
      i += 1.5f;

      display = String.format("%.0f%% bat", cbat);
      graphcanv.drawText(display, w, 0 + GenericTextH * i, pheure);
      i += 1;
      display = String.format("%.1f°C cpu", tcpu);
      graphcanv.drawText(display, w, 0 + GenericTextH * i, pheure);
      i += 1;
      display = String.format("%.1f%% cpu", ccpu);
      graphcanv.drawText(display, w, 0 + GenericTextH * i, pheure);
      i += 1;
      display = String.format("%.1fkB/s u", netu);
      graphcanv.drawText(display, w, 0 + GenericTextH * i, pheure);
      i += 1;
      display = String.format("%.1fkB/s d", netd);
      graphcanv.drawText(display, w, 0 + GenericTextH * i, pheure);
    }

    if (maxtimeo != maxtime) {
      maxtimeo = maxtime;
      llog.d(TAG, "send redraw " + maxtime + " " + maxtimeo);
      return drawgraph(lognamebase, w, h, maxtime, GenericTextH);
    } else {
      llog.d(TAG, "send " + maxtime + " " + maxtimeo);
      return graphbmp;
    }
  }

  private static void drawpath(Canvas graphcanv, Path path, Paint paintpath,
                               float color,
                               float cw,
                               float pi, float dpi,
                               float vpx, float vpy,
                               float y1, float y2,
                               float w, float h
  ) {
    path = new Path(); // devant
    path.moveTo(cw * pi + vpx, y1 + vpy);
    path.lineTo(cw * (pi + dpi) + vpx, y1 + vpy);
    path.lineTo(cw * (pi + dpi) + vpx, y2 + vpy);
    path.lineTo(cw * pi + vpx, y2 + vpy);
    path.close();
    paintpath.setColor(setcolor(color, 1));
    graphcanv.drawPath(path, paintpath);
    path = new Path(); // dessus
    path.moveTo(cw * pi, y2);
    path.lineTo(cw * pi + vpx, y2 + vpy);
    path.lineTo(cw * (pi + dpi) + vpx, y2 + vpy);
    path.lineTo(cw * (pi + dpi), y2);
    path.close();
    paintpath.setColor(setcolor(color, 2));
    graphcanv.drawPath(path, paintpath);
    path = new Path(); // côté
    path.moveTo(cw * pi, y1);
    path.lineTo(cw * pi + vpx, y1 + vpy);
    path.lineTo(cw * pi + vpx, y2 + vpy);
    path.lineTo(cw * pi, y2);
    path.close();
    paintpath.setColor(setcolor(color, 0));
    graphcanv.drawPath(path, paintpath);
  }

  final static float[] breakpoint = new float[]{0.25f, 0.50f, 0.75f, 1.0f};
  private static int setcolor(float c, int side) {
    float r = 0;
    float g = 0;
    float b = 0;
    float cd = 1.0f / 0.25f;
    float oo = 0.0f;
    float delt = 0.25f;
    if (c <= breakpoint[0]) {
      oo = 1.0f - breakpoint[0] * cd;
      r = 0.0f;
      g = c * cd + oo;
      b = 1.0f;
    } else if (c <= breakpoint[1]) {
      oo = 1.0f - breakpoint[1] * cd;
      r = 0.0f;
      g = 1.0f;
      b = 1.0f - (c * cd + oo);
    } else if (c <= breakpoint[2]) {
      oo = 1.0f - breakpoint[2] * cd;
      r = c * cd + oo;
      g = 1.0f;
      b = 0.0f;
    } else {
      delt = breakpoint[3] - breakpoint[2];
      cd = (1.0f - delt * delt) / (delt * breakpoint[3]);
      r = 1.0f;
      g = (c - breakpoint[3]) * (c - 2.0f * breakpoint[3]) * cd;
      b = 0.0f;
    }
    if (side == 1) {
      r = r * 0.8f;
      g = g * 0.8f;
      b = b * 0.8f;
    } else if (side == 2) {
      r = r * 0.6f;
      g = g * 0.6f;
      b = b * 0.6f;
    }
    r = r * 255.0f;
    if (r > 255) r = 255;
    g = g * 255.0f;
    if (g > 255) g = 255;
    b = b * 255.0f;
    if (b > 255) b = 255;
    return Color.rgb((int) r, (int) g, (int) b);
  }

}
