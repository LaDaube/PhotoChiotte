Featured tutorial
Set up your own secure online media server in 5 minutes <i>menu->help->readme</i>.

Features
- browse and display media at the same time
- <b>instant jump</b> to any media even in <b>huge collections</b> (graphical representation of your directory structure)
- <b>split screen</b> to compare pictures, draw, play videos or minigames, read comics... all at the same time
- integrated multiple <b>video player</b> (mpv, optional)
- create <b>collections</b>, favorites
- display and manage your apache/nginx <b><a href="https://codeberg.org/LaDaube/PhotoChiotte/src/branch/master/README.md#apache-server">online media server</a></b>, upload selections directly from app
- resize, crop & filter pictures and videos (using <a href="https://ffmpeg.org/ffmpeg-filters.html">ffmpeg filters</a>)
- <b>minigames</b> using your pictures (puzzle, swap tiles, tilt device, ...)
- <b>draw</b>, on top of pictures, cut/edit/comment them, and with <b>stylus support</b>
- <b>slideshow</b>
- <b>zapping</b> mode
- <b>animated wallpaper</b>/lockscreen with your own picture selection
- <b>comic reading</b> mode
- automatic background music player
- seamlessly <b><a href="https://codeberg.org/LaDaube/PhotoChiotte/src/branch/master/README.md#browse-forums">browse forum</a> pictures</b> bypassing ads and crap
- <i>display pictures directly from inside archives</i>
- <i>root screenshot and screenrecord commands</i>
- <i><a href="https://codeberg.org/LaDaube/PhotoChiotte/src/branch/master/README.md">howto</a> and online readme buttons (direct link to codeberg) for issues/suggestions</i>
- <i>minimal power consumption (no useless redraws)</i>
- <b>AndroidTV</b>, Watch (Wear OS), ChromeOS, AndroidAuto support

Libraries
- mpv : embedded video player, filmstrips & thumbnails, resize crop & filter pictures
- tor : discover service (will be released in v2)

Permissions
- Internet : access your online server, browse forum pictures, tor discover service
- Android 10- : write external storage : only used to copy/move/delete your selections
- Android 11+ : manage external storage : only used to copy/move/delete your selections
- Boot : on AndroidTV the music player will restart if running when shut down
