#!/bin/bash

if [ "$2" = "debug" ]
then
  DBGTST=true
else
  DBGTST=false
fi

download=false
archis=()
if [ "$1" = "arm" ]
then
  archis=(arm)
elif [ "$1" = "aarch64" ]
then
  archis=(aarch64)
elif [ "$1" = "i686" ]
then
  archis=(i686)
elif [ "$1" = "x86_64" ]
then
  archis=(x86_64)
elif [ "$1" = "download" ]
then
  download=true
elif [ "$1" = "all" ]
then
  archis=(arm aarch64 i686 x86_64)
  if [ $DBGTST = false ]
  then
    download=true
  fi
elif [ "$1" = "allnodl" ]
then
  archis=(arm aarch64 i686 x86_64)
  download=false
else
  echo -ne "take your pick :\n\tdownload\tdownload sources\n\tarm\t\tbuild arm 32bits\n\taarch64\t\tbuild arm 64bits\n\ti686\t\tbuild x86 32bits\n\tx86_64\t\tbuild x86 64bits\n\tall\t\tdownload sources and then build all 4 architectures\n\t\tdebug to build only selected repo\n\tallnodl\n\n"
  exit 0
fi

function checkerror(){
  if [ $? -ne 0 ]
  then
    echo " --- ERROR dlbuildmpv${1}/${2}"
    exit 1
  else
    echo " +++ SUCCESS dlbuildmpv${1}/${2}"
  fi
}

function checkerrornoexit(){
  if [ $? -ne 0 ]
  then
    echo "WARNING dlbuildmpv${1}/${2}"
  fi
}

if [ -d "$HOME/Android/Sdk/ndk/28.0.13004108" ]
then
  export ANDROID_NDK="$HOME/Android/Sdk/ndk/28.0.13004108"
fi
if [ ! -d "$ANDROID_NDK/toolchains/llvm/prebuilt" ]
then
  echo -ne "Please set the ANDROID_NDK path : export ANDROID_NDK=sdk/ndk/28.0.13004108\n"
  exit 1
fi
systeme=$(ls -1 "$ANDROID_NDK/toolchains/llvm/prebuilt" | head -n 1)
ndktoolchain="$ANDROID_NDK/toolchains/llvm/prebuilt/${systeme}/bin"
echo $PATH | grep ${ndktoolchain}
if [ $? -ne 0 ]
then
  export PATH="${ANDROID_NDK}:${ndktoolchain}:${PATH}"
fi
export basedir="$PWD"
export NPROC=$(nproc)
export NDK_PLATFORM_LEVEL=24

if [ $download = true ]
then
  rm -fr ${basedir}/dlbuildmpv/*
  sync
  mkdir -p ${basedir}/dlbuildmpv
  
  [ ! -f ${basedir}/lockedhashes.txt ] && echo "no locked hash found : building latest versions of cloned repos"
  
  echo -ne "" > ${basedir}/latesthashes.txt
  
  function wdhash(){
    cd "${1}"
    git log -n1 --format=format:"%ad" >> ${basedir}/latesthashes.txt
    echo -ne "     ${1}    " >> ${basedir}/latesthashes.txt
    git log -n1 --format=format:"%H" >> ${basedir}/latesthashes.txt
    echo -ne "\n" >> ${basedir}/latesthashes.txt
    if [ -f "${basedir}/lockedhashes.txt" ]
    then
      lockedhash=`awk '/ '"${1}"' / {print $NF; exit 0;}' ${basedir}/lockedhashes.txt`
      if [ "${#lockedhash}" -gt 0 ]
      then
        git checkout "${lockedhash}" .
        echo "--------> git checked out ${lockedhash} <--------- return code = $?"
      fi
    fi
  }
  
  cd ${basedir}/dlbuildmpv  && git clone https://git.ffmpeg.org/ffmpeg.git                                     && wdhash ffmpeg      || exit 1
  cd ${basedir}/dlbuildmpv  && git clone https://github.com/fribidi/fribidi.git                                && wdhash fribidi     || exit 1
  cd ${basedir}/dlbuildmpv  && git clone --recurse-submodules git://git.sv.nongnu.org/freetype/freetype2.git   && wdhash freetype2   || exit 1
  cd ${basedir}/dlbuildmpv  && git clone https://github.com/harfbuzz/harfbuzz.git                              && wdhash harfbuzz    || exit 1
  cd ${basedir}/dlbuildmpv  && git clone https://github.com/libass/libass.git                                  && wdhash libass      || exit 1
  cd ${basedir}/dlbuildmpv  && git clone --recurse-submodules https://github.com/Mbed-TLS/mbedtls.git -b v3.6.2                     && wdhash mbedtls     || exit 1
  #cd ${basedir}/dlbuildmpv  && git clone --recurse-submodules https://github.com/Mbed-TLS/mbedtls              && wdhash mbedtls     || exit 1
  #cd ${basedir}/dlbuildmpv  && git clone https://github.com/aurel32/mbedtls.git -b edwards                     && wdhash mbedtls     || exit 1
  cd ${basedir}/dlbuildmpv  && git clone https://code.videolan.org/videolan/dav1d.git                          && wdhash dav1d       || exit 1
  cd ${basedir}/dlbuildmpv  && git clone https://github.com/mpv-player/mpv.git                                 && wdhash mpv         || exit 1
  cd ${basedir}/dlbuildmpv  && git clone https://github.com/adah1972/libunibreak.git                           && wdhash libunibreak || exit 1
  
  cd ${basedir}/dlbuildmpv  && git clone --recursive https://github.com/haasn/libplacebo.git                   && wdhash libplacebo  || exit 1
  # Shaderc sources are provided by the NDK. see <ndk>/sources/third_party/shaderc

  # mpv will never support lua > 5.2
  #cd ${basedir}/dlbuildmpv
  #curl -O -s https://www.lua.org/ftp/lua-5.2.4.tar.gz
  #chksum=(`sha512sum lua-5.2.4.tar.gz`)
  #if [ "${chksum[0]}" = "cd77148aba4b707b6c159758b5e8444e04f968092eb98f6b4c405b2fb647e709370d5a8dcf604176101d3407e196a7433b5dcdce4fe9605c76191d3649d61a8c" ]
  #then
  #  tar zxf lua-5.2.4.tar.gz
  #fi
  #rm -vf lua-5.2.4.tar.gz

  sync
  
  if [ "$1" = "download" ]
  then
	echo "dowload quit"
    exit 0
  else
	echo "nothing else to do"
  fi
  echo "finished downloading"
fi

for archi in ${archis[@]}
do

  echo " -> building for ${archi}"

  if [ $DBGTST = false ]
  then
    rm -fr ${basedir}/dlbuildmpv${archi}/out
    mkdir -p ${basedir}/dlbuildmpv${archi}/out
  fi

  cd ${basedir}/dlbuildmpv${archi}
  export outdir=${basedir}/dlbuildmpv${archi}/out

  if [ $DBGTST = false ]
  then
    # enforce flat structure (/usr/local -> /)
    ln -s . "$outdir/usr"
    ln -s . "$outdir/local"
  fi

  unset CPATH LIBRARY_PATH C_INCLUDE_PATH CPLUS_INCLUDE_PATH

  # abi 21 mini for 64bits & opengl3
  # abi 24 mini for vulkan

  export mesoncpu=${archi}
  export mesoncpufamily=${archi}
  export ndkx=${archi}-linux-android
  export ndkxpath="${archi}-linux-android"
  export CC="${archi}-linux-android${NDK_PLATFORM_LEVEL}-clang"
  export CXX="${archi}-linux-android${NDK_PLATFORM_LEVEL}-clang++"

  if [ "${archi}" == "arm" ]; then

    export ndkx=${ndkx}eabi
    export ndkxpath="${ndkxpath}eabi"

    export mesoncpu=armv7a
    export CC="armv7a-linux-androideabi${NDK_PLATFORM_LEVEL}-clang"
    export CXX="armv7a-linux-androideabi${NDK_PLATFORM_LEVEL}-clang++"
  
  elif [ "${archi}" == "i686" ]; then
    
    export mesoncpufamily="x86"

  fi

  export LDFLAGS="-Wl,-z,max-page-size=16384"
  export AR="llvm-ar"
  export AS="llvm-as"
  export NM="llvm-nm"
  export LD="llvm-ld"
  export RANLIB="llvm-ranlib"
  export STRIP="llvm-strip"
  # dav1d  ../configure: line 5869: aarch64-linux-android-nm: command not found
  #        ../configure: line 5869: arm-linux-androideabi-nm: command not found
  ln -s "${ndktoolchain}/llvm-ar" "${ndktoolchain}/${ndkxpath}-ar"
  ln -s "${ndktoolchain}/llvm-as" "${ndktoolchain}/${ndkxpath}-as"
  ln -s "${ndktoolchain}/llvm-nm" "${ndktoolchain}/${ndkxpath}-nm"
  ln -s "${ndktoolchain}/llvm-ld" "${ndktoolchain}/${ndkxpath}-ld"
  ln -s "${ndktoolchain}/llvm-ranlib" "${ndktoolchain}/${ndkxpath}-ranlib"
  ln -s "${ndktoolchain}/llvm-strip" "${ndktoolchain}/${ndkxpath}-strip"
   
  
  export PKG_CONFIG_SYSROOT_DIR="$outdir"
  export PKG_CONFIG_LIBDIR="$outdir/lib/pkgconfig"
  unset PKG_CONFIG_PATH
  
  # meson wants to be spoonfed this file, so create it ahead of time
	# also define: release build, static libs and no source downloads at runtime(!!!)
  cat >${outdir}/crossfile.txt <<AAA
[built-in options]
buildtype = 'release'
default_library = 'static'
wrap_mode = 'nodownload'
prefix = '/usr/local'
[binaries]
c = '$CC'
cpp = '$CXX'
ar = '$AR'
nm = '$NM'
strip = '$STRIP'
pkgconfig = 'pkg-config'
[host_machine]
system = 'android'
cpu_family = '${mesoncpufamily}'
cpu = '${mesoncpu}'
endian = 'little'
AAA
  
  # pour openssl
  export NDK_ABI="${archi}"
  if [ "${archi}" == "aarch64" ]; then
    export NDK_ABI="arm64"
  elif [ "${archi}" == "i686" ]; then
    export NDK_ABI="x86"
  fi
  
  export APP_ABI="${archi}"
  if [ "${archi}" = "arm" ]; then
    export APP_ABI="armeabi-v7a"
  elif [ "${archi}" = "aarch64" ]; then
    export APP_ABI="arm64-v8a"
  elif [ "${archi}" = "i686" ]; then
    export APP_ABI="x86"
  fi
  export APP_PLATFORM="android-${NDK_PLATFORM_LEVEL}"
  export APP_STL="c++_shared"
  
  echo "------------------------${ndktoolchain}"
  ls -al ${ndktoolchain}/$CC
  ls -al ${ndktoolchain}/$AR
  ls -al ${ndktoolchain}/$NM
  ls -al ${ndktoolchain}/$LD
  ls -al ${ndktoolchain}/$RANLIB
  ls -al ${ndktoolchain}/$STRIP
  echo "------------------------${ndktoolchain}/${ndkxpath}*"
  ls -al ${ndktoolchain}/${ndkxpath}*
  echo "------------------------"
  ls -al $PKG_CONFIG_SYSROOT_DIR
  
  mkdir -p "${outdir}"/include
  mkdir -p "${outdir}"/lib/pkgconfig

function shaderc(){
#------------------------------------------------------------------------------
# shaderc -> necessary for libplacebo -> vulkan
#------------------------------------------------------------------------------
if [ ! -d ${basedir}/dlbuildmpv/shaderc ]
then
cp -a ${ANDROID_NDK}/sources/third_party/shaderc ${basedir}/dlbuildmpv/
sync
cd ${basedir}/dlbuildmpv/shaderc
mkdir out

ndk-build -j${NPROC} \
NDK_PROJECT_PATH=. APP_BUILD_SCRIPT=Android.mk \
NDK_APPLICATION_MK=/dev/null \
NDK_APP_OUT=out NDK_APP_LIBS_OUT=out/libs \
libshaderc_combined
checkerror ${archi} "shaderc ndk-build"
fi

cd ${basedir}/dlbuildmpv/shaderc/out
cp -r include/* "${outdir}/include"
cp libs/*/${APP_ABI}/libshaderc.a "${outdir}/lib/libshaderc_combined.a"
checkerror ${archi} "shaderc copy lib"

# create a pkgconfig file
# 'libc++' instead of 'libstdc++': workaround for meson linking bug
mkdir -p "${outdir}"/lib/pkgconfig
cat >"${outdir}"/lib/pkgconfig/shaderc_combined.pc <<"END"
Name: shaderc_combined
Description:
Version: 2021.0-unknown
Libs: -L/usr/lib -lshaderc_combined -llibc++
Cflags: -I/usr/include
END
}

function libplacebo(){
#------------------------------------------------------------------------------
# libplacebo -> necessary for vulkan needs glad >= 2.0
#------------------------------------------------------------------------------
rm -fr ${basedir}/dlbuildmpv${archi}/libplacebo
cp -a ${basedir}/dlbuildmpv/libplacebo ${basedir}/dlbuildmpv${archi}/
sync
cd ${basedir}/dlbuildmpv${archi}/libplacebo

# Android provides Vulkan, but no pkgconfig file
# you can double-check the version in vk.xml (ctrl+f VK_API_VERSION)
#mkdir -p "${outdir}"/lib/pkgconfig
#cat >"${outdir}"/lib/pkgconfig/vulkan.pc <<"END"
#Name: Vulkan
#Description:
#Version: 1.2.0
#Libs: -lvulkan
#Cflags:
#END
#ndk_vulkan="${ANDROID_NDK}/sources/third_party/vulkan/src"
#export CFLAGS="-isystem ${ndk_vulkan}/include"

mkdir out
# for vulkan :
#   -Dvulkan-registry="${ndk_vulkan}/registry/vk.xml"
meson setup out \
--cross-file ${outdir}/crossfile.txt \
-Dvulkan=disabled \
-Ddemos=false
checkerror ${archi} "libplacebo configure"

ninja -C out -j${NPROC}
checkerror ${archi} "libplacebo make"
DESTDIR="${outdir}" ninja -C out install

# add missing library for static linking
# this isn't "-lstdc++" due to a meson bug: https://github.com/mesonbuild/meson/issues/11300
${SED:-sed} '/^Libs:/ s|$| -lc++|' "${outdir}/lib/pkgconfig/libplacebo.pc" -i
}

function fribidi(){
#------------------------------------------------------------------------------
# fribidi -> necessary for libass -> necessary for mpv
#------------------------------------------------------------------------------
rm -fr ${basedir}/dlbuildmpv${archi}/fribidi
cp -a ${basedir}/dlbuildmpv/fribidi ${basedir}/dlbuildmpv${archi}/
sync
cd ${basedir}/dlbuildmpv${archi}/fribidi
mkdir out
meson setup out \
--cross-file ${outdir}/crossfile.txt \
-D{tests,docs}=false
checkerror ${archi} "fribidi configure"
ninja -C out -j${NPROC} > /dev/null
checkerror ${archi} "fribidi make"
DESTDIR="${outdir}" ninja -C out install
}

function freetype2(){
#------------------------------------------------------------------------------
# freetype2 -> necessary for libass -> necessary for mpv
#------------------------------------------------------------------------------
rm -fr ${basedir}/dlbuildmpv${archi}/freetype2
cp -a ${basedir}/dlbuildmpv/freetype2 ${basedir}/dlbuildmpv${archi}/
sync
cd ${basedir}/dlbuildmpv${archi}/freetype2
./autogen.sh
mkdir out
meson setup out \
--cross-file ${outdir}/crossfile.txt
checkerror ${archi} "freetype2 configure"
ninja -C out -j${NPROC} > /dev/null
checkerror ${archi} "freetype2 make"
DESTDIR="${outdir}" ninja -C out install
}

function harfbuzz(){
#------------------------------------------------------------------------------
# harfbuzz -> necessary for libass -> necessary for mpv
#------------------------------------------------------------------------------
rm -fr ${basedir}/dlbuildmpv${archi}/harfbuzz
cp -a ${basedir}/dlbuildmpv/harfbuzz ${basedir}/dlbuildmpv${archi}/
sync
cd ${basedir}/dlbuildmpv${archi}/harfbuzz
mkdir out
meson setup out \
--cross-file ${outdir}/crossfile.txt \
-Dtests=disabled
checkerror ${archi} "harfbuzz configure"
ninja -C out -j${NPROC} > /dev/null
checkerror ${archi} "harfbuzz make"
DESTDIR="${outdir}" ninja -C out install
}

function libunibreak(){
#------------------------------------------------------------------------------
# libunibreak
#------------------------------------------------------------------------------
rm -fr ${basedir}/dlbuildmpv${archi}/libunibreak
cp -a ${basedir}/dlbuildmpv/libunibreak ${basedir}/dlbuildmpv${archi}/
sync
cd ${basedir}/dlbuildmpv${archi}/libunibreak
./autogen.sh
mkdir out
cd out
../configure \
--host=${ndkx} \
--with-pic \
--enable-static \
--disable-shared
checkerror ${archi} "libunibreak configure"
make -j${NPROC} > /dev/null
checkerror ${archi} "libunibreak make"
make DESTDIR="$outdir" install
}

function libass(){
#------------------------------------------------------------------------------
# libass -> necessary for mpv
#------------------------------------------------------------------------------
rm -fr ${basedir}/dlbuildmpv${archi}/libass
cp -a ${basedir}/dlbuildmpv/libass ${basedir}/dlbuildmpv${archi}/
sync
cd ${basedir}/dlbuildmpv${archi}/libass
./autogen.sh
mkdir out
cd out
../configure \
--host=${ndkx} \
--with-pic \
--enable-static \
--enable-libunibreak \
--disable-shared \
--disable-require-system-font-provider
checkerror ${archi} "libass configure"
cp -vf ${outdir}/usr/local/include/harfbuzz/*.* ${outdir}/../libass/libass/
make -j${NPROC} > /dev/null
checkerror ${archi} "libass make"
make DESTDIR="$outdir" install
}

function mbedtls(){
#------------------------------------------------------------------------------
# mbedtls
#------------------------------------------------------------------------------
rm -fr ${basedir}/dlbuildmpv${archi}/mbedtls
cp -a ${basedir}/dlbuildmpv/mbedtls ${basedir}/dlbuildmpv${archi}/
sync
cd ${basedir}/dlbuildmpv${archi}/mbedtls
#------------------------------------------------------------------------------
# patch mbedtls :
#     - do not link shared libraries and keep .so extension
#cat ${basedir}/0001-no-shared-lib-link-aurel32.patch
#cp ${basedir}/0001-no-shared-lib-link-aurel32.patch .
#git apply 0001-no-shared-lib-link-aurel32.patch
#cat ${basedir}/0001-no-shared-lib-link.patch
#cp ${basedir}/0001-no-shared-lib-link.patch .
#git apply 0001-no-shared-lib-link.patch
#checkerror ${archi} "mbedtls patch"
#echo -ne "\n\n\n--------------------\n\n\n"
#------------------------------------------------------------------------------
if [ "$archi" == "i686" ]
then
	./scripts/config.py unset MBEDTLS_AESNI_C
else
	./scripts/config.py set MBEDTLS_AESNI_C
fi
# missing python3-jsonschema python3-markupsafe python3-jinja2
#pipx ensurepath
#pipx install jsonschema
#pipx install jinja2
# scripts/basic.requirements.txt
#make SHARED=1 -j${NPROC} no_test
make -j${NPROC} no_test > /dev/null
checkerror ${archi} "mbedtls make"
make DESTDIR="$outdir" install
}

function dav1d(){
#------------------------------------------------------------------------------
# dav1d
#------------------------------------------------------------------------------
rm -fr ${basedir}/dlbuildmpv${archi}/dav1d
cp -a ${basedir}/dlbuildmpv/dav1d ${basedir}/dlbuildmpv${archi}/
sync
cd ${basedir}/dlbuildmpv${archi}/dav1d
mkdir out
meson setup out \
--cross-file ${outdir}/crossfile.txt \
-Denable_tests=false \
-Db_lto=true \
-Dstack_alignment=16
checkerror ${archi} "dav1d configure"
# https://www.reddit.com/r/AV1/comments/pq669h/custom_mpvandroid_build_with_fully_uptodate_dav1d/
# added LTO option -Db_lto=true -Dstack_alignment=16 as compiler options, which manages to get an extra 7-8% decoding performance
# and march=armv8.2+simd, which optimizes the dav1d source further
# march=armv8.2+simd, that forces a minimum A75/A55 core requirement for ARM SOCs(such as the Snapdragon 845, Snapdragon 710, etc)
ninja -C out -j${NPROC} > /dev/null
checkerror ${archi} "dav1d make"
DESTDIR="${outdir}" ninja -C out install
}

function ffmpeg(){
#------------------------------------------------------------------------------
# ffmpeg
#------------------------------------------------------------------------------
rm -fr ${basedir}/dlbuildmpv${archi}/ffmpeg
cp -a ${basedir}/dlbuildmpv/ffmpeg ${basedir}/dlbuildmpv${archi}/
sync
cd ${basedir}/dlbuildmpv${archi}/ffmpeg
mkdir out
cd out
cpu=armv7-a
[[ "${archi}" == "aarch64" ]] && cpu=armv8-a
[[ "${archi}" == "x86_64" ]] && cpu=generic
[[ "${archi}" == "i686" ]] && cpu="i686 --disable-asm"
cpuflags=
[[ "${archi}" == "arm" ]] && cpuflags="${cpuflags} -mfpu=neon -mcpu=cortex-a8"
../configure \
--target-os=android \
--enable-cross-compile \
--cross-prefix=${ndkxpath}- \
--cc=$CC \
--pkg-config=pkg-config \
--nm=$NM \
--arch=${archi} \
--cpu=$cpu \
--extra-cflags="-I$outdir/include $cpuflags" \
--extra-ldflags="-L$outdir/lib" \
--enable-{jni,mediacodec,mbedtls,libdav1d} \
--disable-vulkan \
--disable-static --enable-shared \
--enable-{gpl,version3} \
--disable-{stripping,doc,programs} \
--disable-{muxers,encoders,devices} \
--enable-encoder=mjpeg,png \
--enable-muxer=mov,matroska,mpegts
#--enable-libtesseract
checkerror ${archi} "ffmpeg configure"
cp -vf ${outdir}/../mbedtls/tests/include/test/*.* ${outdir}/../out/include/mbedtls/
make -j${NPROC} > /dev/null
checkerror ${archi} "ffmpeg make"
make DESTDIR="${outdir}" install
}

function lua(){
##------------------------------------------------------------------------------
## lua
##------------------------------------------------------------------------------
rm -fr ${basedir}/dlbuildmpv${archi}/lua-5.2.4
cp -a ${basedir}/dlbuildmpv/lua-5.2.4 ${basedir}/dlbuildmpv${archi}/
sync
cd ${basedir}/dlbuildmpv${archi}/lua-5.2.4
# LUA_T= and LUAC_T= disable building lua & luac
mycflags=(
# ensures correct linking into libmpv.so
-fPIC
# bionic is missing decimal_point in localeconv [src/llex.c]
-Dgetlocaledecpoint\\\(\\\)=\\\(46\\\)
# force fallback as ftello/fseeko are not defined [src/liolib.c]
-Dlua_fseek
)
make CC="$CC" \
AR="$AR rc" \
RANLIB="$RANLIB" \
MYCFLAGS="${mycflags[*]}" \
PLAT=linux LUA_T= LUAC_T= -j${NPROC}
checkerror ${archi} "lua make"
# TO_BIN=/dev/null disables installing lua & luac
make INSTALL=${INSTALL:-install} INSTALL_TOP="${outdir}" TO_BIN=/dev/null install
checkerror ${archi} "lua make install"
# make pc only generates a partial pkg-config file because ????
mkdir -p ${outdir}/lib/pkgconfig
make pc >${outdir}/lib/pkgconfig/lua.pc
cat >>${outdir}/lib/pkgconfig/lua.pc <<'EOF'
Name: Lua
Description:
Version: ${version}
Libs: -L${libdir} -llua
Cflags: -I${includedir}
EOF
}

function mpv(){
#------------------------------------------------------------------------------
# mpv
#------------------------------------------------------------------------------
rm -fr ${basedir}/dlbuildmpv${archi}/mpv
cp -a ${basedir}/dlbuildmpv/mpv ${basedir}/dlbuildmpv${archi}/
sync
cd ${basedir}/dlbuildmpv${archi}/mpv
#------------------------------------------------------------------------------
# patch mpv :
#     - never mark packets as eof so we can always append data to it : option "eof" : "yes" / "no"
#     - force format based on mime-type header for buggy mpegts streams
cat ${basedir}/0001-no-eof-mimetype-format.patch
cp ${basedir}/0001-no-eof-mimetype-format.patch .
git apply 0001-no-eof-mimetype-format.patch
checkerror ${archi} "mpv patch"
# the packet_pool fills the device's memory up to 100%
# demux: reclaim demux_packets to reduce memory allocator pressure
# https://github.com/mpv-player/mpv/commit/038d66549dc9d4330be0cec22f958575c3cc7865
sed -i -e 's/HAVE_DISABLE_PACKET_POOL/1/' demux/packet_pool.c
# lua support : -Dlua=enabled \
#------------------------------------------------------------------------------
mkdir out
meson setup out \
--cross-file ${outdir}/crossfile.txt \
--default-library shared \
-Diconv=disabled \
-Dlibmpv=true \
-Dcplayer=false \
-Dmanpage-build=disabled
checkerror ${archi} "mpv configure"
ninja -C out -j${NPROC} > /dev/null
checkerror ${archi} "mpv make"
DESTDIR="${outdir}" ninja -C out install
}

#shaderc


libplacebo
fribidi
freetype2
harfbuzz
libunibreak
libass
mbedtls
dav1d
ffmpeg
#lua

mpv


  #------------------------------------------------------------------------------
  # copy libraries (arm aarch64 i686 x86_64)
  #------------------------------------------------------------------------------
  a=`ls -al ${outdir}/lib/*.{a,so} | wc -l`
  if [ $a -gt 0 ]
  then

    destidir="${basedir}/app/src/main/cpp/libs/${APP_ABI}"
    destindir="${basedir}/app/src/main/cpp/include/${APP_ABI}"

    mkdir -p ${destidir}
    mkdir -p ${destindir}
    cp -vfr ${outdir}/lib/*.* ${destidir}/
    cp -fr ${outdir}/include/* ${destindir}/

    sync
    
    if [ $DBGTST = false ]
    then
      rm -fr ${basedir}/dlbuildmpv${archi}/
      
      sync
    fi

  else

    echo "error no lib mpv"
    ls -al ${outdir}/lib/*.*

    exit 1

  fi

done



















