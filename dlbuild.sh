#!/bin/bash

commands=${@}
buildall="$1"

if [ "$buildall" = "all" ]
then
  rm -fr app/src/main/cpp/libs
  rm -fr app/src/main/cpp/include
fi

bash dlbuildmpv.sh ${commands}
if [ $? -ne 0 ]
then 
  echo " --- mpv build error"
  exit 1
else
  echo " +++ mpv build success"
fi

if [ "$buildall" = "all" ]
then
  rm -fr dlbuildmpv/
  echo "removed dlbuildmpv $?"
  pwd
  sync
fi

bash dlbuildtor.sh ${commands}
if [ $? -ne 0 ]
then 
  echo " --- tor build error"
  exit 1
else
  echo " +++ tor build success"
fi

if [ "$buildall" = "all" ]
then
  rm -fr dlbuildtor/
  echo "removed dlbuildtor $?"
  pwd
  sync
fi

#bash dlbuildopenvpn.sh ${commands}
#if [ $? -ne 0 ]
#then
#  echo " --- openvpn build error"
#else
#  echo " +++ openvpn build success"
#fi
#
#rm -fr dlbuildopenvpn/
#sync

