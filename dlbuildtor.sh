#!/bin/bash

if [ "$2" = "debug" ]
then
  DBGTST=true
else
  DBGTST=false
fi

download=false
archis=()
if [ "$1" = "arm" ]
then
  archis=(arm)
elif [ "$1" = "aarch64" ]
then
  archis=(aarch64)
elif [ "$1" = "i686" ]
then
  archis=(i686)
elif [ "$1" = "x86_64" ]
then
  archis=(x86_64)
elif [ "$1" = "download" ]
then
  download=true
elif [ "$1" = "all" ]
then
  archis=(arm aarch64 i686 x86_64)
  if [ $DBGTST = false ]
  then
    download=true
  fi
elif [ "$1" = "allnodl" ]
then
  archis=(arm aarch64 i686 x86_64)
  download=false
else
  echo -ne "take your pick :\n\tdownload\tdownload sources\n\tarm\t\tbuild arm 32bits\n\taarch64\t\tbuild arm 64bits\n\ti686\t\tbuild x86 32bits\n\tx86_64\t\tbuild x86 64bits\n\tall\t\tdownload sources and then build all 4 architectures\n\t\tdebug to build only selected repo\n\tallnodl\n\n"
  exit 0
fi

function checkerror(){
  if [ $? -ne 0 ]
  then
    echo " --- ERROR dlbuildtor${1}/${2}"
    exit 1
  else
    echo " +++ SUCCESS dlbuildtor${1}/${2}"
  fi
}

if [ -d "$HOME/Android/Sdk/ndk/28.0.13004108" ]
then
  export ANDROID_NDK="$HOME/Android/Sdk/ndk/28.0.13004108"
fi
if [ ! -d "$ANDROID_NDK/toolchains/llvm/prebuilt" ]
then
  echo -ne "Please set the ANDROID_NDK path : export ANDROID_NDK=sdk/ndk/28.0.13004108\n"
  exit 1
fi
systeme=$(ls -1 "$ANDROID_NDK/toolchains/llvm/prebuilt" | head -n 1)
ndktoolchain="$ANDROID_NDK/toolchains/llvm/prebuilt/${systeme}/bin"
echo $PATH | grep ${ndktoolchain}
if [ $? -ne 0 ]
then
  export PATH="${ANDROID_NDK}:${ndktoolchain}:${PATH}"
fi
export basedir="$PWD"
export NPROC=$(nproc)
export NDK_PLATFORM_LEVEL=24

if [ $download = true ]
then
  rm -fr ${basedir}/dlbuildtor/*
  sync
  mkdir -p ${basedir}/dlbuildtor
  
  [ ! -f ${basedir}/torlockedhashes.txt ] && echo "no locked hash found : building latest versions of cloned repos"
  
  echo -ne "" > ${basedir}/torlatesthashes.txt
  
  function wdhashtor(){
    cd "${1}"
    git log -n1 --format=format:"%ad" >> ${basedir}/torlatesthashes.txt
    echo -ne "     ${1}    " >> ${basedir}/torlatesthashes.txt
    git log -n1 --format=format:"%H" >> ${basedir}/torlatesthashes.txt
    echo -ne "\n" >> ${basedir}/torlatesthashes.txt
    if [ -f "${basedir}/torlockedhashes.txt" ]
    then
      lockedhash=`awk '/ '"${1}"' / {print $NF; exit 0;}' ${basedir}/torlockedhashes.txt`
      if [ "${#lockedhash}" -gt 0 ]
      then
        git checkout "${lockedhash}" .
        echo "--------> git checked out ${lockedhash} <--------- return code = $?"
      fi
    fi
  }
  
  #cd ${basedir}/dlbuildtor  && git clone git://git.openssl.org/openssl.git -b openssl-3.0                && wdhashtor openssl     || exit 1
  cd ${basedir}/dlbuildtor  && git clone https://github.com/openssl/openssl.git -b openssl-3.0           && wdhashtor openssl     || exit 1
  cd ${basedir}/dlbuildtor  && git clone https://github.com/libevent/libevent.git                        && wdhashtor libevent    || exit 1
  #cd ${basedir}/dlbuildtor  && git clone https://git.tukaani.org/xz.git                                  && wdhashtor xz          || exit 1
  cd ${basedir}/dlbuildtor  && git clone https://github.com/tukaani-project/xz.git                       && wdhashtor xz          || exit 1
  cd ${basedir}/dlbuildtor  && git clone https://github.com/madler/zlib.git                              && wdhashtor zlib        || exit 1
  cd ${basedir}/dlbuildtor  && git clone --recurse-submodules https://github.com/facebook/zstd.git                            && wdhashtor zstd        || exit 1
  cd ${basedir}/dlbuildtor  && git clone https://gitlab.torproject.org/tpo/core/tor.git                  && wdhashtor tor         || exit 1
  
  #cd ${basedir}/dlbuildtor  && git clone https://git.kernel.org/pub/scm/libs/libcap/libcap.git           && wdhashtor libcap      || exit 1
  cd ${basedir}/dlbuildtor  && git clone https://github.com/seccomp/libseccomp.git                       && wdhashtor libseccomp  || exit 1
  
  cd ${basedir}/dlbuildtor  && git clone https://github.com/nghttp2/nghttp2.git                          && wdhashtor nghttp2     || exit 1
  cd ${basedir}/dlbuildtor  && git clone https://github.com/curl/curl.git                                && wdhashtor curl        || exit 1

  sync
  
  if [ "$1" = "download" ]
  then
	echo "dowload quit"
    exit 0
  else
	echo "nothing else to do"
  fi
  echo "finished downloading"
fi

for archi in ${archis[@]}
do

  echo " -> building for ${archi}"

  if [ $DBGTST = false ]
  then
    rm -fr ${basedir}/dlbuildtor${archi}/out
    mkdir -p ${basedir}/dlbuildtor${archi}/out
  fi

  cd ${basedir}/dlbuildtor${archi}
  export outdir=${basedir}/dlbuildtor${archi}/out

  if [ $DBGTST = false ]
  then
    # enforce flat structure (/usr/local -> /)
    ln -s . "$outdir/usr"
    ln -s . "$outdir/local"
  fi

  unset CPATH LIBRARY_PATH C_INCLUDE_PATH CPLUS_INCLUDE_PATH

  # abi 21 mini for 64bits & opengl3
  # abi 24 mini for vulkan

  export mesoncpu=${archi}
  export mesoncpufamily=${archi}
  export ndkx=${archi}-linux-android
  export ndkxpath="${archi}-linux-android"
  export CC="${archi}-linux-android${NDK_PLATFORM_LEVEL}-clang"
  export CXX="${archi}-linux-android${NDK_PLATFORM_LEVEL}-clang++"

  if [ "${archi}" == "arm" ]; then

    export ndkx=${ndkx}eabi
    export ndkxpath="${ndkxpath}eabi"

    export mesoncpu=armv7a
    export CC="armv7a-linux-androideabi${NDK_PLATFORM_LEVEL}-clang"
    export CXX="armv7a-linux-androideabi${NDK_PLATFORM_LEVEL}-clang++"
  
  elif [ "${archi}" == "i686" ]; then
    
    export mesoncpufamily="x86"

  fi

  export LDFLAGS="-Wl,-z,max-page-size=16384"
  export AR="llvm-ar"
  export AS="llvm-as"
  export NM="llvm-nm"
  export LD="llvm-ld"
  export RANLIB="llvm-ranlib"
  export STRIP="llvm-strip"
  # dav1d  ../configure: line 5869: aarch64-linux-android-nm: command not found
  #        ../configure: line 5869: arm-linux-androideabi-nm: command not found
  ln -s "${ndktoolchain}/llvm-ar" "${ndktoolchain}/${ndkxpath}-ar"
  ln -s "${ndktoolchain}/llvm-as" "${ndktoolchain}/${ndkxpath}-as"
  ln -s "${ndktoolchain}/llvm-nm" "${ndktoolchain}/${ndkxpath}-nm"
  ln -s "${ndktoolchain}/llvm-ld" "${ndktoolchain}/${ndkxpath}-ld"
  ln -s "${ndktoolchain}/llvm-ranlib" "${ndktoolchain}/${ndkxpath}-ranlib"
  ln -s "${ndktoolchain}/llvm-strip" "${ndktoolchain}/${ndkxpath}-strip"

  export PKG_CONFIG_SYSROOT_DIR="$outdir"
  export PKG_CONFIG_LIBDIR="$outdir/lib"
  unset PKG_CONFIG_PATH
  
  # meson wants to be spoonfed this file, so create it ahead of time
	# also define: release build, static libs and no source downloads at runtime(!!!)
  cat >${outdir}/crossfile.txt <<AAA
[built-in options]
buildtype = 'release'
default_library = 'static'
wrap_mode = 'nodownload'
prefix = '/usr/local'
[binaries]
c = '$CC'
cpp = '$CXX'
ar = '$AR'
strip = '$STRIP'
pkgconfig = 'pkg-config'
[host_machine]
system = 'android'
cpu_family = '${mesoncpufamily}'
cpu = '${mesoncpu}'
endian = 'little'
AAA

  # pour openssl
  export NDK_ABI="${archi}"
  if [ "${archi}" == "aarch64" ]; then
    export NDK_ABI="arm64"
  elif [ "${archi}" == "i686" ]; then
    export NDK_ABI="x86"
  fi
  
  export APP_ABI="${archi}"
  if [ "${archi}" = "arm" ]; then
    export APP_ABI="armeabi-v7a"
  elif [ "${archi}" = "aarch64" ]; then
    export APP_ABI="arm64-v8a"
  elif [ "${archi}" = "i686" ]; then
    export APP_ABI="x86"
  fi
  export APP_PLATFORM="android-${NDK_PLATFORM_LEVEL}"
  export APP_STL="c++_shared"

  ls -al ${ndktoolchain}/$CC
  ls -al ${ndktoolchain}/$AR
  ls -al ${ndktoolchain}/${ndkxpath}*
  ls -al $PKG_CONFIG_SYSROOT_DIR
  
  mkdir -p "${outdir}"/include
  mkdir -p "${outdir}"/lib/pkgconfig
  
function openssl(){
rm -fr ${basedir}/dlbuildtor${archi}/openssl
cp -a ${basedir}/dlbuildtor/openssl ${basedir}/dlbuildtor${archi}/
sync
cd ${basedir}/dlbuildtor${archi}/openssl
#git checkout openssl-3.0
export CFLAGS="-Wno-macro-redefined"
mkdir out
cd out
# shared ou no-shared
# deprecated no-ssl2 no-hw
../Configure \
no-comp \
no-dtls \
no-ec2m \
no-psk \
no-srp \
no-ssl3 \
no-camellia \
no-idea \
no-md2 \
no-md4 \
no-mdc2 \
no-rc2 \
no-rc4 \
no-rc5 \
no-rmd160 \
no-whirlpool \
no-dso \
no-ui-console \
no-shared \
no-unit-test \
android-${NDK_ABI} \
-D__ANDROID_API__=${NDK_PLATFORM_LEVEL} \
--prefix=/ \
--openssldir=/
checkerror ${archi} "openssl configure"
export CFLAGS="-fPIC"
make -j${NPROC} install_dev DESTDIR="$outdir" > /dev/null
checkerror ${archi} "openssl make"
}

function libevent(){
rm -fr ${basedir}/dlbuildtor${archi}/libevent
cp -a ${basedir}/dlbuildtor/libevent ${basedir}/dlbuildtor${archi}/
sync
cd ${basedir}/dlbuildtor${archi}/libevent
./autogen.sh
checkerror ${archi} "libevent autogen"
#./configure --help
mkdir out
cd out
../configure \
--host=${ndkx} \
--disable-libevent-regress \
--disable-samples \
--disable-shared \
--enable-static \
--with-pic \
--prefix=/
checkerror ${archi} "libevent configure"
export CFLAGS="-fPIC"
make -j${NPROC} > /dev/null
checkerror ${archi} "libevent make"
make -j${NPROC} install DESTDIR="$outdir"
checkerror ${archi} "libevent make install"
}

function lzma(){
rm -fr ${basedir}/dlbuildtor${archi}/xz
cp -a ${basedir}/dlbuildtor/xz ${basedir}/dlbuildtor${archi}/
sync
cd ${basedir}/dlbuildtor${archi}/xz
./autogen.sh \
--no-po4a
# error doxygen docs checkerror ${archi} "lzma autogen"
mkdir out
cd out
../configure \
--host=${ndkx} \
--enable-static \
--disable-doc \
--disable-lzma-links \
--disable-lzmadec \
--disable-lzmainfo \
--disable-scripts \
--disable-shared \
--disable-xz \
--disable-xzdec \
--prefix=/
checkerror ${archi} "lzma configure"
export CFLAGS="-fPIC"
make -j${NPROC} > /dev/null
checkerror ${archi} "lzma make"
make -j${NPROC} install DESTDIR="$outdir"
checkerror ${archi} "lzma make install"
}

function zlib(){
rm -fr ${basedir}/dlbuildtor${archi}/zlib
cp -a ${basedir}/dlbuildtor/zlib ${basedir}/dlbuildtor${archi}/
sync
cd ${basedir}/dlbuildtor${archi}/zlib
mkdir out
cd out
../configure \
--static \
--prefix=${outdir}
checkerror ${archi} "zlib configure"
export CFLAGS="-fPIC"
make -j${NPROC} > /dev/null
checkerror ${archi} "zlib make"
make -j${NPROC} install
checkerror ${archi} "zlib make install"
}

function zstd(){
rm -fr ${basedir}/dlbuildtor${archi}/zstd
cp -a ${basedir}/dlbuildtor/zstd ${basedir}/dlbuildtor${archi}/
sync
cd ${basedir}/dlbuildtor${archi}/zstd
# TODO : pb missing from tor
export CFLAGS="-fPIC"
make -j${NPROC} -C lib install DESTDIR="$outdir" > /dev/null
checkerror ${archi} "zstd make"
cp lib/deprecated/zbuff.h ${outdir}/include
}

function libseccomp(){
rm -fr ${basedir}/dlbuildtor${archi}/libseccomp
cp -a ${basedir}/dlbuildtor/libseccomp ${basedir}/dlbuildtor${archi}/
sync
cd ${basedir}/dlbuildtor${archi}/libseccomp
autoreconf -fi
mkdir out
cd out
# missing gperf
../configure \
--enable-static \
--disable-shared \
--host=${ndkx} \
--prefix=${outdir}
checkerror ${archi} "libseccomp configure"
export CFLAGS="-fPIC"
make -j${NPROC} install > /dev/null
checkerror ${archi} "libseccomp make install"
}

function tor(){
rm -fr ${basedir}/dlbuildtor${archi}/tor
cp -a ${basedir}/dlbuildtor/tor ${basedir}/dlbuildtor${archi}/
sync
cd ${basedir}/dlbuildtor${archi}/tor
#------------------------------------------------------------------------------
# patch tor :
#     - Destroying TorService a second time causes crash https://github.com/guardianproject/tor-android/issues/57
cat ${basedir}/0001-tor-fdsan-double-close-crash.patch
cp ${basedir}/0001-tor-fdsan-double-close-crash.patch .
git apply 0001-tor-fdsan-double-close-crash.patch
checkerror ${archi} "tor patch"
#------------------------------------------------------------------------------
autoreconf -fi
checkerror ${archi} "tor autogen"
mkdir out
cd out
# --host=$(ALTHOST) pas $HOST pour armv7a -> arm
#../configure --help
# --disable-seccomp
../configure \
--host=${ndkx} \
--enable-android \
--enable-lzma \
--enable-pic \
--enable-gpl \
--enable-static-libevent --with-libevent-dir=${outdir} \
--enable-static-openssl --with-openssl-dir=${outdir} \
--enable-static-zlib --with-zlib-dir=${outdir} \
--enable-zstd \
--disable-module-dirauth \
--disable-asciidoc \
--disable-manpage \
--disable-html-manual \
--enable-static-tor \
--disable-system-torrc \
--disable-tool-name-check \
--prefix=${outdir}
checkerror ${archi} "tor configure"
grep -E '^# *define +HAVE_LZMA +1$$' orconfig.h
grep -E '^# *define +HAVE_ZSTD +1$$' orconfig.h
grep -E '^# *define +ENABLE_OPENSSL +1$$' orconfig.h
grep -E '^# *define +HAVE_TLS_METHOD +1$$' orconfig.h
export CFLAGS="-fPIC"
make -j${NPROC} > /dev/null
checkerror ${archi} "tor make"
install -d ${outdir}/lib
#cp -v src/app/tor ${outdir}/lib/libtor.so
cp -v orconfig.h ${outdir}/include
cp -v ${basedir}/dlbuildtor${archi}/openssl/out/configdata.pm ${outdir}/include
cp -v libtor.a ${outdir}/lib/libtor.a
checkerror ${archi} "tor no libtor.a"
cd ../src
while read f
do
mkdir -p ${outdir}/include/"${f%/*}"
cp -f "${f}" ${outdir}/include/"${f}"
done < <(find . -name "*.h")
}

function nghttp2(){
rm -fr ${basedir}/dlbuildtor${archi}/nghttp2
cp -a ${basedir}/dlbuildtor/nghttp2 ${basedir}/dlbuildtor${archi}/
sync
cd ${basedir}/dlbuildtor${archi}/nghttp2
autoreconf -fi
mkdir out
cd out
../configure \
--host=${ndkx} \
--enable-static \
--disable-shared \
--without-libxml2 \
--enable-lib-only \
--prefix=${outdir}
checkerror ${archi} "nghttp2 configure"
export CFLAGS="-fPIC"
make -j${NPROC} install > /dev/null
checkerror ${archi} "nghttp2 make install"
}

function curl(){
rm -fr ${basedir}/dlbuildtor${archi}/curl
cp -a ${basedir}/dlbuildtor/curl ${basedir}/dlbuildtor${archi}/
sync
cd ${basedir}/dlbuildtor${archi}/curl
autoreconf -fi
mkdir out
cd out
../configure \
--host=${ndkx} \
--with-ssl=${outdir} \
--with-openssl=${outdir} \
--with-pic \
--with-zlib=${outdir} \
--with-zstd=${outdir} \
--with-nghttp2=${outdir} \
--without-libpsl \
--disable-verbose \
--disable-versioned-symbols \
--enable-threaded-resolver \
--enable-ipv6 \
--enable-static \
--disable-shared \
--disable-dict \
--disable-gopher \
--disable-imap \
--disable-ldap \
--disable-ldaps \
--disable-pop3 \
--disable-rtsp \
--disable-file \
--disable-ftp \
--disable-smb \
--disable-smtp \
--disable-telnet \
--disable-tftp \
--disable-manual \
--disable-progress-meter \
--prefix=/
checkerror ${archi} "curl configure"
export CFLAGS="-fPIC"
make -j${NPROC} > /dev/null
checkerror ${archi} "curl make"
make -j${NPROC} install DESTDIR="$outdir"
checkerror ${archi} "curl make install"
}


openssl
libevent
lzma
zlib
zstd
libseccomp
tor
nghttp2
curl



  #------------------------------------------------------------------------------
  # copy libraries (arm aarch64 i686 x86_64)
  #------------------------------------------------------------------------------
  a=`ls -al ${outdir}/lib/*.{a,so} | wc -l`
  if [ $a -gt 0 ]
  then

    destidir="${basedir}/app/src/main/cpp/libs/${APP_ABI}"
    destindir="${basedir}/app/src/main/cpp/include/${APP_ABI}"

    mkdir -p ${destidir}
    mkdir -p ${destindir}
    cp -vfr ${outdir}/lib/*.* ${destidir}/
    cp -fr ${outdir}/include/* ${destindir}/

    #ndktoolchainlib="${ndktoolchainbase}/sysroot/usr/lib/${ndkx}"
    #cp -vfr ${ndktoolchainlib}/21/liblog.so  ${destidir}/
    #cp -vfr ${ndktoolchainlib}/21/libEGL.so ${destidir}/
    #cp -vfr ${ndktoolchainlib}/21/libGLESv2.so ${destidir}/
    #cp -vfr ${ndktoolchainlib}/21/libGLESv3.so ${destidir}/
    #cp -vfr ${ndktoolchainlib}/libc++_shared.so ${destidir}/

    sync
    
    if [ $DBGTST = false ]
    then
      rm -fr ${basedir}/dlbuildtor${archi}/
      
      sync
    fi

  else

    echo "error no lib tor"
    ls -al ${outdir}/lib/*.*

    exit 1

  fi
  
done



















