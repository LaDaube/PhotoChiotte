# [<img src="app/src/main/res/mipmap-xxxhdpi/ic_launcher.png" alt="icon" height="100">](https://f-droid.org/repo/la.daube.photochiotte_63.apk) PhotoChiotte      [<img src="gplv3-with-text-136x68.png" alt="GPLv3" height="40">](https://f-droid.org/repo/la.daube.photochiotte_63.apk)      
 
[<img src="https://img.shields.io/f-droid/v/la.daube.photochiotte.svg?logo=F-Droid&label=Free/Libre%20Open%20Source%20Software&color=green&link=https://f-droid.org/repo/la.daube.photochiotte_63.apk" alt="v1.36" height="20">](https://f-droid.org/repo/la.daube.photochiotte_63.apk)

[<img src="https://fdroid.gitlab.io/artwork/badge/get-it-on.png" alt="Get it on F-Droid" height="60">](https://f-droid.org/packages/la.daube.photochiotte/)


<img src="fastlane/metadata/android/en-US/images/phoneScreenshots/3.jpg" height="250">
<img src="fastlane/metadata/android/en-US/images/phoneScreenshots/2.jpg" height="250">
<img src="fastlane/metadata/android/en-US/images/phoneScreenshots/5.jpg" height="250">
<img src="fastlane/metadata/android/en-US/images/phoneScreenshots/4.jpg" height="250">

###### media gallery and comic reader to handle easily huge device and online collections
<img src="fastlane/metadata/android/en-US/images/featureGraphic.jpg" width="400" height="200">

###### featured tutorial
Set up your own secure online media server in 5 minutes. Tutorial [below](#online-media-server).

#### features

* browse and display media at the same time
* **instant jump** to any media even in **huge collections** (graphical representation of your directory structure)
* **split screen** to compare pictures, draw, play videos or minigames, read comics... all at the same time
* integrated multiple **video player** (mpv, optional)
* create **collections**, favorites
* display and manage your apache/nginx [**online media server**](#online-media-server), upload selections directly from app
* resize, crop & filter pictures and videos (using [ffmpeg filters](https://ffmpeg.org/ffmpeg-filters.html))
* **minigames** using your pictures (puzzle, swap tiles, tilt device, ...)
* **draw**, on top of pictures, cut/edit/comment them, with **stylus support**
* **slideshow**
* **zapping** mode
* **animated wallpaper**/lockscreen with your own selection
* **comic reading** mode
* automatic background music player
* seamlessly **[browse forums](#browse-forums) pictures** bypassing ads and crap
* _display pictures directly from inside archives_
* _root screenshot and screenrecord commands_
* _howto and online readme buttons (direct link to codeberg) for issues/suggestions_
* _minimal power consumption (no useless redraws)_
* **AndroidTV**, Watch (Wear OS), ChromeOS, AndroidAuto support

# how to
###### browse
* pinch 2 fingers : zoom picture or thumbnails (depends on position)
* click left/right of media or bottom left/right corners of the screen : previous/next media
* click center of media : zoom switch between full size and fit screen
* **3 fingers** or top left corner of the screen : show/hide **menu**
* if the menu is shown : graphical representation of the whole gallery on the left, click wherever you want to jump to
* 4 fingers : show current file name and info
* 5 fingers : only show the main picture
* back button : you need to click it 3 times to exit app and free resources
###### play video
* **3 fingers** : show/hide **menu**
* click **top** of video : **zapping** mode
* click center : play/pause
* click right/left : jump forward/backward in time (time delta depends on menu setting)
* click **bottom** : show **timebar** and current position, click where you want to jump to
* pinch 2 fingers : zoom video
* move 2 fingers around : move video around (inside its surface)
###### androidTV : browse
* ok : switch between fit picture / default zoom mode
* menu : currently selected menu item is underlined in yellow, use pad left/right and top/down to navigate
* back : click 3 times to quit
* numeric / media keys : control the background music player
* keys 1/2/3 : zoom in/out/switch mode
* keys 4/5/6 : music player previous/next/playpause
* keys 7/8/9 : music player stop/random/linear playback
* key 0 : navigate to next media
###### androidTV : play video
* ok : play/pause video
* menu : again, navigate with pad
* **pad top** : zapping mode
* **pad bottom** : show **timebar** and current position
* pad left/right : jump backward/forward in time (time delta depends on menu setting)
###### watch (wear OS)
* 2 fingers without moving : show/hide menu
* 2 fingers without moving, one on top and the other either on the left or on the right side : back. Twice : exit app.

### important
###### hardware acceleration
Hardware acceleration is enabled by default on non-AndroidTV devices.
If browsing media is laggy on your device you can try to switch it off : _menu -> tweaks -> browser hw -> browser sw_.
###### playing videos smoothly
Depending on your device you will find better to use software or hardware acceleration (_menu -> tweaks -> gpu-next hardware copy to ram_). 
**Software decoding** always works. 
If the playback isn't smooth try **hardware copy to ram** (mediacodec-copy) or **hw+** (mediacodec).
If it still doesn't render properly use **hardware direct rendering** (mediacodec-embed). This is the default mode on AndroidTV, 
and you'll benefit from your TV/device's motion interpolation. Subtitles are rendered on an overlay.

### in depth : additional details
###### comments
Instead of renaming medias, commenting them won't modify the original files. Comments are appended to the thumbnails and are preserved when transferred. 
_menu->modify->comment->printName_
###### TV mode
For TV AOSP firmwares or for big video collections you may want to force PhotoChiotte to behave as with an androidTV device :
_menu->tweaks->tv mode on_ (+ _browser sw_ + _mpv hardware direct rendering_ recommended for TV boxes).
###### slideshow
The slideshow runs as long as the purple slideshow submenu is opened (with delay adjustments). 
Hide the menu (3 fingers) and it will start. Showing the menu will pause the slideshow.
Moving around will delay the slideshow.
###### bookmarks
Bookmarks are meant to be volatile. You can display them in a virtual folder and thus build up something like
a favorites collection. But it is recommended to create a collection for that : you can jump back
to different places of your galleries thanks to the `collections -> goto` button.
It keeps a soft reference to the original file as well.
###### music player
The music player will autoplay/autostop whenever you quit/launch a video. You can control it with the standard android media action buttons, 
through the notification, the menu, or with the homescreen widget. On AndroidTV the music player will autostart at boot if enabled when shut down.
You can search for music from another device using 
``adb shell am start -a android.intent.action.VIEW -n la.daube.photochiotte/la.daube.photochiotte.MainActivity -e SearchMusicR "regular expression, case insensitive..."``
###### selections
When creating collections clicking on a folder name section will add the whole folder and all of its subfolders, 
and will preserve the directory structure in the collection.
###### wallpaper/lockscreen
Setting the minimum zoom to 100% will display only one media at a time and will try to fill the screen.
###### picture quality
You can enable dithering to try to improve picture quality in _menu -> modify -> dither off -> dither on_.
###### menu buttons
Only the more opaque part of the menu text is clickable.
###### messages
Click their leftmost part to put them on hold.
###### cleanup
This will remove all thumbnails created from this app in its own cache directory. Note that when accessing online collections, 
thumbnails and media are directly read from the online server and kept in cache in RAM 
(thus nothing is copied/stored on the disk, not even the thumbnails since this is the server's role.)

# tutorials
- [online media server](#online-media-server)
  - [basic local setup](#online-media-server)
  - [upload photos from app](#upload-photos-from-app)
  - [thumbnails](#thumbnails)
  - [worldwide secure access](#internet-access)
  - [nginx (alternative to apache)](#nginx-alternative)
- [collections](#collections)
  - [create a collection](#collections)
  - [buttons as medias](#buttons-as-media)
- [browse forums](#browse-forums)
- [build PhotoChiotte](#build-photochiotte)
- [contact](#contact)
- [download](#download)


# online media server
###### access and browse your own huge collection of media from anywhere in the world, and on all your android devices
Connect your old laptop to your wifi, connect your external hard drive containing your collections of media/movies/photos/comics and install an apache server to be able to access them.
1. find out your laptop LAN IP
```
ip address show
> 192.168.0.35
```
2. install and start apache
```
sudo apt-get install apache2
sudo systemctl start httpd
```
Now you are able to access your server from your android device in a browser using e.g. `http://192.168.0.35`

3. mount your external hard drive and link it to your apache root folder
```
sudo mount /dev/sdb1 /mnt/usb
ln -s /mnt/usb /srv/http/media
```
Now you are able to browse your media collection in PhotoChiotte using `http://192.168.0.35/media/`

4. optional but recommended for an expanded view (instead of 3. the dynamic one). 
* create a folder index from server
```
cd /mnt/usb/
find . -type f -printf "%h/\n" | uniq | sort -f | uniq > folderlist.txt
```
  Now open the **menu** in PhotoChiotte, click **online media**, and then **add online media source**.
  Type in `http://192.168.0.35/media/folderlist.txt` and... done !
* _or_ create a folder index from app _menu->online media->add online media source_ :
  add `http://192.168.0.35/media/folderlist.txt`, even if this file doesn't exist yet. Click _U_ on its side.
  This will recursively scan all valid folders starting from `folderlist.txt`'s parent folder `http://192.168.0.35/media/`
  and then save it back on your server as `http://192.168.0.35/media/folderlist.txt`.


Notes :

PhotoChiotte merely identifies folderlists by the `.txt` extension.

The _update U_ button is useful when you upload new content along with folders from app to your server.


## upload photos from app
It's the same as you would manage medias on your device : _menu->share/copy->copy_ will upload your selection if the destination path is on your web server. 
Similarly you can move, delete and create new folders on your server from app. Thumbnails will also 
automatically be uploaded or deleted alongside.

For this enable webDav in apache : edit `/etc/httpd/conf/httpd.conf`
- uncomment Dav modules
```
LoadModule dav_module modules/mod_dav.so
LoadModule dav_fs_module modules/mod_dav_fs.so
LoadModule dav_lock_module modules/mod_dav_lock.so
```
- append
```
DAVLockDB /srv/http/DAVLock
Alias /media "/srv/http/media/"
<Directory "/srv/http/media/">
  DAV On
  AllowOverride None
  Options Indexes FollowSymLinks
  Require all granted
</Directory>
```
- give apache permission to write to your external hard drive
```
<IfModule unixd_module>
  User http
  Group users
</IfModule>
```
- here we make it writable by any member of our group `users`
```
sudo chown http:users /srv/http
sudo chmod g+w -R /mnt/usb
```
Finally restart the service `sudo systemctl restart httpd`.



## thumbnails
Optional but recommended : it's a good idea to generate thumbnails on the server side since PhotoChiotte won't store anything on your device.
The "apache online" method tries to grab the `folder/subfolder/file.ext.thmb_0` thumbnail that is expected to correspond to the `folder/subfolder/file.ext` file.
If this fails it will generate a thumbnail from the big files, which makes it optional but heavier for both the device and the server.

- build the thumbnail creator (based on GNU libextractor)
```
sudo apt-get install ffmpeg
wget https://codeberg.org/LaDaube/PhotoChiotte/raw/branch/master/app/src/main/cpp/thumb.{h,cpp}
gcc -lavcodec -lpostproc -lswresample -lavutil -lavformat -lswscale -lavfilter -lavdevice thumb.cpp -o thumb.o
```
- create thumbnails

`./thumb.o`   `source file`   `destination thumbnails`   `width`   `height`   
`maximum number of filmstrip pictures (for videos)`   
`maximum percentage of the video`
```
while read f
do
  if [ ! -f "${f}.thmb_0" ]
  then
    nice -n 19 ./thumb.o "${f}" "${f}.thmb" 256 256 15 100
  fi
done < <(find /mnt/usb/ -type f -size +40k | grep -vP '\.thmb[._]\d+$')
```

Notes :

`printName=`, `printDetails=`, `printFooter=`, `playInSequence=true`, `playStartAtPosition=`, `subtitleAddress=`, `subtitleAddressEncode=` can be appended 
in the `.thmb_0` file to add comments or more information to medias. For instance `echo "printName=Mont Blanc versant sud" >> DSC01234.JPG.thmb_0` 
will show _Mont Blanc versant sud_ for the filename when the picture `DSC01234.JPG` is viewed in PhotoChiotte.

Updates :

- v1.45 02/11/2023 : full decode if fast seeking methods fail

- v1.46 09/11/2023 : metadata : handle rotation, grab creation_time

- v1.50 14/02/2024 : pictures can have comments and more information, just like other media types.
  The old `*.thmb$` format is deprecated. Please re-create the thumbnails on your server
  to automatically move on to the new format :
```
while read f
do
  nice -n 19 ./thumb.o "${f}" "${f}.thmb" 256 256 15 100
done < <(find /mnt/usb/ -type f -size +40k | grep -vP '\.thmb([._]\d+)?$')
```


## internet access
To access your collections from anywhere in the world install openvpn on your media server.

- Replace 12.23.34.45 with your server's own public ip address, to be able to access it from the internet.
```
export PUBLIC_IP_WWW="12.23.34.45"
```
- Install openvpn.
```
apt-get install -y openvpn openssl easy-rsa
```
- Generate openvpn keys for secure communication with your server.
```
cd /etc/openvpn/server
mkdir -p sample-ca
touch sample-ca/index.txt
echo "01" > sample-ca/serial
wget https://raw.githubusercontent.com/OpenVPN/openvpn/refs/heads/master/sample/sample-keys/openssl.cnf
openvpn --genkey secret ta.key
openssl req -new -newkey rsa:4096 -days 20000 -nodes -x509 -extensions easyrsa_ca -keyout sample-ca/ca.key -out sample-ca/ca.crt -subj "/C=FR/ST=IDF/L=Paris/O=website.fr openVPN/emailAddress=parisgo@website.fr" -config openssl.cnf
openssl req -new -nodes -config openssl.cnf -subj "/C=FR/ST=IDF/O=website.fr openVPN/CN=VPNserver/emailAddress=parisgo@website.fr" -keyout sample-ca/server.key -out sample-ca/server.csr
openssl ca -batch -config openssl.cnf -extensions server -out sample-ca/server.crt -in sample-ca/server.csr
openssl req -new -nodes -config openssl.cnf -subj "/C=FR/ST=IDF/O=website.fr openVPN/CN=VPNclient/emailAddress=parisgo@website.fr" -keyout sample-ca/client.key -out sample-ca/client.csr
openssl ca -batch -config openssl.cnf -out sample-ca/client.crt -in sample-ca/client.csr
openssl dhparam -out dh2048.pem 2048
cp -vf sample-ca/server.key sample-ca/server.crt sample-ca/client.key sample-ca/client.crt sample-ca/ca.crt .
```
- Create the openvpn client configuration file `client.ovpn`. You can share it with all your devices. Import it in your openvpn app.
```
cat >client.ovpn<<EOF
client
dev tun
tun-mtu 1400
mssfix 1360
proto udp
remote ${PUBLIC_IP_WWW} 443
resolv-retry infinite
nobind
persist-key
persist-tun
remote-cert-tls server
cipher AES-256-GCM
data-ciphers AES-256-GCM
data-ciphers-fallback AES-256-GCM
verb 3
auth-user-pass
auth SHA512
auth-nocache
EOF
echo "<ca>" >> client.ovpn
cat ca.crt >> client.ovpn
echo "</ca>" >> client.ovpn
echo "key-direction 1" >> client.ovpn
echo "<tls-crypt>" >> client.ovpn
cat ta.key >> client.ovpn
echo "</tls-crypt>" >> client.ovpn
echo "<cert>" >> client.ovpn
cat client.crt >> client.ovpn
echo "</cert>" >> client.ovpn
echo "<key>" >> client.ovpn
cat client.key >> client.ovpn
echo "</key>" >> client.ovpn

cp /etc/openvpn/server/client.ovpn /srv/http/client.ovpn
```
- Create the server configuration file.
```
cat >server.conf<<EOF
local 192.168.0.35
port 443
proto udp
dev crypt
dev-type tun
tun-mtu 1400
mssfix 1360
ca /etc/openvpn/server/ca.crt
cert /etc/openvpn/server/server.crt
key /etc/openvpn/server/server.key
dh /etc/openvpn/server/dh2048.pem
topology subnet
server 10.69.69.0 255.255.255.0
push "redirect-gateway def1 bypass-dhcp"
push "dhcp-option DNS 10.69.69.1"
client-to-client
duplicate-cn
keepalive 20 120
tls-crypt /etc/openvpn/server/ta.key
cipher AES-256-GCM
data-ciphers AES-256-GCM
data-ciphers-fallback AES-256-GCM
auth SHA512
persist-key
persist-tun
status /var/log/openvpn/openvpn-status.log
log /var/log/openvpn/openvpn.log
verb 3
explicit-exit-notify 1
script-security 2
username-as-common-name
plugin /usr/lib/openvpn/plugins/openvpn-plugin-auth-pam.so login
EOF

chown -R openvpn:network /etc/openvpn/server
mkdir /var/log/openvpn
chown openvpn:network /var/log/openvpn
```
- Create new users or use your current user credentials. This will be your login/password for the openvpn client app.
```
useradd -m -p $(echo "crappypassword" | openssl passwd -1 -stdin) "openvpnuser"
```
- Start your secure internet media server.
```
systemctl start openvpn-server@server
```
- Open your media server's port 443 on your router to be able to access it from the internet.
- Install the Free/Libre Open Source openVPN app on your android devices.
- Import `http://192.168.0.35/client.ovpn` in openVPN and connect to your server with your login/password.

This way the online media source you added before is still valid, so no change is needed in the PhotoChiotte app.
Just connect to your openVPN server et voilà !


## nginx alternative
By default apache enables `Options Indexes FollowSymLinks` which displays directory listings when you try to access 
any folder from a browser (e.g. `http://192.168.0.35/media/`). For nginx you might need to enable it though :
```
autoindex on;
try_files $uri $uri/ =404;
sendfile on;
tcp_nopush on;
```
Also there are some performance tweaks that may be useful e.g. `worker_processes, worker_rlimit_nofile, worker_connections, multi_accept, sendfile_max_chunk, ...`.


To enable uploads from app comment out the `try_files $uri $uri/ =404;` line above and append
```
dav_methods PUT DELETE MKCOL COPY MOVE;
client_max_body_size 0;
create_full_put_path on;
dav_access  group:rw all:r;
client_body_buffer_size 10M;
```
Also give nginx permission to write to your hard drive
```
user http users;
```






# collections
The live wallpaper and the background music player also work with your _online media_ server. 
Generate a `folderlist.txt` for your music and another one for your pictures, you will then be able to create 
custom collections with whatever source you want. 
This is the prefered way for homogeneous media collections (music, personal pictures and videos, comics...).


However you can also directly populate PhotoChiotte medias and folders by specifying 
the address to a `.sel` collection in the _online media_ menu. 
All of PhotoChiotte variables are accessible with this more flexible method. 
This `collection.sel` consists of a text file with sequential operations on its elements, followed by variable=value lines.
 1. start with an operation (+ add, - remove, / modify) on an element e.g. `element=+element_media`
 2. continue with wanted variable=value lines until the next `element=+element_media` operation

An [element](app/src/main/java/la/daube/photochiotte/Element.java) 
can be either a [collection](app/src/main/java/la/daube/photochiotte/Collection.java), 
an [ordner](app/src/main/java/la/daube/photochiotte/Ordner.java), 
or a [media](app/src/main/java/la/daube/photochiotte/Media.java). 
See their respective java files for an exhaustive list. 
All variables above the `/** generated **/` comment are exposed.

 1. start with an element_collection element
```
$ cat > /mnt/usb/testcoll.sel <<EOF

element=+element_collection
printName=a test collection

element=+element_ordner
address=/dummy/cherry picked !

EOF
```
Its printName will be the one shown in the app when the `http://192.168.0.35/media/testcoll.sel` is loaded. 

Ordners always need an `address=` for identification, be it a dummy one. If we set a `printName=cherry` for this ordner 
the app will show `cherry` instead of `/dummy/cherry picked !` for the folder name.

 2. add a picture to this empty `/dummy/cherry picked !` container
```
$ cat >> /mnt/usb/testcoll.sel <<EOF

element=+element_media
address=https://codeberg.org/LaDaube/PhotoChiotte/raw/branch/master/gplv3-with-text-136x68.png
isOnline=online_apache
ordnerAddress=/dummy/cherry picked !

EOF
```
The `address=` can also be local, but if it needs to be retrieved online (`http://.*`),
you need to set the `isOnline=` variable. 

If there was a `https://codeberg.org/LaDaube/PhotoChiotte/raw/branch/master/gplv3-with-text-136x68.png.thmb_0` thumbnail 
it would also be retrieved automatically when needed thanks to the `online_apache` value. 
To override this set the `addressToGetThumbnail=` variable 
`addressToGetThumbnail=https://codeberg.org/LaDaube/PhotoChiotte/raw/branch/master/app/src/main/res/mipmap-xxxhdpi/ic_launcher.png` 
(and optionally `addressToGetPreviewFullSize=` for video previews). 
Here since there is no `.thmb_0` thumbnail available for this picture 
it would be better to set the variable to `isOnline=online`.

The `ordnerAddress=` will populate the `/dummy/cherry picked !` folder with this picture. 
If not set the media will be placed in the last `element_ordner` of its collection (here also `/dummy/cherry picked !`).

 3. add a video
```
$ cat >> /mnt/usb/testcoll.sel <<EOF

element=+element_media
addressEncode=http://192.168.0.35/media/mes vidéos/Ne ne doesn't exist.mkv
printName=This video is a broken link.
printDetails=Additionnal text.
printFooter=2 audio 3 subs
isOnline=online_apache
type=media_video
ordnerAddress=/dummy/cherry picked !

EOF
```
Important : if you list files from your apache server you will have to do URL encoding (here a valid link would be 
`address=http://192.168.0.35/media/mes%20vid%C3%A9os/Ne%20ne%20doesn't%20exist.mkv`).
You can append `Encode` to any string variable (here `address`, but also `ordnerAddress`, etc...) to ask the app to URL encode your value.

 4. add another ordner
```
$ cat >> /mnt/usb/testcoll.sel <<EOF

element=+element_ordner
addressEncode=http://192.168.0.35/media/Randonnée 2023/
printName=Randonnée 2023
isOnline=online_apache

EOF
```
Whenever the user reaches this folder the app will populate this ordner with the content of `/mnt/usb/Randonnée 2023/*` 
through your apache server.



Collections can also load subcollections to avoid loading everything at startup. 
- this collection will load the `testcoll.sel` above when browsed
```
$ cat > /mnt/usb/master.sel <<EOF

element=+element_collection
printName=Parent collection.

element=+element_ordner
addressEncode=http://192.168.0.35/media/testcoll.sel
isOnline=online_apache

EOF
```
Whenever the user reaches this ordner the linked collection will be loaded.

- or load `testcoll.sel` only when clicked
```
$ cat > /mnt/usb/master.sel <<EOF

element=+element_collection
printName=Parent collection.

element=+element_ordner
address=/dummy/test coll
printName=Click the media underneath to load the testcoll collection.

element=+element_media
ordnerAddress=/dummy/test coll
addressEncode=http://192.168.0.35/media/testcoll.sel
isALinkThatCreatesAnOrdner=true
isOnline=online_apache
printName=Click to load testcoll.sel.

EOF
```
This is merely triggered by the variable `isALinkThatCreatesAnOrdner=true`.


Other useful variables :
- setting the `updateDeltaTime=`_n_ ordner variable will update its whole content every _n_ seconds
- setting the `byDefaultShowMediaNumber=`_n_ ordner variable will show media number _n_ by default. 
Negative values will start counting from the end (e.g. antepenultimate will be -3).
- set the `playInSequence=true` media variable to play videos that are pieces of a single stream in sequence
- the `playStartAtPosition=`_n_ media variable adds the menu button : start track at position _n_ seconds
- the `subtitleAddress=`_subtitleAddress_ media variable adds the external subtitle _subtitleAddress_.
Repeat for multiple external subs, one variable=value per line, as always.


Notes :

You can append `Encode` to any string variable (`address=http` -> `adressEncode=http`, `ordnerAdressEncode=`, `subtitleAddressEncode=`, etc...) 
to ask the app to URL encode your value.

Any line that is not starting with a known variable= is ignored. Variables are case insensitive. Values are read till the end of the line.

`printName=`, `printDetails=`, `printFooter=`, `playInSequence=true`, `playStartAtPosition=`, `subtitleAddress=`, `subtitleAddressEncode=` can be appended 
in the `.thmb_0` file instead for convenience when moving around medias.



## buttons as media
Create dynamic drawable medias that can also contain menus and buttons to execute shell commands,
send http requests to control servers or display remote information, etc.
An example would be `menu->widgets->show`.

Exposed canvas drawing functions (`drawrect=`, `drawcircle=`, `drawtext=`, `runcmd=`, ...) 
can be found in the function [ldpcdrawdraw()](app/src/main/java/la/daube/photochiotte/ThreadMiniature.java).

Exposed button actions (`buttonruncmd=`, `buttongethtml=`, ...) 
can be found in the function [buttonaction()](app/src/main/java/la/daube/photochiotte/ThreadMiniature.java).

Here we create a collection that contains a single media with two buttons on it, 
one to check the current airplane mode and the other to change it.
Additionally it displays the current battery level of the device live.

1. Create a collection in the downloads folder.

```
cat >/storage/emulated/0/Download/hello.sel<<EOF

element=+element_collection
printName=hello

element=+element_ordner
address=/media/hello/

element=+element_media
type=media_button
address=/storage/emulated/0/Download/hello

EOF
```
The media type needs to be set as `type=media_button`.

2. Draw the actual `.thmb.0` media.

```
cat >/storage/emulated/0/Download/hello.thmb.0<<EOF
LDPCDRAW
wh= w 400 h 400
drawrect= r 0 g 0 b 0 s 0 xi 0 xf 1 yi 0 yf 1

drawcircle= r 0.253 g 0.194 b 0.386 s 0 x 0.20 y 0.30 r 0.10
drawtext= r 0.834 g 0.796 b 0.913 s 0.044 c x 0.20 y 0.311 txt airplane status
buttonruncmd= xi 0.10 xf 0.30 yi 0.20 yf 0.40 msg y cmd settings get global airplane_mode_on ; echo "airplane status"

 a1drawcircle= r 0.253 g 0.194 b 0.386 s 0 x 0.60 y 0.30 r 0.10
 a1drawtext= r 0.834 g 0.796 b 0.913 s 0.044 c x 0.60 y 0.311 txt airplane off
 a1buttonruncmd= xi 0.50 xf 0.70 yi 0.20 yf 0.40 msg n cmd settings put global airplane_mode_on 1 ; sed -i -e 's/^ a2/a2/' -e 's/^a1/ a1/' /storage/emulated/0/Download/hello.thmb.*
a2drawcircle= r 0.500 g 0.194 b 0.386 s 0 x 0.60 y 0.30 r 0.10
a2drawtext= r 0.834 g 0.796 b 0.913 s 0.044 c x 0.60 y 0.311 txt airplane on
a2buttonruncmd= xi 0.50 xf 0.70 yi 0.20 yf 0.40 msg n cmd settings put global airplane_mode_on 0 ; sed -i -e 's/^ a1/a1/' -e 's/^a2/ a2/' /storage/emulated/0/Download/hello.thmb.*

runcmd= cmd cat /sys/class/power_supply/battery/capacity ; echo "% " r 0.8 g 0.8 b 0.6 s 0.066 l x 0.33 y 0.6 r 0.6 g 0.6 b 0.6 s 0.044 r x 0.33 y 0.6

dateformat= r 0.8 g 0.8 b 0 s 0.055 c x 0.5 y 0.9 txt EEEE dd MMMM HH:mm:ss

EOF
```
- - `LDPCDRAW` is mandatory to begin with.
  - `wh= w 400 h 400` sets the width and the height of the media to 400 pixels.
  - `drawrect= r 0 g 0 b 0 s 0 xi 0 xf 1 yi 0 yf 1` draws a black rectangle, the background.
  - r, g, b within range [0.00:1.00]
  - xi,xf = left, right within [0.00:1.00] as a ratio of the width of the media specified above.
  - yi,yf = top, bottom within [0.00:1.00] as a ratio of the height.
  - s within [0.00:1.00] as a ratio of the height of the picture sets the border size of the rectangular region. 
0 means fill with solid color.
  - c draws the text centered, l aligned left, r aligned right.
  - `msg y` displays the command result in a red popup, whereas `msg n` won't.
  - `runcmd= cmd` draws the result of the commands on the media. These are run every update cycle of the picture. In the same line repeat setting the color, the size and the position of the text for each command output line.
- Lines can start with any `\w*` word characters and we still be read (`a2drawtext=`). 
If the line starts with any another character (`  a1drawtext=` starts with a space) it will be ignored. 
Thus here the `buttonruncmd=` commands that switch the airplane mode also execute `sed` 
to alternatively activate or deactivate `a1` or `a2` prefixed drawings. 
This allows drawing dynamic buttons, menus or medias. 

3. Create the `.thmb_0` thumbnail.

Setting the filmstripCount to 2 or more in its `.thmb_0` thumbnail will force 
the picture to be updated every update cycle (4s by default but you can adjust it in the menu).
Setting it to 1 will only reload it when clicked.
```
cat >/storage/emulated/0/Download/hello.thmb_0<<EOF
filmstripCount=2
EOF
```

4. We did set it to `filmstripCount=2`, thus we need to create 2 medias.

```
cp /storage/emulated/0/Download/hello.thmb.0 /storage/emulated/0/Download/hello.thmb.1
```

Rescan the `/storage/emulated/0/Download/` folder to show up this `hello.sel` collection and click it to load it and see the results.




# browse forums
###### browse directly pictures scraped from forums, as a spider
This will display forum pictures as if part of your collection by scraping the forum, bypassing ads and flood, in an expanded view. 
Any forum can be browsed this way through the PhotoChiotte app. 

Here are a few examples :
* (18+ adult forum) pornBB.org `CmFkZHJlc3M9aHR0cDovL3d3dy5wb3JuYmIub3JnL2ZvcnVtCmhvc3Q9aHR0cDovL3d3dy5wb3JuYmIub3JnCjBmb3J1bXM9PGEgaHJlZj0uKFteXCJcJ10rKVwuaHRtbC4gY2xhc3M9LmZvcnVtbGluay4+CjFwYWdpbmF0aW9uPS0wLmh0bWwKMXBhZ2luYXRpb25kZWx0YT01MAoxcGFnaW5hdGlvbnN0YXJ0PTAKMWZvcnVtc3BhZ2VzPTxhIGNsYXNzPS5idG4gYnRuLWRlZmF1bHQuIGhyZWY9LlteXCdcIl0rLShcZCspXC5odG1sCjF0aHJlYWRzK3BhZ2U9PGEgaHJlZj0uKFteXCJcJ10rPykoLShcZCspKT9cLmh0bWxbXj5dKz48aSBjbGFzcz0uaWNvIGktbGFzdC1yZXBseS4+CjJwYWdpbmF0aW9uPS0wLmh0bWwKMnBhZ2luYXRpb25kZWx0YT0xNQoycGFnaW5hdGlvbnN0YXJ0PTAKMmltYWdlcyttaW5pPTxhIGhyZWY9LihodHRwW15cJ1wiXSspLiB0YXJnZXQ9Ll9ibGFuay4gY2xhc3M9LnBvc3RsaW5rLiByZWw9Lm5vZm9sbG93Lj48aW1nIHNyYz0uaHR0cFteXCdcIl0rLiBjbGFzcz0uaW1nLXJlc3BvbnNpdmUuIGFsdD0uc2NyZWVuc2hvdC4KMm1pbmlpbmltYWdlcz1zcmM9LihodHRwW15cIlwnXSspCg==`
* (18+ adult forum) vipergirls.to `CmFkZHJlc3M9aHR0cHM6Ly92aXBlcmdpcmxzLnRvL2ZvcnVtLnBocApob3N0PWh0dHBzOi8vdmlwZXJnaXJscy50bwowZm9ydW1zPTxoMiBjbGFzcz0uZm9ydW10aXRsZS4qP2hyZWY9Lihmb3J1bXMvW15cIlwnP10rKQoxcGFnaW5hdGlvbj0vcGFnZTAKMXBhZ2luYXRpb25kZWx0YT0xCjFwYWdpbmF0aW9uc3RhcnQ9MQoxZm9ydW1zcGFnZXM9PGEgaHJlZj0uamF2YXNjcmlwdDovLy4gY2xhc3M9LnBvcHVwY3RybC5bXj5dKj5QYWdlIFxkKyBvZiAoXGQrKTwvYT4KMXRocmVhZHMrcGFnZT0gaHJlZj0uKHRocmVhZHMvW15cJ1wiLz9dKykoL3BhZ2UoXGQrKSk/CjJwYWdpbmF0aW9uPS9wYWdlMAoycGFnaW5hdGlvbmRlbHRhPTEKMnBhZ2luYXRpb25zdGFydD0xCjJpbWFnZXMrbWluaT08YSBbXj5dKmhyZWY9LihodHRwW15cIlwnXSspW14+XSs+W14+PF0qPGltZyBbXj5dKnNyYz0uaHR0cFteXCJcJ10qCjJtaW5paW5pbWFnZXM9c3JjPS4oaHR0cFteXCJcJ10rKQo=`

These are just base64 encoded strings.
1. long press the base64 string and copy it
2. open the PhotoChiotte menu (3 fingers or top left corner)
3. click online media
4. click add online media source
5. long press the text input field and paste your base64 string

Done !



# build PhotoChiotte
```
git clone https://codeberg.org/LaDaube/PhotoChiotte.git
cd PhotoChiotte
```
The first time you need to build mpv and tor libraries.
Dependencies are pretty basic, just have a look at [F-Droid's build metadata](https://gitlab.com/fdroid/fdroiddata/-/blob/master/metadata/la.daube.photochiotte.yml).
```
curl -s https://gitlab.com/fdroid/fdroiddata/-/raw/master/metadata/la.daube.photochiotte.yml | tail -n 23
      - apt-get install -y meson ninja-build autoconf automake libtool make nasm pkg-config
        autopoint cmake python3-markupsafe python3-jsonschema python3-jinja2 gperf
```
Specify the path to your [Android NDK](https://developer.android.com/ndk/downloads/).
```
export ANDROID_NDK="/path/to/android/sdk/ndk/28.0.13004108"
```
Only move if you want to build the latest versions of the libraries :
```
mv -vi lockedhashes.txt lastworkingmpvhashes.txt
mv -vi torlockedhashes.txt lastworkingtorhashes.txt
```
Build the libraries.
```
bash dlbuild.sh all
```
Build the apk either by importing the project in AndroidStudio, or headless with
```
export ANDROID_HOME="/path/to/android/sdk"
./gradlew build assembleDebug
```

Android 7+ : NDK r28 ABI 24

Android 5+ : NDK r25c ABI 21



# contact
The official repository is located at codeberg.org and it is preferable to [post issues, critics and feature requests there](https://codeberg.org/LaDaube/PhotoChiotte/issues) 
unless your local mafia forbids it.

Anonymous mails are welcome. The spam folder is checked automatically 
just specify PhotoChiotte in the subject to be sure.

[fnepcbxayn@tutanota.com](mailto:fnepcbxayn@tutanota.com)



# download

Built and signed by F-Droid.org.

[<img src="https://fdroid.gitlab.io/artwork/badge/get-it-on.png" alt="Get it on F-Droid" height="60">](https://f-droid.org/packages/la.daube.photochiotte/)

https://f-droid.org/repo/la.daube.photochiotte_63.apk

https://codeberg.org/LaDaube/PhotoChiotte/releases/download/v1.62/la.daube.photochiotte_63.apk

https://gitlab.com/-/project/13376566/uploads/a8f3b49e69e22de4a4941247d23450c5/la.daube.photochiotte_63.apk














































